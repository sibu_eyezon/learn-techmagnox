CREATE TABLE public.email_request
(
    id bigserial NOT NULL,
    event_name character varying(200),
    data json,
    status character varying(200),
    template_subject_text character varying(200),
    template_body_text text,
    message_subject character varying(200),
    message_body text,
    to_address character varying(200),
    cc_address character varying(255),
    error_reason text,
    retry_count integer,
    add_datetime timestamp without time zone,
    update_datetime timestamp without time zone,
    PRIMARY KEY (id)
);

ALTER TABLE public.email_request
    OWNER to postgres;