CREATE TABLE public.email_template
(
    id bigint NOT NULL DEFAULT nextval('email_template_id_seq'::regclass),
    title character varying(200) COLLATE pg_catalog."default",
    details text COLLATE pg_catalog."default",
    add_datetime timestamp without time zone,
    update_datetime timestamp without time zone,
    status character(1) COLLATE pg_catalog."default",
    CONSTRAINT email_template_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.email_template
    OWNER to postgres;