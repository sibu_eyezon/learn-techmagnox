$(function(){
	$('#frmreg').validate({
		errorPlacement: function(error, element) {
		  $(element).closest('.form-group').append(error);
		},
		rules: {
			fname: {
				required: true,
				minlength: 3
			},
			lname: {
				required: true
			},
			email: {
				required: true,
				email: true,
			},
			mobile: {
				required: true,
				minlength: 10,
				maxlength: 10,
				digits: true
			},
			newpass: {
				required: true,
				minlength: 6,
			},
			c_code: {
				remote: {
					url: baseURL+'Login/checkCouponStatus',
					type: 'POST',
					data: { c_code: $('#c_code').val() }
				}
			}
		},
		messages: {
			fname: {
				required: 'Must enter firstname.',
				minlength: 'Minimum 3 characters.'
			},
			lname: {
				required: 'Must enter lasstname.'
			},
			email: {
				required: 'Must enter email address.',
				email: 'Invalid email address.'
			},
			mobile: {
				required: 'Must enter your mobile number.',
				minlength: 'Must have 10 digits.',
				maxlength: 'Must have 10 digits.',
				digits: 'Must have digits.'
			},
			newpass: {
				required: 'Must enter your password',
				minlength: 'Password length must be minimum 6.',
			},
		},
		submitHandler: (form, e)=>{
			e.preventDefault();
			$('#btn_save').attr('disabled', true);
			$('#loading').css("display", "block");
			
			var frmApplyData = new FormData($('#frmreg')[0]);
			$.ajax({
				url: baseURL+'Login/userAdmission',
				type: 'POST',
				data: frmApplyData,
				cache : false,
				processData: false,
				contentType: false,
				enctype: 'multipart/form-data',
				success: (res)=>{ 
					$('#frmreg')[0].reset();
					$('#btn_save').removeAttr('disabled');
					$('#loading').css("display", "none");
					var obj = JSON.parse(res);
					if(obj['status']=='success'){
						swal(
						  obj['title'],
						  obj['msg'],
						  obj['status']
						).then(result=>{
							window.location.href = baseURL+obj['webURL'];
						});
					}else{
						swal(
						  obj['title'],
						  obj['msg'],
						  obj['status']
						)
					}
					
				},
				error: (errors)=>{
					console.log(errors);
				}
			});
		}
	});
});

function toggleRegLogin(adm_type)
{
	if(adm_type=='signin'){
		$('#reg_frm').hide();
		$('#adm_type').val(adm_type);
		$('.info-text').html('Don`t have an account? <a href="javascript:toggleRegLogin(`register`);" class="btn btn-sm btn-primary">Sign up</a>');
		$('#btn_save').html('Log in');
	}else if(adm_type=='register'){
		$('#reg_frm').show();
		$('#adm_type').val(adm_type);
		$('#btn_save').html('Sign in');
		$('.info-text').html('Already have an account? <a href="javascript:toggleRegLogin(`signin`);" class="btn btn-sm btn-primary">Log in</a>');
	}
}