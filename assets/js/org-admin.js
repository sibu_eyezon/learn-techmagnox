$(document).ready(function(){
    $('.icon-container').hide();
    organizationAndDepartment();
    programTable();
    preProgramTypeCheck();
    preProgramFeeCheck();
    teacherList();
});

/*********************************************************************************************************************************/
/*                                          Organization And Department                                                          */
/*********************************************************************************************************************************/

// Organization List
function organizationAndDepartment(){
    $('#prg_progress').show();
    $.ajax({
        url : baseURL+'Admin/organizationAndDepartment',
        type : 'POST',
        success: function(data){
            $('#prg_progress').hide();
            $('#prog_department').html(data);
        }
    })
}
/***************************************************** */
// Organization Add And Update Modal
function organizationModal(oper,id=null){
    var oper = oper;
    var id = id;
    $.ajax({
        url:baseURL+'Admin/organizationModalCall',
        type: "POST",
        data:{
            id : id
        },
        success: function(data){ 
			//$('#organizationModal').resizable();
			$("#organizationModal").draggable({ handle: ".modal-header", cursor: "move" });
            $('#organizationModal').modal('show');
            $('#organizationModalLabel').html(oper+' Organization');
            $('#organizationModalBody').html(data);

        }
    });
}
/************************************************************ */
// Organization Add And Update Operation
function organizationInsertUpdate(){ 
    let eTitle    = false;
    let eUrl      = false;
    let logo      = document.getElementById('avatar').files[0]; 
    let crop_logo = $('#crop_img').val();
    let logo_path = $('#logo').val();
    let title     = $('#title').val();
    let url       = $('#url').val();
    let id        = $('#id').val();
    let contact   = CKEDITOR.instances['contact'].getData();
    let details   = CKEDITOR.instances['details'].getData();
    if(title == ''){
        $('#error_title').html('This field is required');
    }else{
        eTitle = true;
        $('#error_title').html('');
    }
    if(url == ''){
        $('#error_url').html('This field is required');
    }else{
        eUrl = true;
        $('#error_url').html('');
    }
    if(eTitle && eUrl){
        let formData = new FormData();
        formData.append('logo',logo);
        formData.append('clogo',crop_logo);
        formData.append('logo_path',logo_path);
        formData.append('title',title);
        formData.append('url',url);
        formData.append('id',id);
        formData.append('contact',contact);
        formData.append('details',details);
        $.ajax({
            url : baseURL+'Admin/organizationInsertUpdate',
            method:'POST',
            data:formData,
            contentType:false,
            cache:false,
            processData:false,
            enctype:'multipart/form-data',
            success:function(resp){
                let responce = JSON.parse(resp);
                $('#organizationModal').modal('hide');
                organizationAndDepartment();
                if(responce.permission){
                    Swal.fire("Congratulation","Successfuly submitted","success");
                }else{
                    Swal.fire("Sorry","There is some error", "error");
                }
            }
        })
    }
    
}
// Department Add And Update Modal
function departmentModal(oper,org_id,id=null){
    var oper   = oper;
    var org_id = org_id;
    var id     = id;
    $.ajax({
        url:baseURL+'Admin/departmentModalCall',
        type: "POST",
        data:{
            org_id : org_id,
            id     : id
        },
        success: function(data){ 
            $('#departmentModal').modal('show');
            $('#departmentModalLabel').html(oper+' Department');
            $('#departmentModalBody').html(data);

        }
    });
}
// Department Add And Update Operation
function departmentInsertUpdate(){
    let eTitle     = false;
    let logo       = document.getElementById('avatar').files[0]; 
    let crop_logo  = $('#crop_img').val();
    let logo_path  = $('#logo').val();
    let title      = $('#title').val();
    let short_name = $('#short_name').val();
    let url        = $('#url').val();
    let id         = $('#id').val();
    let org        = $('#org_id').val();
    let contact    = CKEDITOR.instances['dept_contact'].getData();
    let details    = CKEDITOR.instances['dept_details'].getData();
    if(title == ''){
        $('#error_title').html('This field is required');
    }else{
        eTitle = true;
        $('#error_title').html('');
    }
    if(eTitle){
        let formData = new FormData();
        formData.append('logo',logo);
        formData.append('clogo',crop_logo);
        formData.append('logo_path',logo_path);
        formData.append('title',title);
        formData.append('short_name',short_name);
        formData.append('url',url);
        formData.append('id',id);
        formData.append('org',org);
        formData.append('contact',contact);
        formData.append('details',details);
        $.ajax({
            url : baseURL+'Admin/departmentInsertUpdate',
            method:'POST',
            data:formData,
            contentType:false,
            cache:false,
            processData:false,
            enctype:'multipart/form-data',
            success:function(resp){
                let responce = JSON.parse(resp);
                $('#departmentModal').modal('hide');
                organizationAndDepartment();
                if(responce.permission){
                    Swal.fire("Congratulation","Successfuly submitted","success");
                }else{
                    Swal.fire("Sorry","There is some error", "error");
                }
            }            
        })
    }
}


/*********************************************************************************************************************************/
/*                                                  Program                                                                      */
/*********************************************************************************************************************************/

$('#add_new_org').click(function(){
    if($('#org_title').val() === ''){
        $('#org_title').focus();
        return false;
    }else{
        $('#org_title').blur();
        return true;
    }
});

$('#org').change(function(){
    var data = $(this).val();
    departByOrganization(data);
    programOrgTable(data);
});

$('#dept').change(function(){
    var org  = $('#org').val();
    var dept = $(this).val();
    programOrgDeptTable(org,dept);
});

function programTable(){
    $('#prg_progress').css('display', 'block');
    $('#porgList').html('');
    $.ajax({
        url  : baseURL+'Admin/programTable',
        type : 'POST',
        success: function(data){
            $('#prg_progress').css('display', 'none');
            $('#porgList').html(data);
        }
    });
}

function departByOrganization(org_id){
    $('#dept').html('').selectpicker('refresh');
    $('.icon-container').show();
    var org = org_id;
    if(org == ''){
        $('.icon-container').hide();
        $('#dept').html('').selectpicker('refresh');
    }else{
        $('.icon-container').show();
        $.ajax({
            url  : baseURL+'Admin/departmentByOrganization',
            type : 'POST',
            data :{
                org : org
            },
            success : function(data,status){
                $('.icon-container').hide();
                $('#dept').html(data).selectpicker('refresh');
            }
        });
    }
}

function programOrgTable(data){
    $('#prg_progress').css('display', 'block');
    $('#porgList').html('');
    if(data){
        $.ajax({
            url  : baseURL+'Admin/programTableByOrg',
            type : 'POST',
            data : {data : data},
            success: function(data){
                $('#prg_progress').css('display', 'none');
                $('#porgList').html(data);
            }
        });
    }else{
        programTable(); 
    }
}


function programOrgDeptTable(org,dept){
    $('#prg_progress').css('display', 'block');
    $('#porgList').html('');
    if(dept){
        $.ajax({
            url  : baseURL+'Admin/programTableByOrgAndDept',
            type : 'POST',
            data : {
                org : org,
                dept : dept
            },
            success: function(data){
                $('#prg_progress').css('display', 'none');
                $('#porgList').html(data);
            }
        });
    }else{
        programOrgTable(org); 
    }
}


$('#dtype').change(function(){
    if($(this).val() == 'year'){
        $('#sem_details').css('display','block');
    }else{
        $('#sem_details').css('display','none');
    }
});

$('.form-check-label input:radio').click(function() {
    if($(this).val() == 1){
        $('#ad_details').css('display','block');
    }else if($(this).val() == 2){
        $('#ad_details').css('display','none');
    }else if($(this).val() == 'Paid'){
        $('#fees').removeAttr('disabled');
        $('#rebate').removeAttr('disabled');
    }else{
        $('#fees').attr('disabled','disabled');
        $('#rebate').attr('disabled','disabled');
    }
});

$('#apply_type').change(function(){
    if($(this).val() == 1){
        $('#stype').css('display','block');
    }else{
        $('#stype').css('display','none');
    }
});

function preProgramTypeCheck(){
    var course_type = $('#type input:radio:checked').val();
    if(course_type == 1){
        $('#ad_details').css('display','block'); 
        preCheckApplyType();
    }
}

function preProgramFeeCheck(){
    var course_fee = $('#fee input:radio:checked').val();
    if(course_fee == "Paid"){
        $('#fees').removeAttr('disabled');
        $('#rebate').removeAttr('disabled');
    }
}

function preCheckApplyType(){
    var apply_type = $('#apply_type').val();
    if(apply_type == 1){
        $('#stype').show();
    }
}


/*********************************************************************************************************************************/
/*                                                  Teacher                                                                      */
/*********************************************************************************************************************************/


function teacherList(page=null,record=null,organization=null,department=null){
    $('#prg_progress').show();
    $.ajax({
        url  : baseURL+'Admin/teacherList',
        type : 'POST',
        data : {
            page   : page,
            record : record,
            organization : organization,
            department: department
        },
        success: function(data){
            $('#prg_progress').css('display', 'none');
            $('#datatables_wrapper').html(data);
        }
    });
}

function teacherPagination(page){
    teacherList(page);
}
function showEntries(){
    record = $('#entries').val();
    teacherList(``,record);
}

function teacherUpdateModalCall(id,page){
    $.ajax({
        url: baseURL+'Admin/teacherUpdateModalCall',
        type : 'POST',
        data : {
            id     : id,
            page   : page
        },
        success: function(data){
            $('#teacherModal').modal('show');
            $('#modal_body').html(data);
        }
    })
}

function teacherUpdate(){
    let efirst_name = false;
    let elast_name = false;
    let photo      = document.getElementById('avatar').files[0]; 
    let crop_logo  = $('#crop_img').val();
    let photo_path = $('#path').val();
    let first_name = $('#first_name').val();
    let last_name  = $('#last_name').val();
    let salutation = $('#salutation').val();
    let linkedin   = $('#linkedin').val();
    let id         = $('#id').val();
    let page       = $('#page').val();
    let about      = CKEDITOR.instances['about_me'].getData();
    let record     = $('#entries').val();
    if(first_name == ''){
        $('#error_first_name').html('This field is required');
    }else{
        efirst_name = true;
        $('#error_first_name').html('');
    } 
    if(last_name == ''){
        $('#error_last_name').html('This field is required');
    }else{
        elast_name = true;
        $('#error_last_name').html('');
    }  
    
    if(efirst_name && elast_name){
        let formData = new FormData();
        formData.append('photo',photo);
        formData.append('clogo',crop_logo);
        formData.append('photo_path',photo_path);
        formData.append('first_name',first_name);
        formData.append('last_name',last_name);
        formData.append('salutation',salutation);
        formData.append('id',id);
        formData.append('linkedin',linkedin);
        formData.append('about',about);
        $.ajax({
            url : baseURL+'Admin/teacherUpdate',
            method:'POST',
            data:formData,
            contentType:false,
            cache:false,
            processData:false,
            enctype:'multipart/form-data',
            success:function(resp){
                let responce = JSON.parse(resp);
                $('#teacherModal').modal('hide');
                teacherList(page,record);
                if(responce.permission){
                    Swal.fire("Congratulation","Successfuly submitted","success");
                }else{
                    Swal.fire("Sorry","There is some error", "error");
                }
            }            
        })
    }
}
