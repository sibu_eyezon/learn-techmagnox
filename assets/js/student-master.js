/**************CHECKED***************/
$(document).ready(function(){
    $('.icon-container').hide();
	$('#studentDataTable').DataTable({
		"ordering": true,
		"info": true,
		"paging": true,
		"lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
		"lengthChange": true,
		dom: '1B<"top"lf>rt<"bottom"ip><"clear">',
		buttons: [
			{
				extend: 'excelHtml5',
				title: 'Pre Admission List of <?php echo trim($prog[0]->title).' - '.trim($prog[0]->yearnm); ?>',
				text:'Excel',
				className: 'bg-success text-white mr-2',
				exportOptions: {
					columns: [0, 2, 3, 4, 5, 6]
				}
			},
			{
				extend: 'pdfHtml5',
				title: 'Pre Admission List of <?php echo trim($prog[0]->title).' - '.trim($prog[0]->yearnm); ?>',
				text: 'PDF',
				className: 'bg-success text-white mr-2',
				exportOptions: {
					columns: [0, 2, 3, 4, 5, 6]
				}
			},
			{
				extend: 'print',
				title: 'Pre Admission List of <?php echo trim($prog[0]->title).' - '.trim($prog[0]->yearnm); ?>',
				text: 'Print',
				className: 'bg-success text-white mr-2',
				exportOptions: {
					columns: [1, 2, 3, 4, 5, 6, 7]
				}
			}
		]
	});
	$('[name="studentDataTable_length"]').addClass('browser-default');
});

$('#acaprog').change(function(){
    $('.icon-container').show();
    var programId = $(this).val();
    $('#acasem').html('').selectpicker('refresh');
    $.ajax({
        url: baseURL+'Teacher/getAcademicSemester',
        type: 'POST',
        data:{
            programId : programId
        },
        success: function(data){
            $('.icon-container').hide();
            $('#acasem').html(data).selectpicker('refresh');
        }
    })
});

$('#mainCheckBox').on('click',function(){
    if(this.checked){
        $('.checkbox').each(function(){
            this.checked = true;
        });
    }else{
        $('.checkbox').each(function(){
            this.checked = false;
        });
    }
});

function clickChangeEvent(){
    if($('.checkbox:checked').length == $('.checkbox').length){
        $('#mainCheckBox').prop('checked',true);
    }else{
        $('#mainCheckBox').prop('checked',false);
    }
}

function studentManagerTable(){
    var prog = false; //
    var sem  = false; //
    var year = true; //
    var acaprog = '';
    var acasem = '';
    var acayear = '';

    if($('#acaprog').val() == ''){
        $('#err_prog').html('This field is required');
    }else{
        $('#err_prog').html('');
        prog    = true;
        acaprog = $('#acaprog').val();
    }

    if($('#acasem').val() == ''){
        $('#err_sem').html('This field is required');
    }else{
        $('#err_sem').html('');
        sem    = true;
        acasem = $('#acasem').val();
    }

    /*if($('#acayear').val() == ''){
        $('#err_year').html('This field is required');
    }else{
        $('#err_year').html('');
        year    = true;
        acayear = $('#acayear').val();
		,
                acayear : acayear
    }*/
	//alert(acaprog)
    if(prog && sem && year){
		$('#studentDataTable').DataTable().clear().destroy();
        $('#prg_progress').show();
        $.ajax({
            url  : baseURL+'Teacher/studentMasterList',
            type : 'POST',
            data :{
                acaprog : acaprog,
                acasem  : acasem
            },
            success:function(data){
                $('#prg_progress').hide();
				$('#student_master_list').html(data);
                $('#mainCheckBox').prop('checked',false);
            },
			complete: function(data){
				$('#studentDataTable').DataTable({
					"ordering": true,
					"info": true,
					"paging": true,
					"lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
					"lengthChange": true,
					dom: '1B<"top"lf>rt<"bottom"ip><"clear">',
					buttons: [
						{
							extend: 'excelHtml5',
							title: 'Pre Admission List of <?php echo trim($prog[0]->title).' - '.trim($prog[0]->yearnm); ?>',
							text:'Excel',
							className: 'bg-success text-white mr-2',
							exportOptions: {
								columns: [0, 2, 3, 4, 5, 6]
							}
						},
						{
							extend: 'pdfHtml5',
							title: 'Pre Admission List of <?php echo trim($prog[0]->title).' - '.trim($prog[0]->yearnm); ?>',
							text: 'PDF',
							className: 'bg-success text-white mr-2',
							exportOptions: {
								columns: [0, 2, 3, 4, 5, 6]
							}
						},
						{
							extend: 'print',
							title: 'Pre Admission List of <?php echo trim($prog[0]->title).' - '.trim($prog[0]->yearnm); ?>',
							text: 'Print',
							className: 'bg-success text-white mr-2',
							exportOptions: {
								columns: [1, 2, 3, 4, 5, 6, 7]
							}
						}
					]
				});
				$('[name="studentDataTable_length"]').addClass('browser-default');
			}
        })
    }else{
        return false;
    }
}

function getSingleStudentMaster(stud){
    $.ajax({
        url  : baseURL+'Teacher/getSingleStudentMaster',
        type : 'POST',
        data : {
            stud    : stud,
        },
        success:function(resp){
            $('#row'+stud).html(resp);
        }
    });
}
function singleStudentChange(stud){
    var percent = $('#parce'+stud).val();
    var cgpa    = $('#cgpa'+stud).val();
    var sps_id  = $('#parce'+stud).attr('data-id');
    var status  = $('#status'+stud).val();
    var spc_id  = $('#status'+stud).attr('data-id');
    var sem_id  = $('#acasem').val();

    $('#row'+stud).html('<td></td><td></td><td></td><td></td><td></td><td><i class="loader"></td><td></td><td></td><td></td><td></td>');
	
    $.ajax({
        url  : baseURL+'Teacher/singleStudentChange',
        type : 'POST',
        data : {
            stud    : stud,
            percent : percent,
            cgpa    : cgpa,
            sps_id  : sps_id,
            status  : status,
            spc_id  : spc_id,
            sem_id  : sem_id
        },
        success:function(resp){
            if(resp){
                $('#success_msg').fadeIn('slow').html('Success');
                $('#success_msg').attr('class','text-success small');
                setTimeout(() => {
                    $('#success_msg').fadeOut('slow').html('');
                },3000);
                getSingleStudentMaster(stud);
            }else{
                $('#success_msg').html('Faild');
                $('#success_msg').attr('class','text-danger small');
                setTimeout(() => {
                    $('#success_msg').fadeOut('slow').html('');
                },3000);
                getSingleStudentMaster(stud);
            }
        }
    });
}

function certificateUpload(i,stud){
    var property = document.getElementById("myFile"+i).files[0];  
    var formData = new FormData();
    formData.append("file",property);
    formData.append("spc_id",i);
    $.ajax({
        url : baseURL+'Teacher/studentCertificatesUpload',
        method:'POST',
        data:formData,
        contentType:false,
        cache:false,
        processData:false,
		enctype:'multipart/form-data',
        success:function(data){
            singleStudentChange(stud);
        }
    });
}
function allStatusChange(){
    var status  = $('#all_status').val();
    var spc_ids = [];
    $.each($("input[class='checkbox']:checked"), function(){
        spc_ids.push($(this).val());
    });
    var spc_sl = spc_ids.join(",");
    if(spc_sl){
        $('#prg_progress').show();
        $.ajax({
            url  : baseURL+'Teacher/multipleStudentChange',
            type : 'POST',
            data : {
                status  : status,
                spc_id  : spc_sl,
            },
            success:function(resp){
                studentManagerTable();
            }
        });
    }
}
/********************************************************/
/*$('#stud-serach').click(function(){
    studentManagerTable();
});*/







function getProgramById(program){
    $.ajax({
        url  : baseURL+'Teacher/getProgramById',
        type : 'POST',
        data : {
            pid  : program 
        },
        success:function(resp){
            var prog = JSON.parse(resp);
            $('#prog_title').html(prog.title);
            $('#prog_title').attr('data-id',prog.id);
            $('#prog_title').attr('data-type',prog.feetype);
        }
    });
}

function searchStudentForPreAdmission(){
    var program = $('#program').val();
    if(program == ''){
        $.notify({icon:"add_alert",message:'Please select program'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}});
    }else{
        $('#prg_progress').show();
        getProgramById(program);
        $.ajax({
            url  : baseURL+'Teacher/getPreAdmissionStudentList',
            type : 'POST',
            data : {
                pid  : program 
            },
            success:function(resp){
                $('#prg_progress').hide();
                $('#pre_admission_list').html(resp);
            }
        });
    }
}

function afterApproveOrRejectPreAdmission(pid){
    var program = pid;
    getProgramById(program);
    $.ajax({
        url  : baseURL+'Teacher/getPreAdmissionStudentList',
        type : 'POST',
        data : {
            pid  : program 
        },
        success:function(resp){
            $('#prg_progress').hide();
            $('#mainCheckBox').prop('checked',false);
            $('#pre_admission_list').html(resp);
        }
    });
}

function allStudentPreAddmission(){
    var acayear      = $('#acayear').val();
    var prog_id      = $('#prog_title').attr('data-id');
    var prog_feetype = $('#prog_title').attr('data-type');

    var adm_ids = [];
    $.each($("input[class='checkbox']:checked"), function(){
        adm_ids.push($(this).attr('data-id'));
    }); 
    
    var adm_id = adm_ids.join(",");
    if(acayear != ''){
        if(adm_id != ''){
            swal({
                title: 'Are you sure?',
				text: "You want to change status to First Approval for selected student(s)",
				type: 'warning',
				showCancelButton: true,
				confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                confirmButtonText: 'Yes, Approve it!',
				buttonsStyling: false
            }).then(function(result){
                $('#prg_progress').show();
                if(result.value) {
                    $.ajax({
                        url : baseURL+'Teacher/allStudentPreAddmission',
                        type : 'POST',
                        data : {
                            acayear      : acayear,
                            prog_id      : prog_id,
                            prog_feetype : prog_feetype,
                            adm_id      : adm_id
                        },
                        success: function(resp){
                            $('#loading').hide();
                            if(resp){
                                $.notify({icon:"add_alert",message:'Successfuly approved'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}});
                                afterApproveOrRejectPreAdmission(prog_id);
                            }else{
                                $.notify({icon:"add_alert",message:'Not approved'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}});
                                afterApproveOrRejectPreAdmission(prog_id);
                            }
                        }
                    })
                }
            })
        }else{
            $.notify({icon:"add_alert",message:'No student has been selected'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}});
        }
    }else{
        $.notify({icon:"add_alert",message:'Academic Year not set'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}});
    }
}

function allStudentPreAddmissionReject(){
    var prog_id      = $('#prog_title').attr('data-id');
    var adm_ids = [];
    $.each($("input[class='checkbox']:checked"), function(){
        adm_ids.push($(this).attr('data-id'));
    }); 
    var adm_id = adm_ids.join(",");
    if(adm_id != ''){
        swal({
            title: 'Are you sure?',
			text: "You want to reject selected student(s)",
			type: 'warning',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: 'Yes, Approve it!',
			buttonsStyling: false
        }).then(function(result){
            $('#prg_progress').show();
            if(result.value) {
                $.ajax({
                    url : baseURL+'Teacher/allStudentPreAddmissionReject',
                    type : 'POST',
                    data : {
                        prog_id      : prog_id,
                        adm_id      : adm_id
                    },
                    success: function(resp){
                        if(resp){
                            $.notify({icon:"add_alert",message:'Successfuly reject'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}});
                            afterApproveOrRejectPreAdmission(prog_id);
                        }else{
                            $.notify({icon:"add_alert",message:'Not reject'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}});
                            afterApproveOrRejectPreAdmission(prog_id);
                        }
                    }
                })
            }
        })
    }else{
        $.notify({icon:"add_alert",message:'No student has been selected'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}}); 
    }
}

function searchStudentForAdmission(){
    var program = $('#acaprog').val();
    if(program == ''){
        $.notify({icon:"add_alert",message:'Please select program'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}});
    }else{
        $('#prg_progress').show();
        getProgramById(program);
        $.ajax({
            url  : baseURL+'Teacher/getAdmissionStudentList',
            type : 'POST',
            data : {
                pid  : program,
            },
            success:function(resp){
                $('#prg_progress').hide();
                $('#admission_list').html(resp);
                //console.log(resp);
            }
        });
    }
}

function afterApproveOrRejectAdmission(prog_id){
    var program = prog_id;
    $.ajax({
        url  : baseURL+'Teacher/getAdmissionStudentList',
        type : 'POST',
        data : {
            pid  : program,
        },
        success:function(resp){
            $('#prg_progress').hide();
            $('#admission_list').html(resp);
            $('#mainCheckBox').prop('checked',false);
        }
    });
}

function allStudentAddmission(){
    var program = $('#acaprog').val();
    var semester = $('#acasem').val();
    var feetype = $('#prog_title').attr('data-type');
    var adm_ids = [];
    var enrolles = [];
    var rolles = [];
    $.each($("input[class='checkbox']:checked"), function(){
        adm_ids.push($(this).attr('data-id'));
        enrolles.push($('#enroll_'+$(this).attr('data-id')).val());
        rolles.push($('#roll_'+$(this).attr('data-id')).val());
    }); 
    var adm_id = adm_ids.join(","); 
    var enrolle = enrolles.join(",");
    var rolle   = rolles.join(",");

    if(semester != ''){
        if(adm_id != ''){
            if(enrolle != ''){
                if(rolle != ''){
                    swal({
                        title: 'Are you sure?',
                        text: "You want to change status to Final Approval for selected student(s)",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        confirmButtonText: 'Yes, Approve it!',
                        buttonsStyling: false
                    }).then(function(result) {
                        $('#prg_progress').show();
                        if(result.value) {
                            $.ajax({
                                url : baseURL+'Teacher/allStudentAddmission',
                                type : 'POST',
                                data : {
                                    pid      : program,
                                    acp_ids  : adm_id,
                                    ftype    : feetype,
                                    semid    : semester,
                                    enroll  : enrolle,
                                    roll    : rolle
                                },
                                success: function(resp){
                                    if(resp == 0){
                                        $.notify({icon:"add_alert",message:'Something went worng.'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}});
                                        afterApproveOrRejectAdmission(program);
                                    }else if(resp == 2){
                                        $.notify({icon:"add_alert",message:'Courses are not added/not linked with semesters'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}});
                                        afterApproveOrRejectAdmission(program);
                                    }else{
                                        $.notify({icon:"add_alert",message:'The student(s) has been selected for final approval.'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}});
                                        afterApproveOrRejectAdmission(program);
                                    }
                                }
                            })
                        }
                    })
                }else{
                    $.notify({icon:"add_alert",message:'Fill all selected student rollment no.'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}}); 
                }
            }else{
                $.notify({icon:"add_alert",message:'Fill all selected student enrollment no.'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}}); 
            }
        }else{
            $.notify({icon:"add_alert",message:'No student has been selected'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}}); 
        }
    }else{
        $.notify({icon:"add_alert",message:'No semester has been selected'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}}); 
    }
    
}

function allStudentAddmissionReject(){
    var program = $('#acaprog').val();
    var adm_ids = [];
    $.each($("input[class='checkbox']:checked"), function(){
        adm_ids.push($(this).attr('data-id'));
    }); 
    var adm_id = adm_ids.join(",");  
    
    if(adm_id != ''){
		swal({
			title: 'Are you sure?',
			text: "You want to reject selected student(s)",
			type: 'warning',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: 'Yes, Approve it!',
			buttonsStyling: false
		}).then(function(result) {
			if(result.value) {
				$('#loading').show();
				$.ajax({
					url: baseURL+'Teacher/allStudentAddmissionReject',
					type: 'POST',
					data: {
                        prog_id : program,
                        acp_ids : adm_id
                    },
					success: function(resp){
                        if(resp){
                            $.notify({icon:"add_alert",message:'The student(s) has been rejected.'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}});
                            afterApproveOrRejectAdmission(program);
                        }else{
                            $.notify({icon:"add_alert",message:'Something went worng.'},{type:'warning',timer:3e3,placement:{from:'top',align:'right'}});
                            afterApproveOrRejectAdmission(program);
                        }
                    }
				})
			}
		});
    }else{

    }
}

