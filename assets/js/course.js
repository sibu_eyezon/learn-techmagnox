$(document).ready(function(){
	$('form#frmCourse').validate({
		errorPlacement: function(error, element) {
		  $(element).closest('.form-group').append(error);
		},
		rules: {
			semid: {
				required: true
			},
			title: {
				required: true,
				minlength: 2
			},
			type: {
				required: true
			},
			ccode: {
				required: true
			},
			category: {
				required: true
			},
			lecture: {
				required: true
			},
			tutorial: {
				required: true
			},
			practical: {
				required: true
			},
			sdate: {
				required: true
			},
			edate: {
				required: true
			},
			ccredit: {
				required: true
			}
		},
		messages: {
			semid: {
				required: 'Must select a semester.'
			},
			title: {
				required: 'Must enter course title.',
				minlength: 'The title must be minimum 2 characters.'
			},
			type: {
				required: 'Must select a type.'
			},
			ccode: {
				required: 'Must enter course code.'
			},
			category: {
				required: 'Must select on category.'
			},
			lecture: {
				required: 'Must select no. of lectures.'
			},
			tutorial: {
				required: 'Must select no. of tutorial.'
			},
			practical: {
				required: 'Must select no. of practical.'
			},
			sdate: {
				required: 'Must enter start date.'
			},
			edate: {
				required: 'Must enter end date.'
			},
			ccredit: {
				required: 'Must provide course credit.'
			}
		},
		submitHandler: function(form, e) {
			e.preventDefault();
			var cid = parseInt($('#cid').val());
			var prog = parseInt($('#prog_id').val());
			$('#loading').css('display', 'block');
			var frmData = new FormData($('#frmCourse')[0]);
			frmData.set('overview', CKEDITOR.instances['overview'].getData());
			frmData.set('importance', CKEDITOR.instances['importance'].getData());
			$.ajax({
				url: baseURL+'Teacher/cuProCourse',
				type: 'POST',
				data: frmData,
				cache : false,
				processData: false,
				contentType: false,
				enctype: 'multipart/form-data',
				async: false,
				success: (data)=>{
					$('#loading').fadeOut(1000);
					var obj = JSON.parse(data);
					$.notify({icon:"add_alert",message:obj['msg']},{type:obj['status'],timer:3e3,placement:{from:'top',align:'right'}})
					if(obj['status']=='success'){
						$('#frmCourse')[0].reset();
						$('#semid').val("").selectpicker('refresh');
						$('#crtype').val("").selectpicker('refresh');
						$('#lecture').val("").selectpicker('refresh');
						$('#tutorial').val("").selectpicker('refresh');
						$('#practical').val("").selectpicker('refresh');
						if(cid!=0){
							window.open(baseURL+'Teacher/courseDetails/?cid='+btoa(cid)+'&prog='+btoa(prog), '_self');
						}
					}
				},
				error: (errors)=>{
					console.log(errors);
				}
			});
		}
	});
});