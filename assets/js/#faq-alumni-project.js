/*==================================================================================*/

/*======================= function for all Select check ======================*/
function isAllChecked(){
    if($('#allCheck').is(":checked")){
        $('.checkBox').each(function(index){
            $(this).prop('checked',true);
            $('#changeButton').show();
        })
    }else{
        $('.checkBox').each(function(index){
            $(this).prop('checked',false);
            $('#changeButton').hide();
        })
    }
}

function buttonShowHide(){
   let number = $('[class="checkBox"]:checked').length; 
   if(number){
        $('#changeButton').show();
   }else{
        $('#changeButton').hide();
   }
}

function allCkeckBoxIsChecked(){
    let totalCheckboxes        = $('[class="checkBox"]').length;
    let totalCheckedCheckboxes = $('[class="checkBox"]:checked').length;
    if(totalCheckboxes === totalCheckedCheckboxes){
        $('#allCheck').prop("checked", true);
    }else{
        $('#allCheck').prop("checked", false);
    }
}

function isSingleChecked(i){
    let id = `checkBox${i}`
    if($(id).is(":checked")){
        buttonShowHide();
        allCkeckBoxIsChecked();
    }else{
        buttonShowHide();
        allCkeckBoxIsChecked();
    }
}

function multipleStatusChange(str){
    let page  = $(`#table_${str}`).attr('data-id');
    let rid   = [];
    let valid = [];
    $('input[class="checkBox"]:checked').each(function(){
        rid.push($(this).attr("data-id"));
        valid.push(this.value);
    });
    
    $.ajax({
        url  : baseURL + 'Operation/multiple_status_change',
        type : 'POST',
        data :{
            id : rid,
            yn_valid : valid,
            job : str
        },
        success : function(data){
            $('#changeButton').hide();
            let res = JSON.parse(data);
            if(str === 'faq'){
                getFaqList(page);
            }else if(str === 'project'){
                getProjectList(page);
            }else if(str === 'speak'){
                getAlumniSpeakList(page);
            }
            $.notify(
                {icon:"add_alert", message: res.message},
                {type:res.type, timer:2e2, placement:{from:'top',align:'right'}
            })
        }
    })
}

function singleStateChange(str, rid, valid){
    let page     = $(`#table_${str}`).attr('data-id');
    let id       = rid;
    let yn_valid = valid;
    $.ajax({
        url  : baseURL + 'Operation/single_status_change',
        type : 'POST',
        data :{
            id : id,
            yn_valid : yn_valid,
            job : str
        },
        success : function(data){
            let res = JSON.parse(data);
            if(str === 'faq'){
                getFaqList(page);
            }else if(str === 'project'){
                getProjectList(page);
            }else if(str === 'speak'){
                getAlumniSpeakList(page);
            }
            $.notify(
                {icon:"add_alert", message: res.message},
                {type:res.type, timer:2e2, placement:{from:'top',align:'right'}
            })
        }
    })
}


/*==================================================================================*/

/*======================= function to get url parameter value ======================*/
function getParamsValue(...args){
	let params   = window.location.search.substring(1);
    let paramArr = params.split('&');
    let qString  = {};
    for(let i=0; i<args.length; i++){
        for(let j=0; j<paramArr.length; j++){
            let paramIndex = paramArr[j].split('=');
            if(args[i] === paramIndex[0]){
                let data = paramArr[j].split(`${args[i]}=`)[1];
                qString[args[i]] = data;
            }
        }
    }
    return JSON.stringify(qString);
}

/*==========================================================================================*/

/*============================== Add Edit Modal Body =======================================*/
function addEditModal(pJob ,id='', pid='', pageNo=''){
    let param   = JSON.parse(getParamsValue('id'));
    let prog_id = (pid)?btoa(pid):(Object.keys(param).length)?param.id:btoa(0);
    let rid     = (id)?id:'';
    let page    = (pageNo)?pageNo:1;
    $.ajax({
        url: baseURL + 'Operation/get_add_edit_modal_body',
        type: 'POST',
        data:{
            prog_id : prog_id,
            rid     : rid,
            page    : page,
            job     : pJob
        },
        success: function(data){
            $('#operationModalCenter').modal('show');
            $('#modal_content').html(data);
        }
    })    
}

/*==========================================================================================*/

/*============================= functions of get faq =======================================*/
function getFaqList(page){
    let param = JSON.parse(getParamsValue('id'));
    $.ajax({
        url: baseURL + 'Operation/get_faq_list',
        type: 'POST',
        data:{
            prog_id : param.id,
            page    : page
        },
        success: function(data){
            $('#faq_list').html(data);
        }
    })
}

function add_edite_faq(){
    let question = $('#question').val();
    let answer   = CKEDITOR.instances['answer'].getData();
    let page     = $('#page').val();
    let rid      = $('#rid').val();
    let pid      = $('#pid').val();
    if(question){
        $('#error_question').html('');
    }else{
        $('#error_question').html('Thi field is required');
    }

    if(answer){
        $('#error_answer').html('');
    }else{
        $('#error_answer').html('This field is required');
    }
    if(question && answer){
        $.ajax({
            url : baseURL + 'Operation/perform_add_edite_operation',
            type : 'POST',
            data : {
                rid : rid, 
                pid : pid,
                question : question,
                answer : answer,
                job    : 'faq_add_edit'
            },
            success : function(data){
                $('#operationModalCenter').modal('hide');
                getFaqList(page);
                let res = JSON.parse(data);
                $.notify(
                    {icon:"add_alert", message: res.message},
                    {type:res.type, timer:2e2, placement:{from:'top',align:'right'}
                })
            }
        })
    }
}

/*==========================================================================================*/

/*============================= functions of get faq =======================================*/
function getProjectList(page){
    let param = JSON.parse(getParamsValue('id'));
    $.ajax({
        url: baseURL + 'Operation/get_project_list',
        type: 'POST',
        data:{
            prog_id : param.id,
            page    : page
        },
        success: function(data){
            $('#project_list').html(data);
        }
    })
}

function add_edite_project(){
    let title = $('#title').val();
    let details   = CKEDITOR.instances['details'].getData();
    let page     = $('#page').val();
    let rid      = $('#rid').val();
    let pid      = $('#pid').val();
    if(title){
        $('#error_title').html('');
    }else{
        $('#error_title').html('Thi field is required');
    }

    if(details){
        $('#error_details').html('');
    }else{
        $('#error_details').html('This field is required');
    }
    if(title && details){
        $.ajax({
            url : baseURL + 'Operation/perform_add_edite_operation',
            type : 'POST',
            data : {
                rid : rid, 
                pid : pid,
                title : title,
                details : details,
                job    : 'project_add_edit'
            },
            success : function(data){
                $('#operationModalCenter').modal('hide');
                getProjectList(page);
                let res = JSON.parse(data);
                $.notify(
                    {icon:"add_alert", message: res.message},
                    {type:res.type, timer:2e2, placement:{from:'top',align:'right'}
                })
            }
        })
    }
}

/*==========================================================================================*/

/*============================= functions of get speak =======================================*/
function getAlumniSpeakList(page){
    let param = JSON.parse(getParamsValue('id'));
    $.ajax({
        url: baseURL + 'Operation/get_alumni_speak_list',
        type: 'POST',
        data:{
            prog_id : param.id,
            page    : page
        },
        success: function(data){
            $('#speak_list').html(data);
        }
    })
}

function add_edite_alumni_speak(){
    let image         = ($('#profile_pic')[0].files[0]) ? $('#profile_pic')[0].files[0] : null;
    let name          = $('#name').val();
    let email         = $('#email').val();
    let phone         = $('#phone').val();
    let organization  = $('#organization').val();
    let position      = $('#position').val();
    let message       = CKEDITOR.instances['message'].getData();
    let preImage      = $('#preImage').val();
    let page          = $('#page').val();
    let rid           = $('#rid').val();
    let pid           = $('#pid').val();

    let isName         = false;
    let isEmail        = false;
    let isPhone        = false;
    let isPosition     = false;
    let isOrganization = false;
    let isMessage      = false;
    if(name){
        $('#error_name').html('');
        isName = true;
    }else{
        $('#error_name').html('Name is required');
    }

    if(position){
        $('#error_position').html('');
        isPosition = true;
    }else{
        $('#error_position').html('Position is required');
    }

    if(organization){
        $('#error_organization').html('');
        isOrganization = true;
    }else{
        $('#error_organization').html('Organization is required');
    }

    if(message){
        $('#error_message').html('');
        isMessage = true;
    }else{
        $('#error_message').html('Message is required');
    }

    if(phone){
        let phoneRegx = /^[4-9][0-9]{9}$/
        if(phoneRegx.test($('#phone').val())){
            $('#error_phone').html('');
            isPhone = true;
        }else{
            $('#error_phone').html('Phone number is invalid');
        }
    }else{
        $('#error_phone').html('Phone is required');
    }

    if(email){
        let emailRegx = /^[A-za-z0-9._$#]{1,}@[A-Za-z0-9]{3,}[.]{1}[A-Za-z0-9.]{1,}$/
        if(emailRegx.test($('#email').val())){
            $('#error_email').html('');
            isEmail      = true;
        }else{
            $('#error_email').html('Email is invalid');
        }
    }else{
        $('#error_email').html('Email is required');
    }

    if(isName && isEmail && isPhone && isPosition  && isOrganization && isMessage ){
        var formData = new FormData();
        formData.append('img',image);
        formData.append('name', name);
        formData.append('email', email);
        formData.append('phone', phone);
        formData.append('position', position);
        formData.append('organization', organization);
        formData.append('message', message);
        formData.append('preImage', preImage)
        formData.append('rid',rid);
        formData.append('pid',pid);
        formData.append('job','alumni_speak_add_edit');
        $.ajax({
            url: baseURL + 'Operation/perform_add_edite_operation',
            type: 'POST',
            data: formData ,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            enctype: 'multipart/form-data',
            success: function(data){
                $('#operationModalCenter').modal('hide');
                getAlumniSpeakList(page);
                let res = JSON.parse(data);
                $.notify(
                    {icon:"add_alert", message: res.message},
                    {type:res.type, timer:2e2, placement:{from:'top',align:'right'}
                })
            },
       });
    }
}
