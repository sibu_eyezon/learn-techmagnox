function deptByOrg(org){
    $('#dept').html('').selectpicker('refresh');
    $('#dept_loader').css('display', 'block');
    if(org == ''){
        $('#dept_loader').css('display', 'none');
        $('#dept').html('').selectpicker('refresh');
    }else{
        $('#dept_loader').css('display', 'block');
        $.ajax({
            url  : baseURL+'Admin/departmentByOrganization',
            type : 'POST',
            data :{
                org : org
            },
            success : function(data,status){
                $('#dept_loader').css('display', 'none');
                $('#dept').html(data).selectpicker('refresh');
            }
        });
    }
}

function progByDept(dept){
    $('#prog').html('').selectpicker('refresh');
    $('#prog_loader').css('display', 'block');
    if(dept == ''){
        $('#prog_loader').css('display', 'none');
        $('#prog').html('').selectpicker('refresh');
    }else{
        $('#prog_loader').css('display', 'block');
        $.ajax({
            url  : baseURL+'Admin/programByDepartment',
            type : 'POST',
            data :{
                dept : dept
            },
            success : function(data,status){
                $('#prog_loader').css('display', 'none');
                $('#prog').html(data).selectpicker('refresh');
            }
        });
    }
}

function changeOfOrganization(){
    var org = $('#org').val();
    deptByOrg(org);
}

function changeOfDepartment(){
    var dept = $('#dept').val();
    progByDept(dept);
}
