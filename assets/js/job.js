function isAllChecked(){
    if($('#allCheck').is(":checked")){
        $('.checkBox').each(function(index){
            $(this).prop('checked',true);
            buttonShowHide();
        })
    }else{
        $('.checkBox').each(function(index){
            $(this).prop('checked',false);
            buttonShowHide();
        })
    }
}

function buttonShowHide(){
   let number = $('[class="checkBox"]:checked').length; 
   if(number){
        $('#changeButton').show();
        $('#deleteButton').show();
   }else{
        $('#changeButton').hide();
        $('#deleteButton').hide();
   }
}

function allCkeckBoxIsChecked(){
    let totalCheckboxes        = $('[class="checkBox"]').length;
    let totalCheckedCheckboxes = $('[class="checkBox"]:checked').length;
    if(totalCheckboxes === totalCheckedCheckboxes){
        $('#allCheck').prop("checked", true);
    }else{
        $('#allCheck').prop("checked", false);
    }
}

function isSingleChecked(i){
    let id = `#checkBox${i}`
    if($(id).is(":checked")){
        buttonShowHide();
        allCkeckBoxIsChecked();
    }else{
        buttonShowHide();
        allCkeckBoxIsChecked();
    }
}

function backToJobList(){
    let page = parseInt(atob($('#current_page').attr('data-page')));
    getJobList(page);
}

function singleJObStatusChange(jid, jstatus){
    let page = $('#table_job').attr("data-page");
    let id   = atob(jid);
    let status = atob(jstatus);
    $.ajax({
        url: baseURL + 'Job/single_job_status_change',
        type: 'POST',
        data:{
            id : id,
            status : status
        },
        success: function(data){
            getJobList(page);
            let res = JSON.parse(data);
            $.notify(
                {icon:"add_alert", message: res.message},
                {type:res.type, timer:2e2, placement:{from:'top',align:'right'}
            })
        }
    })
}



function multipleStatusChange(){
    let page = $('#table_job').attr("data-page");
    let id   = [];
    let status = [];
    $('input[class="checkBox"]:checked').each(function(){
        id.push($(this).attr("data-id"));
        status.push(this.value);
    });

    $.ajax({
        url: baseURL + 'Job/multiple_job_status_change',
        type: 'POST',
        data:{
            id : id,
            status : status
        },
        success: function(data){
            getJobList(page);
            let res = JSON.parse(data);
            $.notify(
                {icon:"add_alert", message: res.message},
                {type:res.type, timer:2e2, placement:{from:'top',align:'right'}
            })
        }
    })
}


function singleJobDelete(jid){
    let page = $('#table_job').attr("data-page");
    let id   = atob(jid);

    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: baseURL + 'Job/single_job_delete',
                type: 'POST',
                data:{
                    id : id
                },
                success: function(data){
                    getJobList(page);
                    let res = JSON.parse(data);
                    $.notify(
                        {icon:"add_alert", message: res.message},
                        {type:res.type, timer:2e2, placement:{from:'top',align:'right'}
                    })
                }
            })
        }
    })
}

function multipleDelete(){
    let page = $('#table_job').attr("data-page");
    let id   = [];
    $('input[class="checkBox"]:checked').each(function(){
        id.push($(this).attr("data-id"));
    });
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: baseURL + 'Job/multiple_job_delete',
                type: 'POST',
                data:{
                    id : id,
                },
                success: function(data){
                    getJobList(page);
                    let res = JSON.parse(data);
                    $.notify(
                        {icon:"add_alert", message: res.message},
                        {type:res.type, timer:2e2, placement:{from:'top',align:'right'}
                    })
                }
            })
        }
    })
}   




function getJobList(page){
    $.ajax({
        url: baseURL + 'Job/get_job_list',
        type: 'POST',
        data:{
            page    : page
        },
        success: function(data){
            $('#page_title').html('Job Listing');
            $('#add_edit_job_btn').show();
            $('#changeButton').hide();
            $('#deleteButton').hide();
            $('#backButton').hide();
            $('#job_list').html(data);
        }
    })
}

function singleJobDetails(id){
    let page = $('#table_job').attr("data-page");
    $.ajax({
        url: baseURL + 'Job/view_job_details',
        type: 'POST',
        data:{
            id: atob(id),
            page: page
        },
        success: function(data){
            $('#page_title').html('Job Details');
            $('#add_edit_job_btn').hide();
            $('#backButton').show();
            $('#changeButton').hide();
            $('#deleteButton').hide();
            $('#job_list').html(data);
        }
    })
}

function addEditJob(id=null){
    let page = $('#table_job').attr("data-page");
    $.ajax({
        url: baseURL + 'Job/add_edit_job',
        type: 'POST',
        data:{
            id: id,
            page: page
        },
        success: function(data){
            $('#page_title').html('Add New Job');
            $('#add_edit_job_btn').hide();
            $('#backButton').show();
            $('#changeButton').hide();
            $('#deleteButton').hide();
            $('#job_list').html(data);
        }
    }) 
}

function addEditJobOperation(){
    let title        = $('#title').val();
    let type         = $('#type').val();
    let decription   = CKEDITOR.instances['decription'].getData();
    let designation  = $('#designation').val();
    let location     = $('#location').val();
    let skill        = $('#skill').val();
    let organization = $('#organization').val();
    let industry     = $('#industry').val();
    let year         = $('#year').val();
    let month        = $('#month').val();
    let degree       = $('#degree').val();
    let min_salary   = $('#min_salary').val();
    let max_salary   = $('#max_salary').val();
    let group        = $('#group').val();
    let link         = $('#link').val();
    let email        = $('#email').val();
    let start        = $('#start').val();
    let end          = $('#end').val();

    if(title){
        $('#e_title').html('');
    }else{
        $('#e_title').html('Job title is required');
    }

    if(location){
        $('#e_location').html('');
    }else{
        $('#e_location').html('Location is required');
    }

    if(skill.length){
        if(skill.length >= 3){
            $('#e_skill').html('');
        }else{
            $('#e_skill').html('There are minimux 3 skills required');
        }
    }else{
        $('#e_skill').html('Skill is required');
    }

    if(organization){
        $('#e_organization').html('');
    }else{
        $('#e_organization').html('Organization is required');
    }

    if(industry){
        $('#e_industry').html('');
    }else{
        $('#e_industry').html('Industry is required');
    }

    if(title && location && organization && industry && (skill.length >= 3)){
        $.ajax({
            url  : baseURL + 'Job/add_edit_job_operation',
            type : 'POST',
            data  : {
                    title        : title,
                    type         : type,
                    decription   : decription,
                    designation  : designation,
                    organization : organization,
                    industry     : industry,
                    location     : location,
                    skill        : skill,
                    year         : year,
                    month        : month,
                    degree       : degree,
                    min_salary   : min_salary,
                    max_salary   : max_salary,
                    group        : group,
                    link         : link,
                    email        : email,
                    start        : start,
                    end          : end
            },
            success : function(data,status){
                getJobList(1);
                let res = JSON.parse(data);
                $.notify(
                    {icon:"add_alert", message: res.message},
                    {type:res.type, timer:2e2, placement:{from:'top',align:'right'}
                })
            }
            
        })
    }
}