<?php
	$obj = array('sl'=>0, 'course_sl'=>0, 'title'=>"", 'tdate'=>date('Y-m-d'), 'marks'=>0, 'deadline'=>date('Y-m-d'));
	if($asgn_id == 0){
		$asgn[0] = (object)$obj;
	}else{
		$asgn = $this->Course_model->getCAssignmentByID($asgn_id);
	}
?>
<form action="<?php echo site_url('Certifyprogram/cuAssignments/'.$progtype); ?>" method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label class="text-dark">Assignment Title</label>
		<input type="text" name="title" id="title" class="form-control" value="<?php echo trim($asgn[0]->title); ?>" required>
	</div>

	<input type="hidden" name="prog_id" id="prog_id" value="<?php echo $prog_id; ?>">
	<input type="hidden" name="asgn_id" id="asgn_id" value="<?php echo $asgn_id; ?>">

	<div class="form-group">
		<label class="text-dark">Select Module</label>
		<select class="custom-select" name="section_id" id="section_id" required>
			<?php foreach ($sections as $section){
				echo '<option value="'.$section->id.'" '.(($asgn[0]->course_sl==$section->id)? 'selected':'').'>'.trim($section->title).'</option>';
			}
			?>
		</select>
	</div>
	<div class="form-group">
		<label for="amarks" class="text-dark mb-0">Marks</label>
		<input type="number" class="form-control" name="amarks" id="amarks" required min="1" max="100" number="true" value="<?php echo $asgn[0]->marks; ?>">
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label for="sdate" class="text-dark">Start Date</label>
				<input type="date" class="form-control" name="sdate" id="sdate" required value="<?php echo $asgn[0]->tdate; ?>">
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label for="ddate" class="text-dark">Deadline</label>
				<input type="date" class="form-control" name="ddate" id="ddate" required="true" value="<?php echo $asgn[0]->deadline; ?>">
			</div>
		</div>
	</div>
	<div class="text-right">
		<button class = "btn btn-success" type="submit" name="button">Save</button>
	</div>
</form>
<script>
$(document).ready(()=>{
	toggleFileTypes("<?php echo $asgnf[0]->type; ?>")
});
$("#lfiles").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

$('input[name="ltype"]').on('click',(e)=>{
	toggleFileTypes($('input[name="ltype"]:checked').val());
});
function toggleFileTypes(value)
{
	if(value=='fl'){
		$('#sh_file').show();
		$('#sh_lk').hide();
		$('#sh_yt').hide();
	}else if(value=='lk'){
		$('#sh_file').hide();
		$('#sh_lk').show();
		$('#sh_yt').hide();
	}else if(value=='yt'){
		$('#sh_file').hide();
		$('#sh_lk').hide();
		$('#sh_yt').show();
	}
}
</script>