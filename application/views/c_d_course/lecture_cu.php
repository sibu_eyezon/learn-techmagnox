<?php
	$obj = array('sl'=>0, 'course_sl'=>0, 'title'=>"", 'file_name'=>"", 'file_type'=>"", 'link'=>"");
	if($lec_id == 0){
		$lc[0] = $obj;
	}else{
		$lc = $this->Course_model->getCLectureByID($lec_id);
	}
?>
<form action="<?php echo site_url('Certifyprogram/cuLectures/'.$progtype); ?>" method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label class="text-dark">Lecture Title</label>
		<input type="text" name="title" id="title" class="form-control" value="<?php echo trim($lc[0]['title']); ?>" required>
	</div>

	<input type="hidden" name="prog_id" id="prog_id" value="<?php echo $prog_id; ?>">
	<input type="hidden" name="lesson_id" id="lesson_id" value="<?php echo $lec_id; ?>">

	<div class="form-group">
		<label class="text-dark">Select Module</label>
		<select class="custom-select" name="section_id" id="section_id" required>
			<?php foreach ($sections as $section){
				echo '<option value="'.$section->id.'" '.(($lc[0]['course_sl']==$section->id)? 'selected':'').'>'.trim($section->title).'</option>';
			}
			?>
		</select>
	</div>
	<div class="form-group">
		<div class="checkbox-radios">
			<div class="form-check form-check-inline">
			  <label class="form-check-label text-dark">
				<input class="form-check-input" type="radio" value="fl" name="ltype" id="fl"  required="true" <?php if($lc[0]['file_type']=='fl'){ echo 'checked'; } ?>> File*
				<span class="form-check-sign">
				  <span class="check"></span>
				</span>
			  </label>
			</div>
			<div class="form-check form-check-inline">
			  <label class="form-check-label text-dark">
				<input class="form-check-input" type="radio" value="yt" name="ltype" id="yt"  required="true" <?php if($lc[0]['file_type']=='yt'){ echo 'checked'; } ?>> Youtube*
				<span class="form-check-sign">
				  <span class="check"></span>
				</span>
			  </label>
			</div>
			<div id="type_error"></div>
		</div>
	</div>
	<div class="form-group mb-4" id="sh_file" style="display:none">
		<code>PDF/Word/Excel</code>
		<div class="custom-file">
			<input type="file" class="custom-file-input" name="lfiles" id="lfiles" accept="*.pdf, *.doc, *.docs, *.xls, *.xlsx">
			<label class="custom-file-label" for="lfiles">Choose program brochure</label>
		</div>
	</div>
	<div class="form group" id="sh_lkyt" style="display:none">
		<textarea class="form-control w-100" cols="80" rows="5" name="llink" id="llink" placeholder="Copy-Paste your youtube shared link here[Click share button of your youtube video and copy the url like->https://youtu.be/evknSAkUIvs]"><?php echo trim($lc[0]['link']); ?></textarea>
	</div>
	<div class="text-right">
		<button class = "btn btn-success" type="submit" name="button">Save</button>
	</div>
</form>
<script>
$(document).ready(()=>{
	toggleFileTypes("<?php echo $lc[0]['file_type']; ?>")
});
$("#lfiles").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

$('input[name="ltype"]').on('click',(e)=>{
	toggleFileTypes($('input[name="ltype"]:checked').val());
});
function toggleFileTypes(value)
{
	if(value=='fl'){
		$('#sh_file').show();
		$('#sh_lkyt').hide();
	}else if(value=='yt'){
		$('#sh_file').hide();
		$('#sh_lkyt').show();
	}
}
</script>