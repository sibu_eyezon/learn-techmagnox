<?php
	$obj = array('sl'=>0, 'course_sl'=>0, 'title'=>"");
	$obj1 = array('res_sl'=>0, 'type'=>"", 'linkfile'=>"");
	if($res_id == 0){
		$rc[0] = $obj;
		$rcf[0] = (object)$obj1;
	}else{
		$rc = $this->Course_model->getCResourceByID($res_id);
		$rcf = $this->Course_model->getCResourceFileByRID($res_id);
		if(empty($rcf)){
			$rcf[0] = (object)$obj1;
		}
	}
?>
<form action="<?php echo site_url('Certifyprogram/cuResources/'.$progtype); ?>" method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label class="text-dark">Resource Title</label>
		<input type="text" name="title" id="title" class="form-control" value="<?php echo trim($rc[0]['title']); ?>" required>
	</div>

	<input type="hidden" name="prog_id" id="prog_id" value="<?php echo $prog_id; ?>">
	<input type="hidden" name="res_id" id="res_id" value="<?php echo $res_id; ?>">

	<div class="form-group">
		<label class="text-dark">Select Module</label>
		<select class="custom-select" name="section_id" id="section_id" required>
			<?php foreach ($sections as $section){
				echo '<option value="'.$section->id.'" '.(($rc[0]['course_sl']==$section->id)? 'selected':'').'>'.trim($section->title).'</option>';
			}
			?>
		</select>
	</div>
	<div class="form-group">
		<div class="checkbox-radios">
			<div class="form-check form-check-inline">
			  <label class="form-check-label text-dark">
				<input class="form-check-input" type="radio" value="fl" name="ltype" id="fl"  required="true" <?php if($rcf[0]->type=='fl'){ echo 'checked'; } ?>> File/Video*
				<span class="form-check-sign">
				  <span class="check"></span>
				</span>
			  </label>
			</div>
			<div class="form-check form-check-inline">
			  <label class="form-check-label text-dark">
				<input class="form-check-input" type="radio" value="yt" name="ltype" id="yt"  required="true" <?php if($rcf[0]->type=='yt'){ echo 'checked'; } ?>> Youtube*
				<span class="form-check-sign">
				  <span class="check"></span>
				</span>
			  </label>
			</div>
			<div class="form-check form-check-inline">
			  <label class="form-check-label text-dark">
				<input class="form-check-input" type="radio" value="lk" name="ltype" id="lk"  required="true" <?php if($rcf[0]->type=='lk'){ echo 'checked'; } ?>> Link*
				<span class="form-check-sign">
				  <span class="check"></span>
				</span>
			  </label>
			</div>
			<div id="type_error"></div>
		</div>
	</div>
	<div class="form-group mb-4" id="sh_file" style="display:none">
		<code>PDF/Word/Excel</code>
		<div class="custom-file">
			<input type="file" class="custom-file-input" name="lfiles" id="lfiles" accept="*.pdf, *.doc, *.docs, *.xls, *.xlsx">
			<label class="custom-file-label" for="lfiles">Choose program brochure</label>
		</div>
	</div>
	<div class="form group" id="sh_yt" style="display:none">
		<textarea class="form-control w-100" cols="80" rows="5" name="yt_link" id="yt_link" placeholder="Copy-Paste your youtube shared link here[Click share button of your youtube video and copy the url like->https://youtu.be/evknSAkUIvs]"><?php echo trim($rcf[0]->linkfile); ?></textarea>
	</div>
	<div class="form group" id="sh_lk" style="display:none">
		<textarea class="form-control w-100" cols="80" rows="5" name="lk_link" id="lk_link" placeholder="Copy-Paste your  link here"><?php echo trim($rcf[0]->linkfile); ?></textarea>
	</div>
	<div class="text-right">
		<button class = "btn btn-success" type="submit" name="button">Save</button>
	</div>
</form>
<script>
$(document).ready(()=>{
	toggleFileTypes("<?php echo $rcf[0]->type; ?>")
});
$("#lfiles").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

$('input[name="ltype"]').on('click',(e)=>{
	toggleFileTypes($('input[name="ltype"]:checked').val());
});
function toggleFileTypes(value)
{
	if(value=='fl'){
		$('#sh_file').show();
		$('#sh_lk').hide();
		$('#sh_yt').hide();
	}else if(value=='lk'){
		$('#sh_file').hide();
		$('#sh_lk').show();
		$('#sh_yt').hide();
	}else if(value=='yt'){
		$('#sh_file').hide();
		$('#sh_lk').hide();
		$('#sh_yt').show();
	}
}
</script>