<style>
@media (min-width: 768px) {
    .h-md-100 { height: 100vh; }
}
.left {
	background: #fff;
	background-image: url(<?php echo base_url('assets/img/'.(($userType=='teacher')? 'teacherreg':'studentreg').'.jpg'); ?>);
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;
}
.side_intro .card-title{
	font-size: 1.8rem;
    font-weight: 600;
    color: #fff;
    font-family: inherit;
}
.side_intro .card-title.italic {
	font-family: 'Arizonia', cursive !important;
}
.side_intro h6,.side_intro h6 a{
	font-weight: 500;
    color: #fff;
	font-size: .85rem
}
h6 {
	font-size: .85rem !important;
}
</style>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/common.js'); ?>"></script>
<div class="content mt-0 p-0" style="background:transparent !important;">
<div class="container-fluid">
	<div class="row d-md-flex h-md-100 align-items-center">
		<div class="col-md-6 p-0 bg-indigo h-md-100 mx-auto">
			<div class="d-md-flex align-items-center h-100 p-5 justify-content-center">
				<div class="card" style="box-shadow:none !important;">
					<div class="card-header text-center">
						<figure class="figure">
						  <img src="<?php echo base_url(); ?>assets/img/logo.png" class="figure-img img-fluid" width="150">
						  <figcaption class="figure-caption">Register now for Learning, Jobs and more ..</figcaption>
						</figure>
					</div>
					<div class="card-body">
						<?php
							if($this->session->flashdata('error')!=NULL){
								echo '<div class="alert alert-warning">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										  <i class="material-icons">close</i>
										</button>
										<span>
										  <b> Warning - </b> '.$this->session->flashdata('error').'</span>
									  </div>';
							}
							if($this->session->flashdata('success')!=NULL){
								echo '<div class="alert alert-success">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										  <i class="material-icons">close</i>
										</button>
										<span>
										  <b> Success - </b> '.$this->session->flashdata('success').'</span>
									  </div>';
							}
						  ?>
						  <form class="form" method="" action="#" id="register">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="fname">Firstname <span class="text-danger">*</span></label>
										<input type="text" name="fname" id="fname" class="form-control"/>
										<input type="hidden" name="user_type" id="user_type" value="<?php echo $userType; ?>"/>
									  </div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label for="lname">Lastname <span class="text-danger">*</span></label>
										<input type="text" name="lname" id="lname" class="form-control"/>
									  </div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="email">Email <span class="text-danger">*</span></label>
										<input type="text" name="email" id="email" class="form-control"/>
									  </div>	
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label for="phone">Phone <span class="text-danger">*</span></label>
										<input type="text" name="phone" id="phone" class="form-control"/>
									  </div>
								</div>
							</div>
							<?php if($userType=='teacher'){ ?>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="org">Organisation <span class="text-danger">*</span></label>
										<input type="text" name="org" id="org" class="form-control"/>
									  </div>
									  
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label for="designation">Designation <span class="text-danger">*</span></label>
										<input type="text" name="designation" id="designation" class="form-control"/>
									  </div>
									  
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label for="weblink">Website</label>
										<input type="text" name="weblink" id="weblink" class="form-control"/>
									  </div>
								</div>
							</div>
							<?php } ?>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="passwd">Password <span class="text-danger">*</span></label>
										<input type="password" name="passwd" id="passwd" class="form-control" autocomplete="off"/>
									  </div>
									  
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label for="repasswd">Retype Password <span class="text-danger">*</span></label>
										<input type="password" name="repasswd" id="repasswd" class="form-control" autocomplete="off"/>
									  </div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<div class="form-group g-recaptcha" data-sitekey="6LcAodEZAAAAAD_eDVKc-mC1XluCkg4P1kMKzIWy"></div>
									  </div>
									  <div class="form-group">
										<input type="reset" style="display:none"/>
										<button type="submit" id="btn_login" class="btn btn-info btn-block">Register</button>
									  </div>
								</div>
							</div>
							<div class="form-group">
								<h6>Already have an account? <a href="<?php echo base_url(); ?>">Login here</a></h6>
								<h6>Visit Company Website <a href="https://techmagnox.com/" target="_blank">Click here</a></h6>
								<h6>Enhance your Skills and Career <a href="https://billionskills.com/" target="_blank">Click here</a></h6>
							  </div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<!-- Second Half 
		<div class="col-md-6 p-0 bg-white h-md-100 loginarea">
			<div class="d-md-flex align-items-center h-md-100 p-5 justify-content-center left">
				<div class="card side_intro" style="background:transparent !important; box-shadow:none !important;">
					<div class="card-header text-left">
						<h2 class="card-title">One of the best Learning Platform</h2>
						<h2 class="card-title italic">Continue Learning Anything, Anywhere, Anytime</h2>
					</div>
					<div class="card-body">
						<h6>Don't have a student account? <a href="<?php //echo base_url('register/student'); ?>">Register here</a></h6>
						<h6>Want to Enroll in a Program? <a href="https://billionskills.com/home/explore_programs"  target="_blank">Click here</a></h6>
						<h6>Are you an Instructor? <a href="<?php //echo base_url('register/teacher'); ?>">Click here</a></h6>
						<h6>Visit Company Website <a href="https://techmagnox.com/" target="_blank">Click here</a></h6>
						<h6>Enhance your Skills and Career <a href="https://billionskills.com/" target="_blank">Click here</a></h6>
						
					</div>
				</div>
			</div>
		</div>-->
	</div>
</div>