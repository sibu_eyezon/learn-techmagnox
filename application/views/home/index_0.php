<link rel="preconnect" href="https://fonts.gstatic.com">

<style>
.home_intro {
	background-color: #000;
	background-image:url('<?php echo base_url('assets/img/loginimage.jpg'); ?>');
	background-repeat: no-repeat;
	background-position: center;
	background-size: 100% 100%;
}
.side_intro .card-title{
	font-size: 1.8rem;
    font-weight: 600;
    color: #fff;
    font-family: inherit;
}
.side_intro .card-title.italic {
	font-family: 'Arizonia', cursive !important;
}
.side_intro h6,.side_intro h6 a{
	font-weight: 500;
    color: #fff;
}
</style>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/common.js'); ?>"></script>
<div class="content mt-0 p-0" style="background:transparent !important;">
	<div class="container-fluid">
		<div class="row bg-white" style="">
			<div class="col-md-6 bg-white" style="display:grid; place-items:center; padding:5rem;">
				<div class="card" style="background:transparent !important; box-shadow:none !important;">
					<div class="card-header text-center">
						<figure class="figure">
						  <img src="<?php echo base_url(); ?>assets/img/logo.png" class="figure-img img-fluid" width="150">
						  <figcaption class="figure-caption">Continue your learning</figcaption>
						</figure>
					</div>
					<div class="card-body">
						<?php
							if($this->session->flashdata('error')!=NULL){
								echo '<div class="alert alert-warning">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										  <i class="material-icons">close</i>
										</button>
										<span>
										  <b> Warning - </b> '.$this->session->flashdata('error').'</span>
									  </div>';
							}
							if($this->session->flashdata('success')!=NULL){
								echo '<div class="alert alert-success">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										  <i class="material-icons">close</i>
										</button>
										<span>
										  <b> Success - </b> '.$this->session->flashdata('success').'</span>
									  </div>';
							}
						  ?>
						  <form class="form" id="frmLogin">
							  <div class="form-group">
								<label for="email" class="bmd-label-floating">Email Address</label>
								<input type="text" name="username" id="username" class="form-control"/>
							  </div>
							  <div class="form-group">
								<label for="passwd" class="bmd-label-floating">Password</label>
								<input type="password" name="password" id="password" class="form-control" autocomplete="off"/>
							  </div>
							  <div class="form-group">
								<div class="form-group g-recaptcha" data-sitekey="6LcAodEZAAAAAD_eDVKc-mC1XluCkg4P1kMKzIWy"></div>
							  </div>
							  <div class="form-group">
								<input type="reset" style="display:none"/>
								<!--<button type="submit" class="home-btn pull-left" id="btn_login">Login</button>-->
								<input type="reset" style="visibility:hidden;"/>
								<a href="javascript:;" class="pull-right" onClick="SendOTP();" style="font-size:14px;">Forgot Password ?</a>
							  </div>
							  <div class="form-group">
								<button type="submit" id="btn_login" class="btn btn-info btn-block btn-md">Login</button>
							  </div>
							  <!--<h6 class="text-center">Or Login with</h6>
							  <div class="row">
								<div class="col-sm-6">
									<button type="button" class="btn btn-block bg-danger"><i class="fa fa-google mr-2"></i>   Google</button>
								</div>
								<div class="col-sm-6">
									<button type="button" class="btn btn-block bg-primary"><i class="fa fa-facebook-f mr-2"></i>   Facebook</button>
								</div>
							  </div>-->
							  <div class="form-group">
								<h6 class="text-primary">Don't have an account? <a href="<?php echo base_url('register/student'); ?>">Register here</a></h6>
								<h6 class="text-dark">Are you an Instructor? <a href="<?php echo base_url('register/teacher'); ?>">Click here</a></h6>
							  </div>
						  </form>
					</div>
				</div>
			</div>
			<div class="col-md-6 d-none d-sm-block d-sm-none d-md-block home_intro">
				<div class="h-100" style="display:grid; place-items:center;">
					<div class="card side_intro" style="background:transparent !important; box-shadow:none !important;">
						<div class="card-header text-left">
							<h2 class="card-title">One of the best Learning Platform</h2>
							<h2 class="card-title italic">Continue Learning Anything, Anywhere, Anytime</h2>
						</div>
						<div class="card-body">
							<h6>Don't have a student account? <a href="<?php echo base_url('register/student'); ?>">Register here</a></h6>
							<h6>Want to Enroll in a Program? <a href="<?php echo base_url('programs'); ?>">Click here</a></h6>
							<h6>Are you an Instructor? <a href="<?php echo base_url('register/teacher'); ?>">Click here</a></h6>
							<h6>Visit Company Website <a href="https://www.billionskills.com/" target="_blank">Click here</a></h6>
							<h6>Enhance your Skills and Career <a href="https://www.billionskills.com/" target="_blank">Click here</a></h6>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
