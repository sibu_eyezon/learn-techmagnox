<?php
	if(count($program)>0){
		$i=1;
		echo '<div class="row">';
		foreach($program as $prow){
				$curdate = strtotime(date('Y-m-d'));
				$ldt = strtotime(date('Y-m-d',strtotime($prow->aend_date)));
				$sdt = strtotime(date('Y-m-d',strtotime($prow->astart_date)));
				$fee = trim($prow->feetype);
				$plevel = $prow->prog_level;
			echo '<div class="col-lg-3 col-md-3 mb-3">
					<div class="card card-product shadow">
					  <a href="'.base_url().'programDetails/?id='.base64_encode($prow->id).'">
					  <div class="card-header card-header-image">
						
						  <img class="img" src="'.base_url().'assets/img/banner/'.trim($prow->banner).'" onerror="this.src=`'.base_url().'assets/img/sample.jpg`">
						
					  </div>
					  </a>
					  <div class="card-body" style="height:auto;">
						<h5 class="card-title font-weight-bold"><a href="'.base_url().'programDetails/?id='.base64_encode($prow->id).'">'.trim($prow->title).'</a></h5>
						<div class="card-description">';
						  if(!empty(${'ins_'.$i}))
						  {
							  echo trim(${'ins_'.$i}[0]->title).', ';
						  }
						  $dur = intval(trim($prow->duration));
						  echo '<h6 class="text-center">Duration: '.$dur.' '.trim($prow->dtype).(($dur==1)? '':'s').'	;	Total hrs: '.trim($prow->prog_hrs).' Hrs</h6>
						  <h6 class="text-center">'.(($fee=='Paid')? 'Rs '.$prow->total_fee : $fee).'</h6>
						  <h6 class="text-center">Start From: '.date('jS M Y',strtotime($prow->start_date)).' To '.date('jS M Y',strtotime($prow->end_date)).'</h6>
						  <h6 class="text-center text-danger">Deadline: '.(($curdate<=$ldt)? date('jS M Y',strtotime($prow->aend_date)) : 'Expired').'</h6>
						</div>
					  </div>
					  <div class="card-footer">
						<h6 class="font-weight-bold">'.trim($prow->category).' <span class="pull-right">'.(($plevel=='3')? 'Advanced':(($plevel=='2')? 'Beginner' : 'Intermediate')).'</span></h6>
					  </div>
					</div>
				  </div>';
		$i++;
		}
		echo '</div>';
	}
?>