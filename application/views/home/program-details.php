<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css" integrity="sha512-H9jrZiiopUdsLpg94A333EfumgUBpO9MdbxStdeITo+KEIMaNfHNvwyjjDJb+ERPaRS6DpyRlKbvPUasNItRyw==" crossorigin="anonymous" />
<style>
#course_code::placeholder {
	color: white;
}
iframe: {
	width: 100% !important;
}
.heading {
	padding: 20px 15px;
	background-color: #11075f;
	background-image: linear-gradient(135deg, #0aceab, #efa915, #fbc52f, #1db2f5);
	color: #fff;
}
.video_btn {
	position: relative;
	width: 100%;
	height:100%;
	display:grid;
	place-items:center;
}
.play_icon {
	font-size: 5rem;
    color: #fff;
    transition: all 0.5s;
}
.play_icon:hover {
	color: red;
    font-size: 6rem;
}
.banner{
	background-color: #fff;
	background-position: cover;
	background-repeat: no-repeat;
	background-size: 100% 100%;
	height: 60vmin;
}
.intro{
	width: 100%;
	padding: 1rem;
	background-color: #fff;
}
.list-column {
	-webkit-columns: 2;
	-moz-columns: 2;
	columns: 2;
	padding-left: 0;
    list-style: square;
}
.list-column li {
	list-style-position: inside;
	-webkit-column-break-inside: avoid;
	page-break-inside: avoid;
	break-inside: avoid;
	font-size: 14px;
    font-weight: 500;
}
.intro-contact{
    font-size: 14px;
    text-align: center;
	-moz-text-align:center;
	-ms-text-align:center;
	-webkit-text-align:center;
	-os-text-align:center;
}
.prog-content{
	width: 100%;
}
.prog-side{
	position: sticky;
	top: 0;
	background-color: #fff; /* Black */
	overflow-x: hidden;
	height: 100%;
}
.card-box{
	position: relative;
	right: -90px;
	border-bottom: 6px solid skyblue;
}
.card-left-img{
	position: absolute;
	top: 50px;
	left: -100px;
	box-shadow: 1px 1px 1px 1px #cccccc;
}
.card-box-body{
	margin-left:70px;
	padding: 20px;
}
.prog-section{
	margin-bottom: 1.5rem;
}
</style>
<?php
	//banner img setting
	$src = '';
	$banner = trim($prog[0]->banner);
	if($banner!=""){
		if(file_exists('./assets/img/banner/'.$banner)){
			$src = 'assets/img/banner/'.$banner;
		}else{
			$src = 'assets/img/sample.jpg';
		}
	}else{
		$src = 'assets/img/sample.jpg';
	}
	$intro = trim($prog[0]->intro_video_link);
	//Admission deadline
	$curdate = strtotime(date('d-m-Y'));
	$ldt = strtotime(date('d-m-Y',strtotime($prog[0]->aend_date)));
	$sdt = strtotime(date('d-m-Y',strtotime($prog[0]->astart_date)));
	//Fee Details
	$fee = trim($prog[0]->feetype);
	$amt = (int)$prog[0]->total_fee;
	$dis = (int)($prog[0]->discount);
	if($dis!=0){
		$famt = floatval($amt*(1-(floatval($dis/100))));
	}else{
		$famt = 0;
	}
	$cert = trim($prog[0]->certificate_sample);
	if($cert!=""){
		if(file_exists('./uploads/programs/'.$cert)){
			$csrc = 'uploads/programs/'.$cert;
		}else{
			$csrc = 'uploads/programs/certificate20.jpg';
		}
	}else{
		$csrc = 'uploads/programs/certificate20.jpg';
	}
?>
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6 col-md-6 intro">
				<h2 class="font-weight-bold"><?php echo trim($prog[0]->title);?></h2>
				<h5 class="font-weight-bold"><span style="color:#f69508"><?php echo ($prog[0]->start_date)? date('jS M Y',strtotime($prog[0]->start_date)): '';?> to <?php echo ($prog[0]->end_date)? date('jS M Y',strtotime($prog[0]->end_date)): '';?>,</span><span style="color:#206992"> Instructor Led Training Program (Online)</span></h5>
				<ul class="list-column">
					<li><?php echo "Duration {$prog[0]->duration} {$prog[0]->dtype}";?></li>
					<li><?php echo "{$prog[0]->prog_hrs} Hours";?></li>
					<li><?php echo (!empty($procourse))? count($procourse) : '';?> Courses</li>
					<li><?php echo (!empty($skills))? count($skills):'';?> Skills and Tools</li>
					<li>Starting From <?php echo date('jS F Y',strtotime($prog[0]->start_date)); ?></li>
					<li>Live Industry Project</li>
				</ul>
				 <?php
					if(!empty($why_learn)):
						echo '<h4 class="font-weight-bold">Why '.trim($prog[0]->title).' at Magnox ?</h4>';
						echo '<ul class="list-column">';
						foreach($why_learn as $wl){
							echo '<li>'.trim($wl->txt_benefit).'</li>';
						}
						echo '</ul>'; 
					endif; 
				?>
				<h6 class="text-center">
				<p class="font-weight-bold mb-0" style="color:#206992">Program Fees : <?php echo (($fee=='Paid')? (($famt==0)? 'Rs '.$amt : 'Rs. strike>'.$amt.'</strike> Rs. '.$famt) : $fee);?></p>
				<?php
					if($sdt!=19800 || $ldt!=19800){
						if($curdate>=$sdt && $curdate<=$ldt){
							echo '<a href="'.base_url('programAdmission/?id='.base64_encode($prog[0]->id)).'" class="btn btn-warning btn-sm">Apply Now</a>';
						}
					}
					if($prog[0]->program_brochure!=null){
						if(file_exists('./uploads/programs/'.$prog[0]->program_brochure)){
							echo '<a href="'.base_url('uploads/programs/'.$prog[0]->program_brochure).'" class="btn btn-info btn-sm"><i class="fa fa-download"></i> Brochure</a>';
						}
					}
				?>
				<p class="font-weight-bold" style="color:#206992">Deadline : <?php echo ($prog[0]->aend_date)? date('jS F Y',strtotime($prog[0]->aend_date)): '';?></p>
				</h6>
			</div>
			<div class="col-lg-6 col-md-6 intro">
				<div>
				<?php
					echo '<img src="'.base_url().$src.'" class="img-responsive w-100"/>';
					if($intro!=""){
						echo '<div class="video_btn">
							<a data-fancybox href="'.$intro.'"> <i class="material-icons play_icon">play_circle_filled</i> </a>
						</div>';
					}
				?>
				</div>
				<p class="font-weight-bold intro-contact"><span style="color:#206992;">Talk With Our Executives : </span><span style="color:#000;">6292259475, 6292259474, 6292259479</span></p>
			</div>
		</div>
		<div class="row mt-3 bg-white py-4" id="main-content">
			<div class="col-md-9 prog-content">
				<div class="prog-section">
					<h2 class="font-weight-bold">Overview</h2>
					<div class="text-justify"><?php echo trim($prog[0]->overview);?></div>
				</div>
				<?php
				if(!empty($why_learn)){
					echo '<div class="prog-section">';
					echo '<h2 class="font-weight-bold">Why '.trim($prog[0]->title).' at Magnox ?</h2>';
					echo '<ul list-style="square">';
					foreach($why_learn as $wl){
					echo '<li><span class="font-weight-bold" style="color:#206992">'.trim($wl->txt_benefit).'</span><p>
								'.((($wl->txt_benefit_dtls)? $wl->txt_benefit_dtls : '')).'
							</p></li>';
					}
					echo '</ul></div>';
				}
				?>
				<div class="prog-section">
				<?php
					if(!empty($skills)): 
						echo '<h2 class="font-weight-bold">Skills Covered</h2>';
						foreach($skills as $data): 
				?>
				<img src="<?php echo base_url("assets/img/skills/{$data->slogo}");?>" width="100" height="100" class="mr-3">
				<?php
						endforeach;
					endif;
				?>
				</div>
				<div class="prog-section">
				 <h2 class="font-weight-bold">Advisors and Instructors</h2>
				 <div class="p-3 m-auto carousel slide" id="carouselExampleIndicators" data-ride="carousel" style="background-color: aqua;">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <?php
                            if(!empty($pprof)){
                                $fc = 1;
                                foreach($pprof as $row){
                                    echo '
                                            <div class="carousel-item '.(($fc === 1)? 'active' : '').'">
                                                <div class="card card-box p-5">
                                                    <div class="card-body card-box-body text-left">
                                                        <h3 class="card-title font-weight-bold">'.(($row->name)? $row->name : '').'</h3>
                                                        <p class="card-title font-weight-bold">
                                                            '.(($row->designation)? $row->designation : '').' of '.(($row->organization)? $row->organization : '').'
                                                        </p>
                                                        <p>
                                                            '.(($row->about_me)? $row->about_me : '').'
                                                        </p>
                                                    </div>
                                    ';
                                    echo'
										<div class="card-left-img">
											<img src="'.base_url('assets/img/'.$row->photo_sm).'" onerror="this.src=`'.base_url().'assets/img/default-avatar.png`" style="width:200px;">
										</div>
									';  
                                    echo '
                                                </div>
                                            </div>                                    
                                    ';
                                    $fc++;
                                }
                            }
                        ?>
                    </div>
                </div>
				</div>
				<div class="prog-section">
				 <h2 class="font-weight-bold">Course Details</h2>
				<?php
					$countsms = count($sems);
					if($countsms==0 || $countsms==1){
						if(!empty($procourse)){
							$cc = 1;
							echo '<div id="accordion" role="tablist">';
							foreach($procourse as $pc){
								  echo '<div class="card-collapse bg-white" style="padding: 0 1rem; border-radius: 10px;">
											<div class="card-header" role="tab" id="heading'.$cc.'">
												<h5 class="mb-0">
													<a data-toggle="collapse" href="#collapse'.$cc.'" aria-expanded="true" aria-controls="collapse'.$cc.'" class="collapsed">
														<span style="color:#206992"> '.$pc->title.' ('.$pc->c_code.')</span>
														<i class="material-icons">keyboard_arrow_down</i>
													</a>
												</h5>
											</div>
											<div id="collapse'.$cc.'" class="collapse '.(($cc == 1)? 'show':'').'" role="tabpanel" aria-labelledby="'.$cc.'" data-parent="#accordion" style="">
												<div class="card-body">
													<a href="javascript:;" class="mr-2"> Lectures ('.$pc->lec.')</a>
													<a href="javascript:;" class="mr-2"> Tutorials ('.$pc->tut.')</a>
													<a href="javascript:;" class="mr-2"> Practicals ('.$pc->prac.')</a><br>
														'.trim($pc->overview).'
												</div>
											</div>
										</div>';
									$cc++;
							}
							echo '</div>';
						}
					}else{
						foreach($sems as $srow){
						   echo'<li class="timeline-inverted">
									<div class="timeline-badge success">'.$i.'</div>
									<div class="timeline-panel">
										<div class="timeline-heading">
											'.$srow->title.'
										</div>
										<div class="timeline-body">';
											if(!empty(${'procourse_'.$i})){
												foreach(${'procourse_'.$i} as $pc){
													echo '<span class="badge badge-pill badge-info"> '.$pc->title.' ('.$pc->c_code.')</span><br>
													<a href="javascript:;" class="mr-2"> Lectures ('.$pc->lec.')</a>
													<a href="javascript:;" class="mr-2"> Tutorials ('.$pc->tut.')</a>
													<a href="javascript:;" class="mr-2"> Practicals ('.$pc->prac.')</a>';
													if($pc->syllabus!=''){
														echo '<a href="'.base_url().'uploads/course/'.$pc->syllabus.'" target="_blank">Shyllabus</a>';
													}
													echo '<br>';
												}
											}
							echo '      </div>
									</div>
								</li>
							';
							$i++;
						}
					}
					echo '</ul>';
				?>
				</div>
			</div>
			<aside class="col-md-3 prog-side">
				<div class="sidebar__inner justify-content-center text-center">
				<h4 class="font-weight-bold text-center">Program Fees</h4>
				<p class="font-weight-bold mb-0" style="color:#206992">Program Fees : <?php echo (($fee=='Paid')? (($famt==0)? 'Rs '.$amt : 'Rs. strike>'.$amt.'</strike> Rs. '.$famt) : $fee);?></p>
				<?php
					if($sdt!=19800 || $ldt!=19800){
						if($curdate>=$sdt && $curdate<=$ldt){
							echo '<a href="'.base_url('programAdmission/?id='.base64_encode($prog[0]->id)).'" class="btn btn-warning btn-sm">Apply Now</a>';
						}
					}
				?>
				<p class="font-weight-bold" style="color:#206992">Deadline : <?php echo ($prog[0]->aend_date)? date('jS F Y',strtotime($prog[0]->aend_date)): '';?></p>
				<ul style="list-style:none; columns:2;">
					<li>Sougam Maity</li>
					<li>9932242598</li>
					<li>Abhinaba Hazra</li>
					<li>6295622155</li>
				</ul>
				</div>
			</aside>
		</div>
		<div class="row mt-3 bg-white py-4">
			<div class="col-md-12">
				<div class="prog-section">
					<h2 class="font-weight-bold">Sample Certificate</h2>
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="banner" style="background-image: url('<?php echo base_url($csrc);?>'); height: 60vmin; border: 2px solid #eee;">
								<div class="video_btn">
								<a data-fancybox href="<?php echo base_url($csrc);?>"> <i class="material-icons play_icon text-dark">zoom_in</i> </a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="w-75 m-auto">
								<h3 class="font-weight-bold"> Magnox Certificate Holders Works On Companies Like</h3>
								<div class="d-flex">
									<ul class="row row-cols-3 row-cols-sm-4 row-cols-lg-6 row-cols-xl-8 list-unstyled list">
										<li class="col mb-4">
											<img src="<?php echo base_url('assets/img/ibm.jpg');?>" style="max-width:150px;">
										</li>
										<li class="col mb-4">
											<img src="<?php echo base_url('assets/img/tcs.png');?>" style="max-width:150px;">
										</li>
									</ul>
								</div>
								<P class="font-weight-bold">
									Industry recognized course completion certificate which will have a lifelong validity. Certificate can be varified from <a href="<?php echo base_url('veryficertificate')?>" target="_blank">this link</a>
								</P>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12 my-3">
				<div class="prog-section">
					<h2 class="font-weight-bold">Application Process</h2>
					<?php
						if(!empty($prog[0]->apply_type)){
							if($prog[0]->apply_type === "0"){
								echo '
										<p class="font-weight-bold">
											Candidates can apply to this '.$prog[0]->title.' in 2 steps. Selected candidates receive an offer of admission, which is accepted by the admission free payment.
										</p>
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="w-75 m-auto">
													<div class="d-flex flex-column justify-content-center align-items-center">
														<img src="'.base_url('assets/img/submit_form.png').'" style="max-width:208px;">
														<span class="font-weight-bold">Submit Application</span>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="w-75 m-auto">
													<div class="d-flex flex-column justify-content-center align-items-center">
														<img src="'.base_url('assets/img/admission.png').'" style="max-width:208px;">
														<span class="font-weight-bold">Admission</span>
													</div>
												</div>
											</div>
										</div>
								';
							}else{
								echo '
										<p class="font-weight-bold">
											Candidates can apply to this '.$prog[0]->title.' in 3 steps. Selected candidates receive an offer of admission, which is accepted by the admission free payment.
										</p>
										<div class="row">
											<div class="col-lg-4 col-md-4 col-sm-12">
												<div class="w-75 m-auto">
													<div class="d-flex flex-column justify-content-center align-items-center">
														<img src="'.base_url('assets/img/submit_form.png').'" style="max-width:208px;">
														<span class="font-weight-bold">Submit Application</span>
													</div>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<div class="w-75 m-auto">
													<div class="d-flex flex-column justify-content-center align-items-center">
														<img src="'.base_url('assets/img/pre-addmission.png').'" style="max-width:208px;">
														<span class="font-weight-bold">Pre-Admission</span>
													</div>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<div class="w-75 m-auto">
													<div class="d-flex flex-column justify-content-center align-items-center">
														<img src="'.base_url('assets/img/admission.png').'" style="max-width:208px;">
														<span class="font-weight-bold">Admission</span>
													</div>
												</div>
											</div>
										</div>
								';
							}
						}
					?>
				</div>
			</div>
			<div class="col-md-12">
				<div class="prog-section">
					<h2 class="font-weight-bold">Live Industry Project</h2>
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-12">
							<div class="wh-75 m-auto">
								<div class="card">
									<div class="card-body h-50">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12 my-3">
				<div class="prog-section">
					<h2 class="font-weight-bold">FAQ Section</h2>

					<div id="accordion">
					<?php
						if(!empty($faq)):
							$fn = 1;
							foreach($faq as $row):
					
							echo '
									<div class="card-collapse bg-white" style="padding: 0 1rem; border-radius: 10px;">
										<div class="card-header" role="tab" id="heading_faq'.$fn.'">
											<h5 class="mb-0">
												<a data-toggle="collapse" href="#collapse_faq'.$fn.'" aria-expanded="true" aria-controls="collapse_faq'.$fn.'" class="collapsed">
													<span class="font-weight-bold text-dark">'.$row->txt_question.'</span>
													<i class="material-icons">keyboard_arrow_down</i>
												</a>
											</h5>
										</div>
										<div id="collapse_faq'.$fn.'" class="collapse '.(($fn == 1)? 'show':'').'" role="tabpanel" aria-labelledby="'.$fn.'" data-parent="#accordion" style="">
											<div class="card-body">
												'.$row->txt_answer.'
											</div>
										</div>
									</div>
								 ';
					
							$fn++;
							endforeach;
						endif;
					?>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="prog-section">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="w-75 align-self-center text-center">
								<div class="d-flex flex-column text-center mb-3">
									<h2 class="font-weight-bold"><span style="color:#206992">Talk With Our Executives</span></h2>
									<h4 class="p-2 font-weight-bold">9932242598</h4>
									<h4 class="p-2 font-weight-bold">6295622155</h4>
								</div>
								<a href="<?php echo base_url('programAdmission/?id='.base64_encode($prog[0]->id));?>" class="btn btn-warning">
									APPLY NOW
									<div class="ripple-container"></div>
								</a>
								<?php
									if($prog[0]->program_brochure!=null){
										if(file_exists('./uploads/programs/'.$prog[0]->program_brochure)){
											echo '<a href="'.base_url('uploads/programs/'.$prog[0]->program_brochure).'" class="btn btn-info">BROCHURE<div class="ripple-container"></div></a>';
										}
									}
								?>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="w-75 m-auto">
								<div class="card">
									<div class="card-body">
										<h2 class="card-title font-weight-bold text-center">Contact Us From</h2>
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group">
													<label for="first_name">First Name</label>
													<input type="text" class="form-control" id="first_name" autocomplete="off">
													<span class="text-danger h6" id="error_first_name"></span>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group">
													<label for="last_name">Last Name</label>
													<input type="text" class="form-control" id="last_name" autocomplete="off">
													<span class="text-danger h6" id="error_last_name"></span>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group">
													<label for="email">Email</label>
													<input type="text" class="form-control" id="email" autocomplete="off">
													<span class="text-danger h6" id="error_email"></span>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group">
													<label for="phone">Phone</label>
													<input type="text" class="form-control" id="phone" autocomplete="off">
													<span class="text-danger h6" id="error_phone"></span>
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="form-group">
													<label for="message">Message</label>
													<textarea class="form-control" id="message" rows="3" autocomplete="off"></textarea>
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<span id="form_submit_msg"></span>
												<button type="button" class="btn btn-primary btn-block" onclick="lmsContactUsForm()">Submit</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js" integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sticky-sidebar/3.3.1/sticky-sidebar.min.js" integrity="sha512-iVhJqV0j477IrAkkzsn/tVJWXYsEqAj4PSS7AG+z1F7eD6uLKQxYBg09x13viaJ1Z5yYhlpyx0zLAUUErdHM6A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
	var sidebar = new StickySidebar('.prog-side', {
        containerSelector: '#main-content',
        innerWrapperSelector: '.sidebar__inner',
        topSpacing: 20,
        bottomSpacing: 20
    });


    function lmsContactUsForm(){
        let first_name = $('#first_name').val();
        let last_name  = $('#last_name').val();
        let email      = $('#email').val();
        let phone      = $('#phone').val();
        let message    = $('#message').val();
        
        let isFirstName = false;
        let isLastName  = false;
        let isEmail     = false;
        let isPhone     = false;

        if(first_name == ''){
            $('#error_first_name').html('First Name is required');
        }else{
            $('#error_first_name').html('');
            isFirstName = true;
        }

        if(last_name == ''){
            $('#error_last_name').html('Last Name is required');
        }else{
            $('#error_last_name').html('');
            isLastName  = true
        }

        if(email == ''){
            $('#error_email').html('Email is required');
        }else if(email !== ''){
            let emailRegx = /^[A-za-z0-9._$#]{1,}@[A-Za-z0-9]{3,}[.]{1}[A-Za-z0-9.]{1,}$/
            if(emailRegx.test(email)){
                $('#error_email').html('');
                isEmail = true;
            }else{
                $('#error_email').html('Email is not valid');
            }            
        }

        if(phone == ''){
            $('#error_phone').html('Phone is required');
        }else if(phone !== ''){
            let phoneRegx = /^[4-9][0-9]{9}$/
            if(phoneRegx.test(phone)){
                $('#error_phone').html('');
                isPhone = true;
            }else{
                $('#error_phone').html('Phone is not valid');
            }
        }

        if(isFirstName && isLastName && isEmail && isPhone){
            $.ajax({
                url  : baseURL+'Login/getContactUs',
                type : 'POST',
                data : {
                    first_name : first_name,
                    last_name : last_name,
                    email : email,
                    phone : phone,
                    message : message
                },
                success: function(data){
                    $('#first_name').val('');
                    $('#last_name').val('');
                    $('#email').val('');
                    $('#phone').val('');
                    $('#message').val('');
                    $('#error_first_name').html('');
                    $('#error_last_name').html('');
                    $('#error_email').html('');
                    $('#error_phone').html('');
                    console.log(data);
                    let resp = JSON.parse(data);
                    $('#form_submit_msg').fadeIn('slow').addClass(resp.class);
                    $('#form_submit_msg').fadeIn('slow').html(resp.msg);
                    setTimeout(function(){
                        $('#form_submit_msg').removeClass(resp.class).fadeOut('slow');
                        $('#form_submit_msg').html('').fadeOut('slow');
                    }, 10000);
                }
            })
        }
    }
</script>