<div class="content">
	<div class="container">
		<div class="row">
			<div class="col-md-8 mr-auto ml-auto">
			  <div class="card">
				<div class="card-header">
					<h4 class="card-title font-weight-bold">Refund Policy</h4>
				</div>
				<div class="card-body">
					A Organization or an individual will get refund only if it is there in the agreement. The Minimum 30% will be deducted in case of Refund.
					<br><br>
					The following points are as follows :
					<br><br>
					1)  Organization can claim of refund if it is there in the agreement.<br><br>

					2) Incase of Payments of the Training Program 30% will be deducted also in case of Refund. The Refund must be claimed within 3 days of his / her registration to the program and before 7 days of Deadline.  <br><br>

					3) Incase of Tieup with the organization for the Training Program, the refund will be given if the refund policy is there in the agreement with the organization.<br><br>

					4) Incase the program is not organized by Magnox or incase of Magnox faults Magnox will return the whole money. <br>
				</div>
			  </div>
			</div>
		</div>
	</div>
</div>