<style>
#course_code::placeholder {
	color: white;
}
.loader {
  background-color: #ffffff;
  opacity:0.5;
  position: fixed;
  z-index: 999999;
  height: 100%;
  width: 100%;
  top: 0;
  left: 0;
}
.loader img {
  position: absolute;
  top: 50%;
  left: 50%;
  text-align: center;
  -webkit-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
}
</style>
<div class="loader" id="loading" style="display:none;">
	<img src="<?php echo base_url().'assets/img/loading.gif'; ?>">
</div>
<div class="content mt-0">
	<div class="container">
	
		<div class="row">
			<div class="col-12">
				<div class="banner-text">
					<?php 
						echo '<h3 class="font-weight-bold text-center">'.$prog[0]->title.'</h3>';
						if(!empty($institute)){
							if($institute[0]->logo!=""){
								if(file_exists('./assets/img/institute/'.$institute[0]->logo)){
									echo '<img src="'.base_url('assets/img/institute/'.$institute[0]->logo).'" class="img-thumbnail" style="width: 100px;"/>';
								}
							}
							
							echo '<h4 class="font-weight-bold">'.$institute[0]->title;
							if(!empty($stream)){
								echo ', '.$stream[0]->title;
							}
							echo '</h4>';
						}
						$type = trim($prog[0]->ptype);
						$category = trim($prog[0]->category);
						$curdate = strtotime(date('d-m-Y'));
						$ldt = strtotime(date('d-m-Y',strtotime($prog[0]->aend_date)));
						$sdt = strtotime(date('d-m-Y',strtotime($prog[0]->astart_date)));
					?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<?php if($curdate<=$ldt){ ?>
			<div class="col-md-5 mx-auto">
				<div class="card">
					<div class="card-header border-bottom">
						<h4 class="card-title font-weight-bold">Sign Up and Complete your Registration <small>(<span class="text-danger">*</span> are mandatory)</small></h4>
					</div>
					<form action="#" method="POST" id="frmreg">
					<div class="card-body">
						<input type="hidden" name="pid" id="pid" value="<?php echo $prog[0]->id; ?>"/>
						<input type="hidden" name="adm_type" id="adm_type" value="register"/>
						<div id="reg_frm">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="fname" class="bmd-label-floating">First Name <span class="text-danger">*</span></label>
									<input type="text" class="form-control" id="fname" name="fname">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="lname" class="bmd-label-floating">Last Name <span class="text-danger">*</span></label>
									<input type="text" class="form-control" id="lname" name="lname">
								</div>
							</div>
						</div>	
						<div class="form-group">
							<label for="email" class="bmd-label-floating">Mobile <span class="text-danger">*</span></label>
							<input type="text" class="form-control" id="mobile" name="mobile">
						</div>
						</div>
						
						<div class="form-group">
							<label for="email" class="bmd-label-floating">Email Address <span class="text-danger">*</span></label>
							<input type="email" class="form-control" id="email" name="email">
						</div>
						
						<div class="form-group">
							<label for="newpass" class="bmd-label-floating">Password <span class="text-danger">*</span></label>
							<input type="password" class="form-control" id="newpass" name="newpass">
						</div>
						
						<div class="form-group">
							<label for="c_code" class="bmd-label-floating">Have Coupon Code?</label>
							<input type="text" class="form-control" id="c_code" name="c_code">
						</div>
						
						<div class="form-group">
							<input type="reset" value="reset" style="display:none;"/>
							<button type="submit" id="btn_save" class="btn btn-block btn-primary">Sign Up</button>
						</div>
					</div>
					<div class="card-footer">
						<h5 class="info-text"> Already have an account? <a href="javascript:toggleRegLogin('signin');" class="btn btn-sm btn-primary">Log in</a></h5>
					</div>
					</form>
				</div>
			</div>
			<?php }else{ ?>
			<div class="col-md-4 mx-auto">
				<div class="card bg-warning">
					<div class="card-body">
						<h4 class="text-center">Application is CLOSED.</h4>
						 <h6 class="text-center text-danger">Deadline : <?php echo ($prog[0]->aend_date)? date('jS F Y',strtotime($prog[0]->aend_date)): '';?>
						 <br>
						 <a href="https://billionskills.com/home/explore_programs" class="btn btn-md btn-success">Explore more programs</a>
						 </h6>
						 
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
		
	</div>
</div>
<script src="<?php echo base_url('assets/js/main.js'); ?>"></script>