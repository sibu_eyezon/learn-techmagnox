<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<div class="content" style="background-color: #d5ddeb;">
	<div class="container">
		<div class="row">
			<div class="col-md-8 mx-auto">
			  <?php
				if($this->session->flashdata('errors')!=""){
					echo '<div class="alert alert-warning alert-with-icon" data-notify="container">
                    <i class="material-icons" data-notify="icon">notifications</i>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
                    <span data-notify="icon" class="now-ui-icons ui-1_bell-53"></span>
                    <span data-notify="message">'.$this->session->flashdata('errors').'</span>
                  </div>';
				}else if($this->session->flashdata('success')!=""){
					echo '<div class="alert alert-success alert-with-icon" data-notify="container">
                    <i class="material-icons" data-notify="icon">notifications</i>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
                    <span data-notify="icon" class="now-ui-icons ui-1_bell-53"></span>
                    <span data-notify="message">'.$this->session->flashdata('success').'</span>
                  </div>';
				}
			  ?>
			  <div class="card">
				<div class="card-body ">
					<div class="row mb-3 justify-content-center">
						<div class="col-sm-10 text-center">
							<img src="<?php echo base_url('assets/img/itewb.png'); ?>" class="img-responsive w-100"/>
						</div>
					</div>
					<h3 class=" text-center">Register Now :<i> <b>Introduction to Python</b> </i></h3>
					<h6>Remember the <span class="text-danger"> *</span> fields are mandatory. Once you submit the REGISTER button you will get an email with an auto generated password. Click the Verification link given in that email to complete the process. </h6><br>
					<form class="form" method="POST" action="<?php echo base_url('Login/progRegistration'); ?>" id="register">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="fname" class="bmd-label-floating">Firstname <span class="text-danger">*</span></label>
									<input type="text" name="fname" id="fname" class="form-control" required="true"/>
									<input type="hidden" name="user_type" id="user_type" value="Student"/>
									<input type="hidden" name="prog_id" id="prog_id" value="45"/>
								  </div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="lname" class="bmd-label-floating">Lastname <span class="text-danger">*</span></label>
									<input type="text" name="lname" id="lname" class="form-control" required="true"/>
								  </div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="email" class="bmd-label-floating">Email <span class="text-danger">*</span></label>
									<input type="text" name="email" id="email" class="form-control" required="true"/>
								  </div>	
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="phone" class="bmd-label-floating">Phone</label>
									<input type="text" name="phone" id="phone" class="form-control"/>
								  </div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
							  <div class="form-group">
								<div class="form-group g-recaptcha w-100" data-sitekey="6LcAodEZAAAAAD_eDVKc-mC1XluCkg4P1kMKzIWy"></div>
							  </div>
							  <div class="form-group">
								<input type="reset" style="display:none"/>
								<button type="submit" class="btn btn-info pull-left" id="btn_login">Register</button>
								<input type="reset" style="visibility:hidden;"/>
							  </div>
							</div>
						</div>
					</form>
				</div>
				<div class="card-footer">
					<a href="<?php echo base_url(); ?>" style="font-size:14px">Already have an account? <b>Sign In</b></a>
				</div>
			  </div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		$('#register').validate({
			errorPlacement: function(error, element) {
			  $(element).closest('.form-group').append(error);
			},
			rules: {
				fname: {
					required: true,
					minlength: 3
				},
				lname: {
					required: true,
					minlength: 3
				},
				email: {
					required: true,
					email: true
				},
				phone: {
					digits: true,
					minlength: 10,
					maxlength: 10
				}
			},

			messages: {
				fname: {
					required: 'Must enter your firstname.',
					minlength: 'The firstname must be minimum 3characters.'
				},
				lname: {
					required: 'Must enter your lastname.',
					minlength: 'The lastname must be minimum 3characters.'
				},
				email: {
					required: 'Must enter your email address.',
					email: 'Enter valid email address'
				},
				phone: {
					digits: 'Must enter digits only.',
					minlength: 'Phone must be 10 digits.',
					maxlength: 'Phone must be 10 digits.'
				}
			}
		});
	});
</script>