<div class="content">
	<div class="container">
		<div class="row">
			<div class="col-md-8 mr-auto ml-auto">
			  <div class="card">
				<div class="card-header">
					<h4 class="card-title font-weight-bold">Privacy Policy</h4>
				</div>
				<div class="card-body">
					Magnox provides a platform for the Training Program. Magnox will not be responsible for the Training Contents and the Copyright of the contents belong to the Training Partner and in such case Magnox will not be responsible for any copyright infringement issue.<br><br>
					The Teacher will not use the copyright material of other teacher without the permission of the owner of the content.
				</div>
			  </div>
			</div>
		</div>
	</div>
</div>