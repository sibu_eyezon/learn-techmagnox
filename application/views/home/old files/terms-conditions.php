<div class="content">
	<div class="container">
		<div class="row">
			<div class="col-md-8 mr-auto ml-auto">
			  <div class="card">
				<div class="card-header">
					<h4 class="card-title font-weight-bold">Terms and Conditions</h4>
				</div>
				<div class="card-body">
					The Terms and Conditions for the use of Magnox Learning Cloud Platform are stated in brief :
					<br><br>
					Teachers :<br>
					1) Teacher Refer to anyone who will use the portal to teach the students, add or manage a program or teach in others program.<br>
					2) If the teacher add a new program and get it approved, he or she will become the admin of the program<br>
					3) Incase of Payment settlement, it will be done once in a month.<br>
					4) The Teacher will either pay a monthly / yearly charge or will pay on per students basis , depending on the agreement with him.<br>
					5) The Teacher will not use the copyright material of other teacher without the permission of the owner of the content.<br>
					6) The teacher will not upload videos of the other Professor which do not belong to him/her<br>
					7) Incase the Teacher violates the agreement we can block his or her account.<br>
					<br>
					Students :<br>
					1) The Students will register into the Portal, search a program and enroll there for Learning.<br>
					2) For the Free Programs students can access it for Free<br>
					3) For the Paid Programs students will have to pay.<br>
					4) The Refund regarding cancellation of Program Registration the Refund will be given as per the Refund Policy
				</div>
			  </div>
			</div>
		</div>
	</div>
</div>