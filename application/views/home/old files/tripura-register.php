<style>
	.img-reg {
		width: 150px;
	}
</style>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<div class="content" style="background-color: #d5ddeb;">
	<div class="container">
		<div class="row">
			<div class="col-md-10 mx-auto">
			  
			  <div class="card">
				<div class="card-header">
					<div class="row justify-content-center">
						<div class="col-md-12 text-center">
							<img src="<?php echo base_url('assets/img/icfai.png'); ?>" class="img-responsive"/>
						</div>
					</div>	
					<div class="row justify-content-center">
						<div class="col-12">
							<h3 class="card-title text-center">Programs to be applied</h3>
						</div>
					</div>
				</div>
				<div class="card-body">
					<h4 class="card-title">Here are top 5 programs listed out. Do have a look to find your interest and upgrade your skills.</h4>
					<h6 class="text-center">Use promocode: 'CODE1000' for flat Rs. 1000/- off on each programs.</h6>
					<div class="row justify-content-center">
					<?php
						$i=1;
						foreach($progs as $prow){
							$fee = trim($prow->feetype);
							echo '<div class="col-lg-4 col-md-4 mb-3">
							<div class="card card-product">
							  <a href="'.base_url().'programDetails/?id='.base64_encode($prow->id).'" target="_blank">
							  <div class="card-header card-header-image">
								
								  <img class="img" src="'.base_url().'assets/img/banner/'.trim($prow->banner).'" onerror="this.src=`'.base_url().'assets/img/sample.jpg`">
								
							  </div>
							  </a>
							  <div class="card-body">
								<h5 class="card-title font-weight-bold"><a href="'.base_url().'programDetails/?id='.base64_encode($prow->id).'" target="_blank">'.trim($prow->title).' ('.trim($prow->yearnm).')</a></h5>
								<div class="card-description">';
								  if(!empty(${'ins_'.$i}))
								  {
									  echo trim(${'ins_'.$i}[0]->title).', ';
								  }
								  $dur = intval(trim($prow->duration));
								  echo 'Duration: '.$dur.' '.trim($prow->dtype).(($dur==1)? '':'s').'
								  <h6 class="text-center">'.(($fee=='Paid')? 'Rs '.$prow->total_fee : $fee).'</h6>
								  <h6 class="text-center">Start Date: '.date('jS M Y',strtotime($prow->start_date)).'</h6>
								</div>
							  </div>
							  <div class="card-footer">
								<div class="price">
								  <h6>'.trim($prow->category).'</h6>
								</div>
							  </div>
							</div>
						  </div>';
						  $i++;
						}
					?>
					</div>
				</div>
				<div class="card-footer">
					<div class="pull-right text-left font-weight-bold ml-auto">
						Powered By<br>
						tech<img src="https://techmagnox.com/assets/images/logo.png" width="150"/>
					</div>
				</div>
			  </div>
			  
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		$('#register').validate({
			errorPlacement: function(error, element) {
			  $(element).closest('.form-group').append(error);
			},
			rules: {
				fname: {
					required: true,
					minlength: 3
				},
				lname: {
					required: true,
					minlength: 3
				},
				email: {
					required: true,
					email: true
				},
				phone: {
					digits: true,
					minlength: 10,
					maxlength: 10
				},
				address: {
					required: true
				},
				country: {
					required: true
				},
				gender: {
					required: true
				},
				category: {
					required: true
				},
				org: {
					required: true
				}
			},

			messages: {
				fname: {
					required: 'Must enter your firstname.',
					minlength: 'The firstname must be minimum 3characters.'
				},
				lname: {
					required: 'Must enter your lastname.',
					minlength: 'The lastname must be minimum 3characters.'
				},
				email: {
					required: 'Must enter your email address.',
					email: 'Enter valid email address'
				},
				phone: {
					digits: 'Must enter digits only.',
					minlength: 'Phone must be 10 digits.',
					maxlength: 'Phone must be 10 digits.'
				},
				address: {
					required: 'Must enter your current address.'
				},
				country: {
					required: 'Must enter your country name.'
				},
				gender: {
					required: 'Must select your gender.'
				},
				category: {
					required: 'Must select your category.'
				},
				org: {
					required: 'Must enter your current Organization/Institute.'
				}
			}
		});
	});
</script>