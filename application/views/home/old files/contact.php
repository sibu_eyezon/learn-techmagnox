<script type="text/javascript" src="<?php echo base_url('assets/js/common.js'); ?>"></script>
<div class="content">
	<div class="container">
		<div class="row">
			<div class="col-10 mx-auto">
				<div class="card ">
					<div class="card-body">
						<div class="row">
							<div class="col-md-12">
								<img src="<?php echo base_url('assets/img/contact-us-banner.jpg'); ?>" class="img-responsive img-fluid w-100"/>
							</div>
						</div>
						<div class="row">
							<div class="col-md-8">
								<p style="font-size: 1.2rem;">
									Magnox Technologies Pvt. Ltd.<br>
									7th Floor, Nasscom Warehouse, Mani Bhandar (7th Floor)<br>
									Webel Bhaban, Sector V, Saltlake<br>
									Kolkata - 91
								</p>
								<br>
								<p style="font-size: 1.2rem;">
									Magnox Technologies Pvt. Ltd.<br>
									2nd Floor, Flot No : 72<br>
									Prembazar, Hijli Co-Operative Society<br>
									Kharagpur - 721306
								</p>
							</div>
							<div class="col-md-4">
								<a href="https://www.facebook.com/magnox/" target="_blank" class="btn btn-just-icon btn-link btn-facebook">
									<i class="fa fa-facebook"></i>
								</a>
								<a href="javascript:;" target="_blank" class="btn btn-just-icon btn-link btn-instagram">
									<i class="fa fa-instagram"></i>
								</a>
								<a href="javascript:;" target="_blank" class="btn btn-just-icon btn-link btn-twitter">
									<i class="fa fa-twitter"></i>
								</a>
								<a href="https://www.linkedin.com/company/magnox-technologies-private-limited" target="_blank" class="btn btn-just-icon btn-link btn-linkedin">
									<i class="fa fa-linkedin"></i>
								</a>
								<p style="font-size: 1.2rem;">
									(91) 9932242598<br>
									sougam@techmagnox.com<br>
									magnox.iitkgp@gmail.com
								</p>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-12">
								<form class="form" method="POST" id="reqDemo">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="fname">Firstname <span class="text-danger">*</span></label>
												<input type="text" name="fname" id="fname" class="form-control"/>
												
											  </div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="lname">Lastname <span class="text-danger">*</span></label>
												<input type="text" name="lname" id="lname" class="form-control"/>
											  </div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="email">Email <span class="text-danger">*</span></label>
												<input type="text" name="email" id="email" class="form-control"/>
											  </div>	
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="phone">Phone <span class="text-danger">*</span></label>
												<input type="text" name="phone" id="phone" class="form-control"/>
											  </div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="job_title">Job Title</label>
												<input type="text" name="job_title" id="job_title" class="form-control"/>
											  </div>
											  
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="company">Company</label>
												<input type="text" name="company" id="company" class="form-control">
											  </div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label for="desc">Description</label>
												<textarea name="desc" id="desc" class="form-control"></textarea>
											  </div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12 text-center">
											<button type="submit" class="btn btn-info btn-md" id="btn_login">Request for Demo</button>
										</div>
									</div>	
									<div class="row">
										<div class="col-sm-12">
											<input type="reset" style="visibility:hidden;"/>
										</div>
									</div>									
							  </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>