<style>
	.img-reg {
		width: 150px;
	}
</style>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<div class="content" style="background-color: #d5ddeb;">
	<div class="container">
		<div class="row">
			<div class="col-md-10 mx-auto">
			  <?php
				if($this->session->flashdata('errors')!=""){
					echo '<div class="alert alert-warning alert-with-icon" data-notify="container">
                    <i class="material-icons" data-notify="icon">notifications</i>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
                    <span data-notify="icon" class="now-ui-icons ui-1_bell-53"></span>
                    <span data-notify="message">'.$this->session->flashdata('errors').'</span>
                  </div>';
				}else if($this->session->flashdata('success')!=""){
					echo '<div class="alert alert-success alert-with-icon" data-notify="container">
                    <i class="material-icons" data-notify="icon">notifications</i>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
                    <span data-notify="icon" class="now-ui-icons ui-1_bell-53"></span>
                    <span data-notify="message">'.$this->session->flashdata('success').'</span>
                  </div>';
				}
			  ?>
			  <div class="card">
				<div class="card-header">
					<div class="row justify-content-center">
						<div class="col-md-4 text-center">
							<img src="<?php echo base_url('assets/img/portfolio/1.png'); ?>" class="img-reg"/>
						</div>
						<div class="col-md-8">
							<img src="<?php echo base_url('assets/img/title.png'); ?>" class="w-100"/>
						</div>
					</div>	
					<div class="row justify-content-center">
						<div class="col-12">
							<h3 class="card-title text-center">Registration Form</h3>
							<p class="text-right"><small class="text-danger">(All fields are mandatory)</small></p>
						</div>
					</div>
				</div>
				<div class="card-body bg-info">
					<form class="form" method="POST" action="<?php echo base_url('Login/purdueRegistration'); ?>" id="register">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="fname" class="bmd-label-floating text-white">Firstname</label>
									<input type="text" name="fname" id="fname" class="form-control"/>
								  </div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="lname" class="bmd-label-floating text-white">Lastname</label>
									<input type="text" name="lname" id="lname" class="form-control"/>
								  </div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="email" class="bmd-label-floating text-white">Email</label>
									<input type="email" name="email" id="email" class="form-control"/>
								  </div>	
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="phone" class="bmd-label-floating text-white">Phone</label>
									<input type="text" name="phone" id="phone" class="form-control"/>
								  </div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label for="address" class="bmd-label-floating text-white">Address</label>
									<textarea class="form-control" style="width:100%;" rows="5" cols="80" name="address" id="address"></textarea>
								</div>
							</div>
						</div>	
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="country" class="bmd-label-floating text-white">Country</label>
									<input type="text" name="country" id="country" class="form-control"/>
								  </div>	
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="gender" class="text-white">Gender:</label>
									<div class="checkbox-radios">
										<div class="form-check form-check-inline">
										  <label class="form-check-label text-white">
											<input class="form-check-input" type="radio" value="M" name="gender" id="M"> Male
											<span class="form-check-sign">
											  <span class="check"></span>
											</span>
										  </label>
										</div>
										<div class="form-check form-check-inline">
										  <label class="form-check-label text-white">
											<input class="form-check-input" type="radio" value="F" name="gender" id="F"> Female
											<span class="form-check-sign">
											  <span class="check"></span>
											</span>
										  </label>
										</div>
									</div>
								  </div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label for="org" class="bmd-label-floating text-white">Organization/Institute</label>
									<input type="text" name="org" id="org" class="form-control"/>
								  </div>	
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 justify-content-center">
								<div class="form-group">
									<label for="category" class="text-white">Category:</label>
									<div class="checkbox-radios">
										<div class="form-check form-check-inline">
										  <label class="form-check-label text-white">
											<input class="form-check-input" type="radio" value="ST" name="category" id="ST"> Student
											<span class="form-check-sign">
											  <span class="check"></span>
											</span>
										  </label>
										</div>
										<div class="form-check form-check-inline">
										  <label class="form-check-label text-white">
											<input class="form-check-input" type="radio" value="FY" name="category" id="FY"> Faculty
											<span class="form-check-sign">
											  <span class="check"></span>
											</span>
										  </label>
										</div>
										<div class="form-check form-check-inline">
										  <label class="form-check-label text-white">
											<input class="form-check-input" type="radio" value="IF" name="category" id="IF"> Industry Professionals
											<span class="form-check-sign">
											  <span class="check"></span>
											</span>
										  </label>
										</div>
										<!--<div class="form-check form-check-inline">
										  <label class="form-check-label text-white">
											<input class="form-check-input" type="radio" value="UG" name="category" id="UG"> Under Graduate
											<span class="form-check-sign">
											  <span class="check"></span>
											</span>
										  </label>
										</div>
										<div class="form-check form-check-inline">
										  <label class="form-check-label text-white">
											<input class="form-check-input" type="radio" value="PG" name="category" id="PG"> Post Graduate
											<span class="form-check-sign">
											  <span class="check"></span>
											</span>
										  </label>
										</div>-->
									</div>
								  </div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
							  <div class="form-group">
								<div class="form-group g-recaptcha w-100" data-sitekey="6LcAodEZAAAAAD_eDVKc-mC1XluCkg4P1kMKzIWy"></div>
							  </div>
							  <div class="form-group">
								<input type="reset" style="display:none"/>
								<button type="submit" class="btn btn-success pull-left" id="btn_login">Register</button>
								<input type="reset" style="visibility:hidden;"/>
							  </div>
							</div>
						</div>
					</form>
				</div>
				<div class="card-footer">
					<div class="pull-right text-left font-weight-bold ml-auto">
						Powered By<br>
						tech<img src="https://techmagnox.com/assets/images/logo.png" width="150"/>
					</div>
				</div>
			  </div>
			  
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function() {
		$('#register').validate({
			errorPlacement: function(error, element) {
			  $(element).closest('.form-group').append(error);
			},
			rules: {
				fname: {
					required: true,
					minlength: 3
				},
				lname: {
					required: true,
					minlength: 3
				},
				email: {
					required: true,
					email: true
				},
				phone: {
					digits: true,
					minlength: 10,
					maxlength: 10
				},
				address: {
					required: true
				},
				country: {
					required: true
				},
				gender: {
					required: true
				},
				category: {
					required: true
				},
				org: {
					required: true
				}
			},

			messages: {
				fname: {
					required: 'Must enter your firstname.',
					minlength: 'The firstname must be minimum 3characters.'
				},
				lname: {
					required: 'Must enter your lastname.',
					minlength: 'The lastname must be minimum 3characters.'
				},
				email: {
					required: 'Must enter your email address.',
					email: 'Enter valid email address'
				},
				phone: {
					digits: 'Must enter digits only.',
					minlength: 'Phone must be 10 digits.',
					maxlength: 'Phone must be 10 digits.'
				},
				address: {
					required: 'Must enter your current address.'
				},
				country: {
					required: 'Must enter your country name.'
				},
				gender: {
					required: 'Must select your gender.'
				},
				category: {
					required: 'Must select your category.'
				},
				org: {
					required: 'Must enter your current Organization/Institute.'
				}
			}
		});
	});
</script>