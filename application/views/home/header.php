<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <link rel="icon" type="image/png" href="<?php echo base_url().'assets/img/favicon.png'; ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
	<?php echo $pageTitle; ?>
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--  Social tags      -->
  <meta name="keywords" content="">
  <meta name="description" content="">

  <!--     Fonts and icons     -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/latest/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css2?family=Arizonia&display=swap" rel="stylesheet">
  <!-- CSS Files -->
  <link href="<?php echo base_url(); ?>assets/css/material-dashboard.min1c51.css?v=2.1.2" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>assets/demo/demo.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>assets/vendor/owlcarousel/css/owl.carousel.min.css" rel="stylesheet">
  <!--   Core JS Files   -->
  <script src="<?php echo base_url(); ?>assets/js/core/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/core/popper.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/plugins/moment.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap-notify.js"></script>
  <script>var baseURL='<?php echo base_url(); ?>';</script>
</head>

<body>
<div class="preloader" id="loader">
	<img src="<?php echo base_url().'assets/img/Preloader_3.gif'; ?>">
</div>
<div class="wrapper ">
	<div class="sidebar" data-color="purple" data-background-color="black" data-image="<?php echo base_url(); ?>assets/img/sidebar-1.jpg">
		<div class="sidebar-wrapper">
			<ul class="nav">
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url(); ?>">
					  <i class="material-icons">home</i>
					  <p> Home </p>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url('programs'); ?>">
					  <i class="material-icons">home</i>
					  <p> Explore Programs </p>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="https://www.billionskills.com/" target="_blank">
					  <i class="material-icons">home</i>
					  <p> BillionSkills.com </p>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" data-toggle="collapse" href="#sideRegister">
					  <i class="material-icons">developer_board</i>
					  <p> Register
						<b class="caret"></b>
					  </p>
					</a>
					<div class="collapse" id="sideRegister">
					  <ul class="nav">
						<li class="nav-item ">
						  <a class="nav-link" href="<?php echo base_url('register/student'); ?>">
							<span class="sidebar-mini"> SR </span>
							<span class="sidebar-normal"> Student Register </span>
						  </a>
						</li>
						<li class="nav-item ">
						  <a class="nav-link" href="<?php echo base_url('register/teacher'); ?>">
							<span class="sidebar-mini"> TR </span>
							<span class="sidebar-normal"> Teacher Register </span>
						  </a>
						</li>
					  </ul>
					</div>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="https://www.billionskills.com/" target="_blank">
					  <i class="material-icons">home</i>
					  <p> Skill & Career </p>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="main-panel">
		<nav class="navbar navbar-expand-lg navbar-absolute fixed-top" style="background:transparent !important; box-shadow:none !important;">
			<div class="container-fluid d-none d-sm-block d-md-none d-block d-sm-none">
				<div class="navbar-wrapper">
					<a class="navbar-brand" href="<?php echo base_url('Teacher'); ?>"><img src="<?php echo base_url(); ?>assets/img/logo.png" class="img-responsive w-100" /></a>
				</div>
				<button class="navbar-toggler pull-right" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
					<span class="sr-only">Toggle navigation</span>
					<span class="navbar-toggler-icon icon-bar"></span>
					<span class="navbar-toggler-icon icon-bar"></span>
					<span class="navbar-toggler-icon icon-bar"></span>
				</button>
			</div>
		</nav>