<style>
    .icon-container {
        position: absolute;
        /*right: 10px;*/
        top: calc(50% - 10px);
    }
    .loader {
        position: relative;
        height: 20px;
        width: 20px;
        display: inline-block;
        animation: around 5.4s infinite;
    }

    @keyframes around {
        0% {
            transform: rotate(0deg)
        }
        100% {
            transform: rotate(360deg)
        }
    }

    .loader::after, .loader::before {
        content: "";
        background: white;
        position: absolute;
        display: inline-block;
        width: 100%;
        height: 100%;
        border-width: 2px;
        border-color: #333 #333 transparent transparent;
        border-style: solid;
        border-radius: 20px;
        box-sizing: border-box;
        top: 0;
        left: 0;
        animation: around 0.7s ease-in-out infinite;
    }

    .loader::after {
        animation: around 0.7s ease-in-out 0.1s infinite;
        background: transparent;
    }
</style>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
                <form class="form" onsubmit="return validation()" method="post" action="<?php echo base_url('Veryficertificate/first_step_varification');?>">
                    <div class="card card-login card-hidden">
                        <div class="card-header card-header-rose text-center">
                            <h4 class="card-title">Verify Certificate</h4>
                        </div>
                        <div class="card-body ">
                            <p class="card-description text-center">Step-1</p>
                              <div class="form-group">
                                  <label for="name">Full Name <span class="text-danger">*</span></label>
                                  <input type="text" class="form-control" id="name" name="name" autocomplete="off">
                                  <small id="errorName" class="form-text text-muted text-danger"></small>
                              </div>
                              <div class="form-group">
                                <label for="email">Email <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="email" name="email" autocomplete="off">
                                <small id="errorEmail" class="form-text text-muted text-danger"></small>
                              </div>
                              <div class="form-group">
                                <label for="phone">Phone <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="phone" name="phone" autocomplete="off">
                                <small id="errorPhone" class="form-text text-muted text-danger"></small>
                              </div>
                        </div>
                        <div class="card-footer justify-content-center">
                            <button type="submit" class="btn btn-rose btn-link btn-lg" id="submit">Lets Go</button>
                            <div class="icon-container" style="display:none;">
                                <i class="loader"></i>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
  function validation(){
    let name  = $('#name').val();
    let email = $('#email').val();
    let phone = $('#phone').val();
    let nameMsg = false;
    let emailMsg = false;
    let phoneMsg = false;

    if(name === ''){
      $('label[for="name"]').attr('class','text-danger');
      $('#errorName').html('Full Name is required');
    }else if(name !== ''){
      $('label[for="name"]').removeAttr('class');
      $('#errorName').html('');
      nameMsg = true;
    }

    if(email === ''){
      $('label[for="email"]').attr('class','text-danger');
      $('#errorEmail').html('Email is required');
    }else if(email !== ''){
      let emailRegx = /^[A-za-z0-9._$#]{1,}@[A-Za-z0-9]{3,}[.]{1}[A-Za-z0-9.]{1,}$/
      if(emailRegx.test(email)){
        $('label[for="email"]').removeAttr('class');
        $('#errorEmail').html('');
        emailMsg = true;
      }else{
        $('label[for="email"]').attr('class','text-danger');
        $('#errorEmail').html('Please enter a valid email');
      }
    }

    if(phone === ''){
      $('label[for="phone"]').attr('class','text-danger');
      $('#errorPhone').html('Phone Number is required');
    }else if(phone !== ''){
      let phoneRegx = /^[4-9][0-9]{9}$/
      if(phoneRegx.test(phone)){
        $('label[for="phone"]').removeAttr('class');
        $('#errorPhone').html('');
        phoneMsg = true;
      }else{
        $('label[for="phone"]').attr('class','text-danger');
        $('#errorPhone').html('Please enter a valid phone number');
      }
      
    }

    if(nameMsg && emailMsg && phoneMsg){
      $('#submit').hide();
      $('.icon-container').show();
      return true;
    }else{
      return false;
    }

  }
</script>


