<style>
    th {
        background-color: #4287f5;
        color: white;
    }

    th,td {
        width: 250px;
        text-align: left;
    }
    .geeks {
        border-right:hidden;
    }
    .gfg {
        border-collapse:separate;
        border-spacing:0 15px;
    }
</style>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="card-header card-header-info">
						<h3 class="card-title">Certificate Details</h3>
					</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="card p-2">
						            <h4 class="text-center text-uppercase font-weight-bold mb-3">User Details</h4>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <img src="<?php echo ($user_details->photo_sm)? base_url($user_details->photo_sm) : base_url('assets/img/default-avatar.png');?>" width="220">
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <table class = "gfg">
                                                    <tr class="mb-3">
                                                        <td class="font-weight-bold geeks">Name : </td>
                                                        <td><?php echo $user_details->first_name.' '.$user_details->last_name; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="font-weight-bold geeks">Email : </td>
                                                        <td><?php echo $user_details->email;?></td>
                                                    </tr>
                                                    <!-- <tr>
                                                        <td class="font-weight-bold geeks">Phone : </td>
                                                        <td><?php echo $user_details->phone;?></td>
                                                    </tr> -->
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="card p-2">
                                    <h4 class="text-center text-uppercase font-weight-bold">Program Details</h4>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <table class = "gfg">
                                                    <tr class="mb-3">
                                                        <td class="font-weight-bold geeks">Title : </td>
                                                        <td><?php echo $program_details->title;?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="font-weight-bold geeks">Code : </td>
                                                        <td><?php echo $program_details->code;?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="font-weight-bold geeks">Category : </td>
                                                        <td><?php echo $program_details->category;?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="font-weight-bold geeks">Start Date : </td>
                                                        <td><?php echo $program_details->start_date;?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="font-weight-bold geeks">End Date : </td>
                                                        <td><?php echo $program_details->end_date;?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="font-weight-bold geeks">Certificate Code : </td>
                                                        <td><?php echo $code;?></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>