<style>
    .icon-container {
        position: absolute;
        /*right: 10px;*/
        top: calc(50% - 10px);
    }
    .loader {
        position: relative;
        height: 20px;
        width: 20px;
        display: inline-block;
        animation: around 5.4s infinite;
    }

    @keyframes around {
        0% {
            transform: rotate(0deg)
        }
        100% {
            transform: rotate(360deg)
        }
    }

    .loader::after, .loader::before {
        content: "";
        background: white;
        position: absolute;
        display: inline-block;
        width: 100%;
        height: 100%;
        border-width: 2px;
        border-color: #333 #333 transparent transparent;
        border-style: solid;
        border-radius: 20px;
        box-sizing: border-box;
        top: 0;
        left: 0;
        animation: around 0.7s ease-in-out infinite;
    }

    .loader::after {
        animation: around 0.7s ease-in-out 0.1s infinite;
        background: transparent;
    }
</style>
<div class="content">
    <div class="container-fluid" id="pageId">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
                
                    <div class="card card-login card-hidden">
                        <div class="card-header card-header-rose text-center">
                            <h4 class="card-title">Verify Certificate</h4>
                        </div>
                        <div class="card-body ">
                            <p class="card-description text-center">Step-2</p>
                            <span class="bmd-form-group">
                                <div class="form-group">
                                    <label for="certificate">Certificate Code <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="certificate" placeholder="Enter Certificate Number" name="certificate">
                                    <small id="errCertificate" class="form-text text-danger"></small>
                                </div>
                            </span>
                            <span class="bmd-form-group">
                                <div class="form-group">
                                    <label for="otp">OTP <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="otp" placeholder="OTP" name="otp">
                                    <small id="errOtp" class="form-text text-danger"></small>
                                    <small id="timer"></small>
                                    <a id="resesnOtp" href="javascript:resendOTP()">Resend OTP</a>
                                    <small id="otpMsg" class="form-text text-muted">Please check your email for OTP</small>
                                </div>
                            </span>
                        </div>
                        <div class="card-footer justify-content-center">
                            <button class="btn btn-rose btn-link btn-lg" onclick="validation()" id="submit">Lets Go</button>
                            <div class="icon-container" style="display:none;">
                                <i class="loader"></i>
                            </div>
                        </div>
                    </div>
                
            </div>
        </div>
    </div>
</div>

<script>
    function timerOperation(){
        $('#timer').show();
        $('a[id="resesnOtp"]').hide();
        let time = 40;
        var myVar = setInterval(updateCounter,1000);
        function updateCounter(){
            let second = time < 10 ? `0${time}` : time ;
            let counter = `00:${second}`;
            if(time >= 10){
                $('#timer').attr('class','form-text text-success');
                $('#timer').html(counter);
            }else if(time < 10 && time >= 0){
                $('#timer').attr('class','form-text text-danger');
                $('#timer').html(counter);
            }else if(time < 0){
                $('#timer').attr('class','form-text text-primary');
                $('#timer').hide();
                $('a[id="resesnOtp"]').show();
                $('label[for="certificate"]').removeAttr('class');
                $('#errCertificate').html('');
                $('label[for="otp"]').removeAttr('class');
                $('#errOtp').html('');
                clearInterval(myVar);
            }
            time --;
        }

    }


    function resendOTP(){
        $('#otp').val('');
        $('#submit').hide();
        $('.icon-container').show();
        $('#otpMsg').hide();
        $.ajax({
            url : baseURL + 'Veryficertificate/resendOtp',
            type : 'POST',
            success : function(data){
                let res = JSON.parse(data);
                if(res.key === '1'){
                    $('label[for="certificate"]').removeAttr('class');
                    $('#errCertificate').html('');
                    $('label[for="otp"]').removeAttr('class');
                    $('#errOtp').html('');
                    $('#submit').show();
                    $('.icon-container').hide();
                    $('#otpMsg').show();
                    timerOperation();
                }
            }
        })
    }

    $(document).ready(function(){
        let getVal = document.cookie
        if(getVal.split(';').length <= 2){
            $('#timer').hide();
            $('a[id="resesnOtp"]').show();
        }else{
            timerOperation();
        }
        
    });

    function validation(){
        let certificate = $('#certificate').val();
        let otp         = $('#otp').val();

        let isCCcode = false;
        let isOtp    = false;

        if(certificate === ''){
            $('label[for="certificate"]').attr('class','text-danger');
            $('#errCertificate').html('Certificate Code is required');
        }else if(certificate !== ''){
            $('label[for="certificate"]').removeAttr('class');
            $('#errCertificate').html('');
            isCCcode = true;
        }

        if(otp === ''){
            $('label[for="otp"]').attr('class','text-danger');
            $('#errOtp').html('OTP is required');
        }else if(certificate !== ''){
            $('label[for="otp"]').removeAttr('class');
            $('#errOtp').html('');
            isOtp = true;
        }


        if(isCCcode && isOtp){
            $.ajax({
                url  : baseURL + 'Veryficertificate/get_verify_certificate_code_and_opt',
                type : 'POST',
                data : {
                    certificate : certificate,
                    otp : otp
                },
                success : function(data){
                    let res = JSON.parse(data);
                    if(res.key === "otp_error"){
                        $('label[for="otp"]').attr('class','text-danger');
                        $('#errOtp').html(res.message);
                    }else if(res.key === "code_error"){
                        $('label[for="certificate"]').attr('class','text-danger');
                        $('#errCertificate').html(res.message);
                    }else if(res.key === "code_success"){
                        let prog_id          = res.response.prog_id;
                        let stud_id          = res.response.stud_id;
                        let certificate_code = res.response.enrollment_no
                        window.location.href = baseURL + `Veryficertificate/get_certificate_details/?prog_id=${btoa(prog_id)}&stud_id=${btoa(stud_id)}&code=${btoa(certificate_code)}`
                    }
                }
            })
        }

    }
</script>