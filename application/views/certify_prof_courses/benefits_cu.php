<?php
	$prog_benefits = $this->Member->getAllProgramBenefits($prog_id);
	$pbcount = count($prog_benefits);
	$obj = array('id'=>0, 'program_id'=>0, 'txt_benefit'=>"", 'txt_benefit_dtls'=>"");
	if($pb_id == 0){
		$pb[0] = (object)$obj;
	}else{
		$pb = $this->Member->getProgBenefitsByID($pb_id);
	}
	if($pbcount<4){
?>
<form action="#" id="frmBenefits" method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label class="text-dark">Benefit Title</label>
		<input type="text" name="title" id="title" class="form-control" value="<?php echo trim($pb[0]->txt_benefit); ?>"  required="true">
	</div>

	<input type="hidden" name="prog_id" id="prog_id" value="<?php echo $prog_id; ?>">
	<input type="hidden" name="pb_id" id="pb_id" value="<?php echo $pb_id; ?>">
	<input type="hidden" name="progtype" id="progtype" value="<?php echo $progtype; ?>">

	<div class="form group">
		<textarea class="form-control w-100" cols="80" rows="5" name="details" id="details"  required="true"><?php echo trim($pb[0]->txt_benefit_dtls); ?></textarea>
	</div>
	<div class="text-right">
		<button class = "btn btn-success" type="submit" name="button">Save</button>
	</div>
</form>
	<?php }else{ echo 'You can only add 4 Benefits.'; } ?>
<script>
$(document).ready(()=>{
	$('#frmBenefits').validate({
		errorPlacement: function(error, element) {
		  $(element).closest('.form-group').append(error);
		},
		submitHandler: function(form, e)
		{
			e.preventDefault();
			var frmData = new FormData($('#frmBenefits')[0]);
			$.ajax({
				url: baseURL+'Certifyprogram/pcuBenefits',
				type: 'POST',
				processData:false,
				contentType:false,
				data: frmData,
				success: (res)=>{
					var obj = JSON.parse(res);
					if(obj['status']){
						setProgramBenefits(res);
						$('#programModal').modal('hide');
						
						$.notify({icon:"add_alert",message:'The benefit has been saved successfully.'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})	
					}else{
						$.notify({icon:"add_alert",message:'Save error. Please try again.'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})	
					}
				}
			});
		}
	});
});
$("#lfiles").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

$('input[name="ltype"]').on('click',(e)=>{
	toggleFileTypes($('input[name="ltype"]:checked').val());
});
function toggleFileTypes(value)
{
	if(value=='fl'){
		$('#sh_file').show();
		$('#sh_lkyt').hide();
	}else if(value=='yt'){
		$('#sh_file').hide();
		$('#sh_lkyt').show();
	}
}
</script>