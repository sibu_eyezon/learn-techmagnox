<?php
	$studassgn = $this->Course_model->getStudentAssignmentSubmission($lra_id);
?>
<div class="card">
	<div class="card-body">
		<h4 class="card-title">Assigment Submissions
			<button type="button" onClick="getProgMenuContents('cassignments');" class="btn btn-sm btn-info pull-right"><i class="fa fa-list"></i> Assignments</button>
		</h4>
		
		<div class="material-datatables">
			<table class="table table-striped table-no-bordered table-hover" id="tbl_pre" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th width="5%">Sl#</th>
						<th width="20%">Name</th>
						<th width="25%">Assignment Submission</th>
						<th width="15%">Submission Date</th>
						<th width="10%">Full Marks</th>
						<th width="15%">Marks</th>
						<th width="10%">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$i=1;
						foreach($studassgn as $sa){
							echo '<tr>';
							echo '<td>'.$i.'</td>';
							echo '<td>'.trim($sa->stud_name).'</td>';
							echo '<td>'.((file_exists('./uploads/stud_assign_sub/'.trim($sa->file_name)))? '<a href="'.base_url('uploads/stud_assign_sub/'.trim($sa->file_name)).'" target="_blank">'.trim($sa->file_name).'</a>' : 'File not added.').'</td>';
							echo'<td>'.date('jS M Y',strtotime($sa->tdate)).'</td>';
							echo '<td>'.trim($sa->full_marks).'</td>';
							echo '<td id="marks_'.$i.'">';
							$marks = intval(trim($sa->marks));
							$fmarks = intval(trim($sa->full_marks));
							if($marks===0){
								echo '<input type="number" name="stud_marks_'.$i.'" id="stud_marks_'.$i.'" value="'.$marks.'" class="form-control"/>';
							}else{
								echo $marks;
							}
							echo'</td>';
							echo '<td id="action_'.$i.'">';
								if($marks===0){
									echo '<button class="btn btn-success btn-sm" id="btn_'.$i.'" onClick="saveStudMarks('.$i.', '.$sa->psm_sl.', '.$fmarks.')">Update</button>';
								}else{
									echo 'Checked';
								}
							echo'</td>';
							echo '</tr>';
						}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<script>
	function saveStudMarks(id, psm_sl, fmarks)
	{
		var marks = parseInt($('#stud_marks_'+id).val().trim());
		if(marks!=0){
			if(marks>0 && marks<=fmarks){
				$('#btn_'+id).attr('disabled', true);
				$.ajax({
					url: baseURL+'Teacher/updateStudMarks',
					type: 'POST',
					data: { marks: marks, psm_sl, psm_sl },
					success: (res)=>{
						if(res){
							$('#action_'+id).html("Checked");
							$('#marks_'+id).html(marks);
							$.notify({icon:"add_alert",message:'Marks has been updated'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})
						}else{
							$('#stud_marks_'+id).val(0);
							$('#btn_'+id).removeAttr('disabled');
							$.notify({icon:"add_alert",message:'Something went wrong. Please try again.'},{type:'warning',timer:3e3,placement:{from:'top',align:'right'}})
						}
					}
				});
				
			}else{
				$.notify({icon:"add_alert",message:'Marks cannot be greater than Full-marks.'},{type:'warning',timer:3e3,placement:{from:'top',align:'right'}})
			}
		}else{
			$.notify({icon:"add_alert",message:'Marks cannot be zero.'},{type:'warning',timer:3e3,placement:{from:'top',align:'right'}})
		}
	}
</script>