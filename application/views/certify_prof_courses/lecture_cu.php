<?php
	$obj = array('sl'=>0, 'course_sl'=>0, 'title'=>"", 'file_name'=>"", 'file_type'=>"", 'link'=>"");
	if($lec_id == 0){
		$lc[0] = $obj;
	}else{
		$lc = $this->Course_model->getCLectureByID($lec_id);
	}
?>
<form action="#" id="frmLesson" method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label class="text-dark">Lecture Title</label>
		<input type="text" name="title" id="title" class="form-control" value="<?php echo trim($lc[0]['title']); ?>"  required="true">
	</div>

	<input type="hidden" name="prog_id" id="prog_id" value="<?php echo $prog_id; ?>">
	<input type="hidden" name="lesson_id" id="lesson_id" value="<?php echo $lec_id; ?>">

	<div class="form-group">
		<label class="text-dark">Select Module</label>
		<select class="custom-select" name="section_id" id="section_id"  required="true">
			<?php foreach ($sections as $section){
				echo '<option value="'.$section->id.'" '.(($lc[0]['course_sl']==$section->id)? 'selected':'').'>'.trim($section->title).'</option>';
			}
			?>
		</select>
	</div>
	<div class="form-group">
		<div class="checkbox-radios">
			<div class="form-check form-check-inline">
			  <label class="form-check-label text-dark">
				<input class="form-check-input" type="radio" value="fl" name="ltype" id="fl"  required="true" <?php if($lc[0]['file_type']=='fl'){ echo 'checked'; } ?> required="true"> File*
				<span class="form-check-sign">
				  <span class="check"></span>
				</span>
			  </label>
			</div>
			<div class="form-check form-check-inline">
			  <label class="form-check-label text-dark">
				<input class="form-check-input" type="radio" value="yt" name="ltype" id="yt"  required="true" <?php if($lc[0]['file_type']=='yt'){ echo 'checked'; } ?> required="true"> Youtube*
				<span class="form-check-sign">
				  <span class="check"></span>
				</span>
			  </label>
			</div>
			<div id="type_error"></div>
		</div>
	</div>
	<div class="form-group mb-4" id="sh_file" style="display:none">
		<code>PDF/Word/Excel/MP4</code>
		<div class="custom-file">
			<input type="file" class="custom-file-input" name="lfiles" id="lfiles" accept="*.pdf, *.doc, *.docs, *.xls, *.xlsx, *.mp4" <?php if($lec_id==0){ echo 'required="true"'; } ?>>
			<label class="custom-file-label" for="lfiles">Choose lecture file</label>
		</div>
		<div class="progress" id="file-progress" style="display:none; margin-top:3rem;">
			<div class="progress-bar progress-bar-striped progress-bar-animated" id="file-progbra"></div>
		</div>
		<input type="hidden" name="lec_file" id="lec_file" value="<?= $lc[0]['file_name']; ?>"/>
	</div>
	<div class="form group" id="sh_lkyt" style="display:none">
		<textarea class="form-control w-100" cols="80" rows="5" name="llink" id="llink" placeholder="Copy-Paste your youtube shared link here [Click share button of your youtube video and copy the url like->https://youtu.be/evknSAkUIvs]" required="true"><?php echo trim($lc[0]['link']); ?></textarea>
	</div>
	<div class="text-right">
		<button class="btn btn-success" type="submit" name="button" id="btn-lec">Save</button>
	</div>
</form>
<script>
$(document).ready(()=>{
	toggleFileTypes("<?php echo $lc[0]['file_type']; ?>");
	$('#frmLesson').validate({
		errorPlacement: function(error, element) {
		  $(element).closest('.form-group').append(error);
		},
		submitHandler: function(form, e)
		{
			e.preventDefault();
			var frmData = new FormData($('#frmLesson')[0]);
			$.ajax({
				beforeSend: ()=>{
					$('#loading').css('display', 'block');
				},
				url: baseURL+'Certifyprogram/pcuLectures',
				type: 'POST',
				processData:false,
				contentType:false,
				data: frmData,
				success: (res)=>{
					$('#loading').css('display', 'none');
					if(res){
						$('#programModal').modal('hide');
						if(window.location.href.indexOf("/viewProgram/") > -1){
							getProgMenuContents('clectures');
						}else{
							getProgMenuContents('cmodule');
						}
						
						$.notify({icon:"add_alert",message:'The lecture has been saved successfully.'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})	
					}else{
						$.notify({icon:"add_alert",message:'Save error. Please try again.'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})	
					}
				},
				error: (errors)=>{
					$('#loading').css('display', 'none');
					console.log(errors);
				}
			});
		}
	});
});
$("#lfiles").on("change", function() {
	var allowedTypes = ['application/pdf', 'application/msword', 'application/vnd.ms-office', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'video/mp4'];
	var fileName = $(this).val().split("\\").pop();
	$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
	var file = this.files[0];
    var fileType = file.type;
	if(!allowedTypes.includes(fileType)){
		$.notify({icon:"add_alert",message:'Please select a valid file'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})	
		$("#lfiles").val('');
		return false;
	}else{
		var frmFile = new FormData($('#frmLesson')[0]);
		$("#file-progress").show();
		//frmFile.append('lecfile', file, file.name);
		$.ajax({
			url: baseURL+'Certifyprogram/uploadLecFile',
			type: 'POST',
			enctype: 'multipart/form-data',
			data: frmFile,
			contentType: false,
            cache: false,
            processData:false,
			beforeSend: function(){
                $("#file-progress").show();
				$('#file-progbra').css('width', '0%');
				$('#btn-lec').attr('disabled', true);
            },
			xhr: function () {
				var xhr = new window.XMLHttpRequest();
				xhr.upload.addEventListener("progress", function (evt) {
					if (evt.lengthComputable) {
						var percentComplete = evt.loaded / evt.total;
						console.log(percentComplete);
						$('#file-progbra').css({
							width: percentComplete * 100 + '%'
						});
						/*if (percentComplete === 1) {
							$('#file-progbra').addClass('hide');
						}*/
					}
				}, false);
				xhr.addEventListener("progress", function (evt) {
					if (evt.lengthComputable) {
						var percentComplete = evt.loaded / evt.total;
						console.log(percentComplete);
						$('#file-progbra').css({
							width: percentComplete * 100 + '%'
						});
					}
				}, false);
				return xhr;
			},
            error:function(){
				$.notify({icon:"add_alert",message:'File upload failed, please try again.'},{type:'warning',timer:3e3,placement:{from:'top',align:'right'}});
				$('#btn-lec').removeAttr('disabled');
            },
            success: function(resp){
                $('#btn-lec').removeAttr('disabled');
				var obj = JSON.parse(resp);
				if(obj['status']){
					$("#frmLesson #lfiles").val('');
					$("#frmLesson #lfiles").removeAttr('required');
                    $('#frmLesson #lec_file').val(obj['file_name']);
					$.notify({icon:"add_alert",message:'File has uploaded successfully!'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}});
                }else if(resp == 'err'){
					$("#file-progress").css('display', 'none');
                    $.notify({icon:"add_alert",message:'Please select a valid file to upload.'},{type:'warning',timer:3e3,placement:{from:'top',align:'right'}});
                }
            }
		});
	}
});

$('input[name="ltype"]').on('click',(e)=>{
	toggleFileTypes($('input[name="ltype"]:checked').val());
});
function toggleFileTypes(value)
{
	if(value=='fl'){
		$('#sh_file').show();
		$('#sh_lkyt').hide();
	}else if(value=='yt'){
		$('#sh_file').hide();
		$('#sh_lkyt').show();
	}
}
</script>