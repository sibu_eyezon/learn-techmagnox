<div class="col-md-12">
	<h4 class="mx-3">Lectures
		<button class="btn btn-outline-primary btn-rounded btn-sm pull-right" onClick="showAjaxModal('<?php echo site_url('Certifyprogram/lecture_pcu/'.$prog_id.'/'.$progtype); ?>', 'Add New Lecture')">Add Lecture</button>
	</h4>
	<hr style="border:2px solid #eee;">
	<div class="row">
		<?php
			$lectures = $this->Course_model->get_all_lectures_by_prog_id($prog_id);
			if(!empty($lectures)){
				foreach($lectures as $clow){
					$lra_id = $clow->sl;
					$ltype = trim($clow->file_type);
					$ltitle = trim($clow->title);
					if($ltype=='fl'){
						$src=base_url('assets/img/icons/lecture.png');
					}else if($ltype=='yt'){
						$src=base_url('assets/img/icons/youtube.png');
					}else if($ltype=='lk'){
						$src=base_url('assets/img/icons/link.png');
					}
		?>
		<div class="col-sm-3">
			<div class="card" style="box-shadow: 0 3px 15px 0 rgb(0 0 0 / 30%) !important;" id="lecture_<?php echo $lra_id; ?>">
				<div class="card-header">
					<h5 class="card-title"><?php echo $ltitle; ?></h5>
				</div>
				<div class="card-body text-center">
					<?php
						if($ltype=='fl'){
							echo '<a href="javascript:;" onClick="getLecFile('.$lra_id.')">';
						}else if($ltype=='yt'){
							echo '<a href="javascript:;" onClick="openYTModal('.$lra_id.')">';
						}else if($ltype=='lk'){
							echo '<a href="'.$clow->link.'" target="_blank">';
						}
					?>
					<img src="<?php echo $src; ?>" class="img-responsive" width="80"/>
					<?php echo '</a>'; ?>
					<h6>Added on: <?php echo date('jS M Y h:i a', strtotime($clow->add_date)); ?></h6>
					<h6>By <?php echo trim($clow->uname); ?></h6>
					<h6>Module: <?php echo trim($clow->pc_title); ?></h6>
				</div>
				<div class="card-footer">
					<?php if($clow->notify=='f'){
						echo '<a href="javascript:notifyStudsSubModule(`lectures`, '.$lra_id.', '.$clow->course_sl.');" class="btn btn-sm btn-primary">Notify All</a>';
					}else{
						echo '<span class="label label-success pull-left">Notified</span>';
					}
					?>
					<a href="javascript:showAjaxModal('<?php echo site_url('Certifyprogram/lecture_pcu/'.$prog_id.'/'.$progtype.'/'.$lra_id); ?>', 'Edit Lecture');" class="text-info pull-right"><i class="material-icons">edit</i></a>
					<a href="javascript:deleteSubModule(<?php echo $lra_id; ?>, '<?php echo $ltitle; ?>', 'lecture');" class="text-danger pull-right"><i class="material-icons">delete</i></a>
					
				</div>
			</div>
		</div>
		<?php
				}
			}
		?>
	</div>
</div>
<div class="modal fade" id="YTModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	  <div class="modal-content">
		<div class="modal-header">
		  <h4 class="modal-title">Lecture Youtube Video Preview</h4>
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			<i class="material-icons">clear</i>
		  </button>
		</div>
		<div class="modal-body" id="ytbody">
		  
		</div>
		<div class="modal-footer">
		  <button type="button" class="btn btn-danger btn-link" data-dismiss="modal">Close</button>
		</div>
	  </div>
	</div>
</div>

<script>
function openYTModal(lec_id)
{
	$('#loading').show();
	$.ajax({
		url:baseURL+'Teacher/getLecture/?clid='+lec_id,
		type: 'GET',
		success: (res)=>{
			$('#loading').hide();
			var obj = JSON.parse(res);
			if((obj[0].link).trim()!=""){
				$('#YTModal #ytbody').html('<div class="plyr__video-embed" id="player"><iframe height="350" src="'+obj[0].link+'?origin=https://plyr.io&amp;iv_load_policy=3&amp;modestbranding=1&amp;playsinline=1&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1" allowfullscreen allowtransparency allow="autoplay"></iframe></div>');
				const player = new Plyr('#player');
				$('#YTModal').modal('show');
			}else{
				$.notify({icon:"add_alert",message: "No Youtube Link Found."},{type:"danger",timer:3e3,placement:{from:'top',align:'right'}})
			}
		},
		error: (errors)=>{
			console.log(errors);
		}
	});
}
$('#YTModal').on('hide.bs.modal', function (e) {
	$('#YTModal #ytbody').html("");
}) 

function getLecFile(lec_id)
{
	$('#loading').show();
	$.ajax({
		url:baseURL+'Teacher/getLectureFile/?clid='+lec_id,
		type: 'GET',
		success: (res)=>{
			$('#loading').hide();
			if(res!='0'){
				var ftype = res.slice((Math.max(0, res.lastIndexOf(".")) || Infinity) + 1);
				if(ftype=='mp4'){
					
					$('#YTModal #ytbody').html('<video id="player" width="100%" height="350px" playsinline controls><source src="'+baseURL+'/uploads/courselra/'+res+'" type="video/mp4"></video>');
					const player = new Plyr('#player');
					$('#YTModal').modal('show');
				}else{
					$.get(baseURL+'uploads/courselra/'+res)
					.done(function() { 
						window.open(baseURL+'uploads/courselra/'+res, '_blank');
					}).fail(function() { 
						$.notify({
							icon:"add_alert",
							message: "Sorry! File is missing."},
							{type:"danger",
							timer:3e3,
							placement:{from:'top',align:'right'}
						})
					});
				}
			}else{
				$.notify({
					icon:"add_alert",
					message: "Sorry! File is missing."},
					{type:"danger",
					timer:3e3,
					placement:{from:'top',align:'right'}
				})
			}
		},
		error: (errors)=>{
			console.log(errors);
		}
	});
}
</script>