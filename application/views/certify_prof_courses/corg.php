<?php
	$institute = $this->Member->getProgramOrg($prog_id);
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/croppie/croppie.css" />
<style>
#upload-demo{
	width: 300px;
	height: 300px;
	padding-bottom:25px;
}
</style>
<div class="col-md-12">
	<h4 class="mx-3">Organisation
		<button class="btn btn-sm btn-primary pull-right" onClick="modalInstitute('add', <?php echo $prog_id; ?>, '')">Add Organisation</button>
	</h4>
	<hr style="border:2px solid #eee;">
	<div class="material-datatables">
		<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th width="5%">Sl.</th>
					<th width="90%">Details</th>
					<th width="5%">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php $i=1; foreach($institute as $irow){ ?>
					<tr>
						<td><?php echo $i; ?></td>
						<td style="word-break:break-all;">
							<?php 
								if($irow->logo!=""){
									if(file_exists('./assets/img/institute/'.$irow->logo)){
										echo '<img src="'.base_url('assets/img/institute/'.$irow->logo).'" class"img-thumbnail" style="width: 60px; float:left;margin-right: 5px;"';
									}
								}
								echo '<p>'.$irow->title; 
								if($irow->website!=""){
									echo '<br>'.$irow->website;
								}
								if($irow->contact_info!=""){
									echo '<br>'.$irow->contact_info;
								}
								echo '</p>';
							?>
						</td>
						<td class="td-actions text-right">
							<button type="button" title="Edit" rel="tooltip" class="btn btn-success" onClick="modalInstitute('edit', <?php echo $prog_id; ?>, <?php echo $irow->id; ?>)">
							  <i class="material-icons">edit</i>
							</button>
							<button type="button" title="Delete" rel="tooltip" class="btn btn-warning" onClick="deleteInstitute(<?php echo $irow->id; ?>)">
							  <i class="material-icons">delete</i>
							</button>
						  </td>
					</tr>
				<?php $i++; } ?>
			</tbody>
		</table>
	</div>
</div>
<div class="modal fade" id="addInstitute" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	  <div class="modal-content">
		<div class="modal-header">
		  <h4 class="modal-title" id="inst_title"></h4>
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			<i class="material-icons">clear</i>
		  </button>
		</div>
		<form id="frmInstitute" enctype="multipart/form-data">
			<div class="modal-body">
				<div class="form-group mb-5">
					<div class="custom-file">
						<input type="file" class="custom-file-input" name="avatar" id="avatar" accept="image/jpg, image/jpeg, image/png">
						<label class="custom-file-label text-dark" for="brochure">Upload Institute Logo</label>
					</div>
				</div>
				<div class="form-group">
					<input type="hidden" name="iprog_id" id="iprog_id" value="">
					<input type="hidden" name="org_id" id="org_id" value="">
					<label for="org_title">Institute Title*</label>
					<input type="text" name="org_title" id="org_title" class="form-control">
				</div>
				<div class="form-group">
					<label for="org_web">Website</label>
					<input type="url" name="org_web" id="org_web" class="form-control">
				</div>
				<div class="form-group">
					<label for="org_contact" class="">Contact</label><br>
					<textarea class="form-control" name="org_contact" id="org_contact" cols="80" rows="5" style="width:100%;"></textarea>
				</div>
			</div>
			<div class="modal-footer">
			  <input type="reset" style="visibility: hidden;"/>
			  <button type="button" id="orgSubmit" class="btn btn-link"></button>
			  <button type="button" class="btn btn-danger btn-link" data-dismiss="modal">Close</button>
			</div>
		</form>
	  </div>
	</div>
</div>
<script>
$(document).ready(function() {
	$('#org_contact').summernote();
});

function modalInstitute(func, prog_id, inst_id)
{
	if(func=='add'){
		$('#frmInstitute')[0].reset();
		$('#addInstitute #org_contact').summernote("code", "");
		$('#addInstitute #iprog_id').val(prog_id);
		$('#addInstitute #org_id').val(inst_id);
		$('#addInstitute #inst_title').html('Add Institute');
		$('#addInstitute #orgSubmit').html('Save');
		$('#addInstitute').modal('show');
	}else{
		$.ajax({
			url: baseURL+'Teacher/getOrgById/?org='+inst_id,
			type: 'GET',
			success: (data)=>{
				//$('#loading').fadeOut(1000);
				var res = JSON.parse(data);
				if(res[0].logo!=""){
					$('#addInstitute #wizardPicturePreview').attr('src', baseURL+'assets/img/institute/'+res[0].logo);
				}
				$('#addInstitute #iprog_id').val(prog_id);
				$('#addInstitute #org_id').val(inst_id);
				$('#addInstitute #org_title').val(res[0].title);
				$('#addInstitute #org_web').val(res[0].website);
				$('#addInstitute #org_contact').summernote("code", res[0].contact_info);
				$('#addInstitute #inst_title').html('Update Institute');
				$('#addInstitute #orgSubmit').html('Update');
				$('#addInstitute').modal('show');
			},
			error: (errors)=>{
				console.log(errors);
			}
		});
	}
}

function deleteInstitute(org_id)
{
	swal({
		title: 'Are you sure?',
		text: "You won't be able to revert this!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonClass: 'btn btn-success',
		cancelButtonClass: 'btn btn-danger',
		confirmButtonText: 'Yes, delete it!',
		buttonsStyling: false
	}).then(function() {
		//$('#loading').show();
		$.ajax({
			url: baseURL+'Teacher/delOrg/?org='+org_id,
			type: 'GET',
			success: (res)=>{
				//$('#loading').fadeOut(1000);
				if(res)
				{
					swal({
						title: 'Deleted!',
						text: 'The Institute has been deleted.',
						type: 'success',
						confirmButtonClass: "btn btn-success",
						buttonsStyling: false
					}).then((result)=>{
						getProgMenuContents('corg');
					})
				}
			}
		})
	}).catch(swal.noop)
}

$('#orgSubmit').on('click', ()=>{
	var title = $('#org_title').val().trim();
	if(title==''){
		$('#org_title').focus();
	}else{
		var frmIns = new FormData($('#frmInstitute')[0]);
		$.ajax({
			url: baseURL+'Teacher/cuORG',
			type: 'POST',
			data: frmIns,
			cache : false,
			processData: false,
			contentType: false,
			enctype: 'multipart/form-data',
			async: false,
			success: (data)=>{
				$('#frmInstitute')[0].reset();
				$('#addInstitute').modal('hide');
				if(data){
					swal({
						title: "Institute",
						text: "saved successfully",
						buttonsStyling: false,
						confirmButtonClass: "btn btn-success",
						type: "success"
					}).then((result)=>{
						getProgMenuContents('corg');
					}).catch(swal.noop)
				}else{
					swal({
						title: "Institute",
						text: "saved error",
						buttonsStyling: false,
						confirmButtonClass: "btn btn-warning",
						type: "warning"
					}).catch(swal.noop)
				}
			},
			error: (errors)=>{
				console.log(errors);
			}
		});
	}
})
</script>