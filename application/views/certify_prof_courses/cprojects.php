<div class="col-md-12">
	<h4 class="mx-3">Projects
		<button class="btn btn-sm btn-primary pull-right btn-link" onclick="addEditModal('project')">Add Project</button>
	</h4>
	<hr style="border:2px solid #eee;">
	<div class="material-datatables" id="project_list">
	</div>
</div>
<script src="<?php echo base_url('assets/js/faq-alumni-project.js'); ?>"></script>
<script>
	$(document).ready(function() {
		getProjectList(1);
	});
</script>