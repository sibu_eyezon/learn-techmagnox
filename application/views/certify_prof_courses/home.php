<div class="col-md-8">
	<div class="card shadow mt-2">
		<div class="card-header">
			<h4 class="card-title font-weight-bold text-dark">Configuration</h4>
		</div>
		<div class="card-body">
			<ul class="list-column">
				<li><?php echo trim($prog[0]->category).' ('.trim($prog[0]->ptype).')'; ?></li>
				<li><?php echo 'Admission start: '.date('jS M Y',strtotime($prog[0]->astart_date)); ?></li>
				<li><?php echo 'Date range: '.date('jS M Y',strtotime($prog[0]->start_date)).' - '.date('jS M Y',strtotime($prog[0]->end_date)); ?></li>
				<li><?php echo 'No. of seats: '.trim($prog[0]->total_seat); ?></li>
				<li><?php echo (trim($prog[0]->feetype)=='Paid')? 'Rs. '.$prog[0]->total_fee.' / Discount: '.$prog[0]->discount.' %' : 'Free'; ?></li>
				<li><?php echo 'Approval type: '.(($prog[0]->apply_type=='0')? 'All approval':'Selective approval'); ?></li>
				<li><?php echo 'Deadline: '.date('jS M Y',strtotime($prog[0]->aend_date)); ?></li>
				<li><?php echo 'Total Hours: '.trim($prog[0]->prog_hrs).' Hrs'; ?></li>
			</ul>
		</div>
	</div>
	<?php
		$section = $this->AutoProgramModel->get_section($prog_id);
		$csec = count($section);
		?>
	<div class="card shadow mt-2">
		<div class="card-header">
			<h4 class="card-title font-weight-bold text-dark"><?= $csec; ?> Modules</h4>
		</div>
		<div class="card-body">
			<?php
				if($csec != 0){
					echo '<ul class="list-group">';
					foreach($section as $secrow){
						echo '<li class="list-group-item">'.trim($secrow->title).'</li>';
					}
					echo '</ul>';
				}
			?>
		</div>
	</div>
</div>
<aside class="col-md-4">
	<div class="card">
		<div class="card-header card-header-info">
			<h4 class="card-title">Notices
				<a href="javascript:;" data-toggle="modal" data-target="#noticeM" class="pull-right btn-primary btn btn-sm btn-link"><i class="material-icons">add</i> Notice</a>
			</h4>
		</div>
		<div class="card-body">
			<div class="notice_ticker" style="height:40vh;">
				<ul class="list-group w-100" id="notc_details">
				
				</ul>
			</div>
		</div>
		<div class="card-footer">
			<a href="<?php echo base_url('Teacher/notices'); ?>" class="btn btn-info btn-sm pull-right">Know More</a>
		</div>
	</div>
</aside>
<script>
	$(document).ready(()=>{
		getProgramNotices();
	});
</script>