<?php
$section = $this->AutoProgramModel->get_section($prog_id);
//$csec = count($section);
?>
<h4 class="mx-3">Curriculum</h4>
<hr style="border:2px solid #eee;">
<div class="row justify-content-center">
    <div class="col-xl-12 mb-4 text-center mt-3">
<a href="javascript:;" class="btn btn-outline-primary btn-rounded btn-sm ml-1" onclick="showLargeModal('<?php echo site_url('Certifyprogram/section_cu/'.$prog_id.'/'.$progtype); ?>', 'Add New Module')"><i class="material-icons">add</i> Add Module</a>
<a href="javascript:;" class="btn btn-outline-primary btn-rounded btn-sm ml-1" onclick="showAjaxModal('<?php echo site_url('Certifyprogram/lecture_pcu/'.$prog_id.'/'.$progtype); ?>', 'Add New Lecture')"><i class="material-icons">add</i> Add Lecture</a>
<a href="javascript:;" class="btn btn-outline-primary btn-rounded btn-sm ml-1" onclick="showAjaxModal('<?php echo site_url('Certifyprogram/resource_pcu/'.$prog_id.'/'.$progtype); ?>', 'Add New Resource')"><i class="material-icons">add</i> Add Resource</a>
<a href="javascript:;" class="btn btn-outline-primary btn-rounded btn-sm ml-1" onclick="showAjaxModal('<?php echo site_url('Certifyprogram/assignment_pcu/'.$prog_id.'/'.$progtype); ?>', 'Add New Assignment')"><i class="material-icons">add</i> Add Assignment</a>
<!--<a href="javascript:;" class="btn btn-outline-primary btn-rounded btn-sm ml-1" onclick="showLargeModal('<?php //echo site_url('Certifyprogram/sort_section/'.$prog_id.'/'.$progtype); ?>', 'Sort Sections')"><i class="material-icons">sort</i> Sort Sections</a>-->
    </div>
	<div class="col-xl-12">
		<?php
			$i=1;
			foreach($section as $secrow){
				$sec_id = $secrow->id;
		?>
		<div class="card bg-dark text-white mt-0" id="sec_<?php echo $sec_id; ?>">
			<div class="card-header">
				<h4 class="card-title">Module <?php echo $i; ?>: <?php echo $secrow->title; ?>
					<a href="javascript:;" onClick="deleteSection(<?php echo $sec_id; ?>, '<?php echo $secrow->title; ?>')" class="btn btn-outline-primary btn-rounded btn-sm ml-1 pull-right"><i class="fa fa-trash"></i></a>
					<a href="javascript:;" onClick="showLargeModal('<?php echo site_url('Certifyprogram/section_cu/'.$prog_id.'/'.$progtype.'/'.$sec_id); ?>', 'Edit Module')" class="btn btn-outline-primary btn-rounded btn-sm ml-1 pull-right"><i class="fa fa-edit"></i></a> 
				</h4>
			</div>
			<div class="card-body">
				<?php
					$clra = $this->Course_model->getAllCourseSubDetails($sec_id);
					if(!empty($clra)){
					foreach($clra as $lra){
						$lra_type = trim($lra->type);
						$lra_id = $lra->sl;
						$lrtitle =trim($lra->title);
				?>
				<div class="card bg-info mt-0" id="<?php echo $lra_type.'_'.$lra_id; ?>">
					<div class="card-body">
						<h4 class="card-title"><?php echo ucfirst($lra_type).': '.$lrtitle; ?>
							<a href="javascript:;" onClick="deleteSubModule(<?php echo $lra_id; ?>, '<?php echo $lrtitle; ?>', '<?php echo $lra_type; ?>')" class="btn btn-outline-primary btn-rounded btn-sm ml-1 pull-right"><i class="fa fa-trash"></i></a>
							<a href="javascript:;" onClick="showAjaxModal('<?php echo site_url('Certifyprogram/'.$lra_type.'_pcu/'.$prog_id.'/'.$progtype.'/'.$lra_id); ?>', 'Edit <?php echo ucfirst($lra_type); ?>');" class="btn btn-outline-primary btn-rounded btn-sm ml-1 pull-right"><i class="fa fa-edit"></i></a> 
						</h4>
						<?php
							if($lra_type=='lecture'){
								$lfile = $this->Course_model->getLectureFile($lra_id);
								if($lfile[0]->file_type=='fl'){
									if(trim($lfile[0]->file_name)!=""){
										if(file_exists('./uploads/courselra/'.$lfile[0]->file_name)){
											echo '<a href="'.base_url('uploads/courselra/'.$lfile[0]->file_name).'" target="_blank"><i class="material-icons">download</i> Download File</a>';
										}
									}
								}else if($lfile[0]->file_type=='yt'){
									echo '<a href="'.$lfile[0]->link.'" target="_blank"></a>';
								}
							}else if($lra_type=='resource'){
								$crfiles = $this->Course_model->getCResourceFileByRID($lra_id);
								foreach($crfiles as $frow){
									$isrc = '';
									$cfid = $frow->sl;
									$rtype = trim($frow->type);
									$ftype = trim($frow->file_type);
									if($ftype=='pdf'){
										$isrc = 'fa-file-pdf-o';
									}else if($ftype=='doc' || $ftype=='docx'){
										$isrc = 'fa-file-word-o';
									}else if($ftype=='xls' || $ftype=='xlsx'){
										$isrc = 'fa-file-excel-o';
									}else if($ftype=='jpg'){
										$isrc = 'fa-file-image-o';
									}else if($ftype=='mp4'){
										$isrc = 'fa-file-video-o';
									}else{
										$isrc = 'fa-file';
									}
									if($rtype=='yt'){
										echo '<div class="alert alert-primary alert-with-icon mt-3" id="alert_'.$cfid.'" data-notify="container">
												<i class="fa fa-youtube-play" data-notify="icon"></i>
												<span data-notify="icon" class="now-ui-icons ui-1_bell-53"></span>
												<span data-notify="message"><a href="'.trim($frow->linkfile).'" target="_blank"> View Video</a></span>
											</div>';
									}else if($rtype=='lk'){
										echo '<div class="alert alert-primary alert-with-icon mt-3" id="alert_'.$cfid.'" data-notify="container">
												<i class="material-icons" data-notify="icon">link</i>
												<span data-notify="icon" class="now-ui-icons ui-1_bell-53"></span>
												<span data-notify="message"><a href="'.$frow->linkfile.'" target="_blank">View Link</a></span>
											</div>';
									}else if($rtype=='fl'){
										echo '<div class="alert alert-primary alert-with-icon mt-3" id="alert_'.$cfid.'" data-notify="container">
												<i class="fa '.$isrc.'" data-notify="icon"></i>
												<span data-notify="icon" class="now-ui-icons ui-1_bell-53"></span>
												<span data-notify="message"><a href="'.base_url('uploads/cresources/'.trim($frow->linkfile)).'" target="_blank"> Download</a></span>
											</div>';
									}
								}
							}else if($lra_type=='assignment'){
								$cafiles = $this->Course_model->getAssignmentFilesById($lra_id);
								foreach($cafiles as $frow){
									$isrc = '';
									$caid = $frow->sl;
									$rtype = trim($frow->file_type);
									$ftype = trim($frow->file_ext);
									if($ftype=='pdf'){
										$isrc = 'fa-file-pdf-o';
									}else if($ftype=='doc' || $ftype=='docx'){
										$isrc = 'fa-file-word-o';
									}else if($ftype=='xls' || $ftype=='xlsx'){
										$isrc = 'fa-file-excel-o';
									}else if($ftype=='jpg'){
										$isrc = 'fa-file-image-o';
									}else{
										$isrc = 'fa-file';
									}
									if(file_exists('./uploads/cassignments/'.$frow->file_name)){
										echo '<div class="alert alert-primary alert-with-icon mt-3" id="alert_'.$caid.'" data-notify="container">
											<i class="fa '.$isrc.'" data-notify="icon"></i>
											<button type="button" class="close" onClick="deleteAssgnFiles('.$caid.');" aria-label="Close">
											  <i class="material-icons">close</i>
											</button>
											<span data-notify="icon" class="now-ui-icons ui-1_bell-53"></span>
											<span data-notify="message"><a href="'.base_url().'uploads/cassignments/'.$frow->file_name.'" target="_blank"> Download</a></span>
										</div>';
									}
								}
							}
						?>
					</div>
				</div>
				<?php
						}
					}
				?>
			</div>
		</div>
		<?php $i++; } ?>
	</div>	
    
</div>
<script>
function showAjaxModal(url, header)
{
	jQuery('#programModal .modal-title').html(header);
	jQuery('#programModal .modal-body').html("");
	$.ajax({
		url: url,
		success: (response)=>{
			jQuery('#programModal .modal-body').html(response);
		}
	});
	$('#programModal').modal('show', {backdrop: 'true'});
}
function showLargeModal(url, header)
{
	jQuery('#programLgModal .modal-body').html("");
	jQuery('#programLgModal .modal-title').html(header);
	$.ajax({
		url: url,
		success: (response)=>{
			jQuery('#programLgModal .modal-body').html(response);
			jQuery('#programLgModal .modal-title').html(header);
			
		}
	});
	$('#programLgModal').modal('show', {backdrop: 'true'});
}

function deleteSection(id, title)
{
	swal({
		title: 'Are you sure?',
		text: "You want to remove section: "+title,
		type: 'warning',
		showCancelButton: true,
		confirmButtonClass: 'btn btn-success',
		cancelButtonClass: 'btn btn-danger',
		confirmButtonText: 'Yes, delete it!',
		buttonsStyling: false
	}).then(function() {
		$.ajax({
			url: baseURL+'Certifyprogram/module_delete/'+id,
			type: 'GET',
			success: (res)=>{
				if(res)
				{
					$.notify({icon:"add_alert",message:'The section has been deleted.'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})	
					$('#sec_'+id).remove();
				}else{
					$.notify({icon:"add_alert",message:'Something went wrong. Deletion error!!!.'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})
				}
			}
		})
	}).catch(swal.noop)
}
function deleteSubModule(id, title, ctype)
{
	swal({
		title: 'Are you sure?',
		text: "You want to remove "+ctype+": "+title,
		type: 'warning',
		showCancelButton: true,
		confirmButtonClass: 'btn btn-success',
		cancelButtonClass: 'btn btn-danger',
		confirmButtonText: 'Yes, delete it!',
		buttonsStyling: false
	}).then(function(opt) {
		if(opt.value){
			$.ajax({
				url: baseURL+'Certifyprogram/subModule_delete/'+id+'/'+ctype,
				type: 'GET',
				success: (res)=>{
					if(res)
					{
						$.notify({icon:"add_alert",message:'The '+ctype+' has been deleted.'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})	
						$('#'+ctype+'_'+id).remove();
					}else{
						$.notify({icon:"add_alert",message:'Something went wrong. Deletion error!!!.'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})
					}
				}
			})
		}
	}).catch(swal.noop)
}
</script>