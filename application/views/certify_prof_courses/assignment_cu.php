<?php
	$obj = array('sl'=>0, 'course_sl'=>0, 'title'=>"", 'tdate'=>date('Y-m-d\TH:i:s'), 'marks'=>0, 'deadline'=>date('Y-m-d\TH:i:s'), 'file_name'=>"");
	if($asgn_id == 0){
		$asgn[0] = (object)$obj;
	}else{
		//$asgn = $this->Course_model->getCAssignmentByID($asgn_id);
		$asgn = $this->Course_model->getCAssignmentDetailsById($asgn_id);
	}
?>
<form action="#" id="frmAssignment" method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label class="text-dark">Assignment Title</label>
		<input type="text" name="title" id="title" class="form-control" value="<?php echo trim($asgn[0]->title); ?>" required="true">
	</div>

	<input type="hidden" name="prog_id" id="prog_id" value="<?php echo $prog_id; ?>">
	<input type="hidden" name="asgn_id" id="asgn_id" value="<?php echo $asgn_id; ?>">

	<div class="form-group">
		<label class="text-dark">Select Module</label>
		<select class="custom-select" name="section_id" id="section_id" required="true">
			<?php foreach ($sections as $section){
				echo '<option value="'.$section->id.'" '.(($asgn[0]->course_sl==$section->id)? 'selected':'').'>'.trim($section->title).'</option>';
			}
			?>
		</select>
	</div>
	<div class="form-group">
		<label for="amarks" class="text-dark mb-0">Marks</label>
		<input type="number" class="form-control" name="amarks" id="amarks" required="true" min="1" max="100" number="true" value="<?php echo $asgn[0]->marks; ?>">
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<label for="sdate" class="text-dark">Start Date</label>
				<input type="datetime-local" class="form-control" name="sdate" id="sdate" required="true" value="<?php echo date('Y-m-d\TH:i:s',strtotime($asgn[0]->tdate)); ?>">
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label for="ddate" class="text-dark">Deadline</label>
				<input type="datetime-local" class="form-control" name="ddate" id="ddate" required="true" value="<?php echo date('Y-m-d\TH:i:s',strtotime($asgn[0]->deadline)); ?>">
			</div>
		</div>
	</div>
	<div class="form-group mb-4">
		<code>PDF/Word/EXCEL/PPT</code>
		<div class="custom-file">
			<input type="file" class="custom-file-input" name="lfiles" id="lfiles" <?php if($asgn_id==0){ echo 'required="true"'; } ?>>
			<label class="custom-file-label" for="lfiles">Choose file</label>
		</div>
		<div class="progress" id="file-progress" style="display:none; margin-top:3rem;">
			<div class="progress-bar progress-bar-striped progress-bar-animated" id="file-progbra"></div>
		</div>
		<input type="hidden" name="asgn_file" id="asgn_file" value="<?= $asgn[0]->file_name; ?>"/>
	</div>
	<div class="text-right">
		<button class = "btn btn-success" type="submit" name="button" id="btn-asgn">Save</button>
	</div>
</form>
<script>
$(document).ready(()=>{
	$('#frmAssignment').validate({
		errorPlacement: function(error, element) {
		  $(element).closest('.form-group').append(error);
		},
		submitHandler: function(form, e)
		{
			e.preventDefault();
			var frmData = new FormData($('#frmAssignment')[0]);
			$.ajax({
				beforeSend: ()=>{
					$('#loading').css('display', 'block');
				},
				url: baseURL+'Certifyprogram/pcuAssignments',
				type: 'POST',
				processData:false,
				contentType:false,
				data: frmData,
				success: (res)=>{
					$('#loading').css('display', 'none');
					if(res){
						$('#programModal').modal('hide');
						if(window.location.href.indexOf("/viewProgram/") > -1){
							getProgMenuContents('cassignments');
						}else{
							getProgMenuContents('cmodule');
						}
						$.notify({icon:"add_alert",message:'The assignment has been saved successfully.'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})	
					}else{
						$.notify({icon:"add_alert",message:'Save error. Please try again.'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})	
					}
				}
			});
		}
	});
	$("#lfiles").on("change", function() {
		var allowedTypes = ['application/pdf', 'application/msword', 'application/vnd.ms-office', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/ppt', 'application/pptx'];
		var fileName = $(this).val().split("\\").pop();
		$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
		var file = this.files[0];
		var fileType = file.type;
		if(!allowedTypes.includes(fileType)){
			$.notify({icon:"add_alert",message:'Please select a valid file'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})	
			$("#lfiles").val('');
			return false;
		}else{
			var frmFile = new FormData($('#frmAssignment')[0]);
			$("#file-progress").show();
			//frmFile.append('lecfile', file, file.name);
			$.ajax({
				url: baseURL+'Certifyprogram/uploadAsgnFile',
				type: 'POST',
				enctype: 'multipart/form-data',
				data: frmFile,
				contentType: false,
				cache: false,
				processData:false,
				beforeSend: function(){
					$("#file-progress").show();
					$('#file-progbra').css('width', '0%');
					$('#btn-asgn').attr('disabled', true);
				},
				xhr: function () {
					var xhr = new window.XMLHttpRequest();
					xhr.upload.addEventListener("progress", function (evt) {
						if (evt.lengthComputable) {
							var percentComplete = evt.loaded / evt.total;
							console.log(percentComplete);
							$('#file-progbra').css({
								width: percentComplete * 100 + '%'
							});
							/*if (percentComplete === 1) {
								$('#file-progbra').addClass('hide');
							}*/
						}
					}, false);
					xhr.addEventListener("progress", function (evt) {
						if (evt.lengthComputable) {
							var percentComplete = evt.loaded / evt.total;
							console.log(percentComplete);
							$('#file-progbra').css({
								width: percentComplete * 100 + '%'
							});
						}
					}, false);
					return xhr;
				},
				error:function(){
					$.notify({icon:"add_alert",message:'File upload failed, please try again.'},{type:'warning',timer:3e3,placement:{from:'top',align:'right'}});
					$('#btn-asgn').removeAttr('disabled');
				},
				success: function(resp){
					$('#btn-asgn').removeAttr('disabled');
					$("#lfiles").val('');
					var obj = JSON.parse(resp);
					if(obj['status']){
						$("#lfiles").removeAttr('required');
						$('#frmAssignment #asgn_file').val(obj['file_name']);
						$.notify({icon:"add_alert",message:'File has uploaded successfully!'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}});
					}else if(resp == 'err'){
						$("#file-progress").css('display', 'none');
						$.notify({icon:"add_alert",message:'Please select a valid file to upload.'},{type:'warning',timer:3e3,placement:{from:'top',align:'right'}});
					}
				}
			});
		}
	});
});
</script>