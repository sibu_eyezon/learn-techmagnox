<?php
	$data['sems'] = $this->Course_model->getAllSemestersByProgId($prog_id);
	$obj = array('id'=>0, 'prog_id'=>$prog_id, 'title'=>"", 'start_date'=>date('Y-m-d H:i:s'), 'end_date'=>date('Y-m-d H:i:s'), 'total_credit'=>"", 'syllabus'=>"", 'overview'=>"", 'importance'=>"", 'lec'=>"", 'tut'=>"", 'prac'=>"", 'type'=>"", 'c_code'=>"");
	if($m_id==0){
		$cd[0]=(object)$obj;
	}else{
		$cd = $this->Course_model->getCourseDetailsById($m_id);
	}
?>
<form action="#" method="POST" id="frmSection" enctype="multipart/form-data">
	<div class="row mb-3">
		<div class="col-sm-12">
			<div class="form-group">
			  <label for="title" class="text-dark">Course Title*</label>
			  <input type="text" class="form-control" id="title" name="title" value="<?php echo $cd[0]->title; ?>" required>
			  <input type="hidden" name="cid" id="cid" value="<?php echo $cd[0]->id; ?>">
			  <input type="hidden" name="prog_id" id="prog_id" value="<?php echo $cd[0]->prog_id; ?>">
			</div>
		</div>
	</div>
	<div class="row mb-3">
		<div class="col-sm-6">
			<div class="form-group">
				<select class="custom-select" name="crtype" id="crtype" data-title="Select type*" required>
					<option value="">Select type*</option>
					<option value="Compulsory" <?php if(trim($cd[0]->type)=='Compulsory'){ echo 'selected'; } ?>>Compulsory</option>
					<option value="Elective" <?php if(trim($cd[0]->type)=='Elective'){ echo 'selected'; } ?>>Elective</option>
					<option value="Selective" <?php if(trim($cd[0]->type)=='Selective'){ echo 'selected'; } ?>>Selective</option>
				</select>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label for="title" class="text-dark">Course Code*</label>
				<input type="text" class="form-control" id="ccode" name="ccode" placeholder="Auto-generated" value="<?php echo $cd[0]->c_code; ?>" readonly>
			</div>
		</div>
	</div>
	<div class="row mb-3">
		<div class="col-sm-6">
			<div class="form-group">
				 <label for="sdate" class="text-dark">Start Date*</label>
				 <input type="datetime-local" name="sdate" id="sdate" class="form-control" value="<?php echo strftime('%Y-%m-%dT%H:%M:%S', strtotime($cd[0]->start_date)); ?>">
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				 <label for="edate" class="">End Date*</label>
				 <input type="datetime-local" name="edate" id="edate" class="form-control" value="<?php echo strftime('%Y-%m-%dT%H:%M:%S', strtotime($cd[0]->end_date)); ?>">
			</div>
		</div>
	</div>
	<div class="row mb-3">
		<div class="col-sm-12">
			<div class="form-group">
				 <label for="sdate" class="text-dark">Details</label><br>
				 <textarea name="lec_details" id="lec_details" ><?= $cd[0]->overview; ?></textarea>
			</div>
		</div>
	</div>
	<div class="text-right">
		<button class = "btn btn-success" type="submit" name="button">Save</button>
	</div>
</form>
<script>
	$(document).ready(()=>{
		$('#lec_details').summernote('refresh');
		$('#frmSection').validate({
			errorPlacement: function(error, element) {
			  $(element).closest('.form-group').append(error);
			},
			submitHandler: function(form, e)
			{
				e.preventDefault();
				var frmData = new FormData($('#frmSection')[0]);
				$.ajax({
					url: baseURL+'Certifyprogram/cuSections',
					type: 'POST',
					processData:false,
					contentType:false,
					data: frmData,
					success: (res)=>{
						if(res){
							$('#programLgModal').modal('hide');
							getProgMenuContents('cmodule');
							$.notify({icon:"add_alert",message:'The module has been saved successfully.'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})	
						}else{
							$.notify({icon:"add_alert",message:'Save error. Please try again.'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})	
						}
					}
				});
			}
		});
	});
</script>