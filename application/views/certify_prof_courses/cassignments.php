<div class="col-md-12">
	<h4 class="mx-3">Assignments
		<button class="btn btn-outline-primary btn-rounded btn-sm pull-right" onClick="showAjaxModal('<?php echo site_url('Certifyprogram/assignment_pcu/'.$prog_id.'/'.$progtype); ?>', 'Add New Assignment')">Add Assignment</button>
	</h4>
	<hr style="border:2px solid #eee;">
	<div class="row">
		<?php
			$lectures = $this->Course_model->get_all_assignments_by_prog_id($prog_id);
			if(!empty($lectures)){
				foreach($lectures as $clow){
					$lra_id = $clow->sl;
					$ltitle = trim($clow->title);
					$cassnsubmit = $this->Course_model->get_all_assignment_submission($lra_id);
		?>
		<div class="col-sm-4">
			<div class="card" style="box-shadow: 0 3px 15px 0 rgb(0 0 0 / 30%) !important;" id="assignment_<?php echo $lra_id; ?>">
				<div class="card-header">
					<h5 class="card-title"><?php echo $ltitle; ?></h5>
				</div>
				<div class="card-body text-center">
					<?php
						$cafiles = $this->Course_model->getAssignmentFilesById($lra_id);
						foreach($cafiles as $frow){
							$isrc = '';
							$caid = $frow->sl;
							$rtype = trim($frow->file_type);
							$ftype = trim($frow->file_ext);
							if(file_exists('./uploads/cassignments/'.$frow->file_name)){
								echo '<a href="'.base_url('uploads/cassignments/'.$frow->file_name).'" target="_blank"><img src="'.base_url('assets/img/icons/assignment.png').'" class="img-responsive" width="80"/></a>';
							}
						}
					?>
					<h6>Added on: <?php echo date('jS M Y h:i a', strtotime($clow->add_date)); ?></h6>
					<h6>Deadline: <?php echo date('jS M Y h:i a', strtotime($clow->deadline)); ?></h6>
					<h6>Marks <?php echo trim($clow->marks); ?></h6>
					<h6>By <?php echo trim($clow->uname); ?></h6>
					<h6>Module: <?php echo trim($clow->pc_title); ?></h6>
					<?php
						if($cassnsubmit>0){
							echo '<button type="button" onClick="getAssigmentContents('.$lra_id.');" class="btn btn-info btn-sm">View Submission</button>';
						}
					?>
				</div>
				<div class="card-footer">
					<?php 
					$curdate = date('Ymd');
					$dline = date('Ymd',strtotime($clow->deadline));
					if($clow->notify=='f'){
						if($curdate<$dline){
							echo '<a href="javascript:notifyStudsSubModule(`assignments`, '.$lra_id.', '.$clow->course_sl.');" class="btn btn-sm btn-primary">Notify All</a>';
						}
					}else{ echo '<span class="label label-success pull-left">Notified</span>'; }
					?>
					<a href="javascript:showAjaxModal('<?php echo site_url('Certifyprogram/assignment_pcu/'.$prog_id.'/'.$progtype.'/'.$lra_id); ?>', 'Edit Assignment');" class="text-info pull-right"><i class="material-icons">edit</i></a>
					<a href="javascript:deleteSubModule(<?php echo $lra_id; ?>, '<?php echo $ltitle; ?>', 'assignment');" class="text-danger pull-right"><i class="material-icons">delete</i></a>
				</div>
			</div>
		</div>
		<?php
				}
			}
		?>
	</div>
</div>