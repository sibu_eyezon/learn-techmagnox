<?php
	$obj = array('sl'=>0, 'course_sl'=>0, 'title'=>"", 'type'=>"", 'linkfile'=>"");
	$obj1 = array('res_sl'=>0, 'type'=>"", 'linkfile'=>"");
	if($res_id == 0){
		$rc[0] = $obj;
		//$rcf[0] = (object)$obj1;
	}else{
		$rc = $this->Course_model->getCResourseDetailsById($res_id);
		/*$rc = $this->Course_model->getCResourceByID($res_id);
		$rcf = $this->Course_model->getCResourceFileByRID($res_id);
		if(empty($rcf)){
			$rcf[0] = (object)$obj1;
		}*/
	}
?>
<form action="#" id="frmResource" method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label class="text-dark">Resource Title</label>
		<input type="text" name="title" id="title" class="form-control" value="<?php echo trim($rc[0]['title']); ?>" required="true">
	</div>

	<input type="hidden" name="prog_id" id="prog_id" value="<?php echo $prog_id; ?>">
	<input type="hidden" name="res_id" id="res_id" value="<?php echo $res_id; ?>">

	<div class="form-group">
		<label class="text-dark">Select Module</label>
		<select class="custom-select" name="section_id" id="section_id" required="true">
			<?php foreach ($sections as $section){
				echo '<option value="'.$section->id.'" '.(($rc[0]['course_sl']==$section->id)? 'selected':'').'>'.trim($section->title).'</option>';
			}
			?>
		</select>
	</div>
	<div class="form-group">
		<div class="checkbox-radios">
			<div class="form-check form-check-inline">
			  <label class="form-check-label text-dark">
				<input class="form-check-input" type="radio" value="fl" name="ltype" id="fl"  required="true" <?php if($rc[0]['type']=='fl'){ echo 'checked'; } ?>> File/Video*
				<span class="form-check-sign">
				  <span class="check"></span>
				</span>
			  </label>
			</div>
			<div class="form-check form-check-inline">
			  <label class="form-check-label text-dark">
				<input class="form-check-input" type="radio" value="yt" name="ltype" id="yt"  required="true" <?php if($rc[0]['type']=='yt'){ echo 'checked'; } ?>> Youtube*
				<span class="form-check-sign">
				  <span class="check"></span>
				</span>
			  </label>
			</div>
			<div class="form-check form-check-inline">
			  <label class="form-check-label text-dark">
				<input class="form-check-input" type="radio" value="lk" name="ltype" id="lk"  required="true" <?php if($rc[0]['type']=='lk'){ echo 'checked'; } ?>> Link*
				<span class="form-check-sign">
				  <span class="check"></span>
				</span>
			  </label>
			</div>
			<div id="type_error"></div>
		</div>
	</div>
	<div class="form-group mb-4" id="sh_file" style="display:none">
		<code>PDF/Word/Excel/MP4</code>
		<div class="custom-file">
			<input type="file" class="custom-file-input" name="lfiles" id="lfiles" accept="*.pdf, *.doc, *.docs, *.xls, *.xlsx, *.mp4" <?php if($res_id==0){ echo 'required="true"'; } ?>>
			<label class="custom-file-label" for="lfiles">Choose resource file</label>
		</div>
		<div class="progress" id="file-progress" style="display:none; margin-top:3rem;">
			<div class="progress-bar progress-bar-striped progress-bar-animated" id="file-progbra"></div>
		</div>
		<input type="hidden" name="res_file" id="res_file" value="<?= $rc[0]['linkfile']; ?>"/>
	</div>
	<div class="form group" id="sh_yt" style="display:none">
		<textarea class="form-control w-100" cols="80" rows="5" name="yt_link" id="yt_link" placeholder="Copy-Paste your youtube shared link here[Click share button of your youtube video and copy the url like->https://youtu.be/evknSAkUIvs]"required="true"><?php echo trim($rc[0]['linkfile']); ?></textarea>
	</div>
	<div class="form group" id="sh_lk" style="display:none">
		<textarea class="form-control w-100" cols="80" rows="5" name="lk_link" id="lk_link" placeholder="Copy-Paste your link here"required="true"><?php echo trim($rc[0]['linkfile']); ?></textarea>
	</div>
	<div class="text-right">
		<button class="btn btn-success" type="submit" name="button" id="btn-res">Save</button>
	</div>
</form>
<script>
$(document).ready(()=>{
	toggleFileTypes("<?= $rc[0]['type']; ?>");
	$('#frmResource').validate({
		errorPlacement: function(error, element) {
		  $(element).closest('.form-group').append(error);
		},
		submitHandler: function(form, e)
		{
			e.preventDefault();
			var frmData = new FormData($('#frmResource')[0]);
			$.ajax({
				beforeSend: ()=>{
					$('#loading').css('display', 'block');
				},
				url: baseURL+'Certifyprogram/pcuResources',
				type: 'POST',
				processData:false,
				contentType:false,
				data: frmData,
				success: (res)=>{
					$('#loading').css('display', 'none');
					if(res){
						$('#programModal').modal('hide');
						if(window.location.href.indexOf("/viewProgram/") > -1){
							getProgMenuContents('cresources');
						}else{
							getProgMenuContents('cmodule');
						}
						$.notify({icon:"add_alert",message:'The resource has been saved successfully.'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})	
					}else{
						$.notify({icon:"add_alert",message:'Save error. Please try again.'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})	
					}
				}
			});
		}
	});
});
$("#lfiles").on("change", function() {
	var allowedTypes = ['application/pdf', 'application/msword', 'application/vnd.ms-office', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'video/mp4'];
	var fileName = $(this).val().split("\\").pop();
	$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
	var file = this.files[0];
    var fileType = file.type;
	if(!allowedTypes.includes(fileType)){
		$.notify({icon:"add_alert",message:'Please select a valid file'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})	
		$("#lfiles").val('');
		return false;
	}else{
		var frmFile = new FormData($('#frmResource')[0]);
		$("#file-progress").show();
		//frmFile.append('lecfile', file, file.name);
		$.ajax({
			url: baseURL+'Certifyprogram/uploadResFile',
			type: 'POST',
			enctype: 'multipart/form-data',
			data: frmFile,
			contentType: false,
            cache: false,
            processData:false,
			beforeSend: function(){
                $("#file-progress").show();
				$('#file-progbra').css('width', '0%');
				$('#btn-res').attr('disabled', true);
            },
			xhr: function () {
				var xhr = new window.XMLHttpRequest();
				xhr.upload.addEventListener("progress", function (evt) {
					if (evt.lengthComputable) {
						var percentComplete = evt.loaded / evt.total;
						console.log(percentComplete);
						$('#file-progbra').css({
							width: percentComplete * 100 + '%'
						});
						/*if (percentComplete === 1) {
							$('#file-progbra').addClass('hide');
						}*/
					}
				}, false);
				xhr.addEventListener("progress", function (evt) {
					if (evt.lengthComputable) {
						var percentComplete = evt.loaded / evt.total;
						console.log(percentComplete);
						$('#file-progbra').css({
							width: percentComplete * 100 + '%'
						});
					}
				}, false);
				return xhr;
			},
            error:function(){
				$.notify({icon:"add_alert",message:'File upload failed, please try again.'},{type:'warning',timer:3e3,placement:{from:'top',align:'right'}});
				$('#btn-res').removeAttr('disabled');
            },
            success: function(resp){
                $('#btn-res').removeAttr('disabled');
				$("#lfiles").val('');
				var obj = JSON.parse(resp);
				if(obj['status']){
					$("#lfiles").removeAttr('required');
                    $('#frmResource #res_file').val(obj['file_name']);
					$.notify({icon:"add_alert",message:'File has uploaded successfully!'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}});
                }else if(resp == 'err'){
					$("#file-progress").css('display', 'none');
                    $.notify({icon:"add_alert",message:'Please select a valid file to upload.'},{type:'warning',timer:3e3,placement:{from:'top',align:'right'}});
                }
            }
		});
	}
});

$('input[name="ltype"]').on('click',(e)=>{
	toggleFileTypes($('input[name="ltype"]:checked').val());
});
function toggleFileTypes(value)
{
	if(value=='fl'){
		$('#sh_file').show();
		$('#sh_lk').hide();
		$('#sh_yt').hide();
	}else if(value=='lk'){
		$('#sh_file').hide();
		$('#sh_lk').show();
		$('#sh_yt').hide();
	}else if(value=='yt'){
		$('#sh_file').hide();
		$('#sh_lk').hide();
		$('#sh_yt').show();
	}
}
</script>