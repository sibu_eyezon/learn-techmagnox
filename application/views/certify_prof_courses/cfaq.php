<div class="col-md-12">
	<h4 class="mx-3">FAQ
		<button class="btn btn-sm btn-primary pull-right btn-link" onclick="addEditModal('faq')">Add FAQ</button>
	</h4>
	<hr style="border:2px solid #eee;">
	<div class="material-datatables" id="faq_list">
	</div>
</div>
<script src="<?php echo base_url('assets/js/faq-alumni-project.js'); ?>"></script>
<script>
	$(document).ready(function() {
		getFaqList(1);
	});
</script>