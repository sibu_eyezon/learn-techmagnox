<div class="content">
  <div class="container">
	<div class="row">
	  <div class="col-md-12">
		<div class="card">
			<div class="card-header card-header-info card-header-text">
				<div class="card-text">
					<h4 class="card-title">Category List</h4>
				  </div>
				  <?php
					if($params=='list'){
						echo '<a href="'.base_url('Admin/category/add').'" class="btn btn-sm btn-info pull-right"><i class="material-icons">add</i> Add Category</a>';
					}else{
						echo '<a href="'.base_url('Admin/category/list').'" class="btn btn-sm btn-info pull-right"><i class="material-icons">list</i> Category List</a>';
					}
				  ?>
			</div>
			<div class="card-body">
				
				<div class="row">
					<?php foreach($clist as $catlt){ $id = $catlt->id; ?>
					<div class="col-sm-4 mb-2">
						<div class="card card-product">
							<div class="card-header card-header-image" data-header-animation="true">
								<a href="#pablo">
								  <img class="img" src="<?php echo base_url().'assets/img/category/'.$catlt->thumbnail; ?>" onerror="this.src='<?php echo base_url('assets/img/category/category-thumbnail.png'); ?>'">
								</a>
							  </div>
							<div class="card-body">
								<div class="card-actions text-center">
								  <a href="<?php echo base_url('Admin/category/edit/?id='.$id); ?>" class="btn btn-success btn-link" rel="tooltip" data-placement="bottom" title="Edit">
									<i class="material-icons">edit</i>
								  </a>
								  <button type="button" onClick="deleteCategory('Category', <?php echo $id; ?>);" class="btn btn-danger btn-link" rel="tooltip" data-placement="bottom" title="Remove">
									<i class="material-icons">delete</i>
								  </button>
								</div>
								<h4 class="card-title">
								  <a href="#pablo"><?php echo trim($catlt->name); ?></a>
								</h4>
								<div class="card-description">
									<?php
										$csublist = $this->Admin_model->getParentCategories($id);
										
									?>
									<ul class="list-group">
										<?php if(!empty($csublist)){ foreach($csublist as $csub){ 
											echo '<li class="list-group-item text-left">'.trim($csub->name);
											echo '<a href="javascript:deleteCategory(`Sub-category`,'.$csub->id.');" title="Delete" class="text-danger pull-right"><i class="material-icons">delete</i></a>';
											echo '<a href="'.base_url('Admin/category/edit/?id='.$csub->id).'" title="Edit" class="text-success pull-right"><i class="material-icons">edit</i></a>';
											
											echo '</li>';
										} } ?>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			
			</div>
		</div>
	  </div>
	</div>
  </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/plugins/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {
		$('#tbl').DataTable();
	});
	function deleteCategory(title, id)
	{
		Swal.fire({
		  title: 'Are yoi sure?',
		  text: "You want to remove the "+title,
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!',
		  preConfirm: () => {
			return fetch(`${baseURL}/Admin/removeCategory/${id}`)
			  .then(response => {
				if (!response.ok) {
				  throw new Error(response.statusText)
				}
				return response.json()
			  })
			  .catch(error => {
				Swal.showValidationMessage(
				  `Request failed: ${error}`
				)
			  })
		  },
		  allowOutsideClick: () => !Swal.isLoading()
		}).then((result) => {
		  if(result.value.status){
			  Swal.fire(
				  'Success',
				  title+' has been deleted.',
				  'success'
				)
				window.location.reload();
		  }else{
			  Swal.fire(
				  'Error',
				  'Deletion error. Please try again.',
				  'error'
				)
		  }
		})
	}
</script>