<div class="content">
  <div class="container">
	<div class="row">
	  <div class="col-md-12">
		<div class="card">
			<div class="card-header card-header-info card-header-text">
				<div class="card-text">
					<h4 class="card-title"><?php echo ucfirst($params); ?> Program List</h4>
				  </div>
			</div>
			<div class="card-body">
				<div class="material-datatables">
					<table class="table table-striped table-no-bordered table-hover dtinsstrm" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th width="5%">Sl</th>
								<th width="20%">Title</th>
								<th width="30%">Details</th>
								<th width="20%">By</th>
								<th width="15%">Date</th>
								<th width="10%">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(!empty($programs)){
								$i=1;
								foreach($programs as $prow){
									$prog_id = $prow->id;
									$title = trim($prow->title);
									$fee = trim($prow->feetype);
							?>
							<tr id="action_<?php echo $prog_id; ?>">
								<td><?php echo $i; ?></td>
								<td><?php echo $title; ?></td>
								<td>
								<?php
									  $dur = intval(trim($prow->duration));
									  echo 'Duration: '.$dur.' '.trim($prow->dtype).(($dur==1)? '':'s').'
									  <h6 class="text-left">'.(($fee=='Paid')? 'Rs '.$prow->total_fee : $fee).'</h6>
									  <h6 class="text-left">Start Date: '.date('jS M Y',strtotime($prow->start_date)).'</h6>';
								?>
								</td>
								<td><?php echo $prow->uname; ?></td>
								<td><?php echo date('jS M Y h:ia', strtotime($prow->date_added)); ?></td>
								<td>
								<a href="<?php echo base_url('Admin/viewProgramDetails/?id='.base64_encode($prog_id)); ?>" target="_blank" class="btn btn-sm btn-primary btn-block">View Details</a>
								<?php
									if($params=='pending'){
										echo '<input type="hidden" name="prog_'.$i.'" id="prog_'.$i.'" value="'.$prog_id.'"/>';
										echo '<input type="hidden" name="title_'.$i.'" id="title_'.$i.'" value="'.$title.'"/>';
										echo '<select name="action_'.$i.'" id="action_'.$i.'" onChange="changeStatus(this.value, '.$i.')" class="selectpicker" data-style="select-with-transition"><option value="">Select Action</option><option value="approved">Approve</option><option value="rejected">Reject</option><option value="review">Review</option></select>';
									}else{
										echo '<span class="label label-success">Approved</span>';
									}
								?>
								</td>
							</tr>
							<?php 
								$i++; }
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	  </div>
	</div>
  </div>
</div>
<script>
	function changeStatus(action, id)
	{
		if(action!=''){
			var prog_name=$('#title_'+id).val();
			var prog_id=$('#prog_'+id).val();
			if(action=='approved'){
				Swal.fire({
					title: 'Are you sure?',
					text: "You won't be able to revert this!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonText: 'Yes, approve it!',
					cancelButtonText: 'No, cancel!',
					reverseButtons: true,
					showLoaderOnConfirm: true,
					preConfirm: () => {
						return fetch(`${baseURL}Admin/changeProgramStatus/${action}/${prog_id}`)
						.then(response => {
							if (!response.ok) {
							  throw new Error(response.statusText)
							}
							return response.json()
						})
						.catch(error => {
							Swal.showValidationMessage(
							  `Request failed: ${error}`
							)
						})
					},
					allowOutsideClick: () => !Swal.isLoading()
				}).then((result) => {
					var obj = JSON.parse(result.value);
					if(obj['status']){
						$.notify({icon:"add_alert",message:prog_name+obj['msg']},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})
					}else{
						$.notify({icon:"add_alert",message:prog_name+obj['msg']},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})
					}
				})
			}else{
				Swal.fire({
					title: 'Enter your reason:',
					input: 'text',
					inputAttributes: {
						autocapitalize: 'off'
					},
					type: 'warning',
					showCancelButton: true,
					confirmButtonText: 'Yes, '+action+' it!',
					cancelButtonText: 'No, cancel!',
					reverseButtons: true,
					showLoaderOnConfirm: true,
					preConfirm: (message) => {
						return fetch(`${baseURL}Admin/changeProgramStatus/${action}/${prog_id}/${message}`)
						.then(response => {
							if (!response.ok) {
							  throw new Error(response.statusText)
							}
							return response.json()
						})
						.catch(error => {
							Swal.showValidationMessage(
							  `Request failed: ${error}`
							)
						})
					},
					allowOutsideClick: () => !Swal.isLoading()
				}).then((result) => {
					var obj = JSON.parse(result.value);
					if(obj['status']){
						$.notify({icon:"add_alert",message:prog_name+obj['msg']},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})
					}else{
						$.notify({icon:"add_alert",message:prog_name+obj['msg']},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})
					}
				})
			}
		}
	}
</script>