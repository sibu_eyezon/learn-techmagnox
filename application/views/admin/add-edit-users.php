<div class="content">
  <div class="container">
	<div class="row">
	  <div class="col-md-8 mx-auto">
		<?php
			if($this->session->flashdata('error')!=""){
				echo '<div class="alert alert-warning">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
                    <span>'.$this->session->flashdata('error').'</span>
                  </div>';
			}
		?>
		<div class="card">
			<div class="card-header card-header-info card-header-text">
				<div class="card-text">
					<h4 class="card-title"><?php echo ($user[0]->id==0)? 'Add '.ucfirst($params):'Update '.ucfirst($params); ?></h4>
				  </div>
				  <a href="<?php echo base_url('Admin/users/'.$params); ?>" class="btn btn-sm btn-info pull-right"><i class="material-icons">list</i> <?php echo ucfirst($params); ?> List</a>
			</div>
			<div class="card-body">
				<form action="<?php echo base_url('Admin/cuTeacher'); ?>" method="POST" id="frnUsers">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="fname">Firstname <span class="text-danger">*</span></label>
								<input type="text" name="fname" id="fname" class="form-control" value="<?php echo $user[0]->first_name; ?>"/>
								<input type="hidden" name="user_type" id="user_type" value="<?php echo trim($user[0]->user_type); ?>"/>
								<input type="hidden" name="user_id" id="user_id" value="<?php echo $user[0]->id; ?>"/>
							  </div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="lname">Lastname <span class="text-danger">*</span></label>
								<input type="text" name="lname" id="lname" class="form-control" value="<?php echo $user[0]->last_name; ?>"/>
							  </div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="email">Email <span class="text-danger">*</span></label>
								<input type="text" name="email" id="email" class="form-control" value="<?php echo $user[0]->email; ?>"/>
							  </div>	
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="phone">Phone <span class="text-danger">*</span></label>
								<input type="text" name="phone" id="phone" class="form-control" value="<?php echo $user[0]->phone; ?>"/>
							  </div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="org">Organisation <span class="text-danger">*</span></label>
								<input type="text" name="org" id="org" class="form-control" value="<?php echo $user[0]->organization; ?>"/>
							  </div>
							  
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="designation">Designation <span class="text-danger">*</span></label>
								<input type="text" name="designation" id="designation" class="form-control" value="<?php echo $user[0]->designation; ?>"/>
							  </div>
							  
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="weblink">About</label>
								<textarea name="about_me" id="about_me" class="form-control" rows="5" cols="80"><?php echo $user[0]->about_me; ?></textarea>
							  </div>
						</div>
					</div>
					<?php if($user[0]->id==0){ ?>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="passwd">Password <span class="text-danger">*</span></label>
								<input type="password" name="passwd" id="passwd" class="form-control" autocomplete="off"/>
							  </div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="repasswd">Retype Password <span class="text-danger">*</span></label>
								<input type="password" name="repasswd" id="repasswd" class="form-control" autocomplete="off"/>
							  </div>
						</div>
					</div>
					<?php } ?>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<input type="reset" style="display:none"/>
								<button type="submit" class="btn btn-success pull-left" id="btn_login"><?php echo ($user[0]->id==0)? 'Save':'Update'; ?></button>
							  </div>  
						</div>
					</div>
				</form>
			</div>
		</div>
	  </div>
	</div>
  </div>
</div>
<script>
	$(document).ready(function() {
		$("form#frnUsers").validate({
			errorPlacement: function(error, element) {
			  $(element).closest('.form-group').append(error);
			},
			rules: {
				fname: {
					required: true,
					minlength: 3
				},
				lname: {
					required: true,
					minlength: 3
				},
				email: {
					required: true,
					email: true
				},
				phone: {
					required: true,
					digits: true,
					minlength: 10,
					maxlength: 10
				},
				org: {
					required: true,
					minlength: 5
				},
				passwd: {
					required: true,
					minlength: 6
				},
				repasswd: {
					required: true,
					equalTo: '#passwd'
				}
			},
			messages: {
				fname: {
					required: 'Must enter firstname.',
					minlength: 'The firstname must be minimum 3characters.'
				},
				lname: {
					required: 'Must enter lastname.',
					minlength: 'The lastname must be minimum 3characters.'
				},
				email: {
					required: 'Must enter email address.',
					email: 'Enter valid email address'
				},
				phone: {
					required: 'Must enter phone number.',
					digits: 'Must enter digits only.',
					minlength: 'Phone must be 10 digits.',
					maxlength: 'Phone must be 10 digits.'
				},
				org: {
					required: 'Must enter organization.',
					minlength: 'The organization must be minimum 5characters.'
				},
				passwd: {
					required: 'Must enter password.',
					minlength: "The password's length mus be minimum 6."
				},
				repasswd: {
					required: 'Must confirm the password.',
					confirm: 'Password not matched.'
				}
			},
		});
	});
</script>