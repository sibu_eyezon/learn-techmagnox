<div class="content">
	<div class="container-fluid">
        <div class="card bg-light">
            <div class="card-header card-header-info card-header-text">
                <div class="card-text" id="page_title">Blog List</div>
            </div>
            <div class="card-body">
                <div class="align-left mb-2">
                    <a class="btn btn-primary btn-sm" href="<?php echo base_url('Admin/add_edit_blog_form/?page='.base64_encode($page_number));?>">
                        <span class="material-icons">add_circle_outline</span>Add New Blogs
                    </a>
                </div>
                <div class="table text-dark" id="blog_list">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col" class="text-center font-weight-bold">Sl/No.</th>
                                        <th scope="col" class="text-center font-weight-bold">Banner</th>
                                        <th scope="col" class="text-center font-weight-bold">Title</th>
                                        <th scope="col" class="text-center font-weight-bold">Body</th>
                                        <th scope="col" class="text-center font-weight-bold">Status</th>
                                        <th scope="col" class="text-center font-weight-bold">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $num = $slNo;
                                    if(!empty($result)):
                                        foreach($result as $row):
                                ?>
                                    <tr>
                                        <th class="text-center"><?php echo $num;?></th>
                                        <td class="text-center"><img src="<?php echo base_url($row->banner);?>" onerror="src='<?php echo base_url('assets/img/blog_banner_placeholder.png');?>'" style="width:100px"></td>
                                        <td class="text-center"><?php echo $row->title; ?></td>
                                        <td class="text-justify"><?php echo $row->body; ?></td>
                                        <td class="text-center"><?php echo ($row->status == 't')?"Active":"Incative";?></td>
                                        <td class="text-center">
                                            <a type="button" class="btn btn-primary btn-sm" title="Edit" href="<?php echo base_url('Admin/add_edit_blog_form/?id='.base64_encode($row->id).'&page='.base64_encode($page_number));?>">
                                                <i class="material-icons">edit</i>
                                            </a>
                                            <a type="button" class="btn btn-warning btn-sm" title="status" href="<?php echo base_url('Admin/change_blog_status/?state='.$row->status.'&id='.base64_encode($row->id).'&page='.base64_encode($page_number));?>">
                                                <i class="material-icons">published_with_changes</i>
                                            </a>
                                            <a type="button" class="btn btn-danger btn-sm" title="Delete" href="<?php echo base_url('Admin/delete_blog/?id='.base64_encode($row->id).'&page='.base64_encode($page_number));?>">
                                                <i class="material-icons">delete</i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php
                                            $num++;
                                        endforeach;
                                    endif;
                                ?>
                                </tbody>
                            </table>    
                        </div>
                    </div>  
                    <div class="row">
                        <div class="col-md-5 col-sm-12"></div>
                        <div class="col-sm-12 col-md-7 ml-auto">
                        <?php if(!empty($result)):?>
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                <?php
                                    if($page_number > 1):
                                        echo '<li class="page-item"><a class="page-link" href="'.base_url('Admin/blog_page/?page='.base64_encode(($page_number + 1))).'">Previous</a></li>';
                                    else:
                                        echo '<li class="page-item "><button class="page-link" disabled>Previous</button></li>';
                                    endif;
                                    if($page_number == 1):
                                        for($i=$page_number;$i<=($page_number+2);$i++):
                                            if($i == $page_number):
                                                echo '<li class="page-item active"><a class="page-link" href="'.base_url('Admin/blog_page/?page='.base64_decode($i)).'">'.$i.'</a></li>';
                                            elseif($i <= $last_page):
                                                echo '<li class="page-item"><a class="page-link" href="'.base_url('Admin/blog_page/?page='.base64_decode($i)).'">'.$i.'</a></li>';
                                            endif;
                                        endfor;
                                    elseif($page_number != 1 && $page_number < $last_page):
                                        for($i=($page_number - 1);$i<=($page_number + 1);$i++):
                                            if($i == $page_number):
                                                echo '<li class="page-item active"><a class="page-link" href="'.base_url('Admin/blog_page/?page='.base64_decode($i)).'">'.$i.'</a></li>';
                                            else:
                                                echo '<li class="page-item"><a class="page-link" href="'.base_url('Admin/blog_page/?page='.base64_decode($i)).'">'.$i.'</a></li>';
                                            endif;
                                        endfor;
                                    elseif($page_number != 1 && $page_number == $last_page):
                                        for($i=($last_page - 2);$i<=$last_page;$i++):
                                            if($i == $page_number):
                                                echo '<li class="page-item active"><a class="page-link" href="'.base_url('Admin/blog_page/?page='.base64_decode($i)).'">'.$i.'</a></li>';
                                            elseif($i != 0):
                                                echo '<li class="page-item"><a class="page-link" href="'.base_url('Admin/blog_page/?page='.base64_decode($i)).'">'.$i.'</a></li>';
                                            endif;
                                        endfor;
                                    endif;                                    

                                    if($page_number < $last_page ):
                                        echo '<li class="page-item"><a class="page-link" href="'.base_url('Admin/blog_page/?page='.base64_encode(($page_number + 1))).'">Next</a></li>';
                                    else:
                                        echo '<li class="page-item "><button class="page-link" disabled>Next</button></li>';
                                    endif;
                                ?>
                                </ul>
                            </nav>
                        <?php endif;?>
                        </div>
                    </div>             
                </div>
            </div>
        </div>
    </div>
</div>