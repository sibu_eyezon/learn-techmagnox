<div class="content">
  <div class="container">
	<div class="row">
	  <div class="col-md-12">
		<div class="card">
			<div class="card-header card-header-info card-header-text">
				<div class="card-text">
					<h4 class="card-title">Email Template List</h4>
				  </div>
				  <a href="<?= base_url('Admin/add_newsletter'); ?>" class="btn btn-primary btn-sm pull-right">Add Email Template</a>
			</div>
			<div class="card-body">
				
				<div class="row">
					<div class="col-md-12">
						<div class="material-datatables">
							<table id="tbl" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
							  <thead>
								<tr>
								  <th width="5%">Sl</th>
								  <th width="20%">Subject</th>
								  <th width="40%">Message</th>
								  <th width="10%">Action</th>
								</tr>
							  </thead>
							  <tbody>
								<?php $i=1; foreach($result as $mrow){ ?>
								<tr id="comm_<?= $mrow->id; ?>">
								  <td><?= $i; ?></td>
								  <td><?= trim($mrow->title); ?></td>
								  <td><div style="height: 20vh; overflow-y:auto;"><?= trim($mrow->details); ?></div</td>
								  <td>
									<?php
										echo '<a href="'.base_url('Admin/add_newsletter/?id='.$mrow->id).'" class="btn btn-primary btn-sm">Edit</a>';
										echo '<a href="javascript:publishNewsletter('.$mrow->id.');" class="btn btn-success btn-sm">Send Mail</a>';
										echo '<a href="javascript:removeNewsletter('.$mrow->id.');" class="btn btn-danger btn-sm">Delete</a>';
									?>
								  </td>
								</tr>
								<?php $i++; } ?>
							  </tbody>
							</table>
						</div>
					</div>
				</div>
			
			</div>
		</div>
	  </div>
	</div>
  </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/plugins/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {
		$('#tbl').DataTable();
	});
	function removeNewsletter(id)
	{
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You want to remove the Email Template",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!',
		  preConfirm: () => {
			return fetch(`${baseURL}/Admin/removeNewsletter/${id}`)
			  .then(response => {
				if (!response.ok) {
				  throw new Error(response.statusText)
				}
				return response.json()
			  })
			  .catch(error => {
				Swal.showValidationMessage(
				  `Request failed: ${error}`
				)
			  })
		  },
		  allowOutsideClick: () => !Swal.isLoading()
		}).then((result) => {
		  if(result.value){
			  Swal.fire(
				  'Success',
				  'Email Template has been deleted.',
				  'success'
				)
				$('#comm_'+id).remove();
				//window.location.reload();
		  }else{
			  Swal.fire(
				  'Error',
				  'Deletion error. Please try again.',
				  'error'
				)
		  }
		});
	}
	function publishNewsletter(id)
	{
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You want to publish the Email Template",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, publish it!',
		  preConfirm: () => {
			return fetch(`${baseURL}/Admin/publishNewsletter/${id}`)
			  .then(response => {
				if (!response.ok) {
				  throw new Error(response.statusText)
				}
				return response.json()
			  })
			  .catch(error => {
				Swal.showValidationMessage(
				  `Request failed: ${error}`
				)
			  })
		  },
		  allowOutsideClick: () => !Swal.isLoading()
		}).then((result) => {
		  if(result.value){
			  Swal.fire(
				  'Success',
				  'Email Template has been published.',
				  'success'
				)
				window.location.reload();
		  }else{
			  Swal.fire(
				  'Error',
				  'Publishion error. Please try again.',
				  'error'
				)
		  }
		});
	}
</script>