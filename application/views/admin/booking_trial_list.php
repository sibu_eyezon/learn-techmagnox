<div class="content">
  <div class="container">
	<div class="row">
	  <div class="col-md-12">
		<div class="card">
			<div class="card-header card-header-info card-header-text">
				<div class="card-text">
					<h4 class="card-title">Booking Trial Class List</h4>
				  </div>
			</div>
			<div class="card-body">
				
				<div class="row">
					<div class="col-md-12">
						<div class="material-datatables">
							<table id="tbl" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
							  <thead>
								<tr>
								  <th width="5%">Sl</th>
								  <th width="20%">Child's Name</th>
								  <th width="25%">Gaurdian Contact</th>
								  <th width="40%">Details</th>
								  <th width="10%">Action</th>
								</tr>
							  </thead>
							  <tbody>
								<?php $i=1; foreach($result as $mrow){ ?>
								<tr id="comm_<?= $mrow->id; ?>">
								  <td><?php echo $i; ?></td>
								  <td><?php echo trim($mrow->first_name.' '.$mrow->last_name); ?></td>
								  <td><?= trim($mrow->gaurdian_name).'<br>'.trim($mrow->email).'<br>'.trim($mrow->phone); ?></td>
								  <td><?= trim($mrow->subject).'<br>'.trim($mrow->message); ?></td>
								  <td>
									<a href="javascript:removeBooking(<?= $mrow->id; ?>);" class="btn btn-danger btn-md">Delete</a>
								  </td>
								</tr>
								<?php $i++; } ?>
							  </tbody>
							</table>
						</div>
					</div>
				</div>
			
			</div>
		</div>
	  </div>
	</div>
  </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/plugins/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {
		$('#tbl').DataTable();
	});
	function removeBooking(id)
	{
		Swal.fire({
		  title: 'Are yoi sure?',
		  text: "You want to remove the Booking",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!',
		  preConfirm: () => {
			return fetch(`${baseURL}/Admin/removeBooking/${id}`)
			  .then(response => {
				if (!response.ok) {
				  throw new Error(response.statusText)
				}
				return response.json()
			  })
			  .catch(error => {
				Swal.showValidationMessage(
				  `Request failed: ${error}`
				)
			  })
		  },
		  allowOutsideClick: () => !Swal.isLoading()
		}).then((result) => {
		  if(result.value){
			  Swal.fire(
				  'Success',
				  'Booking has been deleted.',
				  'success'
				)
				$('#comm_'+id).remove();
				//window.location.reload();
		  }else{
			  Swal.fire(
				  'Error',
				  'Deletion error. Please try again.',
				  'error'
				)
		  }
		});
	}
	/*function deleteCategory(title, id)
	{
		Swal.fire({
		  title: 'Are yoi sure?',
		  text: "You want to remove the "+title,
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!',
		  preConfirm: () => {
			return fetch(`${baseURL}/Admin/removeCategory/${id}`)
			  .then(response => {
				if (!response.ok) {
				  throw new Error(response.statusText)
				}
				return response.json()
			  })
			  .catch(error => {
				Swal.showValidationMessage(
				  `Request failed: ${error}`
				)
			  })
		  },
		  allowOutsideClick: () => !Swal.isLoading()
		}).then((result) => {
		  if(result.value.status){
			  Swal.fire(
				  'Success',
				  title+' has been deleted.',
				  'success'
				)
				window.location.reload();
		  }else{
			  Swal.fire(
				  'Error',
				  'Deletion error. Please try again.',
				  'error'
				)
		  }
		})
	}*/
</script>