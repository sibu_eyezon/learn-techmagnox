<div class="content">
	<div class="container-fluid">
        <div class="card bg-light">
            <div class="card-header card-header-info card-header-text">
                <div class="card-text"><?php echo $title;?></div>
                <a class="btn btn-primary btn-sm pull-right" href="<?php echo base_url('Admin/blog_page/?page='.base64_encode($page));?>">
                    <i class="material-icons">list</i> Back to Blog List
                </a>
            </div>
            <div class="card-body">
                <form class="form-horizontal" action="<?php echo base_url('Admin/add_edit_blog')?>" method="post" enctype="multipart/form-data" onsubmit="return blogFormValidation()">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <img class="img-responsive" id="img_view" src="<?php echo base_url($result[0]->banner);?>" onerror="src='<?php echo base_url('assets/img/blog_banner_placeholder.png');?>'" style="width:300px;">
                            <div class="form-group">
                                <label for="banner">Banner Upload</label>
                                <input type="file" class="form-control-file" id="banner" name="banner">
                                <span class="h6 text-danger" id="error_banner"></span>
                            </div>
                            <input type="hidden" id="abspath" name="abspath" value="<?php echo (!empty($result[0]->banner))?$result[0]->banner : null;?>">
                            <input type="hidden" name="id" value="<?php echo (!empty($id))?base64_encode($id):0;?>">
                            <input type="hidden" name="page" value="<?php echo (!empty($page))?base64_encode($page):null;?>">
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="title">Blog Title</label>
                                <input type="text" class="form-control" id="title" name="title" value="<?php echo$result[0]->title;?>">
                                <span class="h6 text-danger" id="error_title"></span>
                            </div>
                            <div class="form-group">
                                <select class="form-control selectpicker" name="categories[]" id="categories" multiple="multiple" data-title="Select Blog Categories" data-style="select-with-transition" data-live-search="true">
                                    <?php
                                        if(!empty($categories)){
                                            foreach($categories as $row){
                                                if(!empty($result[0]->categories) && in_array($row->id,json_decode($result[0]->categories))){
                                                    echo '<option value="'.$row->id.'" selected>'.$row->name.'</option>';
                                                }else{
                                                    echo '<option value="'.$row->id.'">'.$row->name.'</option>';
                                                }
                                            }
                                        }
                                    ?>
                                </select>
                                <span class="h6 text-danger" id="error_categories"></span>
                            </div>
                            <div class="form-group">
                                <select class="form-control selectpicker" name="skills[]" id="skills" multiple="multiple" data-title="Select Blog Skills" data-style="select-with-transition" data-live-search="true">
                                    <?php
                                        if(!empty($skills)){
                                            foreach($skills as $row){
                                                if(!empty($result[0]->skills) && in_array($row->id,json_decode($result[0]->skills))){
                                                    echo '<option value="'.$row->id.'" selected>'.$row->name.'</option>';
                                                }else{
                                                    echo '<option value="'.$row->id.'">'.$row->name.'</option>';
                                                }
                                            }
                                        }
                                    ?>
                                </select>
                                <span class="h6 text-danger" id="error_skills"></span>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="body">Blog Body</label>
                                <textarea class="ckeditor form-control" id="body" name="body" rows="3"><?php echo$result[0]->body;?></textarea>
                                <span class="h6 text-danger" id="error_body"></span>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <div class="m-auto">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('#banner').change(function(e){
			$("#img_view").attr("src",URL.createObjectURL(e.target.files[0]))
		});
    });
    function blogFormValidation(){
        let isBanner = false;
        let isTitle  = false;
        let isCategory = false;
        let isSkill = false;
        let isBody = false;
        if($('#banner')[0].files[0] || $('#abspath').val()){
            $('#error_banner').html('');
            isBanner = true;
        }else{
            $('#error_banner').html('Banner is required');
        }
        
        if($('#title').val()){
            $('#error_title').html('');
            isTitle = true;
        }else{
            $('#error_title').html('Title is required');
        }

        if($('#body').val()){
            $('#error_body').html('');
            isBody = true;
        }else{
            $('#error_body').html('Body is required');
        }

        if($('#categories').val().length){
            $('#error_categories').html('');
            isCategory = true;
        }else{
            $('#error_categories').html('Categories are required');
        }

        if($('#skills').val().length){
            $('#error_skills').html('');
            isSkill = true;
        }else{
            $('#error_skills').html('Skills are required');
        }

        if(isBanner && isTitle && isCategory && isSkill && isBody){
            return true;
        }else{
            return false;
        }
    }
</script>