<div class="content bg-dark">
  <div class="container">
	<div class="row">
	  
	  <div class="col-lg-3 col-md-6 col-sm-6">
		<div class="card card-stats">
		  <div class="card-header card-header-warning card-header-icon">
			<div class="card-icon">
			  <i class="material-icons">verified_users</i>
			</div>
			<p class="card-category">Approved Programs</p>
			<h3 class="card-title"><?php echo $caprog; ?></h3>
		  </div>
		  <div class="card-footer">
			<div class="stats">
			  <i class="material-icons text-danger">list</i>
			  <a href="<?php echo base_url('Admin/program/approved'); ?>">View Details</a>
			</div>
		  </div>
		</div>
	  </div>
	  <div class="col-lg-3 col-md-6 col-sm-6">
		<div class="card card-stats">
		  <div class="card-header card-header-rose card-header-icon">
			<div class="card-icon">
			  <i class="material-icons">privacy_tip</i>
			</div>
			<p class="card-category">Pending Programs</p>
			<h3 class="card-title"><?php echo $cpprog; ?></h3>
		  </div>
		  <div class="card-footer">
			<div class="stats">
			  <i class="material-icons">list</i> 
			  <a href="<?php echo base_url('Admin/program/pending'); ?>">View Details</a>
			</div>
		  </div>
		</div>
	  </div>
	  <div class="col-lg-3 col-md-6 col-sm-6">
		<div class="card card-stats">
		  <div class="card-header card-header-success card-header-icon">
			<div class="card-icon">
			  <i class="material-icons">supervisor_account</i>
			</div>
			<p class="card-category">Teachers</p>
			<h3 class="card-title"><?php echo $cprof; ?></h3>
		  </div>
		  <div class="card-footer">
			<div class="stats">
			  <i class="material-icons">list</i> 
			  <a href="<?php echo base_url('Admin/users/teacher'); ?>">View Details</a>
			</div>
		  </div>
		</div>
	  </div>
	  <div class="col-lg-3 col-md-6 col-sm-6">
		<div class="card card-stats">
		  <div class="card-header card-header-info card-header-icon">
			<div class="card-icon">
			  <i class="material-icons">assignment_ind</i>
			</div>
			<p class="card-category">Students</p>
			<h3 class="card-title"><?php echo $cstud; ?></h3>
		  </div>
		  <div class="card-footer">
			<div class="stats">
			  <i class="material-icons">list</i> 
			  <a href="<?php echo base_url('Admin/users/student'); ?>">View Details</a>
			</div>
		  </div>
		</div>
	  </div>
	  
	</div>
  </div>
</div>