<div class="content">
  <div class="container">
	<div class="row">
	  <div class="col-md-12">
		<div class="card">
			<div class="card-header card-header-info card-header-text">
				<div class="card-text">
					<h4 class="card-title">Feedback List</h4>
				  </div>
			</div>
			<div class="card-body">
				<div class="material-datatables">
					<table class="table table-striped table-no-bordered table-hover dtinsstrm" cellspacing="0" id="tbl" width="100%">
						<thead>
							<tr>
								<th width="5%">Sl</th>
								<th width="15%">Name</th>
								<th width="15%">Contact</th>
								<th width="40%">Feedback</th>
								<th width="15%">Date</th>
								<th width="10%">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(!empty($fdlist)){
								$i=1;
								foreach($fdlist as $prow){
									$id = $prow->sl;
									$name = trim($prow->first_name." ".$prow->last_name);
							?>
							<tr>
								<td><?php echo $i; ?></td>
								<td>
									<?php 	
										echo $name
									?>
								</td>
								<td>
								<?php
									  echo trim($prow->email).'<br>'.trim($prow->mobile);
								?>
								</td>
								<td><?php echo $prow->msg; ?></td>
								<td><?php echo date('jS M Y h:ia', strtotime($prow->create_datetime)); ?></td>
								<td>
									<?php
										$stat = ($prow->status=='2')? 'Blocked' : (($prow->status=='1')? 'Published' : 'Pending');
										if($prow->status=='0'){
											echo '<button type="button" onClick="changeStatus('.$id.', 2, `block`);" class="bt btn-block btn-sm btn-warning">Block?</button>';
											echo '<button type="button" onClick="changeStatus('.$id.', 1, `publish`);" class="bt btn-block btn-sm btn-primary">Publish?</button>';
										}else if($prow->status=='2'){
											echo '<span class="label label-danger">'.$stat.'</span>';
										}else if($prow->status=='1'){
											echo '<span class="label label-success">'.$stat.'</span>';
										}
									?>
								</td>
							</tr>
							<?php 
								$i++; }
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	  </div>
	</div>
  </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/plugins/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {
		$('#tbl').DataTable();
	});
	
	function changeStatus(id, stat, status){
		Swal.fire({
		  title: 'Are yoi sure?',
		  text: "You want to "+status+" this feedback",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!',
		  preConfirm: () => {
			return fetch(`${baseURL}/Admin/changeFeedbackStatus/${id}/${stat}`)
			  .then(response => {
				if (!response.ok) {
				  throw new Error(response.statusText)
				}
				return response.json()
			  })
			  .catch(error => {
				Swal.showValidationMessage(
				  `Request failed: ${error}`
				)
			  })
		  },
		  allowOutsideClick: () => !Swal.isLoading()
		}).then((result) => {
		  if(result.value.status){
			  Swal.fire(
				  'Success',
				  title+' has been deleted.',
				  'success'
				)
				window.location.reload();
		  }else{
			  Swal.fire(
				  'Error',
				  'Deletion error. Please try again.',
				  'error'
				)
		  }
		})
	}
</script>