<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <link rel="icon" type="image/png" href="<?php echo base_url().'assets/img/favicon.png'; ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
	<?php echo $pageTitle; ?>
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--  Social tags      -->
  <meta name="keywords" content="">
  <meta name="description" content="">

  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="<?php echo base_url(); ?>assets/css/material-dashboard.min1c51.css?v=2.1.2" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>assets/demo/demo.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>assets/vendor/owlcarousel/css/owl.carousel.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/vendor/jquery-ui/jquery-ui.css" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
  <!--   Core JS Files   -->
  <script src="<?php echo base_url(); ?>assets/js/core/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/core/popper.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/plugins/moment.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap-notify.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendor/jquery-ui/jquery-ui.js"></script>  
  <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
  <script src="<?php echo base_url().'assets/vendor/ckeditor/ckeditor.js'; ?>" type="text/javascript"></script>
  <script>var baseURL='<?php echo base_url(); ?>'; var uType = "<?php echo $_SESSION['userData']['utype']; ?>";</script>
</head>

<body>
<div class="preloader" id="loader">
	<img src="<?php echo base_url().'assets/img/Preloader_3.gif'; ?>">
</div>
<div class="wrapper">
	<div class="sidebar" data-color="purple" data-background-color="black" data-image="<?php echo base_url(); ?>assets/img/sidebar-1.jpg">
		<div class="sidebar-wrapper">
			<div class="user">
				<div class="photo">
					<img src="<?php echo base_url().$_SESSION['userData']['photo']; ?>" onerror="this.src='<?php echo base_url(); ?>assets/img/default-avatar.png'" />
				</div>
				<div class="user-info">
					<a data-toggle="collapse" href="#sideProfile" class="username">
					  <span>
						<?php echo $_SESSION['userData']['name']; ?>
						<b class="caret"></b>
					  </span>
					</a>
					<div class="collapse" id="sideProfile">
						<ul class="nav">
							<li class="nav-item">
							  <a class="nav-link" href="<?php echo base_url().'Teacher/userProfile'; ?>">
								<span class="sidebar-mini"> MP </span>
								<span class="sidebar-normal"> My Profile </span>
							  </a>
							</li>
							<li class="nav-item">
							  <a class="nav-link" href="javascript:;" data-toggle="modal" data-target="#passModal">
								<span class="sidebar-mini"> S </span>
								<span class="sidebar-normal"> Change Password </span>
							  </a>
							</li>
									<li class="nav-item">
							  <a class="nav-link" href="<?php echo base_url('Teacher/logout'); ?>">
								<span class="sidebar-mini"> L </span>
								<span class="sidebar-normal"> Logout </span>
							  </a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<ul class="nav">
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url('Teacher'); ?>">
					  <i class="material-icons">dashboard</i>
					  <p> Dashboard </p>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" data-toggle="collapse" href="#sideProgram">
					  <i class="material-icons">developer_board</i>
					  <p> Program
						<b class="caret"></b>
					  </p>
					</a>
					<div class="collapse" id="sideProgram">
					  <ul class="nav">
						<li class="nav-item ">
						  <a class="nav-link" href="<?php echo base_url('Admin/program/approved'); ?>">
							<span class="sidebar-mini"> AP </span>
							<span class="sidebar-normal"> Approved Programs </span>
						  </a>
						</li>
						<li class="nav-item ">
						  <a class="nav-link" href="<?php echo base_url('Admin/program/pending'); ?>">
							<span class="sidebar-mini"> PS </span>
							<span class="sidebar-normal"> Pending Programs </span>
						  </a>
						</li>
					  </ul>
					</div>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url('Admin/users/teacher'); ?>">
					  <i class="material-icons">supervisor_account</i>
					  Teachers
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url('Admin/users/student'); ?>">
					  <i class="material-icons">assignment_ind</i>
					  Students
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url('Admin/feedbacks'); ?>">
						<i class="material-icons">campaign</i> Feedbacks
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" data-toggle="collapse" href="#sideCommon">
					  <i class="material-icons">business</i>
					  <p> Common
						<b class="caret"></b>
					  </p>
					</a>
					<div class="collapse" id="sideCommon">
					  <ul class="nav">
						<li class="nav-item ">
						  <a class="nav-link" href="<?php echo base_url('Admin/category/list'); ?>">
							<span class="sidebar-mini"> CL </span>
							<span class="sidebar-normal"> Category </span>
						  </a>
						</li>
						<li class="nav-item ">
						  <a class="nav-link" href="<?php echo base_url('Admin/topic/list'); ?>">
							<span class="sidebar-mini"> TL </span>
							<span class="sidebar-normal"> Topics </span>
						  </a>
						</li>
						<li class="nav-item ">
						  <a class="nav-link" href="<?php echo base_url('Admin/coupons/list'); ?>">
							<span class="sidebar-mini"> CC </span>
							<span class="sidebar-normal"> Coupon Codes </span>
						  </a>
						</li>
						<li class="nav-item ">
						  <a class="nav-link" href="<?php echo base_url('Admin/skills/list'); ?>">
							<span class="sidebar-mini"> SS </span>
							<span class="sidebar-normal"> Skills </span>
						  </a>
						</li>
						<li class="nav-item ">
						  <a class="nav-link" href="<?php echo base_url('Admin/organizations'); ?>">
							<span class="sidebar-mini"> OG </span>
							<span class="sidebar-normal"> Organizations </span>
						  </a>
						</li>
					  </ul>
					</div>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url('Admin/admin_faq_page/FAQ'); ?>">
						<i class="material-icons">quiz</i> 
						<p> Faq </p>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url('Admin/admin_alumni_speak_page/Speak'); ?>">
					  <i class="material-icons">record_voice_over</i>
					  <p> Alumni Speak</p>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url('Admin/admin_project_page/Project'); ?>">
					  <i class="material-icons">assignment_turned_in</i>
					  <p> Project </p>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url('Admin/admin_instructor_page/Instructor'); ?>">
						<i class="material-icons">supervisor_account</i> Instructors
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url('Admin/blog_page'); ?>">
						<i class="material-icons">article</i> Blogs
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url('Admin/booking_trial_class'); ?>">
						<i class="material-icons">article</i> Bookings techjnrs
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url('Admin/newsletters'); ?>">
						<i class="material-icons">news</i> Email Template
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url('Admin/message'); ?>">
					  <i class="material-icons">email</i>
					  Messages
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url('uploads/help.pdf'); ?>" target="_blank">
					  <i class="material-icons">help</i>
					  Help
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="main-panel">
		<nav class="navbar navbar-expand-lg bg-white navbar-absolute fixed-top">
			<div class="container-fluid">
				<div class="navbar-wrapper">
					<a class="navbar-brand" href="<?php echo base_url('Admin'); ?>"><img src="<?php echo base_url(); ?>assets/img/logo.png" class="img-responsive w-100" /></a>
				</div>
				<div class="justify-content-end d-lg-none d-none d-lg-block d-xl-none d-none d-xl-block">
					<ul class="navbar-nav">
						
						<li class="nav-item dropdown">
							<a class="nav-link" href="javascript:;" id="navProgram" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="material-icons">developer_board</i>Program
							</a>
							<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navProgram">
								<a class="dropdown-item" href="<?php echo base_url('Admin/program/approved'); ?>">Approved Programs</a>
								<a class="dropdown-item" href="<?php echo base_url('Admin/program/pending'); ?>">Pending Programs</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link" href="javascript:;" id="navCommon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="material-icons">business</i>Common
							</a>
							<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navCommon">
								<a class="dropdown-item" href="<?php echo base_url('Admin/category/list'); ?>">Category</a>
								<a class="dropdown-item" href="<?php echo base_url('Admin/topics/list'); ?>">Topics</a>
								<a class="dropdown-item" href="<?php echo base_url('Admin/coupons/list'); ?>">Coupon Codes</a>
								<a class="dropdown-item" href="<?php echo base_url('Admin/skills/list'); ?>">Skills</a>
								<a class="dropdown-item" href="<?php echo base_url('Admin/organizations'); ?>">Organizations</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link" href="javascript:;" id="navPeople" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="material-icons">supervisor_account</i>People
							</a>
							<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navPeople">
								<a class="dropdown-item" href="<?php echo base_url('Admin/users/teacher'); ?>">Teacher</a>
								<a class="dropdown-item" href="<?php echo base_url('Admin/users/student'); ?>">Student</a>
								<a class="dropdown-item" href="<?php echo base_url('Admin/admin_instructor_page/Instructor'); ?>">Instructors</a>
								<a class="dropdown-item" href="<?php echo base_url('Admin/add_instructor_with_programs'); ?>">Add Instructors With Programs</a>
								<a class="dropdown-item" href="<?php echo base_url('Admin/add_logined_user_admin_registration'); ?>">Add user using EXCEL OR CSV</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link" href="javascript:;" id="navTestimonials" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="material-icons">thumb_up</i>Testimonials
							</a>
							<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navTestimonials">
								<a class="dropdown-item" href="<?php echo base_url('Admin/feedbacks'); ?>">Feedbacks</a>
								<a class="dropdown-item" href="<?php echo base_url('Admin/admin_faq_page/FAQ'); ?>">Faq</a>
								<a class="dropdown-item" href="<?php echo base_url('Admin/admin_alumni_speak_page/Speak'); ?>">Alumni Speak</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link" href="javascript:;" id="navPost" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="material-icons">post_add</i>Post
							</a>
							<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navPost">
								<a class="dropdown-item" href="<?php echo base_url('Admin/admin_project_page/Project'); ?>">Project</a>
								<a class="dropdown-item" href="<?php echo base_url('Admin/admin_job_page/Job'); ?>">Job</a>
								<a class="dropdown-item" href="<?php echo base_url('Admin/blog_page'); ?>">Blogs</a>
								<a class="dropdown-item" href="<?php echo base_url('Admin/newsletters'); ?>">Email Template</a>
							</div>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?php echo base_url('Admin/booking_trial_class'); ?>">
								<i class="material-icons">list</i> Bookings techjnrs
							</a>
						</li>
					</ul>
				</div>
				<button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
					<span class="sr-only">Toggle navigation</span>
					<span class="navbar-toggler-icon icon-bar"></span>
					<span class="navbar-toggler-icon icon-bar"></span>
					<span class="navbar-toggler-icon icon-bar"></span>
				</button>
				<div class="collapse navbar-collapse justify-content-end">
					<ul class="navbar-nav">
						<li class="nav-item">
							<a class="nav-link" rel="tooltip" title="Dashboard" href="<?php echo base_url('Admin'); ?>">
							<i class="material-icons">dashboard</i>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" rel="tooltip" title="Message" href="<?php echo base_url('Admin/message'); ?>">
							<i class="material-icons">email</i>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" rel="tooltip" title="Help" href="<?php echo base_url('uploads/help.pdf'); ?>" target="_blank">
							<i class="material-icons">help_center</i>
							</a>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							  <img src="<?php echo base_url().$_SESSION['userData']['photo']; ?>" onerror="this.src='<?php echo base_url(); ?>assets/img/default-avatar.png'" class="avatar-dp" >
							</a>
							<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile" style="min-width:15rem !important;">
							  <h6 class="text-center"><?php echo '<strong>'.$_SESSION['userData']['name'].'</strong><br>'.$_SESSION['userData']['email']; ?></h6>
							  <div class="dropdown-divider"></div>
							  <a class="dropdown-item" href="<?php echo base_url('Admin'); ?>"><i class="material-icons">dashboard</i> Dashboard</a>
							  <a class="dropdown-item" href="javascript:;" data-toggle="modal" data-target="#passModal"><i class="material-icons">settings</i> Settings</a>
							  <div class="dropdown-divider"></div>
							  <a class="dropdown-item" href="<?php echo base_url().'Admin/logout'; ?>"><i class="fa fa-sign-out"></i> Log out</a>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		
		<div class="modal fade" id="passModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Change Password</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						<i class="material-icons">clear</i>
						</button>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="new-pass" class="control-label">New Password</label>
									<input type="password" class="form-control" id="new-pass" required autocomplete="off">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="progress progress-striped active">
									<div class="progress-bar progress-bar-red" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" id="pass-progress" style="width: 0%"></div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="form-group has-info" id="re-pass-div">
									<label for="re-pass" class="control-label">Re-enter Password</label>
									<input type="password" class="form-control" id="re-pass" required autocomplete="off">
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
					  <button type="button" class="btn btn-link" id="btn_save_pass">Save Password</button>
					  <button type="button" class="btn btn-danger btn-link" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<script src="<?php echo base_url('assets/js/passcommon.js'); ?>"></script>