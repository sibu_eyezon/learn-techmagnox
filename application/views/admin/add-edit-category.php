<div class="content">
  <div class="container">
	<div class="row">
	  <div class="col-md-8 mx-auto">
		<?php
			if($this->session->flashdata('error')!=""){
				echo '<div class="alert alert-warning">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
                    <span>'.$this->session->flashdata('error').'</span>
                  </div>';
			}
		?>
		<div class="card">
			<div class="card-header card-header-info card-header-text">
				<div class="card-text">
					<h4 class="card-title"><?php echo ($cat[0]->id==0)? 'Add Category':'Update Category'; ?></h4>
				  </div>
				  <a href="<?php echo base_url('Admin/category/list'); ?>" class="btn btn-sm btn-info pull-right"><i class="material-icons">list</i> Category List</a>
			</div>
			<div class="card-body">
				<form action="<?php echo base_url('Admin/cuCategory'); ?>" method="POST" id="frncats">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label>Category name</label>
								<input type="text" class="form-control" name="catname" id="catname" value="<?php echo trim($cat[0]->name); ?>"/>
								<input type="hidden" name="cid" id="cid" value="<?php echo $cat[0]->id; ?>"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<select name="pcat_list" id="pcat_list" class="selectpicker" data-style="select-with-transition">
									<option value="0">Select Category</option>
									<?php foreach($clist as $clow){
										echo '<option value="'.$clow->id.'" '.(($clow->id==$cat[0]->parent)? 'selected':'').'>'.trim($clow->name).'</option>';
									} ?>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label>Details</label><br>
								<textarea class="form-control" name="details" id="details"><?php echo trim($cat[0]->details); ?></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<input type="reset" style="display:none"/>
								<button type="submit" class="btn btn-success pull-left" id="btn_login"><?php echo ($cat[0]->id==0)? 'Save':'Update'; ?></button>
							  </div>  
						</div>
					</div>
				</form>
			</div>
		</div>
	  </div>
	</div>
  </div>
</div>
<script src="<?php echo base_url(); ?>assets/vendor/ckeditor/ckeditor.js"></script>
<script>
	$(document).ready(function() {
		CKEDITOR.replace('details', {
			extraPlugins: 'uploadimage,colorbutton,colordialog,autoembed,embedsemantic,mathjax,codesnippet,font,justify,table,specialchar, tabletools, uicolor',
			height: 200,
			mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML'
		});
	});
</script>