<div class="content">
  <div class="container">
	<div class="row">
	  <div class="col-md-12">
		<div class="card">
			<div class="card-header card-header-info card-header-text">
				<div class="card-text">
					<h4 class="card-title">Coupons List</h4>
				  </div>
				  <?php
					if($params=='list'){
						echo '<a href="'.base_url('Admin/coupons/add').'" class="btn btn-sm btn-info pull-right"><i class="material-icons">add</i> Add Coupons</a>';
					}else{
						echo '<a href="'.base_url('Admin/coupons/list').'" class="btn btn-sm btn-info pull-right"><i class="material-icons">list</i> Coupons List</a>';
					}
				  ?>
			</div>
			<div class="card-body">
				<div class="material-datatables">
					<table class="table table-striped table-no-bordered table-hover dtinsstrm" cellspacing="0" id="tbl" width="100%">
						<thead>
							<tr>
								<th width="5%">Sl</th>
								<th width="20%">Coupon Code</th>
								<th width="35%">Comments</th>
								<th width="15%">Date</th>
								<th width="10%">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(!empty($cclist)){
								$i=1;
								foreach($cclist as $prow){
									$tid = $prow->sl;
									$name = trim($prow->coupon_code);
									$is_active = (trim($prow->is_active)=='t')? true : false;
									$oppo_stat = (trim($prow->is_active)=='t')? 'block' : 'check';
									$ctype = trim($prow->coupon_type);
							?>
							<tr>
								<td><?php echo $i; ?></td>
								<td>
									<?php 	
										echo $name.'<br>';
										echo ($is_active)? 'Status: Active' : 'Status: In-Active';
									?>
								</td>
								<td>
								<?php
										  echo trim($prow->comments);
									?>
								</td>
								<td><?php echo date('jS M Y h:ia', strtotime($prow->add_datetime)); ?></td>
								<td>
									<!--<a href="<?php //echo base_url('Admin/coupons/edit/?id='.$tid); ?>" class="btn btn-sm btn-block btn-primary"><i class="material-icons">edit</i> Edit</a>-->
									<button type="button" onClick="changeStatusCoupon(<?php echo $tid; ?>, '<?php echo $oppo_stat; ?>');" class="btn btn-sm btn-block btn-warning"><?php echo '<i class="material-icons">'.$oppo_stat.'</i> '.(($is_active)? 'Deactivate it' : 'Activate it'); ?></button>
								</td>
							</tr>
							<?php 
								$i++; }
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	  </div>
	</div>
  </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/plugins/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {
		$('#tbl').DataTable();
	});
	function changeStatusCoupon(id, status)
	{
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You want to change status?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, change it!',
		  preConfirm: () => {
			return fetch(`${baseURL}/Admin/updateStatusCoupon/${id}/${status}`)
			  .then(response => {
				if (!response.ok) {
				  throw new Error(response.statusText)
				}
				return response.json()
			  })
			  .catch(error => {
				Swal.showValidationMessage(
				  `Request failed: ${error}`
				)
			  })
		  },
		  allowOutsideClick: () => !Swal.isLoading()
		}).then((result) => {
		  if(result.value.status){
			  Swal.fire(
				  'Success',
				  'The coupon status has been updated.',
				  'success'
				)
				window.location.reload();
		  }else{
			  Swal.fire(
				  'Error',
				  'Updation error. Please try again.',
				  'error'
				)
		  }
		})
	}
</script>