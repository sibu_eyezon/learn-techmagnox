<div class="content">
  <div class="container">
	<div class="row">
	  <div class="col-md-8 mx-auto">
		<?php
			if($this->session->flashdata('error')!=""){
				echo '<div class="alert alert-warning">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
                    <span>'.$this->session->flashdata('error').'</span>
                  </div>';
			}
		?>
		<div class="card">
			<div class="card-header card-header-info card-header-text">
				<div class="card-text">
					<h4 class="card-title"><?php echo ($skill[0]->id==0)? 'Add Skill':'Update Skill'; ?></h4>
				  </div>
				  <a href="<?php echo base_url('Admin/skills/list'); ?>" class="btn btn-sm btn-info pull-right"><i class="material-icons">list</i> skill List</a>
			</div>
			<div class="card-body">
				<form action="<?php echo base_url('Admin/cuSkill'); ?>" method="POST" id="frnskills" enctype="multipart/form-data">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label>Skill name</label>
								<input type="text" class="form-control" name="sname" id="sname" value="<?php echo trim($skill[0]->name); ?>"/>
								<input type="hidden" name="sid" id="sid" value="<?php echo $skill[0]->id; ?>"/>
							</div>
						</div>
					</div>
					<div class="row mb-5">
						<div class="col-sm-6">
							<div class="form-group">
								<div class="custom-file">
									<input type="file" class="custom-file-input" name="slogo" id="slogo" accept=".png, .jpg, .jpeg">
									<label class="custom-file-label text-dark" for="slogo"><?php echo (trim($skill[0]->slogo)!="")? trim($skill[0]->slogo) : "Upload Logo [250 x 250]"; ?> </label>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label>Details</label><br>
								<textarea class="form-control" name="details" id="details"><?php echo trim($skill[0]->desc); ?></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<input type="reset" style="display:none"/>
								<button type="submit" class="btn btn-success pull-left" id="btn_login"><?php echo ($skill[0]->id==0)? 'Save':'Update'; ?></button>
							  </div>  
						</div>
					</div>
				</form>
			</div>
		</div>
	  </div>
	</div>
  </div>
</div>
<script src="<?php echo base_url(); ?>assets/vendor/ckeditor/ckeditor.js"></script>
<script>
	$(document).ready(function() {
		CKEDITOR.replace('details', {
			extraPlugins: 'uploadimage,colorbutton,colordialog,autoembed,embedsemantic,mathjax,codesnippet,font,justify,table,specialchar, tabletools, uicolor',
			height: 200,
			mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML'
		});
		$("#slogo").on("change", function() {
		  var fileName = $(this).val().split("\\").pop();
		  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
		});
	});
</script>