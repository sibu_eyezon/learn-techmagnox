<div class="content">
  <div class="container">
	<div class="row">
	  <div class="col-md-12">
		<div class="card">
			<div class="card-header card-header-info card-header-text">
				<div class="card-text">
					<h4 class="card-title">Skills List</h4>
				  </div>
				  <?php
					if($params=='list'){
						echo '<a href="'.base_url('Admin/skills/add').'" class="btn btn-sm btn-info pull-right"><i class="material-icons">add</i> Add Skill</a>';
					}else{
						echo '<a href="'.base_url('Admin/skills/list').'" class="btn btn-sm btn-info pull-right"><i class="material-icons">list</i> Skill List</a>';
					}
				  ?>
			</div>
			<div class="card-body">
				<div class="material-datatables">
					<table class="table table-striped table-no-bordered table-hover dtinsstrm" cellspacing="0" id="tbl" width="100%">
						<thead>
							<tr>
								<th width="5%">Sl</th>
								<th width="10%">Image</th>
								<th width="20%">Skills</th>
								<th width="40%">Details</th>
								<th width="10%">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(!empty($slist)){
								$i=1;
								foreach($slist as $prow){
									$tid = $prow->id;
									$name = trim($prow->name);
									$files = trim($prow->slogo);
							?>
							<tr>
								<td><?php echo $i; ?></td>
								<td><?php 
									echo '<img src="'.base_url('assets/img/skills/'.$files).'" class="img-responsive" width="150" onerror="this.src=`'.base_url('assets/img/sqrpop.png').'`"/>';
								?></td>
								<td>
									<?php 	
										echo $name;
									?>
								</td>
								<td>
								<div style="max-height: 150px; overflow-y:scroll;">
								<?php
									  echo trim($prow->desc);
								?>
								</div>
								</td>
								
								<td>
									<a href="<?php echo base_url('Admin/skills/edit/?id='.$tid); ?>" class="btn btn-sm btn-block btn-primary"><i class="material-icons">edit</i> Edit</a>
									<button type="button" onClick="deleteSkill(<?php echo $tid; ?>);" class="btn btn-sm btn-block btn-warning"><i class="material-icons">delete</i> Delete</button>
								</td>
							</tr>
							<?php 
								$i++; }
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	  </div>
	</div>
  </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/plugins/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {
		$('#tbl').DataTable();
	});
	function deleteSkill(id)
	{
		Swal.fire({
		  title: 'Are yoi sure?',
		  text: "You want to remove the skill",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!',
		  preConfirm: () => {
			return fetch(`${baseURL}/Admin/removeSkills/${id}`)
			  .then(response => {
				if (!response.ok) {
				  throw new Error(response.statusText)
				}
				return response.json()
			  })
			  .catch(error => {
				Swal.showValidationMessage(
				  `Request failed: ${error}`
				)
			  })
		  },
		  allowOutsideClick: () => !Swal.isLoading()
		}).then((result) => {
		  if(result.value.status){
			  Swal.fire(
				  'Success',
				  'The skill has been deleted.',
				  'success'
				)
				window.location.reload();
		  }else{
			  Swal.fire(
				  'Error',
				  'Deletion error. Please try again.',
				  'error'
				)
		  }
		})
	}
</script>