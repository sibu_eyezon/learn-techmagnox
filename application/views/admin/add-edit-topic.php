<div class="content">
  <div class="container">
	<div class="row">
	  <div class="col-md-8 mx-auto">
		<?php
			if($this->session->flashdata('error')!=""){
				echo '<div class="alert alert-warning">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
                    <span>'.$this->session->flashdata('error').'</span>
                  </div>';
			}
		?>
		<div class="card">
			<div class="card-header card-header-info card-header-text">
				<div class="card-text">
					<h4 class="card-title"><?php echo ($topic[0]->sl==0)? 'Add Topic':'Update Topic'; ?></h4>
				  </div>
				  <a href="<?php echo base_url('Admin/topics/list'); ?>" class="btn btn-sm btn-info pull-right"><i class="material-icons">list</i> Topic List</a>
			</div>
			<div class="card-body">
				<form action="<?php echo base_url('Admin/cuTopic'); ?>" method="POST" id="frntopics" enctype="multipart/form-data">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label>Topic name</label>
								<input type="text" class="form-control" name="tname" id="tname" value="<?php echo trim($topic[0]->tname); ?>"/>
								<input type="hidden" name="cid" id="cid" value="<?php echo $topic[0]->sl; ?>"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label>Upload Youtube Embeded iFrame code here</label><br>
								<textarea class="form-control" name="videos" id="videos"><?php echo trim($topic[0]->video); ?></textarea>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<div class="custom-file">
									<input type="file" class="custom-file-input" name="brochure" id="brochure" accept=".doc, .docx, application/pdf,application/vnd.ms-excel">
									<label class="custom-file-label text-dark" for="brochure"><?php echo (trim($topic[0]->brochure)!="")? trim($topic[0]->brochure) : "Upload brochure"; ?> </label>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label>Details</label><br>
								<textarea class="form-control" name="details" id="details"><?php echo trim($topic[0]->details); ?></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<input type="reset" style="display:none"/>
								<button type="submit" class="btn btn-success pull-left" id="btn_login"><?php echo ($topic[0]->sl==0)? 'Save':'Update'; ?></button>
							  </div>  
						</div>
					</div>
				</form>
			</div>
		</div>
	  </div>
	</div>
  </div>
</div>
<script src="<?php echo base_url(); ?>assets/vendor/ckeditor/ckeditor.js"></script>
<script>
	$(document).ready(function() {
		CKEDITOR.replace('details', {
			extraPlugins: 'uploadimage,colorbutton,colordialog,autoembed,embedsemantic,mathjax,codesnippet,font,justify,table,specialchar, tabletools, uicolor',
			height: 200,
			mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML'
		});
		$("#brochure").on("change", function() {
		  var fileName = $(this).val().split("\\").pop();
		  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
		});
	});
</script>