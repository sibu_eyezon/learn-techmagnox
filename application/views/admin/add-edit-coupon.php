<div class="content">
  <div class="container">
	<div class="row">
	  <div class="col-md-8 mx-auto">
		<?php
			if($this->session->flashdata('error')!=""){
				echo '<div class="alert alert-warning">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <i class="material-icons">close</i>
                    </button>
                    <span>'.$this->session->flashdata('error').'</span>
                  </div>';
			}
		?>
		<div class="card">
			<div class="card-header card-header-info card-header-text">
				<div class="card-text">
					<h4 class="card-title"><?php echo ($coupon[0]->sl==0)? 'Add Coupon':'Update Coupon'; ?></h4>
				  </div>
				  <a href="<?php echo base_url('Admin/coupons/list'); ?>" class="btn btn-sm btn-info pull-right"><i class="material-icons">list</i> Coupon List</a>
			</div>
			<div class="card-body">
				<form action="<?php echo base_url('Admin/cuCoupon'); ?>" method="POST" id="frncoupons" enctype="multipart/form-data">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label class="text-dark">Coupon code</label>
								<input type="text" class="form-control" name="coupon_code" id="coupon_code" value="<?php echo trim($coupon[0]->coupon_code); ?>"/>
								<input type="hidden" name="cid" id="cid" value="<?php echo $coupon[0]->sl; ?>"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								 <div class="checkbox-radios">
									<div class="form-check form-check-inline">
									  <label class="form-check-label text-dark">
										<input class="form-check-input" type="radio" <?php echo ($coupon[0]->coupon_type=='A')? 'checked' : ''; ?> value="A" name="coupon_type" id="amount"> Amount?
										<span class="form-check-sign">
										  <span class="check"></span>
										</span>
									  </label>
									</div>
									<div class="form-check form-check-inline">
									  <label class="form-check-label text-dark">
										<input class="form-check-input" type="radio" <?php echo ($coupon[0]->coupon_type=='P')? 'checked' : ''; ?> value="P" name="coupon_type" id="percent"> Percentage?
										<span class="form-check-sign">
										  <span class="check"></span>
										</span>
									  </label>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group" id="show_amt" style="display:none;">
								<label class="text-dark">Discount Amount</label>
								<input type="number" class="form-control" name="discount_amt" id="discount_amt" value="<?php echo $coupon[0]->discount_amt; ?>"/>
							</div>
							<div class="form-group" id="show_percent" style="display:none;">
								<label class="text-dark">Discount Percentage</label>
								<input type="number" class="form-control" name="discount_percent" id="discount_percent" value="<?php echo $coupon[0]->discount_percent; ?>"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label class="text-dark">Comments</label>
								<textarea class="form-control" name="comments" id="comments"><?php echo trim($coupon[0]->comments); ?></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<input type="reset" style="display:none"/>
								<button type="submit" class="btn btn-success pull-left" id="btn_login"><?php echo ($coupon[0]->sl==0)? 'Save':'Update'; ?></button>
							  </div>  
						</div>
					</div>
				</form>
			</div>
		</div>
	  </div>
	</div>
  </div>
</div>
<script src="<?php echo base_url(); ?>assets/vendor/ckeditor/ckeditor.js"></script>
<script>
	$(document).ready(function() {
		$("form#frncoupons").validate({
			errorPlacement: function(error, element) {
			  $(element).closest('.form-group').append(error);
			},
			rules: {
				coupon_code: {
					required: true,
					minlength: 3
				},
				coupon_type: {
					required: true
				},
				discount_amt: {
					required: true,
					digits: true
				},
				discount_percent: {
					required: true,
					digits: true
				}
			},
			messages: {
				coupon_code: {
					required: 'Must enter coupon code.',
					minlength: 'It must be minimum 3characters.'
				},
				coupon_type: {
					required: 'Must select coupon type.'
				},
				discount_amt: {
					required: 'Must enter discount amount.',
					digits: 'Must enter digits only.'
				},
				discount_percent: {
					required: 'Must enter discount percentage.',
					digits: 'Must enter digits only.'
				}
			},
		});
	});
	
	$('input[type=radio][name=coupon_type]').on('change', ()=>{
		var value = $('input[type=radio][name=coupon_type]:checked').val();
		toggleDiscount(value);
	});
	function toggleDiscount(value)
	{
		if(value=='A'){
			$('#show_amt').show();
			$('#show_percent').hide();
		}else if(value=='P'){
			$('#show_percent').show();
			$('#show_amt').hide();
		}
	}
</script>