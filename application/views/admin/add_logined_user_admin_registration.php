<style>
    .error{
        color: red;
    }
</style>
<div class="content">
	<div class="container-fluid">
        <div class="card">
            <!-- <div class="card-body">
                <div class="header">
                    <u><h2 class="text-center">Import And Show User Data</h2></u>
                </div>
                <div class="upload">
                    <form id="php_spread_sheet" action="<?= site_url('PhpSpreadSheet/uploadUserRegistrationSpradSheet'); ?>" method="POST" enctype="multipart/form-data"> 
                        <strong class="text-dark">Browse either Excel or CSV file</strong><br/>
                        <input type="file" name="cvsUploader" id="cvsUploader">
                        <input type="submit" class="mt-5" name="" value="IMPORT"/>
                    </form>
                </div>
                <div class="dataTable">

                </div>
            </div> -->
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="container py-2 px-4">
                            <p class="h4 text-center"><u>Import Excel or CSV File</u></p>
                            <div class="w-75 mx-auto">
                                <form class="my-3" id="php_spread_sheet" action="<?= site_url('PhpSpreadSheet/uploadUserRegistrationSpradSheet'); ?>" method="POST" enctype="multipart/form-data">
                                        <label class="control-label text-dark">Browse either Excel or CSV file</label><br/>
                                        <input type="file" name="cvsUploader" id="cvsUploader">
                                    <small id="emailHelp" class="form-text text-muted">Please download and check sample excel format before upload</small>
                                    <button class="btn btn-primary btn-sm" type="submit">Submit</button>
                                </form>
                                <div class="progress-bar progress-bar-striped progress-bar-animated" id="prg_progress" style="width:100%; display:none;">Excel is uploading. Please wait...</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="container py-2 px-4">
                            <p class="h4 text-center"><u>Sample excel sheet</u></p>
                            <div class="text-center">
                                <img src="<?= base_url('assets/img/sheet.png')?>" alt="sheet.." width="50%"/>
                                <br/>
                                <a href="<?= base_url('assets/img/sheetFormat.xlsx')?>" type="button" class="btn btn-primary" download>Download</a>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= site_url(); ?>/assets/js/plugins/jquery.validate.min.js"></script>
<script>
    $(document).ready(function(){
        $('#php_spread_sheet').validate({
            rules:{
                cvsUploader:{
                    required:true,
                }
            },
            messages:{
                cvsUploader:{
                    required:"Please upload file***",
                }
            },
            submitHandler: function(form){
                $('#prg_progress').css('display', 'block');
                let formData = new FormData(form);
                $.ajax({
                    url: form.action,
	                type: form.method,
	                data: formData,
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    enctype: 'multipart/form-data',
                    success:function(data,status){
                        $('#prg_progress').css('display', 'none');
                        $('#php_spread_sheet')[0].reset();
                        let res = JSON.parse(data);
                        $.notify(
                            {icon:"add_alert", message: res.message},
                            {type:res.type, timer:2e2, placement:{from:'top',align:'right'}
                        })
                    },
                    error: function(err){
                        $('#prg_progress').css('display', 'none');
                        $.notify(
                            {icon:"add_alert", message: 'Something went wrong'},
                            {type:'error', timer:2e2, placement:{from:'top',align:'right'}
                        })
                    }
                })
            }
        });
    });
</script>