<div class="content">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header card-header-primary card-header-text">
					  <div class="card-text">
						<h4 class="card-title">Organization</h4>
					  </div>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-md-12">
								<div class="material-datatables">
									<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
									  <thead>
										<tr>
										  <th width="5%">Sl</th>
										  <th width="20%">Name</th>
										  <th width="15%">Contact</th>
										  <th width="15%">Address</th>
										  <th width="5%">Status</th>
										  <th width="15%">Action</th>
										</tr>
									  </thead>
									  <tbody>
										<?php $i=1; foreach($orgs as $mrow){ ?>
										<tr id="row_<?= $mrow->id; ?>">
										  <td><?php echo $i; ?></td>
										  <td><?php 
											$status = trim($mrow->status);
											$logo = trim($mrow->logo);
											if(file_exists('./assets/img/institute/'.$logo)){
												echo '<img src="'.base_url('assets/img/institute/'.$logo).'" width="70"/><br>';
											}
											echo $mrow->name; 
											?></td>
										  <td><?= trim($mrow->contact); ?></td>
										  <td><?= trim($mrow->address); ?></td>
										  <td><?= ($status=='t')? 'Active' : 'Blocked'; ?></td>
										  <td>
											<?php
												if($status=='t'){
													echo '<a href="javascript:toggleBlockOrganization('.$mrow->id.', `false`);" class="btn btn-warning btn-sm">Block</a>';
												}else{
													echo '<a href="javascript:toggleBlockOrganization('.$mrow->id.', `true`);" class="btn btn-success btn-sm">Un-Block</a>';
												}
											?>
										  	<a href="javascript:removeOrganization(<?= $mrow->id; ?>);" class="btn btn-danger btn-sm">Delete</a>
										  </td>
										</tr>
										<?php $i++; } ?>
									  </tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url(); ?>assets/js/plugins/jquery.dataTables.min.js"></script>
<script>
	//$('#datatables').DataTable();
	
	var tabreq = $('#datatables').DataTable();

	function removeOrganization(comm_id)
	{
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You want to remove Organization",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
		  if (result.value) {
			$('#loading').show();
				$.ajax({
					url:baseURL+'Admin/deleteOrganization?clid='+comm_id,
					type: 'GET',
					success: (res)=>{
						$('#loading').hide();
						if(res){
							$('#row_'+comm_id).remove();
							$.notify({
								icon:"add_alert",
								message: "The Organization has been deleted"},
								{type:"success",
								timer:3e3,
								placement:{from:'top',align:'right'}
							})
						}else{
							$.notify({
								icon:"add_alert",
								message: "The Organization deletion error"},
								{type:"danger",
								timer:3e3,
								placement:{from:'top',align:'right'}
							})
						}
					},
					error: (errors)=>{
						console.log(errors);
					}
				});
		  }
		});
	}
	function toggleBlockOrganization(comm_id, status)
	{
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You want to "+((status=='true')? 'unblock' : 'block')+" Organization",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, '+((status=='true')? 'unblock' : 'block')+' it!'
		}).then((result) => {
		  if (result.value) {
			$('#loading').show();
				$.ajax({
					url:baseURL+'Admin/toggleBlockOrganization?clid='+comm_id+'&status='+status,
					type: 'GET',
					success: (res)=>{
						$('#loading').hide();
						if(res){
							$.notify({
								icon:"add_alert",
								message: "The Organization has been "+((status=='true')? 'unblock' : 'block')+"ed"},
								{type:"success",
								timer:3e3,
								placement:{from:'top',align:'right'}
							});
							window.location.reload();
						}else{
							$.notify({
								icon:"add_alert",
								message: "Something went wrong. Please try again."},
								{type:"danger",
								timer:3e3,
								placement:{from:'top',align:'right'}
							})
						}
					},
					error: (errors)=>{
						console.log(errors);
					}
				});
		  }
		});
	}
</script>