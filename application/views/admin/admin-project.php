<div class="content">
	<div class="container-fluid">
        <div class="card bg-light">
            <div class="card-header card-header-info card-header-text">
                <div class="card-text">Project</div>
            </div>
            <div class="card-body">
                <div class="align-left mb-2">
                    <button class="btn btn-primary btn-sm" onclick="addEditModal('project')">
                        <span class="material-icons">add_circle_outline</span>Add Project
                    </button>
                    <button class="btn btn-warning btn-sm" id="changeButton" style="display:none;" onclick="multipleStatusChange('project')">
                        <span class="material-icons">published_with_changes</span>
                    </button>
                </div>
                <div class="table text-dark" id="project_list">
                    
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="operationModalCenter" tabindex="-1" role="dialog" aria-labelledby="operationModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" id="modal_content">
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/js/faq-alumni-project.js'); ?>"></script>

<script>
    $(document).ready(function(){
        getProjectList(1);
    });
</script>