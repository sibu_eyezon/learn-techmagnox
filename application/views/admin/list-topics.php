<div class="content">
  <div class="container">
	<div class="row">
	  <div class="col-md-12">
		<div class="card">
			<div class="card-header card-header-info card-header-text">
				<div class="card-text">
					<h4 class="card-title">Topic List</h4>
				  </div>
				  <?php
					if($params=='list'){
						echo '<a href="'.base_url('Admin/topics/add').'" class="btn btn-sm btn-info pull-right"><i class="material-icons">add</i> Add Topic</a>';
					}else{
						echo '<a href="'.base_url('Admin/topics/list').'" class="btn btn-sm btn-info pull-right"><i class="material-icons">list</i> Topic List</a>';
					}
				  ?>
			</div>
			<div class="card-body">
				<div class="material-datatables">
					<table class="table table-striped table-no-bordered table-hover dtinsstrm" cellspacing="0" id="tbl" width="100%">
						<thead>
							<tr>
								<th width="5%">Sl</th>
								<th width="20%">Topic</th>
								<th width="35%">Details</th>
								<th width="15%">Date</th>
								<th width="10%">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(!empty($tlist)){
								$i=1;
								foreach($tlist as $prow){
									$tid = $prow->sl;
									$name = trim($prow->tname);
									$files = trim($prow->brochure);
							?>
							<tr>
								<td><?php echo $i; ?></td>
								<td>
									<?php 	
										echo $name.'<br>';
										if(file_exists('uploads/topics/'.$files)){
											echo '<br><a href="'.base_url('uploads/topics/'.$files).'" target="_blank">Brochure</a>';
										}
									?>
								</td>
								<td>
								<div  style="height:250px; overflow-y:scroll;">
								<?php
									  echo trim($prow->video).'<br>'; 
									  echo trim($prow->details);
								?>
								</div>
								</td>
								<td><?php echo date('jS M Y h:ia', strtotime($prow->added_date)); ?></td>
								<td>
									<a href="<?php echo base_url('Admin/topics/edit/?id='.$tid); ?>" class="btn btn-sm btn-block btn-primary"><i class="material-icons">edit</i> Edit</a>
									<button type="button" onClick="deleteTopic(<?php echo $tid; ?>);" class="btn btn-sm btn-block btn-warning"><i class="material-icons">delete</i> Delete</button>
								</td>
							</tr>
							<?php 
								$i++; }
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	  </div>
	</div>
  </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/plugins/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {
		$('#tbl').DataTable();
	});
	function deleteTopic(id)
	{
		Swal.fire({
		  title: 'Are yoi sure?',
		  text: "You want to remove the topic",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!',
		  preConfirm: () => {
			return fetch(`${baseURL}/Admin/removeTopics/${id}`)
			  .then(response => {
				if (!response.ok) {
				  throw new Error(response.statusText)
				}
				return response.json()
			  })
			  .catch(error => {
				Swal.showValidationMessage(
				  `Request failed: ${error}`
				)
			  })
		  },
		  allowOutsideClick: () => !Swal.isLoading()
		}).then((result) => {
		  if(result.value.status){
			  Swal.fire(
				  'Success',
				  'The Topic has been deleted.',
				  'success'
				)
				window.location.reload();
		  }else{
			  Swal.fire(
				  'Error',
				  'Deletion error. Please try again.',
				  'error'
				)
		  }
		})
	}
</script>