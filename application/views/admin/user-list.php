<div class="content">
  <div class="container">
	<div class="row">
	  <div class="col-md-12">
		<div class="card">
			<div class="card-header card-header-info card-header-text">
				<div class="card-text">
					<h4 class="card-title"><?php echo ucfirst($params); ?> List</h4>
				  </div>
				  <?php
					if($params=='teacher'){
						echo '<a href="'.base_url('Admin/addUser/'.$params).'" class="btn btn-sm btn-info pull-right"><i class="material-icons">add</i> Add Teacher</a>';
					}else if($params=='student'){
						//echo '<a href="'.base_url('Admin/addStudent').'" class="btn btn-sm btn-info pull-right"><i class="material-icons">add</i> Add Student</a>';
					}
				  ?>
			</div>
			<div class="card-body">
				<div class="material-datatables">
					<table class="table table-striped table-no-bordered table-hover dtinsstrm" cellspacing="0" id="tbl" width="100%">
						<thead>
							<tr>
								<th width="5%">Sl</th>
								<th width="20%">Name</th>
								<th width="15%">Contact</th>
								<th width="35%">About</th>
								<th width="15%">Date</th>
								<th width="10%">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(!empty($users)){
								$i=1;
								foreach($users as $prow){
									$user_id = $prow->id;
									$name = trim($prow->first_name." ".$prow->last_name);
							?>
							<tr>
								<td><?php echo $i; ?></td>
								<td>
									<?php 	
										echo $name.'<br>';
										echo (trim($prow->verification_status)=='t')? '<span class="label label-success">verified</span>' : '<span class="label label-danger">not verified</span>';
									?>
								</td>
								<td>
								<?php
									  echo trim($prow->email).'<br>'.trim($prow->phone);
								?>
								</td>
								<td><?php echo $prow->about_me; ?></td>
								<td><?php echo date('jS M Y h:ia', strtotime($prow->created_date_time)); ?></td>
								<td>
									<a href="<?php echo base_url('Admin/addUser/'.$params.'/'.$user_id); ?>" class="btn btn-sm btn-block btn-primary"><i class="material-icons">edit</i> Edit</a>
									<button type="button" class="btn btn-sm btn-block btn-warning"><i class="material-icons">block</i> Block</button>
								</td>
							</tr>
							<?php 
								$i++; }
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	  </div>
	</div>
  </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/plugins/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {
		$('#tbl').DataTable();
	});
	
</script>