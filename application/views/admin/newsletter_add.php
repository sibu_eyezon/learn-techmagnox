<?php
	$submit = ($result[0]->id=='0')? 'Add' : 'Update';
?>
<style>
.form-error{
	color:red;
}
#image_prev {
	cursor: pointer;
}
</style>
<div class="content">
	<div class="container-fluid">
		<form action="<?= base_url('Admin/cuNewsletter'); ?>" enctype="multipart/form-data" id="frmCommunity" method="POST" style="width: 100%;">
			<div class="row">
				<div class="col-lg-9 mx-auto">
					<div class="card">
						<div class="card-header card-header-primary card-header-icon">
						  <div class="card-icon">
							<i class="material-icons">create</i>
						  </div>
						  <h4 class="card-title"><?= $submit; ?> Email Template <small class="text-danger">*'s are important</small>
						  <a href="<?= base_url('Admin/newsletters'); ?>" class="btn btn-primary btn-sm pull-right">Email Template List</a>
						  </h4>
						  
						</div>
						<div class="card-body">
							<div class="row justify-content-center">
								<div class="col-sm-12">
									<div class="form-group">
									  <label for="title" class="text-dark">Subject <span class="text-danger">*</span></label>
									  <input type="text" class="form-control" id="title" name="title" placeholder="Max length: 200 characters" value="<?= $result[0]->title; ?>">
									  <input type="hidden" name="cid" id="cid" value="<?= $result[0]->id; ?>"/>
									</div>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-md-12">
									<div class="form-group">
										 <label for="overview">Message</label><br>
										<textarea name="desc" id="desc" class="form-control" cols="80" rows="5"><?= $result[0]->details; ?></textarea>
									</div>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-md-12">
									<div class="form-group text-center">
										<button type="submit" class="btn btn-primary btn-md"><?= $submit; ?></button>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
			
		</form>
	</div>
</div>
<script src="<?php echo base_url(); ?>assets/vendor/ckeditor/ckeditor.js"></script>
<script>
	CKEDITOR.replace('desc', {
		extraPlugins: 'image2,colorbutton,colordialog,autoembed,embedsemantic,mathjax,codesnippet,font,justify,table,specialchar, tabletools, uicolor',
		removePlugins: 'image, easyimage',
		height: 400,
		mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML',
		filebrowserBrowseUrl: '<?php echo base_url(); ?>assets/vendor/ckfinder/ckfinder.html',
		filebrowserImageBrowseUrl: '<?php echo base_url(); ?>assets/vendor/ckfinder/ckfinder.html?type=Images',
		filebrowserUploadUrl: '<?php echo base_url(); ?>assets/vendor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl: '<?php echo base_url(); ?>assets/vendor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
		filebrowserWindowWidth: '1000',
		filebrowserWindowHeight: '700'
	});
	$(function(){
		$('#frmCommunity').validate({
			errorPlacement: function(error, element) {
			  $(element).closest('.form-group').append(error);
			},
			rules: {
				title: {
					required: true,
					maxlength: 200
				}
			},
		});
	});

	
</script>