<form action="<?php echo site_url('Autoprogram/cuSections/'.$progtype); ?>" id="frmSection" method="post">
	<div class="form-group">
		<label for="title text-dark">Section Title *</label>
		<input class="form-control" type="text" name="title" id="title" placeholder="Enter Section Title" required/>
		<input type="hidden" id="prog_id" name="prog_id" value="<?php echo $params1; ?>"/>
		<input type="hidden" id="sec_id" name="sec_id" value="0"/>
		<input type="hidden" id="rank" name="rank" value="<?php echo $params2; ?>"/>
	</div>
	<div class="text-right">
		<button class="btn btn-success" type="submit" name="button">Save</button>
	</div>
</form>