<?php
$sections = $this->AutoProgramModel->get_section($params);
?>

<?php if (count($sections)){ ?>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<div class="row" id = "parent-div" data-plugin="dragula" data-containers='["section-list"]'>
					<div class="col-md-12">
						<div class="bg-dragula p-2 p-lg-4">
							<h5 class="mt-0">List of Sections
								<button type="button" class="btn btn-outline-primary btn-sm btn-rounded alignToTitle" id = "section-sort-btn" onclick="sort()" name="button">Update Sorting</button>
							</h5>
							<div id="section-list" class="py-2">
								<?php foreach ($sections as $section){ ?>
								<div class="card mb-0 mt-2 draggable-item" id = "<?php echo $section->id; ?>">
									<div class="card-body">
										<div class="media">
											<div class="media-body">
												<h5 class="mb-1 mt-0"><?php echo $section->title; ?></h5>
											</div>
										</div>
									</div>
								<?php } ?>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<script src="<?php echo base_url('assets/vendor/dagula-master/dragula.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/dagula-master/component.dragula.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendor/dagula-master/onDomChange.js'); ?>"></script>
<!-- Init Dragula -->
<script type="text/javascript">
    ! function(r) {
        "use strict";
        var a = function() {
            this.$body = r("body")
        };
        a.prototype.init = function() {
            r('[data-plugin="dragula"]').each(function() {
                var a = r(this).data("containers"),
                t = [];
                if (a)
                for (var n = 0; n < a.length; n++) t.push(r("#" + a[n])[0]);
                else t = [r(this)[0]];
                var i = r(this).data("handleclass");
                i ? dragula(t, {
                    moves: function(a, t, n) {
                        return n.classList.contains(i)
                    }
                }) : dragula(t)
            })
        }, r.Dragula = new a, r.Dragula.Constructor = a
    }(window.jQuery),
    function(a) {
        "use strict";
        window.jQuery.Dragula.init()
    }();
</script>
<script type="text/javascript">
    function sort() {
        var containerArray = ['section-list'];
        var itemArray = [];
        var itemJSON;
        for(var i = 0; i < containerArray.length; i++) {
            $('#'+containerArray[i]).each(function () {
                $(this).find('.draggable-item').each(function() {
                    //console.log(this.id);
                    itemArray.push(this.id);
                });
            });
        }

        itemJSON = JSON.stringify(itemArray);
        console.log(itemJSON)
		/*$.ajax({
            url: '<?php echo site_url('admin/ajax_sort_section/');?>',
            type : 'POST',
            data : {itemJSON : itemJSON},
            success: function(response)
            {
                success_notify('<?php echo 'sections_have_been_sorted'; ?>');
                setTimeout(
                  function()
                  {
                    location.reload();
                }, 1000);

            }
        });*/
    }
    onDomChange(function(){
        $('#section-sort-btn').show();
    });
</script>