<?php if($job === 'faq'):?>
    <div class="modal-header">
        <h5 class="modal-title" id="operationModalLongTitle"><?php echo $title;?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          	<span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="form-group">
                    <label for="question">Question</label>
                    <input type="text" class="form-control" id="question" value="<?php echo $result[0]->txt_question;?>">
                    <span class="h6 text-danger" id="error_question"></span>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="form-group">
                    <label for="answer">Answer</label>
                    <textarea class="form-control" id="answer" rows="3"><?php echo $result[0]->txt_answer;?></textarea>
                    <span class="h6 text-danger" id="error_answer"></span>
                    <script>
						CKEDITOR.replace("answer");
					</script>
                </div>
            </div>
            <input type="hidden" class="form-control" id="page" value="<?php echo $page;?>">
            <input type="hidden" class="form-control" id="rid" value="<?php echo $result[0]->id;?>">
            <input type="hidden" class="form-control" id="pid" value="<?php echo $result[0]->program_id;?>">
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm mr-2" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btn-sm" onclick='<?php echo "{$save}()";?>'>Save</button>
    </div>

<?php elseif($job === 'project'):?>
    <div class="modal-header">
        <h5 class="modal-title" id="operationModalLongTitle"><?php echo $title;?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          	<span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" value="<?php echo $result[0]->txt_project;?>">
                    <span class="h6 text-danger" id="error_title"></span>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="form-group">
                    <label for="details">Details</label>
                    <textarea class="form-control" id="details" rows="3"><?php echo $result[0]->txt_project_dtls;?></textarea>
                    <span class="h6 text-danger" id="error_details"></span>
                    <script>
						CKEDITOR.replace("details");
					</script>
                </div>
            </div>
            <input type="hidden" class="form-control" id="page" value="<?php echo $page;?>">
            <input type="hidden" class="form-control" id="rid" value="<?php echo $result[0]->id;?>">
            <input type="hidden" class="form-control" id="pid" value="<?php echo $result[0]->program_id;?>">
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm mr-2" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btn-sm" onclick='<?php echo "{$save}()";?>'>Save</button>
    </div>

<?php elseif($job === 'speak'):?>
    <div class="modal-header">
        <h5 class="modal-title" id="operationModalLongTitle"><?php echo $title;?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          	<span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" value="<?php echo $result[0]->txt_name;?>">
                    <span class="h6 text-danger" id="error_name"></span>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" value="<?php echo $result[0]->txt_emailid;?>">
                    <span class="h6 text-danger" id="error_email"></span>
                </div>
                <div class="form-group">
                    <label for="phone">Phone</label>
                    <input type="text" class="form-control" id="phone" value="<?php echo $result[0]->num_mobile_no;?>">
                    <span class="h6 text-danger" id="error_phone"></span>
                </div>
                <div class="form-group">
					<img src="<?php echo base_url($result[0]->txt_profile_pic);?>" onerror="src='<?php echo base_url('assets/img/default-avatar.png');?>'" style="width: 250px;" id="img_preview">
					<label for="profile_pic" class="btn btn-sm">Upload Profile Picture</label>
    				<input type="file" class="form-control-file" id="profile_pic" name="profile_pic">
					<script>
						$("#profile_pic").change(function(e){
							$("#img_preview").attr("src",URL.createObjectURL(e.target.files[0]))
						});
					</script>
				</div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="position">Position</label>
                    <input type="text" class="form-control" id="position" value="<?php echo $result[0]->txt_position;?>">
                    <span class="h6 text-danger" id="error_position"></span>
                </div>
                <div class="form-group">
                    <label for="organization">Organization</label>
                    <input type="text" class="form-control" id="organization" value="<?php echo $result[0]->txt_organization;?>">
                    <span class="h6 text-danger" id="error_organization"></span>
                </div>
                <div class="form-group">
                    <label for="message">Message</label>
                    <textarea class="form-control" id="message" rows="3"><?php echo $result[0]->txt_message;?></textarea>
                    <span class="h6 text-danger" id="error_message"></span>
                    <script>
						CKEDITOR.replace("message");
					</script>
                </div>
            </div>
            <input type="hidden" class="form-control" id="preImage" value="<?php echo $result[0]->txt_profile_pic;?>">
            <input type="hidden" class="form-control" id="page" value="<?php echo $page;?>">
            <input type="hidden" class="form-control" id="rid" value="<?php echo $result[0]->id;?>">
            <input type="hidden" class="form-control" id="pid" value="<?php echo $result[0]->program_id;?>">
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm mr-2" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btn-sm" onclick='<?php echo "{$save}()";?>'>Save</button>
    </div>
<?php endif;?>