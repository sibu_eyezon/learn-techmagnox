<div class="row">
    <div class="col-sm-12 col-md-5"></div>
    	<div class="col-sm-12 col-md-7 ml-auto">
        	<nav aria-label="Page navigation example" class="float-right">
            	<ul class="pagination">
                <?php
                    if($count > 0):
                        if($page_number > 1):
                            echo '<li class="page-item "><a class="page-link" href="javascript:'.$func.'('.($page_number - 1).')">Previous</a></li>';
                        else:
                            echo '<li class="page-item "><button class="page-link" href="javascript:void(0)" disabled>Previous</button></li>';
                        endif;
                        
                        if($page_number == 1):
                            for($i=$page_number;$i<=($page_number+2);$i++):
                                if($i == $page_number):
                                    echo '<li class="page-item active"><a class="page-link" href="javascript:'.$func.'('.$i.')">'.$i.'</a></li>';
                                elseif($i <= $last_page):
                                    echo '<li class="page-item"><a class="page-link" href="javascript:'.$func.'('.$i.')">'.$i.'</a></li>';
                                endif;
                            endfor;
                        elseif($page_number != 1 && $page_number < $last_page):
                            for($i=($page_number - 1);$i<=($page_number + 1);$i++):
                                if($i == $page_number):
                                    echo '<li class="page-item active"><a class="page-link" href="javascript:'.$func.'('.$i.')">'.$i.'</a></li>';
                                else:
                                    echo '<li class="page-item"><a class="page-link" href="javascript:'.$func.'('.$i.')">'.$i.'</a></li>';
                                endif;
                            endfor;
                        elseif($page_number != 1 && $page_number == $last_page):
                            for($i=($last_page - 2);$i<=$last_page;$i++):
                                if($i == $page_number):
                                    echo '<li class="page-item active"><a class="page-link" href="javascript:'.$func.'('.$i.')">'.$i.'</a></li>';
                                elseif($i != 0):
                                    echo '<li class="page-item"><a class="page-link" href="javascript:'.$func.'('.$i.')">'.$i.'</a></li>';
                                endif;
                            endfor;
                        endif;
                        if($page_number < $last_page ):
                            echo '<li class="page-item"><a class="page-link" href="javascript:'.$func.'('.($page_number + 1).')">Next</a></li>';
                        else:
                            echo '<li class="page-item "><button class="page-link" href="javascript:void(0)" disabled>Next</button></li>';
                        endif;
                    endif;
                ?>
                </ul>
            </nav>
        </div>
    </div>
</div>