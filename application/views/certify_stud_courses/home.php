<?php
$spc = $this->Member->getStudProgCons($userId, $prog_id);
if(!empty($spc)){
	$status = $spc[0]->status;
	$certificate = $spc[0]->certificate;
}else{
	$status = '0';
	$certificate = "";
}
$plevel = $prog[0]->prog_level;
$ptype=intval(trim($prog[0]->type));
$bolcertify = $prog[0]->bolcertify;
$aflag = intval(trim($prog[0]->approve_flag));
?>
<div class="col-md-8">
	<div class="card shadow mt-2">
		<div class="card-header">
			<h4 class="card-title font-weight-bold text-dark">Configuration</h4>
		</div>
		<div class="card-body">
			<ul class="list-column">
				<li><?php echo 'Status: '.(($status=='1')? 'Ongoing' : (($status=='2')? 'Completed' : (($status=='3')? 'Failed' : 'Pending'))); ?></li>
				<li><?php echo 'Admission start: '.date('jS M Y',strtotime($prog[0]->astart_date)); ?></li>
				<li><?php echo date('jS M Y',strtotime($prog[0]->start_date)).' - '.date('jS M Y',strtotime($prog[0]->end_date)); ?></li>
				<li><?php echo 'No. of seats: '.trim($prog[0]->total_seat); ?></li>
				<li><?php echo (trim($prog[0]->feetype)=='Paid')? 'Rs. '.$prog[0]->total_fee.' / Discount: '.$prog[0]->discount.' %' : 'Free'; ?></li>
				<li><?php echo 'Approval type: '.(($prog[0]->apply_type=='0')? 'All approval':'Selective approval'); ?></li>
				<li><?php echo 'Deadline: '.date('jS M Y',strtotime($prog[0]->aend_date)); ?></li>
				<li><?php echo 'Total Hours: '.trim($prog[0]->prog_hrs).' Hrs'; ?></li>
				<?php if($plevel!=""){ ?>
				<li><?= 'Level: '.(($plevel=='3')? 'Advanced' : (($plevel=='2')? 'Beginner' : 'Intermediate')); ?></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="card shadow mt-2">
		<div class="card-body">
			<?php
				if($aflag==0){
					echo '<h6>Your application is pending for first approval</h6>';
				}else if($aflag==1){
					echo '<h6>Your application is pending for final approval</h6>';
				} if($aflag==2){
					if($status=='2'){
						if($bolcertify=='auto'){
							echo '<h6 class="text-center"><a href="'.base_url('Student/studCertificate/?id='.base64_encode($prog_id)).'" target="_blank"><i class="material-icons">cloud_download</i> Download Certificate</a><br></h6>';
						}else if($bolcertify=='manual'){
							if($certificate!=""){
								if(file_exists('./'.$certificate)){
									echo '<h6 class="text-center"><a href="'.base_url($certificate).'" target="_blank"><i class="material-icons">cloud_download</i> Download Certificate</a><br></h6>';
								}
							}
						}else if($bolcertify=='no'){
							echo '<h6 class="text-center">No certificate.</h6>';
						}
						
					}else if($status=='1'){
						echo '<h6>Your course is ongoing.</h6>';
					}else{
						echo '<h6>Your course is pending.</h6>';
					}
				}
				
			?>
		</div>
	</div>
</div>
<aside class="col-md-4">
	<div class="card">
		<div class="card-header card-header-info">
			<h4 class="card-title">Notices
			</h4>
		</div>
		<div class="card-body">
			<div class="notice_ticker" style="height:40vh;">
				<ul class="list-group w-100" id="notc_details">
				
				</ul>
			</div>
		</div>
		<div class="card-footer">
			<a href="<?php echo base_url('Teacher/notices'); ?>" class="btn btn-info btn-sm pull-right">Know More</a>
		</div>
	</div>
</aside>
<script>
	$(document).ready(()=>{
		getProgramNotices();
	});
</script>