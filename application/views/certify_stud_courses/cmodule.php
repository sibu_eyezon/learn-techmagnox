<?php
$section = $this->AutoProgramModel->get_section($prog_id);
//$csec = count($section);
?>
<div class="col-xl-12">
	<h4 class="mx-3">Curriculum</h4>
	<hr style="border:2px solid #eee;">
	<?php
		$i=1;
		foreach($section as $secrow){
			$sec_id = $secrow->id;
	?>
	<div class="card bg-dark text-white mt-0" id="sec_<?php echo $sec_id; ?>">
		<div class="card-header">
			<h4 class="card-title">Module <?php echo $i; ?>: <?php echo $secrow->title; ?></h4>
		</div>
		<div class="card-body">
			<?php
				$clra = $this->Course_model->getAllCourseSubDetails($sec_id);
				if(!empty($clra)){
				foreach($clra as $lra){
					$lra_type = trim($lra->type);
					$lra_id = $lra->sl;
					$lrtitle =trim($lra->title);
			?>
			<div class="card bg-info mt-0" id="<?php echo $lra_type.'_'.$lra_id; ?>">
				<div class="card-body">
					<h4 class="card-title"><?php echo ucfirst($lra_type).': '.$lrtitle; ?></h4>
					<?php
						if($lra_type=='lecture'){
							$lfile = $this->Course_model->getLectureFile($lra_id);
							if($lfile[0]->file_type=='fl'){
								if(trim($lfile[0]->file_name)!=""){
									if(file_exists('./uploads/courselra/'.$lfile[0]->file_name)){
										echo '<a href="'.base_url('uploads/courselra/'.$lfile[0]->file_name).'" target="_blank"><i class="material-icons">download</i> Download File</a>';
									}
								}
							}else if($lfile[0]->file_type=='yt'){
								echo '<a href="'.$lfile[0]->link.'" target="_blank"></a>';
							}
						}else if($lra_type=='resource'){
							$crfiles = $this->Course_model->getCResourceFileByRID($lra_id);
							foreach($crfiles as $frow){
								$isrc = '';
								$cfid = $frow->sl;
								$rtype = trim($frow->type);
								$ftype = trim($frow->file_type);
								if($ftype=='pdf'){
									$isrc = 'fa-file-pdf-o';
								}else if($ftype=='doc' || $ftype=='docx'){
									$isrc = 'fa-file-word-o';
								}else if($ftype=='xls' || $ftype=='xlsx'){
									$isrc = 'fa-file-excel-o';
								}else if($ftype=='jpg'){
									$isrc = 'fa-file-image-o';
								}else if($ftype=='mp4'){
									$isrc = 'fa-file-video-o';
								}else{
									$isrc = 'fa-file';
								}
								if($rtype=='yt'){
									echo '<div class="alert alert-primary alert-with-icon mt-3" id="alert_'.$cfid.'" data-notify="container">
											<i class="fa fa-youtube-play" data-notify="icon"></i>
											<span data-notify="icon" class="now-ui-icons ui-1_bell-53"></span>
											<span data-notify="message"><a href="'.trim($frow->linkfile).'" target="_blank"> View Video</a></span>
										</div>';
								}else if($rtype=='lk'){
									echo '<div class="alert alert-primary alert-with-icon mt-3" id="alert_'.$cfid.'" data-notify="container">
											<i class="material-icons" data-notify="icon">link</i>
											<span data-notify="icon" class="now-ui-icons ui-1_bell-53"></span>
											<span data-notify="message"><a href="'.trim($frow->linkfile).'" target="_blank">View Link</a></span>
										</div>';
								}else if($rtype=='fl'){
									echo '<div class="alert alert-primary alert-with-icon mt-3" id="alert_'.$cfid.'" data-notify="container">
											<i class="fa '.$isrc.'" data-notify="icon"></i>
											<span data-notify="icon" class="now-ui-icons ui-1_bell-53"></span>
											<span data-notify="message"><a href="'.base_url('uploads/cresources/'.trim($frow->linkfile)).'" target="_blank"> Download</a></span>
										</div>';
								}
							}
						}else if($lra_type=='assignment'){
							$cafiles = $this->Course_model->getAssignmentFilesById($lra_id);
							foreach($cafiles as $frow){
								$isrc = '';
								$caid = $frow->sl;
								$rtype = trim($frow->file_type);
								$ftype = trim($frow->file_ext);
								if($ftype=='pdf'){
									$isrc = 'fa-file-pdf-o';
								}else if($ftype=='doc' || $ftype=='docx'){
									$isrc = 'fa-file-word-o';
								}else if($ftype=='xls' || $ftype=='xlsx'){
									$isrc = 'fa-file-excel-o';
								}else if($ftype=='jpg'){
									$isrc = 'fa-file-image-o';
								}else{
									$isrc = 'fa-file';
								}
								if(file_exists('./uploads/cassignments/'.$frow->file_name)){
									echo '<div class="alert alert-primary alert-with-icon mt-3" id="alert_'.$caid.'" data-notify="container">
										<i class="fa '.$isrc.'" data-notify="icon"></i>
										<button type="button" class="close" onClick="deleteAssgnFiles('.$caid.');" aria-label="Close">
										  <i class="material-icons">close</i>
										</button>
										<span data-notify="icon" class="now-ui-icons ui-1_bell-53"></span>
										<span data-notify="message"><a href="'.base_url().'uploads/cassignments/'.$frow->file_name.'" target="_blank"> Download</a></span>
									</div>';
								}
							}
						}
					?>
				</div>
			</div>
			<?php
					}
				}
			?>
		</div>
	</div>
	<?php $i++; } ?>
</div>
