<?php
	$institute = $this->Member->getProgramOrg($prog_id);
?>
<div class="col-md-12">
	<h4 class="mx-3">Organisation</h4>
	<hr style="border:2px solid #eee;">
	<div class="material-datatables">
		<table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th width="10%">Sl.</th>
					<th width="90%">Details</th>
				</tr>
			</thead>
			<tbody>
				<?php $i=1; foreach($institute as $irow){ ?>
					<tr>
						<td><?php echo $i; ?></td>
						<td style="word-break:break-all;">
							<?php 
								if($irow->logo!=""){
									if(file_exists('./assets/img/institute/'.$irow->logo)){
										echo '<img src="'.base_url('assets/img/institute/'.$irow->logo).'" class"img-thumbnail" style="width: 60px; float:left;margin-right: 5px;"';
									}
								}
								echo '<p>'.$irow->title; 
								if($irow->website!=""){
									echo '<br>'.$irow->website;
								}
								if($irow->contact_info!=""){
									echo '<br>'.$irow->contact_info;
								}
								echo '</p>';
							?>
						</td>
					</tr>
				<?php $i++; } ?>
			</tbody>
		</table>
	</div>
</div>