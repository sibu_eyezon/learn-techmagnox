<div class="col-md-12">
	<h4 class="mx-3">Assignments</h4>
	<hr style="border:2px solid #eee;">
	<div class="row">
		<?php
			$lectures = $this->Course_model->get_all_assignments_by_prog_id($prog_id);
			$userid = $_SESSION['userData']['userId'];
			if(!empty($lectures)){
				foreach($lectures as $clow){
					$lra_id = $clow->sl;
					$ltitle = trim($clow->title);
					$tdate = date('dmY',strtotime($clow->tdate));
					$deadline = date('dmY',strtotime($clow->deadline));
					$curdate = date('dmY');
					$course_sl = $clow->course_sl;
					$prof_sl = $clow->user_id;
					$assgn_sub = $this->Course_model->getAssignmentSubmission($lra_id, $userid);
		?>
		<div class="col-sm-4">
			<div class="card" style="box-shadow: 0 3px 15px 0 rgb(0 0 0 / 30%) !important;" id="assignment_<?php echo $lra_id; ?>">
				<div class="card-header">
					<h5 class="card-title"><?php echo $ltitle; ?></h5>
				</div>
				<div class="card-body text-center">
					<?php
						$cafiles = $this->Course_model->getAssignmentFilesById($lra_id);
						foreach($cafiles as $frow){
							$isrc = '';
							$caid = $frow->sl;
							$rtype = trim($frow->file_type);
							$ftype = trim($frow->file_ext);
							if(file_exists('./uploads/cassignments/'.$frow->file_name)){
								echo '<a href="'.base_url('uploads/cassignments/'.$frow->file_name).'" target="_blank"><img src="'.base_url('assets/img/icons/assignment.png').'" class="img-responsive" width="80"/></a>';
							}
						}
					?>
					<h6>Added on: <?php echo date('jS M Y', strtotime($clow->tdate)); ?></h6>
					<h6>Deadline: <?php echo date('jS M Y', strtotime($clow->deadline)); ?></h6>
					<h6>Marks <?php echo trim($clow->marks); ?></h6>
					<h6>By <?php echo trim($clow->uname); ?></h6>
					<h6>Module: <?php echo trim($clow->pc_title); ?></h6>
					<?php
						if(count($assgn_sub)>0){
							echo '<h6 class="text-danger">Submitted</h6>';
						}
					?>
				</div>
				<div class="card-footer text-center">
					<?php
						if(($curdate >= $tdate) && ($curdate <= $deadline)){
							if(count($assgn_sub)<=0){
								echo '<h4 class="text-center"><button type="button" onClick="uploadAssignment('.$lra_id.')" class="btn btn-info btn-sm">Upload your assignment</button></h4>';
							}else{
								$marks = intval(trim($assgn_sub[0]->marks));
								echo 'Marks: '.(($marks==0)? 'Not received': $marks);
							}
							
						}else{
							if(count($assgn_sub)>0){
								$marks = intval(trim($assgn_sub[0]->marks));
								echo 'Marks: '.(($marks==0)? 'Not received': $marks);
							}
						}
					?>
				</div>
			</div>
		</div>
		<?php
				}
			}
		?>
	</div>
</div>
<div class="modal fade" id="assignModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	  <div class="modal-content">
		<div class="modal-header">
		  <h4 class="modal-title">Submit Your Assignment</h4>
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			<i class="material-icons">clear</i>
		  </button>
		</div>
		<div class="modal-body">
			<form class="form" id="frmAssgnFiles" enctype="multipart/form-data">
				<div class="form-group">
					<input type="hidden" value="0" name="ass_sl" id="ass_sl">
					<label for="rtitle" class="text-dark">Assignment Submission</label>
					<label class="text-danger">You must upload only one assignment in given file types below:</label>
				</div>
				<div class="form-group">
					<code>PDF/WORD/EXCEL/ZIP/PPT</code>
					<div class="custom-file" style="margin-bottom:20px;">
						<input type="file" class="custom-file-input" onChange="customFileName('fl_link01')" name="fl_link01" id="fl_link01" accept=".pdf, .doc, .docx, .xls, .xlsx, .ppt, .pptx, .zip" required="true">
						<label class="custom-file-label" for="fl_link01">Choose Files</label>
					</div>
				</div>
				<div class="form group mt-3">
					<label for="rdetails" class="text-dark">Description</label>
					<textarea class="form-control w-100" cols="80" rows="5" name="rdetails" id="rdetails" placeholder="If you want to write something...(* not mandatory)"></textarea>
				</div>
				<div class="form group">
					<button type="submit" class="btn btn-success btn-sm pull-right">Save</button>
					<input type="reset" style="visibility:hidden;"/>
					<button type="button" class="btn btn-danger btn-sm pull-left" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	  </div>
	</div>
</div>
<script>
function uploadAssignment(assn_id)
{
	$('#frmAssgnFiles')[0].reset();
	$('#ass_sl').val(assn_id);
	$('#assignModal').modal('show');
}
function customFileName(customid)
{
	var fileName = $('#'+customid).val().split("\\").pop();
	$('#'+customid).siblings(".custom-file-label").addClass("selected").html(fileName);
}
$('#frmAssgnFiles').on('submit', (e)=>{
	e.preventDefault();
	var frmData = new FormData($('#frmAssgnFiles')[0]);
	$.ajax({
		url: baseURL+'Student/submitAssignment',
		type: 'POST',
		data: frmData,
		enctype: 'multipart/form-data',
		processData: false,
		contentType: false,
		success: (res)=>{
			$('#frmAssgnFiles')[0].reset();
			$('#ass_sl').val(0);
			$('#assignModal').modal('hide');
			if(res){
				$.notify({icon:"add_alert",message:'Your Assignment has been uploaded successfully.'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})
				getProgMenuContents('cassignments');
			}else{
				$.notify({icon:"add_alert",message:'Something went wrong. Please try again.'},{type:'warning',timer:3e3,placement:{from:'top',align:'right'}})
			}
			
		}
	});
});
</script>