<?php
	$pprof = $this->Member->getAllRequestByIdRole($prog_id, 'Teacher', NULL, 'accepted');
?>
<div class="col-md-12">
	<h4 class="mx-3">Instructors </h4>
	<hr style="border:2px solid #eee;">
	<table class="table table-striped table-no-bordered table-hover dtinsstrm" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th width="5%">Sl.</th>
				<th width="60%" colspan="2">Name</th>
				<th width="20%">Email</th>
				<th width="15%">Phone</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=1; foreach($pprof as $trow){ ?>
				<tr>
					<td><?php echo $i; ?></td>
					<td width="9%"><?php echo '<img src="'.base_url().$trow->photo_sm.'" onerror="this.src=`'.base_url('assets/img/default-avatar.png').'`" class="avatar-dp mr-2" />'; ?></td>
					<td><?php echo '<div class="td-name">'.$trow->name.'</div>'; ?></td>
					<td><?php //echo $trow->email; ?><code> - protected</code></td>
					<td><?php //echo $trow->phone; ?><code> - protected</code></td>
				</tr>
			<?php $i++; } ?>
		</tbody>
	</table>
</div>