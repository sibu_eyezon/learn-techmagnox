<div class="col-md-12">
	<h4 class="mx-3">Resources</h4>
	<hr style="border:2px solid #eee;">
	<div class="row">
		<?php
			$lectures = $this->Course_model->get_all_resources_by_prog_id($prog_id);
			if(!empty($lectures)){
				foreach($lectures as $clow){
					$lra_id = $clow->sl;
					$ltitle = trim($clow->title);
		?>
		<div class="col-sm-3">
			<div class="card" style="box-shadow: 0 3px 15px 0 rgb(0 0 0 / 30%) !important;" id="resource_<?php echo $lra_id; ?>">
				<div class="card-header">
					<h5 class="card-title"><?php echo $ltitle; ?></h5>
				</div>
				<div class="card-body text-center">
					<?php
						$crfiles = $this->Course_model->getCResourceFileByRID($lra_id);
						foreach($crfiles as $frow){
							$isrc = '';
							$cfid = $frow->sl;
							$rtype = trim($frow->type);
							if($rtype=='yt'){
								echo '<a href="javascript:;" onClick="openYTModal('.$cfid.')"><img src="'.base_url('assets/img/icons/youtube.png').'" class="img-responsive" width="80"/></a>';
							}else if($rtype=='lk'){
								echo '<a href="'.$frow->linkfile.'" target="_blank"><img src="'.base_url('assets/img/icons/link.png').'" class="img-responsive" width="80"/></a>';
							}else if($rtype=='fl'){
								echo '<a href="javascript:;" onClick="getResFile('.$cfid.')"><img src="'.base_url('assets/img/icons/resources.png').'" class="img-responsive" width="80"/></a>';
							}
						}
					?>
					<h6>Added on: <?php echo date('jS M Y h:i a', strtotime($clow->add_date)); ?></h6>
					<h6>By <?php echo trim($clow->uname); ?></h6>
					<h6>Module: <?php echo trim($clow->pc_title); ?></h6>
				</div>
			</div>
		</div>
		<?php
				}
			}
		?>
	</div>
</div>
<div class="modal fade" id="YTModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	  <div class="modal-content">
		<div class="modal-header">
		  <h4 class="modal-title">Youtube Video</h4>
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			<i class="material-icons">clear</i>
		  </button>
		</div>
		<div class="modal-body" id="ytbody">
		</div>
		<div class="modal-footer">
		  <button type="button" class="btn btn-danger btn-link" data-dismiss="modal">Close</button>
		</div>
	  </div>
	</div>
</div>

<script>
function openYTModal(res_id)
{
	$.ajax({
		url:baseURL+'Teacher/getResourceYTLink/?crid='+res_id,
		type: 'GET',
		success: (res)=>{
			var obj = JSON.parse(res);
			if((obj[0].linkfile).trim()!=""){
				
				$('#YTModal #ytbody').html('<div class="plyr__video-embed" id="player"><iframe height="350" src="'+obj[0].linkfile+'?origin=https://plyr.io&amp;iv_load_policy=3&amp;modestbranding=1&amp;playsinline=1&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1" allowfullscreen allowtransparency allow="autoplay"></iframe></div>');
				const player = new Plyr('#player');
				$('#YTModal').modal('show');
			}else{
				$.notify({icon:"add_alert",message: "No Youtube Link Found."},{type:"danger",timer:3e3,placement:{from:'top',align:'right'}})
			}
			
		},
		error: (errors)=>{
			console.log(errors);
		}
	});
}
$('#YTModal').on('hide.bs.modal', function (e) {
	$('#YTModal #ytbody').html("");
})

function getResFile(res_id)
{
	$('#loading').show();
	$.ajax({
		url:baseURL+'Teacher/getResourceFiles/?crid='+res_id,
		type: 'GET',
		success: (res)=>{
			$('#loading').hide();
			if(res!='0'){
				var obj = JSON.parse(res);
				var ftype = obj[0].file_type.trim();
				if(ftype=='mp4'){
					
					$('#YTModal #ytbody').html('<video id="player" width="100%" height="350px" playsinline controls><source src="'+baseURL+'/uploads/cresources/'+obj[0].linkfile+'" type="video/mp4"></video>');
					const player = new Plyr('#player');
					$('#YTModal').modal('show');
				}else{
					$.get(baseURL+'uploads/cresources/'+obj[0].linkfile)
					.done(function() { 
						window.open(baseURL+'uploads/cresources/'+obj[0].linkfile, '_blank');
					}).fail(function() { 
						$.notify({
							icon:"add_alert",
							message: "Sorry! File is missing."},
							{type:"danger",
							timer:3e3,
							placement:{from:'top',align:'right'}
						})
					});
				}
				
			}else{
				$.notify({
					icon:"add_alert",
					message: "Sorry! File is missing."},
					{type:"danger",
					timer:3e3,
					placement:{from:'top',align:'right'}
				});
			}
		},
		error: (errors)=>{
			console.log(errors);
		}
	});
}
</script>