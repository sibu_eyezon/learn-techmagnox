<?php
	$ftype = trim($prog[0]->feetype);
	$category = trim($prog[0]->category);
?>
<div class="card">
	<div class="card-header card-header-info card-header-text">
		<div class="card-text">
			<h4 class="card-title">Student List under: <?php echo trim($prog[0]->title).' - ('.trim($prog[0]->yearnm).')'; ?></h4>
		</div>
	</div>
	<div class="card-body">
		<form id="frmFtApprove">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<select name="semid" id="semid" class="custom-select">
							<?php
								if(!empty($sems)){
									$csems = count($sems);
									if($csems==1){
										echo '<option value="'.$sems[0]->id.'" selected>'.$sems[0]->title.'</option>';
									}else{
										foreach($sems as $ssow){
											echo '<option value="'.$ssow->id.'">'.$ssow->title.'</option>';
										}
									}
								}
							?>
						</select>
					</div>
				</div>
				<div class="col-md-8">
					<div class="form-group">
						<button class="btn btn-success btn-sm mr-2" id="fla" disabled>Final Approval</button>
						<?php if($category!="Seminar Program" && $category!="Webinar Program"){ ?>
						<button class="btn btn-warning btn-sm mr-2" id="raf" disabled>Reject</button>
						<?php } ?>
						<span class="text-danger">Enrollment and Roll are required, else it will not update.</span>
					</div>
				</div>
			</div>
			
			<input type="hidden" name="prog_id2" id="prog_id2" value="<?php echo $prog[0]->id; ?>"/>
			<input type="hidden" name="feetype2" id="feetype2" value="<?php echo ($ftype=='Paid')? 1:0; ?>"/>
			<div class="material-datatables">
				<table class="table table-striped table-no-bordered table-hover datatables" id="tbl_adm" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th width="5%">#</th>
							<th width="5%">
								<div class="form-check mr-2">
								  <label class="form-check-label">
									<input class="form-check-input" type="checkbox" value="1" id="check_all_students" data-to-table="tasks">
									<span class="form-check-sign">
									  <span class="check"></span>
									</span>
								  </label>
								</div>
							</th>
							<th width="20%">Name</th>
							<th width="10%">Email</th>
							<th width="10%">Phone</th>
							<th width="15%">Enrollment</th>
							<th width="15%">Roll</th>
							<th width="15%">Payment</th>
							<th width="5%">Status</th>
						</tr>
					</thead>
					<tbody>
						<?php 
							$i=1;
							foreach($adm_list as $afow){
								$aid = $afow->sl;
								echo '<tr>';
								echo '<td>'.$i.'</td>';
								echo '<td>';
								if($afow->approve_flag=='1'){
									echo '<div class="form-check">
									  <label class="form-check-label">
										<input class="form-check-input chk_user2" type="checkbox" value="'.$aid.'" name="acp2[]" id="acp2_'.$i.'">
										<span class="form-check-sign">
										  <span class="check"></span>
										</span>
									  </label>
									</div>';
								}
								echo '<input type="hidden" name="spcid_'.$aid.'" id="spcid_'.$aid.'" value="'.((!empty(${'uroll_'.$i}[0]->spc_id))? ${'uroll_'.$i}[0]->spc_id:'').'"/>
								</td>';
								echo '<td>'.trim($afow->stud_name).'<br>';
								$vf = trim($afow->verification_status);
								echo (($vf=='f')? '<span class="label label-danger">Not Verified</span>':'<span class="label label-success">Verified</span>').'</td>';
								echo '<td>'.trim($afow->email).'</td>';
								echo '<td>'.trim($afow->phone).'</td>';
								//echo '<td><input type="text" name="enroll_'.$aid.'" id="enroll_'.$aid.'" class="form-control"/></td>';
								//echo '<td></td>';
								echo '<td>';
								if($afow->approve_flag=='2' || $afow->approve_flag=='1'){
									if(!empty(${'uroll_'.$i}[0]->enrollment_no)){
										echo ${'uroll_'.$i}[0]->enrollment_no;
										echo '<input type="hidden" name="enroll_'.$aid.'" id="enroll_'.$aid.'" value="'.trim(${'uroll_'.$i}[0]->enrollment_no).'" class="form-control"/>';
									}else{
										echo '<input type="text" name="enroll_'.$aid.'" id="enroll_'.$aid.'" class="form-control"/>';
									}
								}else{
									echo 'NIL';
								}
								echo '</td>';
								echo '<td>';
								if($afow->approve_flag=='2' || $afow->approve_flag=='1'){
									if(!empty(${'uroll_'.$i}[0]->roll_no)){
										echo ${'uroll_'.$i}[0]->roll_no;
									}else{
										echo '<input type="number" name="roll_'.$aid.'" id="roll_'.$aid.'" class="form-control"/>';
									}
								}else{
									echo 'NIL';
								}
								echo '</td>';
								echo '<td>';
								if($ftype=='Paid'){
									
									if(!empty(${'uroll_'.$i})){
										$totalfees = intval(${'uroll_'.$i}[0]->totalfees);
										$discount = intval(${'uroll_'.$i}[0]->discount);
										$pdone = intval(${'uroll_'.$i}[0]->payment_done);
										if($discount != 0){
											$totalfees = $totalfees*(1-($discount/100));
										}
										echo 'Amount: Rs. '.$totalfees.'<br>';
										if($pdone == 0){
											echo '<span class="label label-warning">Payment Incomplete</span>';
										}else if($totalfees - $pdone ===0){
											echo '<span class="label label-success">Payment Complete</span>';
										}else if($totalfees - $pdone != 0){
											echo '<span class="label label-info">Payment Done: Rs. '.($totalfees - $pdone).'</span>';
										}
									}else{
										echo '<span class="label label-danger">Payment data not found</span>';
									}
								}else{
									echo 'Free';
								}
								echo '</td><td>';
								$af = intval(trim($afow->approve_flag));
								if($af==0){
									echo '<span class="label label-warning">Pending</span>';
								}else if($af==1){
									echo '<span class="label label-info">First Approval</span>';
								}else if($af==2){
									echo '<span class="label label-success">Final Approval</span>';
								}else if($af==3){
									echo '<span class="label label-danger">Rejected</span>';
								}
								echo '</td></tr>';
								$i++;
							}
						?>
					</tbody>
				</table>
			</div>
		</form>
	</div>
</div>

<script>
	$(document).ready(function(){
		$('#tbl_adm').DataTable({
			"columnDefs": [{
				  "targets": [1],
				  "orderable": false,
			}],
			"ordering": true,
			"info": true,
			"paging": true,
			"lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
			"lengthChange": true,
			dom: '1B<"top"lf>rt<"bottom"ip><"clear">',
			buttons: [
				{
					extend: 'excelHtml5',
					title: 'Admission List of <?php echo trim($prog[0]->title).' - '.trim($prog[0]->yearnm); ?>',
					text:'<i class="fa fa-file-excel-o"></i>',
					className: 'bg-success btn-sm text-white mr-2',
					exportOptions: {
						columns: [0, 2, 3, 4, 5, 6, 7]
					}
				},
				{
					extend: 'pdfHtml5',
					title: 'Admission List of <?php echo trim($prog[0]->title).' - '.trim($prog[0]->yearnm); ?>',
					text: '<i class="fa fa-file-pdf-o"></i>',
					className: 'bg-success btn-sm text-white mr-2',
					exportOptions: {
						columns: [0, 2, 3, 4, 5, 6, 7]
					}
				},
				{
					extend: 'print',
					title: 'Admission List of <?php echo trim($prog[0]->title).' - '.trim($prog[0]->yearnm); ?>',
					text: '<i class="fa fa-print"></i>',
					className: 'bg-success btn-sm text-white mr-2',
					exportOptions: {
						columns: [0, 2, 3, 4, 5, 6, 7]
					}
				}
			]
		});
		$('[name="tbl_adm_length"]').addClass('browser-default');
	});
	
	/**********************APPROVE LIST**********************/
	$('#check_all_students').on('change', ()=>{
		var stud_row, checked=false;
		stud_row = $('#tbl_adm').find('tbody tr');
		checked = $('#check_all_students').prop('checked');
		if(checked){
			$('#fla').removeAttr('disabled');
			$('#raf').removeAttr('disabled');
		}else{
			$('#fla').attr('disabled', true);
			$('#raf').attr('disabled', true);
		}
		$.each(stud_row, function() {
		  var checkbox = $($(this).find('td').eq(1)).find('input').prop('checked', checked);
	   });
	});
	
	$('.chk_user2').on('change', ()=>{
		if($('.chk_user2').is(':checked')){
			$('#fla').removeAttr('disabled');
			$('#raf').removeAttr('disabled');
		}else{
			$('#fla').attr('disabled', true);
			$('#raf').attr('disabled', true);
		}
	});
	
	$('#fla').on('click', (e)=>{
		e.preventDefault();
		var counter = parseInt(<?php echo $i; ?>);
		var i=1;
		var j=0;
		for(i=1; i<=counter; i++){
			if($('#acp2_'+i).is(':checked')){
			  j++;
		  } 
		}
	   if(j!=0){
		   var frmData = new FormData($('#frmFtApprove')[0]);
		   swal({
				title: 'Are you sure?',
				text: "You want to change status to Final Approval for selected student(s)",
				type: 'warning',
				showCancelButton: true,
				confirmButtonClass: 'btn btn-success',
				cancelButtonClass: 'btn btn-danger',
				confirmButtonText: 'Yes, Approve it!',
				buttonsStyling: false
			}).then(function(result) {
				if(result.value) {
					$('#prg_progress').html('Updating the student list.');
					$('#prg_progress').width(0);
					$('#loading').show();
					$.ajax({
						xhr: function () {
							var xhr = new window.XMLHttpRequest();
							xhr.upload.addEventListener("progress", function (evt) {
								if (evt.lengthComputable) {
									var percentComplete = evt.loaded / evt.total;
									console.log(percentComplete);
									$('#prg_progress').css({
										width: percentComplete * 100 + '%'
									});
									if (percentComplete === 1) {
										$('#prg_progress').hide();
									}
								}
							}, false);
							xhr.addEventListener("progress", function (evt) {
								if (evt.lengthComputable) {
									var percentComplete = evt.loaded / evt.total;
									console.log(percentComplete);
									$('#prg_progress').css({
										width: percentComplete * 100 + '%'
									});
								}
							}, false);
							return xhr;
						},
						url: baseURL+'Teacher/finalSelectedStud',
						type: 'POST',
						data: frmData,
						processData: false,
						contentType: false,
						success: (res)=>{
							$('#loading').hide();
							$('#prg_progress').hide();
							$('#prg_progress').html('Loading the list. Please wait...');
							console.log(res)
							if(res=='0')
							{
								$.notify({icon:"add_alert",message:'Something went worng.'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})
							}else if(res=='1'){
								getAdmissionList(<?php echo $prog[0]->id; ?>, '<?php echo $atype; ?>');
								$.notify({icon:"add_alert",message:'The '+j+' student(s) has been selected for final approval.'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})
							}else if(res=='2'){
								$.notify({icon:"add_alert",message:'Courses are not added/not linked with semesters'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})
							}
						}
					})
				}
			});
	   }else{
		   $.notify({icon:"add_alert",message:'No student has been selected'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})
	   }
	})
	$('#raf').on('click', ()=>{
		e.preventDefault();
		var cb = [];
		var i=1;
		var j=0;
		$('.chk_user2').each(function() {
		  if($('#acp2_'+i).is(':checked')){
			  j++;
		  } 
		  i++;
	   });
	   var frmData = new FormData($('#frmFtApprove')[0]);
		swal({
			title: 'Are you sure?',
			text: "You want to reject selected student(s)",
			type: 'warning',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: 'Yes, Reject it!',
			buttonsStyling: false
		}).then(function(result) {
			if(result.value) {
				$('#loading').show();
				$.ajax({
					url: baseURL+'Teacher/rejectApprovedSelectedStud',
					type: 'POST',
					data: frmData,
					processData: false,
					contentType: false,
					success: (res)=>{
						$('#loading').hide();
						//console.log(res)
						if(res)
						{
							getAdmissionList(<?php echo $prog[0]->id; ?>, '<?php echo $atype; ?>');
							$.notify({icon:"add_alert",message:'The '+j+' student(s) has been rejected.'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})
						}else{
							$.notify({icon:"add_alert",message:'Something went worng.'},{type:'warning',timer:3e3,placement:{from:'top',align:'right'}})
						}
					}
				})
			}
		});
	});
	/********************************************************/
</script>