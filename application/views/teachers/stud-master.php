<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" />

<script src="<?php echo base_url(); ?>assets/js/plugins/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>  
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<style>
.icon-container {
  position: absolute;
  top: calc(50% - 10px);
}
.loader {
  position: relative;
  height: 20px;
  width: 20px;
  display: inline-block;
  animation: around 5.4s infinite;
}

@keyframes around {
  0% {
    transform: rotate(0deg)
  }
  100% {
    transform: rotate(360deg)
  }
}

.loader::after, .loader::before {
  content: "";
  background: white;
  position: absolute;
  display: inline-block;
  width: 100%;
  height: 100%;
  border-width: 2px;
  border-color: #333 #333 transparent transparent;
  border-style: solid;
  border-radius: 20px;
  box-sizing: border-box;
  top: 0;
  left: 0;
  animation: around 0.7s ease-in-out infinite;
}

.loader::after {
  animation: around 0.7s ease-in-out 0.1s infinite;
  background: transparent;
}
</style>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-info">
                        <h3 class="card-title">Student Master</h3>
                    </div>
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12 d-flex">
								<div class="form-group mr-3">
									<select class="selectpicker" data-style="select-with-transition" name="acaprog" id="acaprog" data-title="Set academic program <span class='text-danger'>*</span>">
										<?php
											if(!empty($academic_program)){
												foreach($academic_program as $row){
												echo '<option value="'.$row->id.'">'.trim($row->title).' ('.trim($row->code).')<br>'.$row->yearnm.'</option>';
												}
											}
										?>
									</select>
									<span id="err_prog" class="text-danger small"></span>
								</div>
								<div class="form-group mr-3">
									<div class="icon-container"><span class="loader"></span></div>
									<select class="selectpicker" data-style="select-with-transition" name="acasem" id="acasem" data-title="Set semester <span class='text-danger'>*</span>">
									</select>
									<span id="err_sem" class="text-danger small"></span>
								</div>
								<!--<div class="form-group mr-3">
									<select class="selectpicker" data-style="select-with-transition" name="acayear" id="acayear" data-title="Set academic year <span class='text-danger'>*</span>">
									<?php
										/*if(!empty($academic_year)){
											foreach($academic_year as $row){
												echo '<option value="'.$row['sl'].'">'.$row['yearnm'].'</option>';
											}
										}*/
									?>
									</select>
									<span id="err_year" class="text-danger small"></span>
								</div>-->
								<div class="form-group">
								<button class="btn btn-primary btn-sm" id="stud-serach" onClick="studentManagerTable();">view</button>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 d-flex">
								<div class="form-group mr-3">
									<select class="selectpicker" data-style="select-with-transition" name="all_status" id="all_status" data-title="Change Status">
										<option value="1">Started</option>
										<option value="2">Completed</option>
										<option value="3">Failed</option>
									</select>
								</div>
								<div class="form-group">
								<button type="button" class="btn btn-success btn-sm" onClick="allStatusChange();">Save</button>
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
            <div class="col-lg-12 col-md-12">
                <div class="card mt-0">
                    <div class="card-body">
                        <div class="material-datatables">
                            <span id="success_msg"></span>
                            <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" id="studentDataTable">
                                <thead>
                                    <tr>
                                        <th>
                                            <input type="checkbox" id="mainCheckBox">
                                        </th>
                                        <th>Name</th>
                                        <th>Enrolment</th>
                                        <th>Phone</th>
                                        <th>Percentage</th>
                                        <th>CGPA</th>
                                        <th>Finance</th>
                                        <th>Status</th>
                                        <th>Certificate</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="student_master_list">
                                 
                                </tbody>
                                <div class="progress-bar progress-bar-striped progress-bar-animated" id="prg_progress" style="width: 100%; display:none;">Loading the list. Please wait...</div>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url('assets/js/student-master.js'); ?>"></script>
<script src="<?php //echo base_url('assets/js/admin-student.js'); ?>"></script>