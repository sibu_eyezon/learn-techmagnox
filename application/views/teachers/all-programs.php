<style>
.list-group-item {
	border-bottom: 1px solid #ccabab !important;
}
</style>
<div class="content">
	<div class="container-fluid">
		<div class="card">
			<div class="card-header card-header-info card-header-text">
			  <div class="card-text">
				<h4 class="card-title">All Programs</h4>
			  </div>
			</div>
			<div class="card-body">
				<div class="row">
					
					<?php $i=1; foreach($programs as $prow){ ?>
					<div class="col-md-6">
					<div class="card bg-light">
						<div class="card-body">
							<div class="row">
								<div class="col-sm-12 text-dark">
									<h5><?php 
										echo $prow->title; 
										$yearnm = trim($prow->yearnm);
										if($yearnm!=''){
											echo ' (Academic year: '.$yearnm.')';
										}
										?></h5>
									<?php
										echo '<h6>';
										$type = trim($prow->ptype);
										$category = trim($prow->category);
										if($type!=""){
											echo $type.' Program, ';
										}
										if($category!=""){
											echo 'Category : '.$category.', ';
										}
										$dur = intval(trim($prow->duration));
										echo 'Duration: '.$dur.' '.trim($prow->dtype).'(s), ';
										if(trim($prow->feetype)=='paid'){
											echo 'Total Fees: '.trim($prow->total_fee).',   ';
										}
										if(trim($prow->total_credit)!=""){
											echo 'Total Credit: '.$prow->total_credit;
										}
										echo '</h6>';
										if(count(${'org'.$i})>0){
											$org='';
											echo 'organized by';
											foreach(${'org'.$i} as $orow){
												$org.=$orow->title.', ';
											}
											echo '<h6>'.rtrim($org,", ").'</h6>';
										}
											
										if($prow->program_brochure!=''){
											echo '<h6><a href="'.base_url().'uploads/programs/'.$prow->program_brochure.'" target="_blank">Brochure</a></h6>';
										}

										$src = 'assets/img/blank_certificate.png';
										$cert = trim($prow->certificate_sample);
										if($cert!=""){
											if(file_exists('./uploads/programs/'.$cert)){
												$src = 'uploads/program/'.$cert;
											}
										}
									?>
									<div class="card bg-warning my-0">
										<div class="card-body">
											<h6>Admission Deadline: <?php echo date('jS M Y',strtotime($prow->lastdate_apply)); ?></h6>
											<h6>Start Date: <?php echo date('jS M Y',strtotime($prow->start_date)); ?></h6>
											<?php
												if($prow->end_date!=null){
													echo '<h6>End Date: '.date('jS M Y',strtotime($prow->end_date)).'</h6>';
												}
											?>
										</div>
										<div class="card-footer">
											<?php
												echo '<a href="'.base_url().'Teacher/viewProgramDetails/?id='.base64_encode($prow->id).'" class="btn btn-success btn-sm">View Details</a>';
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					</div>
					<?php $i++; } ?>

				</div>
			</div>
		</div>
	
	</div>
</div>
<script src="<?php echo base_url().'assets/js/function.js'; ?>"></script>