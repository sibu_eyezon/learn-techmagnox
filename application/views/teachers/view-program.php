<link rel="stylesheet" href="https://cdn.plyr.io/3.6.8/plyr.css" />
<script src="https://cdn.plyr.io/3.6.8/plyr.polyfilled.js"></script>
<style>
.menu-list { list-style: none; margin: 0.5em auto; padding: 0; width: 10em; } @media (min-width: 20em) { .menu-list { width: 19em } } @media (min-width: 30em) { .menu-list { width: 29em } } @media (min-width: 40em) { .menu-list { width: 39em } } @media (min-width: 60em) { .menu-list { width: 59em } } .menu-list .menu-list-item { float: left; margin: 0rem 0.2em; padding: 0.5em; } .menu-list .menu-list-item>a { color: #000; font-weight: 500; } .menu-list li a { display: grid; place-items: center; } .menu-list .menu-num { width: 50px; height: 50px; display: grid; place-items: center; background-color: #007986; color: #fff; font-size: 1.5rem; border-radius: 50%; } .menu-list .menu-txt { font-size: 0.9rem; font-weight: 500; text-transform: uppercase; } .prog-wrapper { display: flex; text-decoration: none; transition: all 0.4s; } #prog-sidebar { min-width: 200px; max-width: 200px; background: #eee; color: #fff; transition: all 0.4s; max-height: 100vh; overflow-y:scroll; -ms-overflow-style: none; /* IE and Edge */ scrollbar-width: none; /* Firefox */ } #prog-sidebar::-webkit-scrollbar { display: none; } #prog-sidebar.active { min-width: 100px; max-width: 100px; } #prog-sidebar.active>.prog_components li a { display: grid !important; place-items: center !important; } #prog-sidebar.active>.prog_components li a span { display:none; } .prog_components li a span { font-size: 0.8rem; } #prog-sidebar.active>.prog_components .icon-logo { width: 55px !important; height: 55px !important; } #prog-sidebar .sidebar-header { padding: 20px; background: #1b1d24; } #prog-sidebar ul.components { padding: 20px 0; } #prog-sidebar ul p { color: #fff; padding: 10px; } #prog-sidebar ul li a { font-size: 1.1em; display: inline-block; align-items: center; background: transparent; color: #000; font-weight: 500; font-size: 1rem; text-transform: uppercase; } a[data-toggle="collapse"] { position: relative; } .prog_components li a { font-size: 0.9em !important; text-align: left; background: #1b1d24; } #prog-content { width: 100%; min-width: height 100vh; transition: all 0.4s; background-color: #fff; } #prog-content h2 { margin-bottom: 50px; } #prog-content p { text-align: justify; } @media (max-width: 768px) { #prog-sidebar { min-width: 0px !important; max-width: 0px !important; } #prog-sidebar.active { min-width: 100px !important; max-width: 100px !important; } #prog-sidebar .prog_components li a { display: grid !important; place-items: center !important; } #prog-sidebar .prog_components li a span { display:none; } .prog_components li a span { font-size: 0.8rem; } #prog-sidebar .prog_components .icon-logo { width: 55px !important; height: 55px !important; } } .icon-logo { font-size: 20px; color: #123456; padding: 12px 15px; font-family: FontAwesome; width: 55px; height: 55px; } @media (max-width: 768px) { .list-column { -webkit-columns: 1 !important; -moz-columns: 1 !important; columns: 1!important; padding-left: 0; list-style: square; } } .list-column { -webkit-columns: 2; -moz-columns: 2; columns: 2; padding-left: 0; list-style: square; } .list-column li { list-style-position: inside; -webkit-column-break-inside: avoid; page-break-inside: avoid; break-inside: avoid; font-size: 0.8rem; font-weight: 500; }
</style>
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" />
<script src="<?php echo base_url(); ?>assets/js/plugins/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>  
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script>function shownotify(msg) { $.notify({icon:"add_alert",message:msg},{type:'warning',timer:3e3,placement:{from:'top',align:'right'}}) }</script>
<?php
	if($this->session->flashdata('error')!=""){
		echo '<script>shownotify(`'.$this->session->flashdata('error').'`);</script>';
	}
	$ptype = intval($prog[0]->type);
	$progtype = ($ptype==4)? 'certificate' : (($ptype==3)? 'selfbased' : 'degree');
?>
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="prog-wrapper">
					<nav id="prog-sidebar">
						<ul class="list-unstyled prog_components">
							<li>
								<a href="javascript:" data-toggle="modal" data-target="#liveClass" class="prog-link" title="Live Class">
									<img src="<?php echo base_url(); ?>assets/img/icons/online-class.png" class="icon-logo"/> 
									<span>Live Class</span>
								</a>
							</li>
							<li>
								<a href="<?php echo base_url('Calendar/scheduleClass'); ?>" class="prog-link" title="Schedule Class">
									<img src="<?php echo base_url(); ?>assets/img/icons/schedule.png" class="icon-logo"/> 
									<span>Schedule Class</span>
								</a>
							</li>
							<!--<li>
								<a href="javascript:getProgMenuContents('cmodule');" class="prog-link" title="Modules">
									<img src="<?php //echo base_url(); ?>assets/img/icons/schedule.png" class="icon-logo"/> 
									<span>Modules</span>
								</a>
							</li>-->
							<li>
								<a href="javascript:getProgMenuContents('clectures');" class="prog-link">
									<img src="<?php echo base_url(); ?>assets/img/icons/lecture.png" class="icon-logo"/> 
									<span>Lectures</span>
								</a>
							</li>
							<li>
								<a href="javascript:getProgMenuContents('cresources');" class="prog-link">
									<img src="<?php echo base_url(); ?>assets/img/icons/resources.png" class="icon-logo"/> 
									<span>Resources</span>
								</a>
							</li>
							<li>
								<a href="javascript:getProgMenuContents('cassignments');" class="prog-link">
									<img src="<?php echo base_url(); ?>assets/img/icons/assignment.png" class="icon-logo"/> 
									<span>Assignments</span>
								</a>
							</li>
							<li>
								<a href="javascript:getProgMenuContents('cfaq');" class="prog-link" title="Projects">
									<img src="<?php echo base_url(); ?>assets/img/icons/ques.png" class="icon-logo"/>
									<span>FAQs</span>
								</a>
							</li>
							<li>
								<a href="javascript:getProgMenuContents('cprojects');" class="prog-link" title="Projects">
									<img src="<?php echo base_url(); ?>assets/img/icons/schedule.png" class="icon-logo"/>
									<span>Projects</span>
								</a>
							</li>
							<li>
								<a href="javascript:getProgMenuContents('cstudapplied');" class="prog-link" title="Applied Students">
									<img src="<?php echo base_url(); ?>assets/img/icons/schedule.png" class="icon-logo"/>
									<span>Applied</span>
								</a>
							</li>
							<li>
								<a href="javascript:getProgMenuContents('cstudents');" class="prog-link" title="Students">
									<img src="<?php echo base_url(); ?>assets/img/icons/students.png" class="icon-logo"/>
									<span>Students</span>
								</a>
							</li>
							<li>
								<a href="javascript:getProgMenuContents('cteachers');;" class="prog-link" title="Teachers">
									<img src="<?php echo base_url(); ?>assets/img/icons/teacher.png" class="icon-logo"/>
									<span>Teachers</span>
								</a>
							</li>
							<li>
								<a href="javascript:getProgMenuContents('corg');" class="prog-link" title="Organisation">
									<img src="<?php echo base_url(); ?>assets/img/icons/schedule.png" class="icon-logo"/>
									<span>Organisation</span>
								</a>
							</li>
						</ul>
					</nav>
					<div id="prog-content">
						<nav class="navbar navbar-expand-lg" style="background:transparent !important;">
							<div class="container-fluid">
								<h4 class="card-title w-100">
									<a href="javascript:;" id="progSidebarCollapse" class="btn btn-sm btn-primary"><i class="fa fa-bars"></i></a> 
									<?php echo trim($prog[0]->title); ?>
									<a href="<?php echo base_url('programDetails/?id='.base64_encode($prog_id)); ?>" target="_blank" class="pull-right" title="Web" rel="tooltip"><i class="material-icons">language</i></a>
									<a href="javascript:;" class="pull-right mr-3 text-warning" title="Review" rel="tooltip"><i class="material-icons">reviews</i></a>
									<a href="javascript:;" class="pull-right mr-3 text-primary" title="Quiz" rel="tooltip"><i class="material-icons">quiz</i></a>
									<a href="javascript:getProgMenuContents('home');" class="pull-right mr-3 text-info" title="Home" rel="tooltip"><i class="material-icons">home</i></a>
								</h4>
							</div>
						</nav>
						<div class="progress" id="progress" style="display:none;">
							<div class="progress-bar progress-bar-striped progress-bar-animated" id="progbra"></div>
						</div>
						<div class="row px-3" id="pcontent">

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--===================================================-->
<div class="modal fade" id="programModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            <i class="material-icons">clear</i>
          </button>
        </div>
        <div class="modal-body">
		  
		</div>
      </div>
    </div>
</div>
<div class="modal fade" id="programLgModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            <i class="material-icons">clear</i>
          </button>
        </div>
        <div class="modal-body">
		  
		</div>
      </div>
    </div>
</div>
<div class="modal fade" id="liveClass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			  <h4 class="modal-title">Start Live Class</h4>
			  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				<i class="material-icons">clear</i>
			  </button>
			</div>
			<form id="frmLive">
			<div class="modal-body">
				<div class="form-group">
					<?php echo $prog[0]->title; ?>
					<input type="hidden" name="pid" id="pid" value-"<?php echo $prog_id; ?>"/>
				  </div>
				  <div class="form-group">
					<select name="course_code" id="course_code" class="custom-select" data-style="select-with-transition" data-title="Select a course*" required="true" data-size="6">
						<?php
							$section = $this->AutoProgramModel->get_section($prog_id);
							foreach($section as $srow){
								echo '<option value="'.trim($srow->id).'">'.trim($srow->title).'</option>';
							}
						?>
					</select>
				  </div>
			</div>
			<div class="modal-footer">
				<input type="reset" style="visibility:hidden">
				<button type="submit" class="btn btn-success btn-sm mr-3" id="liveBt">Start Live Class</button>
				<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
			</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="operationModalCenter" tabindex="-1" role="dialog" aria-labelledby="operationModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
    	<div class="modal-content" id="modal_content">
    	</div>
  	</div>
</div>
<div class="modal fade" id="noticeM" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			  <h4 class="modal-title">Add Notice</h4>
			  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				<i class="material-icons">clear</i>
			  </button>
			</div>
			<form id="frmNotice" enctype="multipart/form-data">
				<div class="modal-body">
				  <div class="form-group mb-0">
					<label for="nottitle" class="text-dark">Title</label>
					<input type="text" class="form-control" name="nottitle" id="nottitle" required="true">
					<input type="hidden" name="pn_id" id="pn_id" value="0">
				  </div>	
				  <div class="form-group mb-0">
					<?= $prog[0]->title; ?>
					<input type="hidden" name="not_prog" id="not_prog" value="<?= $prog[0]->id; ?>"/>
				  </div>
				  <div class="form-group mb-0">
					<select name="not_course" id="not_course" class="selectpicker" data-style="select-with-transition" required="true">
						<option value="0" selected>All Courses</option>
					</select>
				  </div>
				  <div class="custom-file">
					<input type="file" class="custom-file-input" name="fl_notice" id="fl_notice" accept="application/pdf, .doc, .docx">
					<label class="custom-file-label text-dark" for="fl_notice">Upload your file</label>
				  </div>
				  <div class="form-group mt-5">
					<label for="notdetails" class="text-dark">Details</label><br>
					<textarea class="form-control w-100" name="notdetails" id="notdetails"></textarea>
				  </div>
				</div>
				<div class="modal-footer">
				  <input type="reset" style="visibility:hidden">
				  <button type="submit" class="btn btn-success mr-2 btn-sm">Save</button>
				  
				  <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script src="<?php echo base_url('assets/js/home.js'); ?>"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
	$("#progSidebarCollapse").on('click',function() {
		$("#prog-sidebar").toggleClass('active');
	  });
	getProgMenuContents('home');
});
function getProgMenuContents(menu)
{
	$('#pcontent').html("", 1000);
	$('#progbra').css('width', '0%');
	$('#progress').show();
	$.ajax({
		xhr: function () {
			var xhr = new window.XMLHttpRequest();
			xhr.upload.addEventListener("progress", function (evt) {
				if (evt.lengthComputable) {
					var percentComplete = evt.loaded / evt.total;
					console.log(percentComplete);
					$('#progbra').css({
						width: percentComplete * 100 + '%'
					});
					if (percentComplete === 1) {
						$('#progbra').addClass('hide');
					}
				}
			}, false);
			xhr.addEventListener("progress", function (evt) {
				if (evt.lengthComputable) {
					var percentComplete = evt.loaded / evt.total;
					console.log(percentComplete);
					$('#progbra').css({
						width: percentComplete * 100 + '%'
					});
				}
			}, false);
			return xhr;
		},
		type: 'GET',
		url: baseURL+"Teacher/getProgMenuContent",
		data: {page: menu, prog_id: <?php echo $prog_id; ?>},
		success: function(data){
			$('#progress').hide();
			$('#pcontent').html(data);
			$('#pcontent').fadeIn(2000);
		}
	});
}
function getAssigmentContents(lra_id)
{
	$('#pcontent').html("", 1000);
	$('#progbra').css('width', '0%');
	$('#progress').show();
	$.ajax({
		xhr: function () {
			var xhr = new window.XMLHttpRequest();
			xhr.upload.addEventListener("progress", function (evt) {
				if (evt.lengthComputable) {
					var percentComplete = evt.loaded / evt.total;
					console.log(percentComplete);
					$('#progbra').css({
						width: percentComplete * 100 + '%'
					});
					if (percentComplete === 1) {
						$('#progbra').addClass('hide');
					}
				}
			}, false);
			xhr.addEventListener("progress", function (evt) {
				if (evt.lengthComputable) {
					var percentComplete = evt.loaded / evt.total;
					console.log(percentComplete);
					$('#progbra').css({
						width: percentComplete * 100 + '%'
					});
				}
			}, false);
			return xhr;
		},
		type: 'GET',
		url: baseURL+"Teacher/getAssigmentContents",
		data: {lra_id: lra_id, prog_id: <?php echo $prog_id; ?>},
		success: function(data){
			$('#progress').hide();
			$('#pcontent').html(data);
			$('#pcontent').fadeIn(2000);
		}
	});
}
function showAjaxModal(url, header)
{
	jQuery('#programModal .modal-title').html(header);
	jQuery('#programModal .modal-body').html("");
	$.ajax({
		url: url,
		success: (response)=>{
			jQuery('#programModal .modal-body').html(response);
		}
	});
	$('#programModal').modal('show', {backdrop: 'true'});
}
function showLargeModal(url, header)
{
	jQuery('#programLgModal .modal-body').html("");
	jQuery('#programLgModal .modal-title').html(header);
	$.ajax({
		url: url,
		success: (response)=>{
			jQuery('#programLgModal .modal-body').html(response);
			jQuery('#programLgModal .modal-title').html(header);
			
		}
	});
	$('#programLgModal').modal('show', {backdrop: 'true'});
}
function deleteSubModule(id, title, ctype)
{
	swal({
		title: 'Are you sure?',
		text: "You want to remove "+ctype+": "+title,
		type: 'warning',
		showCancelButton: true,
		confirmButtonClass: 'btn btn-success',
		cancelButtonClass: 'btn btn-danger',
		confirmButtonText: 'Yes, delete it!',
		buttonsStyling: false
	}).then(function(opt) {
		if(opt.value){
			$.ajax({
				url: baseURL+'Certifyprogram/subModule_delete/'+id+'/'+ctype,
				type: 'GET',
				success: (res)=>{
					if(res)
					{
						$.notify({icon:"add_alert",message:'The '+ctype+' has been deleted.'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})	
						$('#'+ctype+'_'+id).remove();
					}else{
						$.notify({icon:"add_alert",message:'Something went wrong. Deletion error!!!.'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})
					}
				}
			})
		}
		/**/
	}).catch(swal.noop)
}
function notifyStudsSubModule(ctype, id, course_sl)
{
	swal({
		title: 'Are you sure?',
		text: "You want to notify all student about this "+ctype,
		type: 'warning',
		showCancelButton: true,
		confirmButtonClass: 'btn btn-success',
		cancelButtonClass: 'btn btn-danger',
		confirmButtonText: 'Yes, notify all!',
		buttonsStyling: false
	}).then(function(opt) {
		if(opt.value){
			$.ajax({
				url: baseURL+'Certifyprogram/subModule_notify',
				type: 'GET',
				data: {
					id: id,
					ctype: ctype,
					prog_id: <?php echo $prog_id; ?>,
					course_sl: course_sl
				},
				success: (res)=>{
					if(res)
					{
						$.notify({icon:"add_alert",message:'All students has been notified.'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})	
						getProgMenuContents('c'+ctype);
					}else{
						$.notify({icon:"add_alert",message:'Something went wrong. Notify error!!!.'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})
					}
				}
			})
		}
		/**/
	}).catch(swal.noop)
}
</script>