<style>
.dropdown bootstrap-select {
	width:100% !important;
}
.loader {
  background-color: #ffffff;
  opacity:0.5;
  position: fixed;
  z-index: 999999;
  height: 100%;
  width: 100%;
  top: 0;
  left: 0;
}
.loader img {
  position: absolute;
  top: 50%;
  left: 50%;
  text-align: center;
  -webkit-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
}
</style>
<div class="loader" id="loading" style="display:none;">
	<img src="<?php echo base_url().'assets/img/loading.gif'; ?>">
</div>
<div class="content">
	<div class="container-fluid">
		<div class="row">
		
			<div class="col-12 mx-auto">
				<div class="card">
					<div class="card-header card-header-info">
						<h3 class="card-title">All Jobs
						<a href="<?= base_url('Teacher/add_job'); ?>" class="btn btn-sm btn-primary pull-right"><i class="material-icons">add</i> Add Job</a>
						</h3>
						
					</div>
					
					<div class="card-body">
						<div class="material-datatables">
							<table class="table table-striped table-no-bordered table-hover" id="tblNotice" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th width="5%">Sl.</th>
										<th width="50%">Job Details</th>
										<th width="25%">Duration</th>
                                        <th width="10%">Applied</th>
										<th width="10%">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1; foreach($pjobs as $nrow){ ?>
										<tr>
											<td><?php echo $i; ?></td>
											<td>
                                                <?php 
													echo trim($nrow->title);
                                                    if($nrow->type){
                                                        echo '<br>'.implode(", ", json_decode($nrow->type));
                                                    }
                                                    if($nrow->designation_id){
                                                        echo '<br>'.trim($nrow->designation_name);
                                                    }
                                                    /*if(trim($nrow->min_qualification_id)){
                                                        echo '<br>'.trim($nrow->degree_name." (".$nrow->short.")");
                                                    }
                                                    if($nrow->org_id){
                                                        echo '<br>Organization: '.trim($nrow->org_title);
                                                    }*/
                                                    if(trim($nrow->salary)){
                                                        echo '<br>Salary: '.trim($nrow->salary);
                                                    }
                                                    if($nrow->experience){
                                                        echo '<br>Experience: '.trim($nrow->experience);
                                                    }
												?>
											</td>
											<td><?php echo date('jS M Y',strtotime($nrow->start_time)).' - '.date('jS M Y',strtotime($nrow->end_time)); ?></td>
											<td><a href="javascript:viewAppliedUsers(<?= $nrow->id; ?>);" id="view_<?= $nrow->id; ?>"><u>Applied: <?= ${'applied_'.$i}; ?></u></a></td>
											<td>
												<a href="<?= base_url('Teacher/add_job/?id='.base64_encode($nrow->id)); ?>" class="btn btn-sm btn-success"><i class="material-icons">edit</i> Edit/View</a><br>
												<button type="button" onClick="deleteJob(<?php echo '`'.$nrow->title.'`, '.$nrow->id; ?>);" class="btn btn-sm btn-danger"><i class="material-icons">delete</i> Delete</button>
											</td>
										</tr>
									<?php $i++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>

<div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			  <h4 class="modal-title" id="not_head">Applied User</h4>
			  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				<i class="material-icons">clear</i>
			  </button>
			</div>
			<div class="modal-body" id="user-list">
			
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url(); ?>assets/js/plugins/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {
		$('#tblNotice').DataTable();
	});

	function viewAppliedUsers(id)
	{
		$('#user-list').html("");
		$.ajax({
			beforeSend: () => {
				$('#view_'+id).addClass('btn-progress');
			},
			url: baseURL+'Teacher/viewAppliedUsers/?id='+id,
			type: 'GET',
			success: (res)=>{
				$('#view_'+id).removeClass('btn-progress');
				$('#user-list').html(res);
				$('#userModal').modal('show');
			},
			error: (err)=>{
				$('#view_'+id).removeClass('btn-progress');
			}
		});
	}
	
	function deleteJob(title, id)
	{
		swal({
			title: 'Are you sure?',
			text: "You want to delete this job: "+title,
			type: 'warning',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: 'Yes, delete it!',
			buttonsStyling: false
		}).then(function() {
			$('#loading').show();
			$.ajax({
				url: baseURL+'Teacher/deleteJob/?pnid='+id,
				type: 'GET',
				success: (res)=>{
					$('#loading').fadeOut(1000);
					if(res)
					{
						swal({
							title: 'Deleted!',
							text: 'The Job has been deleted.',
							type: 'success',
							confirmButtonClass: "btn btn-success",
							buttonsStyling: false
						}).then((result)=>{
							window.location.reload();
						})
					}else{
						swal({
							title: 'Failed!',
							text: 'Something went worng.',
							type: 'warning',
							confirmButtonClass: "btn btn-success",
							buttonsStyling: false
						});
					}
				}
			})
		}).catch(swal.noop)
	}
</script>