<?php
	$submit = ($org[0]->id=='0')? 'Add' : 'Update';
?>
<style>
.form-error{
	color:red;
}
#image_prev {
	cursor: pointer;
}
</style>
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-9 mx-auto">
				<div class="card">
					<div class="card-header card-header-primary card-header-icon">
					  <div class="card-icon">
						<i class="material-icons">create</i>
					  </div>
					  <h4 class="card-title"><?php echo $submit; ?> Organization <small class="text-danger">*'s are important</small></h4>
					</div>
					<div class="card-body">
						<form action="<?= base_url('Teacher/cuOrganization'); ?>" enctype="multipart/form-data" id="frmCommunity" method="POST" style="width: 100%;">
						<div class="row justify-content-center">
							<div class="col-sm-4">
								<div class="fileinput fileinput-new text-center">
									<div class="fileinput-new thumbnail">
										<img src="<?php echo base_url().'assets/img/institute/'.$org[0]->banner; ?>" onerror="this.src='<?php echo base_url(); ?>assets/img/image_placeholder.jpg'" class="picture-src" onClick="$('#avatar').click();" id="image_prev" name="image_prev" title="" />
									</div>
									<h6 class="description">Choose Banner</h6>
								</div>
								<input type="file" class="custom-file-input" name="avatar" id="avatar" onChange="preview();" accept="image/jpg, image/jpeg, image/png" style="display:none;">
							</div>
						</div>
						<div class="row justify-content-center">
							<div class="col-sm-12">
								<div class="form-group">
								  <label for="title" class="text-dark">Organization Title <span class="text-danger">*</span></label>
								  <input type="text" class="form-control" id="title" name="title" placeholder="Max length: 200 characters" value="<?= trim($org[0]->name); ?>">
								  <input type="hidden" name="cid" id="cid" value="<?= $org[0]->id; ?>"/>
								</div>
							</div>
						</div>
						<div class="row mb-5">
							<div class="col-md-6">
								<div class="form-group">
									<div class="custom-file">
										<input type="file" class="custom-file-input" name="logo" id="logo" accept="image/png">
										<label class="custom-file-label text-dark" for="logo"><?php echo (trim($org[0]->logo) == "")? 'Choose LOGO (.png only)' : trim($org[0]->logo); ?></label>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<div class="custom-file">
										<input type="file" class="custom-file-input" name="brochure" id="brochure" accept="application/pdf">
										<label class="custom-file-label text-dark" for="brochure"><?php echo (trim($org[0]->brochure) == "")? 'Choose brochure (.pdf only)' : trim($org[0]->brochure); ?></label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									 <label for="overview" class="text-dark">PAN number</label>
									<input type="text" name="pan_no" id="pan_no" class="form-control" value="<?= trim($org[0]->pan); ?>"/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									 <label for="overview" class="text-dark">GST number</label>
									<input type="text" name="gst_no" id="gst_no" class="form-control" value="<?= trim($org[0]->gst); ?>"/>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									 <label for="overview" class="text-dark">Website</label>
									<input type="url" name="website" id="website" class="form-control" value="<?= trim($org[0]->website); ?>"/>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									 <label for="overview" class="text-dark">Linkedin</label>
									<input type="url" name="linkedin" id="linkedin" class="form-control" value="<?= trim($org[0]->linkedin); ?>"/>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									 <label for="address" class="text-dark">Address</label>
									<textarea name="address" id="address" class="form-control" cols="80" rows="5"><?php echo $org[0]->address; ?></textarea>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									 <label for="contact" class="text-dark">Contact</label>
									<textarea name="contact" id="contact" class="form-control" cols="80" rows="5"><?php echo $org[0]->contact; ?></textarea>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="details" class="text-dark">Overview</label><br>
									<textarea name="details" id="details" class="form-control" cols="80" rows="5"><?php echo $org[0]->details; ?></textarea>
								</div>
							</div>
						</div>
						<div class="row mb-3">
							<div class="col-md-12">
								<div class="form-group text-center">
									<button type="submit" class="btn btn-primary btn-md"><?php echo $submit; ?></button>
								</div>
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url(); ?>assets/vendor/ckeditor/ckeditor.js"></script>
<script>
	function preview()
	{
		$("#image_prev").attr("src",URL.createObjectURL(event.target.files[0]));
	}
	CKEDITOR.replace('details', {
		extraPlugins: 'uploadimage,colorbutton,colordialog,autoembed,embedsemantic,mathjax,codesnippet,font,justify,table,specialchar, tabletools, uicolor',
		height: 200,
		mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML'
	});
	
	$(function(){
		$("#logo").on("change", function() {
		  var fileName = $(this).val().split("\\").pop();
		  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
		});
		$("#brochure").on("change", function() {
		  var fileName = $(this).val().split("\\").pop();
		  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
		});
		$('#frmCommunity').validate({
			errorPlacement: function(error, element) {
			  $(element).closest('.form-group').append(error);
			},
			rules: {
				title: {
					required: true,
					maxlength: 200
				}
			},
		});
	});

	
</script>