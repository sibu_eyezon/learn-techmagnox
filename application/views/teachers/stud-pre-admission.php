<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-info">
                        <h3 class="card-title">Pre Admission</h3>
                    </div>
                    <div class="card-body d-flex p-2 bd-highlight">
                        <div class="d-flex flex-row justify-content-start mb-3">
                            <div class="p-2">
                                <select class="selectpicker" data-style="select-with-transition" name="program" id="program" data-title="Set academic program <span class='text-danger'>*</span>">
                                <?php
                                    if(!empty($progs)):
                                        foreach($progs as $row):
                                ?>
                                        <option value="<?php echo $row->id;?>"><?php echo $row->title;?>(<?php echo $row->code;?>)</option>
                                <?php
                                        endforeach;
                                    endif;
                                ?>
                                </select>
                                <span id="err_prog" class="text-danger small"></span>
                            </div>
                            <div class="p-2">
                                    <button class="btn btn-primary btn-sm" onClick="searchStudentForPreAdmission()">view</button>
                            </div>
                        </div>
                        <div class="d-flex flex-row justify-content-end mb-3">
                            <div class="p-2">
								<select class="selectpicker" data-style="select-with-transition" name="acayear" id="acayear" data-title="Set academic year <span class='text-danger'>*</span>">
                                <?php
                                    if(!empty($academic_year)):
                                        foreach($academic_year as $row):
                                ?>
                                            <option value="<?php echo $row['sl'];?>"><?php echo $row['yearnm'];?></option>
                                <?php
                                        endforeach;
                                    endif;
                                ?>
                                </select>
                            </div>
                            <div class="p-2"><button class="btn btn-success btn-sm" onclick="allStudentPreAddmission()">Approvae</button></div>
                            <div class="p-2"><button class="btn btn-warning btn-sm" onclick="allStudentPreAddmissionReject()">Reject</button></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12">
                <div class="card">
					<div class="card-header card-header-info">
                        <h3 class="card-title">Student admission list under: <span id="prog_title"></span></h3>
                    </div>
					<div class="card-body">
						<div class="progress-bar progress-bar-striped progress-bar-animated" id="prg_progress" style="width: 100%; display:none">Loading the list. Please wait...</div>
						<table class="table table-striped table-no-bordered table-hover" id="tbl_adm" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th width="5%"><input type="checkbox" id="mainCheckBox"></th>
									<th width="15%">Name</th>
									<th width="20%">Contact</th>
									<th width="35%">Academic</th>
									<th width="15%">Status</th>
									<th width="25%">Academic Year</th>
								</tr>
							</thead>
							<tbody id="pre_admission_list">
							</tbody>
						</table>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url('assets/js/student-master.js'); ?>"></script>