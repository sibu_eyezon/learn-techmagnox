<div class="content">
	<div class="container-fluid">
		<div class="row">
		
			<div class="col-md-8 mx-auto">
				<div class="card"> 
					<div class="card-header card-header-info">
						<h3 class="card-title"> <?= ($job[0]->id==0)? 'Add Job':'Update Job'; ?>
						<a href="<?= base_url('Teacher/jobs'); ?>" class="btn btn-sm btn-primary pull-right"><i class="material-icons">list</i> Job List</a>
						</h3>
						
					</div>
                    <div class="card-body">
                        
                        <form action="<?= base_url('Teacher/cuJobs'); ?>" method="POST" id="frmJob">
                            <div class="row mb-3">
								<div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Job Title *</label>
                                        <input type="text" name="title" id="title" class="form-control" value="<?= trim($job[0]->title); ?>" required="true"/>
                                    </div>
                                    <input type="hidden" name="job_id" id="job_id" value="<?= $job[0]->id; ?>"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Salary *</label>
                                        <input type="text" name="salary" id="salary" class="form-control" value="<?= $job[0]->salary; ?>" required="true"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Min. Experience Required *</label>
                                        <input type="text" name="experience" id="experience" class="form-control" value="<?= $job[0]->experience; ?>" required="true"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Start datetime *</label>
                                        <input type="datetime-local" name="start_time" id="start_time" class="form-control" value="<?= date('Y-m-d\TH:i:s',strtotime($job[0]->start_time)); ?>" required="true"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>End datetime *</label>
                                        <input type="datetime-local" name="end_time" id="end_time" class="form-control" value="<?= date('Y-m-d\TH:i:s',strtotime($job[0]->end_time)); ?>" required="true"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Qualification *</label><br>
                                        <select class="selectpicker" data-style="select-with-transition" data-title="Select one minimum qualification" data-size="5" name="degree_id" id="degree_id" required="true">
                                            <option value="">Select one minimum qualification</option>
                                            <?php 
                                            if(!empty($degrees)){
                                                foreach($degrees as $degr){
                                                    echo '<option value="'.$degr->id.'" '.(($degr->id == $job[0]->min_qualification_id)? 'selected':'').'>'.trim($degr->degree_name).'</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Designation *</label><br>
                                        <select class="selectpicker" data-style="select-with-transition" data-title="Select one designation" data-size="5" name="designation" id="designation" required="true">
                                            <option value="">Select one designation</option>
                                            <?php 
                                            if(!empty($designation)){
                                                foreach($designation as $desg){
                                                    echo '<option value="'.$desg->id.'" '.(($desg->id == $job[0]->designation_id)? 'selected':'').'>'.trim($desg->name).'</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?php
                                        $types = (!empty($job[0]->type))? json_decode($job[0]->type) : $job[0]->type;
                                    ?>
                                    <div class="form-group">
                                        <label>Type *</label><br>
                                        <select class="selectpicker" data-style="select-with-transition" data-title="Select one type" data-size="5" name="type[]" id="type" multiple="true" required="true">
                                            <option value="Internship" <?php if(in_array("Internship", $types)){ echo 'selected'; } ?>>Internship</option>
                                            <option value="Full-Time" <?php if(in_array("Full-Time", $types)){ echo 'selected'; } ?>>Full-Time</option>
                                            <option value="Part-Time" <?php if(in_array("Part-Time", $types)){ echo 'selected'; } ?>>Part-Time</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Location *</label><br>
                                        <select class="selectpicker" data-style="select-with-transition" data-title="Select one location" data-size="5" name="location" id="location" required="true">
                                            <option value="">Select one location</option>
                                            <?php 
                                            if(!empty($city)){
                                                foreach($city as $crow){
                                                    echo '<option value="'.$crow->id.'" '.(($crow->id == $job[0]->location_id)? 'selected':'').'>'.trim($crow->name).'</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Organization</label><br>
                                        <select class="selectpicker" data-style="select-with-transition" data-title="Select one organization" data-size="5" name="organization_id" id="organization_id" required="true">
                                            <?php 
                                            if(!empty($orgs)){
                                                foreach($orgs as $orow){
                                                    echo '<option value="'.$orow->id.'" '.(($orow->id == $job[0]->org_id)? 'selected':'').'>'.trim($orow->name).'</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Industry</label><br>
                                        <select class="selectpicker" data-style="select-with-transition" data-title="Select one industry" data-size="5" name="industry" id="industry" required="true">
                                            <?php 
                                            if(!empty($industry)){
                                                foreach($industry as $inds){
                                                    echo '<option value="'.$inds->id.'" '.(($inds->id == $job[0]->indus_id)? 'selected':'').'>'.trim($inds->name).'</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
								<div class="col-sm-12">
									<div class="form-group">
										 <label for="desc" class="text-dark">Description</label><br>
										<textarea name="desc" id="desc" class="form-control" cols="80" rows="5"><?php echo trim($job[0]->desc); ?></textarea>
									</div>
								</div>
							</div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success btn-sm"><?= ($job[0]->id==0)? 'Save':'Update'; ?></button>
                                        <input type="reset" style="visibility:hidden;"/>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/vendor/ckeditor/ckeditor.js"></script>
<script>
	$(document).ready(function(){
		CKEDITOR.replace('desc', {
			extraPlugins: 'easyimage,uploadimage,colorbutton,colordialog,autoembed,embedsemantic,mathjax,codesnippet,font,justify,table,specialchar, tabletools, uicolor',
			height: 200,
			mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML'
		});
	});
    $("form#frmJob").validate({
			errorPlacement: function(error, element) {
			  $(element).closest('.form-group').append(error);
			}
		});
</script>