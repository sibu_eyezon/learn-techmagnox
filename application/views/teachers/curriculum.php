<?php
$section = $this->AutoProgramModel->get_section($prog_id);
//$csec = count($section);
?>
<h4 class="mx-3">Curriculum</h4>
<hr style="border:2px solid #eee;">
<div class="row justify-content-center">
    <div class="col-xl-12 mb-4 text-center mt-3">
<a href="javascript:;" class="btn btn-outline-primary btn-rounded btn-sm ml-1" onclick="showLargeModal('<?php echo site_url('Certifyprogram/module_cu/'.$prog_id.'/'.$progtype); ?>', 'Add New Module')"><i class="material-icons">add</i> Add Module</a>
<a href="javascript:;" class="btn btn-outline-primary btn-rounded btn-sm ml-1" onclick="showAjaxModal('<?php echo site_url('Certifyprogram/lecture_cu/'.$prog_id.'/'.$progtype); ?>', 'Add New Lecture')"><i class="material-icons">add</i> Add Lecture</a>
<a href="javascript:;" class="btn btn-outline-primary btn-rounded btn-sm ml-1" onclick="showAjaxModal('<?php echo site_url('Certifyprogram/resource_cu/'.$prog_id.'/'.$progtype); ?>', 'Add New Resource')"><i class="material-icons">add</i> Add Resource</a>
<a href="javascript:;" class="btn btn-outline-primary btn-rounded btn-sm ml-1" onclick="showAjaxModal('<?php echo site_url('Certifyprogram/assignment_cu/'.$prog_id.'/'.$progtype); ?>', 'Add New Assignment')"><i class="material-icons">add</i> Add Assignment</a>
<!--<a href="javascript:;" class="btn btn-outline-primary btn-rounded btn-sm ml-1" onclick="showLargeModal('<?php //echo site_url('Certifyprogram/sort_section/'.$prog_id.'/'.$progtype); ?>', 'Sort Sections')"><i class="material-icons">sort</i> Sort Sections</a>-->
    </div>
	<div class="col-xl-12">
		<?php
			$i=1;
			foreach($section as $secrow){
				$sec_id = $secrow->id;
		?>
		<div class="card bg-dark text-white mt-0" id="sec_<?php echo $sec_id; ?>">
			<div class="card-header">
				<h4 class="card-title">Module <?php echo $i; ?>: <?php echo $secrow->title; ?>
					<a href="javascript:;" onClick="deleteSection(<?php echo $sec_id; ?>, '<?php echo $secrow->title; ?>')" class="btn btn-outline-primary btn-rounded btn-sm ml-1 pull-right"><i class="fa fa-trash"></i></a>
					<a href="javascript:;" onClick="showLargeModal('<?php echo site_url('Certifyprogram/module_cu/'.$prog_id.'/'.$progtype.'/'.$sec_id); ?>', 'Edit Module')" class="btn btn-outline-primary btn-rounded btn-sm ml-1 pull-right"><i class="fa fa-edit"></i></a> 
				</h4>
			</div>
			<div class="card-body">
				<?php
					$clra = $this->Course_model->getAllCourseSubDetails($sec_id);
					if(!empty($clra)){
					foreach($clra as $lra){
						$lra_type = trim($lra->type);
						$lra_id = $lra->sl;
						$lrtitle =trim($lra->title);
				?>
				<div class="card bg-info mt-0" id="<?php echo $lra_type.'_'.$lra_id; ?>">
					<div class="card-body">
						<h4 class="card-title"><?php echo ucfirst($lra_type).': '.$lrtitle; ?>
							<a href="javascript:;" onClick="deleteSubModule(<?php echo $lra_id; ?>, '<?php echo $lrtitle; ?>', '<?php echo $lra_type; ?>')" class="btn btn-outline-primary btn-rounded btn-sm ml-1 pull-right"><i class="fa fa-trash"></i></a>
							<a href="javascript:;" onClick="showAjaxModal('<?php echo site_url('Certifyprogram/'.$lra_type.'_cu/'.$prog_id.'/'.$progtype.'/'.$lra_id); ?>', 'Edit <?php echo ucfirst($lra_type); ?>');" class="btn btn-outline-primary btn-rounded btn-sm ml-1 pull-right"><i class="fa fa-edit"></i></a> 
						</h4>
						<?php
							if($lra_type=='resource'){
								$crfiles = $this->Course_model->getCResourceFileByRID($lra_id);
								foreach($crfiles as $frow){
									$isrc = '';
									$cfid = $frow->sl;
									$rtype = trim($frow->type);
									$ftype = trim($frow->file_type);
									if($ftype=='pdf'){
										$isrc = 'fa-file-pdf-o';
									}else if($ftype=='doc' || $ftype=='docx'){
										$isrc = 'fa-file-word-o';
									}else if($ftype=='xls' || $ftype=='xlsx'){
										$isrc = 'fa-file-excel-o';
									}else if($ftype=='jpg'){
										$isrc = 'fa-file-image-o';
									}else if($ftype=='mp4'){
										$isrc = 'fa-file-video-o';
									}else{
										$isrc = 'fa-file';
									}
									if($rtype=='yt'){
										echo '<div class="alert alert-primary alert-with-icon mt-3" id="alert_'.$cfid.'" data-notify="container">
												<i class="fa fa-youtube-play" data-notify="icon"></i>
												<span data-notify="icon" class="now-ui-icons ui-1_bell-53"></span>
												<span data-notify="message"><a href="'.trim($frow->linkfile).'" target="_blank"> View Video</a></span>
											</div>';
									}else if($rtype=='lk'){
										echo '<div class="alert alert-primary alert-with-icon mt-3" id="alert_'.$cfid.'" data-notify="container">
												<i class="material-icons" data-notify="icon">link</i>
												<span data-notify="icon" class="now-ui-icons ui-1_bell-53"></span>
												<span data-notify="message"><a href="'.$frow->linkfile.'" target="_blank">View Link</a></span>
											</div>';
									}else if($rtype=='fl'){
										echo '<div class="alert alert-primary alert-with-icon mt-3" id="alert_'.$cfid.'" data-notify="container">
												<i class="fa '.$isrc.'" data-notify="icon"></i>
												<span data-notify="icon" class="now-ui-icons ui-1_bell-53"></span>
												<span data-notify="message"><a href="'.base_url('uploads/cresources/'.trim($frow->linkfile)).'" target="_blank"> Download</a></span>
											</div>';
									}
								}
							}
						?>
					</div>
				</div>
				<?php
						}
					}
				?>
			</div>
		</div>
		<?php $i++; } ?>
	</div>	
    
</div>
<script>
function deleteSection(id, title)
{
	swal({
		title: 'Are you sure?',
		text: "You want to remove section: "+title,
		type: 'warning',
		showCancelButton: true,
		confirmButtonClass: 'btn btn-success',
		cancelButtonClass: 'btn btn-danger',
		confirmButtonText: 'Yes, delete it!',
		buttonsStyling: false
	}).then(function() {
		$.ajax({
			url: baseURL+'Certifyprogram/module_delete/'+id,
			type: 'GET',
			success: (res)=>{
				if(res)
				{
					$.notify({icon:"add_alert",message:'The section has been deleted.'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})	
					$('#sec_'+id).remove();
				}else{
					$.notify({icon:"add_alert",message:'Something went wrong. Deletion error!!!.'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})
				}
			}
		})
	}).catch(swal.noop)
}
function deleteSubModule(id, title, ctype)
{
	swal({
		title: 'Are you sure?',
		text: "You want to remove "+ctype+": "+title,
		type: 'warning',
		showCancelButton: true,
		confirmButtonClass: 'btn btn-success',
		cancelButtonClass: 'btn btn-danger',
		confirmButtonText: 'Yes, delete it!',
		buttonsStyling: false
	}).then(function() {
		$.ajax({
			url: baseURL+'Certifyprogram/subModule_delete/'+id+'/'+ctype,
			type: 'GET',
			success: (res)=>{
				if(res)
				{
					$.notify({icon:"add_alert",message:'The '+ctype+' has been deleted.'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})	
					$('#'+ctype+'_'+id).remove();
				}else{
					$.notify({icon:"add_alert",message:'Something went wrong. Deletion error!!!.'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})
				}
			}
		})
	}).catch(swal.noop)
}
</script>