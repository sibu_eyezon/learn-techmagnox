<style>
#course_code::placeholder {
	color: white;
}
</style>
<div class="content">
	<div class="container-fluid">
		<div class="row">
			
			<div class="col-md-8">
				<div id="jitsi-container" style="min-height:400px"></div>
			</div>
			
			<aside class="col-md-4">
				<div class="card bg-info text-white">
					<div class="card-body">
						<p>
							<?php
								$count_sh = count($sch_class);
								echo '<strong>Program: </strong>'.$progcourse[0]->program_title.'<br>';
								echo '<strong>Course: </strong>'.$progcourse[0]->course_title.'<br>';
								/*echo '<div class="togglebutton">
									<label class="text-dark">
										Schecduled Class?
										<input type="checkbox" name="schNotify" id="schNotify" checked="true" value="1" '.(($count_sh<=0)? 'checked': '').'>
										<span class="toggle"></span>
									</label>
								</div>';
								if($count_sh>0){
									echo '<select class="selectpicker" data-style="select-with-transition" title="Select a schedule class*">';
									foreach($sch_class as $shrow){ 
										echo '<option value="'.$shrow->id.'">'.$shrow->class_title.' ('.$shrow->class_type.')</option>';
									}
									echo '</select>';
								}*/
								
							?>
						</p>
						
						<button class="btn bg-success btn-block" id="start">Start Class Now</button>
						<button class="btn bg-danger btn-block" id="stop" style="display:none">Take Attendance and Stop the Class</button>
						<button class="btn bg-primary btn-block" id="invite" style="display:none" onClick="inviteStudents()">Invite Students</button>
						<div class="card">
							<div class="card-header card-header-primary">
								<div class="card-title">
									<div class="form-check">
										<label class="form-check-label">
											<input class="form-check-input" type="checkbox" value=""
												onChange="toggleSelection()">
											STUDENTS
											<span class="form-check-sign">
												<span class="check"></span>
											</span>
										</label>
									</div>
								</div>
							</div>
							<div class="card-body">
								<div id="student-list" style="max-height:40vh; overflow-y:scroll;">
									<ul class="list-group" id='student-list-ul'>
										<?php for($i=0; $i<1; $i++){ ?>
										<?php foreach($pstud as $srow){ 
										echo '<li class="list-group-item">';
										echo '<input type="checkbox" class="form-check-input" name="students" value="'.$srow->id.'"><span id="span-'.$srow->id.'">'.$srow->name.'</span> <i id="tick-'.$srow->id.'" class="material-icons tick d-none">done</i>';
										echo '</li>';
									} }?>
									</ul>
								</div>
							</div>
						</div>
						<!--<button class="btn bg-primary btn-block" id="invite" style="display:none"  onClick="inviteStudents()">Invite Students</button>
						<div class="mt-3" style="max-height:40vh; overflow-y:scroll;">
							<ul class="list-group">
								<?php /*foreach($pstud as $srow){ 
									echo '<li class="list-group-item">';
									echo '<input type="checkbox" class="form-check-input" name="students" value="'.$srow->id.'" checked>'.$srow->name.' <i id="tick-'.$srow->id.'" class="material-icons tick d-none">done</i>';
									echo '</li>';
								}*/ ?>
							</ul>
						</div>-->
						
						<!-- <button class="btn bg-primary btn-block" id="attendance" style="display:none"  onClick="takeAttendance()">Take Attendance</button> -->
					</div>
				</div>
			</aside>

		</div>
	</div>
</div>
<script>
function toggleSelection() {
    $.each($('input[name="students"]'), function() {
        this.checked = !this.checked;
    })
}

$(document).ready(function() {

    //const ps = new PerfectScrollbar("#student-list");




    //$('.student_ticker').easyTicker();
    $('#frmLive').validate({
        errorPlacement: function(error, element) {
            $(element).closest('.form-group').append(error);
        },
        submitHandler: function(form, e) {
            $('#loading').css('display', 'block');
            $('#liveBt').attr('disabled', true);
            e.preventDefault();
            var frmLiveData = new FormData($('#frmLive')[0]);
            $.ajax({
                url: baseURL + 'Teacher/checkValidCourse',
                type: 'POST',
                data: frmLiveData,
                cache: false,
                processData: false,
                contentType: false,
                async: false,
                success: (res) => {
                    $('#frmLive')[0].reset();
                    $('#liveBt').removeAttr('disabled');
                    $('#loading').css('display', 'none');
                    var obj = JSON.parse(res);
                    if (obj['status'] == 'success') {
                        window.open(baseURL + 'Teacher/liveClass/?id=' + btoa(obj[
                            'cid']), '_self');
                    } else {
                        //alert(obj['msg']);
						$.notify({icon:"add_alert",message:obj['msg']},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})
                        swal(
                            'Course Code',
                            obj['msg'],
                            obj['status']
                        );
                    }
                },
                error: (errors) => {
                    console.log(errors);
                }
            });
        }
    });
});

var startBtn = document.querySelector('#start');
var stopBtn = document.querySelector('#stop');
var container = document.querySelector('#jitsi-container');
var api = null;
var live_id = null;
var invited = false;

startBtn.addEventListener('click', () => {

    var domain = "meet.jit.si";
    var time = new Date().getTime();
    var room = 'magnox' + time;
    var options = {
        "roomName": room,
        "parentNode": container,
        "width": 100 + '%',
        "height": 600,
    };
    configOverwrite = {
        prejoinPageEnabled: false
    };

    options['configOverwrite'] = configOverwrite;

    $.ajax({
        url: baseURL + 'Liveclass/startLiveClass',
        type: 'POST',
        data: {
            course_id: <?=$cid;?>,
            room_name: room,
            teacher_id: <?=$userid?>
        },
        success: (res) => {
            var obj = JSON.parse(res);
            if (obj['status'] == 'success') {
                //console.log(obj['room']);
                live_id = obj['live_id'];
                options['roomName'] = obj['room'];
                api = new JitsiMeetExternalAPI(domain, options);
                $('#jitsi-container').fadeIn(2000);
                $('#start').fadeOut(2000);
                $('#stop').fadeIn(4000);
                $('#invite').fadeIn(500);
                //$('#attendance').fadeIn(500);
            }
            console.log(obj['msg']);
        },
        error: (errors) => {
            console.log(errors);
        }
    });
});
stopBtn.addEventListener('click', () => {
    $.ajax({
        url: baseURL + 'Liveclass/takeAttendance',
        type: 'POST',
        data: {
            live_id: live_id,
            course_id: <?=$cid;?>
        },
        success: (res) => {
            var obj = JSON.parse(res);
            console.log(res);
            //alert(obj['msg']);
			$.notify({icon:"add_alert",message:obj['msg']},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})
            // stop the class
            api.dispose();
            $.ajax({
                url: baseURL + 'Liveclass/stopLiveClass',
                type: 'POST',
                data: {
                    course_id: <?=$cid;?>
                },
                success: (res) => {
                    var obj = JSON.parse(res);
                    live_id = null;
                    console.log(obj['msg']);
					$('input[name="students"]').each(function(){
						$(this).prop('checked',0);
						//$(this).removeClass('invited');
					});	
					$('i.tick').each(function(){
						$(this).addClass('d-none');
					});
					$('#student-list-ul span').each(function(){
						$(this).removeClass('invited');
					});
                },
                error: (errors) => {
                    console.log(errors);
                }
            });
            $('#jitsi-container').fadeOut(2000);
            $('#stop').fadeOut(2000);
            $('#start').fadeIn(4000);
            $('#invite').fadeOut(500);
            //$('#attendance').fadeOut(500);
            $('i.tick').addClass('d-none');
            invited = false;
        },
        error: (errors) => {
            console.log(errors);
        }
    });


});



setInterval(() => {
    if (live_id != null) {
        $.ajax({
            url: baseURL + 'Liveclass/getInvitationStatus',
            type: 'POST',
            data: {
                live_id: live_id,
				course_id: <?=$cid;?>
            },
            success: (res) => {
                var obj = JSON.parse(res);
                obj.data.forEach(element => {
					if(!$("input[name='students'][value='"+element.student_id+"']").length){
						console.log("Not Found: "+element.student_id);
						var newStudent =  '<li class="list-group-item">';
						newStudent += '<input type="checkbox" class="form-check-input" name="students" value="'+element.student_id+'"><span id="span-'+element.student_id+'">'+element.name+'</span> <i id="tick-'+element.student_id+'" class="material-icons tick d-none">done</i>';
						newStudent += '</li>';
						$('#student-list-ul').append(newStudent);
					}
					if (element.joined == 't') {
						$('i#tick-' + element.student_id).removeClass('d-none');
					} else {
						$('i#tick-' + element.student_id).addClass('d-none');
					}
					if (element.invited == 'yes'){
						$("span#span-"+element.student_id).addClass('invited');
					}else{
						$("span#span-"+element.student_id).removeClass('invited');
					}
					                    
                });
                //console.log(obj);		
            },
            error: (errors) => {
                console.log(errors);
            }
        });
    }else{
        $('i.tick').each(function(){
            $(this).addClass('d-none');
        });
        $('#student-list-ul span').each(function(){
            $(this).removeClass('invited');
        });
    }
}, 5000);

function inviteStudents() {
    var students = [];
    $.each($('input[name="students"]:checked'), function() {
        students.push($(this).val());
    })
    if (students.length > 0) {
        $.ajax({
            url: baseURL + 'Liveclass/inviteStudents',
            type: 'POST',
            data: {
                live_id: live_id,
                students: students
            },
            success: (res) => {
                var obj = JSON.parse(res);
                if (obj['status'] == 'success') {
					$.notify({icon:"add_alert",message:obj['msg']},{type:'success',timer:3e3,placement:{from:'top',align:'right'}});
                    //alert(obj['msg']);
                    // clear checkbox
					$('input[name="students"]').each(function(){
						$(this).prop('checked',0);
					});					
					invited = true;
					
                }
            },
            error: (errors) => {
                console.log(errors);
            }
        });
    } else {
		$.notify({icon:"add_alert",message:"Please select student(s) to invite"},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}});
    }

}
</script>
<script src='https://meet.jit.si/external_api.js'></script>
<script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
<!-- <script src="<?php //echo base_url();?>assets/vendor/video/test.js"></script> <script src="<?php //echo base_url();?>assets/js/perfect-scrollbar.js"></script>-->
<script src="<?php echo base_url();?>assets/vendor/video/main.js"></script>

<!--
<script>

var startBtn = document.querySelector('#start');
var stopBtn = document.querySelector('#stop');
var container = document.querySelector('#jitsi-container');
var api = null;
var live_id = null;
var invited = false;

startBtn.addEventListener('click', () => {
	
	var domain = "meet.jit.si";
	var time = new Date().getTime();
    var room = 'magnox' + time;
    var options = {
        "roomName": room,
        "parentNode": container,
        "width": 100+'%',
        "height": 600,
	};
	configOverwrite = {
        prejoinPageEnabled: false
    };

    options['configOverwrite'] = configOverwrite;

	$.ajax({
		url: baseURL+'Liveclass/startLiveClass',
		type: 'POST',
		data: {course_id: <?=$cid;?>, room_name: room, teacher_id: <?=$userid?>},
		success: (res)=>{ 			
			var obj = JSON.parse(res);
			if(obj['status'] ==  'success'){
				//console.log(obj['room']);
				live_id = obj['live_id'];
				options['roomName'] = obj['room'];
				api = new JitsiMeetExternalAPI(domain, options);
				$('#jitsi-container').fadeIn(2000);
				$('#start').fadeOut(2000);
				$('#stop').fadeIn(4000);
				$('#invite').fadeIn(500);
				//$('#attendance').fadeIn(500);
			}
			$.notify({icon:"add_alert",message:obj['msg']},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})		
		},
		error: (errors)=>{
			console.log(errors);
		}
	});	
});
stopBtn.addEventListener('click', () => {
	$.ajax({
		url: baseURL+'Liveclass/takeAttendance',
		type: 'POST',
		data: {live_id: live_id, course_id: <?=$cid;?>},
		success: (res)=>{ 			
			var obj = JSON.parse(res);	
			console.log(res);		
			//alert(obj['msg']);
			$.notify({icon:"add_alert",message:obj['msg']},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})
			// stop the class
			api.dispose();
			$.ajax({
				url: baseURL+'Liveclass/stopLiveClass',
				type: 'POST',
				data: {course_id: <?=$cid;?>},
				success: (res)=>{ 			
					var obj = JSON.parse(res);
					live_id = null;
					console.log(obj['msg']);			
				},
				error: (errors)=>{
					console.log(errors);
				}
			});
			$('#jitsi-container').fadeOut(2000);
			$('#stop').fadeOut(2000);
			$('#start').fadeIn(4000);
			$('#invite').fadeOut(500);
			//$('#attendance').fadeOut(500);
			$('i.tick').addClass('d-none');
			invited = false;
		},
		error: (errors)=>{
			console.log(errors);
		}
	});
	
	
});


/*
setInterval(() => {
	if(live_id != null){
		$.ajax({
			url: baseURL+'Liveclass/getInvitationStatus',
			type: 'POST',
			data: {live_id: live_id},
			success: (res)=>{ 			
				var obj = JSON.parse(res);
				obj.data.forEach(element => {
					if(element.joined == 't'){
						$('i#tick-'+element.student_id).removeClass('d-none');
					}else{
						$('i#tick-'+element.student_id).addClass('d-none');
					}
				});
				//console.log(obj);		
			},
			error: (errors)=>{
				console.log(errors);
			}
		});
	}
}, 5000);*/

function inviteStudents(){
	var prog_title = '<?php //echo $progcourse[0]->program_title; ?>';
	var students = [];
	$.each($('input[name="students"]:checked'), function(){
		students.push($(this).val());
	})
	if(students.length > 0){		
		$.ajax({
			url: baseURL+'Liveclass/inviteStudents',
			type: 'POST',
			data: {live_id: live_id, students: students, prog_title: prog_title},
			success: (res)=>{ 			
				console.log(res)
				var obj = JSON.parse(res);
				if(obj['status'] == 'success'){
					//alert(obj['msg']);
					$.notify({icon:"add_alert",message:obj['msg']},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})
					invited = true;
				}		
			},
			error: (errors)=>{
				console.log(errors);
			}
		});
	}else{
		//alert("Please select student(s) to invite");
		$.notify({icon:"add_alert",message:"Please select student(s) to invite"},{type:'warning',timer:3e3,placement:{from:'top',align:'right'}})
	}
	
}


</script>
<script src='https://meet.jit.si/external_api.js'></script>
<script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
<script src="<?php //echo base_url();?>assets/vendor/video/test.js"></script>
<script src="<?php //echo base_url();?>assets/vendor/video/main.js"></script>-->