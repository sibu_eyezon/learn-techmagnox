<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" />

<script src="<?php echo base_url(); ?>assets/js/plugins/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>  
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<style>
.loader {
  background-color: #ffffff;
  opacity:0.5;
  position: fixed;
  z-index: 999999;
  height: 100%;
  width: 100%;
  top: 0;
  left: 0;
}

.loader img {
  position: absolute;
  top: 50%;
  left: 50%;
  text-align: center;
  -webkit-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
}
</style>
<script>
	function getAdmissionList(pid, atype)
	{
		$('#adm_list').html("");
		if(pid!=""){
			$('#prg_progress').show();
			$.ajax({
				url: baseURL+'Teacher/getProgAdmissionList',
				type: 'GET',
				data: { pid: pid, atype: atype },
				success: (resp)=>{
					$('#prg_progress').hide();
					$('#adm_list').html(resp);
				}
			});
		}

	}
</script>
<div class="loader" id="loading" style="display:none;">
	<img src="<?php echo base_url().'assets/img/loading.gif'; ?>">
</div>
<div class="content">
    <div class="container-fluid">
        <div class="row">
			
			<div class="col-md-12">
				<div class="card">
					<div class="card-header card-header-info card-header-text">
						<div class="card-text">
							<h4 class="card-title">Program List</h4>
						</div>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-sm-6">
								<select class="custom-select" onChange="getAdmissionList(this.value, '<?php echo $atype; ?>');" name="program_list" id="program_list">
									<option value="">Program List</option>
									<?php
										if(!empty($progs)){
											foreach($progs as $prow){
												echo '<option value="'.$prow->id.'">'.trim($prow->title).' - '.$prow->yearnm.'</option>';
											}
										}
									?>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="progress-bar progress-bar-striped progress-bar-animated" id="prg_progress" style="width:100%; display:none;">Loading the list. Please wait...</div>
				<div id="adm_list"></div>
			</div>
			
		</div>
	</div>
</div>