<?php
	$i=1;
	foreach($student_list as $data){
		if(!empty(${'stud_adm_'.$i})){
			$stud_id = $data->stud_id;
			$spc_id = ${'stud_adm_'.$i}[0]->spc_id;
			$enroll = ${'stud_adm_'.$i}[0]->enrollment_no;
			$status = ${'stud_adm_'.$i}[0]->status;
			$sps_cgpa = ${'stud_adm_'.$i}[0]->sps_cgpa;
			$sps_percent = ${'stud_adm_'.$i}[0]->sps_percent;
			$totalfees = ${'stud_adm_'.$i}[0]->totalfees;
			$certificate = trim(${'stud_adm_'.$i}[0]->certificate);
			echo '<tr id="row'.$stud_id.'">
				<td><input type="checkbox" class="checkbox" onClick="clickChangeEvent()" value="'.$spc_id.'"></td>
				<td>'.$data->name.'</td>
				<td>'.$enrollment_no.'</td>
				<td>'.$data->phone.'</td>
				<td>
					<div class="form-group">
						<input type="text" class="form-control" id="parce'.$stud_id.'" name="parce" data-id="'.$sps_id.'" value="'.((!empty($sps_percent))? $sps_percent:"").'"> 
					</div>
				</td>
				<td>
					<div class="form-group">
						<input type="text" class="form-control" id="cgpa'.$stud_id.'" name="cgpa"  value="'.((!empty($sps_cgpa))? $sps_cgpa:"").'">
					</div>
				</td>
				<td>'.((intval($totalfees)==0)?'Free':$totalfees).'</td>
				<td>
					<div class="form-group">
						<select class="form-control" id="status'.$stud_id.'" name="status" data-id="'.$spc_id.'">
							<option value="0" '.(($status == 0)?'selected="selected"':"").'>Started</option>
							<option value="1" '.(($status == 1)?'selected="selected"':"").'>Completed</option>
							<option value="2" '.(($status == 2)?'selected="selected"':"").'>Failed</option>
						</select>
					</div>
				</td>
				<td>';
				
				if($certificate!=""){
					if(file_exists('./'.$certificate)){
						echo '<a href="'.base_url($certificate).'" target="_blank"><i class="material-icons">cloud_download</i> Click</a>';
					}else{
						if($status == 1){
							echo '<a href="javascript:;" onClick="$(`#myFile'.$spc_id.'`).click();"><i class="material-icons">cloud_upload</i> Upload</a>';
						}
					}
				}else{
					if($status == 1){
						echo '<a href="javascript:;" onClick="$(`#myFile'.$spc_id.'`).click();"><i class="material-icons">cloud_upload</i> Upload</a>';
					}
				}
			echo '<input type="file" class="btn btn-success btn-sm" id="myFile'.$spc_id.'" onchange="certificateUpload('.$spc_id.','.$stud_id.')" style="display:none;" accept="application/pdf, image/jpg, image/png, image/jpeg">
				</td>
				<td><button class="btn btn-success btn-sm" onClick="singleStudentChange('.$stud_id.')">Save</button></td>
			</tr>';
		}
	$i++;
	}
?>