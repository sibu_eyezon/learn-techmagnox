<div class="content">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header card-header-primary card-header-text">
					  <div class="card-text">
						<h4 class="card-title">Organization</h4>
					  </div>
					  <a href="<?= base_url('Teacher/addOrganization'); ?>" class="btn btn-primary btn-sm pull-right">Add Organization</a>
					</div>
					<div class="card-body">
						<?php
							if($this->session->flashdata('errors')!=NULL){
								echo '<div class="alert alert-warning">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										  <i class="material-icons">close</i>
										</button>
										<span>
										  <b> Warning - </b> '.$this->session->flashdata('error').'</span>
									  </div>';
							}
							if($this->session->flashdata('success')!=NULL){
								echo '<div class="alert alert-success">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										  <i class="material-icons">close</i>
										</button>
										<span>
										  <b> Success - </b> '.$this->session->flashdata('success').'</span>
									  </div>';
							}
						?>
						<div class="row">
							<div class="col-md-12">
								<div class="material-datatables">
									<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
									  <thead>
										<tr>
										  <th width="5%">Sl</th>
										  <th width="20%">Name</th>
										  <th width="15%">Contact</th>
										  <th width="15%">Address</th>
										  <th width="5%">Status</th>
										  <th width="15%">Action</th>
										</tr>
									  </thead>
									  <tbody>
										<?php $i=1; foreach($orgs as $mrow){ ?>
										<tr id="comm_<?= $mrow->id; ?>">
										  <td><?php echo $i; ?></td>
										  <td><?php 
											$logo = trim($mrow->logo);
											if(file_exists('./assets/img/institute/'.$logo)){
												echo '<img src="'.base_url('assets/img/institute/'.$logo).'" width="70"/><br>';
											}
											echo $mrow->name; 
											?></td>
										  <td><?= trim($mrow->contact); ?></td>
										  <td><?= trim($mrow->address); ?></td>
										  <td><?= (trim($mrow->status=='t'))? 'Active' : 'Blocked'; ?></td>
										  <td>
										  	<a href="<?= base_url('Teacher/addOrganization/?id='.$mrow->id); ?>" class="btn btn-primary btn-sm">View/Edit</a>
										  	<a href="javascript:removeOrganization(<?= $mrow->id; ?>);" class="btn btn-danger btn-sm">Delete</a>
										  </td>
										</tr>
										<?php $i++; } ?>
									  </tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url(); ?>assets/js/plugins/jquery.dataTables.min.js"></script>
<script>
	//$('#datatables').DataTable();
	
	var tabreq = $('#datatables').DataTable();

	function removeOrganization(comm_id)
	{
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You want to remove Organization",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
		  if (result.value) {
			$('#loading').show();
				$.ajax({
					url:baseURL+'Teacher/deleteOrganization?clid='+comm_id,
					type: 'GET',
					success: (res)=>{
						$('#loading').hide();
						if(res){
							$('#comm_'+comm_id).remove();
							$.notify({
								icon:"add_alert",
								message: "The Organization has been deleted"},
								{type:"success",
								timer:3e3,
								placement:{from:'top',align:'right'}
							})
						}else{
							$.notify({
								icon:"add_alert",
								message: "The Organization deletion error"},
								{type:"danger",
								timer:3e3,
								placement:{from:'top',align:'right'}
							})
						}
					},
					error: (errors)=>{
						console.log(errors);
					}
				});
		  }
		});
	}
</script>