<?php
$bg_image = "";
$ctitle = "";
$txt_before = "";
$txt_after = "";
$bolstud = "";

if(!empty($certify)){
	$bg_image = trim($certify[0]->bg_image);
	$ctitle = $certify[0]->title;
	$txt_before = $certify[0]->text_before;
	$txt_after = $certify[0]->text_after;
	$bolstud = ($certify[0]->student_name=='t')? 1:0;
}
?>
<style>
.form-error{
	color:red;
}
#upload-demo{
	width: 500px;
	height: 400px;
	padding-bottom:25px;
}
#wizardPicturePreview:hover {
	cursor:pointer;
}
.arrow {
  cursor: pointer;
  display: inline-block;  
  height: 40px;
  margin-left: 40px;
  margin-right: 40px;
  position: relative;
  line-height: 2.5em;
  padding-left: 1em;
  padding-right: 2em;
  background: white;
  color: black;
   &:after {
    // triangle hover color
    border-left: 20px solid white;
  }
}

.arrow:after {
  // the triangle
  content: "";
  position: absolute;
  border-bottom: 20px solid transparent;
  border-top: 20px solid transparent;
  height: 0px;
  width: 0px;
  margin-right: -20px;
  right: 0; 
}

.arrow:hover, .arrow:active {
  box-shadow: 0 5px 15px rgba(0,0,0,0.3);
  color: black;
  &:after {
    // triangle hover
    border-left: 20px solid yellow;
  }
}
#certify_preview {
	border: 1px solid black; 
	height: 400px; 
	border-radius: 10px;
	padding: 15px;
	background-color: #fff;
	background-image: url('<?php echo base_url('uploads/certificates/'.$bg_image); ?>');
	background-size: cover;
    background-position: 100% 100%;
	background-repeat: no-repeat;
}
#mheader {
	width:100%;
	height:115px;
}
#mheader #mlogo {
	width:150px;
}
#mheader #info {
	float: right;
}
#mbody {
	width:100%;
	height:210px;
}
#mbody #csignature {
	float: right;
}
#mfooter {
	width:100%;
	height: 40px;
	display: inline-flex;
    font-size: 0.8rem;
}
</style>
<script>
	function shownotify(msg)
	{
		$.notify({icon:"add_alert",message:msg},{type:'info',timer:3e3,placement:{from:'top',align:'right'}})
	}
</script>
<?php
	if($this->session->flashdata('error')!=""){
		echo '<script>shownotify(`'.$this->session->flashdata('error').'`);</script>';
	}
	$status = trim($prog[0]->status);
?>
<script src="<?php echo base_url(); ?>assets/vendor/ckeditor/ckeditor.js"></script>
<div class="content">
	<div class="container-fluid">
		<div class="card">
			<div class="card-header card-header-primary card-header-icon">
				<div class="card-icon">
					<i class="material-icons">create</i>
				</div>
				<h4 class="card-title">Edit Program <small class="text-danger">*'s are important</small></h4>
			</div>
			<div class="card-body">
				<form action="<?php echo base_url('Teacher/cuSCProgram'); ?>" id="frmProgram" method="POST" enctype="multipart/form-data" style="width:100%;">
					<div class="row">
						<!--SIDE MENUS-->
						<aside class="col-lg-2">
							<ul class="nav nav-pills nav-pills-info flex-column" role="tablist">
								<li class="nav-item">
								  <a class="nav-link active" data-toggle="tab" href="#curriculum" role="tablist">
									Curriculum
								  </a>
								</li>
								<li class="nav-item">
								  <a class="nav-link" data-toggle="tab" href="#basic_info" role="tablist">
									Basic Information
								  </a>
								</li>
								<li class="nav-item">
								  <a class="nav-link" data-toggle="tab" href="#imp_details" role="tablist">
									Important Details
								  </a>
								</li>
								<li class="nav-item">
								  <a class="nav-link" data-toggle="tab" href="#pricing" role="tablist">
									Pricing
								  </a>
								</li>
								<li class="nav-item">
								  <a class="nav-link" data-toggle="tab" href="#target_stud" role="tablist">
									Target Students
								  </a>
								</li>
								<?php if($prog[0]->type==4){ ?>
								<li class="nav-item">
								  <a class="nav-link" data-toggle="tab" href="#admission" role="tablist">
									Admission Details
								  </a>
								</li>
								<li class="nav-item">
								  <a class="nav-link" data-toggle="tab" href="#faculties" role="tablist">
									Faculty
								  </a>
								</li>
								<?php } if($prog[0]->type==4 || $prog[0]->type==3){ ?>
								<li class="nav-item">
								  <a class="nav-link" data-toggle="tab" href="#certificate" role="tablist">
									Certificate
								  </a>
								</li>
								<?php } ?>
								<li class="nav-item">
								  <a class="nav-link" data-toggle="tab" href="#settings" role="tablist">
									Setting
								  </a>
								</li>
								<li class="nav-item">
								  <a class="nav-link" data-toggle="tab" href="#conclusion" role="tablist">
									Conclusion
								  </a>
								</li>
							  </ul>
							  <button type="submit" class="btn btn-info btn-block"><?php echo ($status=='approved' || $status=='pending')? 'Update' : 'Save as draft' ?></button>
							  <button type="button" class="btn btn-danger btn-block" onClick="submitReview(<?= $prog[0]->id; ?>);">Submit for Review</button>
							  <button type="button" class="btn btn-success btn-block" onClick="previewProgram();">Preview</button>
						</aside>
						<!--SIDE MENUS ENDS-->
						<!--MAIN OCNTENTS-->
						<div class="col-lg-10">
							<div class="tab-content">
								<div class="tab-pane active" id="curriculum">
									<?php include 'curriculum.php'; ?>
								</div>
								<div class="tab-pane" id="basic_info">
									<h4 class="mx-3">Course Landing Information</h4>
									<hr style="border:2px solid #eee;">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
											  <label for="title" class="text-dark">Program Title <span class="text-danger">*</span></label>
											  <input type="text" class="form-control" id="title" name="title" placeholder="Max length: 100 characters" value="<?php echo trim($prog[0]->title); ?>">
											  <input type="hidden" id="status" name="status" value="<?php echo trim($status); ?>">
											  <input type="hidden" name="type" id="type" value="<?= $prog[0]->type; ?>"/>
											  <input type="hidden" name="pid" id="pid" value="<?= $prog[0]->id; ?>"/>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label for="title" class="text-dark">Program Code <span class="text-danger">*</span></label>
												<input type="text" class="form-control" id="pcode" name="pcode" value="<?php echo (trim($prog[0]->code)=="")? 'Auto Generated': trim($prog[0]->code); ?>" readonly>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<?php
													if($progtype=='selfbased'){
														echo 'Sub-Category: '.trim($prog[0]->category);
														echo '<input type="hidden" id="category" name="category" value="'.trim($prog[0]->category).'">';
													}else{
														echo '<select class="selectpicker" data-style="select-with-transition" name="category" id="category" data-title="Select sub-category <span class=`text-danger`>*</span>">
															<option value="Training Program" '.((trim($prog[0]->category)=='Training Program')? 'selected':'').'>Training Program</option>
															<option value="Seminar Program" '.((trim($prog[0]->category)=='Seminar Program')? 'selected':'').'>Seminar Program</option>
															<option value="Webinar Program" '.((trim($prog[0]->category)=='Webinar Program')? 'selected':'').'>Webinar Program</option>
														</select>';
														/*<option value="Corporate Program" '.((trim($prog[0]->category)=='Corporate Program')? 'selected':'').'>Corporate Program</option>
															<option value="Tutor" '.((trim($prog[0]->category)=='Tutor')? 'selected':'').'>Tutor</option>
															<option value="Diploma Program" '.((trim($prog[0]->category)=='Diploma Program')? 'selected':'').'>Diploma Program</option>*/
													}
												?>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<select name="prog_subcategory" id="prog_subcategory" class="selectpicker" data-title="Select Category" data-style="select-with-transition" data-size="7" required>
													<?php
														if(!empty($pcategory)){
															foreach($pcategory as $pcat){
																$cat_id = $pcat->id;
																echo '<option value="'.$cat_id.'" disabled>'.trim($pcat->name).'</option>';
																$subcat = $this->Admin_model->getParentCategories($cat_id);
																if(!empty($subcat)){
																	foreach($subcat as $pscat){
																		echo '<option value="'.$pscat->id.'" '.(($prog[0]->prog_subcategory==$pscat->id)? 'selected':'').'>'.trim($pscat->name).'</option>';
																	}
																}
															}
														}else{
															echo '<option value="">No category added.</option>';
														}
													?>
												</select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<select name="prog_level" id="prog_level" class="selectpicker" data-title="Select Level" data-style="select-with-transition" required="true">
												<option value="1" <?php echo ($prog[0]->prog_level=='1')? 'selected':''; ?>>Live Buddy</option>
											<option value="2" <?php echo ($prog[0]->prog_level=='2')? 'selected':''; ?>>Mentorship</option>
											<option value="3" <?php echo ($prog[0]->prog_level=='3')? 'selected':''; ?>>Certification</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<?php
													if($progtype=='selfbased'){
														echo 'Program Type: Online';
														echo '<input type="hidden" id="prog_type" name="prog_type" value="'.trim($prog[0]->ptype).'">';
													}else{
														echo '<select class="selectpicker" data-style="select-with-transition" name="prog_type" id="prog_type" data-title="Select one type <span class=`text-danger`>*</span>">
															<option value="Online" '.((trim($prog[0]->ptype)=='Online')? 'selected':'').'>Online Program</option>
															<option value="Regular"'.((trim($prog[0]->ptype)=='Regular')? 'selected':'').'>Regular Program</option>
															<option value="Distance" '.((trim($prog[0]->ptype)=='Distance')? 'selected':'').'>Distance Program</option>
														</select>';
													}
												?>
											</div>
										</div>
										<div class="col-md-6">
											<div class="row">
												<div class="col-sm-6">
													<div class="form-group">
														 <label for="total_seat" class="text-dark">Total Seats (0 for infinite) <span class="text-danger">*</span></label>
														<input type="number" name="total_seat" id="total_seat" value="<?php echo $prog[0]->total_seat; ?>" class="form-control" required="true">
													</div>
												</div>
												<div class="col-sm-6">
													<div class="form-group">
														<label for="total_hrs" class="text-dark">Total Hours <span class="text-danger">*</span></label>
														<input type="number" name="total_hrs" id="total_hrs" value="<?php echo $prog[0]->prog_hrs; ?>" class="form-control" required="true">
													</div>
												</div>
											</div>
										</div>
									</div>
									<?php if($progtype=='certificate'){ ?>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
											  <select class="selectpicker" data-style="select-with-transition" data-title="Select Academic Year" name="aca_year" id="aca_year">
												<?php
													if(!empty($ayr)){
														foreach($ayr as $ayow){
															echo '<option value="'.$ayow->sl.'" '.(($prog[0]->aca_year==$ayow->sl)? 'selected' : '').'>'.$ayow->yearnm.'</option>';
														}
													}
												?>
											  </select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												 <label for="duration">Duration <span class="text-danger">*</span></label>
												 <div class="input-group">
													<div class="input-group-prepend">
														<input type="text" name="duration" id="duration" class="form-control" value="<?php echo (int)trim($prog[0]->duration); ?>">
														<select class="custom-select custom-select-sm" name="dtype" id="dtype">
															<option value="">Duration <span class="text-danger">*</span></option>
															<option value="day" <?php echo (trim($prog[0]->dtype)=='day')? 'selected':''; ?>>Days</option>
															<option value="month" <?php echo (trim($prog[0]->dtype)=='month')? 'selected':''; ?>>Months</option>
															<!--<option value="year" <?php //echo (trim($prog[0]->dtype)=='year')? 'selected':''; ?>>Years</option>-->
														</select>
													</div>
												 </div>
											</div>
										</div>
									</div>
									<div class="row">	
										<div class="col-md-6">
											<div class="form-group">
												<label for="sdate">Start Date <span class="text-danger">*</span></label>
												<input type="datetime-local" name="sdate" id="sdate" value="<?php echo strftime('%Y-%m-%dT%H:%M:%S', strtotime($prog[0]->start_date)); ?>" class="form-control">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label for="edate">End Date <span class="text-danger">*</span></label>
												<input type="datetime-local" name="edate" id="edate" value="<?php echo strftime('%Y-%m-%dT%H:%M:%S', strtotime($prog[0]->end_date)); ?>" class="form-control">
											</div>
										</div>
									</div>
									<?php } ?>
								</div>
								<div class="tab-pane" id="imp_details">
									<h4 class="mx-3">Course More Details</h4>
									<hr style="border:2px solid #eee;">
									<div class="row justify-content-center">
										<div class="col-sm-4">
											<div class="fileinput fileinput-new text-center">
												<div class="fileinput-new thumbnail">
													<img src="<?php echo base_url().'assets/img/banner/'.$prog[0]->banner; ?>" onerror="this.src='<?php echo base_url(); ?>assets/img/image_placeholder.jpg'" class="picture-src" id="wizardPicturePreview" title="" />
												</div>
												<h6 class="description">Choose Banner</h6>
											</div>
											<input type="file" class="custom-file-input" name="avatar" id="avatar" onChange="readURL(this);" accept="image/jpg, image/jpeg, image/png" style="visibility:hidden;">
										</div>
										<div class="col-sm-6">
											<div class="form-group mb-5">
												<?php if($prog[0]->id!=''){ 
													if($prog[0]->program_brochure!=""){
														if(file_exists('./uploads/programs/'.$prog[0]->program_brochure)){
															echo '<h6 class="text-center"><a href="'.base_url('uploads/programs/'.$prog[0]->program_brochure).'" target="_blank"></a></h6>';
														}else{
															echo '<h6 class="text-center">File is missing.</h6>';
														}
													}else{
														echo '<h6 class="text-center">Upload the brochure below</h6>';
													}
												} ?>
												<div class="custom-file">
													<input type="file" class="custom-file-input" name="schedule" id="schedule" accept=".doc, .docx, application/pdf,application/vnd.ms-excel">
													<label class="custom-file-label text-dark" for="schedule">Choose program brochure</label>
												</div>
											</div>
											<div class="form-group">
												<?php if($prog[0]->id!=''){ 
													if($prog[0]->certificate_sample!=""){
														if(file_exists('./uploads/programs/'.$prog[0]->certificate_sample)){
															echo '<h6 class="text-center"><a href="'.base_url('uploads/programs/'.$prog[0]->certificate_sample).'" target="_blank"></a></h6>';
														}else{
															echo '<h6 class="text-center">File is missing.</h6>';
														}
													}else{
														echo '<h6 class="text-center">Upload the certificate below</h6>';
													}
												} ?>
												<div class="custom-file">
													<input type="file" class="custom-file-input" name="brochure" id="brochure" accept="image/jpg, image/jpeg, image/png">
													<label class="custom-file-label text-dark" for="brochure">Choose sample certificate</label>
												</div>
											</div>
										</div>
										<div class="col-lg-10 mt-3">
											<div class="form-group">
												 <label for="intro">Introduction (YouTube Share link)</label><br>
												<textarea name="intro" id="intro" class="form-control" cols="80" rows="5"><?php echo $prog[0]->intro_video_link; ?></textarea>
											</div>
										</div>
									</div>
									<div class="row mb-3">
										<div class="col-md-12">
											<div class="form-group">
												 <label for="overview">Overview</label><br>
												<textarea name="overview" id="overview" class="form-control" cols="80" rows="5"><?php echo $prog[0]->overview; ?></textarea>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="target_stud">
									<h4 class="mx-3">What will your students learn from this course?</h4>
									<hr style="border:2px solid #eee;">
									<h4 class="mx-3">Skill Sets</h4>
									<div class="row justify-content-center">
										<div class="col-sm-12">
											<?php 
												 $skills = $this->Admin_model->getAllSkills();
												 $pskills = (is_array(json_decode($prog[0]->prog_skills)))? json_decode($prog[0]->prog_skills) : array();
												 foreach($skills as $srow){
													 if(in_array($srow->id, $pskills)){
														 echo '<span class="label label-primary mr-2">'.$srow->name.'</span>';
													 }
												 }
											?>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<select name="prog_skills[]" id="prog_skills" multiple="true" class="selectpicker" data-size="5" data-style="select-with-transition" data-title="Multi-select skills">
													<?php
														foreach($skills as $skow){
															if(in_array($skow->id, $pskills)){
																echo '<option value="'.$skow->id.'" selected>'.trim($skow->name).'</option>';
															}else{
																echo '<option value="'.$skow->id.'">'.trim($skow->name).'</option>';
															}
														}
													?>
												</select>
											</div>
										</div>
									</div>
									<div class="row justify-content-center">
										<div class="col-sm-12">
											<div class="form-group">
												 <label for="requirement">Requirements</label><br>
												<textarea name="requirement" id="requirement" class="form-control" cols="80" rows="5"><?php echo $prog[0]->requirements; ?></textarea>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label for="title">What will student learn? <span class="text-danger">*</span></label><br>
												<textarea class="form-control" name="wlearn" id="wlearn" style="width:100%;"><?php echo trim($prog[0]->why_learn); ?></textarea>
											</div>
										</div>
									</div>
									<?php
										$prog_benefits = $this->Member->getAllProgramBenefits($prog_id);
										$pbcount = count($prog_benefits);
									?>
									<div class="row">
										<div class="col-sm-12">
											<h4 class="mx-3">Benefits
												<?php if($pbcount<4){ ?>
												<button type="button" onClick="showAjaxModal('<?php echo base_url('Certifyprogram/benefits_cu/'.$prog_id.'/'.$progtype); ?>', 'Add Benefit');" class="btn btn-outline-primary btn-rounded btn-sm pull-right">Add Benefits</button>
												<?php } ?>
											</h4>
											<div class="form-group" id="prog_benefits">
											<?php
												if(!empty($prog_benefits)){
													
													foreach($prog_benefits as $pbow){
														$pb_id = $pbow->id;
														echo '<div class="card bg-dark" id="pb_'.$pb_id.'">
															<div class="card-body">
																<h4 class="card-title" id="pb_title_'.$pb_id.'">'.trim($pbow->txt_benefit).'
																	<a href="javascript:deleteBenefits('.$pb_id.');" class="btn btn-link text-white pull-right btn-sm"><i class="material-icons">delete</i></a>
																	<a href="javascript:showAjaxModal(`'.base_url('Certifyprogram/benefits_cu/'.$prog_id.'/'.$progtype.'/'.$pb_id).'`, `Edit Benefit`);" class="btn btn-link text-white pull-right btn-sm"><i class="material-icons">edit</i></a>
																</h4>
																<div id="pb_dtls_'.$pb_id.'">
																'.trim($pbow->txt_benefit_dtls).'
																</div>
															</div>
														</div>';
													}
												}
											?>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="pricing">
									<h4 class="mx-3">Course Price Tier</h4>
									<hr style="border:2px solid #eee;">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												 <div class="checkbox-radios">
													<div class="form-check form-check-inline">
													  <label class="form-check-label text-dark">
														<input class="form-check-input" type="radio" <?php echo ($prog[0]->feetype=='Free')? 'checked' : ''; ?> value="Free" name="feetype" id="free"> Free
														<span class="form-check-sign">
														  <span class="check"></span>
														</span>
													  </label>
													</div>
													<div class="form-check form-check-inline">
													  <label class="form-check-label text-dark">
														<input class="form-check-input" type="radio" <?php echo ($prog[0]->feetype=='Paid')? 'checked' : ''; ?> value="Paid" name="feetype" id="paid"> Paid
														<span class="form-check-sign">
														  <span class="check"></span>
														</span>
													  </label>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="row">
												<div class="col-sm-6">
													<div class="form-group">
														 <label for="fees" class="text-dark">Total Fees</label>
														<input type="text" name="fees" id="fees" class="form-control" value="<?php echo trim($prog[0]->total_fee); ?>" disabled>
													</div>
												</div>
												<div class="col-sm-6">
													<div class="form-group">
														<label for="fdetails" class="text-dark">Discount (If any)</label>
														<input type="number" name="rebate" id="rebate" value="<?php echo $prog[0]->discount; ?>" class="form-control" disabled>
													</div>
												</div>
											</div>
											
										</div>
									</div>
									<?php if(trim($prog[0]->feetype)=='Paid'){ ?>
										<div class="row mb-3" id="feesd">
											<div class="col-sm-12">
												<div class="form-group">
													 <label for="fdetails">Fees Details</label><br>
													<textarea name="fdetails" id="fdetails" class="form-control" cols="80" rows="5"><?php echo $prog[0]->fee_details; ?></textarea>
												</div>
											</div>
										</div>
										<script>
										CKEDITOR.replace('fdetails', {
											extraPlugins: 'uploadimage,colorbutton,colordialog,autoembed,embedsemantic,mathjax,codesnippet,font,justify,table,specialchar, tabletools, uicolor',
											height: 200,
											mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML'
										});
										</script>
									<?php } ?>
								</div>
								<div class="tab-pane" id="admission">
									<h4 class="mx-3">Admission Procedure</h4>
									<hr style="border:2px solid #eee;">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<select class="selectpicker" data-style="select-with-transition" name="apply_type" id="apply_type" title="Single Select <span class='text-danger'>*</span>">
													<option value="0" <?php if($prog[0]->apply_type=='0'){ echo 'selected'; } ?>>All Approved</option>
													<option value="1" <?php if($prog[0]->apply_type=='1'){ echo 'selected'; } ?>>Selective Approved</option>
												</select>
											</div>
										</div>
										<div class="col-md-6" id="stype" style="display:none;">
											<div class="form-group">
											<?php
												if(isset($prog[0]->screen_type)){
													$st = explode(',', $prog[0]->screen_type);
												}else{
													$st = array();
												}
											?>
											<select class="selectpicker" data-style="select-with-transition" title="Screen type <span class='text-danger'>*</span>" multiple name="screen_type[]" id="screen_type">
												<option value="0" <?php if(in_array(0,$st)){ echo 'selected'; } ?>>Manual Checking</option>
												<option value="1" <?php if(in_array(1,$st)){ echo 'selected'; } ?>>Online Exam</option>
												<option value="2" <?php if(in_array(2,$st)){ echo 'selected'; } ?>>Interview</option>
											</select>
										</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label for="fdetails">Start Date <span class="text-danger">*</span></label>
												<input type="date" name="adstart" id="adstart" value="<?php echo $prog[0]->astart_date; ?>" class="form-control">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label for="fdetails">End Date <span class="text-danger">*</span></label>
												<input type="date" name="adend" id="adend" value="<?php echo $prog[0]->aend_date; ?>" class="form-control">
											</div>
										</div>
									</div>
									<div class="row mb-3">
										<div class="col-sm-12">
											<div class="form-group">
												<label for="fdetails">Criteria <span class="text-danger">*</span></label>
												<textarea name="criteria" id="criteria" class="form-control"><?php echo $prog[0]->criteria; ?></textarea>
											</div>
										</div>
									</div>
									<div class="row mb-3">
										<div class="col-sm-12">
											<div class="form-group">
												 <label for="placement">Placement</label><br>
												<textarea name="placement" id="placement" class="form-control" cols="80" rows="5"><?php echo $prog[0]->placement; ?></textarea>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="faculties">
									<h4 class="mx-3">Instructor List
									<button type="button" class="btn btn-sm btn-primary pull-right" onClick="getInviteModal(<?php echo $prog[0]->id; ?>)">Invite Now</button>
									</h4>
									<hr style="border:2px solid #eee;">
									<?php include 'instructors.php'; ?>
								</div>
								<div class="tab-pane" id="certificate">
									<h4 class="mx-3">Course Certificate</h4>
									<hr style="border:2px solid #eee;">
									<div class="row">
										<div class="col-sm-4">
											<label class="text-dark">Will ceritified students?</label>
											<div class="form-group">
												 <div class="checkbox-radios">
													<div class="form-check form-check-inline">
													  <label class="form-check-label text-dark">
														<input class="form-check-input" type="radio" <?php echo ($prog[0]->bolcertify=='auto')? 'checked' : ''; ?> value="auto" name="bolcertify" id="auto"> Yes
														<span class="form-check-sign">
														  <span class="check"></span>
														</span>
													  </label>
													</div>
													<div class="form-check form-check-inline">
													  <label class="form-check-label text-dark">
														<input class="form-check-input" type="radio" <?php echo ($prog[0]->bolcertify=='no')? 'checked' : ''; ?> value="no" name="bolcertify" id="no"> No
														<span class="form-check-sign">
														  <span class="check"></span>
														</span>
													  </label>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div id="show_certify" style="display:none;">
										<div class="row justify-content-center">
											<div class="col-sm-6">
												<div class="custom-file">
													<input type="file" class="custom-file-input" name="bg_image" id="bg_image" accept="image/*">
													<label class="custom-file-label text-dark" for="schedule">Choose background image</label>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<label for="ctitle">Main Title</label>
													<input type="text" class="form-control" name="ctitle" id="ctitle" value="<?php echo $ctitle; ?>"/>
												</div>
											</div>
										</div>
										<div class="row justify-content-center">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="txt_before">Text Before</label>
													<input type="text" class="form-control" name="txt_before" id="txt_before" value="<?php echo $txt_before; ?>"/>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<label for="txt_after">Text After</label>
													<input type="text" class="form-control" name="txt_after" id="txt_after" value="<?php echo $txt_after; ?>"/>
												</div>
											</div>
										</div>
										<div class="row justify-content-center">
											<div class="col-sm-6">
												<label class="text-dark">Will student's name apprear?</label>
												<div class="form-group">
													 <div class="checkbox-radios">
														<div class="form-check form-check-inline">
														  <label class="form-check-label text-dark">
															<input class="form-check-input" type="radio" value="1" <?php echo ($bolstud=='1')? 'checked' : ''; ?> name="bolstud" id="cyes"> Yes
															<span class="form-check-sign">
															  <span class="check"></span>
															</span>
														  </label>
														</div>
														<div class="form-check form-check-inline">
														  <label class="form-check-label text-dark">
															<input class="form-check-input" type="radio" value="0" <?php echo ($bolstud=='0')? 'checked' : ''; ?> name="bolstud" id="cno"> No
															<span class="form-check-sign">
															  <span class="check"></span>
															</span>
														  </label>
														</div>
													</div>
												</div>
											</div>
										</div>
										<hr>
										<h4 class="text-center font-weight-bold">Preview</h4>
										<div class="row justify-content-center mt-3">
											<div class="col-sm-8 mx-auto" id="certify_preview">
												<div id="mheader">
													<img src="<?php echo base_url('assets/img/logo.png'); ?>" id="mlogo"/>
													<div id="info">
														<p class="text-left">
														www.techmagnox.com<br>
														www.billionskills.com<br>
														magnox.iitkgp@gmail.com<br>
														+91 9932242598
														</p>
													</div>
												</div>
												<div id="mbody">
												<h4 class="text-center" style="color:#9e9e0e;font-family: fangsong; font-weight: 600; font-size: 1.5rem;" id="mtitle"><i>Certificate of Completion</i></h4>
												<p>
												<span id="btext">before_text</span> <span id="cstud" class="text-primary" style="display:none;">Student_Name</span> <span id="atext">after_text</span> in <?php echo trim($prog[0]->title); ?>  in our company from <?php echo date('jS M, Y',strtotime($prog[0]->start_date)); ?> to <?php echo date('jS M, Y',strtotime($prog[0]->end_date)); ?> 
												</p>
												<h6 class="text-left" id="csignature">Course Co-ordinator<br>From<br>Magnox Technologies Pvt. Ltd.</h6>
												</div>
												<div id="mfooter">
													<p class="text-left">
													Magnox Technologies Pvt. Ltd., Mani Bhandar (7th Floor), Webel Bhawan, Sector V, Salt Lake, Kolkata - 700091
													</p>
													<p class="text-left">
														Certificate no: BS505
													</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="settings">
									<h4 class="mx-3">Course Status</h4>
									<hr style="border:2px solid #eee;">
									<div class="row">
										<div class="col-md-3">
											<button type="button" class="btn btn-info btn-md btn-block" disabled><?php echo ucfirst($prog[0]->status); ?></button>
										</div>
										<div class="col-md-9">
											New students cannot find your course via search, but existing students can still access content.
										</div>
									</div>
									<div class="row mb-3">
										<div class="col-md-3">
											<button type="button" class="btn btn-info btn-md btn-block">Delete</button>
										</div>
										<div class="col-md-9">
											We promise students lifetime access, so courses cannot be deleted after students have enrolled.
										</div>
									</div>
									<h4 class="mx-3">Enrollment (Privacy)</h4>
									<hr style="border:2px solid #eee;">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<select class="selectpicker" name="view_mode" id="view_mode" data-style="select-with-transition" data-title="Set privacy">
													<option value="1" <?php echo ($prog[0]->view_mode=='1')? 'selected': ''; ?>>Public</option>
													<option value="2" <?php echo ($prog[0]->view_mode=='2')? 'selected': ''; ?>>Private (Invitation Only)</option>
													<option value="3" <?php echo ($prog[0]->view_mode=='3')? 'selected': ''; ?>>Private (Password Protected)</option>
												</select>
											</div>
											<?php
												$view_mode = intval($prog[0]->view_mode);
												if($view_mode>1){
													if($view_mode==2){
														echo 'Your Program Link: https://billionskills.com/get_program_details/?id='.base64_encode($prog[0]->id);
													}else if($view_mode==3){
														echo 'Your Program Link: https://billionskills.com/get_program_details/?id='.base64_encode($prog[0]->id).'<br> Password: '.trim($prog[0]->vmpassword);
													}
												}
											?>
											<div id="show_1" style="display:none;">
												<p>
												Public courses show up in search results and are available for anyone to take on BillionSkills.
												</p>
											</div>
											<div id="show_2" style="display:none;">
												<p>
												If a course's enrollment page is invitation only, the course won't show up in search results on BillionSkills. Accept new student requests and send invitations from the "Students" page found under "Course Management" in the left navigation.
												</p>
											</div>
											<div id="show_3" style="display:none;">
												<div class="form-group">
													<input type="text" name="vmpassword" id="vmpassword" required="true" class="form-control"/>
													<span class="text-danger">Password need not be blank</span>
												</div>
												<p>
												If a course's enrollment page is password protected, the course won't show up in search results on BillionSkills. Instead, share the course URL and password directly with students you want to enroll. Keep in mind this provides only a low level of security. Students could share your course URL with an embedded password.
												</p>
											</div>
											
										</div>
									</div>
								</div>
								<div class="tab-pane" id="conclusion">
									<h4 class="mx-3">Conclusion</h4>
									<hr style="border:2px solid #eee;">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label for="title">Welcome Message <span class="text-danger">*</span></label><br>
												<textarea class="form-control" name="welcome" id="welcome" style="width:100%;"><?php echo trim($prog[0]->welcome_msg); ?></textarea>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group">
												<label for="title">Congratulation Message <span class="text-danger">*</span></label><br>
												<textarea class="form-control" name="congrats" id="congrats" style="width:100%;"><?php echo trim($prog[0]->congrats_msg); ?></textarea>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--MAIN OCNTENTS ENDS-->
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!--===================================================-->
<div class="modal fade" id="programModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            <i class="material-icons">clear</i>
          </button>
        </div>
        <div class="modal-body">
		  
		</div>
      </div>
    </div>
</div>
<div class="modal fade" id="programLgModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            <i class="material-icons">clear</i>
          </button>
        </div>
        <div class="modal-body">
		  
		</div>
      </div>
    </div>
</div>
<div class="modal fade" id="noticeM" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			  <h4 class="modal-title">Invite Teacher Form</h4>
			  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				<i class="material-icons">clear</i>
			  </button>
			</div>
			<form id="frmInvite" enctype="multipart/form-data">
				<div class="modal-body">
				  <div class="form-group mb-0">
					<label for="inv_email" class="text-dark">Email*</label>
					<input type="hidden" name="inv_prog" id="inv_prog" value="<?php echo $prog_id; ?>"/>
					<input type="hidden" name="inv_role" id="inv_role" value="Teacher"/>
					<input type="hidden" name="inv_exist" id="inv_exist" value="0"/>
					<input type="hidden" name="semid" id="semid" value="0"/>
					<input type="hidden" name="ay_id" id="ay_id" value="0"/>
					<input type="hidden" name="enroll" id="enroll" value="0"/>
					<input type="hidden" name="roll" id="roll" value="0"/>
					<input type="text" class="form-control" list="inv_users" name="inv_email" id="inv_email" required="true" email="true">
					<datalist id="inv_users">
					
					</datalist>
				  </div>	
					<div class="form-group mb-0">
					<label for="inv_fname" class="text-dark">Firstname</label>
					<input type="text" class="form-control" name="inv_fname" id="inv_fname" required="true" minLength="2">
				  </div>
				  <div class="form-group mb-0">
					<label for="inv_lname" class="text-dark">Lastname*</label>
					<input type="text" class="form-control" name="inv_lname" id="inv_lname" required="true">
				  </div>
				  <div class="form-group mb-0">
					<label for="inv_phone" class="text-dark">Phone*</label>
					<input type="text" class="form-control" name="inv_phone" id="inv_phone" required="true" minLength="10" maxLength="10" digits="true">
				  </div>
				</div>
				<div class="modal-footer">
				  <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
				  <input type="reset" style="visibility:hidden">
				  <button type="submit" id="btn_invite" class="btn btn-success">Invite</button>
				  
				</div>
			</form>
		</div>
	</div>
</div>
<script src="<?php echo base_url(); ?>assets/js/program.js"></script>
<script>
	$(document).ready(function(){
		toggleFees('<?php echo trim($prog[0]->feetype); ?>');
		toggleScreenType('<?php echo trim($prog[0]->apply_type); ?>');
		CKEDITOR.replace('overview', {
			extraPlugins: 'uploadimage,colorbutton,colordialog,autoembed,embedsemantic,mathjax,codesnippet,font,justify,table,specialchar, tabletools, uicolor',
			height: 200,
			mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML'
		});
		CKEDITOR.replace('requirement', {
			extraPlugins: 'uploadimage,colorbutton,colordialog,autoembed,embedsemantic,mathjax,codesnippet,font,justify,table,specialchar, tabletools, uicolor',
			height: 200,
			mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML'
		});
		CKEDITOR.replace('placement', {
			extraPlugins: 'uploadimage,colorbutton,colordialog,autoembed,embedsemantic,mathjax,codesnippet,font,justify,table,specialchar, tabletools, uicolor',
			height: 200,
			mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML'
		});
		CKEDITOR.replace('wlearn', {
			extraPlugins: 'uploadimage,colorbutton,colordialog,autoembed,embedsemantic,mathjax,codesnippet,font,justify,table,specialchar, tabletools, uicolor',
			height: 200,
			mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML'
		});
		CKEDITOR.replace('welcome', {
			extraPlugins: 'uploadimage,colorbutton,colordialog,autoembed,embedsemantic,mathjax,codesnippet,font,justify,table,specialchar, tabletools, uicolor',
			height: 200,
			mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML'
		});
		CKEDITOR.replace('congrats', {
			extraPlugins: 'uploadimage,colorbutton,colordialog,autoembed,embedsemantic,mathjax,codesnippet,font,justify,table,specialchar, tabletools, uicolor',
			height: 200,
			mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML'
		});
		
		$("#bg_image").on("change", function() {
		  var fileName = $(this).val().split("\\").pop();
		  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
		  setBackground(this);
		});
	});
	$('input[type=radio][name=feetype]').on('change', ()=>{
		var radioValue = $('input[type=radio][name=feetype]:checked').val();
		toggleFees(radioValue);
	});
	function toggleFees(rvalue)
	{
		if(rvalue!=''){
			if(rvalue=='Free')
			{
				$('#fees').attr('disabled', true);
				$('#rebate').attr('disabled', true);
			}else{
				$('#fees').removeAttr('disabled');
				$('#rebate').removeAttr('disabled');
			}
		}
	}
	$('#apply_type').on('change', ()=>{
		toggleScreenType($('#apply_type').val());
	});
	function toggleScreenType(at_id)
	{
		if(at_id==0){
			$('#stype').hide();
		}else{
			$('#stype').show();
		}
	}
	$('#wizardPicturePreview').on('click', ()=>{
		$('#avatar').click();
	});
	function readURL(input) {
		if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function(e) {
		  $('#wizardPicturePreview').attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
		}
	  }
	$('#view_mode').on('change', ()=>{
		for(var i=1; i<=3; i++){
			$('#show_'+i).hide();
		}
		$('#show_'+$('#view_mode').val()).show();
	})
	
	$("input[type='radio'][name='bolcertify']").on('click', ()=>{
		if($("input[type='radio'][name='bolcertify']:checked")){
			var radioVal = $("input[type='radio'][name='bolcertify']:checked").val();
			toggleCertify(radioVal);
		}
		
	});
	function toggleCertify(radioVal)
	{
		if(radioVal=='auto'){
			$('#show_certify').show();
		}else if(radioVal=='no'){
			$('#show_certify').hide();
		}
	}
	/*=================================*/
	function setBackground(input){
		var bgImg;
		if (input.files && input.files[0]) {
		  var reader = new FileReader();
			reader.onload = function (e) {
				bgImg = e.target.result;
				$('#certify_preview').css('background-image', "url('"+bgImg+"')");
			}
			
			reader.readAsDataURL(input.files[0]);
		}
		else {
			Swal.fire("Sorry","You're browser doesn't support the FileReader API", "error");
		}
	}
	$('#ctitle').on('keyup', ()=>{
		$('#mtitle').html($('#ctitle').val());
	});
	$('#txt_before').on('keyup', ()=>{
		$('#btext').html($('#txt_before').val());
	});
	$('#txt_after').on('keyup', ()=>{
		$('#atext').html($('#txt_after').val());
	});
	$("input[type='radio'][name='bolstud']").on('click', ()=>{
		if($("input[type='radio'][name='bolstud']:checked")){
			var radioVal = $("input[type='radio'][name='bolstud']:checked").val();
			toggleStudent(radioVal);
		}
	});
	function toggleStudent(radioVal)
	{
		if(radioVal==='1'){
			$('#cstud').show();
		}else if(radioVal==='0'){
			$('#cstud').hide();
		}
	}
	
	function submitReview(prog_id)
	{
		swal({
			title: 'Are you sure?',
			text: "You want to submit for review",
			type: 'warning',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: 'Yes, submit it!',
			buttonsStyling: false
		}).then(function() {
			$.ajax({
				url: baseURL+'Certifyprogram/submit_preview/'+prog_id,
				type: 'GET',
				success: (res)=>{
					if(res)
					{
						$.notify({icon:"add_alert",message:'The program has been submitted for review.'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})	
					}else{
						$.notify({icon:"add_alert",message:'Something went wrong. Submition error!!!.'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})
					}
				}
			})
		}).catch(swal.noop)
	}
	function showAjaxModal(url, header)
	{
		jQuery('#programModal .modal-title').html(header);
		jQuery('#programModal .modal-body').html("");
		$.ajax({
			url: url,
			success: (response)=>{
				jQuery('#programModal .modal-body').html(response);
			}
		});
		$('#programModal').modal('show', {backdrop: 'true'});
	}
	function showLargeModal(url, header)
	{
		jQuery('#programLgModal .modal-body').html("");
		jQuery('#programLgModal .modal-title').html(header);
		$.ajax({
			url: url,
			success: (response)=>{
				jQuery('#programLgModal .modal-body').html(response);
				jQuery('#programLgModal .modal-title').html(header);
				
			}
		});
		$('#programLgModal').modal('show', {backdrop: 'true'});
	}
	function setProgramBenefits(res)
	{
		var obj = JSON.parse(res);
		if(obj['type']=='add'){
			var hmtl_body = '<div class="card bg-dark" id="pb_'+obj['msg'].pb_id+'"><div class="card-body"><h4 class="card-title" id="pb_title_'+obj['msg'].pb_id+'">'+obj['msg'].txt_benefit+'<a href="javascript:deleteBenefits('+obj['msg'].pb_id+');" class="btn btn-link text-white pull-right btn-sm"><i class="material-icons">delete</i></a><a href="javascript:showAjaxModal(`'+baseURL+'Certifyprogram/benefits_cu/'+obj['msg'].program_id+'/'+obj['msg'].progtype+'/'+obj['msg'].pb_id+'`, `Edit Benefit`);" class="btn btn-link text-white pull-right btn-sm"><i class="material-icons">edit</i></a></h4><div id="pb_dtls_'+obj['msg'].pb_id+'">'+obj['msg'].txt_benefit_dtls+'</div></div></div>';
			$('#prog_benefits').append(hmtl_body);
		}else{
			$('#pb_title_'+obj['msg'].pb_id).html(obj['msg'].txt_benefit+'<a href="javascript:deleteBenefits('+obj['msg'].pb_id+');" class="btn btn-link text-white pull-right btn-sm"><i class="material-icons">delete</i></a><a href="javascript:showAjaxModal(`'+baseURL+'Certifyprogram/benefits_cu/'+obj['msg'].program_id+'/'+obj['msg'].progtype+'/'+obj['msg'].pb_id+'`, `Edit Benefit`);" class="btn btn-link text-white pull-right btn-sm"><i class="material-icons">edit</i></a>');
			$('#pb_dtls_'+obj['msg'].pb_id).html(obj['msg'].txt_benefit_dtls);
		}
	}
	function deleteBenefits(id)
	{
		swal({
			title: 'Are you sure?',
			text: "You want to remove this benefit.",
			type: 'warning',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: 'Yes, delete it!',
			buttonsStyling: false
		}).then(function() {
			$.ajax({
				url: baseURL+'Certifyprogram/benefit_delete/'+id,
				type: 'GET',
				success: (res)=>{
					if(res)
					{
						$.notify({icon:"add_alert",message:'The benefit has been deleted.'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})	
						$('#pb_'+id).remove();
					}else{
						$.notify({icon:"add_alert",message:'Something went wrong. Deletion error!!!.'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})
					}
				}
			})
		}).catch(swal.noop)
	}
</script>