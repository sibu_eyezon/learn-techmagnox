<style>
.instructor-list {
	padding: 10px 10px;
    margin-bottom: 0rem;
    place-items: flex-start;
    display: flow-root;
}
</style>
<?php
$if_about_me = 0;
$if_photo = 0;
$facMem = $this->Member->getAllTeachersByPid($prog[0]->id);
/*echo '<div class="row">';
foreach($facMem as $fcow){
	$if_photo = (trim($fcow->photo_sm) == "")? 0:1;
	$if_about_me = (trim($fcow->about_me) == "")? 0:1;
	echo '<div class="col-sm-4">
		<div class="d-flex w-100" style="place-items:center;">
		
		<img src="'.base_url($fcow->photo_sm).'" class="rounded-circle mr-20" width="70"/> <span style="display:block">'.$fcow->name.'<br>'.(($prog[0]->user_id==$fcow->id)?'Owner':'Instructor').'</span><button class="close">&times</button>
		</div>';
		if($if_photo==0 || $if_about_me ==0){
			echo '<p class="bg-warning p-3">'.(($if_photo==0)? 'The profile image is required.<br>': '').(($if_about_me ==0)? 'The biography must have at least 50 words.' : '').'</p>';
		}
	echo '</div>';
}
echo '</div>';*/
?>
<div class="row">
	<?php
		$i=1;
		foreach($facMem as $fcow){
			$if_photo = (trim($fcow->photo_sm) == "")? 0:1;
			$if_about_me = (trim($fcow->about_me) == "")? 0:1;	
	?>
	<div class="col-sm-6 prof_<?php echo $i; ?>">
		<div class="instructor-list">
			<?php if($prog[0]->user_id!=$fcow->id){ ?>
			<button type="button" class="close pull-right" data-dismiss="prof_<?php echo $i; ?>" onClick="removeInstructor(<?php echo $i; ?>, <?php echo $fcow->id; ?>, <?php echo $prog[0]->id; ?>)" aria-label="Close">
			  <i class="material-icons">close</i>
			</button>
			<?php } ?>
			<span>
			<?php
				echo '<img src="'.base_url($fcow->photo_sm).'" class="rounded-circle mr-2 pull-left" width="70"/>
				'.$fcow->name.'<br>'.(($prog[0]->user_id==$fcow->id)?'Owner':'Instructor');
				?>
			</span>
		</div>
		<?php if($if_photo==0 || $if_about_me ==0){ ?>
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			  <i class="material-icons">close</i>
			</button>
			<span>
				<?php
					echo (($if_photo==0)? '<b> <i class="material-icons">warning</i> - </b> The profile image is required.<br>': '').(($if_about_me ==0)? '<b> <i class="material-icons">warning</i> - </b> The biography must have at least 50 words.' : '')
				?>
			</span>
		  </div>
		<?php } ?>
	</div>
	<?php $i++; } ?>
</div>
<!--===================================================-->
<script>
	$(document).ready(function() {
		
		$('#frmInvite').validate({
			errorPlacement: function(error, element) {
			  $(element).closest('.form-group').append(error);
			},
			submitHandler: function(form, e) {
				$('#loading').css('display', 'block');
				e.preventDefault();
				var frmInviteData = new FormData($('#frmInvite')[0]);
				$.ajax({
					url: baseURL+'Teacher/inviteENEUser',
					type: 'POST',
					data: frmInviteData,
					cache : false,
					processData: false,
					contentType: false,
					enctype: 'multipart/form-data',
					async: false,
					success: (res)=>{ 
						$('#noticeM').modal('hide');
						$('#frmInvite')[0].reset();
						$('#loading').css('display', 'none');
						var obj = JSON.parse(res);
						swal(
						  'Invitation',
						  obj.msg,
						  obj.status
						).then(result=>{
							getProgramNotices();
						});
					},
					error: (errors)=>{
						console.log(errors);
					}
				});
			}
		});
	});
	
	function getInviteModal(prog_id)
	{
		$('#frmInvite')[0].reset();
		var uList = '';
		//$('#noticeM').modal('show');
		$('#loading').css('display', 'block');
		$.ajax({
			url:baseURL+'Teacher/getUserList',
			type: 'GET',
			data: { type: 'Teacher', prog: prog_id },
			success: (resp)=>{
				//console.log(res);
				var obj = JSON.parse(resp);
				if(obj!=null){
					$.each(obj, (i, val)=>{
						uList+='<option value="'+(val['email']).trim()+'" id="'+(val['first_name']).trim()+'+'+(val['last_name']).trim()+'+'+(val['phone']).trim()+'+1">';
					});
				}
				$('#loading').css('display', 'none');
				$('#inv_users').html(uList);
				$('#noticeM').modal('show');
			},
			error: (errors)=>{
				console.log(errors);
			}
		});
	}
	
	$('#inv_email').on('input', function() {
		var userText = $(this).val();

		$("#inv_users").find("option").each(function() {
		  if ($(this).val() == userText) {
			  var str = $(this).attr('id').split('+');
			  $('#inv_fname').val(str[0]);
			  $('#inv_lname').val(str[1]);
			  $('#inv_phone').val(str[2]);
			  $('#inv_exist').val(str[3]);
		  }
		})
	});
	
	function removeInstructor(id, user_id, prog_id)
	{
		$('#prof_'+i).hide();
		$.ajax({
			url: baseURL+'Teacher/removeProfessor',
			type: 'POST',
			data: { user_id: user_id, prog_id:prog_id },
			success: (respond)=>{
				if(respond){
					$('#prof_'+i).remove();
					$.notify({icon:"add_alert",message:"The instructor has been removed from this program."},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})
				}else{
					$('#prof_'+i).show();
					$.notify({icon:"add_alert",message:"Deletion error. Please try again."},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})
				}
			}
		});
	}
</script>