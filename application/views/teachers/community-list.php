<div class="content">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header card-header-primary card-header-text">
					  <div class="card-text">
						<h4 class="card-title">My Communities</h4>
					  </div>
					  <a href="<?= base_url('Teacher/addCommunity'); ?>" class="btn btn-primary btn-sm pull-right">Add Community</a>
					</div>
					<div class="card-body">
						<?php
							if($this->session->flashdata('errors')!=NULL){
								echo '<div class="alert alert-warning">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										  <i class="material-icons">close</i>
										</button>
										<span>
										  <b> Warning - </b> '.$this->session->flashdata('error').'</span>
									  </div>';
							}
							if($this->session->flashdata('success')!=NULL){
								echo '<div class="alert alert-success">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										  <i class="material-icons">close</i>
										</button>
										<span>
										  <b> Success - </b> '.$this->session->flashdata('success').'</span>
									  </div>';
							}
						?>
						<div class="clearfix"></div>
						<div class="row">
							<div class="col-md-12">
								<div class="material-datatables">
									<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
									  <thead>
										<tr>
										  <th width="5%">Sl</th>
										  <th width="40%">Name</th>
										  <th width="15%">Category</th>
										  <th width="30%">Details</th>
										  <th width="15%">Action</th>
										</tr>
									  </thead>
									  <tbody>
										<?php $i=1; foreach($community as $mrow){ ?>
										<tr id="comm_<?= $mrow->id; ?>">
										  <td><?php echo $i; ?></td>
										  <td><?php echo $mrow->title; ?></td>
										  <td><?= trim($mrow->name); ?></td>
										  <td><?= trim($mrow->desc); ?></td>
										  <td>
										  	<a href="<?= base_url('Teacher/addCommunity/?id='.$mrow->id); ?>" class="btn btn-primary btn-sm">Edit</a>
										  	<a href="javascript:removeCommunity(<?= $mrow->id; ?>);" class="btn btn-primary btn-sm">Delete</a>
										  </td>
										</tr>
										<?php $i++; } ?>
									  </tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url(); ?>assets/js/plugins/jquery.dataTables.min.js"></script>
<script>
	//$('#datatables').DataTable();
	
	var tabreq = $('#datatables').DataTable();

	function removeCommunity(comm_id)
	{
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You want to remove community",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
		  if (result.value) {
			$('#loading').show();
				$.ajax({
					url:baseURL+'Teacher/deleteCommunity?clid='+comm_id,
					type: 'GET',
					success: (res)=>{
						$('#loading').hide();
						$('#comm_'+comm_id).remove();
						if(res){
							$.notify({
								icon:"add_alert",
								message: "The Community has been deleted"},
								{type:"success",
								timer:3e3,
								placement:{from:'top',align:'right'}
							})
						}else{
							$.notify({
								icon:"add_alert",
								message: "The Community deletion error"},
								{type:"danger",
								timer:3e3,
								placement:{from:'top',align:'right'}
							})
						}
					},
					error: (errors)=>{
						console.log(errors);
					}
				});
		  }
		});
	}
</script>