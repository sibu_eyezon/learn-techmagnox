<aside class="col-md-2 justify-contect-center">
				<button type="button" class="btn btn-md btn-primary btn-link" data-toggle="modal" data-target="#liveClass"><span class="material-icons imp_icons">cast_for_education</span><br> Live Class</button>
				<a href="<?php echo base_url('Calendar/scheduleClass'); ?>" class="btn btn-md btn-warning btn-link"><span class="material-icons imp_icons">event_note</span><br> Schedule Class</a>
			</aside>
			<div class="col-md-10">
				<div class="card bg-light">
					<div class="card-header card-header-info card-header-text">
					  <div class="card-text">
						<h4 class="card-title"><?php echo trim($prog[0]->title); ?></h4>
					  </div>
					  <button type="button" class="btn btn-danger pull-right">Stop</button>
					</div>
					<div class="card-body">
						
						<div class="row mb-3">
							<div class="col-md-12">
								<ul class="menu-list">
									<li class="menu-list-item"><a href="javascript:;">Web</a></li>
									<li class="menu-list-item"><a href="javascript:;">Edit Content</a></li>
									<li class="menu-list-item"><a href="javascript:;">Settings</a></li>
									<li class="menu-list-item"><a href="javascript:;">Modules (<?php echo $pcounters[0]['cmodule']; ?>)</a></li>
									<li class="menu-list-item"><a href="http://localhost/Learning/Teacher/studAdmission/pre">Applied (<?php echo $pcounters[0]['capplied']; ?>)</a></li>
									<li class="menu-list-item"><a href="http://localhost/Learning/Teacher/studAdmission/final">Students (<?php echo $pcounters[0]['cstudent']; ?>)</a></li>
									<li class="menu-list-item"><a href="<?php echo base_url().'Teacher/progTeachers/?id='.base64_encode($prog_id); ?>">Instructors (<?php echo $pcounters[0]['cinstuctor']; ?>)</a></li>
									<li class="menu-list-item"><a href="<?php echo base_url().'Teacher/progOrgStrm/?id='.base64_encode($prog_id); ?>">Organizaion (<?php echo $pcounters[0]['corg']; ?>)</a></li>
									<li class="menu-list-item"><a href="javascript:;">Review (0)</a></li>
								</ul>
							</div>
						</div>

						<div class="row">
							<div class="col-md-9">
								<div class="card shadow">
									<div class="card-header">
										<h4 class="card-title font-weight-bold text-dark">Configuration</h4>
									</div>
									<div class="card-body">
										<div class="row">
											<div class="col-sm-3 txt_format">
											<?php echo trim($prog[0]->category).' ('.trim($prog[0]->ptype).')'; ?>
											</div>
											<div class="col-sm-3 txt_format">
											<?php echo 'Admission start: '.date('jS M Y',strtotime($prog[0]->astart_date)); ?>
											</div>
											<div class="col-sm-3 txt_format">
											<?php echo 'Date range: '.date('jS M Y',strtotime($prog[0]->start_date)).' - '.date('jS M Y',strtotime($prog[0]->end_date)); ?>
											</div>
											<div class="col-sm-3 txt_format">
											<?php echo 'No. of seats: '.trim($prog[0]->total_seat); ?>
											</div>
											<div class="col-sm-3 txt_format">
											<?php echo (trim($prog[0]->feetype)=='Paid')? 'Rs. '.$prog[0]->total_fee.' / Discount: '.$prog[0]->discount.' %' : 'Free'; ?>
											</div>
											<div class="col-sm-3 txt_format">
											<?php echo 'Approval type: '.(($prog[0]->apply_type=='0')? 'All approval':'Selective approval'); ?>
											</div>
											<div class="col-sm-3 txt_format">
											<?php echo 'Admission Deadline: '.date('jS M Y',strtotime($prog[0]->aend_date)); ?>
											</div>
											<div class="col-sm-3 txt_format">
											<?php echo 'Total Hours: '.trim($prog[0]->prog_hrs).' Hrs'; ?>
											</div>
										</div>
									</div>
								</div>
								<div class="card">
									<div class="card-header">
										<h4 class="card-title font-weight-bold text-dark">Modules
										<a href="<?php echo base_url().'Teacher/addCourse/?prog='.base64_encode($prog_id); ?>" class="btn btn-primary btn-sm pull-right"><i class="material-icons">add</i> Add Module</a>
										</h4>
									</div>
									<div class="card-body">
										<?php
											$section = $this->AutoProgramModel->get_section($prog_id);
											$i=1;
											foreach($section as $crow){
												$cid = $crow->id;
										?>
										<div class="card bg-info text-white mt-0" id="sec_<?php echo $cid; ?>">
											<div class="card-body">
												<h4 class="card-title">Module <?php echo $i; ?>: <?php echo $crow->title; ?>
													<a href="javascript:;" onCLick="deleteCourse(<?php echo $cid; ?>)" class="btn btn-outline-primary btn-rounded btn-sm ml-1 pull-right"><i class="fa fa-trash"></i></a>
													<a href="<?php echo base_url('Teacher/addCourse/?prog='.base64_encode($prog_id).'&cid='.base64_encode($cid)); ?>" class="btn btn-outline-primary btn-rounded btn-sm ml-1 pull-right"><i class="fa fa-edit"></i></a> 
												</h4>
												<?php
													$totSubMods = $this->Course_model->getTotalLecResAsgn($cid);
													echo 'Lectures: '.$totSubMods[0]['tot_lec'].';	Resources: '.$totSubMods[0]['tot_res'].';	Assignments: '.$totSubMods[0]['tot_asgn'];
												?>
											</div>
										</div>
										<?php } ?>
									</div>
								</div>
							</div>
							<aside class="col-md-3">
								<div class="card">
									<div class="card-header card-header-info">
										<h4 class="card-title">Notices
										</h4>
									</div>
									<div class="card-body">
										<div class="notice_ticker" style="height:30vh;">
											<ul class="list-group w-100" id="notc_details">
											
											</ul>
										</div>
									</div>
									<div class="card-footer">
										<a href="<?php echo base_url('Teacher/notices'); ?>" class="btn btn-info btn-sm pull-right">Know More</a>
									</div>
								</div>
							</aside>
						</div>
						
					</div>
				</div>
			
			</div>