<?php
	$ftype = trim($prog[0]->feetype);
?>
<div class="card">
	<div class="card-header card-header-info card-header-text">
		<div class="card-text">
			<h4 class="card-title">User List under: Workshop</h4>
		</div>
	</div>
	<div class="card-body">
		<form id="frmFtApprove">
			<div class="material-datatables">
				<table class="table table-striped table-no-bordered table-hover datatables" id="tbl_adm" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th width="5%">#</th>
							<th width="15%">Name</th>
							<th width="10%">Email</th>
							<th width="10%">Phone</th>
							<th width="25%">Address</th>
							<th width="10%">Country</th>
							<th width="15%">Organization</th>
							<th width="10%">Category</th>
						</tr>
					</thead>
					<tbody>
						<?php 
							$i=1;
							foreach($reg_users as $afow){
								$aid = $afow->sl;
								echo '<tr>';
								echo '<td>'.$i.'</td>';
								echo '<td>'.trim($afow->first_name." ".$afow->last_name).'</td>';
								echo '<td>'.trim($afow->email).'</td><td>'.trim($afow->phone).'</td>';
								echo '<td>'.trim($afow->address).'</td>';
								echo '<td>'.trim($afow->country).'</td>';
								echo '<td>'.trim($afow->organization).'</td>';
								echo '<td>';
								$af = trim($afow->category);
								if($af=='ST'){
									echo 'Student';
								}else if($af=='FY'){
									echo 'Faculty';
								}else if($af=='IF'){
									echo 'Industry Professionals';
								}
								echo '</td></tr>';
								$i++;
							}
						?>
					</tbody>
				</table>
			</div>
		</form>
	</div>
</div>

<script>
	$('#tbl_adm').DataTable({
		"ordering": true,
		"info": true,
		"paging": true,
		"lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
        "lengthChange": true,
		dom: '1B<"top"lf>rt<"bottom"ip><"clear">',
		buttons: [
			{
				extend: 'excelHtml5',
				title: 'Admission List of <?php echo trim($prog[0]->title).' - '.trim($prog[0]->yearnm); ?>',
				text:'Excel',
				className: 'bg-success text-white mr-2',
				exportOptions: {
					columns: [0, 1, 2, 3, 4, 5]
				}
			},
			{
				extend: 'pdfHtml5',
				title: 'Admission List of <?php echo trim($prog[0]->title).' - '.trim($prog[0]->yearnm); ?>',
				text: 'PDF',
				className: 'bg-success text-white mr-2',
				exportOptions: {
					columns: [0, 1, 2, 3, 4, 5]
				}
			},
			{
				extend: 'print',
				title: 'Admission List of <?php echo trim($prog[0]->title).' - '.trim($prog[0]->yearnm); ?>',
				text: 'Print',
				className: 'bg-success text-white mr-2',
				exportOptions: {
					columns: [0, 1, 2, 3, 4, 5]
				}
			}
		]
	});
	$('[name="tbl_adm_length"]').addClass('browser-default');
	/**********************APPROVE LIST**********************/
	function selectFATuples()
	{
		var checked = $('#sfa_all').is(':checked');
		if ($('.chk_user2').length > 0){
			if(checked){
			   $('.chk_user2').each(function() {
				  $('.chk_user2').prop('checked',true);
			   });
			   $('#fla').removeAttr('disabled');
			   $('#raf').removeAttr('disabled');
			 }else{
			   $('.chk_user2').each(function() {
				 $('.chk_user2').prop('checked',false);
			   });
			   $('#fla').attr('disabled', true);
			   $('#raf').attr('disabled', true);
			 } 
		}
	}
	$('.chk_user2').on('change', ()=>{
		if($('.chk_user2').is(':checked')){
			$('#fla').removeAttr('disabled');
			$('#raf').removeAttr('disabled');
		}else{
			$('#fla').attr('disabled', true);
			$('#raf').attr('disabled', true);
		}
	});
	
	$('#fla').on('click', (e)=>{
		e.preventDefault();
		var counter = parseInt(<?php echo $i; ?>);
		var i=1;
		var j=0;
		for(i=1; i<=counter; i++){
			if($('#acp2_'+i).is(':checked')){
			  j++;
		  } 
		}
	   if(j!=0){
		   var frmData = new FormData($('#frmFtApprove')[0]);
		   swal({
					title: 'Are you sure?',
					text: "You want to change status to Final Approval for selected student(s)",
					type: 'warning',
					showCancelButton: true,
					confirmButtonClass: 'btn btn-success',
					cancelButtonClass: 'btn btn-danger',
					confirmButtonText: 'Yes, Approve it!',
					buttonsStyling: false
				}).then(function(result) {
					if(result.value) {
						$('#prg_progress').html('Updating the student list.');
						$('#prg_progress').width(0);
						$('#loading').show();
						$.ajax({
							xhr: function () {
								var xhr = new window.XMLHttpRequest();
								xhr.upload.addEventListener("progress", function (evt) {
									if (evt.lengthComputable) {
										var percentComplete = evt.loaded / evt.total;
										console.log(percentComplete);
										$('#prg_progress').css({
											width: percentComplete * 100 + '%'
										});
										if (percentComplete === 1) {
											$('#prg_progress').hide();
										}
									}
								}, false);
								xhr.addEventListener("progress", function (evt) {
									if (evt.lengthComputable) {
										var percentComplete = evt.loaded / evt.total;
										console.log(percentComplete);
										$('#prg_progress').css({
											width: percentComplete * 100 + '%'
										});
									}
								}, false);
								return xhr;
							},
							url: baseURL+'Teacher/finalSelectedStud',
							type: 'POST',
							data: frmData,
							processData: false,
							contentType: false,
							success: (res)=>{
								$('#loading').hide();
								$('#prg_progress').hide();
								$('#prg_progress').html('Loading the list. Please wait...');
								console.log(res)
								if(res=='0')
								{
									$.notify({icon:"add_alert",message:'Something went worng.'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})
								}else if(res=='1'){
									getAdmissionList(<?php echo $prog[0]->id; ?>, '<?php echo $atype; ?>');
									$.notify({icon:"add_alert",message:'The '+j+' student(s) has been selected for final approval.'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})
								}else if(res=='2'){
									$.notify({icon:"add_alert",message:'Courses are not added/not linked with semesters'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})
								}
							}
						})
					}
				});
	   }else{
		   $.notify({icon:"add_alert",message:'No student has been selected'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})
	   }
	})
	$('#raf').on('click', ()=>{
		e.preventDefault();
		var cb = [];
		var i=1;
		var j=0;
		$('.chk_user2').each(function() {
		  if($('#acp2_'+i).is(':checked')){
			  j++;
		  } 
		  i++;
	   });
	   var frmData = new FormData($('#frmFtApprove')[0]);
		swal({
			title: 'Are you sure?',
			text: "You want to reject selected student(s)",
			type: 'warning',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: 'Yes, Approve it!',
			buttonsStyling: false
		}).then(function(result) {
			if(result.value) {
				$('#loading').show();
				$.ajax({
					url: baseURL+'Teacher/rejectApprovedSelectedStud',
					type: 'POST',
					data: frmData,
					processData: false,
					contentType: false,
					success: (res)=>{
						$('#loading').hide();
						//console.log(res)
						if(res)
						{
							getAdmissionList(<?php echo $prog[0]->id; ?>, '<?php echo $atype; ?>');
							$.notify({icon:"add_alert",message:'The '+j+' student(s) has been rejected.'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})
						}else{
							$.notify({icon:"add_alert",message:'Something went worng.'},{type:'warning',timer:3e3,placement:{from:'top',align:'right'}})
						}
					}
				})
			}
		});
	});
	/********************************************************/
</script>