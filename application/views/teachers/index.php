<style>
#course_code::placeholder { color: white; } .dropdown bootstrap-select { width:100% !important; } .loader { background-color: #ffffff; opacity:0.5; position: fixed; z-index: 999999; height: 100%; width: 100%; top: 0; left: 0; } .loader img { position: absolute; top: 50%; left: 50%; text-align: center; -webkit-transform: translate(-50%, -50%); transform: translate(-50%, -50%); } .btn .imp_icons{ font-size: 2rem; padding: 2rem 2rem; } .card-prog .card-description { min-height: 20vh; color: #000 !important; } .card-prog .card-title { color: #000 !important; } .card-prog .card-footer { border-top: 1px solid #eee !important; } .prog_menu { top: -125px !important; } .prog-wrapper { display: flex; text-decoration: none; transition: all 0.4s; } #prog-sidebar { min-width: 200px; max-width: 200px; background: #eee; color: #fff; transition: all 0.4s; max-height: 100vh; overflow-y:scroll; -ms-overflow-style: none; /* IE and Edge */ scrollbar-width: none; /* Firefox */ } #prog-sidebar::-webkit-scrollbar { display: none; } #prog-sidebar.active { min-width: 100px; max-width: 100px; } #prog-sidebar.active>.prog_components li a { display: grid !important; place-items: center !important; } #prog-sidebar.active>.prog_components li a span { display:none; } .prog_components li a span { font-size: 0.8rem; } #prog-sidebar.active>.prog_components .icon-logo { width: 55px !important; height: 55px !important; } #prog-sidebar .sidebar-header { padding: 20px; background: #1b1d24; } #prog-sidebar ul.components { padding: 20px 0; } #prog-sidebar ul p { color: #fff; padding: 10px; } #prog-sidebar ul li a { font-size: 1.1em; display: inline-block; align-items: center; background: transparent; color: #000; font-weight: 500; font-size: 1rem; text-transform: uppercase; } a[data-toggle="collapse"] { position: relative; } .prog_components li a { font-size: 0.9em !important; text-align: left; background: #1b1d24; } #prog-content { width: 100%; min-width: height 100vh; transition: all 0.4s; background-color: #fff; } #prog-content h2 { margin-bottom: 50px; } #prog-content p { text-align: justify; } @media (max-width: 768px) { #prog-sidebar { min-width: 0px !important; max-width: 0px !important; } #prog-sidebar.active { min-width: 100px !important; max-width: 100px !important; } #prog-sidebar .prog_components li a { display: grid !important; place-items: center !important; } #prog-sidebar .prog_components li a span { display:none; } .prog_components li a span { font-size: 0.8rem; } #prog-sidebar .prog_components .icon-logo { width: 55px !important; height: 55px !important; } } .icon-logo { font-size: 20px; color: #123456; padding: 12px 15px; font-family: FontAwesome; width: 55px; height: 55px; } @media (max-width: 768px) { .list-column { -webkit-columns: 2 !important; -moz-columns: 2 !important; columns: 2 !important; padding-left: 0; list-style: square; } }
</style>
<script>function shownotify(msg) { $.notify({icon:"add_alert",message:msg},{type:'warning',timer:3e3,placement:{from:'top',align:'right'}}) }</script>
<?php
	if($this->session->flashdata('error')!=""){
		echo '<script>shownotify(`'.$this->session->flashdata('error').'`);</script>';
	}
?>
<div class="loader" id="loading" style="display:none;">
	<img src="<?php echo base_url().'assets/img/loading.gif'; ?>">
</div>
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="prog-wrapper">
					<nav id="prog-sidebar">
						<ul class="list-unstyled prog_components">
							<li>
								<a href="javascript:;" data-toggle="modal" data-target="#liveClass" class="prog-link" title="Live Class">
									<img src="<?php echo base_url(); ?>assets/img/icons/online-class.png" class="icon-logo"/> 
									<span>Live Class</span>
								</a>
							</li>
							<li>
								<a href="<?php echo base_url('Teacher/notices'); ?>" class="prog-link" title="Notices">
									<img src="<?php echo base_url(); ?>assets/img/icons/noticeboard.png" class="icon-logo"/> 
									<span>Notices</span>
								</a>
							</li>
							<li>
								<a href="<?php echo base_url('Calendar/scheduleClass'); ?>" class="prog-link" title="Schedule Class">
									<img src="<?php echo base_url(); ?>assets/img/icons/schedule.png" class="icon-logo"/> 
									<span>Schedule Class</span>
								</a>
							</li>
							<li>
								<a href="<?php echo base_url('Lab/myLabs'); ?>" class="prog-link" title="Notices">
									<img src="<?php echo base_url(); ?>assets/img/icons/labs.png" class="icon-logo"/> 
									<span>Lab</span>
								</a>
							</li>
							<li>
								<a href="<?php echo base_url('Teacher/message'); ?>" class="prog-link" title="Schedule Class">
									<img src="<?php echo base_url(); ?>assets/img/icons/chat.png" class="icon-logo"/> 
									<span>Messages</span>
								</a>
							</li>
							<li>
								<a href="<?php echo base_url('Teacher/jobs'); ?>" class="prog-link" title="Jobs">
									<img src="<?php echo base_url(); ?>assets/img/icons/settings.png" class="icon-logo"/> 
									<span>Jobs</span>
								</a>
							</li>
						</ul>
					</nav>
					<div id="prog-content">
						<nav class="navbar navbar-expand-lg" style="background:transparent !important;">
							<div class="container-fluid">
								<h4 class="card-title w-100 d-flex">
									<a href="javascript:;" id="progSidebarCollapse" class="btn btn-sm btn-primary btn-link"><i class="fa fa-bars"></i></a> 
									<ul class="nav nav-pills nav-pills-info" role="tablist">
										<li class="nav-item">
										  <a class="nav-link active" data-toggle="tab" href="#link1" role="tablist">
											Active
										  </a>
										</li>
										<li class="nav-item">
										  <a class="nav-link" data-toggle="tab" href="#link2" role="tablist">
											Draft
										  </a>
										</li>
										<li class="nav-item">
										  <a class="nav-link" data-toggle="tab" href="#link3" role="tablist">
											Pending
										  </a>
										</li>
										<li class="nav-item">
										  <a class="nav-link" data-toggle="tab" href="#link4" role="tablist">
											Completed
										  </a>
										</li>
									</ul>
								</h4>
							</div>
						</nav>
						<div class="card mt-0" style="border:none !important; box-shadow: none !important;">
							<div class="card-body">
								<div class="tab-content tab-space">
									
									<div class="tab-pane active" id="link1">
										<div class="progress-bar progress-bar-striped progress-bar-animated" id="prg_progress" style="width: 100%; display:none;">Loading the list. Please wait...</div>
										<div class="row" id="active_program">
											
										</div>
									</div>
									
									<div class="tab-pane" id="link2">
										<div class="row" id="draft_program">
										
										</div>
									</div>
									
									<div class="tab-pane" id="link3">
										<div class="row" id="pending_program">
											
										  </div>
									</div>
									
									<div class="tab-pane" id="link4">
										<div class="row" id="archive_program">
											
										  </div>
									</div>
									
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="liveClass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			  <h4 class="modal-title">Start Live Class</h4>
			  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				<i class="material-icons">clear</i>
			  </button>
			</div>
			<form id="frmLive">
			<div class="modal-body">
				<div class="form-group">
					<select name="sch_prog" id="sch_prog" onChange="getActiveProgCourses(this.value);" class="custom-select" required="true">
						<option value="">Select an program*</option>
						<?php 
							foreach($programs as $row){ 
								if(trim($row->status)=='approved'){
									echo '<option value="'.$row->id.'">'.$row->title.' ('.$row->code.')</option>';
								}
							}
						?>
					</select>
				  </div>
				  <div class="form-group">
					<select name="course_code" id="course_code" class="custom-select" required="true" data-size="6">
						
					</select>
				  </div>
			</div>
			<div class="modal-footer">
				<input type="reset" style="visibility:hidden">
				<button type="submit" class="btn btn-success btn-sm mr-2">Start Live Class</button>
				<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
			</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="noticeM" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			  <h4 class="modal-title">Add Notice</h4>
			  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				<i class="material-icons">clear</i>
			  </button>
			</div>
			<form id="frmNotice" enctype="multipart/form-data">
				<div class="modal-body">
				  <div class="form-group mb-0">
					<label for="nottitle" class="text-dark">Title</label>
					<input type="text" class="form-control" name="nottitle" id="nottitle" required="true">
					<input type="hidden" name="pn_id" id="pn_id" value="0">
				  </div>	
				  <div class="form-group mb-0">
					<select name="not_prog" id="not_prog" onChange="getProgCourses(this.value, 'not_');" class="custom-select" required="true">
						<option value="">Select an program*</option>
						<?php 
							foreach($programs as $row){ 
								echo '<option value="'.$row->id.'">'.$row->title.' ('.$row->code.')</option>';
							}
						?>
					</select>
				  </div>
				  <div class="form-group mb-0">
					<select name="not_course" id="not_course" class="custom-select" required="true">

					</select>
				  </div>
				  <div class="custom-file">
					<input type="file" class="custom-file-input" name="fl_notice" id="fl_notice" accept="application/pdf, .doc, .docx">
					<label class="custom-file-label text-dark" for="fl_notice">Upload your file</label>
				  </div>
				  <div class="form-group mt-5">
					<label for="notdetails" class="text-dark">Details</label><br>
					<textarea class="form-control w-100" name="notdetails" id="notdetails"></textarea>
				  </div>
				</div>
				<div class="modal-footer">
				  <input type="reset" style="visibility:hidden">
				  <button type="submit" class="btn btn-success btn-sm mr-2">Save</button>
				  
				  <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="schedule" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			  <h4 class="modal-title">Schedule Class</h4>
			  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				<i class="material-icons">clear</i>
			  </button>
			</div>
			<form id="frmSchedule">
			<div class="modal-body">
				<div class="form-group">
					<label for="schtitle" class="text-dark">Title</label>
					<input type="text" class="form-control" name="schtitle" id="schtitle" required="true">
				</div>
				<div class="form-group">
					<select name="sch_prog" id="sch_prog" onChange="getProgCourses(this.value, 'sch_');" class="selectpicker" data-style="select-with-transition" title="Select an program*" required="true">
						<option value="Select an program*"></option>
						<?php 
							foreach($programs as $row){ 
								echo '<option value="'.$row->id.'">'.$row->title.' ('.$row->code.')</option>';
							}
						?>
					</select>
				  </div>
				  <div class="form-group">
					<select name="sch_course" id="sch_course" class="selectpicker" data-style="select-with-transition" title="Select an course*" required="true">
						
					</select>
				  </div>
				  <div class="form-group">
					<label for="sch_start" class="text-dark">Start Date Time</label>
					<input type="datetime-local" class="form-control" name="sch_start" id="sch_start" required="true">
				  </div>
				  <div class="form-group">
					<label for="sch_end" class="text-dark">End Date Time</label>
					<input type="datetime-local" class="form-control" name="sch_end" id="sch_end" required="true">
				  </div>
				  <div class="form group">
						<div class="togglebutton">
							<label class="text-dark">
							  Offline
							  <input type="checkbox" name="schLine" id="schLine" checked="true" value="1">
							  <span class="toggle"></span>
							  Online
							</label>
						  </div>
				  </div>
				  <div class="form-group">
					<label for="sch_online" class="text-dark">Online Link</label><br>
					<textarea class="form-control" name="sch_online" id="sch_online"></textarea>
				  </div>
				  <div class="form group">
						<div class="togglebutton">
							<label class="text-dark">
							  Notify?
							  <input type="checkbox" name="schNotify" id="schNotify" checked="true" value="1">
							  <span class="toggle"></span>
							</label>
						  </div>
				  </div>
			</div>
			<div class="modal-footer">
				<input type="reset" style="visibility:hidden">
				<button type="submit" class="btn btn-link">Save</button>
				<button type="button" class="btn btn-danger btn-link" data-dismiss="modal">Close</button>
			</div>
			</form>
		</div>
	</div>
</div>
<script src="<?php echo base_url('assets/js/home.js'); ?>"></script>
<script>
$(document).ready(function() {
	$("#progSidebarCollapse").on('click',function() {
		$("#prog-sidebar").toggleClass('active');
	  });
});
</script>