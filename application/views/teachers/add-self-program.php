<?php
	$submit = ($prog[0]->id=='0')? 'Add' : 'Update';
?>
<style>
.form-error{
	color:red;
}
</style>
<div class="content">
	<div class="container-fluid">
		<form action="<?= base_url('Teacher/cuProgram/'.$progtype); ?>" enctype="multipart/form-data" id="frmProgram" method="POST" style="width: 100%;">
			<div class="row">
				
				<div class="col-lg-9 mx-auto">
					<div class="card">
						<div class="card-header card-header-primary card-header-icon">
						  <div class="card-icon">
							<i class="material-icons">create</i>
						  </div>
						  <h4 class="card-title"><?php echo $submit; ?> Program <small class="text-danger">*'s are important</small></h4>
						</div>
						<div class="card-body">
							
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<select name="prog_subcategory" id="prog_subcategory" class="selectpicker" data-title="Select Category <span class=`text-danger`>*</span>" data-style="select-with-transition" data-size="7" required="true">
											<?php
												if(!empty($pcategory)){
													foreach($pcategory as $pcat){
														$cat_id = $pcat->id;
														echo '<option value="'.$cat_id.'" disabled>'.trim($pcat->name).'</option>';
														$subcat = $this->Admin_model->getParentCategories($cat_id);
														if(!empty($subcat)){
															foreach($subcat as $pscat){
																echo '<option value="'.$pscat->id.'" '.(($prog[0]->prog_subcategory==$pscat->id)? 'selected':'').'>'.trim($pscat->name).'</option>';
															}
														}
													}
												}else{
													echo '<option value="">No category added.</option>';
												}
											?>
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<select name="prog_level" id="prog_level" class="selectpicker" data-title="Select Level <span class=`text-danger`>*</span>" data-style="select-with-transition" required="true">
											<option value="1" <?php echo ($prog[0]->prog_level=='1')? 'selected':''; ?>>Live Buddy</option>
											<option value="2" <?php echo ($prog[0]->prog_level=='2')? 'selected':''; ?>>Mentorship</option>
											<option value="3" <?php echo ($prog[0]->prog_level=='3')? 'selected':''; ?>>Certification</option>
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<select class="selectpicker" data-style="select-with-transition" name="category" id="category" data-title="Select Type <span class=`text-danger`>*</span>">
											<option value="Training Program" <?= ((trim($prog[0]->category)=='Training Program')? 'selected':''); ?>>Training Program</option>
											<option value="Seminar Program" <?= ((trim($prog[0]->category)=='Seminar Program')? 'selected':''); ?>>Seminar Program</option>
											<option value="Webinar Program" <?= ((trim($prog[0]->category)=='Webinar Program')? 'selected':''); ?>>Webinar Program</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label for="title" class="text-dark">Program Title <span class="text-danger">*</span></label>
										<input type="text" class="form-control" id="title" name="title" placeholder="Max length: 100 characters" value="<?php echo trim($prog[0]->title); ?>">
										<input type="hidden" name="type" id="type" value="<?= $prog[0]->type; ?>"/>
									</div>
								</div>
							</div>
							<!--<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="title" class="text-dark">Program Code <span class="text-danger">*</span></label>
										<input type="text" class="form-control" id="pcode" name="pcode" value="<?php //echo (trim($prog[0]->code)=="")? 'Auto Generated': trim($prog[0]->code); ?>" readonly>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<?php
											//echo '';
											/*if($progtype=='selfbased'){
												echo 'Sub-Category: '.trim($prog[0]->category);
												echo '<input type="hidden" id="category" name="category" value="'.trim($prog[0]->category).'">';
											}else{
												
												/*<option value="Corporate Program" '.((trim($prog[0]->category)=='Corporate Program')? 'selected':'').'>Corporate Program</option>
													<option value="Tutor" '.((trim($prog[0]->category)=='Tutor')? 'selected':'').'>Tutor</option>
													<option value="Diploma Program" '.((trim($prog[0]->category)=='Diploma Program')? 'selected':'').'>Diploma Program</option>
											}*/
										?>
									</div>
								</div>
							</div>-->
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<?php
											if($progtype=='selfbased'){
												echo 'Program Type: Online';
												echo '<input type="hidden" id="prog_type" name="prog_type" value="'.trim($prog[0]->ptype).'">';
											}else{
												echo '<select class="selectpicker" data-style="select-with-transition" name="prog_type" id="prog_type" data-title="Select one type <span class=`text-danger`>*</span>">
													<option value="Online" '.((trim($prog[0]->ptype)=='Online')? 'selected':'').'>Online Program</option>
													<option value="Regular"'.((trim($prog[0]->ptype)=='Regular')? 'selected':'').'>Regular Program</option>
													<option value="Distance" '.((trim($prog[0]->ptype)=='Distance')? 'selected':'').'>Distance Program</option>
												</select>';
											}
										?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												 <label for="total_seat" class="text-dark">Total Seats (0 for infinite) <span class="text-danger">*</span></label>
												<input type="number" name="total_seat" id="total_seat" value="<?php echo $prog[0]->total_seat; ?>" class="form-control" required="true">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="total_hrs" class="text-dark">Total Hours <span class="text-danger">*</span></label>
												<input type="number" name="total_hrs" id="total_hrs" value="<?php echo $prog[0]->prog_hrs; ?>" class="form-control" required="true">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										 <div class="checkbox-radios">
											<div class="form-check form-check-inline">
											  <label class="form-check-label text-dark">
												<input class="form-check-input" type="radio" <?php echo ($prog[0]->feetype=='Free')? 'checked' : ''; ?> value="Free" name="feetype" id="free"> Free
												<span class="form-check-sign">
												  <span class="check"></span>
												</span>
											  </label>
											</div>
											<div class="form-check form-check-inline">
											  <label class="form-check-label text-dark">
												<input class="form-check-input" type="radio" <?php echo ($prog[0]->feetype=='Paid')? 'checked' : ''; ?> value="Paid" name="feetype" id="paid"> Paid
												<span class="form-check-sign">
												  <span class="check"></span>
												</span>
											  </label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												 <label for="fees" class="text-dark">Total Fees</label>
												<input type="text" name="fees" id="fees" class="form-control" value="<?php echo trim($prog[0]->total_fee); ?>" disabled>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="fdetails" class="text-dark">Discount (If any)</label>
												<input type="number" name="rebate" id="rebate" value="<?php echo $prog[0]->discount; ?>" class="form-control" disabled>
											</div>
										</div>
									</div>
									
								</div>
							</div>
							<?php if($progtype=='certificate'){ ?>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
									  <select class="selectpicker" data-style="select-with-transition" data-title="Select Academic Year" name="aca_year" id="aca_year">
										<?php
											if(!empty($ayr)){
												foreach($ayr as $ayow){
													echo '<option value="'.$ayow->sl.'" '.(($prog[0]->aca_year==$ayow->sl)? 'selected' : '').'>'.$ayow->yearnm.'</option>';
												}
											}
										?>
									  </select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										 <label for="duration">Duration <span class="text-danger">*</span></label>
										 <div class="input-group">
											<div class="input-group-prepend">
												<input type="text" name="duration" id="duration" class="form-control" value="<?php echo (int)trim($prog[0]->duration); ?>">
												<select class="custom-select custom-select-sm" name="dtype" id="dtype">
													<option value="">Duration <span class="text-danger">*</span></option>
													<option value="day" <?php echo (trim($prog[0]->dtype)=='day')? 'selected':''; ?>>Days</option>
													<option value="month" <?php echo (trim($prog[0]->dtype)=='month')? 'selected':''; ?>>Months</option>
												</select>
											</div>
										 </div>
									</div>
								</div>
							</div>
							<div class="row">	
								<div class="col-md-6">
									<div class="form-group">
										<label for="sdate">Start Date <span class="text-danger">*</span></label>
										<input type="datetime-local" name="sdate" id="sdate" value="<?php echo strftime('%Y-%m-%dT%H:%M:%S', strtotime($prog[0]->start_date)); ?>" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="edate">End Date <span class="text-danger">*</span></label>
										<input type="datetime-local" name="edate" id="edate" value="<?php echo strftime('%Y-%m-%dT%H:%M:%S', strtotime($prog[0]->end_date)); ?>" class="form-control">
									</div>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-md-12">
									<div class="form-group">
										 <label for="overview">Overview</label><br>
										<textarea name="overview" id="overview" class="form-control" cols="80" rows="5"><?php echo $prog[0]->overview; ?></textarea>
									</div>
								</div>
							</div>
							<?php } ?>
							<div class="justify-content-center text-center">
								<button type="submit" class="btn btn-primary btn-md pull-right"><?php echo $submit; ?></button>
							</div>
							
						</div>
					</div>
				</div>
				<?php if($progtype=='certificate'){ ?>
				<aside class="col-md-3">
					<div class="card">
						<div class="card-body">
							<h4 class="card-title">Contacts <small class="text-danger">*</small></h4>
							<div class="form-group">
								 <label for="email">Email <span class="text-danger">*</span></label>
								 <input type="email" name="email" id="email" class="form-control" value="<?php echo $_SESSION['userData']['email']; ?>">
							</div>
							<div class="form-group">
								 <label for="phone">Phone <span class="text-danger">*</span></label>
								 <input type="text" name="phone" id="phone" class="form-control" value="<?php echo $_SESSION['userData']['mobile']; ?>">
							</div>
						</div>
					</div>
					<div class="card" id="ad_details">
						<div class="card-body">
							<h4 class="card-title">Admission Info. <small class="text-danger">*</small></h4>
							<div class="form-group">
								<select class="selectpicker" data-style="select-with-transition" name="apply_type" id="apply_type" title="Single Select <span class='text-danger'>*</span>">
									<option value="0" <?php if($prog[0]->apply_type=='0'){ echo 'selected'; } ?>>All Approved</option>
									<option value="1" <?php if($prog[0]->apply_type=='1'){ echo 'selected'; } ?>>Selective Approved</option>
								</select>
							</div>
							<div class="form-group" id="stype" style="display:none;">
								<?php
									if(isset($prog[0]->screen_type)){
										$st = explode(',', $prog[0]->screen_type);
									}else{
										$st = array();
									}
								?>
								<select class="selectpicker" data-style="select-with-transition" title="Screen type <span class='text-danger'>*</span>" multiple name="screen_type[]" id="screen_type">
									<option value="0" <?php if(in_array(0,$st)){ echo 'selected'; } ?>>Manual Checking</option>
									<option value="1" <?php if(in_array(1,$st)){ echo 'selected'; } ?>>Online Exam</option>
									<option value="2" <?php if(in_array(2,$st)){ echo 'selected'; } ?>>Interview</option>
								</select>
							</div>
							<div class="form-group">
								<label for="fdetails">Start Date <span class="text-danger">*</span></label>
								<input type="date" name="adstart" id="adstart" value="<?php echo $prog[0]->astart_date; ?>" class="form-control">
							</div>
							<div class="form-group">
								<label for="fdetails">End Date <span class="text-danger">*</span></label>
								<input type="date" name="adend" id="adend" value="<?php echo $prog[0]->aend_date; ?>" class="form-control">
							</div>
							<div class="form-group">
								<label for="fdetails">Criteria <span class="text-danger">*</span></label>
								<textarea name="criteria" id="criteria" class="form-control"><?php echo $prog[0]->criteria; ?></textarea>
							</div>
						</div>
					</div>
				</aside>
				<?php } ?>
				
			</div>
		</form>
	</div>
</div>
<script src="<?php echo base_url().'assets/js/program.js'; ?>"></script>
<script src="<?php echo base_url(); ?>assets/vendor/ckeditor/ckeditor.js"></script><script>
	$(document).ready(function() {
		toggleFees('<?php echo trim($prog[0]->feetype); ?>');
		toggleScreenType('<?php echo trim($prog[0]->apply_type); ?>');
		CKEDITOR.replace('overview', {
			extraPlugins: 'uploadimage,colorbutton,colordialog,autoembed,embedsemantic,mathjax,codesnippet,font,justify,table,specialchar, tabletools, uicolor',
			height: 200,
			mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML'
		});
	});
	
	$('input[type=radio][name=feetype]').on('change', ()=>{
		var radioValue = $('input[type=radio][name=feetype]:checked').val();
		toggleFees(radioValue);
	});
	function toggleFees(rvalue)
	{
		if(rvalue!=''){
			if(rvalue=='Free')
			{
				$('#fees').attr('disabled', true);
				$('#rebate').attr('disabled', true);
			}else{
				$('#fees').removeAttr('disabled');
				$('#rebate').removeAttr('disabled');
			}
		}
	}
	$('#apply_type').on('change', ()=>{
		toggleScreenType($('#apply_type').val());
	});
	function toggleScreenType(at_id)
	{
		if(at_id==0){
			$('#stype').hide();
		}else{
			$('#stype').show();
		}
	}
</script>