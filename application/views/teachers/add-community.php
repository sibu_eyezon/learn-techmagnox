<?php
	$submit = ($comm[0]->id=='0')? 'Add' : 'Update';
?>
<style>
.form-error{
	color:red;
}
#image_prev {
	cursor: pointer;
}
</style>
<div class="content">
	<div class="container-fluid">
		<form action="<?= base_url('Teacher/cuCommunity'); ?>" enctype="multipart/form-data" id="frmCommunity" method="POST" style="width: 100%;">
			<div class="row">
				<div class="col-lg-9 mx-auto">
					<div class="card">
						<div class="card-header card-header-primary card-header-icon">
						  <div class="card-icon">
							<i class="material-icons">create</i>
						  </div>
						  <h4 class="card-title"><?php echo $submit; ?> Community <small class="text-danger">*'s are important</small></h4>
						</div>
						<div class="card-body">
							
							<div class="row justify-content-center">
								<div class="col-sm-4">
									<div class="fileinput fileinput-new text-center">
										<div class="fileinput-new thumbnail">
											<img src="<?php echo base_url().'assets/img/banner/'.$comm[0]->photo; ?>" onerror="this.src='<?php echo base_url(); ?>assets/img/image_placeholder.jpg'" class="picture-src" onClick="$('#avatar').click();" id="image_prev" name="image_prev" title="" />
										</div>
										<h6 class="description">Choose Image</h6>
									</div>
									<input type="file" class="custom-file-input" name="avatar" id="avatar" onChange="preview();" accept="image/jpg, image/jpeg, image/png" style="display:none;">
								</div>
							</div>
							<div class="row justify-content-center">
								<div class="col-sm-12">
									<div class="form-group">
									  <label for="title" class="text-dark">Community Title <span class="text-danger">*</span></label>
									  <input type="text" class="form-control" id="title" name="title" placeholder="Max length: 200 characters" value="<?= trim($comm[0]->title); ?>">
									  <input type="hidden" name="cid" id="cid" value="<?= $comm[0]->id; ?>"/>
									</div>
								</div>
							</div>
							<div class="row justify-content-center">
								<div class="col-sm-6">
									<div class="form-group">
									  	<select name="category_id" id="category_id" class="selectpicker" data-title="Select Category <span class='text-danger'>*</span>" data-style="select-with-transition" required="true" data-size="7">
									  		<?php 
									  			if(!empty($category)){
									  			foreach ($category as $crow) {
									  				if($crow->id == $comm[0]->category_id){
									  					echo '<option value="'.$crow->id.'" selected="selected">'.trim($crow->name).'</option>';
									  				}else{
									  					echo '<option value="'.$crow->id.'">'.trim($crow->name).'</option>';
									  				}
									  			}
									  			}else{
									  				echo '<option value="">No data found</option>';
									  			}
									  		?>
										</select>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
									  <select name="skills[]" id="skills" class="selectpicker" data-title="Select Multi Skills <span class='text-danger'>*</span>" data-style="select-with-transition" required="true" multiple data-size="7">
											<?php 
												$comm_skills = json_decode($comm[0]->skills);
									  			if(!empty($skills)){
									  			foreach ($skills as $srow) {
									  				if(in_array($srow->id, $comm_skills)){
									  					echo '<option value="'.$srow->id.'" selected="selected">'.trim($srow->name).'</option>';
									  				}else{
									  					echo '<option value="'.$srow->id.'">'.trim($srow->name).'</option>';
									  				}
									  			}
									  			}else{
									  				echo '<option value="">No data found</option>';
									  			}
									  		?>
										</select>
									</div>
								</div>
							</div>
							<div class="row justify-content-center">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="price" class="text-dark">Price <span class="text-danger">*</span></label>
									  	<input type="text" class="form-control" id="price" name="price" value="<?= trim($comm[0]->price); ?>">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<select name="type" id="type" class="selectpicker" data-title="Select Type" data-style="select-with-transition" required="true">
											<option value="1" <?php echo ($comm[0]->type=='1')? 'selected':''; ?>>Public</option>
											<option value="2" <?php echo ($comm[0]->type=='2')? 'selected':''; ?>>Private</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-md-12">
									<div class="form-group">
										 <label for="overview">Overview</label><br>
										<textarea name="desc" id="desc" class="form-control" cols="80" rows="5"><?php echo $comm[0]->desc; ?></textarea>
									</div>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-md-12">
									<div class="form-group text-center">
										<button type="submit" class="btn btn-primary btn-md"><?php echo $submit; ?></button>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
			
		</form>
	</div>
</div>
<script src="<?php echo base_url(); ?>assets/vendor/ckeditor/ckeditor.js"></script>
<script>
	function preview()
	{
		$("#image_prev").attr("src",URL.createObjectURL(event.target.files[0]));
	}
	CKEDITOR.replace('desc', {
		extraPlugins: 'uploadimage,colorbutton,colordialog,autoembed,embedsemantic,mathjax,codesnippet,font,justify,table,specialchar, tabletools, uicolor',
		height: 200,
		mathJaxLib: 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML'
	});
	$(function(){
		$('#frmCommunity').validate({
			errorPlacement: function(error, element) {
			  $(element).closest('.form-group').append(error);
			},
			rules: {
				title: {
					required: true,
					maxlength: 200
				},
				category_id: {
					required: true
				},
				skills: {
					required: true
				},
				price: {
					required: true,
					digits: true
				},
				type: {
					required: true
				}
			},
		});
	});

	
</script>