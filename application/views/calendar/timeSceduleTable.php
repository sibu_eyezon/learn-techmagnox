<table class="table table-bordered">
    <thead>
        <tr>
            <th scope="col"><input type="checkbox" id="allCheck" checked="checked" onChange="isAllChecked()"></th>
            <th scope="col">Date</th>
            <th scope="col">Day</th>
            <th scope="col">Start Time</th>
            <th scope="col">End Time</th>
        </tr>
    </thead>
    <tbody>  
        <?php
            if(!empty($dates)):
                for($i = 0 ; $i < count($dates) ; $i++):
        ?> 
                <tr>
                    <td>
                        <input type="checkbox" class="checkbox" name="selectSedule" id="selectSedule<?php echo $i;?>" value="<?php echo $i;?>" onChange="rowCheck(<?php echo $i;?>)">
                    </td>
                    <td>
                        <input type="date" class="form-control" name="classDate" id="classDate<?php echo $i;?>" value="<?php echo $dates[$i]; ?>" readonly>
                    </td>
                    <td>
                        <?php echo date('l', strtotime($dates[$i]));?>
                    </td>
                    <td>
                        <input type="time" class="form-control" name="startTime" id="startTime<?php echo $i;?>" disabled>
                    </td>
                    <td>
                        <input type="time" class="form-control" name="endTime" id="endTime<?php echo $i;?>" disabled>
                    </td>
            <?php
                if($i === 0):
            ?>
                    <td rowspan="<?php echo count($dates);?>">
                        <button class="btn btn-primary btn-sm" onClick="toCopyAll()">Copy All</button>
                    </td>
            <?php
                endif;
            ?>
                </tr>  
        <?php
                endfor;
            endif;
        ?> 
    </tbody>                                  
</table>

<script>
    $(document).ready(function(){
        isAllChecked();
    })

    function isAllChecked(){
        if($('#allCheck').is(":checked")){
            $('.checkbox').each(function(index){
                $('#selectSedule'+index).prop("checked", true);
                $('#startTime'+index).removeAttr('disabled');
                $('#endTime'+index).removeAttr('disabled');
            })
        }else{
            $('.checkbox').each(function(index){
                $('#selectSedule'+index).prop("checked", false);
                $('#startTime'+index).attr('disabled','disabled');
                $('#endTime'+index).attr('disabled','disabled');
            })
        }
    }

    function allCkeckBoxIsChecked(){
        var totalCheckboxes        = $('[class="checkbox"]').length;
        var totalCheckedCheckboxes = $('[class="checkbox"]:checked').length;
        if(totalCheckboxes === totalCheckedCheckboxes){
            $('#allCheck').prop("checked", true);
        }else{
            $('#allCheck').prop("checked", false);
        }
    }

    function rowCheck(i){
        if($("#selectSedule"+i).is(":checked")){
            $('#startTime'+i).removeAttr('disabled');
            $('#endTime'+i).removeAttr('disabled');
            allCkeckBoxIsChecked();
        }else{
            $('#startTime'+i).val('');
            $('#startTime'+i).attr('disabled','disabled');
            $('#endTime'+i).val('');
            $('#endTime'+i).attr('disabled','disabled');
            allCkeckBoxIsChecked();
        }
    }

    function copyToAllSchedule(index, startTime, endTime){
        var totalCheckboxes  = $('[class="checkbox"]').length;
        for(let i=(index + 1); i< totalCheckboxes; i++){
            if($('#selectSedule'+i).is(':checked')){
                $('#startTime'+i).val(startTime);
                $('#endTime'+i).val(endTime);
            }
        }
    }

    function toCopyAll(){
        var totalCheckboxes  = $('[class="checkbox"]').length; 
        for(let i=0; i< totalCheckboxes; i++){
            if($('#selectSedule'+i).is(':checked') && $('#startTime'+i).val() && $('#endTime'+i).val()){
                let index = i;
                let startTime = $('#startTime'+i).val();
                let endTime = $('#endTime'+i).val();
                copyToAllSchedule(index, startTime, endTime);
                break;
            }
        }
    }
</script>


