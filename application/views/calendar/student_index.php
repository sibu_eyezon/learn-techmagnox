<div class="content">
	<div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-info">
                        <h3 class="card-title">Class Schedule </h3>
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-around">...</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8">
                        <div id="calendar"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function showCalendar(){
        var calendar = $('#calendar').fullCalendar({
            timeZone: 'UTC',
            editable:true,
            header:{
                left:'prev,next today',
                center:'title',
                right:'month,agendaWeek,agendaDay'
            },
            events: baseURL + 'Calendar/studentClassSchedule',
            selectable:true,
            selectHelper:true,
            
        })
    }
    $(document).ready(function(){
        showCalendar();
    });
</script>