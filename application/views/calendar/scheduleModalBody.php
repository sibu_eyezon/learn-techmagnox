<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <label for="sec_program" class="text-dark">Program <span class="text-danger">*</span></label>
                <select class="form-control"  name="sec_program" id="sec_program" onChange="getCourseByProgram()">
                    <option value="">--- Select ---</option>
                    <?php 
                        foreach($program as $row){ 
                            echo '<option value="'.$row->id.'">'.$row->title.'</option>';
                        }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12" id="course_div" style="display: none">
            <div class="form-group">
                <label for="sec_course" class="text-dark">Course <span class="text-danger">*</span></label>
                <select class="form-control" name="sec_course" id="sec_course" onChange="onChangeCourseSelect()">
                </select>
            </div>
        </div>
    </div>
    <div class="row" id="schedule_div" style="display: none">
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <label for="start_date" class="text-dark">Start Date Time <span class="text-danger">*</span></label>
                <input type="date" class="form-control" name="start_date" id="start_date" onChange="getDateScheduler()">
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <label for="end_date" class="text-dark">End Date Time <span class="text-danger">*</span></label>
                <input type="date" class="form-control" name="end_date" id="end_date" onChange="getDateScheduler()">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="material-datatables" id="timeScheduleTable" style="display: none">
            </div>
        </div>
    </div>
</div>

<script>
    function courseRender(getData){
        let prog_id = $('#sec_program').val();
        $.ajax({
            url  : baseURL + 'Calendar/getCourseByProgram',
            type : 'POST',
            data : { prog : prog_id},
            success : function(data){
                $('#sec_course').html(data);
            }
        })
    }

    function onChangeCourseSelect(){
        let section_id = $('#sec_course').val();
        if(section_id !== ''){
            $('#schedule_div').show();
            $('#timeScheduleTable').html('');
            $('#scheduleModalFooter').html('');
        }else{
            $('#schedule_div').hide();
            $('#start_date').val('');
            $('#end_date').val('');
            $('#timeScheduleTable').html('');
            $('#scheduleModalFooter').html('');
        }
    }

    function getCourseByProgram(){
        let prog_id = $('#sec_program').val();
        if(prog_id !== ''){
            $.ajax({
                url  : baseURL + 'Calendar/getTotalCourseByProgram',
                type : 'POST',
                data : { prog : prog_id},
                success : function(data){
                    let getData = data;
                    if(getData === '0'){
                        $('#course_div').hide();
                        $('#schedule_div').show();
                        $('#start_date').val('');
                        $('#end_date').val('');
                        $('#timeScheduleTable').html('');
                        $('#scheduleModalFooter').html('');
                    }else{
                        $('#course_div').show();
                        courseRender();
                        $('#schedule_div').hide();
                        $('#start_date').val('');
                        $('#end_date').val('');
                        $('#timeScheduleTable').html('');
                        $('#scheduleModalFooter').html('');
                    }
                }
            })
        }else{
            $('#course_div').hide();
            $('#schedule_div').hide();
            $('#start_date').val('');
            $('#end_date').val('');
            $('#timeScheduleTable').html('');
            $('#scheduleModalFooter').html('');
        }

    }
</script>