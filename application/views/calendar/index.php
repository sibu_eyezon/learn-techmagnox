<div class="content">
	<div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-info">
                        <h3 class="card-title">Schedule Class</h3>
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-around">...</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8">
                            <div id="calendar"></div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="d-flex align-items-start flex-column p-5">
                            <!-- <button class="btn btn-default btn-md btn-block">Upload Class Routine</button> -->
                            <button class="btn btn-default btn-md btn-block" onClick="openScheduleModal()">Add Schedule</button>
                            <button class="btn btn-default btn-md btn-block" onClick="openScheduleModal()">Weekly Classes</button>
                            <!-- <button class="btn btn-default btn-md btn-block">View All Schedules</button> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- add schedule modal -->

    <div class="modal fade" id="scheduleModal" tabindex="-1" aria-labelledby="scheduleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width:850px">
                <div class="modal-header">
                    <h5 class="modal-title" id="scheduleModalLabel">Add Schedule</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-3" id="scheduleModalBody">
                </div>
                <div class="modal-footer" id="scheduleModalFooter">
                    
                </div>
            </div>
        </div>
    </div>

<!-- <script src="<?php echo base_url('assets/js/home.js'); ?>"></script> -->
<script>
    function showCalendar(){
        var calendar = $('#calendar').fullCalendar({
            timeZone: 'UTC',
            editable:true,
            header:{
                left:'prev,next today',
                center:'title',
                right:'month,agendaWeek,agendaDay'
            },
            events: baseURL + 'Calendar/clssTable',
            selectable:true,
            selectHelper:true,
            
        })
    }
    $(document).ready(function(){
        showCalendar();
    });

    function openScheduleModal(){
        $.ajax({
            url  : baseURL + 'Calendar/getScheduleModalBody',
            type : 'POST',
            success : function(data){
                $('#scheduleModal').modal('show');
                $('.selectpicker').selectpicker();
                $('#scheduleModalBody').html(data);
            }
        })
    }



    function scheduleFooterButton(){
        $.ajax({
            url : baseURL + 'Calendar/getAddScheduleFooterButton',
            type : 'POST',
            success : function(data){
                $('#scheduleModalFooter').html(data);
            }
        })
    }

    function getDateScheduler(){
        let startDate 
        let endDate ;

        if($('#start_date').val()){
            startDate = $('#start_date').val();
            $('#end_date').prop('min',function(){
                return startDate;
            })
        }

        if($('#end_date').val()){
            endDate   = $('#end_date').val();
            $('#start_date').prop('max',function(){
                return endDate;
            })
        }

        if(startDate && endDate){
            $.ajax({
                url : baseURL + 'Calendar/generateRangeOfDates',
                type : 'POST',
                data : {
                    startDate : startDate,
                    endDate : endDate
                },
                success : function(data){
                    $('#timeScheduleTable').show();
                    $('#timeScheduleTable').html(data);
                    scheduleFooterButton(); 
                }
            })
        }
    }

    function addNewSchedule(){
        let program_id = $('#sec_program').val();
        let program_text = $('#sec_program').find(":selected").text();
        let course_id ;
        let course_text;
        if($('#sec_course').val()){
            course_id = $('#sec_course').val();
            course_text = $('#sec_course').find(":selected").text();
        }
        let schedule_title = (course_id && course_text)? course_text : program_text;
        let row_id          = [];
        let start_date_time = [];
        let end_date_time   = [];

        $('input[name="selectSedule"]:checked').each(function () {
            row_id.push(this.value);
            if($('#startTime'+this.value).val()){
                let startTimeStamp = `${$('#classDate'+this.value).val()} ${$('#startTime'+this.value).val()}:00`;
                start_date_time.push(startTimeStamp);
            }
            if($('#endTime'+this.value).val()){
                let endTimeStamp = `${$('#classDate'+this.value).val()} ${$('#endTime'+this.value).val()}:00`;
                end_date_time.push(endTimeStamp);
            }
        })

        if(row_id.length){
            if((row_id.length === start_date_time.length) && (row_id.length === end_date_time.length)){
                $.ajax({
                    url  : baseURL + 'Calendar/addNewSchedule',
                    type : 'POST',
                    data : {
                        program_id : program_id,
                        course_id  : (course_id)? course_id : 0,
                        title      : schedule_title,
                        startTime  : start_date_time,
                        endTime    : end_date_time,
                        count      : row_id.length
                    },
                    success : function(data){
                        $('#scheduleModal').modal('hide');
                        $('#calendar').fullCalendar('refetchEvents');
                        showCalendar();
                    }
                })
            }else{
                swal.fire({
                    title: "Not allow",
                    text: "Any date is note selected",
                    icon: "warning",
                    dangerMode: true,
                })
            }
        }else{
            swal.fire({
                title: "Not allow",
                text: "Any date is note selected",
                icon: "warning",
                dangerMode: true,
            })
        }
        
    }
    
</script>