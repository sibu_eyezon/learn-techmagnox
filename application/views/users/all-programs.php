
<div class="content">
	<div class="container">
		<?php
			if($this->session->flashdata('error')!=NULL){
				echo '<div class="alert alert-warning">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						  <i class="material-icons">close</i>
						</button>
						<span>
						  <b> Warning - </b> '.$this->session->flashdata('error').'</span>
					  </div>';
			}
			if($this->session->flashdata('success')!=NULL){
				echo '<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						  <i class="material-icons">close</i>
						</button>
						<span>
						  <b> Success - </b> '.$this->session->flashdata('success').'</span>
					  </div>';
			}
		  ?>
		<div class="card">
			<div class="card-header card-header-primary card-header-text">
			  <div class="card-text">
				<h4 class="card-title">All Programs</h4>
			  </div>
			</div>
			<div class="card-body">
				<div class="clearfix"></div>
				<div class="row">
					
					<?php 
						$i=1; 
						foreach($programs as $prow){ 
						$ptype = (int)$prow->type; 
						$durl = 'viewProgramDetails/?id='.base64_encode($prow->id);
					?>
					<div class="col-md-4">
						<div class="card card-product">
						  <div class="card-header card-header-image">
							<a href="<?php echo $durl; ?>">
							  <img class="img" src="<?php echo base_url('assets/img/banner/'.$prow->banner); ?>" onerror="this.src='<?php echo base_url('assets/img/sample.jpg'); ?>'">
							</a>
						  </div>
						  <div class="card-body">
							<h4 class="card-title">
							  <a href="<?php echo $durl; ?>"><?php echo $prow->title; ?></a>
							</h4>
							<?php
								$fees = 0;$discount=0;$payable=0;
								$feetype = trim($prow->feetype);
								if($feetype=='Paid'){
									$fees = intval(trim($prow->total_fee));
									$discount = intval(trim($prow->discount));
									$payable=((double)$fees-(1-(double)($discount/100)));
								}
								$type = trim($prow->ptype);
								$category = trim($prow->category);
								if($type!=""){
									echo $type;
								}
								if($category!=""){
									echo ', '.$category;
								}
								if($ptype==3){
									echo '<button type="button" onClick="getPaymentInfo(`'.$feetype.'`, '.$payable.','.$prow->id.');" class="btn btn-success btn-sm btn-block">Apply Now</button>';
								}else{
									$dur = intval(trim($prow->duration));
									echo ', Duration: '.$dur.' '.trim($prow->dtype).'(s), ';
									$curdate = strtotime(date('d-m-Y'));
									$ldt = strtotime(date('d-m-Y',strtotime($prow->aend_date)));
									$sdt = strtotime(date('d-m-Y',strtotime($prow->astart_date)));
									if($sdt!=19800 || $ldt!=19800){
										if($curdate<$sdt){
											echo '<h5 class="text-center text-primary">Admission starts on '.date('jS M Y',strtotime($prow->astart_date)).'</h5>';
										}else{
											echo '<h5 class="text-center text-danger">Admission Closed</h5>';
										}
									}else{
										echo '<h5 class="text-center text-danger">Admission Closed</h5>';
									}
								}
								
							?>
						  </div>
						  <div class="card-footer">
							<div class="price">
							  <h4>
							  <?php
								if($feetype=='Paid'){
									if($discount==0 || $discount==''){
										echo 'Rs. '.$fees;
									}else{
										//echo $discount;
										echo 'Rs. <del>'.$fees.'</del>/'.$payable;
									}
								}else{
									echo 'Free';
								}
							  ?>
							  </h4>
							</div>
							<div class="stats">
							  <a href="<?php echo $durl; ?>"><p class="card-category">View Details</p></a>
							</div>
						  </div>
						</div>
					</div>
					<?php $i++; } ?>
						

				</div>
			</div>
		</div>
	
	</div>
</div>
<div class="modal fade" id="payModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	  <div class="modal-content">
		<div class="modal-header">
		  <h4 class="modal-title text-center font-weight-bold">Payment Confirmation</h4>
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			<i class="material-icons">clear</i>
		  </button>
		</div>
		<form id="frmPayment" action="<?php echo base_url('Student/payAutoProgram'); ?>" method="POST">
		<div class="modal-body">
			<div class="form-group">
				<label>Fullname</label><br>
				<input type="text" name="fullname" id="fullname" value="<?php echo $_SESSION['userData']['name']; ?>" class="form-control" readonly/>
				<input type="hidden" name="user_id" id="user_id" value="<?php echo $_SESSION['userData']['userId']; ?>"/>
				<input type="hidden" name="prog_id" id="prog_id" value=""/>
			</div>
			<div class="form-group">
				<label>Email</label><br>
				<input type="text" name="email" id="email" value="<?php echo $_SESSION['userData']['email']; ?>" class="form-control" readonly/>
			</div>
			<div class="form-group">
				<label>Mobile</label><br>
				<input type="text" name="mobile" id="mobile" value="<?php echo $_SESSION['userData']['mobile']; ?>" class="form-control" readonly/>
			</div>
			<div class="form-group">
				<label>Amount Payable</label><br>
				<input type="number" name="pamount" id="pamount" value="" class="form-control" readonly/>
			</div>
		</div>
		<div class="modal-footer">
		  <button type="submit" class="btn btn-link" id="btn_save_pass">Pay Now</button>
		  <button type="button" class="btn btn-danger btn-link" data-dismiss="modal">Close</button>
		</div>
		</form>
	  </div>
	</div>
</div>
<script src="<?php echo base_url().'assets/js/function.js'; ?>"></script>
<script>
	function getPaymentInfo(feetype, pamt, prog_id)
	{
		$.ajax({
			url: baseURL+'Student/applyAutoProgram/'+prog_id+'/'+feetype,
			success: (response)=>{
				if(response=='1'){
					$.notify({icon:"add_alert",message:'You have already enrolled into this program.'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})
				}else if(response=='2'){
					$('#prog_id').val(prog_id);
					$('#pamount').val(pamt);
					$('#payModal').modal('show');
				}else if(response=='3'){
					$.notify({icon:"add_alert",message:'Something went wrong. Please try again.'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})
				}else if(response=='4'){
					$.notify({icon:"add_alert",message:'You have successfully enrolled into the program. Check your dashboard.'},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})
				}
			}
		});
	}
</script>