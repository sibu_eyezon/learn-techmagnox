<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <link rel="icon" type="image/png" href="<?php echo base_url().'assets/img/favicon.png'; ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>Live Class | Magnox Learning+ - Student</title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--  Social tags      -->
  <meta name="keywords" content="">
  <meta name="description" content="">

  <!-- CSS Files -->
  <link href="<?php echo base_url(); ?>assets/css/material-dashboard.min1c51.css?v=2.1.2" rel="stylesheet" />
  
  <!--   Core JS Files   -->
  <script src="<?php echo base_url(); ?>assets/js/core/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendor/jquery-ui/jquery-ui.js"></script>  
</head>

<body>



<style>
#course_code::placeholder {
	color: white;
}
</style>
<div class="content">
	<div class="container-fluid">
		<div class="row" id="layout-1">			
			<div class="col-md-12">
				<div id="jitsi-container" style="min-height:600px; margin-top: 10px;"></div>
			</div>
			<div class="col-md-12 text-center">
				<button class="btn btn-primary" onClick="quitClass()">Quit Class</button>
			</div>
		</div>
		<div class="row" id="layout-2" style="display: none">
			<div class="col-md-12 text-center">
				<button class="btn btn-success" onClick="joinClass()">Rejoin Class</button>
			</div>
		</div>
		<div class="row" id="layout-3" style="display: none">
			<div class="col-md-12 text-center">
				<h2>This class is over</h2>
			</div>
		</div>
	</div>
</div>

<script>
var container = document.querySelector('#jitsi-container');
var api = null;
var room_name = '<?=$room_name;?>';

$(document).ready(function(){
	var domain = "meet.jit.si";
	var options = {
		"roomName": room_name,
		"parentNode": container,
		"width": 100+'%',
		"height": 600,
	};
	configOverwrite = {
		prejoinPageEnabled: false
	};
	options['configOverwrite'] = configOverwrite;
	api = new JitsiMeetExternalAPI(domain, options);
});

function quitClass(){
	api.dispose();
	$('div#layout-1').fadeOut(500);
	$('div#layout-2').fadeIn(500);
}

setInterval(() => {
	$.ajax({
		url: '<?=base_url()?>'+'Student/getLiveClassStatus',
		type: 'GET',
		data: {link: '<?=$link;?>' },
		success: (res)=>{ 			
			var obj = JSON.parse(res);
			if(obj['status'] == 'success'){
				if(obj['live_class_status'] == 'f'){
					api.dispose();
					$('div#layout-1').fadeOut(500);
					$('div#layout-2').fadeOut(500);
					$('div#layout-3').fadeIn(500);
				}
			}	
			if(obj['status'] == 'error'){
				console.log(obj['msg']);
			}		
		},
		error: (errors)=>{
			console.log(errors);
		}
	});
}, 5000);


function joinClass(){
	location.reload();
}

</script>
<script src='https://meet.jit.si/external_api.js'></script>
</body>

</html>