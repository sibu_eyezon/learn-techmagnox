<style>
#course_code::placeholder {
	color: white;
}
.fa-spinner {
	animation-name: spin;
	animation-duration: 5000ms;
	animation-iteration-count: infinite;
	animation-timing-function: linear;
}
@keyframes spin {
    from {
        transform:rotate(0deg);
    }
    to {
        transform:rotate(360deg);
    }
}
.isDisabled {
  color: currentColor;
  cursor: not-allowed;
  opacity: 0.5;
  text-decoration: none;
}
.card-description {
	display:none;
	transition: height 0.5s;
}
.card-description .show {
	display: block;
	height: auto;
}
</style>
<div class="content">
	<div class="container-fluid">
		<div class="row" id="layout-1">
			<div class="col-lg-9 col-md-9 mb-3">
				<div class="row">
					<div class="col-sm-8 mb-3">
						<div class="card bg-primary mt-0">
							<div class="card-body">
								<div class="row px-5">
									<div class="col-sm-6 col-xs-12">
										<img src="<?php echo base_url().$_SESSION['userData']['photo']; ?>" onerror="this.src='<?php echo base_url(); ?>assets/img/default-avatar.png'" class="img-fluid rounded-circle pull-left mr-3" width="85">
										<p>
											<h5 class="font-weight-bold"><?php echo $_SESSION['userData']['name']; ?></h5>
											<h6><?php echo $_SESSION['userData']['email'].', '.$_SESSION['userData']['mobile']; ?></h6>
										</p>
									</div>
									<div class="col-sm-6 col-xs-12">
										<p class="text-left">
											Programs Completed: <?php echo $studInfo[0]->progcomplete; ?><br>
											Programs Applied: <?php echo $studInfo[0]->progapplied; ?><br>
											Total Expenditure: Rs. <?php echo ($studInfo[0]->totalexpense!="")? $studInfo[0]->totalexpense.'/-' : "-"; ?>
										</p>
									</div>
								</div>
							</div>
							<div class="card-footer pt-0">
								<div class="price"></div>
								<div class="stats pull-right">
								<p class="card-category"><?php echo 'Last login: '.date('jS M Y h:i a', strtotime($lastLoggedIn[0]->logout_datetime)); ?></p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-4 mb-3">
						<div class="card bg-light mt-0 mb-0">
							<div class="card-body align-self-center">
								<ul class="list-group list-group-horizontal">
									<!--<li class="list-group-item"><a href="javascript:;" title="Join Live Class" rel="tooltip" class="isDisabled" id="live-show" disabled><i class="material-icons text-info" style="font-size:3rem;">cast_for_education</i></a></li>-->
									<li class="list-group-item"><a href="<?php echo base_url('Lab/userLabs'); ?>" title="Virtual Lab" rel="tooltip"><i class="material-icons text-success" style="font-size:3rem;">science</i></a></li>
									<li class="list-group-item"><a href="javascript:;" onClick="getUserData('skills');" title="My Skills" rel="tooltip"><i class="material-icons text-primary" style="font-size:3rem;">analytics</i></a></li>
									<li class="list-group-item"><a href="javascript:;" onClick="getUserData('resume');" title="My Resume" rel="tooltip"><i class="material-icons text-secondary" style="font-size:3rem;">article</i></a></li>
									<!--<li class="list-group-item"><a href="javasript:;" title="My Video Resume" rel="tooltip"><i class="material-icons text-danger" style="font-size:3rem;">video_camera_front</i></a></li>-->
								</ul>
								<ul class="list-group list-group-horizontal">
									<li class="list-group-item"><a href="<?php echo base_url('Student/message'); ?>" title="Message" rel="tooltip"><i class="material-icons text-success" style="font-size:3rem;">email</i></a></li>
									<li class="list-group-item"><a href="<?php echo base_url('uploads/help.pdf'); ?>" target="_blank" title="Help" rel="tooltip"><i class="material-icons text-primary" style="font-size:3rem;">live_help</i></a></li>
									<li class="list-group-item"><a href="javascript:;" data-toggle="modal" data-target="#passModal" title="Change Password" rel="tooltip"><i class="material-icons text-secondary" style="font-size:3rem;">settings</i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-12">
						<div class="card mt-2" style="background:transparent !important;">
							<div class="card-header card-header-info">
								<h3 class="card-title">My Programs</h3>
							</div>
							<div class="card-body">
							<div class="row">
								<?php $status=''; $certify = ''; $i=1; if(count($programs)>0){ foreach($programs as $prow){ 
									$id = $prow->id;
									$ptype=intval(trim($prow->type));
									$bolcertify = $prow->bolcertify;
									$title = trim($prow->title);
									$type = trim($prow->ptype);
									$category = trim($prow->category);
									$aflag = intval(trim($prow->approve_flag));
									if(!empty(${'spc_'.$i})){
										$status = ${'spc_'.$i}[0]->status;
										$certificate = ${'spc_'.$i}[0]->certificate;
									}
								?>
								<div class="col-sm-4 mb-3">
									<div class="card">
									  <div class="card-body" style="min-height:26vh;">
										<h5 class="card-title text-dark">
										  <a href="<?php echo base_url('Student/viewProgram/?id='.base64_encode($id)); ?>"><?php echo $title; ?></a>
										</h5>
										<?php
											if($aflag==0){
												echo '<h6>Your application is pending for first approval</h6>';
											}else if($aflag==1){
												echo '<h6>Your application is pending for final approval</h6>';
											} if($aflag==2){
												if($ptype==3){
													include 'autopview.php';
												}else{
													include 'otherpview.php';
												}
												if($status=='2'){
													if($bolcertify=='auto'){
														echo '<h6 class="text-center"><a href="'.base_url('Student/studCertificate/?id='.base64_encode($id)).'" target="_blank"><i class="material-icons">cloud_download</i> Download Certificate</a><br></h6>';
													}else if($bolcertify=='manual'){
														if($certificate!=""){
															if(file_exists('./'.$certificate)){
																echo '<h6 class="text-center"><a href="'.base_url($certificate).'" target="_blank"><i class="material-icons">cloud_download</i> Download Certificate</a><br></h6>';
															}
														}
													}else if($bolcertify=='no'){
														echo '<h6 class="text-center">No certificate.</h6>';
													}
													
												}
											}
											
										?>
									  </div>
									  <div class="card-footer text-dark">
										<div class="price">
										  <h5><?php echo ($status=='1')? 'Ongoing' : (($status=='2')? 'Completed' : (($status=='3')? 'Failed' : 'Pending')); ?></h5>
										</div>
										<div class="stats">
										  <p class="card-category text-dark"><?php echo $category; ?></p>
										</div>
									  </div>
									</div>
								</div>
								<?php $i++; } }  ?>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			
			</div>
			<div class=" col-lg-3 col-md-3">
				<div class="card mt-0" id="live-button-container" style="display:none">
					<div class="card-body">
						<div id="live-buttons">
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header card-header-info">
						<h3 class="card-title">Notices</h3>
					</div>
					<div class="card-body">
						<div class="notice_ticker w-100" style="height:30vh;">
							<ul class="list-group w-100" id="notc_details">
							
							</ul>
						</div>
					</div>
					<div class="card-footer">
						<a href="<?php echo base_url('Student/notices'); ?>" class="btn btn-sm btn-info">Know more</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="layout-2" style="display:none;">
			<div class="col-lg-10 col-md-10">
				<div id="jitsi-container" style="min-height:400px;"></div>
			</div>
			<div class="col-lg-2 col-md-2">
				<button class="btn btn-primary btn-block" onClick="quitClass()">Quit Class</button>
				<div class="text-center video-right">
					<a href="<?php //echo base_url('Lab/userLabs'); ?>" target="_blank">
						<i class="material-icons">science</i><br>
						Lab
					</a>
					<!--<a href="<?php //echo base_url().'Student/courseDetails/?cid='.base64_encode(${'pcourses1'}[0]->id).'&prog='.base64_encode(${'pcourses1'}[0]->prog_id); ?>" target="_blank">
					<i class="material-icons">info</i><br>
					Course Details
					</a>
					<br>
					<a href="<?php //echo base_url().'Student/courseDetails/?cid='.base64_encode(${'pcourses1'}[0]->id).'&prog='.base64_encode(${'pcourses1'}[0]->prog_id); ?>;" target="_blank">
					<i class="material-icons">help</i><br>
					Doubts
					</a>
					<br>
					<a href="<?php //echo base_url('Lab/userLabs'); ?>" target="_blank">
					<i class="material-icons">science</i><br>
					Lab
					</a>
					<img src="https://via.placeholder.com/150" class="img-responsive">
					<img src="https://via.placeholder.com/150" class="img-responsive">-->
				</div>
			</div>
		</div>
	</div>
</div>
<div id="applyD" title="List of Applied Programs" style="width: 450px;"></div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			  <h4 class="modal-title" id="mheader"></h4>
			  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				<i class="material-icons">clear</i>
			  </button>
			</div>
			<div class="modal-body" id="mbody"> 
			</div>
			<div class="modal-footer">
			  <button type="button" class="btn btn-danger btn-link" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function() {
	getProgramNotices();
	checkUserApplication();
	$('#frmLive').validate({
		errorPlacement: function(error, element) {
		  $(element).closest('.form-group').append(error);
		},
		submitHandler: function(form, e) {
			$('#loading').css('display', 'block');
			$('#liveBt').attr('disabled', true);
			e.preventDefault();
			var frmLiveData = new FormData($('#frmLive')[0]);
			$.ajax({
				url: baseURL+'Teacher/checkValidCourse',
				type: 'POST',
				data: frmLiveData,
				cache : false,
				processData: false,
				contentType: false,
				async: false,
				success: (res)=>{ 
					$('#frmLive')[0].reset();
					$('#liveBt').removeAttr('disabled');
					$('#loading').css('display', 'none');
					var obj = JSON.parse(res);
					if(obj['status']=='success'){
						window.open(baseURL+'Student/liveClass/?id='+btoa(obj['cid']), '_self');
					}else{
						alert(obj['msg']);
						swal(
						  'Course Code',
						  obj['msg'],
						  obj['status']
						);
					}
				},
				error: (errors)=>{
					console.log(errors);
				}
			});
		}
	});
});
function toggleProgCourse(serial)
{
	$('#prog_course_'+serial).toggle('show');
}
function checkUserApplication()
{
	var dialogBox = '';
	$.ajax({
		url: baseURL+'Student/findUserApply',
		type: 'GET',
		success: (resp)=>{
			var obj = JSON.parse(resp);
			if(obj['status']){
				if(obj['apply_status']){
					dialogBox+='<h5 class="text-danger text-center">You have already applied for this program: '+obj['prog_name']+'</h5>';
				}else{
					dialogBox+='<h5>You have less than 5 minutes to enroll into this program: '+obj['prog_name']+'</h5>';
					dialogBox+='<a href="'+baseURL+'Student/programAdmission/?id='+btoa(obj['prog_id'])+'" class="btn btn-success btn-sm">Enroll Now</a>';
				}
				$('#mbody').html(dialogBox);
				$('#myModal').modal('show');
			}
		}
	});
}
function getUserData(dtype)
{
	var oricon = '';
	if(dtype=='resume'){
		$('#ricon').html('<i class="fa fa-spinner" aria-hidden="true"></i>');
		$('#mheader').html('User Resume');
		oricon = '<i class="material-icons">verified</i>';
	}else if(dtype=='skills'){
		$('#sicon').html('<i class="fa fa-spinner" aria-hidden="true"></i>');
		$('#mheader').html('User Skills');
		oricon = '<i class="material-icons">bar_chart</i>';
	}
	$.ajax({
		url: baseURL+'Student/getUserValidData',
		type: 'GET',
		data: { dtype: dtype },
		success: (resp)=>{
			console.log(resp)
			$('#mbody').html(resp);
			$('#myModal').modal('show');
			if(dtype=='resume'){
				$('#ricon').html('<i class="material-icons">verified</i>');
			}else if(dtype=='skills'){
				$('#sicon').html('<i class="material-icons">bar_chart</i>');
			}
		}
	});
}
var container = document.querySelector('#jitsi-container');
var api = null;
var meeting_joined = false;
var room_name = '';
var invitation = false;
setInterval(() => {
	$.ajax({
		url: baseURL+'Liveclass/getAllMyInvitation',
		type: 'GET',
		data: {user_id: <?=$userid;?>},
		success: (res)=>{ 			
			var obj = JSON.parse(res);
			if(obj['status'] == 'success'){
				let buttons = "";
				for (let i = 0; i < obj['data'].length; i++) {
					const element = obj['data'][i];
					buttons += '<button class="btn btn-primary btn-block" id="'+element['room_name']+'" ';
					buttons += 'data-room="'+element['room_name']+'" ';
					buttons += 'data-lc_id="'+element['live_class_id']+'" ';
					buttons += 'onClick="joinClass(\''+element['room_name']+'\')"';
					buttons += '>Join Online Class ('+element['c_code']+')</button>';					
				}
				//$('#lvcdeails').attr('href', baseURL+'Student/courseDetails/?cid='+btoa(element['course_id'])+'&prog='+btoa());
				$('#live-show').removeAttr('disabled');
				$('#live-show').removeClass('isDisabled');
				$('div#live-buttons').html(buttons);
				$('#live-button-container').fadeIn(500);
			}	
			if(obj['status'] == 'error'){
				if(meeting_joined == true){
					api.dispose();
					$('div#live-buttons').html("");
					$('#live-button-container').fadeOut(500);
					$('div#layout-2').fadeOut(500);
					$('div#layout-1').fadeIn(500);
					$('#live-show').attr('disabled');
					$('#live-show').addClass('isDisabled');
				}
			}		
		},
		error: (errors)=>{
			console.log(errors);
		}
	});
}, 5000);

function joinClass(roomName){
	let lc_id = $('#'+roomName).data('lc_id');
	$.ajax({
		url: baseURL+'Liveclass/joinMeeting',
		type: 'POST',
		data: {lc_id: lc_id},
		success: (res)=>{ 			
			var obj = JSON.parse(res);
			if(obj['status'] == 'success'){
				console.log(obj['msg']);
				var domain = "meet.jit.si";
				var options = {
					"roomName": roomName,
					"parentNode": container,
					"width": 100+'%',
					"height": 600,
				};
				configOverwrite = {
					prejoinPageEnabled: false
				};
				options['configOverwrite'] = configOverwrite;
				api = new JitsiMeetExternalAPI(domain, options);
				$('div#layout-1').fadeOut(500);
				$('div#layout-2').fadeIn(500);
				meeting_joined = true;
			}		
		},
		error: (errors)=>{
			console.log(errors);
		}
	});
}
function quitClass(){
	api.dispose();
	$('div#layout-2').fadeOut(500);
	$('div#layout-1').fadeIn(500);
}
</script>
<script src='https://meet.jit.si/external_api.js'></script>