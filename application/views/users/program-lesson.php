<style>
.carousel-item {
	height:570px;
	background-color:grey;
}
.carousel-control-next, .carousel-control-prev{
	width:5% !important;
	height:auto;
}
.isDisabled {
  color: currentColor;
  cursor: not-allowed;
  opacity: 0.5;
  text-decoration: none;
}
.loader {
  background-color: #ffffff;
  opacity:0.5;
  position: fixed;
  z-index: 999999;
  height: 100%;
  width: 100%;
  top: 0;
  left: 0;
}

.loader img {
  position: absolute;
  top: 50%;
  left: 50%;
  text-align: center;
  -webkit-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
}
</style>
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-9">
				<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
				  <div class="carousel-inner">
					<div class="carousel-item active">
					  <div class="loader" id="loading" style="display:none;">
						<img src="<?php echo base_url().'assets/img/loading.gif'; ?>" width="100">
					  </div>
					  <div id="lesson_data"></div>
					</div>
				  </div>
				  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" id="cprev" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				  </a>
				  <a class="carousel-control-next" href="#carouselExampleControls" role="button" id="cnext" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				  </a>
				</div>
			</div>
			<div class="col-md-3">
				<div class="card mt-0" style="height:500px; overflow-y:scroll;">
					<div class="card-body">
						<div id="accordion" role="tablist">
							<?php 
								$i=1; if(!empty($sections)){ foreach($sections as $section){ 
								$sec_id = $section->id;
								$lessons = $this->AutoProgramModel->get_ordered_lessons($sec_id, $prog_id);
							?>
							<div class="card-collapse">
							  <div class="card-header" role="tab" id="heading_<?php echo $i; ?>">
								<h5 class="mb-0">
								  <a data-toggle="collapse" href="#collapse_<?php echo $i; ?>" aria-expanded="true" aria-controls="collapse_<?php echo $i; ?>" class="collapsed">
									<?php echo trim($section->title); ?>
								  </a>
								</h5>
							  </div>
							  <div id="collapse_<?php echo $i; ?>" class="collapse <?php echo ($i==1)? 'show' : ''; ?>" role="tabpanel" aria-labelledby="heading_<?php echo $i; ?>" data-parent="#accordion" style="">
								<div class="card-body">
								  <?php
									if(!empty($lessons)){
										echo '<ul class="list-group">';
										foreach($lessons as $lesson){
											echo '<li class="list-group-item"><input type="checkbox" name="ap_sl[]" id="lid_'.$lesson->lrt_id.'" value="false" disabled/>  '.trim($lesson->title).'</li>';
										}
										echo '<ul>';
									}
								  ?>
								</div>
							  </div>
							</div>
							<?php $i++; } } ?>
						</div>
					
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('#cnext').attr('disabled', 'true');
		$('#cnext').addClass('isDisabled');
		togglePrevView(<?php echo $ltviewed; ?>);
		getLessons(<?php echo $spc_id; ?>, 0);
	});
	function togglePrevView(ltcount)
	{
		if(ltcount==0){
			$('#cprev').hide();
		}else{
			$('#cprev').show();
		}
	}
	function getLessons(spc_id, spsd_id)
	{
		$.ajax({
			url: baseURL+'AutoProgram/get_lessons/'+spc_id+'/'+spsd_id,
			beforeSend: ()=>{
				$('#loading').show();
			},
			success: (response)=>{
				$('#loading').hide();
				$('#lesson_data').html(response);
			}
		});
	}
</script>