<div class="content">
	<div class="container-fluid">
		<div class="row">
		
			<div class="col-lg-10 col-md-10 mx-auto">
                <div class="row">
                    <?php if(!empty($pjobs)){ foreach($pjobs as $row){ ?> 
                        <div class="col-md-6 mb-2">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title font-weight-bold"><a href="<?= base_url('Student/job_details/?id='.base64_encode($row->id)); ?>"><?= $row->title; ?></a></h4>
                                    <p class="text-justify">
                                        <?php 
                                        if($row->type){
                                            echo '<br>'.implode(", ", json_decode($row->type));
                                        }
                                        if($row->designation_id){
                                            echo '<br>'.trim($row->designation_name);
                                        }
                                        if(trim($row->min_qualification_id)){
                                            echo '<br>'.trim($row->degree_name." (".$row->short.")");
                                        }
                                        if($row->org_id){
                                            echo '<br>Organization: '.trim($row->org_title);
                                        }
                                        if(trim($row->salary)){
                                            echo '<br>Salary: '.trim($row->salary);
                                        }
                                        if($row->experience){
                                            echo '| Experience: '.trim($row->experience);
                                        }
                                        ?>
                                    </p>
									<a href="<?= base_url('Student/job_details/?id='.base64_encode($row->id)); ?>" class="btn btn-primary btn-md">Details</a>
                                </div>
                            </div>
                        </div>
                    <?php } } ?>
                </div>
            </div>
        </div>
    </div>
</div>