<style>
.loader {
  background-color: #ffffff;
  opacity:0.5;
  position: fixed;
  z-index: 999999;
  height: 100%;
  width: 100%;
  top: 0;
  left: 0;
}
.loader img {
  position: absolute;
  top: 50%;
  left: 50%;
  text-align: center;
  -webkit-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
}
</style>
<div class="loader" id="loading" style="display:none;">
	<img src="<?php echo base_url().'assets/img/loading.gif'; ?>">
</div>
<div class="content">
	<div class="container-fluid">
		<div class="row">
		
			<div class="col-lg-10 col-md-10 mx-auto">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title font-weight-bold"><?= $job[0]->title; ?></h4>
                        <div class="text-justify">
                            <?php 
                            if($job[0]->type){
                                echo '<br>'.implode(", ", json_decode($job[0]->type));
                            }
                            if($job[0]->designation_id){
                                echo '<br>'.trim($job[0]->designation_name);
                            }
                            if(trim($job[0]->min_qualification_id)){
                                echo '<br>'.trim($job[0]->degree_name." (".$job[0]->short.")");
                            }
                            if($job[0]->org_id){
                                echo '<br>Organization: '.trim($job[0]->org_title);
                            }
                            if(trim($job[0]->salary)){
                                echo '<br>Salary: '.trim($job[0]->salary);
                            }
                            if($job[0]->experience){
                                echo '| Experience: '.trim($job[0]->experience);
                            }
                            if(trim($job[0]->desc)){
                                echo '<br>'.trim($job[0]->desc);
                            }
                            ?>
                        </div>
                        <button type="button" onClick="user_apply_job(<?= $job[0]->id; ?>, '<?= trim($job[0]->title); ?>')" class="btn btn-primary">Apply</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function user_apply_job(id, title){
        swal({
			title: 'Are you sure?',
			text: "You want to apply to this job: "+title,
			type: 'warning',
			showCancelButton: true,
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: 'Yes, Apply it!',
			buttonsStyling: false
		}).then(function() {
			$('#loading').show();
			$.ajax({
				url: baseURL+'Student/user_job_apply/?pnid='+id,
				type: 'GET',
				success: (res)=>{
					$('#loading').fadeOut(1000);
                    var obj = JSON.parse(res)
					swal({
                        title: obj['title'],
                        text: obj['msg'],
                        type: obj['type'],
                        confirmButtonClass: "btn btn-success",
                        buttonsStyling: false
                    })
				},
                error: (err)=>{
                    $('#loading').fadeOut(1000);
					swal({
                        title: 'Error',
                        text: 'Something went wrong.',
                        type: 'error',
                        confirmButtonClass: "btn btn-success",
                        buttonsStyling: false
                    })
                }
			})
		}).catch(swal.noop)
	}
</script>