<style>
.loader {
  background-color: #ffffff;
  opacity:0.5;
  position: fixed;
  z-index: 999999;
  height: 100%;
  width: 100%;
  top: 0;
  left: 0;
}

.loader img {
  position: absolute;
  top: 50%;
  left: 50%;
  text-align: center;
  -webkit-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
}
</style>
<div class="loader" id="loading" style="display:none;">
	<img src="<?php echo base_url().'assets/img/loading.gif'; ?>">
</div>
<div class="content">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header card-header-primary card-header-text">
					  <div class="card-text">
						<h4 class="card-title">My Program Status</h4>
					  </div>
					</div>
					<div class="card-body">
						<div class="clearfix"></div>
						<div class="row">
							<div class="col-md-12">
								<div class="material-datatables">
									<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
									  <thead>
										<tr>
										  <th width="5%">Sl</th>
										  <th width="45%">Program Title</th>
										  <th width="35%">Payment Status</th>
										  <th width="15%">Status</th>
										</tr>
									  </thead>
									  <tbody>
										<?php 
											$i=1; 
											foreach($rusers as $row){ 
											$af = trim($row->approve_flag);
											$ft = trim($row->feetype);
										?>
										<tr>
										  <td><?php echo $i; ?></td>
										  <td><?php echo trim($row->title); ?></td>
										  <td>
											<?php
												if($ft=='Free'){
													echo 'Free Program';
												}else if($ft=='Paid'){
													if($af=='0'){
														echo 'Approval Pending';
													}else if($af=='1'){
														$dur = intval(trim($row->duration))*12;
														$st = intval(trim($row->sem_type));
														if(isset(${'spcdata_'.$i})){
															$tfee = intval(trim(${'spcdata_'.$i}[0]->totalfees));
															$dis = intval(trim(${'spcdata_'.$i}[0]->discount));
															$pdone = intval(trim(${'spcdata_'.$i}[0]->payment_done));
															if($dis!=0){
																$payable = $tfee*(1-($dis/100));
																echo 'Amount: <strike>Rs. '.$tfee.'</strike><br><strong>Pay: Rs. '.$payable.'</strong>';
															}else{
																$payable = $tfee;
																echo '<strong>Amount: Rs. '.$payable.'</strong>';
															}
															if(($payable-$pdone)==0){
																echo '<br>Payment Completed';
															}else{
																if($pdone==0){
																	echo '<br><button type="button" onClick="getPaymentInfo('.${'spcdata_'.$i}[0]->sl.', '.$payable.', '.${'spcdata_'.$i}[0]->prog_id.', 0)" class="btn btn-warning">Proceed to payment</button>';
																}else{
																	if($st==0){
																		if($payable<5000){
																			$pnow = $payable;
																		}else{
																			$pnow = $payable/4;
																		}
																	}else{
																		$pnow = $payable/($dur/$st);
																	}
																	echo '<strong>Amount: Rs. '.($payable-$pdone).'</strong>';
																	echo '<br><button type="button" onClick="getPaymentInfo('.${'spcdata_'.$i}[0]->sl.', '.$pnow.', '.${'spcdata_'.$i}[0]->prog_id.', 1)" class="btn btn-info">Pay Now: '.$pnow.'</button>';
																}
															}
														}else{
															echo 'Something ia missing';
														}
														
													}else if($af=='2'){
														echo 'Approval Successfull';
													}
												}
											?>
										  </td>
										  <td>
											<?php 
												if($af=='2'){
													echo '<span class="label label-success">Final Approved</span>';
												}else if($af=='1'){
													echo '<span class="label label-danger">First Approved</span>';
												}else{
													echo '<span class="label label-warning">Pending</span>';
												}
											?>
										  </td>
										</tr>
										<?php $i++; } ?>
									  </tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="payModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
	  <div class="modal-content">
		<div class="modal-header">
		  <h4 class="modal-title text-center font-weight-bold">Payment Confirmation</h4>
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			<i class="material-icons">clear</i>
		  </button>
		</div>
		<div class="modal-body">
		
		</div>
	  </div>
	</div>
</div>
<script src="<?php echo base_url(); ?>assets/js/plugins/jquery.dataTables.min.js"></script>
<script>
	function getPaymentInfo(spcid, pamt, prog_id, method)
	{
		$('#loading').show();
		$.ajax({
			url: baseURL+'Student/checkPayment',
			type: 'GET',
			data: {
				spcid: spcid,
				pamt: pamt,
				prog_id: prog_id,
				method: method
			},
			success: (res)=>{
				$('#loading').hide();
				$('.modal-body').html(res);
				$('#payModal').modal('show');
			},
			error: (errors)=>{
				console.log(errors);
			}
		});
	}
</script>