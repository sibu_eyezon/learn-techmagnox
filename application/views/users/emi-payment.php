<div class="row">
	<div class="col-sm-6">
		<div class="card mt-0">
			<div class="card-body">
				<img src="<?= base_url('assets/img/banner/'.$pdetails[0]->banner); ?>" onerror="this.src='<?php echo base_url('assets/img/sample.jpg'); ?>'" class="card-img-top">
				<div class="card-description">
					<?php
						echo '<h5 class="card-title">'.trim($pdetails[0]->title).'</h4>';
						$tfee = intval(trim($spcdata[0]->totalfees));
						$dis = intval(trim($spcdata[0]->discount));
						$dur = intval(trim($pdetails[0]->duration))*12;
						$st = intval(trim($pdetails[0]->sem_type));
						if($dis!=0){
							$payable = $tfee*(1-($dis/100));
							echo 'Amount: <strike>Rs. '.$tfee.'</strike><br><strong>Pay: Rs. '.$payable.'</strong>';
						}else{
							$payable = $tfee;
							echo '<strong>Amount: Rs. '.$payable.'</strong>';
						}
					?>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<form action="<?php echo base_url('Student/payNow'); ?>" method="POST" id="frmPayment">
			<div class="form-group">
				<label>Fullname</label><br>
				<input type="text" name="fullname" id="fullname" value="<?= $_SESSION['userData']['name']; ?>" class="form-control" readonly/>
				<input type="hidden" name="user_id" id="user_id" value="<?= $_SESSION['userData']['userId']; ?>"/>
				<input type="hidden" name="spc_id" id="spc_id" value="<?= $spcid; ?>"/>
				<input type="hidden" name="prog_id" id="prog_id" value="<?= $pdetails[0]->sl; ?>"/>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label>Email</label><br>
						<input type="text" name="email" id="email" value="<?= $_SESSION['userData']['email']; ?>" class="form-control" readonly/>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label>Mobile</label><br>
						<input type="text" name="mobile" id="mobile" value="<?= $_SESSION['userData']['mobile']; ?>" class="form-control" readonly/>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<select class="custom-select" name="payment_type" id="payment_type" onChange="checkPayment(this.value);" required>
							<option value="">Select Payment</option>
							<option value="full">Full Payment</option>
							<option value="emi">EMI</option>
						</select>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<div class="checkbox-radios">
							<div class="form-check form-check-inline">
							  <label class="form-check-label text-dark">
								<input class="form-check-input" type="checkbox" value="1" name="pay_method" id="pay_method" required> Razorpay
								<span class="form-check-sign">
								  <span class="check"></span>
								</span>
							  </label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label>Amount Payable</label><br>
				<input type="text" name="pamount" id="pamount" value="<?= $pamt; ?>" class="form-control" readonly/>
			</div>
			<div class="form-group">
			  <button type="submit" class="btn btn-success btn-md" id="btn_pass">Pay Now</button>
			  <button type="button" class="btn btn-danger btn-md ml-3" data-dismiss="modal">Close</button>
			</div>
		</form>
	</div>
</div>
<script>
var pamount = parseInt('<?= $pamt; ?>');
function checkPayment(pt)
{
	//var pt = $('#payment_type').val();
	
	var dur = parseInt('<?= intval(trim($pdetails[0]->duration))*12; ?>');
	var st = parseInt('<?= trim($pdetails[0]->sem_type); ?>');
	var pnow = 0;
	if(pt!=""){
		if(pt=='full'){
			$('#frmPayment #pamount').val(pamount);
		}else if(pt=='emi'){
			if(st==0){
				if(pamount<=5000){
					pnow = pamount;
					$.notify({icon:"add_alert",message:'EMI is not applicable for amount below 5k.'},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})
				}else{
					pnow = pamount/4;
				}
			}else{
				pnow = pamount/(dur/st);
			}
			$('#frmPayment #pamount').val(pnow);
		}
	}
}
</script>