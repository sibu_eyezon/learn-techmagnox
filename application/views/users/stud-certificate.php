<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Oleo+Script:wght@700&display=swap" rel="stylesheet">
<style> 
.certificate{
    width: 1123px;
    height: 794px;
    margin: 0 auto;
    position: relative;
	border: 1px solid #000;
	padding: 40px;
	background-color: #fff;
	background-image: url('<?php echo base_url('uploads/certificates/'.trim($certificate[0]->bg_image)); ?>');
	background-size: cover;
    background-position: 100% 100%;
	background-repeat: no-repeat;
}
.cheader {
	width:100%;
	height:30%;
}
#mlogo {
	width:300px;
}
#info {
	float: right;
	font-weight: 600;
}
.cbody {
	width:100%;
	height:70%;
}
.cbody #csignature {
	float: right;
}
.cfooter {
	width:100%;
	height: 6%;
	display: inline-flex;
    font-size: 1rem;
}
.certificate-info{
    width: 80%;
    text-align: justify;
    font-weight: bold;   
	margin: 0 auto;
}
.skill-progress {
	width: 500px;
	margin:0 auto;
	background-color:#fff;
	padding: 10px;
}
.stud-name{
    color: #15bacf;
}
.stud-grade{
    color: #ff6223;
}
.stud-prog{
    color: #ff6223;
    font-style: italic;
}
</style>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/html2pdf.bundle.min.js"></script>
<div class="content">
    <div class="container-fluid">
        <div class="row"> 
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-info">
                        <h3 class="card-title">Certificate
						<button class="btn btn-primary btn-sm pull-right" id="downloadCertificate">Download as PDF</button>
						</h3>
                    </div>
                    <div class="card-body">
						<div class="row"> 
                            <div class="col-lg-12 col-md-12">
                                <div class="certificate" id="certificate">
									<div class="cheader">
										<!--<img src="<?php //echo base_url('assets/img/logo.png'); ?>" id="mlogo"/>
										<div id="info">
											<p class="text-left">
											www.techmagnox.com<br>
											www.billionskills.com<br>
											magnox.iitkgp@gmail.com<br>
											+91 9932242598
											</p>
										</div>-->
									</div>
									<div class="cbody justify-content-center">
										<h4 class="text-center" style="color:#ed2024;font-family: 'Oleo Script', cursive; font-weight: 800; font-size: 2.5rem;" id="mtitle"><i><?php echo trim($certificate[0]->title); ?></i></h4>
										<div class="certificate-info">
											<p style="line-height: 1.5; font-size: 1.1rem;">&emsp;&emsp;
												<?php
													echo trim($certificate[0]->text_before)." ";
													if($certificate[0]->student_name=='t'){
														echo "<span class='stud-name'>".trim($student_name)."</span> ";
													}
													echo trim($certificate[0]->text_after).".";
												?>
											</p>
											<!--<div class="skill-progress">
												<label class="text-dark">Knowledge  </label>
												<div class="progress progress-line-primary">
													<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
													</div>
												</div>
												<label class="text-dark">Intelligent  </label>
												<div class="progress progress-line-info">
													<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 75%;">
													</div>
												</div>
												<label class="text-dark">Hardworking  </label>
												<div class="progress progress-line-success">
													<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 95%;">
													</div>
												</div>
												<label class="text-dark">Attentiveness  </label>
												<div class="progress progress-line-rose">
													<div class="progress-bar progress-bar-rose" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 55%;">
													</div>
												</div>
												<label class="text-dark">Intuition  </label>
												<div class="progress progress-line-warning">
													<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">
													</div>
												</div>
											</div>-->
										</div>
									</div>
									<div class="cfooter">
										<!--<div class="text-left font-weight-bold">
										Magnox Technologies Pvt. Ltd., Mani Bhandar (7th Floor), Webel Bhawan, Sector V, Salt Lake, Kolkata - 700091
										</div>-->
										<div class="text-right font-weight-bold" style="margin-left: auto;color: #000; padding: 10px;">
											Certificate no: <?php echo $timestmp[0]->enrollment_no; ?>
										</div>
									</div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script> 
    window.onload = function(){
        document.getElementById("downloadCertificate")
        .addEventListener("click",() => {
            const certificate = this.document.getElementById("certificate");
            // html2pdf().from(certificate).save();
            var opt = {
                margin:       0,
                filename:     'myfile.pdf',
                image:        { type: 'jpeg', quality: 0.98 },
                html2canvas:  { scale: 2 },
                jsPDF:        { unit: 'in', format: 'A4', orientation: 'landscape' }
            };
            html2pdf().from(certificate).set(opt).save();
        })
    }
	$(document).bind("contextmenu",function(e){
	  return false;
	});
</script>