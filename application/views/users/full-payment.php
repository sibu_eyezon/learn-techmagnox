<form id="frmPayment" action="<?php echo base_url('Student/payNow'); ?>" method="POST">
	<div class="form-group">
		<label>Fullname</label><br>
		<input type="text" name="fullname" id="fullname" value="<?= $_SESSION['userData']['name']; ?>" class="form-control" readonly/>
		<input type="hidden" name="user_id" id="user_id" value="<?= $_SESSION['userData']['userId']; ?>"/>
		<input type="hidden" name="spc_id" id="spc_id" value="<?= $spcid; ?>"/>
		<input type="hidden" name="prog_id" id="prog_id" value="<?= $pdetails[0]->sl; ?>"/>
	</div>
	<div class="form-group">
		<label>Email</label><br>
		<input type="text" name="email" id="email" value="<?= $_SESSION['userData']['email']; ?>" class="form-control" readonly/>
	</div>
	<div class="form-group">
		<label>Mobile</label><br>
		<input type="text" name="mobile" id="mobile" value="<?= $_SESSION['userData']['mobile']; ?>" class="form-control" readonly/>
	</div>
	<div class="form-group">
		<label>Amount Payable</label><br>
		<input type="number" name="pamount" id="pamount" value="<?= $pamt; ?>" class="form-control" readonly/>
	</div>
	<div class="form-group">
	  <button type="submit" class="btn btn-success btn-md" id="btn_save_pass">Pay Now</button>
	  <button type="button" class="btn btn-danger btn-md ml-3" data-dismiss="modal">Close</button>
	</div>
</form>