<link rel="stylesheet" href="https://cdn.plyr.io/3.6.8/plyr.css" />
<script src="https://cdn.plyr.io/3.6.8/plyr.polyfilled.js"></script>
<style>
.menu-list {
    list-style: none;
    margin: 0.5em auto;
    padding: 0;
    width: 10em;
}

@media (min-width: 20em) {
    .menu-list {
        width: 19em
    }
}

@media (min-width: 30em) {
    .menu-list {
        width: 29em
    }
}

@media (min-width: 40em) {
    .menu-list {
        width: 39em
    }
}

@media (min-width: 60em) {
    .menu-list {
        width: 59em
    }
}

.menu-list .menu-list-item {
    float: left;
    margin: 0rem 0.2em;
    padding: 0.5em;
}

.menu-list .menu-list-item>a {
    color: #000;
    font-weight: 500;
}

.menu-list li a {
    display: grid;
    place-items: center;
}

.menu-list .menu-num {
    width: 50px;
    height: 50px;
    display: grid;
    place-items: center;
    background-color: #007986;
    color: #fff;
    font-size: 1.5rem;
    border-radius: 50%;
}

.menu-list .menu-txt {
    font-size: 0.9rem;
    font-weight: 500;
    text-transform: uppercase;
}

.prog-wrapper {
    display: flex;
    text-decoration: none;
    transition: all 0.4s;
}

#prog-sidebar {
    min-width: 200px;
    max-width: 200px;
    background: #eee;
    color: #fff;
    transition: all 0.4s;
    max-height: 100vh;
	overflow-y:scroll;
	-ms-overflow-style: none;  /* IE and Edge */
	scrollbar-width: none;  /* Firefox */
}
#prog-sidebar::-webkit-scrollbar {
  display: none;
}

#prog-sidebar.active {
    min-width: 100px;
    max-width: 100px;
}

#prog-sidebar.active>.prog_components li a {
    display: grid !important;
    place-items: center !important;
}

#prog-sidebar.active>.prog_components li a span {
    display:none;
}

.prog_components li a span {
    font-size: 0.8rem;
}

#prog-sidebar.active>.prog_components .icon-logo {
    width: 55px !important;
    height: 55px !important;
}

#prog-sidebar .sidebar-header {
    padding: 20px;
    background: #1b1d24;
}

#prog-sidebar ul.components {
    padding: 20px 0;
}

#prog-sidebar ul p {
    color: #fff;
    padding: 10px;
}

#prog-sidebar ul li a {
    font-size: 1.1em;
    display: inline-block;
    align-items: center;
    background: transparent;
    color: #000;
    font-weight: 500;
    font-size: 1rem;
    text-transform: uppercase;
}

a[data-toggle="collapse"] {
    position: relative;
}

.prog_components li a {
    font-size: 0.9em !important;
    text-align: left;
    background: #1b1d24;
}

#prog-content {
    width: 100%;
    min-width: height 100vh;
    transition: all 0.4s;
    background-color: #fff;
}

#prog-content h2 {
    margin-bottom: 50px;
}

#prog-content p {
    text-align: justify;
}

@media (max-width: 768px) {
    #prog-sidebar {
        min-width: 0px !important;
        max-width: 0px !important;
    }

    #prog-sidebar.active {
        min-width: 100px !important;
        max-width: 100px !important;
    }

    #prog-sidebar .prog_components li a {
        display: grid !important;
        place-items: center !important;
    }

    #prog-sidebar .prog_components li a span {
        display:none;
    }

    .prog_components li a span {
        font-size: 0.8rem;
    }

    #prog-sidebar .prog_components .icon-logo {
        width: 55px !important;
        height: 55px !important;
    }
}

.icon-logo {
    font-size: 20px;
    color: #123456;
    padding: 12px 15px;
    font-family: FontAwesome;
    width: 55px;
    height: 55px;
}

@media (max-width: 768px) {
    .list-column {
        -webkit-columns: 2 !important;
        -moz-columns: 2 !important;
        columns: 2 !important;
        padding-left: 0;
        list-style: square;
    }
}

.list-column {
    -webkit-columns: 3;
    -moz-columns: 3;
    columns: 3;
    padding-left: 0;
    list-style: square;
}

.list-column li {
    list-style-position: inside;
    -webkit-column-break-inside: avoid;
    page-break-inside: avoid;
    break-inside: avoid;
    font-size: 0.8rem;
    font-weight: 500;
}
</style>
<script>
	function shownotify(msg)
	{
		$.notify({icon:"add_alert",message:msg},{type:'warning',timer:3e3,placement:{from:'top',align:'right'}})
	}
</script>
<?php
	if($this->session->flashdata('error')!=""){
		echo '<script>shownotify(`'.$this->session->flashdata('error').'`);</script>';
	}
?>
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="prog-wrapper">
					<nav id="prog-sidebar">
						<ul class="list-unstyled prog_components">
							<li>
								<a href="javascript:;" data-toggle="modal" data-target="#liveClass" class="prog-link" title="Live Class">
									<img src="<?php echo base_url(); ?>assets/img/icons/online-class.png" class="icon-logo"/> 
									<span>Live Class</span>
								</a>
							</li>
							<li>
								<a href="<?php echo base_url('Calendar/studentScheduleView'); ?>" target="_blank" class="prog-link" title="Schedule Class">
									<img src="<?php echo base_url(); ?>assets/img/icons/schedule.png" class="icon-logo"/> 
									<span>Schedule Class</span>
								</a>
							</li>
							<!--<li>
								<a href="javascript:getProgMenuContents('cmodule');" class="prog-link" title="Modules">
									<img src="<?php //echo base_url(); ?>assets/img/icons/schedule.png" class="icon-logo"/> 
									<span>Modules</span>
								</a>
							</li>-->
							<li>
								<a href="javascript:getProgMenuContents('clectures');" class="prog-link">
									<img src="<?php echo base_url(); ?>assets/img/icons/lecture.png" class="icon-logo"/> 
									<span>Lectures</span>
								</a>
							</li>
							<li>
								<a href="javascript:getProgMenuContents('cresources');" class="prog-link">
									<img src="<?php echo base_url(); ?>assets/img/icons/resources.png" class="icon-logo"/> 
									<span>Resources</span>
								</a>
							</li>
							<li>
								<a href="javascript:getProgMenuContents('cassignments');" class="prog-link">
									<img src="<?php echo base_url(); ?>assets/img/icons/assignment.png" class="icon-logo"/> 
									<span>Assignments</span>
								</a>
							</li>
							<li>
								<a href="javascript:;" class="prog-link">
									<img src="<?php echo base_url(); ?>assets/img/icons/assignment.png" class="icon-logo"/> 
									<span>Grade Sheet</span>
								</a>
							</li>
							<li>
								<a href="javascript:getProgMenuContents('cteachers');;" class="prog-link" title="Teachers">
									<img src="<?php echo base_url(); ?>assets/img/icons/teacher.png" class="icon-logo"/>
									<span>Teachers</span>
								</a>
							</li>
							<li>
								<a href="javascript:getProgMenuContents('corg');" class="prog-link" title="Organisation">
									<img src="<?php echo base_url(); ?>assets/img/icons/schedule.png" class="icon-logo"/>
									<span>Organisation</span>
								</a>
							</li>
						</ul>
					</nav>
					<div id="prog-content">
						<nav class="navbar navbar-expand-lg" style="background:transparent !important;">
							<div class="container-fluid">
								<h4 class="card-title w-100">
									<a href="javascript:;" id="progSidebarCollapse" class="btn btn-sm btn-primary"><i class="fa fa-bars"></i></a> 
									<?php echo trim($prog[0]->title); ?>
									<a href="<?php echo base_url('programDetails/?id='.base64_encode($prog_id)); ?>" target="_blank" class="pull-right" title="Web" rel="tooltip"><i class="material-icons">language</i></a>
									<a href="javascript:getProgMenuContents('home');" class="pull-right mr-3 text-info" title="Home" rel="tooltip"><i class="material-icons">home</i></a>
								</h4>
							</div>
						</nav>
						<div class="progress" id="progress" style="display:none;">
							<div class="progress-bar progress-bar-striped progress-bar-animated" id="progbra"></div>
						</div>
						<div class="row px-3" id="pcontent">

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url(); ?>assets/js/plugins/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {
		$("#progSidebarCollapse").on('click',function() {
			$("#prog-sidebar").toggleClass('active');
		  });
		getProgMenuContents('home');
	});
	function getProgMenuContents(menu)
	{
		$('#pcontent').html("", 1000);
		$('#progbra').css('width', '0%');
		$('#progress').show();
		$.ajax({
			xhr: function () {
				var xhr = new window.XMLHttpRequest();
				xhr.upload.addEventListener("progress", function (evt) {
					if (evt.lengthComputable) {
						var percentComplete = evt.loaded / evt.total;
						console.log(percentComplete);
						$('#progbra').css({
							width: percentComplete * 100 + '%'
						});
						if (percentComplete === 1) {
							$('#progbra').addClass('hide');
						}
					}
				}, false);
				xhr.addEventListener("progress", function (evt) {
					if (evt.lengthComputable) {
						var percentComplete = evt.loaded / evt.total;
						console.log(percentComplete);
						$('#progbra').css({
							width: percentComplete * 100 + '%'
						});
					}
				}, false);
				return xhr;
			},
			type: 'GET',
			url: baseURL+"Student/getProgMenuContent",
			data: {page: menu, prog_id: <?php echo $prog_id; ?>},
			success: function(data){
				$('#progress').hide();
				$('#pcontent').html(data);
				$('#pcontent').fadeIn(2000);
			}
		});
	}
</script>