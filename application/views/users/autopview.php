<div class="align-self-center">
  <ul class="list-group list-group-horizontal">
		<?php
			if($status=='0'){
				echo '<li class="list-group-item"><a href="'.base_url('Student/startAutoProgram/?id='.base64_encode($id).'&title='.base64_encode($title)).'" class="btn btn-link btn-success">Start your program</a></li>';
			}else if($status=='1'){
				echo '<li class="list-group-item"><a href="'.base_url('Student/progressLesson/'.preg_replace("/[^\p{L}\p{N}]/u","_",strtolower($title)).'/'.$id).'"><i class="material-icons text-primary" style="font-size:2rem;">search</i></a></li>';
			}else if($status=='2'){
				echo '<li class="list-group-item">Download Certificate</li>';
			}
		?>
	</ul>
</div>