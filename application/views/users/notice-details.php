<div class="content">
	<div class="container-fluid">
		<div class="row">
		
			<div class="col-lg-10 col-md-10 mx-auto">
				<div class="card">
					<div class="card-header card-header-info">
						<h3 class="card-title">Notice: <?php echo $ndetails[0]->title; ?></h3>
					</div>
					
					<div class="card-body text-justify">
						<?php echo $ndetails[0]->details; ?>
					</div>
					<div class="card-footer">
						<small class="pull-right"><?php echo date('jS M Y',strtotime($ndetails[0]->add_date)); ?></small>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>