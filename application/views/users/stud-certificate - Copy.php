<style> 
.certificate{
    min-width: none;
    max-width: 1000px;
    height: auto;
    margin: 0 auto;
    position: relative;
	border: 1px solid #000;
}
.certificate-body{
    box-shadow: 2px 5px 5px #000000;
    width:  100%;
    height: auto; 
}

.certificate-text{
    top: 35%;
    position: absolute;
    width: 80%;
    left:11%;
    /* right: 20%; */
    text-align: left;
    font-weight: bold;
}
.certificate-info{
    top: 42%;
    position: absolute;
    width: 80%;
    left:14%;
    /* right: 20%; */
    text-align: left;
    font-weight: bold;   
}
.stud-name{
    color: #15bacf;
}
.stud-grade{
    color: #ff6223;
}
.stud-prog{
    color: #ff6223;
    font-style: italic;
}
</style>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins/html2pdf.bundle.min.js"></script>
<div class="content">
    <div class="container-fluid">
        <div class="row"> 
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-info">
                        <h3 class="card-title">Certificate Of Completion</h3>
                    </div>
                    <div class="card-body">
                        <div class="row"> 
                            <div class="col-lg-12 col-md-12">
                                <div class="certificate" id="certificate">
                                    <img src="<?php echo base_url('assets/img/certificate.jpg');?>" alt="" class="certificate-body">
                                    <p class="certificate-text"> 
                                        It is certify that <span class="stud-name"><?php echo $student_name;?></span> has successfully completed <span class="stud-prog"><?php echo $programs[0]->title;?></span> in our company from  '<?php echo (!empty($timestmp['start_date']))?$timestmp['start_date']: '';?>' to  '<?php echo (!empty($timestmp['end_date']))?$timestmp['end_date']: '';?>'.
                                    </p>
                                    <p class="certificate-info"> 
                                        During the training her overall performance is <span class="stud-grade">"Good"</span>. The performance details is stated below.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <button class="btn btn-primary btn-sm" id="downloadCertificate">Download as PDF</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script> 
    window.onload = function(){
        document.getElementById("downloadCertificate")
        .addEventListener("click",() => {
            const certificate = this.document.getElementById("certificate");
            // html2pdf().from(certificate).save();
            var opt = {
                margin:       0,
                filename:     'myfile.pdf',
                image:        { type: 'jpeg', quality: 0.98 },
                html2canvas:  { scale: 2 },
                jsPDF:        { unit: 'in', format: 'letter', orientation: 'landscape' }
            };
            html2pdf().from(certificate).set(opt).save();
        })
    }
</script>