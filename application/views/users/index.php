<style>
.icon-logo { width: 35px !important; height: 35px !important; } } .icon-logo { font-size: 20px; color: #123456; padding: 12px 15px; font-family: FontAwesome; width: 55px; height: 55px; } .card-prog .card-description { color: #000 !important; } .card-prog .card-title { color: #000 !important; } .card-prog .card-footer { border-top: 1px solid #eee !important; } .prog-wrapper { display: flex; text-decoration: none; transition: all 0.4s; } #prog-sidebar { min-width: 170px; max-width: 170px; background: #eee; color: #fff; transition: all 0.4s; max-height: 100vh; overflow-y:scroll; -ms-overflow-style: none; /* IE and Edge */ scrollbar-width: none; /* Firefox */ } #prog-sidebar::-webkit-scrollbar { display: none; } #prog-sidebar .prog_components { display: grid; place-items: center; } #prog-sidebar.active { min-width: 0px; max-width: 0px; } #prog-sidebar .sidebar-header { padding: 20px; background: #1b1d24; } #prog-sidebar ul.components { padding: 20px 0; } #prog-sidebar ul li a { display: inline-block; align-items: center; background: transparent; color: #000; font-weight: 500; text-transform: capitalize; } a[data-toggle="collapse"] { position: relative; } #prog-content { width: 100%; min-width: height 100vh; transition: all 0.4s; background-color: #fff; } #prog-content h2 { margin-bottom: 50px; } #prog-content p { text-align: justify; } @media (max-width: 768px) { #prog-sidebar { min-width: 0px !important; max-width: 0px !important; } #prog-sidebar.active { min-width: 170px !important; max-width: 170px !important; } } .loader { background-color: #ffffff; opacity:0.5; position: fixed; z-index: 999999; height: 100%; width: 100%; top: 0; left: 0; } .loader img { position: absolute; top: 50%; left: 50%; text-align: center; -webkit-transform: translate(-50%, -50%); transform: translate(-50%, -50%); }
</style>
<script>
	function shownotify(msg)
	{
		$.notify({icon:"add_alert",message:msg},{type:'warning',timer:3e3,placement:{from:'top',align:'right'}})
	}
</script>
<?php
	if($this->session->flashdata('error')!=""){
		echo '<script>shownotify(`'.$this->session->flashdata('error').'`);</script>';
	}
?>
<div class="loader" id="loading" style="display:none;">
	<img src="<?php echo base_url().'assets/img/loading.gif'; ?>">
</div>
<div class="content px-0">
	<div class="container-fluid p-0">
		<div class="row">
			<div class="col-12">
				<div class="prog-wrapper">
					<nav id="prog-sidebar">
						<ul class="list-unstyled prog_components">
							<li>
								<a href="<?php echo base_url('Student/notices'); ?>" target="_blank" class="btn btn-md btn-primary btn-link">
								<img src="<?php echo base_url(); ?>assets/img/icons/noticeboard.png" class="icon-logo"/><br> Notice Board
								</a>
							</li>
							<li>
								<a href="javascript:;" onClick="getUserData('resume');" class="btn btn-md btn-primary btn-link">
								<img src="<?php echo base_url(); ?>assets/img/icons/resume.png" class="icon-logo"/>
								<br>Resume</a>
							</li>
							<li>
								<a href="<?php echo base_url('Calendar/studentScheduleView'); ?>" target="_blank" class="btn btn-md btn-primary btn-link">
								<img src="<?php echo base_url(); ?>assets/img/icons/schedule.png" class="icon-logo"/>
								<br>Class Schedule</a>
							</li>
							<li>
								<a href="javascript:;" onClick="getUserData('skills');" class="btn btn-md btn-primary btn-link">
								<img src="<?php echo base_url(); ?>assets/img/icons/skills.png" class="icon-logo"/>
								<br>Skills</a>
							</li>
							
							<li>
								<a href="<?php echo base_url('Student/message'); ?>" target="_blank"  class="btn btn-md btn-primary btn-link">
								<img src="<?php echo base_url(); ?>assets/img/icons/chat.png" class="icon-logo"/>
								<br>Messages</a>
							</li>
							<li>
								<a href="<?php echo base_url('uploads/help.pdf'); ?>" target="_blank"  class="btn btn-md btn-primary btn-link">
								<img src="<?php echo base_url(); ?>assets/img/icons/doubts.png" class="icon-logo"/>
								<br>Help</a>
							</li>
							<li>
								<a href="javascript:;" data-toggle="modal" data-target="#passModal" class="btn btn-md btn-primary btn-link">
								<img src="<?php echo base_url(); ?>assets/img/icons/settings.png" class="icon-logo"/>
								<br>Settings</a>
							</li>
						</ul>
					</nav>
					<div id="prog-content">
						<nav class="navbar navbar-expand-lg" style="background:transparent !important;">
							<div class="container-fluid">
								<h4 class="card-title w-100">
									<a href="javascript:;" id="progSidebarCollapse" class="btn btn-sm btn-primary"><i class="fa fa-bars"></i></a> 
									<?php echo $_SESSION['userData']['name']; ?> 's Dashboard
								</h4>
							</div>
						</nav>
						<div class="row px-3" id="pcontent">
							<div class="col-md-9">
								<div id="main_home">
									<div class="row">
										<?php $status=''; $certify = ''; $i=1; if(count($programs)>0){ foreach($programs as $prow){ 
											$paymentdue = -1;
											$id = $prow->id;
											$ptype=intval(trim($prow->type));
											$ft = trim($prow->feetype);
											$bolcertify = $prow->bolcertify;
											$title = trim($prow->title);
											$type = trim($prow->ptype);
											$category = trim($prow->category);
											$dur = intval(trim($prow->duration))*12;
											$st = intval(trim($prow->sem_type));
											$aflag = intval(trim($prow->approve_flag));
											$category = intval($prow->prog_level);
											$progLabel = ($category==3)? 'Certification' : (($category==2)? 'Mentorship' : 'Live Buddy');
											if(!empty(${'spc_'.$i})){
												$status = ${'spc_'.$i}[0]->status;
												$certificate = ${'spc_'.$i}[0]->certificate;
												$tfee = intval(trim(${'spc_'.$i}[0]->totalfees));
												$dis = intval(trim(${'spc_'.$i}[0]->discount));
												$pdone = intval(trim(${'spc_'.$i}[0]->payment_done));
												if($ft=='Paid'){
													if($dis!=0){
														$tfee = $tfee*(1-($dis/100));
													}
													$paymentdue = $tfee-$pdone;
												}
											}
											$viewpurl = ($aflag==2)? base_url('Student/viewProgram/?id='.base64_encode($prow->id)) : '#';
										?>
										<div class="col-sm-4 mb-3">
											<div class="card card-prog mt-2" style="height:12rem;">
												<div class="card-body">
													<h5 class="card-title">
														<a href="<?php echo $viewpurl; ?>" class="text-dark"><?php echo trim($prow->code); ?></a>
														<span class="label label-info pull-right"><?php echo $progLabel; ?></span>
													</h5>
													<div class="card-description">
														<h6 class="card-title font-weight-bold" style="font-size: 0.85rem !important"><a href="<?php echo $viewpurl; ?>"><?php echo $title; ?></a></h6>
														<?php
															if($aflag==0){
																echo '<span class="small text-danger">Application is pending for approval</span>';
															}else if($aflag==1){
																if($ft=='Paid'){
																	if($paymentdue!=-1){
																		if($paymentdue!=0){
																			echo '<span class="small text-danger">Complete the registration,</span>';
																			//echo '<h6 class="text-center"><a href="'.base_url('Student/requestPrograms').'" class="btn btn-sm btn-success btn-link">pay now: <i class="fa fa-inr"></i> '.$paymentdue.'</a></h6>';
																			echo '<span class="small text-center"><button type="button" onClick="getPaymentInfo('.${'spc_'.$i}[0]->sl.', '.$tfee.', '.${'spc_'.$i}[0]->prog_id.', 0)" class="btn btn-warning btn-sm btn-link"> proceed to payment: <i class="fa fa-inr"></i> '.$paymentdue.'</button></span>';
																		}
																	}
																}else{
																	echo '<span class="small text-info">Application is pending for final approval</span>';
																}
																
															} if($aflag==2){
																if($status=='2'){
																	if($bolcertify=='auto'){
																		echo '<span class="small text-center"><a href="'.base_url('Student/studCertificate/?id='.base64_encode($id)).'" target="_blank"><i class="material-icons">cloud_download</i> Download Certificate</a><br></span>';
																	}else if($bolcertify=='manual'){
																		if($certificate!=""){
																			if(file_exists('./'.$certificate)){
																				echo '<span class="small text-center"><a href="'.base_url($certificate).'" target="_blank"><i class="material-icons">cloud_download</i> Download Certificate</a><br></span>';
																			}
																		}
																	}else if($bolcertify=='no'){
																		echo '<span class="small text-center">No certificate.</span>';
																	}
																	
																}
																if($ft=='Paid'){
																	if($paymentdue!=-1){
																		if($paymentdue!=0){
																			if($st==0){
																				if($tfee<5000){
																					$pnow = $tfee;
																				}else{
																					$pnow = $tfee/4;
																				}
																			}else{
																				$pnow = $tfee/($dur/$st);
																			}
																			echo '<strong>Amount Due: Rs. '.($tfee-$pdone).'</strong>';
																			echo '<br><button type="button" onClick="getPaymentInfo('.${'spc_'.$i}[0]->sl.', '.$pnow.', '.${'spc_'.$i}[0]->prog_id.', 1)" class="btn btn-info">Pay Now: '.$pnow.'</button>';
																		}
																		/*if($paymentdue!=0){
																			echo '<h6 class="text-center"><a href="'.base_url('Student/requestPrograms').'" class="btn btn-sm btn-success">Payment Due: Rs. <i class="fa fa-inr"></i> <i class="fa fa-inr"></i> <i class="fa fa-inr"></i> /-</a></h6>';
																		}else{
																			//echo '<h6 class="text-center"><a href="javascript:;" class="btn btn-sm btn-success"><i class="material-icons"></i> Download Invoice</a></h6>';
																			echo 'Payment all complete.';
																		}*/
																	}
																	
																}
															}
														?>
													</div>
												</div>
												<div class="card-footer">
													<div class="stats font-weight-bold">
													  <?php echo ($status=='1')? 'Ongoing' : (($status=='2')? 'Completed' : (($status=='3')? 'Failed' : 'Pending')); ?>
													</div>
													<div class="stats">
														<p class="card-category text-dark"><?php echo $category; ?></p>
													</div>
												</div>
											</div>
										</div>
										<?php $i++; } }  ?>
									</div>
								</div>
								<div id="live_class" style="display:none;">
									<div id="jitsi-container" style="min-height:400px;"></div>
								</div>
							</div>
							<div class="col-md-3 bg-light" style="height:auto;">
								<ul class="list-unstyled prog_components">
									<li id="start_lclass" style="display:none; ">

									</li>
									<li id="stop_lclass" style="display:none;">
										<button class="btn btn-md btn-primary btn-link" onClick="quitClass()">Quit Class</button>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="applyD" title="List of Applied Programs" style="width: 450px;"></div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			  <h4 class="modal-title" id="mheader"></h4>
			  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				<i class="material-icons">clear</i>
			  </button>
			</div>
			<div class="modal-body" id="mbody"> 
			</div>
			<div class="modal-footer">
			  <button type="button" class="btn btn-danger btn-link" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="payModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
	  <div class="modal-content">
		<div class="modal-header">
		  <h4 class="modal-title text-center font-weight-bold">Payment Confirmation</h4>
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			<i class="material-icons">clear</i>
		  </button>
		</div>
		<div class="modal-body">
		
		</div>
	  </div>
	</div>
</div>
<script>
var container = document.querySelector('#jitsi-container');
var api = null;
var meeting_joined = false;
var room_name = '';
var invitation = false;
$(document).ready(function() {
	$("#progSidebarCollapse").on('click',function() {
		$("#prog-sidebar").toggleClass('active');
	  });
	checkUserApplication();
});
function toggleProgCourse(serial)
{
	$('#prog_course_'+serial).toggle('show');
}
function getPaymentInfo(spcid, pamt, prog_id, method)
{
	$('#loading').show();
	$.ajax({
		url: baseURL+'Student/checkPayment',
		type: 'GET',
		data: {
			spcid: spcid,
			pamt: pamt,
			prog_id: prog_id,
			method: method
		},
		success: (res)=>{
			$('#loading').hide();
			$('.modal-body').html(res);
			$('#payModal').modal('show');
		},
		error: (errors)=>{
			console.log(errors);
		}
	});
}
function checkUserApplication()
{
	var dialogBox = '';
	$.ajax({
		url: baseURL+'Student/findUserApply',
		type: 'GET',
		success: (resp)=>{
			var obj = JSON.parse(resp);
			if(obj['status']){
				if(obj['apply_type']=='userProgram'){
					if(obj['apply_status']){
						dialogBox+='<h5 class="text-danger text-center">You have already applied for this program: '+obj['prog_name']+'</h5>';
					}else{
						dialogBox+='<h5>You have less than 5 minutes to enroll into this program: '+obj['prog_name']+'</h5>';
						dialogBox+='<a href="'+baseURL+'Student/programAdmission/?id='+btoa(obj['prog_id'])+'" class="btn btn-success btn-sm">Enroll Now</a>';
					}
				}else if(obj['apply_type']=='userApplyProgram'){
					dialogBox+='<h5 class="text-danger text-center">You have already applied for this program: '+obj['prog_name']+'</h5>';
					dialogBox+='<a href="'+baseURL+'Student/requestPrograms/" class="btn btn-success btn-sm">Check Status Now</a>';
				}
				$('#mbody').html(dialogBox);
				$('#myModal').modal('show');
			}
		}
	});
}
function getUserData(dtype)
{
	var oricon = '';
	if(dtype=='resume'){
		$('#ricon').html('<i class="fa fa-spinner" aria-hidden="true"></i>');
		$('#mheader').html('User Resume');
		oricon = '<i class="material-icons">verified</i>';
	}else if(dtype=='skills'){
		$('#sicon').html('<i class="fa fa-spinner" aria-hidden="true"></i>');
		$('#mheader').html('User Skills');
		oricon = '<i class="material-icons">bar_chart</i>';
	}
	$.ajax({
		url: baseURL+'Student/getUserValidData',
		type: 'GET',
		data: { dtype: dtype },
		success: (resp)=>{
			console.log(resp)
			$('#mbody').html(resp);
			$('#myModal').modal('show');
			if(dtype=='resume'){
				$('#ricon').html('<i class="material-icons">verified</i>');
			}else if(dtype=='skills'){
				$('#sicon').html('<i class="material-icons">bar_chart</i>');
			}
		}
	});
}

setInterval(() => {
	$.ajax({
		url: baseURL+'Liveclass/getAllMyInvitation',
		type: 'GET',
		data: {user_id: <?=$userid;?>},
		success: (res)=>{ 			
			var obj = JSON.parse(res);
			if(obj['status'] == 'success'){
				let buttons = "";
				for (let i = 0; i < obj['data'].length; i++) {
					const element = obj['data'][i];
					buttons += '<button class="btn btn-primary btn-link btn-md" id="'+element['room_name']+'" ';
					buttons += 'data-room="'+element['room_name']+'" ';
					buttons += 'data-lc_id="'+element['live_class_id']+'" ';
					buttons += 'onClick="joinClass(\''+element['room_name']+'\')"';
					buttons += '><img src="<?php echo base_url(); ?>assets/img/icons/online-class.png" class="icon-logo"/><br> Join Class ('+element['c_code']+')</button>';					
				}
				//$('#lvcdeails').attr('href', baseURL+'Student/courseDetails/?cid='+btoa(element['course_id'])+'&prog='+btoa());
				$('#start_lclass').html(buttons);
				$('#start_lclass').fadeIn(500);
			}	
			if(obj['status'] == 'error'){
				if(meeting_joined == true){
					api.dispose();
					$('#start_lclass').html("");
					$('#start_lclass').fadeOut(500);
					$('div#live_class').fadeOut(500);
					$('div#main_home').fadeIn(500);
				}
			}		
		},
		error: (errors)=>{
			console.log(errors);
		}
	});
}, 5000);

function joinClass(roomName){
	$('#start_lclass').fadeOut(500);
	$('#stop_lclass').fadeIn(500);
	let lc_id = $('#'+roomName).data('lc_id');
	$.ajax({
		url: baseURL+'Liveclass/joinMeeting',
		type: 'POST',
		data: {lc_id: lc_id},
		success: (res)=>{ 			
			var obj = JSON.parse(res);
			if(obj['status'] == 'success'){
				console.log(obj['msg']);
				var domain = "meet.jit.si";
				var options = {
					"roomName": roomName,
					"parentNode": container,
					"width": 100+'%',
					"height": 600,
				};
				configOverwrite = {
					prejoinPageEnabled: false
				};
				options['configOverwrite'] = configOverwrite;
				api = new JitsiMeetExternalAPI(domain, options);
				$('div#main_home').fadeOut(500);
				$('div#live_class').fadeIn(500);
				meeting_joined = true;
			}else{
				$('#start_lclass').fadeIn(500);
				$('#stop_lclass').fadeOut(500);
			}	
		},
		error: (errors)=>{
			console.log(errors);
		}
	});
}
function quitClass(){
	api.dispose();
	$('div#live_class').fadeOut(500);
	$('div#main_home').fadeIn(500);
	$('#stop_lclass').fadeOut(500);
}
</script>
<script src='https://meet.jit.si/external_api.js'></script>