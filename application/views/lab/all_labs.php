<style>
.lab-logo {
    width: auto;
    max-height: 150px;
}
</style>

<div class="content">
    <div class="card">
        <div class="card-header card-header-info">
            <h3 class="card-title">All Labs</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <?php foreach ($labs as $lab) { ?>
                <div class="col-md-3">
                    <div class="card card-raised">
                        <div class="card-body">
                            <div class="text-center">
                                <img class="card-img-top lab-logo" src="<?=$lab->image?>"
                                    alt="<?php echo $lab->name ?>">
                            </div>
                            <h4 class="text-center"><?php echo $lab->name ?></h4>
                            <p class="card-description">
                                <?=substr($lab->introduction, 0,80)."..." ?>
                            </p>
                            <div class="text-center"><a href="<?php echo base_url('Lab/?id='.$lab->id); ?>"
                                    class="btn btn-primary btn-round">View Details</a></div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>