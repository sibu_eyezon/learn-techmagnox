<style>
.dropdown.bootstrap-select {
    width: 100% !important;
}
</style>
<link href="<?php echo base_url(); ?>assets/codemirror/lib/codemirror.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/codemirror/lib/codemirror.js"></script>
<div class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header card-header-info">
                <h3 class="card-title">Lab Name: <?=$lab->name ?></h3>
            </div>
            <div class="card-body">
                <div class="row">
					<!--<div class="col-md-8">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Source Code</h4>
								<?php //$key = array_search($lab->prog_language,$compilers) ?>
								<p class="category">Language: <?//=$key?></p>
							</div>
							<div class="card-body">
								<!-- <div class="codearea"> 
									<textarea name="code" id="code"></textarea>
								<!-- </div> 
								<button type="button" id="btnRun" onClick="run(this)"
									class="btn btn-primary">Run</button>
									<button type="button" id="btnReset" onClick="window.reload.location();"
									class="btn btn-warning">Refresh</button>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Input</h4>
							</div>
							<div class="card-body">
								<div class="codearea">
									<textarea name="input" id="input" class="form-control" rows="12"></textarea>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Result</h4>
							</div>
							<div class="card-body">
								<div class="codearea">
									<textarea name="output" id="output" class="form-control" rows="12"></textarea>
								</div>
							</div>
						</div>
					</div>-->
                    <div class="col-md-12">
                        <ul class="nav nav-pills nav-pills-info justify-content-center" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#source-tab" role="tablist">Source Code
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#input-tab" role="tablist">Input
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#output-tab" role="tablist">Output
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content tab-space tab-subcategories">
                            <div class="tab-pane active" id="source-tab">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Source Code</h4>
                                        <?php $key = array_search($lab->prog_language,$compilers) ?>
                                        <p class="category">Language: <?=$key?></p>
                                    </div>
                                    <div class="card-body">
                                        <!-- <div class="codearea">-->
                                            <textarea name="code" id="code"></textarea>
                                        <!-- </div> -->
                                        <button type="button" id="btnRun" onClick="run(this)"
                                            class="btn btn-primary">Run</button>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="input-tab">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Input</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="codearea">
                                            <textarea name="input" id="input" class="form-control" rows="12"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="output-tab">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Result</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="codearea">
                                            <textarea name="output" id="output" class="form-control" rows="12"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if($lab->handson == true) {?>
                    

                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
var codeEditor = null;
var inputEditor = null;
// var outputEditor = null;
$(document).ready(function(){
    codeEditor  = CodeMirror.fromTextArea(document.getElementById('code'),{
        lineNumbers: true
    });
    codeEditor.refresh();
    inputEditor  = CodeMirror.fromTextArea(document.getElementById('input'),{
        lineNumbers: true
    });
    inputEditor.refresh();
    // outputEditor  = CodeMirror.fromTextArea(document.getElementById('output'),{
    //     lineNumbers: true
    // });
    // outputEditor.refresh();
})

var interval;
var running = false;

function getResult(id) {
    $.ajax({
        url: baseURL + 'Lab/getSubmission',
        type: 'POST',
        data: {
            id: id
        },
        success: (res) => {
            var data = JSON.parse(res);
            if (data['status'] == 'success') {
                console.log(data['response']);

                // submitId = data['response']['id'];
                // output.html("Code submitted successfully");
                if (data['response']['executing'] == true) {
                    if (running == false) {
                        running = true;
                        $('#output').append("Executing...\r\n");
                    }
                } else {
                    clearInterval(interval);
                    $('#output').append("Execution completed\r\n");
                    getSubmissionStream(id, 'output');
                }

            } else {
                console.log("error: " + data['error_code']);
            }
        },
        error: (errors) => {
            console.log(errors);
        }
    });
}

function getSubmissionStream(id, stream) {
    $.ajax({
        url: baseURL + 'Lab/getSubmissionStream',
        type: 'POST',
        data: {
            id: id,
            stream: stream
        },
        success: (res) => {
            var data = JSON.parse(res);
            if (data['status'] == 'success') {
                console.log(data['response']);
                $('#output').append(data['response']);
                $('#btnRun').prop('disabled', false);
            } else {
                console.log("error: " + data['error_code']);
				$('#btnRun').prop('disabled', false);
            }
        },
        error: (errors) => {
            console.log(errors);
        }
    });
}

function run(element) {
    $(element).prop('disabled', true);
    // var code = $('#code').val();
    var code = codeEditor.getValue();
    // var input = $('#input').val();
    var input = inputEditor.getValue();
    var output = $('#output');
    var submitId = "";
    running = false;
    output.html("");
    $.ajax({
        url: baseURL + 'Lab/submitCode',
        type: 'POST',
        data: {
            src: code,
            input: input,
            compiler: '<?=$lab->prog_language?>',
            lab_clone_id: '<?=$lab_clone_id?>'
        },
        success: (res) => {
            var data = JSON.parse(res);
            if (data['status'] == 'success') {
                //console.log(data['response']);
                submitId = data['response']['id'];
                output.html("Code submitted successfully\r\n");
                // outputEditor.setValue("Code submitted successfully\r\n");
                interval = setInterval(() => {
                    getResult(submitId);
                }, 2000);
            } else {
                console.log("error: " + data['error_code']);
            }
        },
        error: (errors) => {
            console.log(errors);
        }
    });
}
</script>