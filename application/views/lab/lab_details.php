<style>
.dropdown.bootstrap-select {
	width:100% !important;
}
</style>

<div class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header card-header-info">
                <h3 class="card-title">Lab Name: <?=$lab->name ?></h3>
            </div>
            <div class="card-body">
                <div class="text-right">
                    <button class="btn btn-primary btn-round" onClick="showCloneModal()">Clone This Lab</button>
                </div>
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="text-center"><img width="200px;" class="image-fluid" src="<?=$lab->image?>"
                                alt="<?=$lab->name ?>"></div>
                        <h3 class="display-4">Introduction</h3>
                        <p style="text-align:justify" class="lead"><?=$lab->introduction?></p>
                    </div>
                    <div class="col-md-10 offset-md-1">
                        <h3 class="display-4">Objective</h3>
                        <p style="text-align:justify" class="lead"><?=$lab->objective?></p>
                    </div>
                    <div class="col-md-10 offset-md-1">
                        <h3 class="display-4">Instruction</h3>
                        <p style="text-align:justify" class="lead"><?=$lab->instruction?></p>
                    </div>
                    <div class="col-md-10 offset-md-1 text-center">
                        <?=$lab->video_details?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="lab_clone_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Clone Lab</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="material-icons">clear</i>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group mb-0">
                    <select name="prog" id="prog" class="selectpicker" data-style="select-with-transition"
                        title="Select a program*" required="true">
                        <?php foreach ($programs as $prog) { ?>
                        <option value="<?=$prog->id?>"><?=$prog->title?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onClick="cloneLab(<?=$lab->id?>)">Save changes</button>
            </div>
        </div>
    </div>
</div>

<script>
function showCloneModal(){
    $('#lab_clone_modal').modal('show');
}
function cloneLab(id) {
    var prog = $('select#prog').val();
    if(prog == ''){
        $.notify({icon:"add_alert",message:"Select a program"},{type:'warning',timer:3e3,placement:{from:'top',align:'right'}})
    }else{
        $.ajax({
			url: baseURL+'Lab/cloneLab',
			type: 'POST',
			data: {prog_id: prog, lab_id: id},
			success: (res)=>{ 			
				var obj = JSON.parse(res);
				if(obj['status'] == 'success'){ 
					$.notify({icon:"add_alert",message:"Lab cloned successfully"},{type:'success',timer:3e3,placement:{from:'top',align:'right'}})
				}else if(obj['status'] == 'error'){
                    $.notify({icon:"add_alert",message:obj['msg']},{type:'danger',timer:3e3,placement:{from:'top',align:'right'}})
                }	
                $('#lab_clone_modal').modal('hide');
			},
			error: (errors)=>{
				console.log(errors);
			}
		});        
    }
    
}
</script>