<div class="row">
	<div class="col-md-12 col-sm-12">
        <table class="table table-bordered" id="table_project" data-id="<?php echo $page_number;?>">
            <thead>
				<tr>
                    <?php echo ($utype === 'admin')?'<th scope="col"><input type="checkbox" id="allCheck" onChange="isAllChecked()"></th>':'';?>
                    <th scope="col" class="text-center font-weight-bold">Sl/No.</th>
					<th scope="col" class="text-center font-weight-bold">Title</th>
					<th scope="col" class="text-center font-weight-bold">Project</th>
					<th scope="col" class="text-center font-weight-bold">Status</th>
					<th scope="col" class="text-center font-weight-bold" colspan="<?php echo ($utype === 'admin')?2:1;?>">Action</th>
				</tr>
			</thead>
            <tbody>
            <?php
                $num = $slNo;
                if(!empty($result)):
                    foreach($result as $row):
            ?>
                <tr>
                    <?php echo ($utype === 'admin')?'<td><input type="checkbox" class="checkBox" data-id="'.$row->id.'" value="'.$row->yn_valid.'" id="checkBox'.$num.'" onChange="isSingleChecked('.$num.')"></td>' : '';?>
                    <th scope="row text-center"><?php echo $num;?></th>
                    <td class="text-center"><?php echo $row->txt_project;?></td>
                    <td class="text-justify"><?php echo $row->txt_project_dtls;?></td>
                    <td class="text-center">
                        <?php echo ($row->yn_valid == 1)? '<span class="p-1 h6 label label-success">Active</span>' : '<span class="p-1 h6 label label-danger">Inactive</span>' ;?>
                    </td>
                    <td class="text-center">
                        <button class="btn btn-primary btn-sm" onclick="addEditModal('project',<?php echo $row->id;?>, <?php echo $row->program_id;?>, <?php echo $page_number;?>)"><span class="material-icons">edit</span></button>
                    </td>
                    <?php
                        if($utype === 'admin'){
                            echo '
                                    <td>
                                        <button class="btn btn-warning btn-sm" onclick="singleStateChange(`project`, '.$row->id.', '.$row->yn_valid.')">
                                            <span class="material-icons">published_with_changes</span>
                                        </button>
                                    </td>
                            ';
                        }
                    ?>
                </tr>
            <?php
                    $num++;
                    endforeach;
                endif;
            ?>
            </tbody>
        </table>
    </div>
</div>

<div id="pagination_<?php echo $job;?>"></div>

<script>
    $(document).ready(function(){
        getPagination();
    });

    function getPagination(){
        let count        = '<?php echo count($result);?>';
        let page_number  = '<?php echo $page_number;?>';
        let last_page    = '<?php echo $last_page;?>';
        let job          = '<?php echo $job;?>';
        $.ajax({
            url: baseURL + 'Operation/get_pagination',
            type: 'POST',
            data:{
                count       : count,
                page_number : page_number,
                last_page   : last_page,
                func        : 'getProjectList'
            },
            success: function(data){
                let selector = `#pagination_${job}`;
                $(selector).html(data);
            }
        })
    }
</script>