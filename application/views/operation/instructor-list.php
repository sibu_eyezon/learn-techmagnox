<div class="row">
	<div class="col-md-12 col-sm-12">
        <table class="table table-bordered" id="table_instructor" data-count="<?php echo count($result);?>" data-id="<?php echo $page_number;?>" data-end="<?php echo $last_page;?>" data-job="<?php echo $job;?>">
            <thead>
                <tr>
                    <th scope="col"><input type="checkbox" id="allCheck" onChange="isAllChecked()"></th>
                    <th scope="col" class="text-center font-weight-bold">Sl/No.</th>
					<th scope="col" class="text-center font-weight-bold">Image</th>
                    <th scope="col" class="text-center font-weight-bold">Basic Info</th>
					<th scope="col" class="text-center font-weight-bold">Role</th>
					<th scope="col" class="text-center font-weight-bold">Designation</th>
                    <th scope="col" class="text-center font-weight-bold">Details</th>
                    <th scope="col" class="text-center font-weight-bold">Status</th>
                    <th scope="col" class="text-center font-weight-bold" colspan="2">Action</th>
                </tr>
            </thead>
            <tbody>
            <?php
                $num = $slNo;
                if(!empty($result)):
                    foreach($result as $row):
            ?>
                <tr>
                    <td>
                        <input type="checkbox" class="checkBox" data-id="<?php echo $row->id;?>" value="<?php echo $row->txt_active_flag?>" id="checkBox<?php echo $num; ?>" onChange="isSingleChecked(<?php echo $num; ?>)">
                    </td>
                    <td class="text-center"><?php echo $num;?></td>
                    <td class="text-center">
                        <img src="<?php echo base_url($row->txt_profile_pic);?>" onerror="src='<?php echo base_url('assets/img/default-avatar.png');?>'" class="img-fluid pull-left mr-2" width="100" >
                    </td>
                    <td class="text-center">
                        <strong>
                            <?php echo $row->txt_instructor_name;?>
                        </strong><br/>
                        <span>
                            <?php echo $row->txt_emailid;?>
                        </span><br/>
                        <span>
                            <?php echo $row->num_phone_no;?>
                        </span>
                    </td>
                    <td class="text-center"><?php echo $row->txt_instructor_role;?></td>
                    <td class="text-center"><?php echo $row->txt_instructor_desg;?></td>
                    <td class="text-justify"><?php echo $row->txt_instructor_dtls;?></td>
                    <td class="text-center">
                        <?php echo ($row->txt_active_flag === "Y")? '<span class="p-1 h6 label label-success">Active</span>' : '<span class="p-1 h6 label label-danger">Inactive</span>' ;?>
                    </td>
                    <td class="text-center">
                        <button class="btn btn-primary btn-sm" onclick="addEditModal('instructor',<?php echo $row->id;?>, <?php echo $page_number;?>)"><span class="material-icons">edit</span></button>
                    </td>
                    <td>
                        <button class="btn btn-warning btn-sm" onclick="singleStateChange('instructor', <?php echo $row->id;?>, `<?php echo $row->txt_active_flag;?>`)">
                            <span class="material-icons">published_with_changes</span>
                        </button>
                    </td>
                </tr>
            <?php
                    $num++;
                    endforeach;
                endif;
            ?>
            </tbody>
        </table>
    </div>
</div>



<div id="pagination_<?php echo $job;?>"></div>

<script>
    $(document).ready(function(){
        getPagination();
    });

    function getPagination(){
        let count        = parseInt($('#table_instructor').attr('data-count'));
        let page_number  = parseInt($('#table_instructor').attr('data-id'));
        let last_page    = parseInt($('#table_instructor').attr('data-end'));
        let job          = $('#table_instructor').attr('data-job');
        $.ajax({
            url: baseURL + 'Operation/get_pagination',
            type: 'POST',
            data:{
                count       : count,
                page_number : page_number,
                last_page   : last_page,
                func        : 'getInstructorList'
            },
            success: function(data){
                let selector = `#pagination_${job}`;
                $(selector).html(data);
            }
        })
    }
</script>