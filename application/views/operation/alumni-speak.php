<div class="row">
	<div class="col-md-12 col-sm-12">
        <table class="table table-bordered" id="table_speak" data-id="<?php echo $page_number;?>">
            <thead>
				<tr>
                    <th scope="col"><input type="checkbox" id="allCheck" onChange="isAllChecked()"></th>
					<th scope="col" class="text-center font-weight-bold">Sl/No.</th>
					<th scope="col" class="text-center font-weight-bold">Image</th>
					<th scope="col" class="text-center font-weight-bold">Name</th>
					<th scope="col" class="text-center font-weight-bold">Email</th>
					<th scope="col" class="text-center font-weight-bold">Phone</th>
					<th scope="col" class="text-center font-weight-bold">Position</th>
					<th scope="col" class="text-center font-weight-bold">Organization</th>
					<th scope="col" class="text-center font-weight-bold">Message</th>
					<th scope="col" class="text-center font-weight-bold">Status</th>
					<th scope="col" class="text-center font-weight-bold" colspan="2">Action</th>
				</tr>
			</thead>
            <tbody>
            <?php
                $num = $slNo;
                if(!empty($result)):
                    foreach($result as $row):
            ?>
            <!-- <?php echo base_url($row->txt_profile_pic);?> -->
                <tr>
                    <td>
                    <input type="checkbox" class="checkBox" data-id="<?php echo $row->id;?>" value="<?php echo $row->yn_valid;?>" id="checkBox<?php echo $num;?>" onChange="isSingleChecked(<?php echo $num;?>)">
                    </td>
                    <th scope="row"><?php echo $num;?></th>
                    <td class="text-justify"><img src="<?php echo base_url($row->txt_profile_pic);?>" onerror="src='<?php echo base_url('assets/img/default-avatar.png');?>'" style="width:100px"></td>
					<td class="text-justify"><?php echo $row->txt_name;?></td>
					<td class="text-justify"><?php echo $row->txt_emailid;?></td>
					<td class="text-justify"><?php echo $row->num_mobile_no;?></td>
					<td class="text-justify"><?php echo $row->txt_position;?></td>
					<td class="text-justify"><?php echo $row->txt_organization;?></td>
					<td class="text-justify"><?php echo $row->txt_message;?></td>
                    <td class="text-center">
                        <?php echo ($row->yn_valid == 1)? '<span class="p-1 h6 label label-success">Active</span>' : '<span class="p-1 h6 label label-danger">Inactive</span>' ;?>
                    </td>
                    <td class="text-center">
                        <button class="btn btn-primary btn-sm" onclick="addEditModal('speak',<?php echo $row->id;?>, <?php echo $row->program_id;?>, <?php echo $page_number;?>)"><span class="material-icons">edit</span></button>
                    </td>
                    <td>
                        <button class="btn btn-warning btn-sm" onclick="singleStateChange('speak', <?php echo $row->id;?>, <?php echo $row->yn_valid;?>)">
                            <span class="material-icons">published_with_changes</span>
                        </button>
                    </td>
                </tr>
            <?php
                    $num++;
                    endforeach;
                endif;
            ?>
            </tbody>
        </table>
    </div>
</div>

<div id="pagination_<?php echo $job;?>"></div>

<script>
    $(document).ready(function(){
        getPagination();
    });

    function getPagination(){
        let count        = '<?php echo count($result);?>';
        let page_number  = '<?php echo $page_number;?>';
        let last_page    = '<?php echo $last_page;?>';
        let job          = '<?php echo $job;?>';
        $.ajax({
            url: baseURL + 'Operation/get_pagination',
            type: 'POST',
            data:{
                count       : count,
                page_number : page_number,
                last_page   : last_page,
                func        : 'getProjectList'
            },
            success: function(data){
                let selector = `#pagination_${job}`;
                $(selector).html(data);
            }
        })
    }
</script>