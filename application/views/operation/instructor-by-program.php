<thead>
    <th scope="col" class="text-center text-dark font-weight-bold">Sl/No.</th>
    <th scope="col" class="text-center text-dark font-weight-bold">Image</th>
    <th scope="col" class="text-center text-dark font-weight-bold">Name</th>
    <th scope="col" class="text-center text-dark font-weight-bold">Email</th>
    <th scope="col" class="text-center text-dark font-weight-bold">Phone</th>
    <th scope="col" class="text-center text-dark font-weight-bold">Action</th>
</thead>
<tbody>
<?php
                $num = 1;
                if(!empty($result)):
                    foreach($result as $row):
            ?>
                <tr>

                    <td class="text-center text-dark"><?php echo $num;?></td>
                    <td class="text-center text-dark">
                        <img src="<?php echo base_url($row->txt_profile_pic);?>" onerror="src='<?php echo base_url('assets/img/default-avatar.png');?>'" class="img-fluid pull-left mr-2" width="100" >
                    </td>
                    <td class="text-center text-dark"><?php echo $row->txt_instructor_name;?></td>
                    <td class="text-center text-dark"><?php echo $row->txt_emailid;?></td>
                    <td class="text-center text-dark"><?php echo $row->num_phone_no;?></td>
                    <td class="text-center text-dark">
                        <button class="btn btn-primary btn-sm" onclick="delete_instructor_from_program(<?php echo $row->row_id;?>)"><span class="material-icons">edit</span></button>
                    </td>
                </tr>
            <?php
                    $num++;
                    endforeach;
                endif;
            ?>
</tbody>