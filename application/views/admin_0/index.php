<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/croppie/croppie.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/select2/dist/css/select2.min.css" />
<style>
	#upload-demo{
		width: 300px;
		height: 300px;
		padding-bottom:25px;
	}
	/*.modal .modal-dialog .modal-content{
  		width: 850px;
	}*/
</style>
<div class="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="card">
					<div class="card-header card-header-info">
						<h3 class="card-title">Organizations
						<a href="javascript:organizationModal('Add')" class="btn btn-primary btn-sm pull-right"><i class="material-icons">add</i> Add New Organization</a>
						</h3>
					</div>
					<div class="card-body">
						<div id="prog_department" role="tablist">
							<div class="progress-bar progress-bar-striped progress-bar-animated" id="prg_progress" style="width: 100%; display: none;">Loading the list. Please wait...</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- organization -->
<div class="modal fade" id="organizationModal" tabindex="-1" role="dialog" aria-labelledby="organizationModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
    	<div class="modal-content" >
      		<div class="modal-header">
        		<h5 class="modal-title" id="organizationModalLabel"></h5>
      		</div>
      		<div class="modal-body" id="organizationModalBody">

      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-warning btn-sm" data-dismiss="modal">Close</button>
        		<button type="button" class="btn btn-success btn-sm ml-1" onClick="organizationInsertUpdate()">Submit</button>
      		</div>
    	</div>
  	</div>
</div>

<!-- department -->
<div class="modal fade" id="departmentModal" tabindex="-1" role="dialog" aria-labelledby="departmentModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
    	<div class="modal-content" >
      		<div class="modal-header">
        		<h5 class="modal-title" id="departmentModalLabel"></h5>
      		</div>
      		<div class="modal-body" id="departmentModalBody">

      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-warning btn-sm" data-dismiss="modal">Close</button>
        		<button type="button" class="btn btn-success btn-sm ml-1" onClick="departmentInsertUpdate()">Submit</button>
      		</div>
    	</div>
  	</div>
</div>

<!--<div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
		  		<h4 class="modal-title">Edit Photo<br><small class="text-danger">Please do crop else the image won't be saved.</small></h4>
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					<i class="material-icons">clear</i>
		  		</button>
			</div>
			<div class="modal-body">
		  		
			</div>
			<div class="modal-footer">
		  		
			</div>
	  	</div>
	</div>
</div>-->
<script src="<?php echo base_url().'assets/vendor/croppie/croppie.js'; ?>" type="text/javascript"></script>
<script src="<?php echo base_url();?>/assets/js/org-admin.js"></script>
<script>
	var $uploadCrop;
	var tempFilename;
	var rawImg;
	var imageId;
	
	function readFile(input) {
		if (input.files && input.files[0]) {
		  var reader = new FileReader();
			reader.onload = function (e) {
				$('#upload-demo').addClass('ready');
				//$('#cropImagePop').modal('show');
				rawImg = e.target.result;
				$('#cropImagePop').show(()=>{
					uploadBind();
				});
			}
			reader.readAsDataURL(input.files[0]);
		}
		else {
			Swal.fire("Sorry","You're browser doesn't support the FileReader API", "error");
		}
	}
	
	function uploadBind()
	{
		$uploadCrop = $('#upload-demo').croppie({
			viewport: {
				width: 250,
				height: 250,
			},
			enforceBoundary: false,
			enableExif: true
		});
		//alert('Shown pop');
		$uploadCrop.croppie('bind', {
			url: rawImg
		}).then(function(){
			console.log('jQuery bind complete');
		});
	}
	/*$('#cropImagePop').on('show', function(){
		
	});
	$('#cropImageBtn').on('click', function (ev) {
		
	});*/
	
	function cropImageBtn()
	{
		$uploadCrop.croppie('result', {
			type: 'base64',
			format: 'jpeg',
			size: {width: 250, height: 250}
		}).then(function (resp) {crop_img
			$('#wizardPicturePreview').attr('src', resp);
			$('#crop_img').attr('value', resp);
			$('#cropImagePop').hide();
		});
	}

	function avaterOnChange(){
		imageId = document.getElementById('avatar'); 
		readFile(imageId);
	}
</script>
