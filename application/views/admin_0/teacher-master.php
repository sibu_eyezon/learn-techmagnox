<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/croppie/croppie.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/select2/dist/css/select2.min.css" />
<style>
	#upload-demo{
		width: 300px;
		height: 300px;
		padding-bottom:25px;
	}
	/*.modal .modal-dialog .modal-content{
  		width: 850px;
	}*/
</style>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-info">
                        <h3 class="card-title">Teachers</h3>
                    </div>
                    <!-- <div class="card-body">
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12">
                <div class="card"> -->
                    <div class="card-body">
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="material-datatables">
                            <div class="progress-bar progress-bar-striped progress-bar-animated" id="prg_progress" style="width: 100%; display: none;">Loading the list. Please wait...</div>
                            <div id="datatables_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Edite Teacher -->
<div class="modal fade" id="teacherModal" tabindex="-1" role="dialog" aria-labelledby="teacherModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="width:680px">
      <div class="modal-header">
        <h5 class="modal-title" id="teacherModalLabel">Edit Teacher</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal_body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onClick="teacherUpdate();">Save</button>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo base_url().'assets/vendor/croppie/croppie.js'; ?>" type="text/javascript"></script>
<script src="<?php echo base_url();?>/assets/js/org-admin.js"></script>
<script>
	var $uploadCrop;
	var tempFilename;
	var rawImg;
	var imageId;
	
	function readFile(input) {
		if (input.files && input.files[0]) {
		  var reader = new FileReader();
			reader.onload = function (e) {
				$('#upload-demo').addClass('ready');
				//$('#cropImagePop').modal('show');
				rawImg = e.target.result;
				$('#cropImagePop').show(()=>{
					uploadBind();
				});
			}
			reader.readAsDataURL(input.files[0]);
		}
		else {
			Swal.fire("Sorry","You're browser doesn't support the FileReader API", "error");
		}
	}
	
	function uploadBind()
	{
		$uploadCrop = $('#upload-demo').croppie({
			viewport: {
				width: 250,
				height: 250,
			},
			enforceBoundary: false,
			enableExif: true
		});
		//alert('Shown pop');
		$uploadCrop.croppie('bind', {
			url: rawImg
		}).then(function(){
			console.log('jQuery bind complete');
		});
	}
	/*$('#cropImagePop').on('show', function(){
		
	});
	$('#cropImageBtn').on('click', function (ev) {
		
	});*/
	
	function cropImageBtn()
	{
		$uploadCrop.croppie('result', {
			type: 'base64',
			format: 'jpeg',
			size: {width: 250, height: 250}
		}).then(function (resp) {crop_img
			$('#wizardPicturePreview').attr('src', resp);
			$('#crop_img').attr('value', resp);
			$('#cropImagePop').hide();
		});
	}

	function avaterOnChange(){
		imageId = document.getElementById('avatar'); 
		readFile(imageId);
	}
</script>