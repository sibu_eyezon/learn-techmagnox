<style>
    .icon-container {
        position: absolute;
        top: calc(50% - 10px);
    }
    .loader {
        position: relative;
        height: 20px;
        width: 20px;
        display: inline-block;
        animation: around 5.4s infinite;
    }
    @keyframes around {
        0% {
            transform: rotate(0deg)
        }
        100% {
            transform: rotate(360deg)
        }
    }
    .loader::after, .loader::before {
        content: "";
        background: white;
        position: absolute;
        display: inline-block;
        width: 100%;
        height: 100%;
        border-width: 2px;
        border-color: #333 #333 transparent transparent;
        border-style: solid;
        border-radius: 20px;
        box-sizing: border-box;
        top: 0;
        left: 0;
        animation: around 0.7s ease-in-out infinite;
    }
    .loader::after {
        animation: around 0.7s ease-in-out 0.1s infinite;
        background: transparent;
    }
</style>
<div class="preloader" id="loader" style="display: none;">
	<img src="http://localhost/project/swamiv/assets/img/Preloader_3.gif">
</div>
<div class="content">
	<div class="container-fluid">
        <form id="fmPrg" method="post" action="<?php echo base_url();?>Admin/insertEditNewProgram">
            <div class="row">
                <div class="col-lg-8 col-md-8">
                    <div class="card">
                        <div class="card-header card-header-primary card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">create</i>
                            </div>
                            <h4 class="card-title"><?php echo $form_title;?> Program <small class="text-danger">*'s are important</small></h4>
                        </div>
                        <div class="card-body">
                            <!-------------------------------------------------------------------------------->
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="title">Program Title <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="title" name="title" value="<?php echo (!empty($prog->title))? trim($prog->title) : '';?>">
                                        <span id="err_title"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="aca_year">Academic Year <span class="text-danger">*</span></label>
                                        <select class="selectpicker" data-style="select-with-transition" data-title="Select Academic Year" name="aca_year" id="aca_year">
                                        <?php
                                            if(!empty($prog->sl) && !empty($prog->yearnm)){
                                                echo '<option value="'.$prog->sl.'" selected>'.$prog->yearnm.'</option>'; 
                                            }
                                            if(!empty($acd_year)):
                                                foreach($acd_year as $data):
                                                    if($data->sl == $prog->sl):
                                                        continue;
                                                    endif;
                                                    echo '<option value="'.$data->sl.'">'.$data->yearnm.'</option>';
                                                endforeach;
                                            endif;
                                        ?>
                                        </select>
                                        <span id="err_aca_year"></span>
                                    </div>
                                </div>
                            </div>
                            <!-------------------------------------------------------------------------------->
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="togglebutton">
                                        <label class="text-dark">
                                        <input type="checkbox" name="canStudent" id="canStudent" value="1" <?php echo (!empty($prog->student_enroll) && $prog->student_enroll)? 'checked' : '';?>>
                                        <span class="toggle"></span>
                                        Can student apply? <span class="text-danger">*</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="togglebutton">
                                        <label class="text-dark">
                                        <input type="checkbox" name="canTeacher" id="canTeacher" value="1" <?php echo (!empty($prog->teacher_enroll) && $prog->teacher_enroll)? 'checked' : '';?>>
                                        <span class="toggle"></span>
                                        Can teacher apply? <span class="text-danger">*</span>
                                        </label>
                                    </div>
                                </div>
						    </div>
                            <!-------------------------------------------------------------------------------->
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="org">Organization <span class="text-danger">*</span></label>
                                        <select class="selectpicker" data-style="select-with-transition" data-title="Select Organization" name="org" id="org" width="300">
                                            
                                        <?php
                                            if(!empty($prog->org_id) && !empty($prog->org_title)){
                                                echo '<option value="'.$prog->org_id.'" selected>'.$prog->org_title.'</option>'; 
                                            }
                                            if(!empty($organization)):
                                                foreach($organization as $org):
                                                    if($org->id == $prog->org_id){
                                                        continue;
                                                    }
                                                    echo '<option value='.$org->id.'>'.$org->title.'</option>';
                                                endforeach;
                                            endif;
                                        ?>
                                        </select>
                                        <span id="err_org"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group"> 
                                        <label for="dept">Department <span class="text-danger">*</span></label>
                                        <div class="icon-container">
                                            <i class="loader"></i>
                                        </div>
                                        <select class="selectpicker" data-style="select-with-transition" data-title="Select The Department" id="dept" name="dept">
                                            <?php
                                                if(!empty($prog->dept_id) && !empty($prog->dept_title)){
                                                    echo '<option value="'.$prog->dept_id.'" selected>'.$prog->dept_title.'</option>';
                                                }
                                                if(!empty($department)){
                                                    foreach($department as $drow){
                                                        if($drow->id == $prog->dept_id){
                                                            continue;
                                                        }
                                                        echo '<option value='.$drow->id.'>'.$drow->title.'</option>';
                                                    }
                                                }
                                            ?>
                                        </select>
                                        <span id="err_dept"></span>
                                    </div>     
                                </div>
                            </div>
                            <!-------------------------------------------------------------------------------->
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="checkbox-radios" id="type">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label text-dark">
                                                    <input class="form-check-input" type="radio" value="1" name="type" id="1" <?php echo (!empty($prog->type) && $prog->type == '1')? 'checked' : '';?>> New Admission
                                                    <span class="form-check-sign">
										                <span class="check"></span>
										            </span>
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label text-dark">
                                                    <input class="form-check-input" type="radio" value="2" name="type" id="2" <?php echo (!empty($prog->type) && $prog->type == '2')? 'checked' : '';?>> Exist Admission
                                                    <span class="form-check-sign">
										                <span class="check"></span>
										            </span>
                                                </label>
                                            </div>
                                        </div>
                                        <span id="err_type"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
								    <div class="form-group">
									    <label for="title">Total Credit</label>
									    <input type="text" class="form-control" id="credit" name="credit" value="<?php echo (!empty($prog->total_credit))? trim($prog->total_credit ): '';?>">
								    </div>
							    </div>
                            </div>
                            <!-------------------------------------------------------------------------------->
                            <div class="row">	
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="sdate">Total seats</label>
                                        <input type="number" name="total_seat" id="total_seat"  class="form-control" value="<?php echo (!empty($prog->total_seat))? trim($prog->total_seat) : '';?>">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="edate">Total Hours</label>
                                        <input type="number" name="total_hrs" id="total_hrs"  class="form-control" value="<?php echo (!empty($prog->prog_hrs))? trim($prog->prog_hrs) : '';?>">
                                    </div>
                                </div>
                            </div>
                            <!-------------------------------------------------------------------------------->
                            <div class="row">	
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="sdate">Start Date <span class="text-danger">*</span></label>
                                        <input type="date" name="sdate" id="sdate"  value="<?php echo (!empty($prog->start_date))? trim($prog->start_date):'';?>" class="form-control">
                                        <span id="err_sdate"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="edate">End Date <span class="text-danger">*</span></label>
                                        <input type="date" name="edate" id="edate" value="<?php echo (!empty($prog->end_date))? trim($prog->end_date):'';?>" class="form-control">
                                        <span id="err_edate"></span>
                                    </div>
                                </div>
                            </div>
                            <!-------------------------------------------------------------------------------->
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <select class="selectpicker" data-style="select-with-transition" name="category" id="category" data-title="Select one category <span class='text-danger'>*</span>">
                                            <option value="Corporate Program" <?php echo(!empty($prog->category) && trim($prog->category)=="Corporate Program")?'selected':'';?>>Corporate Program</option>
                                            <option value="Training Program" <?php echo(!empty($prog->category) && trim($prog->category)=="Training Program")?'selected':'';?>>Training Program</option>
                                            <option value="Degree Program (Graduate)" <?php echo(!empty($prog->category) && trim($prog->category)=="Degree Program (Graduate)")?'selected':'';?>>Degree Program (Graduate)</option>
                                            <option value="Degree Program (Post Graduate)" <?php echo(!empty($prog->category) && trim($prog->category)=="Degree Program (Post Graduate)")?'selected':'';?>>Degree Program (Post Graduate)</option>
                                            <option value="Diploma Program" <?php echo(!empty($prog->category) && trim($prog->category)=="Diploma Program")?'selected':'';?>>Diploma Program</option>
                                            <option value="Tutor" <?php echo(!empty($prog->category) && trim($prog->category)=="Tutor")?'selected':'';?>>Tutor</option>
                                        </select>
                                        <span id="err_category"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <select class="selectpicker" data-style="select-with-transition" name="prog_type" id="prog_type" data-title="Select one type <span class='text-danger'>*</span>">
                                            <option value="Online" <?php echo (!empty($prog->ptype) && trim($prog->ptype) == 'Online') ? 'selected' : '';?>>Online Program</option>
                                            <option value="Regular" <?php echo (!empty($prog->ptype) && trim($prog->ptype) == 'Regular') ? 'selected' : '';?>>Regular Program</option>
                                            <option value="Distance" <?php echo (!empty($prog->ptype) && trim($prog->ptype) == 'Distance') ? 'selected' : '';?>>Distance Program</option>
                                        </select>
                                        <span id="err_prog_type"></span>
                                    </div>
                                </div>
                            </div>
                            <!-------------------------------------------------------------------------------->
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="title">Program Code <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="pcode" name="pcode" value="<?php echo(!empty($prog->code))? trim($prog->code) : '' ;?>">
                                        <span id="err_pcode"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="duration">Duration <span class="text-danger">*</span></label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <input type="text" name="duration" id="duration" class="form-control" value="<?php echo(!empty($prog->duration))? trim($prog->duration) : '' ;?>" <?php echo(!empty($prog->duration))? 'readonly' : '' ;?>>
                                                <select class="custom-select custom-select-sm" name="dtype" id="dtype" data-title="Duration" <?php echo(!empty($prog->dtype))? 'disabled' : '' ;?>>
                                                    <option value="">Duration <span class="text-danger">*</span></option>
                                                    <option value="day" <?php echo(!empty($prog->dtype) && trim($prog->dtype)== 'day')? 'selected' : '';?>>Days</option>
                                                    <option value="month" <?php echo(!empty($prog->dtype) && trim($prog->dtype)== 'month')? 'selected' : '';?>>Months</option>
                                                    <option value="year" <?php echo(!empty($prog->dtype) && trim($prog->dtype)== 'year')? 'selected' : '';?>>Years</option>
                                                </select>
                                            </div>
                                            <span id="err_duration"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-------------------------------------------------------------------------------->
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="checkbox-radios" id="fee">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label fees text-dark">
                                                    <input class="form-check-input" type="radio" value="Free" name="feetype" id="free" <?php echo (!empty($prog->feetype) && trim($prog->feetype) == "Free") ? 'checked' : '';?>> Free
                                                    <span class="form-check-sign">
                                                    <span class="check"></span>
                                                    </span>
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label fees text-dark">
                                                    <input class="form-check-input" type="radio" value="Paid" name="feetype" id="paid" <?php echo (!empty($prog->feetype) && trim($prog->feetype) == "Paid") ? 'checked' : '';?>> Paid
                                                    <span class="form-check-sign">
                                                    <span class="check"></span>
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                        <span id="err_feetype"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6 d-flex">
                                    <div class="form-group mr-2">
                                        <label for="fees">Total Fees</label>
                                        <input type="text" name="fees" id="fees" class="form-control" value="<?php echo (!empty($prog->total_fee))? trim($prog->total_fee) : '';?>"  disabled>
                                        <span id="err_fees"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="fdetails">Discount (If any)</label>
                                        <input type="number" name="rebate" id="rebate" class="form-control" value="<?php echo (!empty($prog->discount))? trim($prog->discount) : '';?>" disabled>
                                        <span id="err_rebate"></span>
                                    </div>
                                </div>
                            </div>
                            <!-------------------------------------------------------------------------------->
                            <div class="row mb-3">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="overview">Overview</label><br>
                                        <textarea name="overview" id="overview" class="form-control ckeditor" cols="80" rows="5"><?php echo (!empty($prog->overview))? $prog->overview : '';?></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-------------------------------------------------------------------------------->
                            <div class="justify-content-center text-center">
                                <button type="submit" class="btn btn-primary btn-md pull-right" id="program_submit">submit</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Contacts <small class="text-danger">*</small></h4>
                            <div class="form-group">
                                <label for="email">Email <span class="text-danger">*</span></label>
                                <input type="email" name="email" id="email" class="form-control" value="<?php echo(!empty($prog->email))? trim($prog->email) : '';?>">
                                <span id="err_email"></span>
                            </div>
                            <div class="form-group">
                                <label for="phone">Phone <span class="text-danger">*</span></label>
                                <input type="text" name="phone" id="phone" class="form-control" value="<?php echo(!empty($prog->mobile))? trim($prog->mobile) : '';?>">
                                <span id="err_phone"></span>
                            </div>
                        </div>
                    </div>
                    <div class="card" id="sem_details" style="display:none;">
                        <div class="card-body">
                            <div class="form-group">
                                <select class="selectpicker" data-style="select-with-transition" data-title="Select Semester type" name="semtype" id="semtype">
                                    <option value="12">Yearly</option>
                                    <option value="6">Half-Yearly</option>
                                </select>
                                <span id="err_semtype"></span>
                            </div>
                        </div>
                    </div>
                    <div class="card" id="ad_details" style="display:none;">
                        <div class="card-body">
                            <h4 class="card-title">Admission Info. <small class="text-danger">*</small></h4>
                            <div class="form-group">
                                <select class="selectpicker" data-style="select-with-transition" name="apply_type" id="apply_type" title="Single Select <span class='text-danger'>*</span>">
                                    <option value="0" <?php echo(!empty($prog->apply_type) && trim($prog->apply_type) == 0)? 'selected' : '';?>>All Approved</option>
                                    <option value="1" <?php echo(!empty($prog->apply_type) && trim($prog->apply_type) == 1)? 'selected' : '';?>>Selective Approved</option>
                                </select>
                                <span id="err_apply_type"></span>
                            </div>
                            <div class="form-group" id="stype" style="display:none;">
                                <!-- <?php
                                    if(isset($adm[0]->screen_type)){
                                        $st = explode(',', $adm[0]->screen_type);
                                    }else{
                                        $st = array();
                                    }
                                ?> -->
                                <select class="selectpicker" data-style="select-with-transition" title="Screen type <span class='text-danger'>*</span>" multiple name="screen_type[]" id="screen_type">
                                    <option value="0" <?php echo (!empty($prog->screen_type) && in_array('0',explode(',',trim($prog->screen_type)))) ? 'selected' : '' ;?> >Manual Checking</option>
                                    <option value="1" <?php echo (!empty($prog->screen_type) && in_array('1',explode(',',trim($prog->screen_type)))) ? 'selected' : '' ;?>>Online Exam</option>
                                    <option value="2" <?php echo (!empty($prog->screen_type) && in_array('2',explode(',',trim($prog->screen_type)))) ? 'selected' : '' ;?>>Interview</option>
                                </select>
                                <span id="err_screen_type"></span>
                            </div>
                            <div class="form-group">
                                <label for="fdetails">Start Date <span class="text-danger">*</span></label>
                                <input type="date" name="adstart" id="adstart"  class="form-control" value="<?php echo(!empty($prog->astart_date)) ? trim($prog->astart_date) : '';?>">
                                <span id="err_adstart"></span>
                            </div>
                            <div class="form-group">
                                <label for="fdetails">End Date <span class="text-danger">*</span></label>
                                <input type="date" name="adend" id="adend"  class="form-control" value="<?php echo(!empty($prog->aend_date)) ? trim($prog->aend_date) : '';?>">
                                <span id="err_adend"></span>
                            </div>
                            <div class="form-group">
                                <label for="fdetails">Criteria <span class="text-danger">*</span></label>
                                <textarea name="criteria" id="criteria" class="form-control"><?php echo(!empty($prog->criteria))? trim($prog->criteria) : '' ;?></textarea>
                                <span id="err_criteria"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="<?php echo base_url();?>/assets/js/org-admin.js"></script>