<div class="row">
	<div class="col-md-12 col-sm-12">
        <table class="table table-bordered" id="table_job" data-page="<?php echo $page_number;?>" data-last="<?php echo $last_page;?>" data-count="<?php echo count($result);?>">
            <thead>
				<tr>
                    <th scope="col"><input type="checkbox" id="allCheck" onChange="isAllChecked()"></th>
                    <th scope="col" class="text-center font-weight-bold">Sl/No.</th>
					<th scope="col" class="text-center font-weight-bold">Title</th>
					<th scope="col" class="text-center font-weight-bold">Decription</th>
                    <th scope="col" class="text-center font-weight-bold">Start Time</th>
                    <th scope="col" class="text-center font-weight-bold">End Time</th>
                    <th scope="col" class="text-center font-weight-bold">Reporting Email</th>
                    <th scope="col" class="text-center font-weight-bold">Status</th>
					<th scope="col" class="text-center font-weight-bold">Action</th>
				</tr>
			</thead>
            <tbody>
            <?php
                $num = $slNo;
                if(!empty($result)):
                    foreach($result as $row):
            ?>
                <tr>
                    <td><input type="checkbox" class="checkBox" data-id="<?php echo $row->id;?>" value="<?php echo $row->response_flag;?>" id="checkBox<?php echo $num;?>" onChange="isSingleChecked(<?php echo $num;?>)"></td>
                    <td class="text-center"><?php echo $num;?></td>
                    <td class="text-center"><?php echo $row->title;?></td>
                    <td class="text-justify"><?php echo (!empty($row->desc))? substr($row->desc,0, 100) : '';?></td>
                    <td class="text-center"><?php echo (!empty($row->start_time))? date('jS M Y', strtotime($row->start_time)) : '';?></td>
                    <td class="text-center"><?php echo (!empty($row->end_time))? date('jS M Y', strtotime($row->end_time)) : '';?></td>
                    <td class="text-center"><?php echo (!empty($row->report_email))? $row->report_email : '';?></td>
                    <td class="text-center">
                        <?php echo ($row->response_flag == 1)? 'Pending' : 'Approved' ;?>
                    </td>
                    <td class="text-center">
                        <button type="button" class="btn btn-warning btn-sm" title="change status" onclick="singleJObStatusChange(`<?php echo base64_encode($row->id);?>`, `<?php echo base64_encode($row->response_flag);?>`)">
                            <span class="material-icons">published_with_changes</span>
                        </button>
                        <button type="button" class="btn btn-success btn-sm" title="job details" onclick="singleJobDetails(`<?php echo base64_encode($row->id);?>`)">
                            <span class="material-icons">visibility</span>
                        </button>
                        <button type="button" class="btn btn-danger btn-sm" title="delet job" onclick="singleJobDelete(`<?php echo base64_encode($row->id);?>`)">
                            <span class="material-icons">delete</span>
                        </button>
                    </td>
                </tr>
            <?php
                    $num++;
                    endforeach;
                endif;
            ?>
            </tbody>
        </table>
    </div>
</div>

<div id="pagination"></div>

<script>
    $(document).ready(function(){
        getPagination();
    });

    function getPagination(){
        let count        = parseInt($('#table_job').attr('data-count'));
        let page_number  = parseInt($('#table_job').attr('data-page'));
        let last_page    = parseInt($('#table_job').attr('data-last'));
        $.ajax({
            url: baseURL + 'Job/get_pagination',
            type: 'POST',
            data:{
                count       : count,
                page_number : page_number,
                last_page   : last_page,
                func        : 'getJobList'
            },
            success: function(data){
                $('#pagination').html(data);
            }
        })
    }
</script>