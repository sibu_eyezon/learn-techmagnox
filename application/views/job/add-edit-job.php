<span id="current_page" data-page="<?php echo base64_encode($page);?>"></span>
<div class="row">
	<div class="col-md-6 col-sm-12">
        <div class="row">
            <label class="col-sm-2 col-form-label font-weight-bold">Job Title</label>
            <div class="col-sm-10">
                <div class="form-group bmd-form-group">
                    <input type="text" class="form-control" id="title" name="title">
                    <span class="h6 text-danger" id="e_title"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label font-weight-bold">Job Type</label>
            <div class="col-sm-10">
                <div class="form-group bmd-form-group">
                    <select class="form-control selectpicker" name="type[]" multiple="multiple" id="type" data-title="Select Job Type" data-style="select-with-transition" data-live-search="true">
                        <option value="Internship">Internship</option>
                        <option value="Full Time">Full Time</option>
                        <option value="Part Time">Part Time</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label font-weight-bold">Job Description</label>
            <div class="col-sm-10">
                <div class="form-group bmd-form-group">
                    <textarea class="form-control" id="decription" rows="3"></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label font-weight-bold">Job Designation</label>
            <div class="col-sm-10">
                <div class="form-group bmd-form-group">
                    <select class="form-control selectpicker" name="designation" id="designation" data-title="Select Job Designation" data-style="select-with-transition" data-live-search="true">
                    <?php
                        if(!empty($designation)){
                            foreach($designation as $row){
                                echo '<option value="'.$row->id.'">'.$row->name.'</option>';
                            }
                        }
                    ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label font-weight-bold">Job Skill</label>
            <div class="col-sm-10">
                <div class="form-group bmd-form-group">
                    <select class="form-control selectpicker" name="skill[]" multiple="multiple" id="skill" data-title="Select Job Skill" data-style="select-with-transition" data-live-search="true">
                    <?php
                        if(!empty($skill)){
                            foreach($skill as $row){
                                echo '<option value="'.$row->id.'">'.$row->name.'</option>';
                            }
                        }
                    ?>
                    </select>
                    <span class="h6 text-danger" id="e_skill"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label font-weight-bold">Job Location</label>
            <div class="col-sm-10">
                <div class="form-group bmd-form-group">
                    <select class="form-control selectpicker" name="location" id="location" data-title="Select Job Location" data-style="select-with-transition" data-live-search="true">
                    <?php
                        if(!empty($location)){
                            foreach($location as $row){
                                echo '<option value="'.$row->id.'">'.$row->name.'</option>';
                            }
                        }
                    ?>
                    </select>
                    <span class="h6 text-danger" id="e_location"></span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-sm-12">
        <div class="row">
            <label class="col-sm-2 col-form-label font-weight-bold">Job Organization</label>
            <div class="col-sm-10">
                <div class="form-group bmd-form-group">
                    <select class="form-control selectpicker" name="organization" id="organization" data-title="Select Job Organization" data-style="select-with-transition" data-live-search="true">
                    <?php
                        if(!empty($organization)){
                            foreach($organization as $row){
                                echo '<option value="'.$row->id.'">'.$row->name.'</option>';
                            }
                        }
                    ?>
                    </select>
                    <span class="h6 text-danger" id="e_organization"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label font-weight-bold">Job Industry</label>
            <div class="col-sm-10">
                <div class="form-group bmd-form-group">
                    <select class="form-control selectpicker" name="industry" id="industry" data-title="Select Job Industry" data-style="select-with-transition" data-live-search="true">
                    <?php
                        if(!empty($industry)){
                            foreach($industry as $row){
                                echo '<option value="'.$row->id.'">'.$row->name.'</option>';
                            }
                        }
                    ?>
                    </select>
                    <span class="h6 text-danger" id="e_industry"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label font-weight-bold">Experience</label>

            <div class="col-sm-5">
                <div class="form-group bmd-form-group">
                    <select class="form-control selectpicker" name="year" id="year" data-title="Select Year" data-style="select-with-transition">
                        <option value="fresher">Fresher</option>
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="more than 5">More than 5</option>
                        <option value="No Bar">No Bar</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="form-group bmd-form-group">
                    <select class="form-control selectpicker" name="month" id="month" data-title="Select Month" data-style="select-with-transition">
                        <option selected>0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label font-weight-bold">Job Degree</label>
            <div class="col-sm-10">
                <div class="form-group bmd-form-group">
                    <select class="form-control selectpicker" name="degree" id="degree" data-title="Select Job Degree" data-style="select-with-transition" data-live-search="true">
                    <?php
                        if(!empty($degree)){
                            foreach($degree as $row){
                                echo '<option value="'.$row->id.'">'.$row->degree_name.'</option>';
                            }
                        }
                    ?>
                        <option value="0">No Bar</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label font-weight-bold">Job Salary</label>
            <div class="col-sm-5">
                <div class="form-group bmd-form-group">
                    <input type="text" class="form-control" placeholder="min" id="min_salary">
                </div>
            </div>
            <div class="col-sm-5">
                <div class="form-group bmd-form-group">
                    <input type="text" class="form-control" placeholder="max" id="max_salary">
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label font-weight-bold">Candidates Group</label> 
            <div class="col-sm-10">
                <div class="form-group bmd-form-group">
                    <select class="form-control selectpicker" name="group" id="group" data-title="Select Candidates Group" data-style="select-with-transition" data-live-search="true">
                    <?php
                        if(!empty($groups)){
                            foreach($groups as $row){
                                echo '<option value="'.$row->id.'">'.$row->name.'</option>';
                            }
                        }
                    ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label font-weight-bold">Externam Page Link</label>
            <div class="col-sm-10">
                <div class="form-group bmd-form-group">
                    <input type="text" class="form-control" id="link">
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label font-weight-bold">Report Email</label>
            <div class="col-sm-10">
                <div class="form-group bmd-form-group">
                    <input type="text" class="form-control" id="email">
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label font-weight-bold">Start Time</label>
            <div class="col-sm-10">
                <div class="form-group bmd-form-group">
                    <input type="datetime-local" class="form-control" id="start">
                </div>
            </div>
        </div>
        <div class="row">
            <label class="col-sm-2 col-form-label font-weight-bold">End Time</label>
            <div class="col-sm-10">
                <div class="form-group bmd-form-group">
                    <input type="datetime-local" class="form-control" id="end">
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 text-center">
        <button class="btn btn-primary" onClick="addEditJobOperation()">Submit</button>
    </div>
</div>

<script>
    $(document).ready(function(){
        CKEDITOR.replace("decription");
        $('.selectpicker').selectpicker(["refresh"]);

        $('#start').on('change',function(){
            let fromDate = $(this).val();
            $('#end').prop('min',function(){
                return fromDate;
            })
        });

        $('#end').on('change',function(){
            let toDate = $(this).val();
            $('#start').prop('max',function(){
                return toDate;
            })
        });
    });
</script>
