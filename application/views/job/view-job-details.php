<span id="current_page" data-page="<?php echo base64_encode($page);?>"></span>
<div class="row">
    <div class="col-md-6 col-sm-12">
        <div class="card-header" role="tab">
            <div class="h3 font-weight-bold mb-0"><?php echo $result[0]->title;?></div>
        </div>
        <div class="card-body">
            <?php echo (!empty($result[0]->desc))? $result[0]->desc : '';?>
            <?php
                if(!empty($result[0]->type)){
                    echo '<div class="h6">Job Type</div>';
                    $types = json_decode($result[0]->type);
                    echo '<ul>';
                    for($i=0; $i< count($types); $i++){
                        echo '<li style="display:inline-block; padding:5px">'.$types[$i].'</li>';
                    }
                    echo '</ul>';
                }
            ?>
            <div class="h6">Required Skills</div>
            <ul>
               <?php
                    if(!empty($mapSkill)){
                        foreach($mapSkill as $mRow){
                            if(!empty($skill)){
                                foreach($skill as $row){
                                    if($mRow->skills_id === $row->id){
                                        echo '<li>'.$row->name.'</li>';
                                    }
                                }
                            }
                        }
                    }
               ?> 
            </ul>
            <div class="h6">Location</div>
            <ul>
                <?php
                    if(!empty($location)){
                        foreach($location as $row){
                            if($row->id === $result[0]->location_id){
                                echo '<li>'.$row->name.'</li>';
                                break;
                            }
                        }
                    }
                ?>
            </ul>
            
            <?php
                if(!empty($designation)){
                    echo '
                        <div class="h6">Designation</div>
                        <ul>
                    ';
                    foreach($designation as $row){
                        if($row->id === $result[0]->designation_id){
                            echo '<li>'.$row->name.'</li>';
                            break;
                        }else{

                        }
                    }
                    echo '</ul>';
                }
                if(!empty($result[0]->experience)){
                    echo '
                        <div class="h6">Experience</div>
                            <ul>
                                <li>'.$result[0]->experience.'</li>
                            </ul>
                    ';
                }
                if(!empty($result[0]->salary)){
                    echo '
                        <div class="h6">Salary</div>
                            <ul>
                                <li>'.$result[0]->salary.'</li>
                            </ul>
                    ';
                }
            ?>
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="card-body">
        <?php
            if(!empty($degree)){
                echo '
                    <div class="h6">Qualification</div>
                    <ul>
                ';
                foreach($degree as $row){
                    if($row->id === $result[0]->min_qualification_id){
                        echo '<li>'.$row->degree_name.'</li>';
                        break;
                    }else if($row->id === 0){
                        echo '<li>No bar for current candidate</li>';
                    }
                }
                echo '</ul>';
            }

            if(!empty($groups)){
                echo '
                    <div class="h6">Cadidate Groups</div>
                    <ul>
                ';
                foreach($groups as $row){
                    if($row->id === $result[0]->candidate_group_id){
                        echo '<li>'.$row->name.'</li>';
                        break;
                    }else{

                    }
                }
                echo '</ul>';
            }

            if(!empty($result[0]->external_link)){
                echo '
                    <div class="h6">External Link</div>
                        <ul>
                            <li>'.$result[0]->external_link.'</li>
                        </ul>
                ';
            }

            if(!empty($result[0]->report_email)){
                echo '
                    <div class="h6">Report Email</div>
                        <ul>
                            <li>'.$result[0]->report_email.'</li>
                        </ul>
                ';
            }

            if(!empty($result[0]->start_time)){
                echo '
                    <div class="h6">Start Time</div>
                        <ul>
                            <li>'.date('jS M Y', strtotime($result[0]->start_time)).'</li>
                        </ul>
                ';
            }

            if(!empty($result[0]->end_time)){
                echo '
                    <div class="h6">End Time</div>
                        <ul>
                            <li>'.date('jS M Y', strtotime($result[0]->end_time)).'</li>
                        </ul>
                ';
            }
        ?>
        </div>
    </div>
</div>