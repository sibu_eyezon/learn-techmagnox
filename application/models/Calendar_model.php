<?php 
    defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' ); 

    class Calendar_model extends CI_Model {

        public function __construct(){
            parent::__construct();
        }

        public function getScheduleClass($id){
            $this->db->select('sc.*, pg.title as program');
            $this->db->where('sc.user_id',$id);
            $this->db->join('program pg','pg.id = sc.program_sl');
            return $this->db->get('schedule_class sc')->result();
        }

        public function programByTeacher($id){
            $this->db->select('pg.title, pg.id, pg.code');
            $this->db->join('program pg','pg.id=pur.program_id');
            $this->db->where('pur.user_id',$id);
            $this->db->where('pg.status','approved');
            return $this->db->get('pro_users_role pur')->result();
        }

        public function getTotalCourseByProgram($prog, $userid){
            $this->db->select('*'); 
            $this->db->where('program_id',$prog);
            $this->db->where('user_id',$userid);
            return $this->db->get('pro_map_course')->num_rows();
        }

        public function getCourseByProgramAndTeacher($prog, $userid){
            $this->db->select('cous.id, cous.title');
            $this->db->join('pro_map_course pmc','pmc.course_id = cous.id');
            $this->db->where('pmc.program_id',$prog);
            $this->db->where('pmc.user_id',$userid);
            return $this->db->get('pro_course cous')->result();
        }

        public function addNewSchedule($data){
            return $this->db->insert_batch('schedule_class', $data);
        }

/***********************************************************************************************************************************************************/
        public function getStudentClassSchedule($userid){
            $sql = "select a.*from   schedule_class a,adm_can_apply b
            where  a.program_sl = b.prog_id
            and    b.approve_flag = '2'
            and    b.cand_id =".$userid;
            return $this->db->query($sql)->result();
        }
    }