<?php 
    defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' ); 

    class Operation_model extends CI_Model {

        /*=======================================================================*/

        /*=========================== FAQ =======================================*/
        public function get_fqa_total_row($prog_id=null){
            if($prog_id != null){
                $this->db->where('program_id', $prog_id);
            }
            return $this->db->get('program_faqs')->num_rows();
        }

        public function get_faq_list($prog_id=null, $start_record, $per_page_record){
            $this->db->select('*');
            if($prog_id != null){
                $this->db->where('program_id', $prog_id);
            }
            $this->db->order_by("id", "desc");
            $this->db->limit($per_page_record, $start_record);
            //return $this->db->get_compiled_select('program_faqs');
		    return $this->db->get('program_faqs')->result();
        }

        public function get_faq_by_id($id){
            $this->db->select('*');
            $this->db->where('id', $id);
            return $this->db->get('program_faqs')->result();
        }

        public function add_edit_faq($id,$data){
            if($id != null){
                $this->db->where('id', $id);
                return $this->db->update('program_faqs', $data);
            }else{
               return $this->db->insert('program_faqs', $data);
            }
        }

        public function multiple_faq_status_change($data){
            return $this->db->update_batch('program_faqs', $data, 'id');
        }

        public function single_faq_status_change($id, $data){
            $this->db->where('id', $id);
            return $this->db->update('program_faqs', $data);
        }

        /*=======================================================================*/

        /*=========================== Project ===================================*/
        public function get_project_total_row($prog_id=null){
            if($prog_id != null){
                $this->db->where('program_id', $prog_id);
            }
            return $this->db->get('program_project')->num_rows();
        }

        public function get_project_list($prog_id=null, $start_record, $per_page_record){
            $this->db->select('*');
            if($prog_id != null){
                $this->db->where('program_id', $prog_id);
            }
            $this->db->order_by("id", "desc");
            $this->db->limit($per_page_record, $start_record);
		    return $this->db->get('program_project')->result();
        }

        public function get_project_by_id($id){
            $this->db->select('*');
            $this->db->where('id', $id);
            return $this->db->get('program_project')->result();
        }

        public function add_edit_project($id,$data){
            if($id != null){
                $this->db->where('id', $id);
                return $this->db->update('program_project', $data);
            }else{
               return $this->db->insert('program_project', $data);
            }
        }

        public function multiple_project_status_change($data){
            return $this->db->update_batch('program_project', $data, 'id');
        }

        public function single_project_status_change($id, $data){
            $this->db->where('id', $id);
            return $this->db->update('program_project', $data);
        }

        /*================================================================================================*/

        /*=====================================Alumni Speak===============================================*/

        public function get_alumni_speak_total_row(){
            return $this->db->get('alumni_speak')->num_rows();
        }
        public function get_alumni_speak($start_record, $per_page_record){
            $this->db->select('*');
            $this->db->order_by("id", "desc");
            $this->db->limit($per_page_record, $start_record);
            return $this->db->get('alumni_speak')->result();
        }
        public function get_alumni_speak_by_id($id){
            $this->db->select('*');
            $this->db->where('id', $id);
            return $this->db->get('alumni_speak')->result();
        }
        public function add_edit_alumni_speak($id,$data){
            if($id != null){
                $this->db->where('id', $id);
                return $this->db->update('alumni_speak', $data);
            }else{
               return $this->db->insert('alumni_speak', $data);
            }
        }

        public function multiple_alumni_speak_status_change($data){
            return $this->db->update_batch('alumni_speak', $data, 'id');
        }

        public function single_alumni_speak_status_change($id, $data){
            $this->db->where('id', $id);
            return $this->db->update('alumni_speak', $data);
        }

        /*=======================================================================================================*/

        /*========================================== Instructor =================================================*/

        public function get_instructor_list($start_record=null, $per_page_record=null){
            $this->db->select('*');
            if((($start_record != null) || ($start_record == 0)) && $per_page_record != null){
                $this->db->order_by('id','desc');
                $this->db->limit($per_page_record, $start_record);
                return $this->db->get('instructor_list')->result();
            }else{
                return $this->db->get('instructor_list')->num_rows();
            }
        }

        public function get_instructor_by_id($id){
            $this->db->select('*');
            $this->db->where('id', $id);
            return $this->db->get('instructor_list')->result();  
        }

        public function get_instructor_role_designation(){
            $this->db->select('*');
            //$this->db->where('id', $id);
            return $this->db->get('instructor_role_master')->result();
        }

        public function add_edite_instructor($id,$data){
            if($id != null){
                $this->db->where('id', $id);
                return $this->db->update('instructor_list', $data);
            }else{
               return $this->db->insert('instructor_list', $data);
            }
        }

        public function single_instructor_status_change($id, $data){
            $this->db->where('id', $id);
            return $this->db->update('instructor_list', $data);
        }

        public function multiple_instructor_status_change($data){
            return $this->db->update_batch('instructor_list', $data, 'id');
        }

        public function get_unselected_instructor_by_program(){
            $this->db->select('*');
            return $this->db->get('instructor_list')->result();
        }

        public function get_selected_instructor_by_program($id){
            $this->db->select('a.*, b.id as row_id');
            $this->db->where('a.id = b.instructor_id');
            $this->db->where('b.program_id', $id);
            return $this->db->get(['instructor_list a', 'billionskills_course_instructor b'])->result();
        }

        public function map_instructor_with_program($data){
            return $this->db->insert('billionskills_course_instructor', $data);
        }

        public function delete_instructor_from_program($id){
            $this->db->where('id', $id);	
            return $this->db->delete('billionskills_course_instructor');
        }
    }