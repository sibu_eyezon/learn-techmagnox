<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' ); 

class AutoProgramModel extends CI_Model {
	
	
	/*=======================================================*/
	public function get_ordered_lessons($sec_id, $prog_id)
	{
		$sql1 = "SELECT pl.sl as lrt_id, title, file_name, file_type, link, duration, type_name, ap.pos_id as rank
		FROM auto_prog ap INNER JOIN pro_lectures pl ON pl.sl=ap.type_sl WHERE ap.type_name='lectures' AND ap.prog_sl=".$prog_id." AND ap.course_id=".$sec_id;
		
		$sql2 = "SELECT pr.sl as lrt_id, title, 'nil' as file_name, 'nil' as file_type, 'nil' as link, duration, type_name, ap.pos_id as rank
		FROM auto_prog ap INNER JOIN  pro_resources pr ON pr.sl=ap.type_sl WHERE ap.type_name='resources' AND ap.prog_sl=".$prog_id." AND ap.course_id=".$sec_id." ORDER BY rank ASC";
		
		return $this->db->query($sql1.' UNION DISTINCT '.$sql2)->result();
	}
	public function get_total_viewed_lessons($prog_id, $userid)
	{
		$this->db->select('apsd.*')->from('stu_prog_self_details apsd');
		$this->db->join('stud_prog_cons spc', 'spc.sl=apsd.stu_prog_self_sl', 'inner');
		$this->db->where('spc.stud_id', $userid);
		$this->db->where('spc.prog_id', $prog_id);
		return $this->db->get()->num_rows();
	}
	
	public function get_section($prog_id)
	{
		$this->db->where('prog_id', $prog_id);
		$this->db->order_by('post_date', 'ASC');
		return $this->db->get('pro_course')->result();
	}
	public function get_section_by_id($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('pro_course')->result();
	}
	
	public function getTotalLessonByCid($course_sl, $prog)
	{
		$this->db->where('course_id', $course_sl);
		$this->db->where('prog_sl', $prog);
		return $this->db->get('auto_prog')->num_rows();
	}
	
	public function get_stud_prog_cons_id($prog_id, $userid)
	{
		$this->db->where('stud_id', $userid);
		$this->db->where('prog_id', $prog_id);
		$query = $this->db->get('stud_prog_cons')->result_array();
		return $query[0]['sl'];
	}
	public function get_auto_prog_ordered_list($prog_id)
	{
		$this->db->select('ap.sl')->from('auto_prog ap');
		$this->db->join('pro_course pc', 'pc.id=ap.course_id', 'inner');
		$this->db->where('ap.prog_sl', $prog_id);
		$this->db->where('pc.prog_id', $prog_id);
		$this->db->order_by('pc.rank', 'ASC');
		$this->db->order_by('ap.pos_id', 'ASC');
		return $this->db->get()->result();
	}
	
	public function get_current_lesson($spc_id, $status)
	{
		$this->db->select('ap.type, ap.type_sl, ap.type_name, ap.duration, spsd.status')->from('auto_prog ap');
		$this->db->join('stu_prog_self_details spsd', 'spsd.auto_prog_sl=ap.sl', 'inner');
		$this->db->where('spsd.stu_prog_self_sl', $spc_id);
		$this->db->where('spsd.status', $status);
		$this->db->limit(1);
		return $this->db->get()->result();
	}
	/*=======================================================*/
    public function getAllLabs()
	{
		$this->db->select('*')->from('lab')->order_by('id');
		return $this->db->get()->result();
    }

    public function getTestByProgram($prog_id){
        $this->db->where('publish', "true");
        $this->db->where('cat_id', $prog_id);
        $query = $this->db->get('test');
        return $query->result();
    }
    
    public function insertAutoProgram($data)
    {
        $this->db->insert('auto_prog', $data);
        return $this->db->insert_id();
    }

    public function insertAutoProgramLecture($data)
    {
        $this->db->insert('auto_program_lecture', $data);
        return $this->db->insert_id();
    }

    public function insertAutoProgramResource($data)
    {
        $this->db->insert('auto_program_resource', $data);
        return $this->db->insert_id();
    }

    public function insertAutoProgramTest($data)
    {
        $this->db->insert('auto_program_test', $data);
        return $this->db->insert_id();
    }

    

    
    // public function autoProgram()
    // {
    //     $this->db->select('ps.*')->from('pro_semister ps');
    //     $this->db->join('pro_map_sem pms', 'pms.sem_id=ps.id', 'inner');
    //     $this->db->where('pms.program_id', $prog_id);
    //     $this->db->order_by('ps.title', 'ASC');
    //     return $this->db->get()->result();
    // }

    //======================= get for auto program ==================
    // public function getAutoProgramLecture($prog_id)
    // {
    //     // $this->db->where('auto_program_sl', $prog_id);
    //     // $query = $this->db->get('auto_program_lecture');
    //     // return $query->result();

    //     $this->db->select("pl.*,apl.*");
    //     $this->db->join('auto_program_lecture apl', 'apl.lecture_sl=pl.sl', 'left');
    //     $this->db->where('apl.auto_program_sl', $prog_id);
    //     $this->db->order_by('apl.serial_no', 'ASC');
    //     return $this->db->get('pro_lectures pl')->result();

    // }

      public function getAutoProgramLecture($type_sl_id)
    {
        $this->db->select("pl.*");
        $this->db->where('pl.sl', $type_sl_id);
        $qry = $this->db->get('pro_lectures pl');
        return $qry->result()[0];
    }

    // public function getAutoProgramResource($prog_id)
    // {
    //     // $this->db->where('auto_program_sl', $prog_id);
    //     // $query = $this->db->get('auto_program_resource');
    //     // return $query->result();

    //     $this->db->select("pr.*,apr.*");
    //     $this->db->join('auto_program_resource apr', 'apr.resource_sl=pr.sl', 'left');
    //     $this->db->where('apr.auto_program_sl', $prog_id);
    //     $this->db->order_by('apr.serial_no', 'ASC');
    //     return $this->db->get('pro_resources pr')->result();
    // }

     public function getAutoProgramResource($type_sl_id)
    {
        $this->db->select("pr.*");
        $this->db->where('pr.sl', $type_sl_id);
        $qry = $this->db->get('pro_resources pr');
        return $qry->result()[0];
    }

    // public function getAutoProgramTest($prog_id)
    // {
    //     // $this->db->where('auto_program_sl', $prog_id);
    //     // $query = $this->db->get('auto_program_test');
    //     // return $query->result();

    //     $this->db->select("t.*,apt.*");
    //     $this->db->join('auto_program_test apt', 'apt.test_sl=t.id', 'left');
    //     $this->db->where('apt.auto_program_sl', $prog_id);
    //     $this->db->order_by('apt.serial_no', 'ASC');
    //     return $this->db->get('test t')->result();
    // }

     public function getAutoProgramTest($type_sl_id)
    {
        $this->db->select("t.*");
        $this->db->where('t.id', $type_sl_id);
        $qry = $this->db->get('test t');
        return $qry->result()[0];
    }


    public function autoProgram($prog_id=0){
        $this->db->order_by('pos_id', 'ASC');
        $query = $this->db->get('auto_prog');
        return $query->result();
    }

    //==================== new type search =========
     public function autoProgramType($prog_type='3'){
        $this->db->select('prog.*, pa.*, acayear.yearnm');
        $this->db->join('prog_admission pa', 'pa.prog_id=prog.id', 'left');
        $this->db->join('acayear', 'acayear.sl=pa.aca_year', 'left');
        $this->db->where('prog.type', $prog_type);
        return $this->db->get('program prog')->result();
    }

    public function checkAdmApply($prog_data){
        $this->db->where('prog_id', $prog_data['prog_id']);
        $this->db->where('cand_id', $prog_data['cand_id']);
        $query = $this->db->get('adm_can_apply');
        return $query->result();
    }

     public function checkAdmApply2($prog_id,$cand_id){
        $this->db->where('prog_id', $prog_id);
        $this->db->where('cand_id', $cand_id);
        $query = $this->db->get('adm_can_apply');
        return $query->result();
    }

    public function insertAdmApply($data)
    {
        $this->db->insert('adm_can_apply', $data);
        return $this->db->insert_id();
    }

    public function updateAdmApply($data,$adm_can_apply_id){
        $this->db->where('sl', $adm_can_apply_id);
        $query = $this->db->update("adm_can_apply", $data);
         // $this->db->last_query();die;
        return $this->db->affected_rows();
    }

    
    public function chkProUserRole($prog_data){
        $this->db->where('program_id', $prog_data['program_id']);
        $this->db->where('user_id', $prog_data['user_id']);
        $query = $this->db->get('pro_users_role');
        return $query->result();
    }

     public function insertProUserRole($data)
    {
        $this->db->insert('pro_users_role', $data);
        return $this->db->insert_id();
    }

    //==============================================

     public function autoProgramById($pid){
        $this->db->where('sl', $pid);
        $query = $this->db->get('auto_prog');
        return $query->result();
    }


    public function autoProgramByProgId($progid=0){
        if($progid > 0)
        {
            $this->db->where('prog_sl', $progid);
        }
        $this->db->order_by('pos_id', 'ASC');
        $query = $this->db->get('auto_prog');
        // echo $this->db->last_query();
        return $query->result();
    }

    //================ new saumya =============
     public function autoProgramByCourse($course_id){
        $this->db->where('course_id', $course_id);
        $this->db->order_by('pos_id', 'ASC');
        $query = $this->db->get('auto_prog');
        return $query->result();
    }

    public function getTestByAutoProgram($sl_id){
        $this->db->where('id', $sl_id);
        $query = $this->db->get('test');
        return $query->result();
    }

    //=========================================

    public function deleteProgramByProgId($progid){
    $cond  = ['prog_sl' => $progid];
    $query = $this->db->where($cond);
    return $this->db->delete('auto_prog');
}



    public function autoProgramGroup(){
        $this->db->select('ap.*,prog.*');
        $this->db->join('program prog', 'prog.id=ap.sl', 'left');
        return $this->db->get('auto_prog ap')->result();
    }

    
    public function insertAutoProgramTrack($data)
    {
        $this->db->insert('auto_prog_track', $data);
        return $this->db->insert_id();
    }

    public function getAutoProgramTrack($prog_sl,$stu_sl)
    {
        $this->db->where('auto_prog_sl', $prog_sl);
        $this->db->where('stu_sl', $stu_sl);
        $query = $this->db->get('auto_prog_track');
        // echo $this->db->last_query();die;
        return $query->result();
    }

    public function editAutoProgramTrack($track_array,$program_id,$userid){
        $this->db->where('auto_prog_sl', $program_id);
        $this->db->where('stu_sl', $userid);
        $query = $this->db->update("auto_prog_track", $track_array);
         // $this->db->last_query();die;
        return $this->db->affected_rows();
    }


    //=================== adm_can_apply check ==========
    public function getStudProgSelf($adm_can_apply_id)
    {
        $this->db->where('adm_can_apply_id', $adm_can_apply_id);
        $query = $this->db->get('stud_prog_self');
        // echo $this->db->last_query();die;
        return $query->result();
    }

    public function insertStudProgSelf($data)
    {
        $this->db->insert('stud_prog_self', $data);
        return $this->db->insert_id();
    }

    public function editStudProgSelf($data,$sl_no){
        $this->db->where('sl', $sl_no);
        $query = $this->db->update("stud_prog_self", $data);
         // $this->db->last_query();die;
        return $sl_no;
    }

     //=================== adm_can_apply check details ==========
    public function getStudProgSelfDetails($stu_prog_self_sl, $auto_prog_sl)
    {
        $this->db->where('stu_prog_self_sl', $stu_prog_self_sl);
        $this->db->where('auto_prog_sl', $auto_prog_sl);
        $query = $this->db->get('stu_prog_self_details');
        // echo $this->db->last_query();die;
        return $query->result();
    }

    public function insertStudProgSelfDetails($data)
    {
        $this->db->insert('stu_prog_self_details', $data);
        return $this->db->insert_id();
    }
    
	


}