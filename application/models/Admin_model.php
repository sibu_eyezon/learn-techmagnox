<?php 
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' ); 

class Admin_model extends CI_Model {
	
	public function getTotalUsersByType($user_type)
	{
		$this->db->where('user_type', $user_type);
		return $this->db->get('user_auth')->num_rows();
	}
	public function getTotalProgramsByStatus($status)
	{
		$this->db->where('status', $status);
		return $this->db->get('program')->num_rows();
	}
    /*===========================================================================================
                                Oganization and Department List
    =============================================================================================*/
	public function get_all_user_organizations()
	{
		$this->db->select('*')->from('organization');
		return $this->db->get()->result();
	}
	
    public function getOrganization($userid){
        $this->db->select('id,title');
        $this->db->where('user_id',$userid);
        return $this->db->get('pro_organization')->result();
    }

    public function getDepartment($userid){
        $this->db->select('id,org_id,title,short_name');
        $this->db->where('user_id',$userid);
        return $this->db->get('pro_stream')->result();       
    }

    public function getOrgById($id){
        $this->db->select('*');
        $this->db->where('id',$id);
        return $this->db->get('pro_organization')->row();       
    }

    public function getDeptById($id){
        $this->db->select('pro_stream.id,pro_stream.title,pro_stream.short_name,pro_stream.logo,pro_stream.contact_info,pro_stream.details,pro_stream.website,pro_organization.id as org_id,pro_organization.title as organization');
        $this->db->from('pro_stream');
        $this->db->join('pro_organization', 'pro_organization.id = pro_stream.org_id');
        $this->db->where('pro_stream.id',$id);
        return $this->db->get()->row();
    }

    public function insertNewDepartment($data){
        if($this->db->insert('pro_stream', $data)){
            return true;
        }else{
            return false;
        }
    }

    public function updateNewDepartment($data,$id){
        $this->db->where('id',$id);  
        if($this->db->update('pro_stream', $data)){
            return true;
        }else{
            return false;
        }
    }

    public function insertNewOrganization($data){
        if($this->db->insert('pro_organization', $data)){
            return true;
        }else{
            return false;
        }        
    }

    public function updateNewOrganization($data,$id){
        $this->db->where('id',$id);  
        if($this->db->update('pro_organization', $data)){
            return true;
        }else{
            return false;
        }       
    }
    /*=================================================================================================*/
	public function getUsersByType($params)
	{
		$this->db->select('ud.*, ua.user_type, ua.verification_status, ua.status')->from('user_details ud');
		$this->db->join('user_auth ua', 'ua.user_id=ud.id', 'inner');
		$this->db->where('ua.user_type', $params);
		$this->db->order_by('ud.first_name', 'ASC');
		return $this->db->get()->result();
	}
	
	public function getUserDetailsById($params)
	{
		$this->db->select('ud.*, ua.user_type, ua.verification_status, ua.status')->from('user_details ud');
		$this->db->join('user_auth ua', 'ua.user_id=ud.id', 'inner');
		$this->db->where('ud.id', $params);
		return $this->db->get()->result();
	}
    /*===========================================================================================
                                            Program
    =============================================================================================*/
	public function getProgramsByStatus($status)
	{
		$this->db->select("prog.*, pa.*, acayear.yearnm, CONCAT(ud.first_name,' ', ud.last_name) as uname")->from('program prog');
		$this->db->join('user_details ud', 'ud.id=prog.user_id', 'inner');
		$this->db->join('prog_admission pa', 'pa.prog_id=prog.id', 'left');
		$this->db->join('acayear', 'acayear.sl=pa.aca_year', 'left');
		$this->db->where('prog.status', $status);
		$this->db->order_by('prog.date_added', 'DESC');
		return $this->db->get()->result();
	}
	
	public function getProgramCreator($prog_id)
	{
		$this->db->select("prog.title, CONCAT(ud.first_name,' ', ud.last_name) as uname, ud.email")->from('program prog');
		$this->db->join('user_details ud', 'ud.id=prog.user_id', 'inner');
		return $this->db->get()->result();
	}
	
    public function getAcademicYear(){
        $this->db->select('sl,yearnm');
        return $this->db->get('acayear')->result();
    }

    public function departmentByOrganization($org){
        $this->db->select('id,title');
        $this->db->where('org_id',$org);
        return $this->db->get('pro_stream')->result();
    }

    public function departmentByProgram($id){
        $sql="SELECT id,title FROM pro_stream WHERE org_id = (SELECT org_id FROM pro_map_org WHERE program_id = ".$id.")";
        return $this->db->query($sql)->result();
    }

    public function programTable($userid){
        $this->db->select('program.id as pro_id,program.title,program.code,program.category,program.start_date,program.end_date,program.duration,program.dtype,pro_organization.title as org_title,pro_stream.title as stream_title');
        $this->db->from('program');
        $this->db->join('pro_map_org','program.id=pro_map_org.program_id');
        $this->db->join('pro_map_stream','program.id=pro_map_stream.program_id');
        $this->db->join('pro_organization','pro_organization.id=pro_map_org.org_id');
        $this->db->join('pro_stream','pro_stream.id=pro_map_stream.stream_id');
        $this->db->where('program.user_id',$userid);
        return $this->db->get()->result();
    }

    public function programTableByOrg($userid,$org){
        $this->db->select('program.id as pro_id,program.title,program.code,program.category,program.start_date,program.end_date,program.duration,program.dtype,pro_organization.title as org_title,pro_stream.title as stream_title');
        $this->db->from('program');
        $this->db->join('pro_map_org','program.id=pro_map_org.program_id');
        $this->db->join('pro_map_stream','program.id=pro_map_stream.program_id');
        $this->db->join('pro_organization','pro_organization.id=pro_map_org.org_id');
        $this->db->join('pro_stream','pro_stream.id=pro_map_stream.stream_id');
        $this->db->where('program.user_id',$userid);
        $this->db->where('pro_organization.id',$org);
        return $this->db->get()->result();       
    }

    public function programTableByOrgAndDept($userid,$org,$dept){
        $this->db->select('program.id as pro_id,program.title,program.code,program.category,program.start_date,program.end_date,program.duration,program.dtype,pro_organization.title as org_title,pro_stream.title as stream_title');
        $this->db->from('program');
        $this->db->join('pro_map_org','program.id=pro_map_org.program_id');
        $this->db->join('pro_map_stream','program.id=pro_map_stream.program_id');
        $this->db->join('pro_organization','pro_organization.id=pro_map_org.org_id');
        $this->db->join('pro_stream','pro_stream.id=pro_map_stream.stream_id');
        $this->db->where('program.user_id',$userid);
        $this->db->where('pro_organization.id',$org);
        $this->db->where('pro_stream.id',$dept);
        return $this->db->get()->result();         
    }

    public function programDetailsById($id){
        $this->db->select('program.id as pro_id,program.title,program.code,program.category,program.start_date,program.end_date,program.duration,program.dtype,program.student_enroll,program.teacher_enroll,program.type,program.overview,program.email,program.mobile,program.total_fee,program.total_credit,program.feetype,prog_admission.aca_year,acayear.sl,acayear.yearnm,pro_organization.id as org_id,pro_organization.title as org_title,prog_admission.discount,prog_admission.apply_type,prog_admission.astart_date,prog_admission.aend_date,prog_admission.criteria,prog_admission.screen_type,pro_stream.id as dept_id,pro_stream.title as dept_title,prog_admission.ptype,prog_admission.total_seat,prog_admission.prog_hrs');
        $this->db->from('program');
        $this->db->join('prog_admission','prog_admission.prog_id=program.id');
        $this->db->join('acayear','acayear.sl=prog_admission.aca_year');
        $this->db->join('pro_map_org','program.id=pro_map_org.program_id');
        $this->db->join('pro_map_stream','program.id=pro_map_stream.program_id');
        $this->db->join('pro_organization','pro_organization.id=pro_map_org.org_id');
        $this->db->join('pro_stream','pro_stream.id=pro_map_stream.stream_id');
        $this->db->where('program.id',$id);
        return $this->db->get()->row();
    }



    public function teacherList($start,$record){
        $this->db->select("ud.id,CONCAT(ud.salutation,' ',ud.first_name,' ',ud.last_name) as name,ud.photo_sm,ud.linkedin_link,ud.about_me");
        $this->db->from('user_details ud');
        $this->db->join('user_auth ua','ua.user_id=ud.id');
        $this->db->where('ua.user_type','teacher');
        $this->db->limit($record,$start);
        return $this->db->get()->result();
    }

    public function teacherTotalRow(){
        $this->db->select("*");
        $this->db->from('user_details ud');
        $this->db->join('user_auth ua','ua.user_id=ud.id');
        $this->db->where('ua.user_type','teacher');
        return $this->db->get()->num_rows();       
    }

    public function getTeacherByID($id){
        $this->db->select("ud.id,ud.salutation,ud.first_name,ud.last_name,ud.photo_sm,ud.linkedin_link,ud.about_me"); 
        $this->db->from('user_details ud');
        $this->db->where('ud.id',$id);
        return $this->db->get()->row();
    }

    public function teacherUpdate($data,$id){
        $this->db->where('id',$id);  
        if($this->db->update('user_details', $data)){
            return true;
        }else{
            return false;
        }         
    }
	
	/*===========================================================================================
                                       
    =============================================================================================*/
	public function getAllFeedbacks()
	{
		$this->db->order_by('create_datetime', 'DESC');
		return $this->db->get('feedback')->result();
	}
	/*===========================================================================================
                                       
    =============================================================================================*/
	public function getParentCategories($parent)
	{
		$this->db->where('parent', $parent);
		$this->db->order_by('name', 'ASC');
		return $this->db->get('category')->result();
	}
	public function getParentCategoryById($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('category')->result();
	}
	/*===========================================================================================
                                       
    =============================================================================================*/
	public function getAllTopics()
	{
		$this->db->order_by('tname', 'ASC');
		return $this->db->get('topics')->result();
	}
	public function getTopicById($id)
	{
		$this->db->where('sl', $id);
		return $this->db->get('topics')->result();
	}
	/*===========================================================================================
                                       
    =============================================================================================*/
	public function getAllCoupons()
	{
		$this->db->order_by('coupon_code', 'ASC');
		return $this->db->get('coupons')->result();
	}
	public function getCouponById($id)
	{
		$this->db->where('sl', $id);
		return $this->db->get('coupons')->result();
	}
	/*===========================================================================================
                                       
    =============================================================================================*/
	public function getAllSkills()
	{
		$this->db->order_by('name', 'ASC');
		return $this->db->get('skills')->result();
	}
	public function getSkillById($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('skills')->result();
	}

    /*================================================================================================*/

    /*==============================================Blog==============================================*/

    public function get_blogs($start_record=null, $per_page_record=null){
        $this->db->select('*');
        $this->db->where('active','true');
        if((($start_record != null) || ($start_record == 0)) && $per_page_record != null){
            $this->db->order_by('id','desc');
            $this->db->limit($per_page_record, $start_record);
            return $this->db->get('blogs')->result();
        }else{
            return $this->db->get('blogs')->num_rows();
        }
    }

    public function get_category(){
        return $this->db->get('category')->result();
    }

    public function get_skill(){
        return $this->db->get('skills')->result();
    }

    public function get_blog_by_id($id){
        $this->db->where('id',$id);
        return $this->db->get('blogs')->result();
    }

    public function add_edit_blog($data, $id=null){
        if($id){
            $this->db->where('id',$id);
            return $this->db->update('blogs',$data);
        }else{
            return $this->db->insert('blogs',$data);
        }
    }


    public function to_get_program_list(){
        $this->db->select('*');
        return $this->db->get('program')->result();
    }

    public function isThisSheetIdExiste($rand){
        $this->db->select('*');
        $this->db->where('num_file_id', $rand);
        $query = $this->db->get('user_login_upload');
        return $query->num_rows();
    }

    public function check_email_before_upload_excel($email){
        $this->db->select('*');
        $this->db->where('email', $email);
        $query = $this->db->get('user_auth');
        $num = $query->num_rows();
        return $num;
    }

    // public function check_phone_before_upload_excel($phone){
    //     $this->db->select('*');
    //     $this->db->where('phone', $phone);
    //     $query = $this->db->get('user_auth');
    //     $num = $query->num_rows();
    //     return $num;
    // }

    public function uploadUserRegistrationSpradSheet($data, $fileID){
        if($this->db->insert_batch('user_login_upload', $data)){
            $procedure = "CALL login_excel_upload(".$fileID.",null, null)";
            $data      = $this->db->query($procedure);
            return  $data->result();
            //return true;
        }else{
            return false;
        }
    }
	
	public function to_all_bookings_for_trial_class()
	{
		return $this->db->get('junior_trial_class')->result();
	}
	
	
	public function get_all_newsletters()
	{
		return $this->db->get('email_template')->result();
	}
	
	public function get_newsletter_by_id($id)
	{
		$this->db->where(array('id'=>$id, 'status<>'=>'D'));
		return $this->db->get('email_template')->result();
	}
}