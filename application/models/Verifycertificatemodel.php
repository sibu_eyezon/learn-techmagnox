<?php
    defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

    class Verifycertificatemodel extends CI_Model {
        public function __construct(){
            parent::__construct();
        }

        public function first_step_varification($data){
            $this->db->insert('verify_certificate',$data);
            return $this->db->insert_id();
        }

        public function insertCertificateCode($id, $data){
            $this->db->where('id', $id);
            return $this->db->update('verify_certificate',$data);
        }

        public function matchedCertificateCode($certificate){
            $this->db->select('sl, stud_id, prog_id, enrollment_no');
            $this->db->where('enrollment_no', $certificate);
            return $this->db->get('stud_prog_cons')->result();
        }

        public function get_user_details($id){
            $this->db->select('id, first_name, last_name, photo_sm, email, phone');
            $this->db->where('id', $id);
            return $this->db->get('user_details')->row();
        }

        public function get_program_details($id){
            $this->db->select('id, code, title, category, start_date, end_date');
            $this->db->where('id', $id);
            return $this->db->get('program')->row();
        }
    }
?>