<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' ); 

class Member extends CI_Model {
	
	public function setNotification($pid, $ntype_id, $teacher_id, $stud_id, $desc)
	{
		return $this->db->query("call proc_notification(".$pid.", ".$ntype_id.", ".$teacher_id.", ".$stud_id.", '".$desc."')")->result();
	}
	
	public function deleteMultipleDataById($tablename, $pkid, $data)
	{
		$this->db->where_in($pkid, $data);
		return $this->db->delete($tablename);
	}
	
	public function getTotalProgCourses($pid)
	{
		$this->db->where('prog_id', $pid);
		return $this->db->get('pro_course')->num_rows();
	}
	
	public function getPurdueRegs()
	{
		$this->db->order_by('sl', 'DESC');
		return $this->db->get('purdue_reg')->result();
	}
	
	public function getAllMyPrograms($role, $userid)
	{
		$this->db->select('prog.*, pa.*, prog.user_id as mu_id, pur.user_id AS puserid, pur.role, pur.status as pur_status, prog.status as prog_status, acayear.yearnm')->from('program prog');
		$this->db->join('pro_users_role pur', 'pur.program_id=prog.id', 'inner');
		$this->db->join('prog_admission pa', 'pa.prog_id=prog.id', 'left');
		$this->db->join('acayear', 'acayear.sl=pa.aca_year', 'left');
		$this->db->where('pur.role', $role);
		$this->db->where('pur.user_id', $userid);
		//$this->db->where('pur.status', 'accepted');
		//$this->db->where('prog.status', 'approved');
		$this->db->order_by('prog.date_added', 'DESC');
		return $this->db->get()->result();
	}
	public function get_filtered_user_programs($type, $userid)
	{
		$this->db->select("prog.id, prog.user_id as mu_id, prog.status as prog_status, prog.code, prog.title, prog.type, prog.category, prog.duration, prog.start_date, prog.end_date, , prog.feetype, prog.total_fee, pa.aend_date, pa.prog_level, pa.prog_hrs, pur.user_id AS puserid, (select count(cand_id) from adm_can_apply where prog_id=prog.id) as student_applied, (select count(cand_id) from adm_can_apply where prog_id=prog.id and approve_flag='2') as student_enrolled")->from('program prog');
		$this->db->join('pro_users_role pur', 'pur.program_id=prog.id', 'inner');
		$this->db->join('prog_admission pa', 'pa.prog_id=prog.id', 'left');
		$this->db->where('pur.role', 'Teacher');
		$this->db->where('pur.user_id', $userid);
		if($type == 'active'){
			$this->db->where('prog.status', 'approved');
			$this->db->where('DATE(prog.end_date) >=', date('Y-m-d'));
		}else if($type == 'draft'){
			$this->db->where('prog.status', 'drafted');
		}else if($type == 'pending'){
			$this->db->where('prog.status', 'pending');
		}else if($type == 'archive'){
			$this->db->where('prog.status', 'approved');
			$this->db->where('DATE(prog.end_date) <',  date('Y-m-d'));
		}
		$this->db->order_by('prog.date_added', 'DESC');
		return $this->db->get()->result();
	}
	public function getProgDetCounters($prog_id)
	{
		$sql = "SELECT (SELECT count(id) FROM pro_course WHERE prog_id=".$prog_id.") as cmodule, (SELECT count(cand_id) FROM adm_can_apply WHERE prog_id=".$prog_id.") as capplied, (SELECT count(cand_id) FROM adm_can_apply WHERE prog_id=".$prog_id." AND approve_flag='2') as cstudent, (SELECT count(user_id) FROM pro_users_role WHERE program_id=".$prog_id." AND role='Teacher' AND status='accepted') as cinstuctor, (SELECT count(org_id) FROM pro_map_org WHERE program_id=".$prog_id.") as corg";
		return $this->db->query($sql)->result_array();
	}
	public function getAllStudPrograms($userid)
	{
		//pur.user_id AS puserid, pur.role, pur.status as pur_status,
		$this->db->select('prog.*, pa.*, adm.approve_flag, acayear.yearnm')->from('program prog');
		$this->db->join('adm_can_apply adm', 'adm.prog_id=prog.id', 'inner');
		$this->db->join('prog_admission pa', 'pa.prog_id=prog.id', 'left');
		$this->db->join('acayear', 'acayear.sl=pa.aca_year', 'left');
		$this->db->where('adm.cand_id', $userid);
		$this->db->where('prog.status', 'approved');
		$this->db->order_by('prog.date_added', 'DESC');
		return $this->db->get()->result();
	}
	public function getStudProgCons($userid, $pid)
	{
		$this->db->where('stud_id', $userid);
		$this->db->where('prog_id', $pid);
		return $this->db->get('stud_prog_cons')->result();
	}
	public function getNoticesBYFid($userid)
	{
		$this->db->select('*')->from('program_notice');
		$this->db->where('userid', $userid);
		return $this->db->get()->result();
	}
	public function getNoticesBYid($id)
	{
		$this->db->select('*')->from('program_notice');
		$this->db->where('sl', $id);
		return $this->db->get()->result();
	}
	public function getAllNoticesByUid($userid)
	{
		$this->db->select('pn.*, prog.title as ptitle, pc.title as pc_title, ud.first_name, ud.last_name')->from('program_notice pn');
		$this->db->join('pro_users_role pur', 'pur.program_id=pn.program_sl', 'inner');
		$this->db->join('program prog', 'prog.id=pn.program_sl', 'left');
		$this->db->join('pro_course pc', 'pc.id=pn.course_sl', 'left');
		$this->db->join('user_details ud', 'ud.id=pn.userid', 'left');
		$this->db->where('pur.user_id', $userid);
		return $this->db->get()->result();
	}
	public function getStudentNotices($userid)
	{
		/*select pn.* from program_notice pn
		inner join pro_users_role pur on pur.program_id=pn.program_sl
		where pur.user_id=7;  , CONCAT(ud.fname," ",ud.lname) as profname*/
		$this->db->select('pn.*')->from('program_notice pn');
		$this->db->join('pro_users_role pur', 'pur.program_id=pn.program_sl', 'inner');
		//$this->db->join('user_details ud', 'ud.id=', 'left');
		$this->db->where('pur.user_id', $userid);
		return $this->db->get()->result();
	}
	
	public function getAllActiveAcaYear()
	{
		$this->db->where('status', true);
		return $this->db->get('acayear')->result();
	}
	
	public function insertTeacherNotice($data)
	{
		return $this->db->insert('program_notice', $data);
	}
	public function updateTeacherNotice($data, $id)
	{
		$this->db->where('sl', $id);
		return $this->db->update('program_notice', $data);
	}
	public function deleteNoticeById($id)
	{
		$this->db->where('sl', $id);
		return $this->db->delete('program_notice');
	}
	public function insertScheduleClass($data)
	{
		return $this->db->insert('schedule_class', $data);
	}
	
	public function getUserDetailsById($userid)
	{
		$this->db->where('id', $userid);
		return $this->db->get('user_details')->result();
	}
	public function updateUserAuth($data, $id)
	{
		$this->db->where('user_id', $id);
		return $this->db->update('user_auth', $data);
	}
	public function updateUserDetails($data, $id)
	{
		$this->db->where('id', $id);
		return $this->db->update('user_details', $data);
	}
	
	public function deleteUserSkills($id)
	{
		$this->db->where('user_id', $id);
		return $this->db->delete('b2c_user_skill');
	}
	public function deleteUserAcademic($id)
	{
		$this->db->where('user_id', $id);
		return $this->db->delete('b2c_user_academic');
	}
	
	public function getAllUserSkillsArray($userid)
	{
		$this->db->select('skills.id, name')->from('skills');
		$this->db->join('b2c_user_skill bus', 'bus.skill_id=skills.id', 'inner');
		$this->db->where('bus.user_id', $userid);
		return $this->db->get()->result();
	}
	public function getAllUserSkills($userid)
	{
		$this->db->select('name')->from('skills');
		$this->db->join('b2c_user_skill bus', 'bus.skill_id=skills.id', 'inner');
		$this->db->where('bus.user_id', $userid);
		return $this->db->get()->result();
	}
	public function getAllUserAcademic($userid)
	{
		$this->db->where('user_id', $userid);
		return $this->db->get('b2c_user_academic')->result();
	}
	
	public function insertProgram($data)
	{
		$this->db->insert('program', $data);
		return $this->db->insert_id();
	}
	public function insertUserRole($data)
	{
		return $this->db->insert('pro_users_role', $data);
	}
	public function checkAvailProgAdmission($pid)
	{
		$this->db->where('prog_id', $pid);
		return $this->db->get('prog_admission')->num_rows();
	}
	public function insertAdmission($data)
	{
		return $this->db->insert('prog_admission', $data);
	}
	public function updateAdmission($data, $pid)
	{
		$this->db->where('prog_id', $pid);
		return $this->db->update('prog_admission', $data);
	}
	public function getProgramById($id)
	{
		$this->db->select('prog.*, pa.*, acayear.yearnm');
		$this->db->join('prog_admission pa', 'pa.prog_id=prog.id', 'left');
		$this->db->join('acayear', 'acayear.sl=pa.aca_year', 'left');
		$this->db->where('prog.id', $id);
		return $this->db->get('program prog')->result();
	}
	public function getStudProgramById($id, $userid)
	{
		$this->db->select('prog.*, pa.*, adm.approve_flag, acayear.yearnm')->from('program prog');
		$this->db->join('adm_can_apply adm', 'adm.prog_id=prog.id', 'inner');
		$this->db->join('prog_admission pa', 'pa.prog_id=prog.id', 'left');
		$this->db->join('acayear', 'acayear.sl=pa.aca_year', 'left');
		$this->db->where('adm.cand_id', $userid);
		$this->db->where('prog.id', $id);
		return $this->db->get()->result();
	}
	public function getAdmissionInfoByPid($id)
	{
		$this->db->where('prog_id', $id);
		return $this->db->get('prog_admission')->result();
	}
	public function getCertificateInfoByPid($id)
	{
		$this->db->where('prog_id', $id);
		return $this->db->get('pro_certificate')->result();
	}
	public function updateProgram($data, $id)
	{
		$this->db->where('id', $id);
		return $this->db->update('program', $data);
	}
	public function checkAvailableSemester($pid)
	{
		if($pid!=0){
			$this->db->where('program_id', $pid);
			return $this->db->get('pro_map_sem')->num_rows();
		}else{
			return 0;
		}
	}
	public function removeSemestersByProgId($prog_id)
	{
		$query = 'DELETE FROM pro_semister WHERE id IN (SELECT sem_id FROM pro_map_sem WHERE program_id = '.$prog_id.')';
		return $this->db->query($query);
	}
	public function getProgRoleRequest($role, $prog_id, $userid)
	{
		$this->db->where('role', $role);
		$this->db->where('program_id', $prog_id);
		$this->db->where('status', 'pending');
		$this->db->where_not_in('user_id', $userid);
		$this->db->order_by('add_date', 'DESC');
		return $this->db->get('pro_users_role')->num_rows();
	}
	public function getAllRequestByIdRole($prog, $role, $userId, $status)
	{
		
		$this->db->select("pur.id AS purid, ud.id, CONCAT(ud.salutation,' ',ud.first_name,' ',ud.last_name) AS name, ud.email, ud.phone, ud.photo_sm, ud.about_me, ud.organization, ud.designation, prog.title, pur.role, pur.status")->from('user_details ud');
		$this->db->join('pro_users_role pur', 'pur.user_id=ud.id', 'inner');
		$this->db->join('program prog', 'prog.id=pur.program_id', 'inner');
		if($prog!='all'){
			$this->db->where('pur.program_id', $prog);
		}
		if($role!='All'){
			$this->db->where('pur.role', $role);
		}
		if($userId!=null){
			$this->db->where('prog.user_id', $userId);
		}
		$this->db->where('pur.status', $status);
		//$this->db->where('prog.status', 'approved');
		$this->db->order_by('pur.add_date', 'DESC');
		return $this->db->get()->result();
	}
	public function getProgramStudAccepted($prog_id)
	{
		$this->db->select("DISTINCT(ud.id), CONCAT(ud.first_name,' ',ud.last_name) AS name, ud.email, ud.phone, ud.photo_sm, spc.enrollment_no, pur.role, pur.status")->from('user_details ud');
		$this->db->join('pro_users_role pur', 'pur.user_id=ud.id', 'inner');
		$this->db->join('stud_prog_cons spc', 'spc.stud_id=ud.id', 'inner');
		$this->db->where('spc.prog_id', $prog_id);
		$this->db->where('pur.program_id', $prog_id);
		$this->db->where('pur.status', 'accepted');
		$this->db->order_by('spc.enrollment_no', 'ASC');
		return $this->db->get()->result();
	}
	
	public function getMyRequestForPrograms($userid)
	{
		/*$this->db->select("title, code, type, category, 'Approval' as role, status, date_added")->from('program');
		$this->db->where('user_id', $userid);
		$query1 = $this->db->get_compiled_select();
		
		$this->db->select("program.title, program.code, program.type, program.category, date_added, pur.role, pur.status")->from('program');
		$this->db->join('pro_users_role pur', 'pur.program_id=program.id', 'inner');
		$this->db->where('pur.user_id', $userid);
		$this->db->where('program.user_id<>', $userid);
		$query2 = $this->db->get_compiled_select();
		
		return $this->db->query($query1." UNION ".$query2)->result();*/
		$this->db->select('program.*, acayear.yearnm')->from('program');
		$this->db->join('prog_admission pa', 'pa.prog_id=program.id', 'inner');
		$this->db->join('acayear', 'acayear.sl=pa.aca_year', 'left');
		$this->db->where('user_id', $userid);
		return $this->db->get()->result();
	}
	public function getProgOrganizations($pid)
	{
		$this->db->select('po.*')->from('pro_organization po');
		$this->db->join('pro_map_org pmo', 'pmo.org_id=po.id', 'inner');
		$this->db->where('pmo.program_id', $pid);
		$this->db->order_by('po.title', 'ASC');
		return $this->db->get()->result();
	}
	public function checkRedundLearningApply($userid, $pid)
	{
		$this->db->where('cand_id', $userid);
		$this->db->where('prog_id', $pid);
		return $this->db->get('adm_can_apply')->num_rows();
	}
	public function checkReduntAdmission($prog_id, $userid)
	{
		$this->db->where('prog_id', $prog_id);
		$this->db->where('cand_id', $userid);
		return $this->db->get('adm_can_apply')->num_rows();
	}
	public function getAllProgramsNotUser($role, $userid)
	{
		$this->db->select('prog.*, pa.*, acayear.yearnm')->from('program prog');
		$this->db->join('prog_admission pa', 'pa.prog_id=prog.id', 'inner');
		$this->db->join('acayear', 'acayear.sl=pa.aca_year', 'left');
		$this->db->where('prog.status', 'approved');
		if($role=='Teacher'){
			$this->db->where_not_in('prog.user_id', $userid);
		}
		$this->db->order_by('prog.date_added', 'DESC');
		return $this->db->get()->result();
	}
	public function getUserApplication($email)
	{
		$this->db->select('pui.*, prog.title')->from('pro_user_invite pui');
		$this->db->join('program prog', 'prog.id=pui.program_id', 'inner');
		$this->db->where('pui.user_email', $email);
		$this->db->where('pui.itype', 'applied');
		$this->db->where('pui.status', 'pending');
		return $this->db->get()->result();
	}
	public function getAllMyInvitationRespondById($userid)
	{
		$this->db->select("pui.*, prog.title as prog_name, CONCAT(ud.first_name,' ',ud.last_name) as hostname")->from('pro_user_invite pui');
		$this->db->join('program prog', 'prog.id=pui.program_id', 'inner');
		$this->db->join('user_details ud', 'ud.email=pui.user_email', 'inner');
		$this->db->where('pui.invite_by', $userid);
		$this->db->order_by('invite_datetime', 'DESC');
		return $this->db->get()->result();
	}
	public function getUserInvitationsByEmail($email)
	{
		$this->db->select("pui.*, prog.title as prog_name, CONCAT(ud.first_name,' ',ud.last_name) as hostname")->from('pro_user_invite pui');
		$this->db->join('program prog', 'prog.id=pui.program_id', 'inner');
		$this->db->join('user_details ud', 'ud.id=pui.invite_by', 'inner');
		$this->db->where('pui.user_email', $email);
		$this->db->order_by('invite_datetime', 'DESC');
		return $this->db->get()->result();
	}
	public function checkUserRoleOnProgram($pid, $role, $userid)
	{
		$this->db->where('role', $role);
		$this->db->where('program_id', $pid);
		$this->db->where('user_id', $userid);
		return $this->db->get('pro_users_role')->num_rows();
	}
	public function updateUserInvite($data, $id)
	{
		$this->db->where('sl', $id);
		return $this->db->update('pro_user_invite', $data);
	}
	
	public function checkRedundantADM($prog_id, $userid)
	{
		$this->db->where('prog_id', $prog_id);
		$this->db->where('cand_id', $userid);
		return $this->db->get('adm_can_apply')->num_rows();
	}
	
	public function getCurAdmissionProg($userid)
	{
		$curdate = date('Y-m-d');
		$this->db->select('prog.*, pa.*')->from('program prog');
		$this->db->join('prog_admission pa', 'pa.prog_id=prog.id', 'inner');
		$this->db->where('prog.type', '1');
		$this->db->where('prog.user_id', $userid);
		$this->db->where('pa.astart_date<=', $curdate);
		$this->db->where('pa.aend_date>=', $curdate);
		return $this->db->get()->result();
	}
	public function getCurLearningProg($userid)
	{
		$this->db->select('prog.*, pa.*')->from('program prog');
		$this->db->join('prog_admission pa', 'pa.prog_id=prog.id', 'left');
		$this->db->where('prog.type', '2');
		$this->db->where('prog.user_id', $userid);
		return $this->db->get()->result();
	}
	
	/********************************************************************/
	
	public function getProgramOrg($prog_id)
	{
		$this->db->select('org.*')->from('pro_organization org');
		$this->db->join('pro_map_org pmo', 'pmo.org_id=org.id', 'inner');
		$this->db->where('pmo.program_id', $prog_id);
		$this->db->order_by('org.add_date', 'DESC');
		return $this->db->get()->result();
	}
	public function getOrgByid($org_id)
	{
		$this->db->where('id', $org_id);
		return $this->db->get('pro_organization')->result_array();
	}
	public function insertProOrg($data)
	{
		$this->db->insert('pro_organization', $data);
		return $this->db->insert_id();
	}
	public function updateProOrg($data, $id)
	{
		$this->db->where('id', $id);
		return $this->db->update('pro_organization', $data);
	}
	public function deleteProOrg($org_id)
	{
		$this->db->where('id', $org_id);
		return $this->db->delete('pro_organization');
	}
	public function deleteProMapOrg($org_id)
	{
		$this->db->where('org_id', $org_id);
		return $this->db->delete('pro_map_org');
	}
	public function insertProMapOrg($data)
	{
		return $this->db->insert('pro_map_org', $data);
	}
	
	/********************************************************************/
	
	public function getProgramStreams($prog_id)
	{
		$this->db->select('strm.*, org.title as institute')->from('pro_stream strm');
		$this->db->join('pro_organization org', 'org.id=strm.org_id', 'inner');
		$this->db->join('pro_map_stream pms', 'pms.stream_id=strm.id', 'inner');
		$this->db->where('pms.program_id', $prog_id);
		$this->db->order_by('strm.add_date', 'DESC');
		return $this->db->get()->result();
	}
	public function getStreamByid($strm_id)
	{
		$this->db->where('id', $strm_id);
		return $this->db->get('pro_stream')->result_array();
	}
	public function insertProStream($data)
	{
		$this->db->insert('pro_stream', $data);
		return $this->db->insert_id();
	}
	public function insertProMapStream($data)
	{
		return $this->db->insert('pro_map_stream', $data);
	}
	public function updateProStream($data, $id)
	{
		$this->db->where('id', $id);
		return $this->db->update('pro_stream', $data);
	}
	public function deleteProStrm($strm_id)
	{
		$this->db->where('id', $strm_id);
		return $this->db->delete('pro_stream');
	}
	public function deleteProMapStrm($strm_id)
	{
		$this->db->where('stream_id', $strm_id);
		return $this->db->delete('pro_map_stream');
	}
	
	public function insertUserInvite($data)
	{
		return $this->db->insert('pro_user_invite', $data);
	}
	
	public function checkValidInvite($program_id, $user_email, $role)
	{
		$this->db->where('program_id', $program_id);
		$this->db->where('user_email', $user_email);
		$this->db->where('role', $role);
		return $this->db->get('pro_user_invite')->result();
	}
	public function getUserListNotInProgram($prog_id, $utype)
	{
		$sql = "SELECT
					first_name, last_name, email, phone 
				FROM user_auth
				WHERE user_id NOT IN (SELECT user_id FROM pro_users_role WHERE program_id=".$prog_id." AND role = '".trim($utype)."') 
				AND user_type='".trim(strtolower($utype))."'";
		return $this->db->query($sql)->result();
	}
	
	public function getUserAcademic($uid)
	{
		$this->db->select('bua.*, dg.short')->from('b2c_user_academic bua');
		$this->db->join('degree dg', 'dg.id=bua.degree_id', 'left outer');
		$this->db->where('user_id', $uid);
		return $this->db->get()->result();
	}
	
	public function getStudAdmissionListBYAF($pid)
	{
		/*if($af=='pre'){
			$this->db->select("acp.*, CONCAT(ud.first_name,' ',ud.last_name) as stud_name, ud.email, ud.phone, ud.photo_sm, ud.verification_status")->from('adm_can_apply acp');
			$this->db->join('user_auth ud', 'ud.user_id=acp.cand_id', 'inner');
		}else if($af=='final'){
			$this->db->select("acp.*, CONCAT(ud.first_name,' ',ud.last_name) as stud_name, ud.email, ud.phone, ud.photo_sm, ud.verification_status, spc.sl as spc_id, spc.enrollment_no, sps.roll_no")->from('adm_can_apply acp');
			$this->db->join('user_auth ud', 'ud.user_id=acp.cand_id', 'inner');
			$this->db->join('stud_prog_cons spc', 'spc.stud_id=acp.cand_id', 'left');
			$this->db->join('stud_prog_state sps', 'sps.spc_id=spc.sl', 'inner');
			//$this->db->where('spc.prog_id', $pid);
		}
		if($af=='0'){
			$this->db->select("acp.*, CONCAT(ud.first_name,' ',ud.last_name) as stud_name, ud.email, ud.phone, ud.photo_sm, ud.verification_status")->from('adm_can_apply acp');
			$this->db->join('user_auth ud', 'ud.user_id=acp.cand_id', 'inner');
			$this->db->where('acp.approve_flag', $af);
		}else if($af=='1'){
			
			$this->db->where('acp.approve_flag', $af);
		}else if($af==''){
			
			$this->db->join('stud_prog_cons spc', 'spc.stud_id=acp.cand_id', 'left outer');
		}*/
		$this->db->select("acp.*, CONCAT(ud.first_name,' ',ud.last_name) as stud_name, ud.email, ud.phone, ud.photo_sm, ud.verification_status")->from('adm_can_apply acp');
		$this->db->join('user_auth ud', 'ud.user_id=acp.cand_id', 'inner');
		$this->db->where('acp.prog_id', $pid);
		$this->db->order_by('acp.apply_datetime', 'DESC');
		return $this->db->get()->result();
	}
	public function getUserEnrollRoll($uid, $pid)
	{
		$this->db->select('spc.sl as spc_id, spc.enrollment_no, sps.roll_no, spc.totalfees, spc.discount, spc.payment_done')->from('stud_prog_cons spc');
		$this->db->join('stud_prog_state sps', 'sps.spc_id=spc.sl', 'left');
		$this->db->where('spc.prog_id', $pid);
		$this->db->where('spc.stud_id', $uid);
		return $this->db->get()->result();
	}
	public function getUsersArrayByACPIds($acp_ids)
	{
		$this->db->select("ud.email, CONCAT(ud.first_name,' ',ud.last_name) as name")->from('user_details ud');
		$this->db->join('adm_can_apply acp', 'acp.cand_id=ud.id', 'inner');
		$this->db->where_in('acp.sl', $acp_ids);
		return $this->db->get()->result_array();
	}
	public function getUserIdsByACPIds($acp_ids)
	{
		$this->db->select("ud.id")->from('user_details ud');
		$this->db->join('adm_can_apply acp', 'acp.cand_id=ud.id', 'inner');
		$this->db->where_in('acp.sl', $acp_ids);
		return $this->db->get()->result();
	}
	public function getCourseIdsBySemid($semid, $pid)
	{
		$this->db->select('course_id')->from('pro_map_course');
		$this->db->where('program_id', $pid);
		$this->db->where('sem_id', $semid);
		$this->db->order_by('course_id', 'ASC');
		return $this->db->get()->result();
	}
	public function getCourseIdsByProgid($pid)
	{
		$this->db->select('course_id')->from('pro_map_course');
		$this->db->where('program_id', $pid);
		$this->db->order_by('course_id', 'ASC');
		return $this->db->get()->result();
	}
	
	public function getAllRoleStatusByIdRole($userid)
	{
		$this->db->select('pur.role, pur.status, prog.title')->from('pro_users_role pur');
		$this->db->join('program prog', 'prog.id=pur.program_id', 'inner');
		$this->db->where('pur.user_id', $userid);
		$this->db->where('prog.status', 'approved');
		$this->db->where_not_in('pur.role', 'Teacher');
		$this->db->order_by('pur.add_date', 'DESC');
		return $this->db->get()->result();
	}
	
	public function getUserApplyProgStatus($userid)
	{
		$this->db->select('acp.approve_flag, acp.prog_id, prog.title, prog.feetype, prog.email, prog.mobile, prog.duration, pa.sem_type')->from('adm_can_apply acp');
		$this->db->join('program prog', 'prog.id=acp.prog_id', 'inner');
		$this->db->join('prog_admission pa', 'pa.prog_id=acp.prog_id', 'inner');
		//$this->db->join('stud_prog_cons spc', 'spc.prog_id=acp.prog_id', 'left outer');
		$this->db->where('acp.cand_id', $userid);
		return $this->db->get()->result();
	}
	public function getUserSPCData($userid, $pid)
	{
		$this->db->where('prog_id', $pid);
		$this->db->where('stud_id', $userid);
		return $this->db->get('stud_prog_cons')->result();
	}
	
	public function getProgramCourse($cid)
	{
		$this->db->select('pc.id as cid, pc.title as course_title, pc.c_code as course_code, pc.prog_id as pid, prog.title as program_title, prog.code as program_code')->from('pro_course pc');
		$this->db->join('program prog', 'prog.id=pc.prog_id', 'inner');
		$this->db->where('pc.id', $cid);
		//$this->db->where('prog.status', 'approved');
		return $this->db->get()->result();
	}
	
	public function getAllProgramBenefits($prog_id)
	{
		$this->db->where('program_id', $prog_id);
		$this->db->order_by('dat_sys_date','ASC');
		return $this->db->get('program_benefits')->result();
	}
	
	public function getTotalStudApplied($id)
	{
		$this->db->where('prog_id', $id);
		return $this->db->get('adm_can_apply')->num_rows();
	}
	
	public function getProgBenefitsByID($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('program_benefits')->result();
	}
	/*=========================================================*/
	public function updateMultiAdmApply($acp_ids, $val)
	{
		$this->db->set('approve_flag', $val);
		$this->db->set('change_flag_date', date('Y-m-d H:i:s'));
		$this->db->where_in('sl', $acp_ids);
		return $this->db->update('adm_can_apply');
	}
	public function updateFirstApproval($user_ids, $cacp, $pid, $ayear, $total_fee, $discount)
	{
		$this->db->trans_off();
		$this->db->trans_strict(false);
		
		$this->db->trans_start(true);
		for($i=0; $i<$cacp; $i++){
			$uid = $user_ids[$i]->id;
			
			$data1[$i]['stud_id'] = $uid;
			$data1[$i]['prog_id'] = $pid;
			$data1[$i]['aca_yearid'] = $ayear;
			$data1[$i]['admission_date'] = date('Y-m-d H:i:s');
			$data1[$i]['status'] = 0;
			$data1[$i]['totalfees'] = $total_fee;
			$data1[$i]['discount'] = $discount;
			$data1[$i]['add_datetime'] = date('Y-m-d H:i:s');
			$this->db->insert('stud_prog_cons', $data1[$i]);
			
			$data4[$i]['approve_flag'] = 1;
			$this->db->where('prog_id', $pid);
			$this->db->where('cand_id', $uid);
			$this->db->update('adm_can_apply', $data4[$i]);
		}
		$this->db->trans_complete();
		$trans_status = $this->db->trans_status();
		if($trans_status == FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}
	public function updateFinalApproval($user_ids, $acp_ids, $cids, $inputs, $counter)
	{
		$this->db->trans_off();
		$this->db->trans_strict(false);
		
		$this->db->trans_start();
		$semid = $inputs['semid'];
		$pid = $inputs['prog_id2'];
		if($semid!="" && $pid!=""){
			for($i=0; $i<$counter; $i++)
			{
				$uid = $user_ids[$i]->id;
				$acpid = $acp_ids[$i];
				$spcid = $inputs['spcid_'.$acpid];
				$enroll = trim($inputs['enroll_'.$acpid]);
				$roll = trim($inputs['roll_'.$acpid]);
				if($enroll!="" && $roll!="" && $uid!=""){
					//insert pro_users_role
					$data[$i]['user_id'] = $uid;
					$data[$i]['program_id'] = $pid;
					$data[$i]['add_date'] = date('Y-m-d H:i:s');
					$data[$i]['status'] = 'accepted';
					$data[$i]['role'] = 'Student';
					$this->db->insert('pro_users_role', $data[$i]); 
					//update stud_prog_cons
					$data1[$i]['enrollment_no'] = $enroll;
					$data1[$i]['status'] = '1';
					$this->db->where('sl', $spcid);
					$this->db->update('stud_prog_cons', $data1[$i]);
					//insert stud_prog_state
					$data2[$i]['stud_id'] = $uid;
					$data2[$i]['spc_id'] = $spcid;
					$data2[$i]['roll_no'] = $roll;
					$data2[$i]['aca_year'] = date('Y');
					$data2[$i]['sem_id'] = $semid;
					$data2[$i]['status'] = true;
					$data2[$i]['add_date'] = date('Y-m-d H:i:s');
					$this->db->insert('stud_prog_state', $data2[$i]); 
					$sps_id = $this->db->insert_id();
					//insert stud_prog_cons
					$j=1;
					foreach($cids as $crow){
						$data3[$i][$j]['sps_id'] = $sps_id;
						$data3[$i][$j]['course_id'] = $crow->course_id;
						$data3[$i][$j]['add_date'] = date('Y-m-d H:i:s');
						$this->db->insert('stud_prog_course', $data3[$i][$j]);
						$j++;
					}
					$data4[$i]['approve_flag'] = 2;
					$this->db->where('prog_id', $pid);
					$this->db->where('cand_id', $uid);
					$this->db->update('adm_can_apply', $data4[$i]);
				}else{
					$this->db->trans_rollback();
				}
			}
		}else{
			$this->db->trans_rollback();
		}
		$this->db->trans_complete();
		$trans_status = $this->db->trans_status();
		if($trans_status == FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}
	/*=========================================================*/
	public function getAllTeachersByPid($pid)
	{
		$this->db->select("ud.id, ud.email, CONCAT(ud.first_name,' ',ud.last_name) as name, ud.photo_sm, ud.about_me")->from('user_details ud');
		$this->db->join('pro_users_role pur', 'pur.user_id=ud.id', 'inner');
		$this->db->where('pur.program_id', $pid);
		$this->db->where('pur.role', 'Teacher');
		$this->db->where('pur.status', 'accepted');
		return $this->db->get()->result();
	}
	public function getTotalProgByUserRole($role, $userid, $pid)
	{
		$this->db->join('pro_users_role pur', 'pur.program_id=p.id', 'inner');
		$this->db->where('pur.role', $role);
		if($userid!=null){
			$this->db->where('pur.user_id', $userid);
		}
		if($pid!=null){
			$this->db->where('pur.program_id', $pid);
		}
		$this->db->where('pur.status', 'accepted');
		$this->db->where('p.status', 'approved');
		return $this->db->get('program p')->num_rows();
	}
	public function getStudentNameEmailByProgId($prog)
	{
		$this->db->select("ud.email, CONCAT(ud.first_name,' ',ud.last_name) as name")->from('user_details ud');
		$this->db->join('pro_users_role pur', 'pur.user_id=ud.id', 'inner');
		$this->db->where('pur.program_id', $prog);
		$this->db->where('pur.role', 'Student');
		$this->db->where('pur.status', 'accepted');
		return $this->db->get()->result_array();
	}
	public function getStudentNameEmailById($id)
	{
		$this->db->select("email, CONCAT(first_name,' ',last_name) as name")->from('user_details');
		$this->db->where_in('id', $id);
		return $this->db->get()->result_array();
	}
	public function getProgramCourseNameByProgId($prog, $cid)
	{
		$this->db->select('program.title as program_title, pro_course.title as course_title')->from('program');
		$this->db->join('pro_course', 'pro_course.prog_id=program.id', 'inner');
		$this->db->where('program.id', $prog);
		$this->db->where('pro_course.id', $cid);
		//$this->db->where('prog.status', 'approved');
		return $this->db->get()->result();
	}
	
	public function checkExitsCertify($pid)
	{
		$this->db->where('prog_id', $pid);
		return $this->db->get('pro_certificate')->result();
	}
	/***********************************LIVE-CLASS**********************************/
	public function insertLiveClass($data)
	{
		return $this->db->insert('live_class',$data);
	}
	
	public function updateLiveClass($cid, $data){
		$this->db->where('course_id', $cid);
		return $this->db->update('live_class', $data);
	}

	public function getLiveClassByCid($cid){
		$this->db->where('course_id', $cid);
		$this->db->where('active', true);
		return $this->db->get('live_class')->result();
	}
	
	public function getLiveClass($lid){
		$this->db->where('id', $lid);
		return $this->db->get('live_class')->result();
	}

	public function insertLiveClassInvitation($data)
	{
		return $this->db->insert('live_class_students',$data);
	}

	public function getAllLiveClassInvitationByLiveId($liveId){
		$this->db->where('live_class_id', $liveId);
		return $this->db->get('live_class_students')->result();
	}

	public function getMyInvitation($userId, $cid){
		$this->db->select('lcs.*, lc.room_name, lc.course_id')->from('live_class_students lcs');
		$this->db->join('live_class lc', 'lc.id=lcs.live_class_id', 'inner');
		$this->db->where('lc.course_id', $cid);
		$this->db->where('lc.active',true);
		$this->db->where('lcs.student_id',$userId);
		return $this->db->get()->result();
	}

	public function getAllMyInvitation($userId){
		$this->db->select('lcs.*, lc.room_name, pc.c_code')->from('live_class_students lcs');
		$this->db->join('live_class lc', 'lc.id=lcs.live_class_id', 'inner');
		$this->db->join('pro_course pc', 'pc.id=lc.course_id', 'inner');
		$this->db->where('lc.active',true);
		$this->db->where('lcs.student_id',$userId);
		return $this->db->get()->result();
	}

	public function joinMeeting($lc_id, $userId, $data)
	{
		$this->db->where('live_class_id', $lc_id);
		$this->db->where('student_id', $userId);
		return $this->db->update('live_class_students', $data);
	}

	public function takeAttendance($data)
	{
		$this->db->where('prog_id', $data['prog_id']);
		$this->db->where('course_id', $data['course_id']);
		$this->db->where('student_id', $data['student_id']);
		$this->db->where('teacher_id', $data['teacher_id']);
		$this->db->where('live_id', $data['live_id']);

		$q = $this->db->get('pro_attendance');
		if($q->num_rows() > 0){
			$result = $q->result();
			$id = $result[0]->id;
			$this->db->reset_query();
			$this->db->where('id', $id);
			$data1['m_datetime'] =  date('Y-m-d H:i:s');
			return $this->db->update('pro_attendance', $data1);
		}else{
			$this->db->reset_query();
			return $this->db->insert('pro_attendance', $data);
		}
	}

	public function getLiveClasses($userid, $from, $to){
		$this->db->where('teacher_id', $userid);
		$this->db->where('DATE(start_time) >=', $from);
		$this->db->where('DATE(start_time) <=', $to);
		$this->db->order_by('start_time', 'DESC');
		return $this->db->get('live_class')->result();
	}


	public function getAttendance($live_id)
	{
		$this->db->select("CONCAT(ud.first_name,' ',ud.last_name) as name, pa.status, lc.start_time, lcs.start_time as join_time")->from('pro_attendance pa');
		$this->db->join('user_details ud', 'ud.id=pa.student_id');
		$this->db->join('live_class lc', 'lc.id=pa.live_id');
		$this->db->join('live_class_students lcs', 'lcs.live_class_id=pa.live_id and lcs.student_id=pa.student_id');
		$this->db->where('pa.live_id', $live_id);
		return $this->db->get()->result();
	}
	/*=====================================================================================================*/
	public function getAcademicYear(){
		$this->db->select('*');
		return $this->db->get('acayear')->result_array();
	}

	public function getAcademicProgram($userid){
		$this->db->select('prog.id,prog.code,prog.title, pa.aca_year, acayear.yearnm')->from('program prog');
		$this->db->join('prog_admission pa', 'pa.prog_id=prog.id', 'inner');
		$this->db->join('acayear', 'acayear.sl=pa.aca_year', 'left');
		$this->db->where('prog.user_id', $userid);
		return $this->db->get()->result();		
	}
	public function getAcademicSemester($progId){
		$this->db->select('sem_id,title');
		$this->db->from('pro_map_sem');
		$this->db->join('pro_semister', 'pro_semister.id = pro_map_sem.sem_id');
		$this->db->where('pro_map_sem.program_id', $progId);
		return $this->db->get()->result();	
	}
	/*public function getProgramByPId($progId){
		$this->db->select('id,code,title');
		$this->db->where('id', $progId);
		return $this->db->get('program')->row_array();
	}*/

	public function getSemById($semId){
		$this->db->select('id,title');
		$this->db->where('id', $semId);
		return $this->db->get('pro_semister')->row_array();
	}

	public function getYearById($yearId){
		$this->db->select('sl,yearnm');
		$this->db->where('sl', $yearId);
		return $this->db->get('acayear')->row_array();
	}
	public function getStudentList($progId){
		//SELECT * FROM user_details JOIN pro_users_role ON pro_users_role.user_id = user_details.id JOIN stud_prog_state ON stud_prog_state.stud_id = user_details.id JOIN stud_prog_cons ON stud_prog_cons.stud_id = stud_prog_state.stud_id WHERE pro_users_role.program_id=28 AND pro_users_role.role='Student' AND stud_prog_state.sem_id=1 AND stud_prog_cons.aca_yearid=1
		
		/*$this->db->select('*');
		$this->db->from('user_details');
		$this->db->join('pro_users_role','pro_users_role.user_id = user_details.id');
		$this->db->join('stud_prog_state','stud_prog_state.stud_id = user_details.id');
		$this->db->join('stud_prog_cons','stud_prog_cons.stud_id = stud_prog_state.stud_id');
		$this->db->where('pro_users_role.program_id',$progId);
		$this->db->where('pro_users_role.role','Student');
		$this->db->where('stud_prog_state.sem_id',$semId);
		$this->db->where('stud_prog_cons.aca_yearid',$yearId);*/
		//return $this->db->get()->result();
		/*$sql = "SELECT ud.id as stud_id, CONCAT(ud.first_name,' ',ud.last_name) as name, ud.email, ud.phone, spc.sl as spc_id, spc.enrollment_no, spc.totalfees, spc.discount,spc.payment_done,spc.payment_status, spc.status, sps.sl as sps_id, spc.cgpa as sps_cgpa, spc.marks_percent as sps_percent, spc.certificate, spc.marksheet FROM user_details ud 
		INNER JOIN stud_prog_cons spc ON spc.stud_id=ud.id
		LEFT JOIN stud_prog_state sps ON sps.spc_id=spc.sl
		WHERE spc.prog_id=".$progId." AND spc.aca_yearid=".$yearId." AND sps.sem_id=".$semId." AND sps.status=true ORDER BY spc.enrollment_no ASC";
		return $this->db->query($sql)->result();*/
		/*$sql = "SELECT ud.id as stud_id, CONCAT(ud.first_name,' ',ud.last_name) as name, ud.phone, spc.enrollment_no, spc.totalfees, spc.discount,spc.payment_done,spc.payment_status, sps.percent as sps_percent,sps.cgpa as sps_cgpa, spc.status, sps.sl as sps_id, sps.percent, spc.certificate, spc.marksheet, spc.sl as spc_id FROM user_details ud INNER JOIN stud_prog_cons spc ON spc.stud_id=ud.id LEFT JOIN stud_prog_state sps ON sps.spc_id=spc.sl WHERE ud.id IN (SELECT user_id FROM pro_users_role WHERE prog_id=".$progId." AND role='Student') AND spc.aca_yearid=".$yearId." AND sps.sem_id=".$semId." AND sps.status=true ORDER BY spc.enrollment_no ASC";
		return $this->db->query($sql)->result();*/
		/*$sql = "SELECT DISTINCT(ud.id) as stud_id, CONCAT(ud.first_name,' ',ud.last_name) as name, ud.phone, spc.enrollment_no, spc.totalfees, spc.discount,spc.payment_done,spc.payment_status, sps.percent as sps_percent,sps.cgpa as sps_cgpa, spc.status, sps.sl as sps_id, sps.percent, spc.certificate, spc.marksheet, spc.sl as spc_id FROM user_details ud LEFT JOIN stud_prog_cons spc ON spc.stud_id=ud.id LEFT JOIN stud_prog_state sps ON sps.spc_id=spc.sl WHERE ud.id IN (SELECT user_id FROM pro_users_role pur INNER JOIN prog_admission pa ON pa.prog_id=pur.program_id WHERE pur.program_id=".$progId." AND role='Student' AND pa.aca_year=".$yearId.") AND sps.sem_id=".$semId." AND sps.status=true ORDER BY spc.enrollment_no ASC";
		return $this->db->query($sql)->result();*/
		$sql = "SELECT ud.id as stud_id, CONCAT(ud.first_name,' ',ud.last_name) as name, ud.phone FROM user_details ud
		INNER JOIN pro_users_role pur ON pur.user_id=ud.id
		WHERE pur.program_id=".$progId." AND role='Student'";
		return $this->db->query($sql)->result();
		//echo $sql;
	}
	public function getStudProgConsState($uid, $progId, $semId){
		$this->db->select('spc.enrollment_no, spc.totalfees, spc.discount,spc.payment_done,spc.payment_status, sps.percent as sps_percent,sps.cgpa as sps_cgpa, spc.status, sps.sl as sps_id, sps.percent, spc.certificate, spc.marksheet, spc.sl as spc_id')->from('stud_prog_cons spc');
		$this->db->join('stud_prog_state sps', 'sps.spc_id=spc.sl', 'left');
		$this->db->where('spc.stud_id', $uid);
		$this->db->where('spc.prog_id', $progId);
		$this->db->where('sps.sem_id', $semId);
		$this->db->where('sps.status', true);
		return $this->db->get()->result();
	}
	public function getSingleStudent($id){
		$sql = "SELECT ud.id as stud_id, CONCAT(ud.first_name,' ',ud.last_name) as name, ud.photo_sm, ud.phone, ud.email, spc.enrollment_no, spc.totalfees, spc.discount,spc.payment_done,spc.payment_status, sps.percent as sps_percent,sps.cgpa as sps_cgpa, spc.status, sps.sl as sps_id, sps.percent, spc.certificate, spc.marksheet, spc.sl as spc_id FROM user_details ud INNER JOIN stud_prog_cons spc ON spc.stud_id=ud.id LEFT JOIN stud_prog_state sps ON sps.spc_id=spc.sl WHERE ud.id =".$id;
		return $this->db->query($sql)->row();
	}

	public function singleStudentPercentageAndCgpa($sps_id,$stud,$sem_id,$data){
		$this->db->where('sl', $sps_id);
		$this->db->where('stud_id', $stud);
		$this->db->where('sem_id', $sem_id);
		return $this->db->update('stud_prog_state', $data);
	}

	public function singleStudentStatus($spc_id,$stud,$data){
		$this->db->where('sl', $spc_id);
		$this->db->where('stud_id', $stud);
		return $this->db->update('stud_prog_cons', $data);
	}

	public function singleStudentPercentageAndCgpaAndStatus($data1,$data2,$stud,$sps_id,$spc_id,$sem_id){
		$sspc = $this->singleStudentPercentageAndCgpa($sps_id,$stud,$sem_id,$data1);
		$sss  = $this->singleStudentStatus($spc_id,$stud,$data2);
		if($sspc && $sss){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function multipleStudentStatus($data){
		return $this->db->update_batch('stud_prog_cons', $data, 'sl');
	}
	public function studentCertificatesUpload($data,$spc_id){
		$this->db->where('sl', $spc_id);
		return $this->db->update('stud_prog_cons', $data);		
	}
	/*==========================================================*/
	public function getStudCompleteProgram($userid)
	{
		$this->db->where('stud_id', $userid);
		$this->db->where('status', '2');
		return $this->db->get('stud_prog_cons')->num_rows();
	}
	public function getStudMoreInfo($userid)
	{
		$sql = "SELECT (select count(sl) from stud_prog_cons where stud_id=".$userid." and status='2') as progcomplete, (select count(sl) from adm_can_apply where cand_id=".$userid.") as progapplied, (select sum(amount) from transaction_log where userid=".$userid." and status='captured') as totalexpense";
		return $this->db->query($sql)->result();
	}
	public function getUserLastLogin()
	{
		$this->db->select_max('logout_datetime')->from('login_track');
		$this->db->where('user_email', $_SESSION['userData']['email']);
		$this->db->where('user_id', $_SESSION['userData']['userId']);
		$this->db->where('status', '0');
		return $this->db->get()->result();
	}
	public function getStudCourseCompletionStatus($studid,$progid,$ptype,$pcertify){
		if($ptype == "3" && trim($pcertify)=="auto"){
			$this->db->select('sps.*')->from('stud_prog_self sps');
			$this->db->join('adm_can_apply aca','aca.sl = sps.adm_can_apply_id');
			$this->db->where('aca.cand_id',$studid);
			$this->db->where('aca.prog_id',$progid);
		}else{
			if(trim($pcertify)=="auto"){
				$this->db->select('sps.*')->from('stud_prog_self sps');
				$this->db->join('adm_can_apply aca','aca.sl = sps.adm_can_apply_id');
				$this->db->where('aca.cand_id',$studid);
				$this->db->where('aca.prog_id',$progid);
			}else{
				$this->db->select('*')->from('stud_prog_cons');
				$this->db->where('stud_id', $studid);
				$this->db->where('prog_id', $progid);
			}
		}
		// return $this->db->get_compiled_select();
		return $this->db->get()->result();
	}

	public function getUserAllJobs($userid)
	{
		$this->db->select('*')->from('jobs');
		$this->db->where('user_id', $userid);
		return $this->db->get()->result();
	}

	public function get_all_communities()
	{
		$this->db->select('comm.*, category.name')->from('community comm');
		$this->db->join('category', 'category.id = comm.category_id', 'inner');
		$this->db->where('user_id', $_SESSION['userData']['userId']);
		return $this->db->get()->result();
	}

	public function get_community_by_id($id)
	{
		$this->db->select('*')->from('community');
		$this->db->where('id', $id);
		return $this->db->get()->result();
	}

	public function get_all_category()
	{
		$this->db->select('*')->from('category');
		//$this->db->where('user_id', $userid);
		return $this->db->get()->result();
	}

	public function get_all_skills()
	{
		$this->db->select('id, name')->from('skills');
		return $this->db->get()->result();
	}
	
	public function get_all_user_organizations($userid)
	{
		$this->db->select('*')->from('organization');
		$this->db->where('user_id', $userid);
		return $this->db->get()->result();
	}
	public function get_user_organization_by_id($userid, $id)
	{
		$this->db->select('*')->from('organization');
		$this->db->where('user_id', $userid);
		$this->db->where('id', $id);
		return $this->db->get()->result();
	}

	// public function get_all_array_skills()
	// {
	// 	$arr = [];
	// 	$this->db->select('id, name')->from('skills');
	// 	$variable = $this->db->get()->result();
	// 	foreach ($variable as $value) {
	// 		array_push($arr, $value->id)
	// 	}
	// }
}