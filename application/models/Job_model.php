<?php 
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' ); 

class Job_model extends CI_Model {

    public function getAllJobs($id)
    {
        $this->db->select("jbs.id, jbs.org_id, CONCAT(ua.first_name,' ',ua.last_name) as user_name, jbs.title, jbs.desc, jbs.location_id, jbs.type, jbs.salary, jbs.designation_id, jbs.experience, jbs.min_qualification_id, jbs.external_link, jbs.start_time, jbs.end_time, city.name as city_name, desg.name as designation_name, degr.degree_name, degr.short, po.name as org_title")->from('jobs jbs');
        $this->db->join('user_auth ua', 'ua.user_id=jbs.user_id', 'inner');
        $this->db->join('city', 'city.id=jbs.location_id', 'left');
        $this->db->join('designation desg', 'desg.id=jbs.designation_id', 'left');
        $this->db->join('degree degr', 'degr.id=jbs.min_qualification_id', 'left');
        $this->db->join('organization po', 'po.id=jbs.org_id', 'left');
        if($id != null){
            $this->db->where('jbs.id', $id);
        }
        $this->db->order_by('jbs.create_date_time', 'DESC');
        return $this->db->get()->result();
    }

    public function setUserJobApply($job_id, $userid)
    {
        $sql = "SELECT * FROM jobs_apply WHERE jobs_id=".$job_id." AND user_id=".$userid;
        $result = $this->db->query($sql)->num_rows();
        if($result > 0){
            return 2;
        }else{
            $sql1 = "INSERT INTO jobs_apply (jobs_id, user_id, create_date_time) VALUES (".$job_id.", ".$userid.", NOW())";
            return $this->db->query($sql1);
        }
    }

    public function getUserAllJobs($userid)
    {
        $this->db->select('jbs.id, jbs.org_id, jbs.user_id, jbs.title, jbs.desc, jbs.location_id, jbs.type, jbs.salary, jbs.designation_id, jbs.experience, jbs.min_qualification_id, jbs.external_link, jbs.start_time, jbs.end_time, city.name as city_name, desg.name as designation_name, degr.degree_name, degr.short, po.name as org_title')->from('jobs jbs');
        $this->db->join('user_auth ua', 'ua.user_id=jbs.user_id', 'inner');
        $this->db->join('city', 'city.id=jbs.location_id', 'left');
        $this->db->join('designation desg', 'desg.id=jbs.designation_id', 'left');
        $this->db->join('degree degr', 'degr.id=jbs.min_qualification_id', 'left');
        $this->db->join('organization po', 'po.id=jbs.org_id', 'left');
        $this->db->where('jbs.user_id', $userid);
        return $this->db->get()->result();
    }

    public function get_job_details_by_id($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('jobs')->result();
    }

    public function getJobsApplied($job_id)
    {
        $this->db->select('first_name, last_name, email, phone')->from('user_details');
        $this->db->join('jobs_apply', 'jobs_apply.user_id=user_details.id', 'left');
        $this->db->where('jobs_apply.jobs_id', $job_id);
        $this->db->order_by('jobs_apply.create_date_time', 'DESC');
        return $this->db->get()->num_rows();
    }

    public function getUserJobsApplied($job_id)
    {
        $this->db->select('first_name, last_name, email, phone')->from('user_details');
        $this->db->join('jobs_apply', 'jobs_apply.user_id=user_details.id', 'left');
        $this->db->where('jobs_apply.jobs_id', $job_id);
        $this->db->order_by('jobs_apply.create_date_time', 'DESC');
        return $this->db->get()->result();
    }

    public function getAllLocations()
    {
        $this->db->select('id, name')->from('city');
        $this->db->order_by('name', 'ASC');
        return $this->db->get()->result();
    }

    public function getAllDesignation()
    {
        $this->db->select('id, name')->from('designation');
        $this->db->order_by('name', 'ASC');
        return $this->db->get()->result();
    }

    public function getAllProOrganization()
    {
        $this->db->select('id, name')->from('organization');
        $this->db->order_by('name', 'ASC');
        return $this->db->get()->result();
    }

    public function getAllQualification()
    {
        $this->db->select('id, degree_name, short')->from('degree');
        $this->db->order_by('degree_name', 'ASC');
        return $this->db->get()->result();
    }

    public function getAllSkills()
    {
        $this->db->select('id, name')->from('skills');
        $this->db->order_by('name', 'ASC');
        return $this->db->get()->result();
    }

    public function getAllIndustry()
    {
        $this->db->select('id, name')->from('industry');
        $this->db->order_by('name', 'ASC');
        return $this->db->get()->result();
    }
    
    public function getJobSkillsById($job_id)
    {
        $this->db->select('skill_ids')->from('jobs_skills');
        $this->db->where('job_id', $job_id);
        return $this->db->get()->row()['skill_ids'];
    }
}