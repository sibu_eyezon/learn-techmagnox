<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' ); 

class LoginModel extends CI_Model {
	
	public function insertData($tablename, $data)
	{
		return $this->db->insert($tablename, $data);
	}
	public function insertDataRetId($tablename, $data)
	{
		$this->db->insert($tablename, $data);
		return $this->db->insert_id();
	}
	public function updateData($tablename, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($tablename, $data);
	}
	public function deleteData($tablename, $where)
	{
		$this->db->where($where);
		return $this->db->update($tablename);
	}
	
	public function getCouponByCode($cp_code)
	{
		$this->db->where('coupon_code', $cp_code);
		return $this->db->get('coupons')->result();
	}
	
	public function checkChngPassStat($email, $vcode)
	{
		$this->db->where('user_id', $email);
		$this->db->where('verification_code', $vcode);
		return $this->db->get('b2c_verification_code')->result();
	}
	
	public function checkValidUser($email)
	{
		$this->db->where('email', $email);
		return $this->db->get('user_auth')->result();
	}
	
	public function checkVerifyUser($email)
	{
		$this->db->where('email', $email);
		$this->db->where('verification_status', true);
		return $this->db->get('user_auth')->result();
	}
	
	public function checkExistingUser($email)
	{
		$this->db->where('email', $email);
		return $this->db->get('purdue_reg')->num_rows();
	}
	
	public function checkExistingApply($email, $prog)
	{
		$this->db->where('user_email', $email);
		$this->db->where('program_id', $prog);
		$this->db->where('itype', 'applied');
		return $this->db->get('pro_user_invite')->result();
	}
	
	public function insertUserDRetId($data)
	{
		$this->db->insert('user_details', $data);
		return $this->db->insert_id();
	}
	
	public function insertUserAData($data)
	{
		return $this->db->insert('user_auth', $data);
	}
	
	public function insertUserAcademic($data)
	{
		return $this->db->insert('b2c_user_academic', $data);
	}
	
	public function insertUserSkills($data)
	{
		return $this->db->insert('b2c_user_skill', $data);
	}
	
	public function insertUserAdmission($data)
	{
		return $this->db->insert('adm_can_apply', $data);
	}
	
	public function insertProgUserRole($data)
	{
		return $this->db->insert('pro_users_role', $data);
	}
	
	public function checkVerifuStatus($email, $vercode)
	{
		$this->db->where('verification_status', true);
		$this->db->where('email', $email);
		$this->db->where('verification_code', $vercode);
		return $this->db->get('user_auth')->num_rows();
	}
	public function updateVerifyStatus($email, $vercode)
	{
		$this->db->set('verification_status', true);
		$this->db->where('email', $email);
		$this->db->where('verification_code', $vercode);
		return $this->db->update('user_auth');
	}
	
	public function getProgramsFiltered($title, $cat)
	{
		$this->db->select('prog.*, pa.*, acayear.yearnm')->from('program prog');
		$this->db->join('prog_admission pa', 'pa.prog_id=prog.id', 'left');
		$this->db->join('acayear', 'acayear.sl=pa.aca_year', 'left');
		if($title!=null){
			$this->db->like('prog.title', $title, 'both');
		}
		if($cat!=null){
			$this->db->where('prog.category', $cat);
		}
		//$this->db->where('prog.type', '1');
		$this->db->where('prog.status', 'approved');
		$this->db->order_by('prog.date_added', 'DESC');
		return $this->db->get()->result();
	}
	public function getProgramById($id)
	{
		$this->db->select('prog.*, pa.*, acayear.yearnm')->from('program prog');
		$this->db->join('prog_admission pa', 'pa.prog_id=prog.id', 'left');
		$this->db->join('acayear', 'acayear.sl=pa.aca_year', 'left');
		$this->db->where('prog.id', $id);
		//$this->db->where('prog.status', 'approved');
		return $this->db->get()->result();
	}
	public function getSelectivePrograms($ids)
	{
		$this->db->select('prog.*, pa.*, acayear.yearnm')->from('program prog');
		$this->db->join('prog_admission pa', 'pa.prog_id=prog.id', 'left');
		$this->db->join('acayear', 'acayear.sl=pa.aca_year', 'left');
		$this->db->where_in('prog.id', $ids);
		$this->db->where('prog.status', 'approved');
		$this->db->order_by('prog.title', 'ASC');
		return $this->db->get()->result();
	}
	public function getProgramInfoById($id)
	{
		$this->db->select("prog.*, pa.*, concat(ud.first_name,' ', ud.last_name) as uname")->from('program prog');
		$this->db->join('user_details ud', 'ud.id=prog.user_id', 'inner');
		$this->db->join('prog_admission pa', 'pa.prog_id=prog.id', 'left');
		$this->db->where('prog.id', $id);
		//$this->db->where('prog.status', 'approved');
		return $this->db->get()->result();
	}
	
	public function getAllDegrees()
	{
		$this->db->order_by('short', 'ASC');
		return $this->db->get('degree')->result();
	}
	public function getAllSkills()
	{
		$this->db->order_by('name', 'ASC');
		return $this->db->get('skills')->result();
	}
	public function getAllStreams()
	{
		$this->db->order_by('title', 'ASC');
		return $this->db->get('stream')->result();
	}
	
	public function getAllSemsByProgId($prog_id)
	{
		$this->db->select('ps.*')->from('pro_semister ps');
		$this->db->join('pro_map_sem pms', 'pms.sem_id=ps.id', 'inner');
		$this->db->where('pms.program_id', $prog_id);
		$this->db->order_by('ps.title', 'ASC');
		return $this->db->get()->result();
	}
	
	public function getProgSemCourses($sem_id, $prog_id)
	{
		$this->db->select('pc.*')->from('pro_course pc');
		$this->db->join('pro_map_course pmc', 'pmc.course_id=pc.id', 'inner');
		$this->db->where('pmc.program_id', $prog_id);
		$this->db->where('pmc.sem_id', $sem_id);
		return $this->db->get()->result();
	}
	
	public function insertStudProgAdm($prog_id, $userid, $acayear, $enroll, $pamt, $discount, $af)
	{
		$this->db->trans_off();
		$this->db->trans_strict(false);
		
		$this->db->trans_start();
		$data3['prog_id'] = $prog_id;
		$data3['cand_id'] = $userid;
		$data3['approve_flag'] = $af;
		$data3['prog_status'] = 0;
		$data3['apply_datetime'] = date('Y-m-d H:i:s');
		if($this->db->insert('adm_can_apply',$data3)){
			$data4['stud_id'] = $userid;
			$data4['prog_id'] = $prog_id;
			$data4['aca_yearid'] = $acayear;
			$data4['enrollment_no'] = $enroll;
			$data4['status'] = 0;
			$data4['totalfees'] = $pamt;
			$data4['discount'] = $discount;
			$data4['admission_date'] = date('Y-m-d H:i:s');
			$data4['add_datetime'] = date('Y-m-d H:i:s');
			if(!$this->db->insert('stud_prog_cons',$data4)){
				$this->db->trans_rollback();
			}
		}else{
			$this->db->trans_rollback();
		}
		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}
	
	public function getAllSkillByProgrm($skills_set){
		$this->db->select('name, slogo');
		$this->db->where_in('id', $skills_set);
		return $this->db->get('skills')->result();
	}

	public function getWhyLearnByProgId($program_id){
		$this->db->select('txt_benefit, txt_benefit_dtls');
		$this->db->where('program_id', $program_id);
		return $this->db->get('program_benefits')->result();
	}

	public function getAllFAQ($program_id){
		$this->db->select('txt_question, txt_answer');
		$this->db->where('yn_valid', 1);
		$this->db->where_in('program_id', $program_id);
		return $this->db->get('program_faqs')->result();

	}

	public function getContactUs($data){
		$this->db->set($data);
		return $this->db->insert('contact_us');
	}
	
	public function set_new_user_ret_id($data, $data1)
	{
		$this->db->trans_off();
		$this->db->trans_begin();
			
			$this->db->insert('user_details', $data);
			$data1['user_id'] = $this->db->insert_id();
			
			$this->db->insert('user_auth', $data1);
			
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return 0;
		}
		else
		{
			$this->db->trans_commit();
			return $data1['user_id'];
		}
	}
	
	public function setup_admission_for_user($data3, $apply_type, $data4)
	{
		$this->db->trans_off();
		$this->db->trans_begin();
		
			$this->db->insert('adm_can_apply', $data3);
			
			if($apply_type=='0'){
				$this->db->insert('stud_prog_cons', $data4);
			}
		
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return false;
		}
		else
		{
			$this->db->trans_commit();
			return true;
		}
	}
}