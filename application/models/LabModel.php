<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' ); 

class LabModel extends CI_Model {

    public function getAllLabs()
	{
		$this->db->select('*')->from('lab')->order_by('id');
		return $this->db->get()->result();
    }
    
    public function getLab($id){
        $this->db->select('*')->from('lab')
                ->where('id', $id);
        return $this->db->get()->result();
    }

    public function getCloneLab($id){
        $this->db->select('*')->from('lab l')
                ->join('lab_clone lc', 'lc.lab_id = l.id', 'inner')
                ->where('lc.id', $id);
        return $this->db->get()->result();
    }

    public function cloneLab($data){
        $this->db->insert('lab_clone', $data);
        return $this->db->insert_id();
    }

    public function getMyLabs($userid){
        $this->db->select('l.*, lc.id as lab_clone_id, lc.prog_id')->from('lab l')
                ->join('lab_clone lc', 'lc.lab_id = l.id', 'inner')
                ->where('lc.teacher_id', $userid);
        return $this->db->get()->result();
    }

    public function getUserLabs($userid){
        $this->db->select('l.*, lc.id as lab_clone_id')->from('lab l')
                ->join('lab_clone lc', 'lc.lab_id = l.id', 'inner')
                ->join('pro_users_role pur', 'pur.program_id = lc.prog_id')
                ->join('user_details ud', 'ud.id = pur.user_id')
                ->where('ud.id', $userid);
        return $this->db->get()->result();
    }

    public function addLabCloneUse($data){
        $this->db->insert("lab_clone_use", $data);
        return $this->db->insert_id();
    }

}