<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' ); 

class Exam_model extends CI_Model {
	
	public function insertDataRetId($tablename, $data)
	{
		$this->db->insert($tablename, $data);
		return $this->db->insert_id();
	}
	public function insertData($tablename, $data)
	{
		return $this->db->insert($tablename, $data);
	}
	public function updateData($tablename, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($tablename, $data);
	}
	public function deleteData($tablename, $where)
	{
		$this->db->where($where);
		return $this->db->delete($tablename);
	}
	
	public function getAllTestTypes()
	{
		$this->db->where('parent_id', 1);
		$this->db->order_by('type_name', 'ASC');
		return $this->db->get('test_type')->result();
	}
	
	public function getAllUserQuestionBanks($userid)
	{
		$this->db->select('qb.*, prog.title as prog_title, pc.title as pc_title, tt.type_name')->from('question_bank qb');
		$this->db->join('program prog', 'prog.id=qb.cat_id', 'inner');
		$this->db->join('test_type tt', 'tt.id=qb.type_id', 'inner');
		$this->db->join('pro_course pc', 'pc.id=qb.scat_id', 'left');
		$this->db->where('qb.user_id', $userid);
		$this->db->where('qb.archive_status', false);
		$this->db->order_by('create_date_time', 'DESC');
		return $this->db->get()->result();
	}
	public function getTotalQuestionUnderQBs($qid, $userid)
	{
		$this->db->where('ques_id', $qid);
		$this->db->where('user_id', $userid);
		$this->db->where('status', true);
		return $this->db->get('questions_qb_map')->num_rows();
	}
	public function getQuestionBankById($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('question_bank')->result();
	}
	public function getAllOptionsByQid($id)
	{
		$this->db->where('ques_id', $id);
		return $this->db->get('options')->result();
	}
	
	public function getQuestionById($id, $userid)
	{
		$this->db->select('ques.*, qqm.qb_id')->from('questions ques');
		$this->db->join('questions_qb_map qqm', 'qqm.ques_id=ques.id', 'inner');
		$this->db->where('ques.id', $id);
		$this->db->where('ques.user_id', $userid);
		return $this->db->get()->result();
	}
	public function getQuesOptionsById($id)
	{
		$this->db->where('ques_id', $id);
		return $this->db->get('options')->result();
	}
	public function getAllUserQuestions($qb_id, $userid)
	{
		$this->db->select('ques.*, tt.type_name, qqm.qb_id')->from('questions ques');
		$this->db->join('test_type tt', 'tt.id=ques.type_id', 'inner');
		$this->db->join('questions_qb_map qqm', 'qqm.ques_id=ques.id', 'inner');
		$this->db->join('question_bank qb', 'qb.id=qqm.qb_id', 'inner');
		if($qb_id!=""){
			$this->db->where('qqm.qb_id', $qb_id);
		}
		
		$this->db->where('ques.user_id', $userid);
		$this->db->where('ques.archive_status', false);
		$this->db->where('qb.archive_status', false);
		$this->db->order_by('ques.create_date_time', 'DESC');
		return $this->db->get()->result();
	}
	public function getAllSelectedUserQuestions($qbid, $userid, $sid)
	{
		$sql = 'SELECT 
					ques.*, tt.type_name, qqm.qb_id, sqm.ques_id 
				FROM questions ques 
				INNER JOIN test_type tt ON tt.id=ques.type_id 
				INNER JOIN questions_qb_map qqm ON qqm.ques_id=ques.id 
				INNER JOIN question_bank qb ON qb.id=qqm.qb_id 
				LEFT JOIN section_question_map sqm ON sqm.ques_id=ques.id
				WHERE qqm.qb_id='.$qbid.' 
				AND ques.user_id='.$userid.' 
				AND ques.archive_status=FALSE 
				AND qb.archive_status=FALSE 
				ORDER BY ques.create_date_time DESC';
		return $this->db->query($sql)->result();
	}
	
	public function getAllUserTests($userid, $pid, $cid)
	{
		$this->db->select('*')->from('test');
		$this->db->where('archive_status', false);
		$this->db->where('user_id', $userid);
		if($pid!=''){
			$this->db->where('cat_id', $pid);
		}
		if($cid!=''){
			$this->db->where('scat_id', $cid);
		}
		$this->db->order_by('create_date_time', 'DESC');
		return $this->db->get()->result();
	}
	public function getTestPublish($id)
	{
		$this->db->where('tp_archive', false);
		$this->db->where('test_id', $id);
		return $this->db->get('test_pub')->result();
	}
	public function getTestById($id, $userid)
	{
		$this->db->where('id', $id);
		$this->db->where('user_id', $userid);
		return $this->db->get('test')->result();
	}
	public function getTotalSectionsCount($id, $userid)
	{
		/*$this->db->select('count(sec.id) as sec_count, count(sqm.ques_id) as ques_count')->from('section sec');
		$this->db->join('section_question_map sqm', 'sqm.section_id=sec.id', 'inner');
		$this->db->where('sec.test_id', $id);
		$this->db->where('sec.user_id', $userid);
		return $this->db->get()->result();*/
		$sql = 'select 
					t1.sec_count,t2.ques_count
					from 
				(SELECT count(id) as sec_count FROM section where test_id='.$id.' and user_id='.$userid.') as t1, 
				(SELECT count(sqm.ques_id) as ques_count FROM section_question_map sqm 
				 inner join section sec on sec.id=sqm.section_id 
				 where sec.test_id = '.$id.' and sec.user_id = '.$userid.') as t2';
		return $this->db->query($sql)->result();
	}
	
	public function getAllSectionByTid($id, $userid)
	{
		$this->db->where('test_id', $id);
		$this->db->where('user_id', $userid);
		$this->db->order_by('create_section_time', 'ASC');
		return $this->db->get('section')->result();
	}
	public function getAllQuestionsBySid($id)
	{
		$this->db->select('sqm.*, ques.qbody, ques.marks, tt.type_name')->from('section_question_map sqm');
		$this->db->join('questions ques', 'ques.id=sqm.ques_id', 'inner');
		$this->db->join('test_type tt', 'tt.type_id=ques.type_id', 'inner');
		$this->db->where('sqm.section_id', $id);
		$this->db->order_by('rank', 'ASC');
		return $this->db->get()->result();
	}
	
	public function getQBsByPidCid($catid, $scatid, $userid)
	{
		$this->db->where('cat_id', $catid);
		if($scatid!=0){
			$this->db->where('scat_id', $scatid);
		}
		$this->db->where('user_id', $userid);
		return $this->db->get('question_bank')->result();
	}
	
	public function checkRedundantSecQBs($secid, $qb)
	{
		$this->db->where('sec_id', $secid);
		$this->db->where('qb_id', $qb);
		return $this->db->get('section_qb_map')->num_rows();
	}
	
	public function getTestnTestPublishDetails($tid, $userid)
	{
		$this->db->select('tt.*, tp.publish_type, tp.start_datetm,tp.end_datetm, prog.title as prog_title')->from('test tt');
		$this->db->join('test_pub tp', 'tp.test_id=tt.id', 'inner');
		$this->db->join('program prog', 'prog.id=tt.cat_id', 'inner');
		$this->db->where('tt.id', $tid);
		$this->db->where('tt.user_id', $userid);
		$this->db->where('tp.tp_archive', false);
		return $this->db->get()->result();
	}
	public function getTestnTestPubnProgIds($tid)
	{
		$this->db->select('tt.id as tt_id, tt.cat_id, tp.id as tp_id')->from('test tt');
		$this->db->join('test_pub tp', 'tp.test_id=tt.id', 'inner');
		$this->db->where('tt.id', $tid);
		$this->db->where('tp.tp_archive', false);
		return $this->db->get()->result();
	}
	
	public function getCandidateTestDetails($tp_id, $uid)
	{
		$this->db->where('test_pub_id', $tp_id);
		$this->db->where('can_id', $uid);
		return $this->db->get('runtest_can')->result();
	}

	function getAllQuestionByUser($uid){
		$this->db->select('ques.*,qqbm.qb_id,tp.type_name as type');
		$this->db->from('questions ques');
		$this->db->join('questions_qb_map qqbm', 'qqbm.ques_id = ques.id', 'left');
		$this->db->join('test_type tp','tp.type_id = ques.type_id');
		$this->db->where('ques.user_id', $uid);
		$this->db->where('qqbm.user_id', $uid);
		return $this->db->get()->result();
	}

	function getAllOptions(){
		$this->db->select('*');	
		return $this->db->get('options opts')->result();
	}
	/*--------------------------------------------------------------------------*/
	public function getSingleStudent($id){
		$sql = "SELECT ud.id as stud_id, CONCAT(ud.first_name,' ',ud.last_name) as name, ud.photo_sm, ud.phone, ud.email, spc.enrollment_no, spc.totalfees, spc.discount,spc.payment_done,spc.payment_status, sps.percent as sps_percent,sps.cgpa as sps_cgpa, spc.status, sps.sl as sps_id, sps.percent, spc.certificate, spc.marksheet, spc.sl as spc_id FROM user_details ud INNER JOIN stud_prog_cons spc ON spc.stud_id=ud.id LEFT JOIN stud_prog_state sps ON sps.spc_id=spc.sl WHERE ud.id =".$id;
		return $this->db->query($sql)->row();
	}

	public function testDetailsById($id){
		$this->db->select('tt.id as tt_id,tt.title,prog.title as prog_title,tpub.publish_type,rc.test_start_dttm,rc.test_end_dttm')->from('test tt');
		$this->db->join('program prog','prog.id=tt.cat_id');
		$this->db->join('test_pub tpub','tpub.test_id=tt.id');
		$this->db->join('runtest_can rc','rc.test_pub_id=tpub.id','left');
		$this->db->where('tt.id',$id);
		return $this->db->get()->row();
	}

	public function getSectionById($test_id){
		$this->db->select('section.id, section.test_id, section.section_name, section.type_id, section.ques_number')->from('section');
		$this->db->where('section.test_id',$test_id);
		return $this->db->get()->result();
	}

	public function allQuestionsOfTest($user_id, $test_pub_id){
		$this->db->select('ques.*,rt.sec_id,rt.ans_body,tt.type_name')->from('questions ques');
		$this->db->join('run_test rt','rt.ques_id=ques.id');
		$this->db->join('runtest_can rtc','rtc.id=rt.runtest_can_id');
		$this->db->join('test_type tt','tt.type_id=ques.type_id');
		$this->db->where('rtc.can_id',$user_id);
		$this->db->where('rtc.test_pub_id',$test_pub_id);
		return $this->db->get()->result();
	}

	public function getCorrectAnswer(){
		$this->db->select('id,body,ques_id,correct_flag')->from('options');
		$this->db->where('correct_flag',true);
		return $this->db->get()->result();
	}

	public function getGivenAnswer(){
		$this->db->select('options.id,options.body,options.ques_id,options.correct_flag')->from('options');
		$this->db->join('run_test_option rtp','rtp.option_id=options.id');
		return $this->db->get()->result();		
	}

	public function getQuestionBySection($id){
		$this->db->select('ques_id')->from('section_question_map sqm');
		$this->db->where('section_id',$id);
		return $this->db->get()->result();
	}

	public function getQuestionBankBySection($id){
		$this->db->select('qb_id')->from('section_qb_map sqbm');
		$this->db->where('sec_id',$id);
		return $this->db->get()->result();
	}

	public function newQuestionAddOfSection($data){
		return $this->db->insert_batch('section_question_map', $data);
	}

	public function updateQuestionOfSection($data,$section){
		$query = 'DELETE FROM section_question_map WHERE ques_id IN ('.$data.') AND section_id='.$section;
		return $this->db->query($query);
	}

	public function newQuestionBankAddOfSection($data){
		return $this->db->insert_batch('section_qb_map', $data);
	}

	public function updateQuestionBankOfSection($data,$section){
		$query = 'DELETE FROM section_qb_map WHERE qb_id IN ('.$data.') AND sec_id='.$section;
		return $this->db->query($query);
	}
	
	public function testReport($user_id,$test_id,$test_pub_id){
		if($this->isLoggedIn()){
			$userid = $_SESSION['userData']['userId'];
			$this->global['pageTitle'] = 'Test Details | Magnox Learning+ - Teacher';
			$data = array();
			$i    = 0;
			$data['user_details'] = $this->Member->getSingleStudent(base64_decode($user_id));
			$data['test_details'] = $this->Member->testDetailsById(base64_decode($test_id));
			$section              = $this->Member->getSectionById(base64_decode($test_id));
			$question             = $this->Member->allQuestionsOfTest(base64_decode($user_id),base64_decode($test_pub_id));
			$correct_ans          = $this->Member->getCorrectAnswer();
			$given_ans            = $this->Member->getGivenAnswer();
			$question_answer      = array();

			foreach($section as $sec){
				foreach($question as $ques){
					if($sec->id == $ques->sec_id){
						$question_answer[$sec->section_name][$i] = array(
							'question_id'      => $ques->id,
						    'section'          => $ques->sec_id,
						    'question'         => $ques->qbody,
							'difficulty_level' => $ques->difficulty_level,
							'weightage'        => $ques->weightage,
							'marks'            => $ques->marks,
							'type'             => $ques->type_name,
							'type_id'          => $ques->type_id
						);
						if($ques->type_id == 2){
							foreach($correct_ans as $ca){
								if($ques->id == $ca->ques_id){
									$question_answer[$sec->section_name][$i]['correct_answer']    = $ca->body;
									$question_answer[$sec->section_name][$i]['correct_answer_id'] = $ca->id;
									$question_answer[$sec->section_name][$i]['answer']            = !empty($ques->answer)?$ques->answer : '';
									$question_answer[$sec->section_name][$i]['hint']              = !empty($ques->hints)?$ques->hints : '';
								}
							}
							foreach($given_ans as $ga){
								if($ques->id == $ga->ques_id){
									$question_answer[$sec->section_name][$i]['given_answer'] = $ga->body;
									$question_answer[$sec->section_name][$i]['given_answer_id'] = $ga->id;
								}
							}
						}else{
							$question_answer[$sec->section_name][$i]['correct_answer'] = !empty($ques->answer)?$ques->answer : '';
							$question_answer[$sec->section_name][$i]['hint']           = !empty($ques->hints)?$ques->hints : '';
							$question_answer[$sec->section_name][$i]['given_answer']   = $ques->ans_body;
						}
					}
					$i++;
				}
			}
			$data['test_report'] = $question_answer;
			$this->loadTeacherExam("exam-report", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}		
	}
}