<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once "vendor/autoload.php";
require APPPATH . '/libraries/BaseController.php';

class Liveclass extends BaseController {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('encryption');
		$this->load->model('Member');
		$this->load->library('form_validation');
		date_default_timezone_set('Asia/Kolkata');
		ini_set('memory_limit', '-1');
	}
	public function MultipleMailSystem($users, $subject, $message)
	{
		$email = "admin@billionskills.com";
        $password = "c6spQD82CVfch7aT";

		$mail = new PHPMailer(true);
		//Enable SMTP debugging.
		$mail->SMTPDebug = 0;                               
		//Set PHPMailer to use SMTP.
		$mail->isSMTP();            
		//Set SMTP host name  
		$mail->CharSet = 'utf-8';// set charset to utf8
		//$mail->Encoding = 'base64';
		$mail->SMTPAuth = true;// Enable SMTP authentication
		$mail->SMTPSecure = 'ssl';// Enable TLS encryption, `ssl` also accepted

		$mail->Host = 'lotus.arvixe.com';// Specify main and backup SMTP servers
		$mail->Port = 465;// TCP port to connect to
		$mail->SMTPOptions = array(
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
			)
		);		                     
		//Provide username and password     
		$mail->Username = $email;                 
		$mail->Password = $password;                                                                         

		$mail->From = $email;
		$mail->FromName = 'Magnox+ Plus';

		$mail->isHTML(true);

		$mail->Subject = $subject;
		$mail->Body = $message;
		//$mail->AltBody = "This is the plain text version of the email content";
		foreach ($users as $user) {
		  $mail->addAddress($user['email'], $user['name']);

		  $mail->Body = "<h2>Hello, ".$user['name']."</h2> ".$message."</p>";
		  //$mail->AltBody = "Hello, {$user['name']}! \n How are you?";

		  try {
			  $mail->send();
			  //echo "Message sent to: ({$user['email']}) {$mail->ErrorInfo}\n";
		  } catch (Exception $e) {
			  //echo "Mailer Error ({$user['email']}) {$mail->ErrorInfo}\n";
		  }

		  $mail->clearAddresses();
		}

		return true;//($mail->send())? 1 : $mail->ErrorInfo;
	}
	public function MailSystem($to, $cc, $subject, $message)
	{
		$email = "admin@billionskills.com";
        $password = "c6spQD82CVfch7aT";

		$mail = new PHPMailer(true);
		//Enable SMTP debugging.
		$mail->SMTPDebug = 0;                               
		//Set PHPMailer to use SMTP.
		$mail->isSMTP();            
		//Set SMTP host name  
		$mail->CharSet = 'utf-8';// set charset to utf8
		//$mail->Encoding = 'base64';
		$mail->SMTPAuth = true;// Enable SMTP authentication
		$mail->SMTPSecure = 'ssl';// Enable TLS encryption, `ssl` also accepted

		$mail->Host = 'lotus.arvixe.com';// Specify main and backup SMTP servers
		$mail->Port = 465;// TCP port to connect to
		$mail->SMTPOptions = array(
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
			)
		);		                     
		//Provide username and password     
		$mail->Username = $email;                 
		$mail->Password = $password;                                                                         

		$mail->From = $email;
		$mail->FromName = 'Magnox+ Plus';

		$mail->addAddress($to);

		$mail->isHTML(true);

		$mail->Subject = $subject;
		$mail->Body = $message;
		//$mail->AltBody = "This is the plain text version of the email content";

		return ($mail->send())? 1 : $mail->ErrorInfo;
		$mail->clearAddresses();
	}
	
	/****************************************TEACHER******************************************/
	public function getLiveClasses(){
		$res = array();
		$userid = $_GET['teacher_id'];
		$from = $_GET['from'];
		$to = $_GET['to'];
		$live_classes = $this->Member->getLiveClasses($userid,$from,$to);
		$res['status'] = 'success';
		$res['data'] = $live_classes;
		echo json_encode($res);
	}

	public function startLiveClass()
	{
		$res = array();
		if($this->isLoggedIn()){
			$data['course_id'] = $_POST['course_id'];
			$data['room_name'] = $_POST['room_name'];
			$data['start_time'] = date('Y-m-d H:i:s');
			$data['teacher_id'] = $_SESSION['userData']['userId'];

			// check if there is aleray a live class
			$lclass = $this->Member->getLiveClassByCid($data['course_id']);
			if(count($lclass) == 0){
				if($this->Member->insertLiveClass($data)){
					$res['status'] = 'success';
					$res['msg'] = 'live_class inserted successfully';
					$res['room'] = $data['room_name'];
					$lclass = $this->Member->getLiveClassByCid($data['course_id']);
					$res['live_id'] = $lclass[0]->id;
				}else{
					$res['status'] = 'error';
					$res['msg'] = 'could not insert into live_class';
				}		
			}else{
				$res['status'] = 'success';
				$res['msg'] = 'Live Class is already started';
				$res['room'] = $lclass[0]->room_name;
				$res['live_id'] = $lclass[0]->id;
			}				
		}else{
			$res['status'] = 'error';
			$res['msg'] = 'not loggged in';
		}
		echo json_encode($res);
	}

	public function stopLiveClass()
	{
		$res = array();
		if($this->isLoggedIn()){
			$cid = $_POST['course_id'];
			$data['active'] = false;
			$data['end_time'] = date('Y-m-d H:i:s');

			
			if($this->Member->updateLiveClass($cid, $data)){
				$res['status'] = 'success';
				$res['msg'] = 'live_class updated successfully';
			}else{
				$res['status'] = 'error';
				$res['msg'] = 'could not update live_class';
			}				
		}else{
			$res['status'] = 'error';
			$res['msg'] = 'not loggged in';
		}
		echo json_encode($res);
	}

	// public function test(){
	// 	$live_id = $_GET['live_id'];
	// 	$invitations = $this->Member->getAllLiveClassInvitationByLiveId($live_id);
	// 	echo count($invitations);
	// }

	public function inviteStudents()
	{
		$this->encryption->initialize(array('driver' => 'openssl'));
		$res = array();
		if($this->isLoggedIn()){
			$live_id = $_POST['live_id'];
			$students = $_POST['students'];
			$prog_title = $_POST['prog_title'];
			$status = 'success';
			$msg = '';
			// get all previous invitation
			$invitations = $this->Member->getAllLiveClassInvitationByLiveId($live_id);
			$users = [];
			foreach ($invitations as $inv) {
				array_push($users, $inv->student_id);
			}
			foreach ($students as $student) {
				if(!in_array($student, $users)){
					$data['live_class_id'] = $live_id;
					$data['student_id'] = $student;
					$data['inv_time'] = date('Y-m-d H:i:s');
					if(!$this->Member->insertLiveClassInvitation($data)){					
						$status = 'error';
						$msg = "Could not insert live_class_students for user_id: ".$student;
						break;		
					}
					// // send email
					$user_detail = $this->Member->getUserDetailsById($student);
					$plain_text = $live_id.','.$student;
					$cipher_text = base64_encode($this->encryption->encrypt($plain_text));
					$link = base_url() . "Liveclass/attendClass?link=".$cipher_text;
		
					$subject = 'Live Class Invitation for program: '.$prog_title;					
					$message = 'To attend the live class please click the link below. <br>'.$link;
					$this->MailSystem($user_detail[0]->email, "", $subject, $message);					
				}				
			}
			$res['status'] = $status;
			$res['msg'] = $msg;
			if($status == 'success'){
				$res['msg'] = "All students are invited successfully";
			}
		}else{
			$res['status'] = 'error';
			$res['msg'] = 'not loggged in';
		}
		//echo json_encode($res);
	}
	
	public function takeAttendance()
	{
		$res = array();
		if($this->isLoggedIn()){
			$live_id = $_POST['live_id'];
			$cid = $_POST['course_id'];
			$userid = $_SESSION['userData']['userId'];

			// get prog id
			$progcourses = $this->Member->getProgramCourse($cid);
			$pid = $progcourses[0]->pid;

			// get all previous invitation
			$invitations = $this->Member->getAllLiveClassInvitationByLiveId($live_id);
			$data = [];
			
			foreach ($invitations as $inv) {
				if($inv->joined == 't'){
					$data['prog_id'] = $pid;
					$data['teacher_id'] = $userid;
					$data['course_id'] = $cid;
					$data['student_id'] = $inv->student_id;
					$data['a_datetime'] = date('Y-m-d H:i:s');
					$data['class_type'] = 0;
					$data['live_id'] = $live_id;
					$data['status'] = 'P';					
				}else{
					$data['prog_id'] = $pid;
					$data['teacher_id'] = $userid;
					$data['course_id'] = $cid;
					$data['student_id'] = $inv->student_id;
					$data['a_datetime'] = date('Y-m-d H:i:s');
					$data['class_type'] = 0;
					$data['live_id'] = $live_id;
					$data['status'] = 'A';
				}
				$this->Member->takeAttendance($data);
			}		
			$res['status'] = 'success';
			$res['msg'] = "Attendance recorded successfully";
			
		}else{
			$res['status'] = 'error';
			$res['msg'] = 'not loggged in';
		}
		echo json_encode($res);
	}


	public function getAttendance(){
		$res = array();
		if($this->isLoggedIn()){			
			$live_id = $_GET['live_id'];
			$attendance = $this->Member->getAttendance($live_id);	
			$res['status'] = 'success';
			$res['data'] = $attendance;
			//$res['live_id'] = $live_id;
		}else{
			$res['status'] = 'error';
			$res['msg'] = 'not loggged in';
		}
		echo json_encode($res);
	}

	public function getInvitationStatus(){
		/*$res = array();
		if($this->isLoggedIn()){
			$live_id = $_POST['live_id'];
			$invitations = $this->Member->getAllLiveClassInvitationByLiveId($live_id);
			$res['data'] = $invitations;
			$res['status'] = 'success';
		}else{
			$res['status'] = 'error';
			$res['msg'] = 'not loggged in';
		}
		echo json_encode($res);*/
		$res = array();
		if($this->isLoggedIn()){
			$live_id = $_POST['live_id'];
			$cid = $_POST['course_id'];
			$progcourse = $this->Member->getProgramCourse($cid);
			$pstud = $this->Member->getAllRequestByIdRole($progcourse[0]->pid, 'Student', NULL, 'accepted');
			$invitations = $this->Member->getAllLiveClassInvitationByLiveId($live_id);
			$inv_ids = array();
			foreach ($invitations as $key => $value) {
				array_push($inv_ids, $value->student_id);
			}
			$allInvitations = array();
			foreach($pstud as $pstd){
				$data['student_id'] = $pstd->id;
				$data['name'] = $pstd->name;
				$found = false;
				foreach ($invitations as $key => $value) {
					if($pstd->id == $value->student_id){
						$data['invited'] = 'yes';
						$data['joined'] = $value->joined;
						$found = true;
						break;
					}
				}
				if(!$found){
					$data['invited'] = 'no';
					$data['joined'] = 'f';
				}
				array_push($allInvitations, $data);
			}

			//$res['data'] = $invitations;
			$res['data'] = $allInvitations;
			$res['status'] = 'success';
		}else{
			$res['status'] = 'error';
			$res['msg'] = 'not loggged in';
		}
		echo json_encode($res);
	}
	
	/****************************************STUDENT******************************************/
	public function attendClass(){
		$this->global['pageTitle'] = 'Live Class | Magnox Learning+ - Student';
		$this->encryption->initialize(array('driver' => 'openssl'));		
		try{
			if(isset($_GET["link"])){
				$cipher_text = $_GET['link'];
				$data['link'] = $cipher_text;
			}else
				show_error("Invalid request");
			$plain_text = $this->encryption->decrypt(base64_decode($cipher_text));
			if($plain_text == false){
				show_error("Invalid request");
			}
			$array[] = str_getcsv($plain_text);
			$lc = $this->Member->getLiveClass($array[0][0]);
			if($lc[0]->active == 'f'){
				show_error("This class is over");
			}
			if(sizeof($lc) > 0){
				// join the meeting
				$data1['start_time'] = date('Y-m-d H:i:s');
				$data1['joined'] = true;
				$this->Member->joinMeeting($lc[0]->id, $array[0][1], $data1);
				$data['room_name'] = $lc[0]->room_name;
				$this->load->view("users/live-class-from-link", $data);
			}else{
				show_error("Invalid request");
			}			
		}catch(Exception $e){
			show_error("The resource you have requested is not available");
		}
	}
	
	public function getLiveClassStatus(){
		$this->encryption->initialize(array('driver' => 'openssl'));	
		$res = array();
		$res['status'] = 'error';
		$res['msg'] = 'Invalid Link';	
		try{
			if(isset($_GET["link"])){
				$cipher_text = $_GET['link'];
				$plain_text = $this->encryption->decrypt(base64_decode($cipher_text));
				if($plain_text != false){					
					$array[] = str_getcsv($plain_text);
					$lc = $this->Member->getLiveClass($array[0][0]);
					
					if(sizeof($lc) > 0){
						$res['status'] = 'success';
						unset($res['msg']);
						$res['live_class_status'] = $lc[0]->active;
					}
				}				
			}
		}catch(Exception $e){
			$res['msg'] = $e.getMessage();
		}
		echo json_encode($res);
	}
	
	public function getAllMyInvitation()
	{
		$res = array();
		$cid = null;
		$userid = $_GET['user_id'];
		if($this->isLoggedIn()){
			$inv = $this->Member->getAllMyInvitation($userid);
			if(count($inv)){
				$res['status'] = 'success';
				$res['data'] = $inv;
				$res['msg'] = 'live class started';
			}else{
				$res['status'] = 'error';
				$res['msg'] = 'No live class invitation found';
			}
		}else{
			$res['status'] = 'error';
			$res['msg'] = 'not logged in';
		}
		echo json_encode($res);
	}

	public function getMyInvitation()
	{
		$res = array();
		$cid = null;
		$cid = $_POST['course_id'];
		$userid = $_POST['user_id'];
		if($this->isLoggedIn()){
			$inv = $this->Member->getMyInvitation($userid, $cid);
			if(count($inv)){
				$res['status'] = 'success';
				$res['room_name'] = $inv[0]->room_name;
				$res['id'] = $inv[0]->live_class_id;
				$res['msg'] = 'live class started';
			}else{
				$res['status'] = 'error';
				$res['msg'] = 'No live class invitation found';
			}
		}else{
			$res['status'] = 'error';
			$res['msg'] = 'not logged in';
		}
		echo json_encode($res);
	}

	
	public function joinMeeting(){
		$res = array();
		$lc_id = $_POST['lc_id'];
		$userid = $_SESSION['userData']['userId'];
		if($this->isLoggedIn()){
			$data['start_time'] = date('Y-m-d H:i:s');
			$data['joined'] = true;
			if($this->Member->joinMeeting($lc_id, $userid, $data)){
				$res['status'] = 'success';
				$res['msg'] = 'joined the meeting successfully';
			}else{
				$res['status'] = 'error';
				$res['msg'] = 'could not join meeting';
			}
						
		}else{
			$res['status'] = 'error';
			$res['msg'] = 'not logged in';
		}
		echo json_encode($res);
	}
}