<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once "vendor/autoload.php";
require APPPATH . '/libraries/BaseController.php';

class Teacher extends BaseController {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('encryption');
		$this->load->library('form_validation');
		$this->load->library('image_lib');
		$this->load->model('Job_model', 'jm');
		date_default_timezone_set('Asia/Kolkata');
		ini_set('memory_limit', '-1');
	}
	
	function resize_image($sourcePath, $desPath, $width = '1980', $height = '900', $flag)
	{
		$this->image_lib->clear();
		$config['image_library'] = 'gd2';
		$config['source_image'] = $sourcePath;
		$config['new_image'] = $desPath;
		$config['quality'] = '100%';
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = $flag;
		$config['thumb_marker'] = '';
		$config['width'] = $width;
		$config['height'] = $height;
		//$this->load->library('image_lib', $config);
		$this->image_lib->initialize($config);
		chmod($config['new_image'], 777);
		if ($this->image_lib->resize())
			return true;
		return false;
	}
	/**********************************************************************/
	public function index()
	{
		if($this->isLoggedIn()){
			$userid = $_SESSION['userData']['userId'];
			$this->global['pageTitle'] = 'Dashboard | Magnox Learning+ - Teacher';
			//$data['active_progs'] = $this->Member->get_all_active_programs($userid);
			//$i=1;
			/*foreach($data['programs'] as $row)
			{
				$pid = $row->id;
				$data['cpsub'.$i] = $this->Member->getTotalProgCourses($pid);
				$data['pcourses'.$i] = $this->Course_model->getProgramCourses($pid);
				$i++;
			}*/
			$this->loadTeacherViews("index", $this->global, NULL, NULL);
		}else{
			redirect(base_url());
		}
	}
	
	public function getUserPrograms()
	{
		$userid = $_SESSION['userData']['userId'];
		$obj = array('active_program'=>"", 'draft_program'=>"", 'pending_program'=>"", 'archive_program'=>"");
		$active_program = $this->Member->get_filtered_user_programs('active', $userid);
		$draft_program = $this->Member->get_filtered_user_programs('draft', $userid);
		$pending_program = $this->Member->get_filtered_user_programs('pending', $userid);
		$archive_program = $this->Member->get_filtered_user_programs('archive', $userid);
		
		//print_r($active_program); exit;
		if(!empty($active_program)){
			foreach($active_program as $acp){
				//$status = trim($acp->status);
				$id = $acp->id;
				$title = trim($acp->title);
				$ptype = intval($acp->type);
				//$progType = ($ptype==4)? 'certificate' : (($ptype==3)? 'selfbased' : 'degree');
				$category = intval($acp->prog_level);
				$progLabel = ($category==3)? 'Certification' : (($category==2)? 'Mentorship' : 'Live Buddy');
				$progURI = base_url('Teacher/addProgram/certificate/?id='.base64_encode($id));
				$fees = (trim($acp->feetype)=='Paid')? 'Rs. '.trim($acp->total_fee).'/-' : 'Free';
				$stud_adm = $this->Member->getTotalStudApplied($id);
				$obj['active_program'] .= '<div class="col-sm-4">
					<div class="card card-prog mt-2">
						<div class="card-body">
							<h4 class="card-title">
								<a href="'.base_url('Teacher/viewProgram/?id='.base64_encode($id)).'" class="text-dark">'.trim($acp->code).'</a>
								<span class="label label-info pull-right">'.$progLabel.'</span>
							</h4>
							<div class="card-description">
								<h5 class="card-title font-weight-bold"><a href="'.base_url('Teacher/viewProgram/?id='.base64_encode($id)).'" class="text-dark">'.$title.'</a></h5>
								<h6>'.date('jS M Y',strtotime($acp->start_date)).' - '.date('jS M Y',strtotime($acp->end_date)).'</h6>
								<h6><span class="text-warning">'.trim($acp->prog_hrs).' Hours</span>, Fees: '.$fees.'</h6>
								<h6 class="text-danger">Deadline: '.date('jS M Y',strtotime($acp->aend_date)).'</h6>
								<h6 class="text-primary">Applied: '.$acp->student_applied.'</h6>
							</div>
						</div>
						<div class="card-footer">
							<div class="stats font-weight-bold">
								<i class="material-icons">person</i> '.$acp->student_enrolled.' Students
							</div>
							<div class="stats">
								<div class="dropdown pull-right dropup">
								  <a href="javascript:;" class="" id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="material-icons">more_horiz</i>
								  </a>';
								  if($acp->mu_id==$_SESSION['userData']['userId']){
									  $obj['active_program'] .= '<div class="dropdown-menu dropdown-menu-right prog_menu" aria-labelledby="dropdownMenu">
									  <a href="'.$progURI.'" class="btn btn-success btn-link" data-placement="bottom">Edit Program Details</a>
									  <a href="'.base_url('Teacher/viewProgram/?id='.base64_encode($id)).'" class="btn btn-success btn-link" data-placement="bottom">View Details</a>
								  </div>';
								  }
	$obj['active_program'] .= '</div>
							</div>
						</div>
					</div>
				</div>';
			}
		}
		if(!empty($draft_program)){
			foreach($draft_program as $drp){
				$id = $drp->id;
				$title = trim($drp->title);
				$ptype = intval($drp->type);
				//$progType = ($ptype==4)? 'certificate' : (($ptype==3)? 'selfbased' : 'degree');
				$category = intval($drp->prog_level);
				$progLabel = ($category==3)? 'Certification' : (($category==2)? 'Mentorship' : 'Live Buddy');
				$progURI = base_url('Teacher/addProgram/certificate/?id='.base64_encode($id));
				$fees = (trim($drp->feetype)=='Paid')? 'Rs. '.trim($drp->total_fee).'/-' : 'Free';
				
				$obj['draft_program'] .= '<div class="col-sm-4">
					<div class="card card-prog mt-2">
						<div class="card-body">
							<h4 class="card-title">
								<a href="'.base_url('Teacher/viewProgram/?id='.base64_encode($id)).'" class="text-dark">'.trim($drp->code).'</a>
								<span class="label label-info pull-right">'.$progLabel.'</span>
							</h4>
							<div class="card-description">
								<h5 class="card-title font-weight-bold"><a href="'.base_url('Teacher/viewProgram/?id='.base64_encode($id)).'" class="text-dark">'.$title.'</a></h5>
								<h6>'.date('jS M Y',strtotime($drp->start_date)).' - '.date('jS M Y',strtotime($drp->end_date)).'</h6>
								<h6><span class="text-warning">'.trim($drp->prog_hrs).' Hours</span>, Fees: '.$fees.'</h6>
								<h6 class="text-danger">Deadline: '.date('jS M Y',strtotime($drp->aend_date)).'</h6>
							</div>
						</div>
						<div class="card-footer">
							<div class="stats">
								<div class="dropdown pull-right dropup">
								  <a href="javascript:;" class="" id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="material-icons">more_horiz</i>
								  </a>';
								  if($drp->mu_id==$_SESSION['userData']['userId']){
									  $obj['draft_program'] .= '<div class="dropdown-menu dropdown-menu-right prog_menu" aria-labelledby="dropdownMenu">
									  <a href="'.$progURI.'" class="btn btn-success btn-link" data-placement="bottom">Edit Program Details</a>
									  <a href="'.base_url('Teacher/viewProgram/?id='.base64_encode($id)).'" class="btn btn-success btn-link" data-placement="bottom">View Details</a>
								  </div>';
								  }
	$obj['draft_program'] .= '</div>
							</div>
						</div>
					</div>
				</div>';
			}
		}
		if(!empty($pending_program)){
			foreach($pending_program as $pep){
				$id = $pep->id;
				$title = trim($pep->title);
				$ptype = intval($pep->type);
				//$progType = ($ptype==4)? 'certificate' : (($ptype==3)? 'selfbased' : 'degree');
				$category = intval($pep->prog_level);
				$progLabel = ($category==3)? 'Certification' : (($category==2)? 'Mentorship' : 'Live Buddy');
				$progURI = base_url('Teacher/addProgram/certificate/?id='.base64_encode($id));
				$fees = (trim($pep->feetype)=='Paid')? 'Rs. '.trim($pep->total_fee).'/-' : 'Free';
				$obj['pending_program'] .= '<div class="col-sm-4">
					<div class="card card-prog mt-2">
						<div class="card-body">
							<h4 class="card-title">
								<a href="'.base_url('Teacher/viewProgram/?id='.base64_encode($id)).'" class="text-dark">'.trim($pep->code).'</a>
								<span class="label label-info pull-right">'.$progLabel.'</span>
							</h4>
							<div class="card-description">
								<h5 class="card-title font-weight-bold"><a href="'.base_url('Teacher/viewProgram/?id='.base64_encode($id)).'" class="text-dark">'.$title.'</a></h5>
								<h6>'.date('jS M Y',strtotime($pep->start_date)).' - '.date('jS M Y',strtotime($pep->end_date)).'</h6>
								<h6><span class="text-warning">'.trim($pep->prog_hrs).' Hours</span>, Fees: '.$fees.'</h6>
								<h6 class="text-danger">Deadline: '.date('jS M Y',strtotime($pep->aend_date)).'</h6>
							</div>
						</div>
					</div>
				</div>';
			}
		}
		if(!empty($archive_program)){
			foreach($archive_program as $arp){
				$id = $arp->id;
				$title = trim($arp->title);
				$ptype = intval($arp->type);
				//$progType = ($ptype==4)? 'certificate' : (($ptype==3)? 'selfbased' : 'degree');
				$category = intval($arp->prog_level);
				$progLabel = ($category==3)? 'Certification' : (($category==2)? 'Mentorship' : 'Live Buddy');
				$progURI = base_url('Teacher/addProgram/certificate/?id='.base64_encode($id));
				$fees = (trim($arp->feetype)=='Paid')? 'Rs. '.trim($arp->total_fee).'/-' : 'Free';
				$obj['archive_program'] .= '<div class="col-sm-4">
					<div class="card card-prog mt-2">
						<div class="card-body">
							<h4 class="card-title">
								<a href="'.base_url('Teacher/viewProgram/?id='.base64_encode($id)).'" class="text-dark">'.trim($arp->code).'</a>
								<span class="label label-info pull-right">'.$progLabel.'</span>
							</h4>
							<div class="card-description">
								<h5 class="card-title font-weight-bold"><a href="'.base_url('Teacher/viewProgram/?id='.base64_encode($id)).'" class="text-dark">'.$title.'</a></h5>
								<h6>'.date('jS M Y',strtotime($arp->start_date)).' - '.date('jS M Y',strtotime($arp->end_date)).'</h6>
								<h6><span class="text-warning">'.trim($arp->prog_hrs).' Hours</span>, Fees: '.$fees.'</h6>
								<h6 class="text-danger">Deadline: '.date('jS M Y',strtotime($arp->aend_date)).'</h6>
								<h6 class="text-primary">Applied: '.$arp->student_applied.'</h6>
							</div>
						</div>
						<div class="card-footer">
							<div class="stats font-weight-bold">
								<i class="material-icons">person</i> '.$arp->student_enrolled.' Students
							</div>
						</div>
					</div>
				</div>';
			}
		}
			
		echo json_encode($obj);
	}
	
	public function myProgramRequest()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'My Program Status | Magnox Learning+ - Teacher';
			$userid = $_SESSION['userData']['userId'];
			$data['myreq'] = $this->Member->getMyRequestForPrograms($userid);
			$this->loadTeacherViews("my-request", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	
	public function invitations()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Invitations Status | Magnox Learning+ - Teacher';
			$useremail = $_SESSION['userData']['email'];
			$userid = $_SESSION['userData']['userId'];
			$data['invData'] = $this->Member->getUserInvitationsByEmail($useremail);
			$data['invResp'] = $this->Member->getAllMyInvitationRespondById($userid);
			$this->loadTeacherViews("program-invitation", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	
	public function message()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Messages | Magnox Learning+ - Teacher';
			
			$this->loadTeacherViews("messages", $this->global, NULL, NULL);
		}else{
			redirect(base_url());
		}
	}

	public function jobs()
	{
		if($this->isLoggedIn()){
			$userid = $_SESSION['userData']['userId'];
			$this->global['pageTitle'] = 'Jobs | Magnox Learning+ - Teacher';
			$data['pjobs'] = $this->jm->getUserAllJobs($userid);
			$i=1;
			foreach($data['pjobs'] as $row){
				$job_id = $row->id;
				$data['applied_'.$i] = $this->jm->getJobsApplied($job_id);
				$i++;
			}
			$this->loadTeacherViews("jobs", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function add_job()
	{
		if($this->isLoggedIn()){
			$userid = $_SESSION['userData']['userId'];
			$obj = array('id'=>0, 'org_id'=>0, 'user_id'=>0, 'title'=>"", 'desc'=>"", 'location_id'=>0, 'type'=>[],'salary'=>"",'designation_id'=>0,'experience'=>"", 'min_qualification_id'=>0, 'external_link'=>"", 'start_time'=>date('Y-m-d H:i:s'),'end_time'=>date('Y-m-d H:i:s'), 'indus_id'=>0);
			$data['city'] = $this->jm->getAllLocations();
			$data['designation'] = $this->jm->getAllDesignation();
			$data['orgs'] = $this->jm->getAllProOrganization();
			$data['skills'] = $this->jm->getAllSkills();
			$data['degrees'] = $this->jm->getAllQualification();
			$data['industry'] = $this->jm->getAllIndustry();
			if(isset($_GET['id'])){
				$this->global['pageTitle'] = 'Update Jobs | Magnox Learning+ - Teacher';
				$id = base64_decode($_GET['id']);
				$data['job'] = $this->jm->get_job_details_by_id($id);
			}else{
				$this->global['pageTitle'] = 'Add Jobs | Magnox Learning+ - Teacher';
				$data['job'][0] = (object)$obj;
			}

			$this->loadTeacherViews("add-edit-job", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function cuJobs()
	{
		$userid = $_SESSION['userData']['userId'];
		$job_id = $_POST['job_id'];
		$data['org_id'] = $_POST['organization_id'];
		$data['user_id'] = $userid;
		$data['title'] = trim($_POST['title']);
		$data['desc'] = trim($_POST['desc']);
		$data['location_id'] = $_POST['location'];
		$data['type'] = json_encode($_POST['type']);
		$data['salary'] = trim($_POST['salary']);
		$data['designation_id'] = $_POST['designation'];
		$data['experience'] = trim($_POST['experience']);
		$data['min_qualification_id'] = $_POST['degree_id'];
		$data['indus_id'] = $_POST['industry'];
		$data['start_time'] = date('Y-m-d H:i:s',strtotime($_POST['start_time']));
		$data['end_time'] = date('Y-m-d H:i:s',strtotime($_POST['end_time']));
		$data['response_flag'] = 2;

		if($job_id == 0){
			$data['create_date_time'] = date('Y-m-d H:i:s');
			$this->Exam_model->insertData('jobs', $data);
			$this->session->set_flashdata('success', 'Job has been added');
		}else{
			$data['modified_date_time'] = date('Y-m-d H:i:s');
			$where = 'id='.$job_id;
			$this->Exam_model->updateData('jobs', $data, $where);
			$this->session->set_flashdata('success', 'Job has been updated');
		}
		redirect(base_url('Teacher/jobs'));
	}
	public function deleteJob()
	{
		if(isset($_GET['pnid'])){
			$job_id = $_GET['pnid'];
			$where = 'id='.$job_id;
			echo $this->Exam_model->deleteData('jobs', $where);
		}else{
			echo false;
		}
		
	}
	public function viewAppliedUsers()
	{
		if(isset($_GET['id'])){
			$job_id = $_GET['id'];
			$users = $this->jm->getUserJobsApplied($job_id);
			if(!empty($users)){
				$i=1;
				echo '<table class="table table-responsive" width="100%">';
				echo '<thead><tr><th width="10%">Sl</th><th width="30%">Name</th><th width="30%">Email</th><th width="30%">Phone</th></tr></thead><tbody>';
				foreach($users as $row){
					echo '<tr>';
					echo '<td>'.$i.'</td>';
					echo '<td>'.$row->first_name.' '.$row->last_name.'</td>';
					echo '<td>'.$row->email.'</td>';
					echo '<td>'.$row->phone.'</td>';
					echo '</tr>';
					$i++;
				}
				echo '</tbody></table>';
				
			}else{
				echo "<h1>No data found</h1>";
			}
		}else{
			echo "<h1>No data found</h1>";
		}
	}
	
	public function organizations()
	{
		if($this->isLoggedIn()){
			$userid = $_SESSION['userData']['userId'];
			$this->global['pageTitle'] = 'Organizations| Magnox Learning+ - Teacher';
			$data['orgs'] = $this->Member->get_all_user_organizations($userid);
			$this->loadTeacherViews("organizations-list", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function addOrganization()
	{
		if($this->isLoggedIn()){
			$userid = $_SESSION['userData']['userId'];
			$this->global['pageTitle'] = 'Organizations | Magnox Learning+ - Teacher';
			if(isset($_GET['id'])){
				$data['org'] = $this->Member->get_user_organization_by_id($userid, $_GET['id']);
			}else{
				$data['org'][0] = (object)array('id'=>0, 'name'=>"", 'website'=>"", 'address'=>"", 'email'=>"", 'contact'=>"", 'details'=>"", 'linkedin'=>"", 'pan'=>"", 'gst'=>"", 'logo'=>"", 'brochure'=>"", 'banner'=>"");
			}
			
			$this->loadTeacherViews("organizations-add", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function cuOrganization()
	{
		$id = $_POST['cid'];
		
		$tmp_logo = $_FILES['logo']['tmp_name'];
		$ori_logo = $_FILES['logo']['name'];
		$logo = time().$ori_logo;
		
		$tmp_banner = $_FILES['avatar']['tmp_name'];
		$ori_banner = $_FILES['avatar']['name'];
		$banner = time().$ori_banner;
		
		$tmp_brochure = $_FILES['brochure']['tmp_name'];
		$tmp_brochure = $_FILES['brochure']['name'];
		$brochure = time().$ori_brochure;
		
		$data['name'] = trim($_POST['title']);
		$data['website'] = trim($_POST['website']);
		$data['linkedin'] = trim($_POST['linkedin']);
		$data['pan'] = trim($_POST['pan_no']);
		$data['gst'] = trim($_POST['gst_no']);
		$data['contact'] = trim($_POST['contact']);
		$data['address'] = trim($_POST['address']);
		$data['details'] = trim($_POST['details']);
		$data['status'] = true;
		$data['user_id'] = $_SESSION['userData']['userId'];
		if($ori_logo != ""){
			$data['logo'] = $logo;
			move_uploaded_file($tmp_logo, "./assets/img/institute/".$logo);
		}
		if($ori_banner != ""){
			$data['banner'] = $banner;
			move_uploaded_file($tmp_banner, "./assets/img/institute/".$banner);
		}
		if($tmp_brochure != ""){
			$data['brochure'] = $brochure;
			move_uploaded_file($tmp_brochure, "./assets/img/institute/".$brochure);
		}
		if($id == 0){
			$data['create_date_time'] = date('Y-m-d H:i:s');
			if($this->Exam_model->insertData('organization', $data)){
				$this->session->set_flashdata('success', 'Organization added successfully.');
			}else{
				$this->session->set_flashdata('errors', 'Organization addition failed.');
			}
		}else{
			$data['modified_date_time'] = date('Y-m-d H:i:s');
			if($this->Exam_model->updateData('organization', $data, 'id='.$id)){
				$this->session->set_flashdata('success', 'Organization updated successfully.');
			}else{
				$this->session->set_flashdata('errors', 'Organization updation failed.');
			}
		}
		redirect('Teacher/organizations');
	}
	public function deleteOrganization()
	{
		if(isset($_GET['clid'])){
			echo $this->Exam_model->deleteData('organization', 'id='.$_GET['clid'].' and user_id='.$_SESSION['userData']['userId']);
		}else{
			echo false;
		}
	}
	
	public function questionBank()
	{
		if($this->isLoggedIn()){
			$userid = $_SESSION['userData']['userId'];
			$this->global['pageTitle'] = 'Question Bank | Magnox Learning+ - Teacher';
			$data['programs'] = $this->Member->getAllMyPrograms('Teacher', $userid);
			$data['qbs'] = $this->Exam_model->getAllUserQuestionBanks($userid);
			$data['ttype'] = $this->Exam_model->getAllTestTypes();
			$this->loadTeacherExam("question-bank", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	
	public function test()
	{
		if($this->isLoggedIn()){
			$userid = $_SESSION['userData']['userId'];
			$this->global['pageTitle'] = 'Test | Magnox Learning+ - Teacher';
			$data['programs'] = $this->Member->getAllMyPrograms('Teacher', $userid);
			$this->loadTeacherExam("test", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function testDetails()
	{
		if($this->isLoggedIn()){
			$userid = $_SESSION['userData']['userId'];
			$data['tid'] = base64_decode($_GET['id']);
			$this->global['pageTitle'] = 'Test Details | Magnox Learning+ - Teacher';
			$data['test'] = $this->Exam_model->getTestById($data['tid'], $userid);
			
			$this->loadTeacherExam("test-details", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	
	public function addNotice()
	{
		$resp = array();
		$id = trim($_POST['pn_id']);
		$tmp = $_FILES['fl_notice']['tmp_name'];
		$ori = $_FILES['fl_notice']['name'];
		$files = uniqid().$ori;
		
		$data['program_sl'] = trim($_POST['not_prog']);
		$data['course_sl'] = trim($_POST['not_course']);
		$data['title'] = trim($_POST['nottitle']);
		if($ori!=NULL){
			$data['file_name'] = $files;
		}
		$data['userid'] = $_SESSION['userData']['userId'];
		$data['details'] = trim($_POST['notdetails']);
		$users = $this->Member->getStudentNameEmailByProgId($data['program_sl']);
		$prog = $this->Member->getProgramById($data['program_sl']);
		$subject = trim($prog[0]->title).'(Notice)';
		$message = 'A notice regarding the program: '.trim($prog[0]->title).' is as follows:<br><pre><strong>Notice: </strong>'.$data['title'].'<br><strong>Details:  </strong><br>'.$data['details'].'</pre><br><hr><br>Thanking You,<br>Magnox Learning+<br><img src="https://learn.techmagnox.com/assets/img/logo.png" style="width:100px;"/>';
		if($this->MultipleMailSystem($users, $subject, $message)){
			if($id==0){
				$data['add_date'] = date('Y-m-d H:i:s');
				if($this->Member->insertTeacherNotice($data)){
					if($ori!=NULL){
						move_uploaded_file($tmp, './uploads/notices/'.$files);
					}
					$resp = array('status'=>'success', 'msg'=>'added successfully.');
				}else{
					$resp = array('status'=>'error', 'msg'=>'added failed.');
				}
			}else{
				$data['modified_date'] = date('Y-m-d H:i:s');
				if($this->Member->updateTeacherNotice($data, $id)){
					if($ori!=NULL){
						move_uploaded_file($tmp, './uploads/notices/'.$files);
					}
					$resp = array('status'=>'success', 'msg'=>'updated successfully.');
				}else{
					$resp = array('status'=>'error', 'msg'=>'added failed.');
				}
			}
		}else{
			$resp = array('status'=>'error', 'msg'=>'added failed.');
		}
		
		echo json_encode($resp);
	}
	public function getNotices()
	{
		$userid = $_SESSION['userData']['userId'];
		$notices = $this->Member->getNoticesBYFid($userid);
		echo json_encode($notices);
	}
	public function getNoticeByID()
	{
		$id = trim($_GET['id']);
		$notices = $this->Member->getNoticesBYid($id);
		echo json_encode($notices);
	}
	public function delNotice()
	{
		$id = trim($_GET['pnid']);
		echo $this->Member->deleteNoticeById($id);
	}
	public function notices()
	{
		if($this->isLoggedIn()){
			$userid = $_SESSION['userData']['userId'];
			$this->global['pageTitle'] = 'Notices | Magnox Learning+ - Teacher';
			$data['notices'] = $this->Member->getAllNoticesByUid($userid);
			$data['programs'] = $this->Member->getAllMyPrograms('Teacher', $userid);
			$this->loadTeacherViews("notice", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function noticeDetails($nid)
	{
		if($this->isLoggedIn()){
			$userid = $_SESSION['userData']['userId'];
			$this->global['pageTitle'] = 'Notices | Magnox Learning+ - Student';
			$data['ndetails'] = $this->Member->getNoticesBYid($nid);
			$this->loadUserViews("notice-details", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	
	public function addScheduleClass()
	{
		$resp = array();
		$data['program_sl'] = trim($_POST['sch_prog']);
		$data['course_sl'] = trim($_POST['sch_course']);
		$data['class_title'] = trim($_POST['schtitle']);
		$data['class_type'] = (isset($_POST['schLine']))? 'Online' : 'Offline';
		$data['user_id'] = $_SESSION['userData']['userId'];
		$data['onlinemedium'] = trim($_POST['sch_online']);
		$data['start_datetime'] = date('Y-m-d H:i:s',strtotime($_POST['sch_start']));
		$data['end_datetime'] = date('Y-m-d H:i:s',strtotime($_POST['sch_end']));
		$data['notify'] = (isset($_POST['schNotify']))? true : false;
		$data['add_datetime'] = date('Y-m-d H:i:s');
		
		if($this->Member->insertScheduleClass($data)){
			$resp = array('status'=>'success', 'msg'=>'has been scheduled.');
		}else{
			$resp = array('status'=>'error', 'msg'=>'could not be scheduled.');
		}
		echo json_encode($resp);
	}
	
	public function checkValidCourse()
	{
		$resp = array('status'=>'error', 'msg'=>'Something went wrong.', 'cid'=>'');
		$ccode = strtoupper(trim($_POST['course_code']));
		
		if($ccode!=""){
			$course = $this->Course_model->getCourseByCode($ccode);
			if(count($course)>0){
				if(trim($course[0]->status)=='approved'){
					$resp['status']='success';
					$resp['cid']=$course[0]->id;
				}else{
					$resp['msg'] = 'is valid, but your program is not approved.';
				}
				
			}else{
				$resp['msg'] = 'is not found. Please try entering valid one.';
			}
		}else{
			$resp['msg'] = 'cannot be empty.';
		}
		echo json_encode($resp);
	}
	
	public function updateDoubts()
	{
		$resp = array('status'=>'error', 'msg'=>'Something went wrong.');
		$dbtid = $_POST['dbt_id'];
		
		$data['db_ans'] = trim($_POST['dbt_details']);
		$data['status'] = 'done';
		$data['status_ch_datetime'] = date('Y-m-d H:i:s');
		$data['ans_by'] = $_SESSION['userData']['userId'];
		
		if($dbtid!=0 && $data['db_ans']!=''){
			if($this->Course_model->updateStudentDoubts($data, $dbtid)){
				$resp = array('status'=>'success', 'msg'=>'has been answered.');
			}
		}
		
		echo json_encode($resp);
	}
	
	/*******************************INVITATION*****************************/
	public function getUserList()
	{
		$utype = trim($_GET['type']);
		$prog_id = trim($_GET['prog']);
		
		$userList = $this->Member->getUserListNotInProgram($prog_id, $utype);
		echo json_encode($userList);
	}
	public function inviteENEUser()
	{
		$resp = array('status'=>'error', 'msg'=>'Failed. Something went wrong.');
		
		$exist = trim($_POST['inv_exist']);
		$data['program_id'] = trim($_POST['inv_prog']);
		$data['user_email'] = strtolower(trim($_POST['inv_email']));
		$data['fullname'] = ucwords(strtolower(trim($_POST['inv_fname']." ".$_POST['inv_lname'])));
		$data['phone'] = trim($_POST['inv_phone']);
		$data['role'] = trim($_POST['inv_role']);
		$data['sem_id'] = trim($_POST['semid']);
		$data['acayear_id'] = trim($_POST['ay_id']);
		$data['enrollment_no'] = trim($_POST['enroll']);
		$data['roll_no'] = trim($_POST['roll']);
		$data['status'] = 'pending';
		$data['itype'] = 'invited';
		$data['invite_by'] = $_SESSION['userData']['userId'];
		$data['invite_datetime'] = date('Y-m-d H:i:s');
		
		$chkInvite = $this->Member->checkValidInvite($data['program_id'], $data['user_email'], $data['role']);
		if(empty($chkInvite)){
			$pdetails = $this->LoginModel->getProgramInfoById($data['program_id']);
			$user = $this->LoginModel->checkVerifyUser($data['user_email']);
			$uri = base_url().'Login/inviteRespondStatus/?email='.base64_encode($data['user_email']).'&prog='.base64_encode($data['program_id']).'&role='.$data['role'];
			$subject = 'Program Invitation';
			$message = '<!DOCTYPE html><html lang="en"><head><meta http-equiv="content-type" content="text/html;charset=utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /><meta content="width=device-width, initial-scale=1.0, shrink-to-fit=no" name="viewport" /><style>*{margin:0;padding:0;}body {font-size:14px;font-family: monospace;}table, th, td {border: 1px solid black;border-collapse: collapse;}th {text-align: left;}th, td {padding: 15px;}.container {width: 90%;margin: 0 auto;padding: 3rem 2rem;}</style></head><body><div class="container">Hello '.$data['fullname'].',<br>You have been invited by '.$_SESSION['userData']['name'].' as a role of a '.strtolower($data['role']).' for the program below:<br><br><img src="https://learn.techmagnox.com/assets/img/logo.png" style="width:150px;"/><br><table width="100%"><tr><th width="20%">Program</th><td width="80%">'.trim($pdetails[0]->title).'</td></tr><tr><th width="20%">Duration</th><td width="80%">'.trim($pdetails[0]->duration).' '.trim($pdetails[0]->dtype).'(s)</td></tr><tr><th width="20%">Application Start Date</th><td width="80%">'.date('jS M Y h:ia',strtotime($pdetails[0]->start_date)).'</td></tr><tr><th width="20%">Program Administrator</th><td width="80%">'.trim($pdetails[0]->uname).'</td></tr><tr><th width="20%">Program Fees</th><td width="80%">'.((trim($pdetails[0]->feetype=='Paid')? 'Rs '.trim($pdetails[0]->total_fee) : 'Free')).'</td></tr><tr><th width="20%">Program details</th><td width="80%">For more details of the program, Please | <a href="'.base_url('programDetails/?id='.base64_encode($pdetails[0]->id)).'" target="_blank">Click Here</a></td></tr></table><br>Please respond to your invitation : <a href="'.$uri.'&status=accepted" target="_blank">Accept</a> / <a href="'.$uri.'&status=rejected" target="_blank">Reject</a>';
			if(!empty($user)){
				$message.= ', OR login to your account <a href="'.base_url().'" target="_blank">here</a> and check you invitation.';
			}else{
				$message.= ' and receive you account credentials, OR register <a href="'.base_url('register/'.strtolower($data['role'])).'" target="_blank">here</a> and check you invitation after successfull login.';
			}
			$message.= '<br><br><br>Thanking you,<br>Magnox Learning Plus,<br>Learning and Course Management Platform</div></body></html>';
			if($this->MailSystem($data['user_email'], "", $subject, $message)){
				if($this->Member->insertUserInvite($data)){
					$resp = array('status'=>'success', 'msg'=>'was send successfully.');
				}else{
					$resp = array('status'=>'error', 'msg'=>'send error.');
				}
			}else{
				$resp = array('status'=>'error', 'msg'=>'Failed. Something went wrong. Please try again.');
			}
		}else{
			$resp = array('status'=>'error', 'msg'=>'was already send to this user.');
		}
		
		echo json_encode($resp);
	}
	public function applyInviteToPRogram()
	{
		$userid = $_SESSION['userData']['userId'];
		
		$chkUserRole = $this->Member->checkUserRoleOnProgram($_POST['pid'], $_POST['role'], $userid);
		$data['status'] = trim($_POST['stat']).'ed';
		$data['program_id'] = trim($_POST['pid']);
		$data['respond_datetime'] = date('Y-m-d H:i:s');
		if($this->Member->updateUserInvite($data, $_POST['puid'])){
			if($chkUserRole<=0){
				$data1['program_id'] = trim($_POST['pid']);
				$data1['role'] = trim($_POST['role']);
				$data1['add_date'] = $data1['status_ch_date'] = date('Y-m-d H:i:s');
				$data1['user_id'] = $userid;
				$data1['status'] = 'accepted';
				$this->Member->insertUserRole($data1);
			}
			echo true;
		}else{
			echo false;
		}
	}
	
	public function removeProfessor()
	{
		$where = "user_id=".$_POST['user_id']." AND program_id=".$_POST['prog_id']." AND role='Teacher'";
		echo $this->Exam_model->deleteData('pro_users_role', $where);
	}
	/************************************************************************************ */
	public function studAdmission($type){
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Student  Admission| Magnox Learning+ - Teacher';
			$userid = $_SESSION['userData']['userId'];
			$data['atype'] = $type;
			$data['academic_year'] = $this->Member->getAcademicYear();	
			$data['progs'] = $this->Member->getAcademicProgram($userid);
			$this->loadTeacherViews("stud-admission", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function getProgAdmissionList()
	{
		$pid = trim($_GET['pid']);
		$data['atype'] = trim($_GET['atype']);
		$data['prog'] = $this->Member->getProgramById($pid);
		$data['adm_list'] = $this->Member->getStudAdmissionListBYAF($pid);
		if($data['atype']=='pre'){
			//$data['adnpd_list'] = $this->Member->getStudAdmissionListBYAF($pid, '0');
			//$data['adnpd_list'] = $this->Member->getStudAdmissionListBYAF($pid);
			$data['ayr'] = $this->Member->getAllActiveAcaYear();
			$i=1;
			foreach($data['adm_list'] as $row){
				$uid = $row->cand_id;
				$data['udaca1_'.$i] = $this->Member->getUserAcademic($uid);
				$i++;
			}
			return $this->load->view('teachers/stud-adm-pending', $data);
		}else if($data['atype']=='final'){
			//$data['adnfa_list'] = $this->Member->getStudAdmissionListBYAF($pid);
			
			$data['sems'] = $this->Course_model->getAllSemestersByProgId($pid);
			$i=1;
			foreach($data['adm_list'] as $row){
				$uid = $row->cand_id;
				$data['uroll_'.$i] = $this->Member->getUserEnrollRoll($uid, $pid);
				$i++;
			}
			return $this->load->view('teachers/stud-adm-final', $data);
			
			/*$data['reg_users'] = $this->Member->getPurdueRegs();
			return $this->load->view('teachers/stud-adm-purdue', $data);*/
		}
	}
	//first approval status
	public function firstSelectedStud()
	{
		$pid = $_POST['prog_id1'];
		$ftype = $_POST['feetype1'];
		$acp_ids = $_POST['acp1'];
		$cacp = count($acp_ids);
		
		$users = $this->Member->getUsersArrayByACPIds($acp_ids);
		$user_ids = $this->Member->getUserIdsByACPIds($acp_ids);
		$prog = $this->Member->getProgramById($pid);
		$ayear = $prog[0]->aca_year;
		$subject = trim($prog[0]->title)." | Pre-Admission Notice";
		$message = 'You have been selected in 1st merit list for the program: '.trim($prog[0]->title).'.<br>';
		if($ftype==1){
			$message.='Please pay the amount within last date of Admission or else your application will be rejected.';
		}else{
			$message.='Please wait for final approval from the teacher.';
		}
		$message.='<br>Log in to your dashboard <a href="'.base_url().'" target="_blank">here</a> for more details.';
		$message.='<hr><br>All the best.<br>Thanking You,<br>Magnox Learning+<br><img src="https://learn.techmagnox.com/assets/img/logo.png" style="width:100px;"/>';
		
		if($this->Member->updateFirstApproval($user_ids, $cacp, $pid, $ayear, (int)trim($prog[0]->total_fee), (int)trim($prog[0]->discount))){
			//$this->Member->updateMultiAdmApply($acp_ids, 1);
			$this->MultipleMailSystem($users, $subject, $message);
			echo true;
		}else{
			echo false;
		}
	}
	public function rejectPendingSelectedStud()
	{
		$pid = $_POST['prog_id1'];
		$acp_ids = $_POST['acp1'];
		//$count = count($acp_ids);
		$prog = $this->Member->getProgramById($pid);
		$subject = "Program Rejection Notice";
		$users = $this->Member->getUsersArrayByACPIds($acp_ids);
		$message = 'The admission for the program: '.trim($prog[0]->title).', in which you have applied, has been rejected.<br>Thanking You,<br>Magnox Learning+';
		if($this->MultipleMailSystem($users, $subject, $message)){
			//$this->Member->deleteMultipleDataById('adm_can_apply', 'sl', $acp_ids);
			$this->Member->updateMultiAdmApply($acp_ids, 3);
			echo true;
		}else{
			echo false;
		}
	}
	//final approval
	public function finalSelectedStud()
	{
		$data = [];
		$data1 = [];
		$data2 = [];
		$data3 = [];
		$mcount = 0;
		$pid = $_POST['prog_id2'];
		$acp_ids = $_POST['acp2'];
		$ftype = $_POST['feetype2'];
		$semid = $_POST['semid'];
		$counter = count($acp_ids);
		
		$prog = $this->Member->getProgramById($pid);
		$users = $this->Member->getUsersArrayByACPIds($acp_ids);
		$user_ids = $this->Member->getUserIdsByACPIds($acp_ids);
		$cids = $this->Member->getCourseIdsByProgid($pid);
		$subject = trim($prog[0]->title)." | Final Admission Notice";
		
		$message = 'Congratulation Learner,<br>Your admission for this program is complete. Your Course will start from '.date('d/m/Y',strtotime($prog[0]->start_date)).' at '.date('h:i A',strtotime($prog[0]->start_date)).'.<br>Log in to your dashboard <a href="'.base_url().'" target="_blank">here</a> to start your program.<hr><br>Thanking You,<br>Magnox Learning+<br><img src="https://learn.techmagnox.com/assets/img/logo.png" style="width:100px;"/>';
		//print_r($_POST);exit;
		if(!empty($cids)){
			if($this->Member->updateFinalApproval($user_ids, $acp_ids, $cids, $_POST, $counter)){
				$this->MultipleMailSystem($users, $subject, $message);
				echo '1';
			}else{
				echo '0';
			}
		}else{
			echo '2';
		}
	}
	public function rejectApprovedSelectedStud()
	{
		$pid = $_POST['prog_id2'];
		$acp_ids = $_POST['acp2'];
		$counter = count($acp_ids);
		$prog = $this->Member->getProgramById($pid);
		$subject = "Program Rejection Notice";
		$users = $this->Member->getUsersArrayByACPIds($acp_ids);
		$message = 'The admission for the program: '.trim($prog[0]->title).', in which you have applied, has been rejected.<br>Thanking You,<br>Magnox Learning+';
		if($this->MultipleMailSystem($users, $subject, $message)){
			/*$this->Member->deleteMultipleDataById('adm_can_apply', 'sl', $acp_ids);
			for($i=0; $i<$counter; $i++){
				$acpid = $acp_ids[$i];
				$where = 'sl='.$_POST['spcid_'.$acpid];
					$this->Exam_model->deleteData('stud_prog_cons', $where);
			}*/
			$this->Member->updateMultiAdmApply($acp_ids, 3);
			echo true;
		}else{
			echo false;
		}
	}
	public function studentMaster(){
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Student Master | Magnox Learning+ - Teacher';
			$userid = $_SESSION['userData']['userId'];
			$data['academic_year'] = $this->Member->getAcademicYear();
			$data['academic_program'] = $this->Member->getAcademicProgram($userid);
			$this->loadTeacherViews("stud-master", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}		
	}

	public function getAcademicSemester(){
		$userid = $_SESSION['userData']['userId'];
		$progId = $_POST['programId'];
		$sem_list = $this->Member->getAcademicSemester($progId);
		$output = '<option value="" disabled>Set semester</option>';
		if($sem_list){
			foreach($sem_list as $row){
				$output .='<option value='.$row->sem_id.'>'.$row->title.'</option>';
			}
		}

		echo $output;
	}

	public function studentMasterList(){
		$output       = '';
		$progId       = $_POST['acaprog'];
		$semId        = $_POST['acasem'];
		//$yearId       = $_POST['acayear'];
		$userid       = $_SESSION['userData']['userId'];//,$semId,$yearId
		$student_list = $this->Member->getStudentList($progId);
		if(!empty($student_list)){
			$output = $this->studentMasterTablePrint($student_list, $progId, $semId);
		}
		echo $output;
	}

	public function studentMasterTablePrint($student_list, $progId, $semId){
		$result = "";$i=1;
		
		foreach($student_list as $data){
			$stud_id = $data->stud_id;
			$stud_adm[$i] = $this->Member->getStudProgConsState($stud_id, $progId, $semId);
			if(!empty($stud_adm[$i])){
				//$stud_id = $data->stud_id;
				$spc_id = $stud_adm[$i][0]->spc_id;
				$sps_id = $stud_adm[$i][0]->sps_id;
				$enrollment_no = $stud_adm[$i][0]->enrollment_no;
				$status = $stud_adm[$i][0]->status;
				$sps_cgpa = $stud_adm[$i][0]->sps_cgpa;
				$sps_percent = $stud_adm[$i][0]->sps_percent;
				$totalfees = $stud_adm[$i][0]->totalfees;
				$certificate = trim($stud_adm[$i][0]->certificate);
				$result.= '<tr id="row'.$stud_id.'">
					<td><input type="checkbox" class="checkbox" onClick="clickChangeEvent()" value="'.$spc_id.'"></td>
					<td>'.$data->name.'</td>
					<td>'.$enrollment_no.'</td>
					<td>'.$data->phone.'</td>
					<td>
						<div class="form-group">
							<input type="text" class="form-control" id="parce'.$stud_id.'" name="parce" data-id="'.$sps_id.'" value="'.((!empty($sps_percent))? $sps_percent:"").'"> 
						</div>
					</td>
					<td>
						<div class="form-group">
							<input type="text" class="form-control" id="cgpa'.$stud_id.'" name="cgpa"  value="'.((!empty($sps_cgpa))? $sps_cgpa:"").'">
						</div>
					</td>
					<td>'.((intval($totalfees)==0)?'Free':$totalfees).'</td>
					<td>
						<div class="form-group">
							<select class="form-control" id="status'.$stud_id.'" name="status" data-id="'.$spc_id.'">
								<option value="1" '.(($status == 1)?'selected="selected"':"").'>Started</option>
								<option value="2" '.(($status == 2)?'selected="selected"':"").'>Completed</option>
								<option value="3" '.(($status == 3)?'selected="selected"':"").'>Failed</option>
							</select>
						</div>
					</td>
					<td>';
					
					if($certificate!=""){
						if(file_exists('./'.$certificate)){
							$result.= '<a href="'.base_url($certificate).'" target="_blank"><i class="material-icons">cloud_download</i> Click</a>';
						}else{
							if($status == 1){
								$result.= '<a href="javascript:;" onClick="$(`#myFile'.$spc_id.'`).click();"><i class="material-icons">cloud_upload</i> Upload</a>';
							}
						}
					}else{
						if($status == 1){
							$result.= '<a href="javascript:;" onClick="$(`#myFile'.$spc_id.'`).click();"><i class="material-icons">cloud_upload</i> Upload</a>';
						}
					}
				$result.= '<input type="file" class="btn btn-success btn-sm" id="myFile'.$spc_id.'" onchange="certificateUpload('.$spc_id.','.$stud_id.')" style="display:none;" accept="application/pdf, image/jpg, image/png, image/jpeg">
					</td>
					<td><button class="btn btn-success btn-sm" onClick="singleStudentChange('.$stud_id.')">Save</button></td>
				</tr>';
			}
		$i++;
		}
		return $result;
	}
	public function multipleStudentChange(){
		$status = $_POST['status'];
		$spc_id = explode(",",$_POST['spc_id']);
		$data   = array();

		foreach ($spc_id  as $row) {
			$data[] = array(
				'sl' => $row,
				'status' => $status,
				'completion_date'=>date('Y-m-d H:i:s')
			);
		}

		echo $this->Member->multipleStudentStatus($data);
	}
	/*----------------------------------------------*/
	public function singleStudentChange(){
		$output       = '';
		$stud         = $_POST['stud'];
		$percent      = (!empty($_POST['percent']))?(int)($_POST['percent']) : 0;
		$cgpa         = (!empty($_POST['cgpa']))?(int)($_POST['cgpa']) : 0;
		$sps_id       = $_POST['sps_id'];
		$status       = $_POST['status'];
		$spc_id       = $_POST['spc_id'];
		$sem_id       = $_POST['sem_id'];

		$data1['percent'] = $percent;
		$data1['cgpa']    = $cgpa;
		$data2['status']  = $status; 
		$data2['completion_date']  = date('Y-m-d H:i:s'); 

		$student_list = $this->Member->singleStudentPercentageAndCgpaAndStatus($data1,$data2,$stud,$sps_id,$spc_id,$sem_id);

		if($student_list){
			echo TRUE;
		}else{
			echo FALSE;
		}
	}
	public function getSingleStudentMaster(){
		$id     = $_POST['stud'];
		$data   = $this->Member->getSingleStudent($id);
		$result = '<td>
					<input type="checkbox" class="checkbox" onClick="clickChangeEvent()" value="'.$data->spc_id.'">
				</td>
				<td>'.$data->name.'</td>
				<td>'.$data->enrollment_no.'</td>
				<td>'.$data->phone.'</td>
				<td>
					<div class="form-group">
						<input type="text" class="form-control" id="parce'.$data->stud_id.'" name="parce" data-id="'.$data->sps_id.'" value="'.((!empty($data->sps_percent))?$data->sps_percent:"").'"> 
					</div>
				</td>
				<td>
					<div class="form-group">
						<input type="text" class="form-control" id="cgpa'.$data->stud_id.'" name="cgpa"  value="'.((!empty($data->sps_cgpa))?$data->sps_cgpa:"").'">
					</div>
				</td>
				<td>'.((intval($data->totalfees)==0)?'Free':$data->totalfees).'</td>
				<td>
					<div class="form-group">
						<select class="form-control" id="status'.$data->stud_id.'" name="status" data-id="'.$data->spc_id.'">
							<option value="1" '.(($data->status == 1)?'selected="selected"':"").'>Started</option>
							<option value="2" '.(($data->status == 2)?'selected="selected"':"").'>Completed</option>
							<option value="3" '.(($data->status == 3)?'selected="selected"':"").'>Failed</option>
						</select>
					</div>
				</td>
				<td>';
					$certificate = trim($data->certificate);
					if($certificate!=""){
						if(file_exists('./'.$certificate)){
							$result .= '<a href="'.base_url($certificate).'" target="_blank"><i class="material-icons">cloud_download</i> Click</a>';
						}else{
							if($data->status == 1){
								$result .= '<a href="javascript:;" onClick="$(`#myFile'.$data->spc_id.'`).click();"><i class="material-icons">cloud_upload</i> Upload</a>';
							}
						}
					}else{
						if($data->status == 1){
							$result .= '<a href="javascript:;" onClick="$(`#myFile'.$data->spc_id.'`).click();"><i class="material-icons">cloud_upload</i> Upload</a>';
						}
					}
			$result .= '<input type="file" class="btn btn-success btn-sm" id="myFile'.$data->spc_id.'" onchange="certificateUpload('.$data->spc_id.','.$data->stud_id.')" style="display:none;" accept="application/pdf, image/jpg, image/png, image/jpeg">';
			$result .= '</td>
				<td><button class="btn btn-success btn-sm" onClick="singleStudentChange('.$data->stud_id.')">Save</button></td>
		';
		echo $result;
    }
	/*---------------------------------*/
	public function studentCertificatesUpload(){
		$tmp = $_FILES['file']['tmp_name'];
		$name = $_FILES['file']['name'];
		if($name!=''){
			move_uploaded_file($tmp, './uploads/certificates/'.$name);
			$data['certificate'] = 'uploads/certificates/'.$name;
			$spc_id    = $_POST['spc_id'];
			return $this->Member->studentCertificatesUpload($data,$spc_id);
		}else{
			return false;
		}
	}
	/*******************************PROGRAM********************************/
	public function allPrograms()
	{
		if($this->isLoggedIn()){
			$userid = $_SESSION['userData']['userId'];
			$this->global['pageTitle'] = 'All Programs | Magnox Learning+ - Teacher';
			$data['programs'] = $this->Member->getAllProgramsNotUser('Teacher', $userid);

			$i=1;
			foreach($data['programs'] as $row)
			{
				$pid = $row->id;
				$data['cpprof'.$i] = $this->Member->getTotalProgByUserRole('Teacher', NULL, $pid);
				$data['cpstud'.$i] = $this->Member->getTotalProgByUserRole('Student', NULL, $pid);
				$data['cpsub'.$i] = $this->Member->getTotalProgCourses($pid);
				$data['org'.$i] = $this->Member->getProgOrganizations($pid);
				$i++;
			}
			$this->loadTeacherViews("all-programs", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function viewProgramDetails()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Programs Details | Magnox Learning+ - Teacher';
			$userId = $_SESSION['userData']['userId'];
			$data['prog_id'] = base64_decode($_GET['id']);
			$data['prog'] = $this->Member->getProgramById($data['prog_id']);
			$data['institute'] = $this->Member->getProgramOrg($data['prog_id']);
			$data['stream'] = $this->Member->getProgramStreams($data['prog_id']);
			$data['pprof'] = $this->Member->getAllRequestByIdRole($data['prog_id'], 'Teacher', NULL, 'accepted');
			$data['procourse'] = $this->Course_model->getProgramCourses($data['prog_id']);
			$data['cprof'] = $this->Member->getProgRoleRequest('Teacher', $data['prog_id'], $userId);
			$data['cstud'] = $this->Member->getProgRoleRequest('Student', $data['prog_id'], $userId);
			
			$this->loadTeacherViews("view-programDetails", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function addProgram($progtype = "selfbased")
	{
		if($this->isLoggedIn()){
			$data['progtype'] = $progtype;
			$type = ($progtype == 'selfbase')? 3 : (($progtype == 'certificate')? 4 : 1);
			$category = ($progtype == 'selfbased')? 'Automatic Program' : '';
			$userid = $_SESSION['userData']['userId'];
			$data['pcategory'] = $this->Admin_model->getParentCategories(0);
			$data['ayr'] = $this->Member->getAllActiveAcaYear();
			if($progtype == 'degree'){
				$obj = array('id'=>0,'code'=>"",'title'=>"",'type'=>$type,'category'=>"",'duration'=>"",'start_date'=>date('Y-m-d H:i:s'),'end_date'=>date('Y-m-d H:i:s'),'user_id'=>"",'status'=>"pending",'total_fee'=>"",'fee_details'=>"",'total_credit'=>"",'overview'=>"",'email'=>"",'mobile'=>"",'facebook'=>"",'linkedin'=>"",'twitter'=>"",'student_enroll'=>"",'teacher_enroll'=>"", 'feetype'=>"", 'dtype'=>"", 'apply_type'=>"", 'astart_date'=>date('Y-m-d'), 'aend_date'=>date('Y-m-d'), 'criteria'=>"", 'ptype'=>"", 'placement'=>"", 'apply_status'=>"", 'sem_type'=>"", 'discount'=>0, 'prog_hrs'=>0, 'total_seat'=>0, 'aca_year'=>0, 'prog_subcategory'=>0, 'prog_level'=>'0');
				if(isset($_GET['id'])){
					$id = base64_decode(trim($_GET['id']));
					$this->global['pageTitle'] = 'Update Degree Program | Magnox Learning+ - Teacher';
					$data['prog'] = $this->Member->getProgramById($id);
				}else{
					
					$this->global['pageTitle'] = 'Add Degree Program | Magnox Learning+ - Teacher';
					$data['prog'][0] = (object)$obj;
				}
				$this->loadTeacherViews("add-degree-program", $this->global, $data, NULL);
			}else{
				$obj = array('id'=>0,'code'=>"",'title'=>"",'type'=>$type,'category'=>$category,'duration'=>"",'start_date'=>date('Y-m-d H:i:s'),'end_date'=>date('Y-m-d H:i:s'),'user_id'=>"",'status'=>"pending",'total_fee'=>"",'fee_details'=>"",'total_credit'=>"",'overview'=>"",'email'=>"",'mobile'=>"",'facebook'=>"",'linkedin'=>"",'twitter'=>"",'student_enroll'=>"",'teacher_enroll'=>"", 'feetype'=>"", 'dtype'=>"", 'apply_type'=>"0", 'astart_date'=>date('Y-m-d'), 'aend_date'=>date('Y-m-d'), 'criteria'=>"", 'ptype'=>"Online", 'placement'=>"", 'apply_status'=>"", 'sem_type'=>"", 'discount'=>0, 'prog_hrs'=>0, 'total_seat'=>0, 'aca_year'=>0, 'prog_subcategory'=>0, 'prog_level'=>'0');
				if(isset($_GET['id'])){
					$id = base64_decode(trim($_GET['id']));
					$data['prog_id'] = $id;
					$this->global['pageTitle'] = 'Update '.ucfirst($progtype).' Program | Magnox Learning+ - Teacher';
					$data['prog'] = $this->Member->getProgramById($id);
					$this->loadTeacherViews("update-".$progtype."-program", $this->global, $data, NULL);
				}else{
					$this->global['pageTitle'] = 'Add '.ucfirst($progtype).' Program | Magnox Learning+ - Teacher';
					$data['prog'][0] = (object)$obj;
					$this->loadTeacherViews("add-self-program", $this->global, $data, NULL);
				}
			}
		}else{
			redirect(base_url());
		}
	}
	public function generateProgramCode($title){
		$ret = '';
		foreach (explode(' ', $title) as $word){
			$ret .= strtoupper($word[0]);
		}
		return $ret;
	}
	public function cuProgram($progtype = "selfbased")
	{
		$type = ($progtype == 'selfbase')? 3 : (($progtype == 'certificate')? 4 : 1);
		
		$status = trim($_POST['status']);
		$data['title'] = html_escape(trim($_POST['title']));
		$data['code'] = $this->generateProgramCode($data['title']).date('yds');
		$data['type'] = trim($_POST['type']);
		$data['category'] = trim($_POST['category']);
		$data['user_id'] = $_SESSION['userData']['userId'];
		$data['feetype'] = trim($_POST['feetype']);
		if($data['feetype']=='Paid'){
			$data['total_fee'] = trim($_POST['fees']);
			$data2['discount'] = (trim($_POST['rebate'])!="")? trim($_POST['rebate']):0;
		}else{
			$data['total_fee'] = '';
			$data2['discount'] = 0;
		}
		if($progtype=='degree'){
			$data['bolcertify'] = 'manual';
		}else{
			$data['bolcertify'] = 'no';
		}
		$data['student_enroll'] = 1;
		$data['teacher_enroll'] = 1;
		$data['email'] = strtolower(trim($_POST['email']));
		$data['mobile'] = trim($_POST['phone']);
		
		$data2['prog_hrs'] = (trim($_POST['total_hrs'])!='')? trim($_POST['total_hrs']) : 0;
		$data2['total_seat'] = (trim($_POST['total_seat'])!='')? trim($_POST['total_seat']) : 0;
		$data2['ptype'] = trim($_POST['prog_type']);
		$data2['prog_level'] = trim($_POST['prog_level']);
		$data2['prog_subcategory'] = trim($_POST['prog_subcategory']);
		$data['status'] = 'drafted';
		$data['date_added'] = date('Y-m-d H:i:s');
		if($progtype=='selfbased')
		{
			//SELF-BASED
			$data2['apply_status'] = true;
			$data2['apply_type'] = 0;
			
			$this->db->trans_off();
			$this->db->trans_strict(false);
			
			$this->db->trans_start();
				$prog = $this->Member->insertProgram($data);
				$data1['program_id'] = $prog;
				$data1['user_id'] = $_SESSION['userData']['userId'];
				$data1['role'] = 'Teacher';
				$data1['status'] = 'accepted';
				$data1['add_date'] = date('Y-m-d H:i:s');
				$this->Member->insertUserRole($data1);
				
				$data2['prog_id'] = $prog;
				$this->Member->insertAdmission($data2);
			$this->db->trans_complete();
			$trans_status = $this->db->trans_status();
			if($trans_status == FALSE){
				$this->db->trans_rollback();
				$this->session->set_flashdata('error', ucfirst($progtype).' program insert error.');
			}else{
				$this->db->trans_commit();
				$this->session->set_flashdata('error', ucfirst($progtype).' program was added successfully.');
			}
			
			//redirect(base_url().'Teacher/addProgram/selfbased/?id='.base64_encode($prog));
		}else{
			//CERTIFICATE + DEGREE
			//$data['total_credit'] = trim($_POST['credit']);
			$data['overview'] = trim($_POST['overview']);
			$data['curriculam'] = "";
			$data['selection_procedure'] = "";
			$dur = (int)(trim($_POST['duration']));
			$data['duration'] = $dur;
			$data['dtype'] = trim($_POST['dtype']);
			$data['start_date'] = date('Y-m-d H:i:s',strtotime($_POST['sdate']));
			if(isset($_POST['edate'])){
				$data['end_date'] = date('Y-m-d H:i:s',strtotime($_POST['edate']));
			}
			$data['overview'] = trim($_POST['overview']);
			$data['curriculam'] = "";
			$data['selection_procedure'] = "";
			$data2['apply_type'] = trim($_POST['apply_type']);
			$data2['criteria'] = trim($_POST['criteria']);
			if($progtype == 'degree'){
				if($data['dtype']=='year'){
					$stype = (int)(trim($_POST['semtype']));
					$quote = ($dur*12)/$stype;
				}else{
					$stype = 0;
					$quote = 1;
				}
				$data2['sem_type'] = (string)$stype;
			}else{
				$stype = 0;
				$quote = 1;
				$data2['sem_type'] = (string)$stype;
			}
			
			$data2['aca_year'] = trim($_POST['aca_year']);
			$data2['astart_date'] = date('Y-m-d',strtotime($_POST['adstart']));
			$data2['aend_date'] = date('Y-m-d',strtotime($_POST['adend']));
			if($data2['apply_type']=='1'){
				if(isset($_POST['screen_type'])){
					$st = $_POST['screen_type'];
					for($i=0; $i<count($st); $i++){
						$sc.=$st[$i].',';
					}
					$data2['screen_type'] = rtrim($sc, ',');
				}
			}else{
				$data2['screen_type'] = '';
			}
			$data2['apply_status'] = true;
			
			$this->db->trans_off();
			$this->db->trans_strict(false);
			
			$this->db->trans_start();
				$prog = $this->Member->insertProgram($data);
					
				$data1['program_id'] = $prog;
				$data1['user_id'] = $_SESSION['userData']['userId'];
				$data1['role'] = 'Teacher';
				$data1['status'] = 'accepted';
				$data1['add_date'] = date('Y-m-d H:i:s');
				$this->Member->insertUserRole($data1);
				
				$data2['prog_id'] = $prog;
				$this->Member->insertAdmission($data2);
				for($i=1; $i<=$quote; $i++){
					$data3[$i]['user_id'] = $_SESSION['userData']['userId'];
					$data3[$i]['title'] = 'Sem '.$i;
					$data3[$i]['duration'] = $stype.' months';
					$data3[$i]['add_date'] = date('Y-m-d H:i:s');
					$sem_id[$i] = $this->Exam_model->insertDataRetId('pro_semister', $data3[$i]);
					
					$data4[$i]['sem_id'] = $sem_id[$i];
					$data4[$i]['program_id'] = $prog;
					$data4[$i]['add_date'] = date('Y-m-d H:i:s');
					$this->Exam_model->insertData('pro_map_sem', $data4[$i]);
				}
			$this->db->trans_complete();
			$trans_status = $this->db->trans_status();
			if($trans_status == FALSE){
				$this->db->trans_rollback();
				$this->session->set_flashdata('error', ucfirst($progtype).' program insert error.');
			}else{
				$this->db->trans_commit();
				$this->session->set_flashdata('error', ucfirst($progtype).' program was added successfully.');
			}
		}
		redirect(base_url().'Teacher');
	}
	public function cuSCProgram()
	{
		$pcid = 0;
		$pid = trim($_POST['pid']);
		$progtype = trim($_POST['type']);
		$userid = $_SESSION['userData']['userId'];
		
		$status = trim($_POST['status']);
		$data['code'] = strtoupper(trim($_POST['pcode']));  //checked
		$data['title'] = trim($_POST['title']);  //checked
		$data['type'] = trim($_POST['type']);  //checked
		$data['category'] = trim($_POST['category']);  //checked
		$data2['prog_hrs'] = (trim($_POST['total_hrs'])!='')? trim($_POST['total_hrs']) : 0;  //checked
		$data2['total_seat'] = (trim($_POST['total_seat'])!='')? trim($_POST['total_seat']) : 0;  //checked
		$data2['ptype'] = trim($_POST['prog_type']);  //checked
		$data2['prog_level'] = trim($_POST['prog_level']);  //checked
		$data2['prog_subcategory'] = trim($_POST['prog_subcategory']);  //checked
		
		$ori = $_FILES['avatar']['name'];
		$tmp = $_FILES['avatar']['tmp_name'];
		$thmb = uniqid().$ori;

		$tmp1 = $_FILES['schedule']['tmp_name'];
		$src1 = $_FILES['schedule']['name'];
		$file1 = uniqid().$src1;
		
		$tmp2 = $_FILES['brochure']['tmp_name'];
		$src2 = $_FILES['brochure']['name'];
		$file2 = uniqid().$src2;
		if($ori!=""){
			$data['banner'] = $thmb;
			$this->resize_image($tmp, "./assets/img/banner/".$thmb, 800, 532, false);
		}  //checked
		if($src1!=''){
			$data['program_brochure'] = $file1;
			move_uploaded_file($tmp1, './uploads/programs/'.$file1);
		}  //checked
		if($src2!=''){
			$data['certificate_sample'] = $file2;
			move_uploaded_file($tmp2, './uploads/programs/'.$file2);
		}  //checked
		$data['intro_video_link'] = trim($_POST['intro']);  //checked
		$data['overview'] = trim($_POST['overview']);  //checked
		$data['feetype'] = trim($_POST['feetype']);  //checked
		if($data['feetype']=='Paid'){
			$data['total_fee'] = trim($_POST['fees']);
			$data2['discount'] = (trim($_POST['rebate'])!="")? trim($_POST['rebate']):0;
		}else{
			$data['total_fee'] = '';
			$data2['discount'] = 0;
		}  //checked
		$data['fee_details'] = trim($_POST['fdetails']);  //checked
		$data2['prog_skills'] = json_encode($_POST['prog_skills']);  //checked
		$data['requirements'] = trim($_POST['requirement']);  //checked
		$data['why_learn'] = trim($_POST['wlearn']);  //checked
		if($status=='approved'){
			$data['status'] = $status;
		}else if($status=='pending'){
			$data['status'] = $status;
		}else{
			$data['status'] = 'drafted';
		}
		/*certificate*/  //checked
		if($progtype==4 || $progtype==3){
			$chk_certify = $this->Member->checkExitsCertify($pid);
			$data['bolcertify'] = $_POST['bolcertify'];
			if($data['bolcertify'] == 'auto'){
				$tmp3 = $_FILES['bg_image']['tmp_name'];
				$src3 = $_FILES['bg_image']['name'];
				$file3 = uniqid().$src3;
				
				if($src3!=''){
					$data1['bg_image'] = $file3;
				}
				$data1['title'] = trim($_POST['ctitle']);
				$data1['text_before'] = trim($_POST['txt_before']);
				$data1['text_after'] = trim($_POST['txt_after']);
				$data1['student_name'] = isset($_POST['bolstud'])? (($_POST['bolstud']=='1')? true : false) : false;
				
				if(!empty($chk_certify)){
					$where2 = 'prog_id='.$pid;
					if($this->Exam_model->updateData('pro_certificate', $data1, $where2)){
						if($src3!=''){
							move_uploaded_file($tmp3, './uploads/certificates/'.$file3);
						}
					}
				}else{
					$data1['prog_id'] = $pid;
					$data1['user_id'] = $userid;
					$data1['issue_datetime'] = date('Y-m-d H:i:s');
					if($this->Exam_model->insertData('pro_certificate', $data1)){
						if($src3!=''){
							move_uploaded_file($tmp3, './uploads/certificates/'.$file3);
						}
					}
				}
			}
		}else{
			$data['bolcertify'] = 'manual';  //checked
		}
		$data2['view_mode'] = trim($_POST['view_mode']);  //checked
		$data2['vmpassword'] = trim($_POST['vmpassword']);  //checked
		$data2['congrats_msg'] = trim($_POST['congrats']);  //checked
		$data2['welcome_msg'] = trim($_POST['welcome']); //checked
		if($progtype!=3){
			$data2['apply_type'] = trim($_POST['apply_type']);  //checked
			$data2['criteria'] = trim($_POST['criteria']);  //checked
			$data2['astart_date'] = date('Y-m-d',strtotime($_POST['adstart']));  //checked
			$data2['aend_date'] = date('Y-m-d',strtotime($_POST['adend']));  //checked
			if($data2['apply_type']=='1'){
				if(isset($_POST['screen_type'])){
					$st = $_POST['screen_type'];
					for($i=0; $i<count($st); $i++){
						$sc.=$st[$i].',';
					}
					$data2['screen_type'] = rtrim($sc, ',');
				}
			}else{
				$data2['screen_type'] = '';
			}  //checked
			$data2['apply_status'] = true;  //checked
			$data2['placement'] = trim($_POST['placement']);  //checked
			$data2['aca_year'] = trim($_POST['aca_year']);  //checked
			$data['curriculam'] = "";  //checked
			$data['selection_procedure'] = "";  //checked
			$dur = (int)(trim($_POST['duration']));  //checked
			$data['duration'] = $dur;
			$data['dtype'] = trim($_POST['dtype']);  //checked
			if($dur!='year'){
				$stype = 0;
				$quote = 1;
				$data2['sem_type'] = (string)$stype;
				$chk_sem = $this->Member->checkAvailableSemester($pid);
				if($chk_sem==0){
					for($i=1; $i<=$quote; $i++){
						$data3[$i]['user_id'] = $_SESSION['userData']['userId'];
						$data3[$i]['title'] = 'Sem '.$i;
						$data3[$i]['duration'] = $stype.' months';
						$data3[$i]['add_date'] = date('Y-m-d H:i:s');
						$sem_id[$i] = $this->Exam_model->insertDataRetId('pro_semister', $data3[$i]);
						
						$data4[$i]['sem_id'] = $sem_id[$i];
						$data4[$i]['program_id'] = $pid;
						$data4[$i]['add_date'] = date('Y-m-d H:i:s');
						$this->Exam_model->insertData('pro_map_sem', $data4[$i]);
					}
				}
			}
			
			
			$data['start_date'] = date('Y-m-d H:i:s',strtotime($_POST['sdate']));  //checked
			$data['end_date'] = date('Y-m-d H:i:s',strtotime($_POST['edate']));  //checked
		}
		if($data2['prog_level']!="" && $data2['prog_subcategory']!=""){
			if($this->Member->updateProgram($data, $pid))
			{
				$this->Member->updateAdmission($data2, $pid);
				$this->session->set_flashdata('error', 'The program updation successfull.');
				
			}else{
				$this->session->set_flashdata('error', 'The program updation error.');
			}
		}else{
			$this->session->set_flashdata('error', 'Update Error: Must select Sub-Category and Level in Basic Information.');
		}
		redirect(base_url('Teacher'));
	}
	
	public function viewProgram()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Programs Details | Magnox Learning+ - Teacher';
			$userId = $_SESSION['userData']['userId'];
			$data['prog_id'] = base64_decode($_GET['id']);
			$data['prog'] = $this->Member->getProgramById($data['prog_id']);
			$data['pcounters'] = $this->Member->getProgDetCounters($data['prog_id']);
			
			$this->loadTeacherViews("view-program", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	
	public function getProgMenuContent()
	{
		$page = $_GET['page'];
		$data['prog_id'] = $_GET['prog_id'];
		$data['prog'] = $this->Member->getProgramById($data['prog_id']);
		$data['progtype'] = 'certificate';
		$this->load->view('certify_prof_courses/'.$page, $data);
	}
	public function getAssigmentContents()
	{
		$data['lra_id'] = $_GET['lra_id'];
		$data['prog_id'] = $_GET['prog_id'];
		$data['prog'] = $this->Member->getProgramById($data['prog_id']);
		$data['progtype'] = 'certificate';
		$this->load->view('certify_prof_courses/cassnsubmission', $data);
	}
	/*******************************COURSES********************************/
	public function getCoursebyProg()
	{
		$sl = trim($_GET['sl']);
		$procourse = $this->Course_model->getProgramCourses($sl);
		echo json_encode($procourse);
	}
	public function progCourses()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Programs Courses | Magnox Learning+ - Teacher';
			$data['prog_id'] = base64_decode($_GET['id']);
			$data['prog'] = $this->Member->getProgramById($data['prog_id']);
			$data['procourse'] = $this->Course_model->getProgramCourses($data['prog_id']);
			
			$this->loadTeacherViews("prog-courses", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function addCourse()
	{
		$prog_id = base64_decode($_GET['prog']);
		$obj = array('id'=>0, 'prog_id'=>$prog_id, 'title'=>"", 'start_date'=>date('Y-m-d H:i:s'), 'end_date'=>date('Y-m-d H:i:s'), 'total_credit'=>"", 'syllabus'=>"", 'overview'=>"", 'importance'=>"", 'lec'=>"", 'tut'=>"", 'prac'=>"", 'type'=>"", 'c_code'=>"");
		$data['sems'] = $this->Course_model->getAllSemestersByProgId($prog_id);
		if(isset($_GET['cid'])){
			$this->global['pageTitle'] = 'Update Course | Programs';
			$cid = base64_decode($_GET['cid']);
			$data['cd'] = $this->Course_model->getCourseDetailsById($cid);
		}else{
			$this->global['pageTitle'] = 'Add Course | Programs';
			$data['cd'][0] = (object)$obj;
		}
        
        $this->loadTeacherViews("add-course", $this->global, $data , NULL);
	}
	public function cuProCourse()
	{
		$userid = $_SESSION['userData']['userId'];
		$resp = array('status'=>'danger', 'msg'=>'Something went wrong.');
		$id = trim($_POST['cid']);
		$pid = trim($_POST['prog_id']);
		
		$tmp = $_FILES['syllabus']['tmp_name'];
		$ori = $_FILES['syllabus']['name'];
		$files = uniqid().$ori;
		
		$data['c_code'] = strtoupper(trim($_POST['ccode']));
		$data['title'] = trim($_POST['title']);
		$data['start_date'] = date('Y-m-d H:i:s', strtotime($_POST['sdate']));
		$data['end_date'] = date('Y-m-d H:i:s', strtotime($_POST['edate']));
		$data['total_credit'] = trim($_POST['ccredit']);
		$data['overview'] = trim($_POST['overview']);
		$data['importance'] = trim($_POST['importance']);
		if($ori!=''){
			$data['syllabus'] = $files;
		}
		$data['lec'] = trim($_POST['lecture']);
		$data['tut'] = trim($_POST['tutorial']);
		$data['prac'] = trim($_POST['practical']);
		$data['type'] = trim($_POST['crtype']);
		$data['prog_id'] = $pid;
		
		if($id==0){
			$data['post_date'] = date('Y-m-d H:i:s');
			$cid = $this->Course_model->insertProCourse($data);
			if($cid){
				if($ori!=''){
					move_uploaded_file($tmp,'./uploads/courses/'.$files);
				}
				$data1['course_id'] = $cid;
				$data1['program_id'] = $pid;
				$data1['user_id'] = $userid;
				$data1['sem_id'] = $_POST['semid'];
				$data1['add_date'] = date('Y-m-d H:i:s');
				$this->Course_model->insertProMapCource($data1);
				$resp = array('status'=>'success', 'msg'=>'Course added successfully.');
			}else{
				$resp = array('status'=>'danger', 'msg'=>'Course added error. Something went wrong.');
			}
		}else{
			if($this->Course_model->updateProCourse($data, $id)){
				if($ori!=''){
					move_uploaded_file($tmp,'./uploads/courses/'.$files);
				}
				$chkpmc = $this->Course_model->checkAvailPMC($pid, $id);
				$data1['course_id'] = $id;
				$data1['program_id'] = $pid;
				$data1['user_id'] = $userid;
				$data1['sem_id'] = $_POST['semid'];
				if($chkpmc==0){
					$data1['add_date'] = date('Y-m-d H:i:s');
					$this->Course_model->insertProMapCource($data1);
				}else{
					$this->Course_model->updateProMapCource($data1, $id, $pid);
				}
				
				$resp = array('status'=>'success', 'msg'=>'Course updated successfully.');
			}else{
				$resp = array('status'=>'danger', 'msg'=>'Course update error. Something went wrong.');
			}
		}
		echo json_encode($resp);
	}
	public function courseDetails()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Course Details | Magnox Learning+ - Teacher';
			$data['prog_id'] = base64_decode($_GET['prog']);
			$data['cid'] = base64_decode($_GET['cid']);
			$data['prog'] = $this->Member->getProgramById($data['prog_id']);
			$data['cdetails'] = $this->Course_model->getCourseDetailsById($data['cid']);
			
			$this->loadTeacherViews("course-details", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function deleteProgCourse()
	{
		$id = $_GET['id'];
		$where = 'id='.$id;
		echo $this->Exam_model->deleteData('pro_course', $where);
	}
	/*----------------------------------------------------------------------------------*/
	public function getLecture()
	{
		$clid = trim($_GET['clid']);
		echo json_encode($this->Course_model->getCLectureByID($clid));
	}
	public function getLectureFile()
	{
		$clid = trim($_GET['clid']);
		$lfile = $this->Course_model->getLectureFile($clid);
		$data['user_sl'] = $_SESSION['userData']['userId'];
		$data['lec_sl'] = $clid;
		$data['add_date'] = date('Y-m-d H:i:s');
		if($this->Course_model->insertLecDLrecord($data)){
			echo (file_exists('./uploads/courselra/'.$lfile[0]->file_name))? $lfile[0]->file_name : 0;
		}else{
			echo 0;
		}
	}
	public function cuCLecture()
	{
		$respond = array();
		
		$tmp = $_FILES['lfiles']['tmp_name'];
		$ori = $_FILES['lfiles']['name'];
		$lfile = uniqid().$ori;
		
		$data['course_sl'] = trim($_POST['cid']);
		$prog = trim($_POST['prog']);
		$lecid = trim($_POST['lec_id']);
		$data['user_id'] = $_SESSION['userData']['userId'];
		$data['title'] = trim($_POST['ltitle']);
		$data['file_type'] = trim($_POST['ltype']);
		if($data['file_type']=='fl'){
			if($ori!=''){
				$data['file_name'] = $lfile;
			}
		}else{
			$data['link'] = trim($_POST['llink']);
		}
		$data['notify'] = (isset($_POST['lnotify']))? true:false;
		$data['archive'] = false;
		$data['lec_date'] = ($_POST['ldate']!='')? date('Y-m-d H:i:s',strtotime($_POST['ldate'])) : date('Y-m-d H:i:s');
		if($data['notify']){
			$users = $this->Member->getStudentNameEmailByProgId($prog);
			$pc = $this->Member->getProgramCourseNameByProgId($prog, $data['course_sl']);
			$subject = 'New Lecture added';
			$message = 'New Lecture has been add to your course: "'.$pc[0]->course_title.'" of program: "'.$pc[0]->program_title.'", by '.$_SESSION['userData']['name'].'.';
			
			$this->MultipleMailSystem_Two($users, $subject, $message);
		}
		if($lecid==0){
			$data['add_date'] = date('Y-m-d H:i:s');
			if($this->Course_model->insertClecture($data)){
				if($ori!='' && $data['file_type']=='fl'){
					move_uploaded_file($tmp,'./uploads/courselra/'.$lfile);
				}
				$respond = array('status'=>'success', 'msg'=>'added successfully');
			}else{
				$respond = array('status'=>'error', 'msg'=>'addition error');
			}
		}else{
			$data['modified_date'] = date('Y-m-d H:i:s');
			if($this->Course_model->updateClecture($data, $lecid)){
				if($ori!='' && $data['file_type']=='fl'){
					move_uploaded_file($tmp,'./uploads/courselra/'.$lfile);
				}
				$respond = array('status'=>'success', 'msg'=>'updated successfully');
			}else{
				$respond = array('status'=>'error', 'msg'=>'updation error');
			}
		}
		echo json_encode($respond);
	}
	public function deleteLectures()
	{
		$clid = trim($_GET['clid']);
		$data['archive'] = true;
		/*$lfile = $this->Course_model->getLectureFile($clid);
		if(file_exists(base_url().'uploads/courselra/'.$lfile[0]->file_name)){
			unlink(base_url().'uploads/courselra/'.$lfile[0]->file_name);
		}*/
		//echo ($this->Course_model->delLectureById($clid))? true : false;
		echo ($this->Course_model->updateClecture($data, $clid))? true : false;
	}
	/*----------------------------------------------------------------------------------*/
	public function getResource()
	{
		$crid = trim($_GET['crid']);
		echo json_encode($this->Course_model->getCResourceByID($crid));
	}
	public function getResourceYTLink()
	{
		$crid = trim($_GET['crid']);
		echo json_encode($this->Course_model->getCResourceFileByID($crid));
	}
	public function getResourceFiles()
	{
		$crid = trim($_GET['crid']);
		$rfile = $this->Course_model->getCResourceFileByID($crid);
		$data['user_id'] = $_SESSION['userData']['userId'];
		$data['res_sl'] = $crid;
		$data['add_date'] = date('Y-m-d H:i:s');
		if($this->Course_model->insertResDLrecord($data)){
			echo json_encode($rfile);
		}else{
			echo 0;
		}
	}
	public function cuCResource()
	{
		$respond = array();
		
		$data['course_sl'] = trim($_POST['cid']);
		$prog = trim($_POST['prog']);
		$resid = trim($_POST['res_id']);
		$data['user_id'] = $_SESSION['userData']['userId'];
		$data['title'] = trim($_POST['rtitle']);
		$data['desc'] = trim($_POST['rdetails']);
		$data['notify'] = (isset($_POST['rnotify']))? true : false;
		$data['archive'] = false;
		if($data['notify']){
			$users = $this->Member->getStudentNameEmailByProgId($prog);
			$pc = $this->Member->getProgramCourseNameByProgId($prog, $data['course_sl']);
			$subject = 'New Resource added';
			$message = 'New Resource has been add to your course: "'.$pc[0]->course_title.'" of program: "'.$pc[0]->program_title.'", by '.$_SESSION['userData']['name'].'.';
			
			$this->MultipleMailSystem_Two($users, $subject, $message);
		}
		if($resid==0){
			$data['add_date'] = date('Y-m-d H:i:s');
			if($this->Course_model->insertCresource($data)){
				$respond = array('status'=>'success', 'msg'=>'added successfully');
			}else{
				$respond = array('status'=>'error', 'msg'=>'addition error');
			}
		}else{
			$data['modified_date'] = date('Y-m-d H:i:s');
			if($this->Course_model->updateCresource($data, $resid)){
				$respond = array('status'=>'success', 'msg'=>'updated successfully');
			}else{
				$respond = array('status'=>'error', 'msg'=>'updation error');
			}
		}
		echo json_encode($respond);
	}
	public function cuCResourceFiles()
	{
		$cid = trim($_POST['fcid']);
		$prog = trim($_POST['fprog']);
		$resid = trim($_POST['fres_id']);
		$counter = trim($_POST['rfcount']);
		
		for($i=0; $i<=$counter; $i++)
		{
			if(isset($_POST['yt_link'.$i]))
			{
				$data[$i]['res_sl'] = $resid;
				$data[$i]['type'] = trim($_POST['youtube'.$i]);
				$data[$i]['linkfile'] = trim($_POST['yt_link'.$i]);
				$data[$i]['add_date'] = date('Y-m-d H:i:s');
				
				$this->Course_model->insertCRFiles('pro_res_fileslinks', $data[$i]);
			}
			if(isset($_POST['lk_link'.$i]))
			{
				$data[$i]['res_sl'] = $resid;
				$data[$i]['type'] = trim($_POST['link'.$i]);
				$data[$i]['linkfile'] = trim($_POST['lk_link'.$i]);
				$data[$i]['add_date'] = date('Y-m-d H:i:s');
				
				$this->Course_model->insertCRFiles('pro_res_fileslinks', $data[$i]);
			}
			if(isset($_FILES['fl_link'.$i]['name']))
			{
				$tmp = $_FILES['fl_link'.$i]['tmp_name'];
				$ori = $_FILES['fl_link'.$i]['name'];
				$rfile = uniqid().$ori;
				
				$data[$i]['res_sl'] = $resid;
				$data[$i]['type'] = trim($_POST['files'.$i]);
				$data[$i]['file_type'] = pathinfo($ori, PATHINFO_EXTENSION);
				$data[$i]['linkfile'] = $rfile;
				$data[$i]['add_date'] = date('Y-m-d H:i:s');
				
				if($this->Course_model->insertCRFiles('pro_res_fileslinks', $data[$i]))
				{
					move_uploaded_file($tmp,'./uploads/cresources/'.$rfile);
				}
			}
		}
		echo 1;
	}
	public function deleteResources()
	{
		$crid = trim($_GET['crid']);
		$data['archive'] = true;
		/*$rfile = $this->Course_model->getCResourceFileByRID($crid);
		foreach($rfile as $rrow)
		{
			if(file_exists('./uploads/cresources/'.$rrow->linkfile)){
				unlink('./uploads/cresources/'.$rrow->linkfile);
			}
		}
		$this->Course_model->delResourceFilesByRID($crid);*/
		echo ($this->Course_model->updateCresource($data, $crid))? true : false;
	}
	public function deleteResourceFiles()
	{
		$crid = trim($_GET['crid']);
		//$data['archive'] = true;
		$rfile = $this->Course_model->getCResourceFileByRID($crid);
		foreach($rfile as $rrow)
		{
			if(file_exists('./uploads/cresources/'.$rrow->linkfile)){
				unlink('./uploads/cresources/'.$rrow->linkfile);
			}
		}
		echo ($this->Course_model->delResourceFilesByID($crid))? true : false;
	}
	/*----------------------------------------------------------------------------------*/
	public function getAssignment()
	{
		$caid = trim($_GET['caid']);
		echo json_encode($this->Course_model->getCAssignmentByID($caid));
	}
	public function cuCAssignment()
	{
		$respond = array();
		
		$data['course_sl'] = trim($_POST['cid']);
		$prog = trim($_POST['prog']);
		$assgnid = trim($_POST['assgn_id']);
		$data['user_id'] = $_SESSION['userData']['userId'];
		$data['title'] = trim($_POST['atitle']);
		$data['details'] = trim($_POST['adetails']);
		//$data['marks'] = trim($_POST['amarks']);
		$data['tdate'] = date('Y-m-d H:i:s',strtotime($_POST['sdate']));
		$data['deadline'] = date('Y-m-d H:i:s',strtotime($_POST['ddate']));
		$data['publish'] = (isset($_POST['aPublish']))? true:false;
		$data['notify'] = (isset($_POST['anotify']))? true:false;
		$data['archive'] = false;
		
		$data1['course_sl'] = trim($_POST['cid']);
		$data1['type'] = 'Assignment';
		$data1['subject'] = trim($_POST['atitle']);
		$data1['user_id'] = $_SESSION['userData']['userId'];
		$data1['weightage'] = '40';
		$data1['full_marks'] = trim($_POST['amarks']);
		
		if($data['notify']){
			$users = $this->Member->getStudentNameEmailByProgId($prog);
			$pc = $this->Member->getProgramCourseNameByProgId($prog, $data['course_sl']);
			$subject = 'New Assignment added';
			$message = 'New Assignment has been add to your course: "'.$pc[0]->course_title.'" of program: "'.$pc[0]->program_title.'", by '.$_SESSION['userData']['name'].'.';
			
			$this->MultipleMailSystem_Two($users, $subject, $message);
		}
		if($assgnid==0){
			$data['add_date'] = date('Y-m-d H:i:s');
			$data1['tdatetime'] = date('Y-m-d H:i:s');
			$asid = $this->Course_model->insertCAssignment($data);
			if($asid!=NULL){
				$data1['serial'] = $asid;
				$this->Course_model->insertCWeightage($data1);
				$respond = array('status'=>'success', 'msg'=>'added successfully');
			}else{
				$respond = array('status'=>'error', 'msg'=>'addition error');
			}
		}else{
			$data['modified_date'] = date('Y-m-d H:i:s');
			$data1['modified_datetime'] = date('Y-m-d H:i:s');
			if($this->Course_model->updateCAssignment($data, $assgnid)){
				$this->Course_model->updateCWeightage($data1, $assgnid);
				$respond = array('status'=>'success', 'msg'=>'updated successfully');
			}else{
				$respond = array('status'=>'error', 'msg'=>'updation error');
			}
		}
		echo json_encode($respond);
	}
	public function cuCAssignmentFiles()
	{
		$cid = trim($_POST['fcid']);
		$prog = trim($_POST['fprog']);
		$assgn_id = trim($_POST['fassgn_id']);
		$counter = trim($_POST['afcount']);
		
		for($i=0; $i<=$counter; $i++)
		{
			if(isset($_POST['lk_link'.$i]))
			{
				$data[$i]['assgn_sl'] = $assgn_id;
				$data[$i]['file_type'] = trim($_POST['link'.$i]);
				$data[$i]['file_name'] = trim($_POST['lk_link'.$i]);
				$data[$i]['add_date'] = date('Y-m-d H:i:s');
				
				$this->Course_model->insertCRFiles('pro_assgn_files', $data[$i]);
			}
			if(isset($_FILES['fl_link'.$i]['name']))
			{
				$tmp = $_FILES['fl_link'.$i]['tmp_name'];
				$ori = $_FILES['fl_link'.$i]['name'];
				$rfile = uniqid().$ori;
				
				$data[$i]['assgn_sl'] = $assgn_id;
				$data[$i]['file_type'] = trim($_POST['files'.$i]);
				$data[$i]['file_ext'] = pathinfo($ori, PATHINFO_EXTENSION);;
				$data[$i]['file_name'] = $rfile;
				$data[$i]['add_date'] = date('Y-m-d H:i:s');
				
				if($this->Course_model->insertCRFiles('pro_assgn_files', $data[$i]))
				{
					move_uploaded_file($tmp,'./uploads/cassignments/'.$rfile);
				}
			}
		}
		
		echo 1;
	}
	public function deleteAssignments()
	{
		$caid = trim($_GET['caid']);
		$data['archive'] = true;
		/*$afile = $this->Course_model->getAssignmentFilesById($caid);
		foreach($afile as $arow)
		{
			if(file_exists('./uploads/cassignments/'.$arow->file_name)){
				unlink('./uploads/cassignments/'.$arow->file_name);
			}
		}
		$this->Course_model->delAssignmentFilesByAID($caid);*/
		//$this->Course_model->delAssignmentById($caid)
		echo ($this->Course_model->updateCAssignment($data, $caid))? true : false;
	}
	public function deleteAssignmentFiles()
	{
		$caid = trim($_GET['caid']);
		$afile = $this->Course_model->getAssignmentFilesById($caid);
		foreach($afile as $arow)
		{
			if(file_exists('./uploads/cassignments/'.$arow->file_name)){
				unlink('./uploads/cassignments/'.$arow->file_name);
			}
		}
		echo ($this->Course_model->delAssignmentFilesByID($caid))? true : false;
	}
	
	public function viewAssignmentDetails()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Course Assignments | Magnox Learning+ - Teacher';
			$aid = base64_decode($_GET['id']);
			$data['prog_id'] = base64_decode($_GET['prog']);
			$data['cid'] = base64_decode($_GET['cid']);
			//$data['cdetails'] = $this->Course_model->getCourseDetailsById($data['cid']);
			$data['cadetails'] = $this->Course_model->getCAssignmentByID($aid);
			foreach($data['cadetails'] as $row){
				$id = $row->sl;
				$data['cafiles'] = $this->Course_model->getAssignmentFilesById($id);
			}
			
			$this->loadTeacherViews("cassignment-details", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function getAssignmentSolutions()
	{
		$id = trim($_GET['id']);
		//$userid = $_SESSION['userData']['userId'];
		$data['assgn_sub'] = $this->Course_model->getStudentAssignmentSubmission($id);
		$i=1;
		if(!empty($data['assgn_sub'])){
			$data['asubfiles_'.$i] = $this->Course_model->getAssignSubFiles($data['assgn_sub'][0]->sl);
			$i++;
		}
		return $this->load->view("teachers/assignment-solution", $data);
	}
	public function updateStudentMarks()
	{
		$data['stud_sl'] = trim($_POST['stu_sl']);
		$data['pro_course_wt_sl'] = trim($_POST['pc_wt']);
		$data['marks'] = trim($_POST['assgn_marks']);
		$data['tdatetime'] = date('Y-m-d H:i:s');
		$data['user_id'] = $_SESSION['userData']['userId'];
		if($this->Course_model->insertStudentAssgnMark($data)){
			$res = array('status'=>'success', 'msg'=>'has been added.');
		}else{
			$res = array('status'=>'error', 'msg'=>'addtion has failed.');
		}
		echo json_encode($res);
	}
	public function updateStudMarks()
	{
		$data['marks'] = $_POST['marks'];
		$data['tdatetime'] = date('Y-m-d H:i:s');
		$data['user_id'] = $_SESSION['userData']['userId'];
		$where = 'sl='.$_POST['psm_sl'];
		echo $this->Exam_model->updateData('pro_stud_marks', $data, $where);
	}
	
	/*****************************INSTITUTE/STREAM**********************************/
	public function progOrgStrm()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Institute/Stream | Magnox Learning+ - Teacher';
			$data['prog_id'] = base64_decode($_GET['id']);
			$data['prog'] = $this->Member->getProgramById($data['prog_id']);
			$data['institute'] = $this->Member->getProgramOrg($data['prog_id']);
			$data['stream'] = $this->Member->getProgramStreams($data['prog_id']);
			
			$this->loadTeacherViews("prog-orgstream", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function cuORG()
	{
		$ori = $_FILES['avatar']['name'];
		$tmp = $_FILES['avatar']['tmp_name'];
		$thmb = uniqid().$ori;

		$data_img = $_POST['crop_img']; 
		if($data_img!=""){
			list($type, $data_img) = explode(';', $data_img);
			list(, $data_img)      = explode(',', $data_img);	
		}
		$userId = $_SESSION['userData']['userId'];
		$data['title'] = trim($_POST['org_title']);
		$data['website'] = trim($_POST['org_web']);
		$data['contact_info'] = trim($_POST['org_contact']);
		if($ori!=""){
			$data['logo'] = $thmb;
		}
		$data['user_id'] = $userId;
		if($_POST['org_id']==""){
			$data['add_date'] = date('Y-m-d H:i:s');
			
			$orgid = $this->Member->insertProOrg($data);
			if($orgid){
				$data1['org_id'] = $orgid;
				$data1['program_id'] = $_POST['iprog_id'];
				$data1['add_date'] = date('Y-m-d H:i:s');
				$flag = $this->Member->insertProMapOrg($data1);
				if($ori!=""){
					if($data_img!=''){
						file_put_contents('./assets/img/institute/'.$thmb, base64_decode($data_img));
						//file_put_contents('./public/image/profile_pic/lg/'.$thmb, base64_decode($data_img));
					}
				}
			}else{
				$flag = 0;
			}
		}else{
			$flag = $this->Member->updateProOrg($data, $_POST['org_id']);
			if($ori!=""){
				if($data_img!=''){
					file_put_contents('./assets/img/institute/'.$thmb, base64_decode($data_img));
					//file_put_contents('./public/image/profile_pic/lg/'.$thmb, base64_decode($data_img));
				}
			}
		}
		echo $flag;
	}
	public function getOrgById()
	{
		$org_id = $_GET['org'];
		$org = $this->Member->getOrgByid($org_id);
		echo json_encode($org);
	}
	public function delOrg()
	{
		$org_id = $_GET['org'];
		$this->Member->deleteProMapOrg($org_id);
		echo $this->Member->deleteProOrg($org_id);
	}
	
	public function cuStream()
	{
		if($_POST['strm_id']==NULL){
			$data['title'] = ucwords(strtolower(trim($_POST['title'])));
			$data['website'] = trim($_POST['web']);
			$data['contact_info'] = trim($_POST['contact']);
			$data['org_id'] = $_POST['org_id'];
			$data['user_id'] = $_SESSION['userData']['userId'];
			$data['add_date'] = date('Y-m-d H:i:s');
			
			$strmid = $this->Member->insertProStream($data);
			if($strmid){
				$data1['stream_id'] = $strmid;
				$data1['program_id'] = $_POST['prog_id'];
				$data1['add_date'] = date('Y-m-d H:i:s');
				$flag = $this->Member->insertProMapStream($data1);
			}else{
				$flag = 0;
			}
		}else{
			$data['title'] = ucwords(strtolower(trim($_POST['title'])));
			$data['website'] = trim($_POST['web']);
			$data['contact_info'] = trim($_POST['contact']);
			$data['org_id'] = $_POST['org_id'];
			$data['user_id'] = $_SESSION['userData']['userId'];
			$flag = $this->Member->updateProStream($data, $_POST['strm_id']);
		}
		echo $flag;
	}
	public function getStreamById()
	{
		$strm_id = $_GET['strm'];
		$strm = $this->Member->getStreamByid($strm_id);
		echo json_encode($strm);
	}
	public function delStream()
	{
		$strm_id = $_GET['strm'];
		$this->Member->deleteProMapStrm($strm_id);
		echo $this->Member->deleteProStrm($strm_id);
	}
	/**********************************************************************/
	/*****************************TEACHERS*********************************/
	public function progTeachers()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Teacher | Magnox Learning+ - Teacher';
			$data['prog_id'] = base64_decode($_GET['id']);
			$data['prog'] = $this->Member->getProgramById($data['prog_id']);
			$data['pprof'] = $this->Member->getAllRequestByIdRole($data['prog_id'], 'Teacher', NULL, 'accepted');

			$this->loadTeacherViews("prog-teachers", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	/**********************************************************************/
	/*****************************STUDENTS*********************************/
	public function progStudents()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Students | Magnox Learning+ - Teacher';
			$data['prog_id'] = base64_decode($_GET['id']);
			$data['prog'] = $this->Member->getProgramById($data['prog_id']);
			//$data['pstud'] = $this->Member->getAllRequestByIdRole($data['prog_id'], 'Student', NULL, 'accepted');
			$data['pstud'] = $this->Member->getProgramStudAccepted($data['prog_id']);
			$data['sems'] = $this->Course_model->getAllSemestersByProgId($data['prog_id']);
			$data['ayr'] = $this->Member->getAllActiveAcaYear();
			
			$this->loadTeacherViews("prog-students", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	/**********************************************************************/

	public function community()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Community | Magnox Learning+ - Teacher';
			$data['community'] = $this->Member->get_all_communities();
			
			$this->loadTeacherViews("community-list", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}

	public function addCommunity()
	{
		if($this->isLoggedIn()){
			if(isset($_GET['id'])){
				$this->global['pageTitle'] = 'Update Community | Magnox Learning+ - Teacher';
				$data['comm'] = $this->Member->get_community_by_id($_GET['id']);
			}else{
				$this->global['pageTitle'] = 'Add Community | Magnox Learning+ - Teacher';
				$data['comm'][0] = (object)array('id'=>0, 'title'=>"", 'desc'=>"", 'skills'=>json_encode([]), 'photo'=>"", 'price'=>"", 'type'=>"", 'category_id'=>0);
			}
			$data['skills'] = $this->Member->get_all_skills();
			$data['category'] = $this->Member->get_all_category();
			
			$this->loadTeacherViews("add-community", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}

	public function cuCommunity()
	{
		$cid = $_POST['cid'];
		$tmp = $_FILES['avatar']['tmp_name'];
		$ori = $_FILES['avatar']['name'];
		$photo = time().$ori;

		$data['title'] = trim($_POST['title']);
		$data['user_id'] = $_SESSION['userData']['userId'];
		$data['desc'] = trim($_POST['desc']);
		$data['skills'] = (isset($_POST['skills']))? json_encode($_POST['skills']) : json_encode([]);
		if($ori != ""){
			$data['photo'] = $photo;
			move_uploaded_file($tmp, './assets/img/community/'.$photo);
		}
		$data['price'] = $_POST['price'];
		$data['type'] = trim($_POST['type']);
		$data['category_id'] = trim($_POST['category_id']);

		if($cid == 0){
			$data['add_datetime'] = date('Y-m-d H:i:s');
			if($this->Exam_model->insertData('community', $data)){
				$this->session->set_flashdata('success', 'Community has been added');
			}else{
				$this->session->set_flashdata('error', 'Community failed to add');
			}
			
		}else{
			$data['update_datetime'] = date('Y-m-d H:i:s');
			$where = 'id='.$cid;
			if($this->Exam_model->updateData('community', $data, $where)){
				$this->session->set_flashdata('success', 'Community has been updated');
			}else{
				$this->session->set_flashdata('error', 'Community failed to update');
			}
			
		}
		redirect(base_url('Teacher/community'));
	}

	public function deleteCommunity()
	{
		if(isset($_GET['clid'])){
			$cid = $_GET['clid'];
			$where = 'id='.$cid;
			echo $this->Exam_model->deleteData('community', $where);
		}else{
			echo 0;
		}
	}

	/**********************************************************************/
	public function getMenuContent()
	{
		$userid = $_SESSION['userData']['userId'];
		$page = trim($_GET['page']);
		$cid = trim($_GET['cid']);
		$data['cd'] = $this->Course_model->getCourseDetailsById($cid);
		$data['prog_id'] = trim($_GET['prog']);
		
		if($page=='lectures'){
			$data['clectures'] = $this->Course_model->getLecturessByCid($cid);
		}else if($page=='resources'){
			$data['cresource'] = $this->Course_model->getResourcesByCid($cid);
			$i=1;
			foreach($data['cresource'] as $row){
				$id = $row->sl;
				$data['crfiles'.$i] = $this->Course_model->getResourceFilesById($id);
				$i++;
			}
		}else if($page=='assignments'){
			$data['cassignment'] = $this->Course_model->getAssignmentByCid($cid);
			$i=1;
			foreach($data['cassignment'] as $row){
				$id = $row->sl;
				$data['cafiles'.$i] = $this->Course_model->getAssignmentFilesById($id);
				$i++;
			}
		}else if($page=='grade'){
			$j=1;
			$data['title'] = 'Grades';
			$data['pstud'] = $this->Member->getAllRequestByIdRole($data['prog_id'], 'Student', NULL, 'accepted');
			$data['pcwt'] = $this->Course_model->getOnlySubjectNameByCid($cid);
			foreach($data['pstud'] as $stow){
				$stud_id = $stow->id;
				$data['setGrade_'.$j] = $this->Course_model->getAllAssignmentMarksByCid($cid, $stud_id);
				$j++;
			}
			//
		}else if($page=='quiz'){
			$data['title'] = 'Quiz';
		}else if($page=='schedule'){
			$data['title'] = 'Schedule Classes';
			$data['schClass'] = $this->Course_model->getScheduleClassByCid($cid, $userid);
		}else if($page=='doubts'){
			$data['title'] = 'Doubts';
			$data['stud_dbs'] = $this->Course_model->getStudentDoubtsByCid($cid, $userid);
		}else if($page=='students'){
			$data['title'] = 'Student List';
			$data['pstud'] = $this->Member->getProgramStudAccepted($data['prog_id']);
			//getAllRequestByIdRole($data['prog_id'], 'Student', NULL, 'accepted');
		}else if($page=='teachers'){
			$data['title'] = 'Teacher List';
			$data['pprof'] = $this->Member->getAllRequestByIdRole($data['prog_id'], 'Teacher', NULL, 'accepted');
		}else if($page=='attendance'){
			$data['title'] = 'Attendance';
			$userid = $_SESSION['userData']['userId'];
			$f = new DateTime('first day of this month');
			$t = new DateTime();
			$from = $f->format('Y-m-d');
			$to = $t->format('Y-m-d');
			if(isset($_GET['from'])){
				$form = $_GET['from'];
			}
			if(isset($_GET['to'])){
				$to = $_GET['to'];
			}
			$live_classes = $this->Member->getLiveClasses($userid,$from,$to);
			$data['userid'] = $userid;
			$data['live_classes'] = $live_classes;			
			$data['from'] = $from;
			$data['to'] = $to;
		}

		return $this->loadTeacherCourse('course-'.$page, $data);
	}
	
	public function liveClass()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Live Class | Magnox Learning+ - Teacher';
			$userid = $_SESSION['userData']['userId'];
			//$data['programs'] = $this->Member->getAllMyPrograms('Teacher', $userid);
			$cid = base64_decode(trim($_GET['id']));
			$data['progcourse'] = $this->Member->getProgramCourse($cid);
			$data['sch_class'] = $this->Course_model->getScheduleClassByCid($cid, $userid);
			$data['pstud'] = $this->Member->getProgramStudAccepted($data['progcourse'][0]->pid);
			//getAllRequestByIdRole($data['progcourse'][0]->pid, 'Student', NULL, 'accepted');
			$data['cid'] = $cid;
			$data['userid'] = $userid;
			$this->loadTeacherViews("live-class", $this->global, $data, NULL);
		}else{
			redirect(base_url().'login');
		}
	}
	/*****************************PROFILE**********************************/
	public function userProfile()
	{
		if($this->isLoggedIn()){
			$userid = $_SESSION['userData']['userId'];
			$this->global['pageTitle'] = 'Profile | Magnox Learning+ - Teacher';
			$data['udetails'] = $this->Member->getUserDetailsById($userid);
			
			$this->loadTeacherViews("profile", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function updateProfile()
	{
		$id = $_SESSION['userData']['userId'];
		$ori = $_FILES['avatar']['name'];
		$tmp = $_FILES['avatar']['tmp_name'];
		$thmb = uniqid().$ori;
		
		$data_img = $_POST['crop_img']; 
		if($data_img!=""){
			list($type, $data_img) = explode(';', $data_img);
			list(, $data_img)      = explode(',', $data_img);	
		}
		
		$data['first_name'] = ucfirst(strtolower(trim($_POST['firstname'])));
		$data['last_name'] = ucfirst(strtolower(trim($_POST['lastname'])));
		if($ori!=''){
			$data['photo_sm'] = 'assets/img/users/'.$thmb;
			//$data['photo_lg'] = 'public/image/profile_pic/lg/'.$thmb;
		}
		$data['email'] = strtolower(trim($_POST['email']));
		$data['phone'] = trim($_POST['phone']);
		
		$data['modified_date_time'] = date('Y-m-d H:i:s');
		
		if($this->Member->updateUserAuth($data, $id)){
			if($ori!=""){
				if($data_img!=''){
					file_put_contents('./assets/img/users/'.$thmb, base64_decode($data_img));
					//file_put_contents('./public/image/profile_pic/lg/'.$thmb, base64_decode($data_img));
				}
			}
			$sessionArray = $this->session->userdata('userData');
			$this->session->unset_userdata('userData');
			$sessionArray['userId'] = $id;
			$sessionArray['email'] = trim($data['email']);
			$sessionArray['name'] = trim($data['first_name'].' '.$data['last_name']);
			if($ori!=""){
				$sessionArray['photo'] = 'assets/img/users/'.$thmb;
			}
			$sessionArray['isLoggedIn'] = true;
			$this->session->set_userdata('userData',$sessionArray);
			
			$data['salutation'] = trim($_POST['salutation']);
			$data['organization'] = trim($_POST['org']);
			$data['designation'] = ucwords(strtolower(trim($_POST['designation'])));
			$data['website'] = strtolower(trim($_POST['weblink']));
			$data['dateofbirth'] = date('Y-m-d',strtotime($_POST['dob']));
			$data['gender'] = $_POST['gender'];
			$data['alt_email'] = trim($_POST['alt_email']);
			$data['alt_phone'] = trim($_POST['alt_phone']);
			$data['website'] = trim($_POST['website']);
			$data['about_me'] = trim($_POST['about']);
			$data['address'] = trim($_POST['address']);
			$data['facebook_link'] = trim($_POST['facebook']);
			$data['linkedin_link'] = trim($_POST['linkedin']);
			$data['google_link'] = trim($_POST['google']);
			$this->Member->updateUserDetails($data, $id);
			$this->session->set_flashdata('success', 'Profile details has been updated');
		}else{
			$this->session->set_flashdata('error', 'Profile details updation failed');
		}
		
		redirect(base_url().'Teacher/userProfile');
	}
	/**********************************************************************/
	public function MultipleMailSystem($users, $subject, $message)
	{
		$email = "admin@billionskills.com";
        $password = "c6spQD82CVfch7aT";

		$mail = new PHPMailer(true);
		//Enable SMTP debugging.
		$mail->SMTPDebug = 0;                               
		//Set PHPMailer to use SMTP.
		$mail->isSMTP();            
		//Set SMTP host name  
		$mail->CharSet = 'utf-8';// set charset to utf8
		//$mail->Encoding = 'base64';
		$mail->SMTPAuth = true;// Enable SMTP authentication
		$mail->SMTPSecure = 'ssl';// Enable TLS encryption, `ssl` also accepted

		$mail->Host = 'lotus.arvixe.com';// Specify main and backup SMTP servers
		$mail->Port = 465;// TCP port to connect to
		$mail->SMTPOptions = array(
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
			)
		);		                     
		//Provide username and password     
		$mail->Username = $email;                 
		$mail->Password = $password;                                                                         

		$mail->From = $email;
		$mail->FromName = 'Magnox+ Plus';

		$mail->isHTML(true);

		$mail->Subject = $subject;
		$mail->Body = $message;
		//$mail->AltBody = "This is the plain text version of the email content";
		foreach ($users as $user) {
		  $mail->addAddress($user['email'], $user['name']);

		  $mail->Body = "<h2>Hello, ".$user['name']."</h2> ".$message."</p>";
		  //$mail->AltBody = "Hello, {$user['name']}! \n How are you?";

		  try {
			  $mail->send();
			  //echo "Message sent to: ({$user['email']}) {$mail->ErrorInfo}\n";
		  } catch (Exception $e) {
			  //echo "Mailer Error ({$user['email']}) {$mail->ErrorInfo}\n";
		  }

		  $mail->clearAddresses();
		}

		return true;//($mail->send())? 1 : $mail->ErrorInfo;
	}
	public function MailSystem($to, $cc, $subject, $message)
	{
		$email = "admin@billionskills.com";
        $password = "c6spQD82CVfch7aT";

		$mail = new PHPMailer(true);
		//Enable SMTP debugging.
		$mail->SMTPDebug = 0;                               
		//Set PHPMailer to use SMTP.
		$mail->isSMTP();            
		//Set SMTP host name  
		$mail->CharSet = 'utf-8';// set charset to utf8
		//$mail->Encoding = 'base64';
		$mail->SMTPAuth = true;// Enable SMTP authentication
		$mail->SMTPSecure = 'ssl';// Enable TLS encryption, `ssl` also accepted

		$mail->Host = 'lotus.arvixe.com';// Specify main and backup SMTP servers
		$mail->Port = 465;// TCP port to connect to
		$mail->SMTPOptions = array(
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
			)
		);		                     
		//Provide username and password     
		$mail->Username = $email;                 
		$mail->Password = $password;                                                                         

		$mail->From = $email;
		$mail->FromName = 'Magnox+ Plus';

		$mail->addAddress($to);

		$mail->isHTML(true);

		$mail->Subject = $subject;
		$mail->Body = $message;
		//$mail->AltBody = "This is the plain text version of the email content";

		return ($mail->send())? 1 : $mail->ErrorInfo;
		$mail->clearAddresses();
	}
	public function MultipleMailSystem_Two($users, $subject, $message)
	{
		$email = "admin@billionskills.com";
        $password = "c6spQD82CVfch7aT";

		$mail = new PHPMailer(true);
		//Enable SMTP debugging.
		$mail->SMTPDebug = 0;                               
		//Set PHPMailer to use SMTP.
		$mail->isSMTP();            
		//Set SMTP host name  
		$mail->CharSet = 'utf-8';// set charset to utf8
		//$mail->Encoding = 'base64';
		$mail->SMTPAuth = true;// Enable SMTP authentication
		$mail->SMTPSecure = 'ssl';// Enable TLS encryption, `ssl` also accepted

		$mail->Host = 'lotus.arvixe.com';// Specify main and backup SMTP servers
		$mail->Port = 465;// TCP port to connect to
		$mail->SMTPOptions = array(
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
			)
		);		                     
		//Provide username and password     
		$mail->Username = $email;                 
		$mail->Password = $password;                                                                         

		$mail->From = $email;
		$mail->FromName = 'Magnox+ Plus';

		$mail->isHTML(true);

		$mail->Subject = $subject;
		$mail->Body = $message;
		//$mail->AltBody = "This is the plain text version of the email content";
		foreach ($users as $user) {
		  $mail->addBCC($user['email']);

			/* $mail->Body = "<h2>Hello, ".$user['name']."</h2> ".$message."</p>";
		  //$mail->AltBody = "Hello, {$user['name']}! \n How are you?";

		  try {
			  $mail->send();
			  //echo "Message sent to: ({$user['email']}) {$mail->ErrorInfo}\n";
		  } catch (Exception $e) {
			  //echo "Mailer Error ({$user['email']}) {$mail->ErrorInfo}\n";
		  }

		  $mail->clearAddresses();*/
		}
		$mail->send();
		$mail->clearAddresses();
		return true;//($mail->send())? 1 : $mail->ErrorInfo;
	}
}