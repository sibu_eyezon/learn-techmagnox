<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once "vendor/autoload.php";
require APPPATH . '/libraries/BaseController.php';

class Autoprogram extends BaseController {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		date_default_timezone_set('Asia/Kolkata');
		ini_set('memory_limit', '-1');
	}
	
	public function generateSectionCode($title){
		$ret = '';
		foreach (explode(' ', $title) as $word){
			$ret .= strtoupper($word[0]);
		}
		return $ret;
	}
	/*=========================SECTION=======================*/
	public function section_add($params1="", $progtype="", $params2="")
	{
		$data['params1'] = $params1;
		$data['params2'] = $params2;
		$data['progtype'] = $progtype;
		$this->load->view('teacher_course/section_add', $data);
	}
	public function section_edit($params1="", $progtype="", $params2="")
	{
		$data['section'] = $this->AutoProgramModel->get_section_by_id($params2);
		$data['params1'] = $params1;
		$data['params2'] = $params2;
		$data['progtype'] = $progtype;
		$this->load->view('teacher_course/section_edit', $data);
	}
	public function section_delete($params="", $progtype="")
	{
		$where = 'id='.$params;
		echo $this->Exam_model->deleteData('pro_course', $where);
	}
	public function cuSections($progtype="")
	{
		$sec_id = (int)$_POST['sec_id'];
		$data['prog_id'] = $_POST['prog_id'];
		$data['title'] = trim($_POST['title']);
		if($data['title'] != ""){
			if($sec_id==0){
				$data['c_code'] = $this->generateSectionCode($data['title']).date('ydH');
				$data['rank'] = (intval($_POST['rank'])+1);
				$data['post_date'] = date('Y-m-d H:i:s');
				echo $this->Exam_model->insertData('pro_course', $data);
			}else{
				$data['c_code'] = $this->generateSectionCode($data['title']).date('ydH');
				$where = 'id='.$sec_id;
				echo $this->Exam_model->updateData('pro_course', $data, $where);
			}
		}else{
			echo 0;
		}
		redirect(base_url().'Teacher/addProgram/'.$progtype.'/?id='.base64_encode($data['prog_id']));
	}
	public function sort_section($params="", $progtype="")
	{
		$data['params'] = $params;
		$data['progtype'] = $progtype;
		$this->load->view('teacher_course/section_sort', $data);
	}
	/*==================LESSONs=============================*/
	public function lesson_add($params="", $progtype="", $params2=0)
	{
		$data['sections'] = $this->AutoProgramModel->get_section($params);
		$data['params'] = $params;
		$data['params2'] = $params2;
		$data['progtype'] = $progtype;
		$this->load->view('teacher_course/lesson_add', $data);
	}
	public function cuLessons($progtype="")
	{
		$tmp = $_FILES['lfiles']['tmp_name'];
		$ori = $_FILES['lfiles']['name'];
		$lfile = uniqid().$ori;
		
		$course_sl = trim($_POST['section_id']);
		$data['course_sl'] = $course_sl;
		$prog = trim($_POST['prog_id']);
		$lecid = trim($_POST['lesson_id']);
		$data['user_id'] = $_SESSION['userData']['userId'];
		$data['title'] = trim($_POST['title']);
		$data['file_type'] = trim($_POST['ltype']);
		if($data['file_type']=='fl'){
			if($ori!=''){
				$data['file_name'] = $lfile;
			}
		}else{
			$data['link'] = trim($_POST['llink']);
		}
		$data['notify'] = true;
		$data['archive'] = false;
		$data['lec_date'] = date('Y-m-d H:i:s');
		
		$clesson = $this->AutoProgramModel->getTotalLessonByCid($course_sl, $prog);
		
		if($lecid==0){
			$data['add_date'] = date('Y-m-d H:i:s');
			$lecid = $this->Exam_model->insertDataRetId('pro_lectures', $data);
			
			$data1['prog_sl'] = $prog;
			$data1['duration'] = $_POST['duration'];
			$data1['type'] = 1;
			$data1['type_sl'] = $lecid;
			$data1['pos_id'] = ($clesson+1);
			$data1['authorid'] = $_SESSION['userData']['userId'];
			$data1['type_name'] = 'lectures';
			$data1['course_id'] = $course_sl;
			$data1['adddttm'] = date('Y-m-d H:i:s');
			$this->Exam_model->insertData('auto_prog', $data1);
			
			if($ori!='' && $data['file_type']=='fl'){
				move_uploaded_file($tmp,'./uploads/courselra/'.$lfile);
			}
		}else{
			$data['modified_date'] = date('Y-m-d H:i:s');
			if($this->Course_model->updateClecture($data, $lecid)){
				if($ori!='' && $data['file_type']=='fl'){
					move_uploaded_file($tmp,'./uploads/courselra/'.$lfile);
				}
			}
		}
		redirect(base_url().'Teacher/addProgram/'.$progtype.'/?id='.base64_encode($prog));
	}
	
	public function get_lessons($spc_id=0, $spsd_id=0)
	{
		if($spsd_id==0){
			$data['cur_lesson'] = $this->AutoProgramModel->get_current_lesson($spc_id, 0);
		}else{
			$data['cur_lesson'] = $this->AutoProgramModel->get_current_lesson($spc_id, 1);
		}
		return $this->load->view('users/view_lesson', $data);
	}
}