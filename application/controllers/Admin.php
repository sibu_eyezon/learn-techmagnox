<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once "vendor/autoload.php";
require APPPATH . '/libraries/BaseController.php';

class Admin extends BaseController {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		date_default_timezone_set('Asia/Kolkata');
		ini_set('memory_limit', '-1');
	}
	
	public function MailSystem($to, $cc, $subject, $message)
	{
		$email = "admin@billionskills.com";
        $password = "c6spQD82CVfch7aT";

		$mail = new PHPMailer(true);
		//Enable SMTP debugging.
		$mail->SMTPDebug = 0;                               
		//Set PHPMailer to use SMTP.
		$mail->isSMTP();            
		//Set SMTP host name  
		$mail->CharSet = 'utf-8';// set charset to utf8
		//$mail->Encoding = 'base64';
		$mail->SMTPAuth = true;// Enable SMTP authentication
		$mail->SMTPSecure = 'ssl';// Enable TLS encryption, `ssl` also accepted

		$mail->Host = 'lotus.arvixe.com';// Specify main and backup SMTP servers
		$mail->Port = 465;// TCP port to connect to
		$mail->SMTPOptions = array(
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
			)
		);		                     
		//Provide username and password     
		$mail->Username = $email;                 
		$mail->Password = $password;                                                                         

		$mail->From = $email;
		$mail->FromName = 'Magnox+ Plus';

		$mail->addAddress($to);

		$mail->isHTML(true);

		$mail->Subject = $subject;
		$mail->Body = $message;
		//$mail->AltBody = "This is the plain text version of the email content";

		return ($mail->send())? 1 : 0;
		//$mail->ErrorInfo;
	}
	/********************************MENU*********************************/
	public function index()
	{
		if($this->isLoggedIn()){
			$userid = $_SESSION['userData']['userId'];
			$this->global['pageTitle'] = 'Dashboard | Magnox Learning+ - Admin';
			$data['cprof'] = $this->Admin_model->getTotalUsersByType('teacher');
			$data['cstud'] = $this->Admin_model->getTotalUsersByType('student');
			$data['caprog'] = $this->Admin_model->getTotalProgramsByStatus('approved');
			$data['cpprog'] = $this->Admin_model->getTotalProgramsByStatus('pending');
			$this->loadAdminViews("index", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	/*===================================================================================*/
	/*===================================================================================*/
	public function program($params="approved")
	{
		if($this->isLoggedIn()){
			$userid = $_SESSION['userData']['userId'];
			$this->global['pageTitle'] = ucfirst($params).' Programs | Magnox Learning+ - Admin';
			$data['params'] = $params;
			$data['programs'] = $this->Admin_model->getProgramsByStatus($params);
			$this->loadAdminViews("program-list", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function changeProgramStatus($status, $prog_id, $msg="")
	{
		$res = array('status'=>false, 'msg'=>'Something went wrong.');
		$getUser = $this->Admin_model->getProgramCreator($prog_id);
		$subject = 'Program Approval Notice';
		$message = 'Hello '.$getUser[0]->uname.'<br>   Your program - '.trim($getUser[0]->title).' has been '.$status.'.<br>';
		if($msg!=""){
			$message.='Reason is as follow:<br>'.$msg.'<br>';
		}
		$message.= '<br>Thanking you,<br><img src="https://learn.techmagnox.com/assets/img/logo.png" style="width:100px;"/><br>Magnox Learning Plus,<br>Learning and Course Management Platform';
		$data['status'] = $status;
		$data['statusch_date'] = date('Y-m-d H:i:s');
		$where = 'id='.$prog_id;
		$this->Exam_model->updateData('program', $data, $where);
		$res = array('status'=>true, 'msg'=>' - program status has been updated.');
		/*if($this->MailSystem($getUser[0]->email,'',$subject, $message)){
			$data['status'] = $status;
			$data['statusch_date'] = date('Y-m-d H:i:s');
			$where = 'id='.$prog_id;
			$this->Exam_model->updateData('program', $data, $where);
			$res = array('status'=>true, 'msg'=>' - program status has been updated.');
		}else{
			$res = array('status'=>false, 'msg'=>'Something went wrong.');
		}*/
		echo json_encode($res);
	}
	public function viewProgramDetails()
	{
		if($this->isLoggedIn()){
			$i=1;
			$this->global['pageTitle'] = 'Program Details | Magnox Learning+';
			$data['prog_id'] = base64_decode($_GET['id']);
			$data['prog'] = $this->LoginModel->getProgramById($data['prog_id']);
			$data['institute'] = $this->Member->getProgramOrg($data['prog_id']);
			$data['stream'] = $this->Member->getProgramStreams($data['prog_id']);
			$data['pprof'] = $this->Member->getAllRequestByIdRole($data['prog_id'], 'Teacher', null, 'accepted');
			$data['sems'] = $this->LoginModel->getAllSemsByProgId($data['prog_id']);
			$countsms = count($data['sems']);
			if($countsms<=1){
				$data['procourse'] = $this->Course_model->getProgramCourses($data['prog_id']);
			}else{
				
				foreach($data['sems'] as $row){
					$sem_id = $row->id;
					$data['procourse_'.$i] = $this->LoginModel->getProgSemCourses($sem_id, $data['prog_id']);
					$i++;
				}
			}
			
			$this->loadAdminViews("program-details", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	/*===================================================================================*/
	/*===================================================================================*/
	public function organizations()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Organizations | Magnox Learning+ - Admin';
			$data['orgs'] = $this->Admin_model->get_all_user_organizations();
			$this->loadAdminViews("organizations-list", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function deleteOrganization()
	{
		if(isset($_GET['clid'])){
			echo $this->Exam_model->deleteData('organization', 'id='.$_GET['clid']);
		}else{
			echo false;
		}
	}
	public function toggleBlockOrganization()
	{
		if(isset($_GET['clid'])){
			$status = $_GET['status'];
			echo $this->Exam_model->updateData('organization', array('status'=>$status), 'id='.$_GET['clid']);
		}else{
			echo false;
		}
	}
	/*===================================================================================*/
	/*===================================================================================*/
	public function users($params="")
	{
		if($this->isLoggedIn()){
			$userid = $_SESSION['userData']['userId'];
			$this->global['pageTitle'] = ucfirst($params).' | Magnox Learning+ - Admin';
			$data['params'] = $params;
			$data['users'] = $this->Admin_model->getUsersByType($params);
			$this->loadAdminViews("user-list", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	
	public function addUser($param1="", $param2="")
	{
		if($this->isLoggedIn()){
			$obj = array('id'=>0, 'first_name'=>"", 'last_name'=>"", 'user_type'=>$param1, 'email'=>"", 'phone'=>"", 'organization'=>"", 'designation'=>"", 'about_me'=>"");
			$userid = $_SESSION['userData']['userId'];
			$this->global['pageTitle'] = (($param2=="")? 'Add '.ucfirst($param1) : 'Update '.ucfirst($param1)).' | Magnox Learning+ - Admin';
			if($param2==""){
				$data['user'][0] = (object)$obj;
			}else{
				$data['user'] = $this->Admin_model->getUserDetailsById($param2);
			}
			$data['params'] = $param1;
			$this->loadAdminViews("add-edit-users", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function cuTeacher()
	{
		$this->load->helper('string');
		$vcode = random_string('numeric', 6);
		
		$user_id = $_POST['user_id'];
		$fname = trim(ucwords(strtolower($_POST['fname'])));
		$lname = trim(ucwords(strtolower($_POST['lname'])));
		$email = strtolower(trim($_POST['email']));
		$phone = trim($_POST['phone']);
		$about = trim($_POST['about_me']);
		if($user_id==0){
			$subject = 'New Teacher-Registration';
			$message = 'Hello '.trim($fname." ".$lname).'<br><br>Your account has been created at <a href="'.base_url().'" target="_blank">Magnox Learning</a>.<br><br>Please click <a href="'.base_url().'Login/userVerification/?email='.base64_encode($email).'&vercode='.base64_encode($vcode).'" target="_blank">here</a> to verify your email and complete your registration.<hr>';
			$message.= '<br>Thanking you,<br><img src="https://learn.techmagnox.com/assets/img/logo.png" style="width:100px;"/><br>Magnox Learning Plus,<br>Learning and Course Management Platform';
			if($this->MailSystem($email, '', $subject, $message)){
				$data['first_name'] = $fname;
				$data['last_name'] = $lname;
				$data['email'] = $email;
				$data['phone'] = $phone;
				$data['about_me'] = $about;
				$data['organization'] = trim($_POST['organization']);
				$data['designation'] = trim($_POST['designation']);
				$userid = $this->Exam_model->insertDataRetId('user_details', $data);
				
				$data1['user_id'] = $userid;
				$data1['first_name'] = $fname;
				$data1['last_name'] = $lname;
				$data1['email'] = $email;
				$data1['phone'] = $phone;
				$data1['password'] = md5(trim($_POST['passwd']));
				$this->Exam_model->insertData('user_auth', $data1);
				$this->session->set_flashData('error', 'The Account has been created and the teacher has been mailed.');
				redirect(base_url('Admin/addUser'));
			}else{
				$this->session->set_flashData('error', 'Mail send error.');
				redirect(base_url('Admin/addUser/'.$user_id));
			}
			
		}else{
			$where = 'id='.$user_id;
			$data['first_name'] = $fname;
			$data['last_name'] = $lname;
			$data['email'] = $email;
			$data['phone'] = $phone;
			$data['about_me'] = $about;
			$data['organization'] = trim($_POST['organization']);
			$data['designation'] = trim($_POST['designation']);
			$this->Exam_model->updateData('user_details', $data, $where);
			
			$where = 'user_id='.$user_id;
			$data1['first_name'] = $fname;
			$data1['last_name'] = $lname;
			$data1['email'] = $email;
			$data1['phone'] = $phone;
			$this->Exam_model->updateData('user_auth', $data1, $where);
			$this->session->set_flashData('error', 'The Account has been update.');
			redirect(base_url('Admin/addUser'));
		}
	}
	/*===================================================================================*/
	/*===================================================================================*/
	public function feedbacks()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Feedbacks | Magnox Learning+ - Admin';
			$data['fdlist'] = $this->Admin_model->getAllFeedbacks();
			$this->loadAdminViews("feedbacks", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function changeFeedbackStatus($id=0, $stat="0")
	{
		if($id!=0){
			$where = 'sl='.$id;
			$data['status'] = $stat;
			echo ($this->Exam_model->updateData('feedback', $data, $where))? json_encode(array('status'=>true)) : json_encode(array('status'=>false));
		}else{
			echo json_encode(array('status'=>false));
		}
	}
	/*===================================================================================*/
	/*===================================================================================*/
	public function category($param1="list", $param2="")
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Category '.ucfirst($param1).' | Magnox Learning+ - Admin';
			$data['params'] = $param1;
			if($param1=="list"){
				$data['clist'] = $this->Admin_model->getParentCategories(0);
				$this->loadAdminViews("list-category", $this->global, $data, NULL);
			}else if($param1=="add" || $param1=="edit"){
				$id = (isset($_GET['id'])? $_GET['id']: NULL);
				$obj = array('id'=>0, 'name'=>"", 'slug'=>"", 'parent'=>0, 'thumbnail'=>"", 'details'=>"");
				if($id!=NULL){
					$data['cat'] = $this->Admin_model->getParentCategoryById($id);
				}else{
					$data['cat'][0] = (object)$obj;
				}
				$data['clist'] = $this->Admin_model->getParentCategories(0);
				$this->loadAdminViews("add-edit-category", $this->global, $data, NULL);
			}
		}else{
			redirect(base_url());
		}
	}
	public function cuCategory()
	{
		$this->load->helper('string');
		$cid = $_POST['cid'];
		$name = html_escape(trim($_POST['catname']));
		if($name!=""){
			$data['name'] = $name;
			$data['slug'] = url_title($name,'-',TRUE);
			$data['parent'] = $_POST['pcat_list'];
			$data['details'] = trim($_POST['details']);
			if($cid==0){
				$data['date_added'] = date('Y-m-d H:i:s');
				$this->Exam_model->insertData('category', $data);
				$this->session->set_flashData('error', 'The category has been added.');
			}else{
				$data['last_modified'] = date('Y-m-d H:i:s');
				$where = 'id='.$cid;
				$this->Exam_model->updateData('category', $data, $where);
				$this->session->set_flashData('error', 'The category has been updated.');
			}
		}else{
			$this->session->set_flashData('error', 'Must enter category name.');
		}
		redirect(base_url('Admin/category/add'));
	}
	public function removeCategory($id)
	{
		$where = 'id='.$id;
		echo ($this->Exam_model->deleteData('category', $where))? json_encode(array('status'=>true)) : json_encode(array('status'=>false));
	}
	/*===================================================================================*/
	/*===================================================================================*/
	public function topics($param1="list", $param2="")
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Topics '.ucfirst($param1).' | Magnox Learning+ - Admin';
			$data['params'] = $param1;
			if($param1=="list"){
				$data['tlist'] = $this->Admin_model->getAllTopics();
				$this->loadAdminViews("list-topics", $this->global, $data, NULL);
			}else if($param1=="add" || $param1=="edit"){
				$id = (isset($_GET['id'])? $_GET['id']: NULL);
				$obj = array('sl'=>0, 'tname'=>"", 'slug'=>"", 'video'=>"", 'details'=>"", 'brochure'=>"");
				if($id!=NULL){
					$data['topic'] = $this->Admin_model->getTopicById($id);
				}else{
					$data['topic'][0] = (object)$obj;
				}
				$this->loadAdminViews("add-edit-topic", $this->global, $data, NULL);
			}
		}else{
			redirect(base_url());
		}
	}
	public function cuTopic()
	{
		$tmp_name = $_FILES['brochure']['tmp_name'];
		$ori = $_FILES['brochure']['name'];
		$thumb = uniqid().str_replace(" ","_",html_escape($ori));
		if (!file_exists('uploads/topics')) {
			mkdir('uploads/topics', 0777, true);
		}
		$this->load->helper('string');
		$tid = $_POST['tid'];
		$name = html_escape(trim($_POST['tname']));
		if($name!=""){
			$data['tname'] = $name;
			$data['slug'] = url_title($name,'-',TRUE);
			$data['video'] = trim($_POST['videos']);
			if($ori!=""){
				$data['brochure'] = $thumb;
				move_uploaded_file($tmp_name, './uploads/topics/'.$thumb);
			}
			$data['details'] = trim($_POST['details']);
			if($tid==0){
				$data['added_date'] = date('Y-m-d H:i:s');
				$this->Exam_model->insertData('topics', $data);
				$this->session->set_flashData('error', 'The topic has been added.');
			}else{
				$data['modified_date'] = date('Y-m-d H:i:s');
				$where = 'sl='.$tid;
				$this->Exam_model->updateData('topics', $data, $where);
				$this->session->set_flashData('error', 'The topic has been updated.');
			}
		}else{
			$this->session->set_flashData('error', 'Must enter topic name.');
		}
		redirect(base_url('Admin/topics/add'));
	}
	public function removeTopics($id)
	{
		$where = 'sl='.$id;
		echo ($this->Exam_model->deleteData('topics', $where))? json_encode(array('status'=>true)) : json_encode(array('status'=>false));
	}
	/*===================================================================================*/
	/*===================================================================================*/
	public function coupons($param1="list", $param2="")
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Coupons '.ucfirst($param1).' | Magnox Learning+ - Admin';
			$data['params'] = $param1;
			if($param1=="list"){
				$data['cclist'] = $this->Admin_model->getAllCoupons();
				$this->loadAdminViews("list-coupons", $this->global, $data, NULL);
			}else if($param1=="add" || $param1=="edit"){
				$id = (isset($_GET['id'])? $_GET['id']: NULL);
				$obj = array('sl'=>0, 'coupon_code'=>"", 'coupon_type'=>"", 'discount_amt'=>0, 'discount_percent'=>0, 'comments'=>"", 'is_active'=>false);
				if($id!=NULL){
					$data['coupon'] = $this->Admin_model->getCouponById($id);
				}else{
					$data['coupon'][0] = (object)$obj;
				}
				$this->loadAdminViews("add-edit-coupon", $this->global, $data, NULL);
			}
		}else{
			redirect(base_url());
		}
	}
	public function cuCoupon()
	{
		$this->load->helper('string');
		$cid = $_POST['cid'];
		
		$data['coupon_code'] = strtoupper(html_escape(trim($_POST['coupon_code'])));
		$data['coupon_type'] = trim($_POST['coupon_type']);
		$data['discount_amt'] = trim($_POST['discount_amt']);
		$data['discount_percent'] = trim($_POST['discount_percent']);
		$data['comments'] = trim($_POST['comments']);
		$data['is_active'] = false;
		if($cid==0){
			$data['add_datetime'] = date('Y-m-d H:i:s');
			$this->Exam_model->insertData('coupons', $data);
				$this->session->set_flashData('error', 'The coupon has been added.');
		}else{
			
		}
		redirect(base_url('Admin/coupons/add'));
	}
	public function updateStatusCoupon($id, $stat)
	{
		$data['is_active'] = ($stat=='block')? false : true;
		$where = 'sl='.$id;
		echo ($this->Exam_model->updateData('coupons', $data, $where))? json_encode(array('status'=>true)) : json_encode(array('status'=>false));
	}
	public function removeCoupon($id)
	{
		$where = 'sl='.$id;
		echo ($this->Exam_model->deleteData('coupons', $where))? json_encode(array('status'=>true)) : json_encode(array('status'=>false));
	}
	/*===================================================================================*/
	/*===================================================================================*/
	public function skills($param1="list", $param2="")
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Skills '.ucfirst($param1).' | Magnox Learning+ - Admin';
			$data['params'] = $param1;
			if($param1=="list"){
				$data['slist'] = $this->Admin_model->getAllSkills();
				$this->loadAdminViews("list-skills", $this->global, $data, NULL);
			}else if($param1=="add" || $param1=="edit"){
				$id = (isset($_GET['id'])? $_GET['id']: NULL);
				$obj = array('id'=>0, 'name'=>"", 'desc'=>"", 'slogo'=>"");
				if($id!=NULL){
					$data['skill'] = $this->Admin_model->getSkillById($id);
				}else{
					$data['skill'][0] = (object)$obj;
				}
				$this->loadAdminViews("add-edit-skill", $this->global, $data, NULL);
			}
		}else{
			redirect(base_url());
		}
	}
	public function cuSkill()
	{
		$tmp_name = $_FILES['slogo']['tmp_name'];
		$ori = $_FILES['slogo']['name'];
		$thumb = uniqid().str_replace(" ","_",html_escape($ori));
		if (!file_exists('assets/img/skills')) {
			mkdir('assets/img/skills', 0777, true);
		}
		$this->load->helper('string');
		$sid = $_POST['sid'];
		$name = html_escape(trim($_POST['sname']));
		if($name!=""){
			$data['name'] = $name;
			$data['desc'] = trim($_POST['details']);
			if($ori!=""){
				$data['slogo'] = $thumb;
				move_uploaded_file($tmp_name, './assets/img/skills/'.$thumb);
			}
			if($sid==0){
				$data['add_datetime'] = date('Y-m-d H:i:s');
				$this->Exam_model->insertData('skills', $data);
				$this->session->set_flashData('error', 'The skill has been added.');
			}else{
				$data['modified_datetime'] = date('Y-m-d H:i:s');
				$where = 'id='.$sid;
				$this->Exam_model->updateData('skills', $data, $where);
				$this->session->set_flashData('error', 'The skill has been updated.');
			}
		}else{
			$this->session->set_flashData('error', 'Must enter skill name.');
		}
		redirect(base_url('Admin/skills/add'));
	}
	public function removeSkills($id)
	{
		$where = 'id='.$id;
		echo ($this->Exam_model->deleteData('skills', $where))? json_encode(array('status'=>true)) : json_encode(array('status'=>false));
	}
	/*=============================================================================================*/

	/*=====================================FAQ=====================================================*/

	public function admin_faq_page($params){
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = ucfirst($params).' | Magnox Learning+ - Admin';
			$data = array();
			$this->loadAdminViews("admin-faq", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}

	/*=============================================================================================*/

	/*=====================================Alumni Speak==============================================*/

	public function admin_alumni_speak_page($params){
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Alumni '.ucfirst($params).' | Magnox Learning+ - Admin';
			$data = array();
			$this->loadAdminViews("admin-alumni-speak", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}	

	/*===================================================================================*/

	/*================================== Project ========================================*/

	public function admin_project_page($params){
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = ucfirst($params).' | Magnox Learning+ - Admin';
			$data = array();
			$this->loadAdminViews("admin-project", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}		
	}



	/*===================================================================================*/

	/*================================== Instructor =====================================*/
	public function admin_instructor_page($params){
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = ucfirst($params).' | Magnox Learning+ - Admin';
			$data = array();
			$this->loadAdminViews("admin-instructor", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}

	/*===================================================================================*/

	/*===================================== Job =========================================*/
	public function admin_job_page($params){
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = ucfirst($params).' | Magnox Learning+ - Admin';
			$data = array();
			$this->loadAdminViews("admin-job", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}

	/*===================================================================================*/

	/*===================================== Blogs =========================================*/
	public function blog_page(){
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = ' Blogs | Magnox Learning+ - Admin';
			$data = array();
			$page_number       = (!empty($_GET['page'])) ? base64_decode($_GET['page']) : 1;
			$per_page_record   = 10;
			$row               = $this->Admin_model->get_blogs();
			$last_page         = ceil($row/$per_page_record);
			if($page_number < 1):
				$page_number = 1;
			elseif($page_number > $last_page):
				$page_number = $last_page;
			endif;
			$start_record   = abs(($page_number - 1) * $per_page_record);
			$data['page_number']   = $page_number;
            $data['last_page']     = $last_page;
            $data['result'] = $this->Admin_model->get_blogs($start_record, $per_page_record);
            $data['slNo']   = ($per_page_record * ($page_number - 1)) + 1;
			$this->loadAdminViews("blog-page", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}

	public function add_edit_blog_form(){
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = ' Blogs | Magnox Learning+ - Admin';
			$data = array();
			$data['id']   = (!empty($_GET['id'])) ? base64_decode($_GET['id']) : 0;
			$data['page'] = (!empty($_GET['page'])) ? base64_decode($_GET['page']) : 1;
			$data['categories'] = $this->Admin_model->get_category();
			$data['skills']     = $this->Admin_model->get_skill();
			if($data['id'] != 0){
				$data['title'] = "Edit Blog";
				$data['result'] = $this->Admin_model->get_blog_by_id($data['id']);
			}else{
				$data['title']  = "Add New Blog";
				$data['result'][0] = (object)array('title'=>'', 'banner'=>'', 'body'=>'', 'categories'=>'', 'skills'=>'');
			}
			$this->loadAdminViews("blog-form", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}

	public function add_edit_blog(){
		if($this->isLoggedIn()){
			$id         = (!empty($_POST['id']))? base64_decode($_POST['id']) : 0 ;
			$title      = $_POST['title'];
			$categories = json_encode($_POST['categories']);
			$skills     = json_encode($_POST['skills']);
			$body       = $_POST['body'];
			$preBanner  = $_POST['abspath'];
			$page       = $_POST['page'];
			$image      = '';

            if(!empty($_FILES['banner']['name'])){
                $pic = $_FILES['banner']['name'];
                $config['upload_path']   = './assets/img/banner/'; 
                $config['allowed_types'] = 'jpeg|jpg|png'; 
                $config['file_name']     = time().$pic;
                $this->load->library('upload', $config);
				// echo "<pre>";
				// print_r();
				$this->upload->do_upload('banner');
                if($this->upload->do_upload('banner')){
                    $file = $this->upload->data();
                    print_r($file);
                    $image = $file['file_name'];
                    if(!empty($preImage)){
                        unlink(".{$preImage}");
                    }
                }else{
					echo $this->upload->display_errors();
				}
            }

			$data = array(
				'title'      => $title,
				'banner'     => (!empty($image))? "/assets/img/banner/{$image}" : $preBanner,
				'body'       => $body,
				'categories' => $categories,
				'skills'     => $skills,
				'status'     => true,
				'active'     => true,
			);
			if($id == 0){
				$data['create_date_time'] = date('Y-m-d H:i:s');
			}else{
				$data['update_date_time'] = date('Y-m-d H:i:s');
			}
			if($this->Admin_model->add_edit_blog($data, $id)){
				redirect(base_url('Admin/blog_page/?page='.$page));
			}else{
				if($id){
					redirect(base_url('Admin/add_edit_blog_form/?id='.base64_encode($id).'&page='.$page));
				}else{
					redirect(base_url('Admin/add_edit_blog_form/?page='.$page));
				}
			}
		}else{
			redirect(base_url());
		}		
	}

	public function change_blog_status(){
		$id    = base64_decode($_GET['id']);
		$page  = $_GET['page'];
		$state = $_GET['state'];
		$data  = array(
			'status'     => ($state == 't')? false : true,
		);
		if($this->Admin_model->add_edit_blog($data, $id)){
			redirect(base_url('Admin/blog_page/?page='.$page));
		}
	}

	public function delete_blog(){
		$id = base64_decode($_GET['id']);
		$page = $_GET['page'];
		$data  = array(
			'active' => false
		);
		if($this->Admin_model->add_edit_blog($data, $id)){
			redirect(base_url('Admin/blog_page/?page='.$page));
		}
	}

	public function add_instructor_with_programs(){
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = ' Blogs | Magnox Learning+ - Admin';
			$data = array();
			$data['result'] = $this->Admin_model->to_get_program_list();
			$this->loadAdminViews("add-instructors-with-program", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}

	public function add_logined_user_admin_registration(){
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = ' ADMIN | Magnox Learning+ - Admin';
			$data = array();
			$data['result'] = $this->Admin_model->to_get_program_list();
			$this->loadAdminViews("add_logined_user_admin_registration", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	
	public function booking_trial_class()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Book Trial Class List | Magnox Learning+ - Admin';
			$data = array();
			$data['result'] = $this->Admin_model->to_all_bookings_for_trial_class();
			$this->loadAdminViews("booking_trial_list", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function removeBooking($clid)
	{
		if(isset($clid)){
			$where = 'id='.$clid;
			echo $this->Exam_model->deleteData('junior_trial_class', $where);
		}else{
			echo 0;
		}
	}
	
	
	/*========================================================================================*/
	public function newsletters()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Email Template | Magnox Learning+ - Admin';
			$data = array();
			$data['result'] = $this->Admin_model->get_all_newsletters();
			$this->loadAdminViews("newsletter_list", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function add_newsletter()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Add Email Template | Magnox Learning+ - Admin';
			$data = array();
			if(!isset($_GET['id'])){
				$data['result'][0] = (object)array('id'=>0, 'title'=>"", 'details'=>"");
			}else{
				$data['result'] = $this->Admin_model->get_newsletter_by_id($_GET['id']);
			}
			
			$this->loadAdminViews("newsletter_add", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function cuNewsletter()
	{
		$id = $_POST['cid'];
		$data['title'] = trim($_POST['title']);
		$data['details'] = trim($_POST['desc']);
		$data['status'] = 'S';
		if($id == 0){
			$data['add_datetime'] = date('Y-m-d H:i:s');
			$this->Exam_model->insertData('email_template', $data);
		}else{
			$data['update_datetime'] = date('Y-m-d H:i:s');
			$where = 'id='.$id;
			$this->Exam_model->updateData('email_template', $data, $where);
		}
		redirect(base_url('Admin/newsletters'));
	}
	public function publishNewsletter($clid)
	{
		if(isset($clid)){
			$result = $this->Admin_model->get_newsletter_by_id($clid);
			
			$data['event_name'] = 'BULK_STUDENT_NEWS_PUBLISH';
			$data['data'] = json_encode([]);
			$data['status'] = 'NEW';
			$data['template_subject_text'] = trim($result[0]->title);
			$data['template_body_text'] = trim($result[0]->details);
			$data['message_subject'] = '';
			$data['message_body'] = '';
			$data['to_address'] = '';
			$data['cc_address'] = '';
			$data['retry_count'] = 0;
			$data['add_datetime'] = date('Y-m-d H:i:s');
			$this->Exam_model->insertData('email_request', $data);
			
			/*if($this->MailSystem($data['to_address'],$data['cc_address'],$data['message_subject'], $data['message_body'])){
				return true;
			}*/
			
			$data1['status'] = 'P';
			$data1['update_datetime'] = date('Y-m-d H:i:s');
			$where = 'id='.$clid;
			echo $this->Exam_model->updateData('email_template', $data1, $where);
		}else{
			echo 0;
		}
	}
	public function removeNewsletter($clid)
	{
		if(isset($clid)){
			$data['status'] = 'D';
			$data['update_datetime'] = date('Y-m-d H:i:s');
			$where = 'id='.$clid;
			echo $this->Exam_model->updateData('email_template', $data, $where);
		}else{
			echo 0;
		}
	}
}