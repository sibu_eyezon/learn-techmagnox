<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . '/libraries/BaseController.php';
    
    class Calendar extends BaseController {

        public function __construct(){
		    parent::__construct();
            $this->load->model('Calendar_model');
        }

        public function scheduleClass(){
            if($this->isLoggedIn()){
                $userid = $_SESSION['userData']['userId'];
                $this->global['pageTitle'] = 'Schedule Class | Magnox Learning+ - Teacher';
                $data = array();
                $this->loadScheduleClassViews("index", $this->global, $data, NULL);
            }else{
                redirect(base_url());
            }
        }

        public function clssTable(){
            if($this->isLoggedIn()){
                $userid = $_SESSION['userData']['userId'];
                $result = $this->Calendar_model->getScheduleClass($userid);
                $data   = array();
                foreach($result as $row){
                    $data[] = array(
                        'id' => $row->sl,
                        'title' => $row->class_title,
                        'start' => $row->start_datetime, //date('Y-m-d', strtotime($row->start_datetime)),
                        'end'   => $row->end_datetime, //date('Y-m-d', strtotime($row->end_datetime))
                    );
                }
                echo json_encode($data);
            }else{
                redirect(base_url());
            }
        }

        public function getScheduleModalBody(){
            if($this->isLoggedIn()){
                $userid = $_SESSION['userData']['userId'];
                $data = array();
                $data['program'] = $this->Calendar_model->programByTeacher($userid);
                return $this->load->view('calendar/scheduleModalBody',$data); 
            }else{
                redirect(base_url());
            }
        }

        public function getTotalCourseByProgram(){
            if($this->isLoggedIn()){
                $userid = $_SESSION['userData']['userId'];
                $prog = (int)($_POST['prog']); 
                $totalCourse = $this->Calendar_model->getTotalCourseByProgram($prog, $userid);
                echo json_encode($totalCourse);
            }else{
                redirect(base_url());
            }
        }

        public function getCourseByProgram(){
            if($this->isLoggedIn()){
                $userid = $_SESSION['userData']['userId'];
                $prog = (int)($_POST['prog']);
                $courses = $this->Calendar_model->getCourseByProgramAndTeacher($prog, $userid);
                $html    = '<option value=""> --- Select --- </option>';
                foreach($courses as $row){
                    $html .= '<option value="'.$row->id.'">'.$row->title.'</option>';
                }
                echo $html;
            }else{
                redirect(base_url());
            }
        }

        public function getAddScheduleFooterButton(){
            if($this->isLoggedIn()){
                $html = '
                    <button type="button" class="btn btn-danger btn-sm mr-3" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm" onClick="addNewSchedule()">Save</button>
                ';
                echo $html;
            }else{
                redirect(base_url());
            }
        }

        public function dateRange( $first, $last, $step = '+1 day', $format = 'Y-m-d' ) {
            $dates = array();
            $current = strtotime( $first );
            $last = strtotime( $last );
        
            while( $current <= $last ) {
                $dates[] = date( $format, $current );
                $current = strtotime( $step, $current );
            }
            return $dates;
        }

        public function generateRangeOfDates(){
            if($this->isLoggedIn()){
                $userid = $_SESSION['userData']['userId'];
                $startDate = $_POST['startDate'];
                $endDate   = $_POST['endDate'];
                // print_r( $this->dateRange( $startDate, $endDate) );
                $data['dates']  = $this->dateRange( $startDate, $endDate);
                return $this->load->view('calendar/timeSceduleTable',$data);                
            }else{
                redirect(base_url());
            }
        }

        public function addNewSchedule(){
            if($this->isLoggedIn()){
                $userid     = $_SESSION['userData']['userId'];
                $resp       = array('status'=>'', 'msg'=>'');
                $program_id = $_POST['program_id'];
                $course_id  = $_POST['course_id'];
                $title      = $_POST['title'];
                $startTime  = $_POST['startTime'];
                $endTime    = $_POST['endTime'];
                $count      = $_POST['count'];
                $date_time  = date('Y-m-d H:i:s');
                $data       = array();

                for($i = 0; $i < $count;$i++){
                    $data[] = array(
                        'program_sl'     => (int)$program_id,
                        'course_sl'      => (int)$course_id,
                        'class_title'    => $title,
                        'user_id'        => $userid,
                        'start_datetime' => $startTime[$i],
                        'end_datetime'   => $endTime[$i],
                        'add_datetime'   => $date_time
                    );
                }
                $this->Calendar_model->addNewSchedule($data);
            }else{
                redirect(base_url());
            }
        }

/************************************************************************************************************************************************** */
        public function studentScheduleView(){
            if($this->isLoggedIn()){
                $userid = $_SESSION['userData']['userId'];
                $this->global['pageTitle'] = 'Schedule Class | Magnox Learning+ - Student';
                $data = array();
                $this->loadUserScheduleClassViews("student_index", $this->global, $data, NULL);
            }else{
                redirect(base_url());
            }
        }

        public function studentClassSchedule(){
            if($this->isLoggedIn()){
                $userid = $_SESSION['userData']['userId'];
                $result = $this->Calendar_model->getStudentClassSchedule($userid);
                $data   = array();
                // echo "<pre>";
                // print_r($result);
                foreach($result as $row){
                    $data[] = array(
                        'id' => $row->sl,
                        'title' => $row->class_title,
                        'start' => $row->start_datetime, //date('Y-m-d', strtotime($row->start_datetime)),
                        'end'   => $row->end_datetime, //date('Y-m-d', strtotime($row->end_datetime))
                    );
                }
                echo json_encode($data);
            }else{
                redirect(base_url());
            }
        }

    }

?>