<?php
    require 'vendor/autoload.php';
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

    class PhpSpreadSheet extends CI_Controller{

        public function __constructor(){
            parent::__construct();
        }

        private function get_unque_excel_sheet_id(){
            $rand = rand(1,1000000);
            if($this->Admin_model->isThisSheetIdExiste($rand)){
                $this->get_unque_excel_sheet_id();
            }else{
                return $rand;
            }
        }


        private function is_email_valid_and_exists($email){
            if($this->Admin_model->check_email_before_upload_excel($email) == 0 && filter_var($email, FILTER_VALIDATE_EMAIL)){
                return true;
            }else{
                return false;
            }
        }

        private function is_phone_valid_and_exists($phone){
            $regex = "/^[6-9]{1}[0-9]{9}$/";
            if(preg_match($regex,$phone)){
                return true;
            }else{
                return false; 
            }
        }

        public function uploadUserRegistrationSpradSheet(){
            $responce   = array('message'=> '', 'type'=> 'error');
            $uploadFile = $_FILES['cvsUploader']['name'];
            $extension   = pathinfo($uploadFile, PATHINFO_EXTENSION);
            if($extension == 'xlsx') {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            }elseif($extension == 'xls'){
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
            }else{
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
            }
            $spreadSheet = $reader->load($_FILES['cvsUploader']['tmp_name']);
            $sheetData   = $spreadSheet->getActiveSheet()->toArray();
            $sheetCount  = count($sheetData);
            $sheetId     = $this->get_unque_excel_sheet_id();
            $data        = array();
            if($sheetCount > 1){
                $j = 0;
                for($i=1; $i<$sheetCount; $i++){
                    $firstName = $sheetData[$i][1];
                    $lastName  = $sheetData[$i][2];
                    $email     = $sheetData[$i][3];
                    $phone     = $sheetData[$i][4];
                    if(!empty($firstName) && !empty($lastName) && !empty($email) && !empty($phone)){
                        
                        if($this->is_email_valid_and_exists($email) && $this->is_phone_valid_and_exists($phone)){
                            $data[$j] = array(
                                'txt_first_name' => $firstName,
                                'txt_last_name'  => $lastName,
                                'txt_emailid'    => $email,
                                'phone_no'       => $phone,
                                'num_file_id'    => $sheetId,
                                'file_name'      => $uploadFile,
                                'upload_date'    => date('Y-m-d')
                            );
                            $j++;
                        }

                    }
                }

                if(count($data) > 0){
                    if($this->Admin_model->uploadUserRegistrationSpradSheet($data, $sheetId)){
                        $responce['message'] = 'Excel is successfully updated';
                        $responce['type'] = 'success';
                    }else{
                        $responce['message'] = 'Excel updalodation is faild';
                    }
                }else{
                    $responce['message'] = 'No data added';
                }

                echo json_encode($responce);
            }
        }

    }