<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Job extends BaseController {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('Job_model', 'jm');
	}

    public function get_pagination(){
        $data['count']       = $_POST['count'];
        $data['page_number'] = $_POST['page_number'];
        $data['last_page']   = $_POST['last_page'];
        $data['func']        = $_POST['func'];
        return $this->load->view('job/job-pagination', $data);
    }

    public function get_job_list(){
        if($this->isLoggedIn()){
            $page_number       = (!empty($_POST['page'])) ? $_POST['page'] : 1;
			$per_page_record   = 10;
			$row               = $this->jm->get_job_list();
			$last_page         = ceil($row/$per_page_record);
			if($page_number < 1):
				$page_number = 1;
			elseif($page_number > $last_page):
				$page_number = $last_page;
			endif;
			$start_record   = abs(($page_number - 1) * $per_page_record);
            $data['result'] = $this->jm->get_job_list($start_record, $per_page_record);
            $data['page_number']   = $page_number;
            $data['last_page']     = $last_page;
            $data['slNo']   = ($per_page_record * ($page_number - 1)) + 1;
            return $this->load->view('job/job-list', $data);
        }else{
			redirect(base_url());
		}        
    }

    public function view_job_details(){
        $id = $_POST['id'];
        $data = array();
        $data['page']         = $_POST['page'];
        $data['designation']  = $this->jm->get_designation_of_job();
        $data['skill']        = $this->jm->get_skill_of_job();
        $data['location']     = $this->jm->get_location_of_job();
        $data['organization'] = $this->jm->get_organization_of_job();
        $data['industry']     = $this->jm->get_industry_of_job();
        $data['degree']       = $this->jm->get_degree_of_job();
        $data['groups']       = $this->jm->get_candidates_groups_of_job();    
        $data['result']       = $this->jm->get_job_by_id($id); 
        $data['mapSkill']     = $this->jm->get_job_skill_by_id($id);
        return $this->load->view('job/view-job-details', $data);    
    }

    public function add_edit_job(){
        $id = !empty($_POST['id']) ? $_POST['id'] : null;
        $data = array();
        $data['page']         = $_POST['page'];
        $data['designation']  = $this->jm->get_designation_of_job();
        $data['skill']        = $this->jm->get_skill_of_job();
        $data['location']     = $this->jm->get_location_of_job();
        $data['organization'] = $this->jm->get_organization_of_job();
        $data['industry']     = $this->jm->get_industry_of_job();
        $data['degree']       = $this->jm->get_degree_of_job();
        $data['groups']       = $this->jm->get_candidates_groups_of_job();
        if($id){
            $data['result'] = $this->jm->get_job_by_id($id); 
        }else{
            $object_job = array('title'=>'', 'desc'=>'', 'location_id'=>'', 'type'=>'', 'designation_id'=>'', 'org_id'=>'', 'indus_id'=>'', 'min_qualification_id'=>'', 'designation_id'=> '', 'candidate_group_id'=>'', 'salary'=>'', 'experience'=> '', 'external_link'=> '', 'start_time'=>'', 'end_time'=>'');
            $data['result'][0] = (object)$object_job;
        }
        return $this->load->view('job/add-edit-job', $data);
    }

    public function add_edit_job_operation(){
        $resp         = array('status'=>'', 'message' => '');
        $title        = $_POST['title'];
        $type         = !empty($_POST['type']) ? json_encode($_POST['type']) : null;
        $decription   = !empty($_POST['decription']) ? $_POST['decription'] : null;
        $designation  = !empty($_POST['designation']) ? (int)$_POST['designation'] : null;
        $skill        = $_POST['skill'];
        $location     = (int)$_POST['location'];
        $organization = (int)$_POST['organization'];
        $industry     = (int)$_POST['industry'];
        $year         = !empty($_POST['year']) ? "{$_POST['year']} years" : null;
        $month        = !empty($_POST['month']) ? "{$_POST['month']} months" : null;
        $degree       = !empty($_POST['degree']) ? (int)$_POST['degree'] : null;
        $min_salary   = !empty($_POST['min_salary']) ? $_POST['min_salary'] : null;
        $max_salary   = !empty($_POST['max_salary']) ? $_POST['max_salary'] : null;
        $group        = !empty($_POST['group']) ? (int)$_POST['group'] : null;
        $link         = !empty($_POST['link']) ? $_POST['link'] : null;
        $email        = !empty($_POST['email']) ? $_POST['email'] : null;
        $start        = !empty($_POST['start']) ? date('Y-m-d H:i:s', strtotime($_POST['start'])) : null;
        $end          = !empty($_POST['end'])? date('Y-m-d H:i:s', strtotime($_POST['end'])) : null;
        $skill_data   = array();

        $job_data     = array(
            //'user_id' => (int)$this->session->userdata('userData')['userId'],
            'title'=> $title, 
            'desc'=> $decription, 
            'location_id'=> $location, 
            'type'=> $type, 
            'designation_id'=> $designation, 
            'org_id'=> $organization, 
            'indus_id'=> $industry, 
            'min_qualification_id'=> $degree, 
            'designation_id'=> $designation, 
            'candidate_group_id'=> $group, 
            'salary'=> "{$min_salary} - {$max_salary}", 
            'experience'=> "{$year} {$month}", 
            'external_link'=> $link, 
            'report_email' => $email,
            'start_time'=> $start, 
            'end_time'=> $end,
            'response_flag' => 1,
            'create_date_time' => date("Y-m-d H:i:s")
        );

        

        $job_id = $this->jm->add_edit_job($job_data);
        
        if($job_id){
            for($i=0; $i < count($skill); $i++){
                $skill_data[] = array(
                    'jobs_id' => (int)$job_id,
                    'skills_id' => $skill[$i]
                );
            }
            if($this->jm->add_job_skills($skill_data)){
                $resp['status'] = 'success';
                $resp['message'] = 'A new job is added successfully';
            }else{
                $resp['status'] = 'danger';
                $resp['message'] = 'Job is not add';
            }
        } 

        echo json_encode($resp);
    }

    function single_job_status_change(){
        $resp     = array('status'=>'', 'message' => '');
        $id       = $_POST['id'];
        $status   = $_POST['status'];
        $data = array(
            'response_flag' => ((int)$status === 1)? 2 : 1
        );
        if($this->jm->single_job_status_change($id, $data)){
            $resp['status'] = 'success';
            $resp['message'] = 'Job status change successfully';
        }else{
            $resp['status'] = 'danger';
            $resp['message'] = 'Job status not change';
        }
        echo json_encode($resp);
    }

    function multiple_job_status_change(){
        $resp     = array('status'=>'', 'message' => '');
        $id       = $_POST['id'];
        $status   = $_POST['status'];
        for($i=0; $i<count($id); $i++){
            $data[] = array(
                'id' => (int)$id[$i],
                'response_flag' => ((int)$status[$i] === 1)? 2 : 1,
            );
        }
        if($this->jm->multiple_job_status_change($data)){
            $resp['status'] = 'success';
            $resp['message'] = 'Jobs status change successfully';
        }else{
            $resp['status'] = 'danger';
            $resp['message'] = 'Jobs status not change';
        }
        echo json_encode($resp);
    }

    function single_job_delete(){
        $resp     = array('status'=>'', 'message' => '');
        $id       = $_POST['id'];
        $data = array(
            'response_flag' => 0
        );
        if($this->jm->single_job_status_change($id, $data)){
            $resp['status'] = 'success';
            $resp['message'] = 'Job delete successfully';
        }else{
            $resp['status'] = 'danger';
            $resp['message'] = 'Job is not deleted';
        }
        echo json_encode($resp);
    }

    function multiple_job_delete(){
        $resp     = array('status'=>'', 'message' => '');
        $id       = $_POST['id'];
        for($i=0; $i<count($id); $i++){
            $data[] = array(
                'id' => (int)$id[$i],
                'response_flag' => 0
            );
        }
        if($this->jm->multiple_job_status_change($data)){
            $resp['status'] = 'success';
            $resp['message'] = 'Jobs delete successfully';
        }else{
            $resp['status'] = 'danger';
            $resp['message'] = 'Jobs are not deleted';
        }
        echo json_encode($resp);
    }
}