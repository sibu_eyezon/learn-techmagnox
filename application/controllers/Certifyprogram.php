<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once "vendor/autoload.php";
require APPPATH . '/libraries/BaseController.php';

class Certifyprogram extends BaseController {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		date_default_timezone_set('Asia/Kolkata');
		ini_set('memory_limit', '-1');
	}
	
	public function MultipleMailSystem_Two($users, $subject, $message)
	{
		$email = "admin@billionskills.com";
        $password = "c6spQD82CVfch7aT";

		$mail = new PHPMailer(true);
		//Enable SMTP debugging.
		$mail->SMTPDebug = 0;                               
		//Set PHPMailer to use SMTP.
		$mail->isSMTP();            
		//Set SMTP host name  
		$mail->CharSet = 'utf-8';// set charset to utf8
		//$mail->Encoding = 'base64';
		$mail->SMTPAuth = true;// Enable SMTP authentication
		$mail->SMTPSecure = 'ssl';// Enable TLS encryption, `ssl` also accepted

		$mail->Host = 'lotus.arvixe.com';// Specify main and backup SMTP servers
		$mail->Port = 465;// TCP port to connect to
		$mail->SMTPOptions = array(
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
			)
		);		                     
		//Provide username and password     
		$mail->Username = $email;                 
		$mail->Password = $password;                                                                         

		$mail->From = $email;
		$mail->FromName = 'Magnox+ Plus';

		$mail->isHTML(true);

		$mail->Subject = $subject;
		$mail->Body = $message;
		//$mail->AltBody = "This is the plain text version of the email content";
		foreach ($users as $user) {
		  $mail->addBCC($user['email'], $user['name']);

			/* $mail->Body = "<h2>Hello, ".$user['name']."</h2> ".$message."</p>";
		  //$mail->AltBody = "Hello, {$user['name']}! \n How are you?";

		  try {
			  $mail->send();
			  //echo "Message sent to: ({$user['email']}) {$mail->ErrorInfo}\n";
		  } catch (Exception $e) {
			  //echo "Mailer Error ({$user['email']}) {$mail->ErrorInfo}\n";
		  }

		  $mail->clearAddresses();*/
		}
		$mail->send();
		$mail->clearAddresses();
		return true;//($mail->send())? 1 : $mail->ErrorInfo;
	}
	
	public function generateSectionCode($title){
		$ret = '';
		foreach (explode(' ', $title) as $word){
			$ret .= strtoupper($word[0]);
		}
		return $ret;
	}
	
	public function subModule_delete($id, $type)
	{
		$data['archive'] = true;
		$where = 'sl='.$id;
		echo $this->Exam_model->updateData('pro_'.$type.'s', $data, $where);
	}
	public function subModule_notify($id, $type, $prog)
	{
		$type = $_GET['ctype'];
		$data['notify'] = true;
		$where = 'sl='.$_GET['id'];
		$users = $this->Member->getStudentNameEmailByProgId($_GET['prog_id']);
		$pc = $this->Member->getProgramCourseNameByProgId($_GET['prog_id'], $_GET['course_sl']);
		$subject = 'New Lecture added';
		$message = 'New Lecture has been add to your course: "'.$pc[0]->course_title.'" of program: "'.$pc[0]->program_title.'", by '.$_SESSION['userData']['name'].'.';
		if(!empty($users)){
			if($this->MultipleMailSystem_Two($users, $subject, $message) && $this->Exam_model->updateData('pro_'.$type, $data, $where)){
				echo true;
			}else{
				echo false;
			}
		}else{
			echo false;
		}
		
	}
	
	public function submit_preview($prog_id=0)
	{
		$data['status'] = 'pending';
		$where = 'id='.$prog_id;
		echo $this->Exam_model->updateData('program', $data, $where);
	}
	/*==========================MODULE=======================*/
	public function module_cu($prog_id=0, $progtype="", $m_id=0)
	{
		$data['prog_id'] = $prog_id;
		$data['progtype'] = $progtype;
		$data['m_id'] = $m_id;
		$this->load->view('c_d_course/module_cu', $data);
	}
	public function cuModules($progtype="")
	{
		$userid = $_SESSION['userData']['userId'];
		$id = trim($_POST['cid']);
		$pid = trim($_POST['prog_id']);
		
		$data['title'] = trim($_POST['title']);
		$data['c_code'] = $this->generateSectionCode($data['title']).date('ydH');
		$data['start_date'] = date('Y-m-d H:i:s', strtotime($_POST['sdate']));
		$data['end_date'] = date('Y-m-d H:i:s', strtotime($_POST['edate']));
		$data['type'] = trim($_POST['crtype']);
		$data['overview'] = trim($_POST['lec_details']);
		$data['prog_id'] = $pid;
		if($id==0){
			$data['post_date'] = date('Y-m-d H:i:s');
			$cid = $this->Course_model->insertProCourse($data);
			if($cid){
				if($ori!=''){
					move_uploaded_file($tmp,'./uploads/courses/'.$files);
				}
				$data1['course_id'] = $cid;
				$data1['program_id'] = $pid;
				$data1['user_id'] = $userid;
				$data1['sem_id'] = 0;
				$data1['add_date'] = date('Y-m-d H:i:s');
				$this->Course_model->insertProMapCource($data1);
				$this->session->set_flashdata('error', 'Module has been added successfully.');
				//$resp = array('status'=>'success', 'msg'=>'Course added successfully.');
			}else{
				$this->session->set_flashdata('error', 'Module insert error. Please try again');
				//$resp = array('status'=>'danger', 'msg'=>'Course added error. Something went wrong.');
			}
		}else{
			if($this->Course_model->updateProCourse($data, $id)){
				if($ori!=''){
					move_uploaded_file($tmp,'./uploads/courses/'.$files);
				}
				$chkpmc = $this->Course_model->checkAvailPMC($pid, $id);
				$data1['course_id'] = $id;
				$data1['program_id'] = $pid;
				$data1['user_id'] = $userid;
				$data1['sem_id'] = 0;
				if($chkpmc==0){
					$data1['add_date'] = date('Y-m-d H:i:s');
					$this->Course_model->insertProMapCource($data1);
				}else{
					$this->Course_model->updateProMapCource($data1, $id, $pid);
				}
				$this->session->set_flashdata('error', 'Module has been updated successfully.');
				//$resp = array('status'=>'success', 'msg'=>'Course updated successfully.');
			}else{
				$this->session->set_flashdata('error', 'Module update error. Please try again');
				//$resp = array('status'=>'danger', 'msg'=>'Course update error. Something went wrong.');
			}
		}
		redirect(base_url().'Teacher/addProgram/'.$progtype.'/?id='.base64_encode($data['prog_id']));
	}
	public function module_delete($params)
	{
		$where = 'id='.$params;
		echo $this->Exam_model->deleteData('pro_course', $where);
	}
	/*=========================LECTURE=======================*/
	public function lecture_cu($prog_id=0, $progtype="", $lec_id=0)
	{
		$data['sections'] = $this->AutoProgramModel->get_section($prog_id);
		$data['prog_id'] = $prog_id;
		$data['progtype'] = $progtype;
		$data['lec_id'] = $lec_id;
		$this->load->view('c_d_course/lecture_cu', $data);
	}
	public function cuLectures($progtype="")
	{
		$tmp = $_FILES['lfiles']['tmp_name'];
		$ori = $_FILES['lfiles']['name'];
		$lfile = uniqid().$ori;
		
		$data['course_sl'] = trim($_POST['section_id']);
		$prog = trim($_POST['prog_id']);
		$lecid = trim($_POST['lesson_id']);
		$data['user_id'] = $_SESSION['userData']['userId'];
		$data['title'] = trim($_POST['title']);
		$data['file_type'] = trim($_POST['ltype']);
		if($data['file_type']=='fl'){
			if($ori!=''){
				$data['file_name'] = $lfile;
			}
		}else{
			$data['link'] = trim($_POST['llink']);
		}
		$data['notify'] = true;
		$data['archive'] = false;
		$data['lec_date'] = date('Y-m-d H:i:s');
		if($lecid==0){
			$data['add_date'] = date('Y-m-d H:i:s');
			if($this->Course_model->insertClecture($data)){
				if($ori!='' && $data['file_type']=='fl'){
					move_uploaded_file($tmp,'./uploads/courselra/'.$lfile);
				}
				$this->session->set_flashdata('error', 'Lecture has been added successfully.');
			}else{
				$this->session->set_flashdata('error', 'Lecture insert error. Please try again');
			}
		}else{
			$data['modified_date'] = date('Y-m-d H:i:s');
			if($this->Course_model->updateClecture($data, $lecid)){
				if($ori!='' && $data['file_type']=='fl'){
					move_uploaded_file($tmp,'./uploads/courselra/'.$lfile);
				}
				$this->session->set_flashdata('error', 'Lecture has been updated successfully.');
			}else{
				$this->session->set_flashdata('error', 'Lecture update error. Please try again');
			}
		}
		redirect(base_url().'Teacher/addProgram/'.$progtype.'/?id='.base64_encode($prog));
	}
	/*=========================RESOURCE=======================*/
	public function resource_cu($prog_id=0, $progtype="", $res_id=0)
	{
		$data['sections'] = $this->AutoProgramModel->get_section($prog_id);
		$data['prog_id'] = $prog_id;
		$data['progtype'] = $progtype;
		$data['res_id'] = $res_id;
		$this->load->view('c_d_course/resource_cu', $data);
	}
	public function cuResources($progtype="")
	{
		$tmp = $_FILES['lfiles']['tmp_name'];
		$ori = $_FILES['lfiles']['name'];
		$lfile = uniqid().$ori;
		
		$data['course_sl'] = trim($_POST['section_id']);
		$prog = trim($_POST['prog_id']);
		$res_id = trim($_POST['res_id']);
		$data['user_id'] = $_SESSION['userData']['userId'];
		$data['title'] = trim($_POST['title']);
		$data['notify'] = true;
		$data['archive'] = false;
		
		$data2['type'] = trim($_POST['ltype']);
		if($data2['type']=='fl'){
			if($ori!=""){
				$data2['file_type'] = pathinfo($ori, PATHINFO_EXTENSION);
				$data2['linkfile'] = $lfile;
				move_uploaded_file($tmp,'./uploads/cresources/'.$lfile);
			}
		}else if($data2['type']=='lk'){
			$data2['linkfile'] = trim($_POST['lk_link']);
		}else if($data2['type']=='yt'){
			$data2['linkfile'] = trim($_POST['yt_link']);
		}
		$data2['add_date'] = date('Y-m-d H:i:s');
		
		if($res_id==0){
			$data['add_date'] = date('Y-m-d H:i:s');
			$res_id = $this->Exam_model->insertDataRetId('pro_resources', $data);
			
			if($res_id!=0){
				$data2['res_sl'] = $res_id;
				//print_r($data2); exit;
				$this->Course_model->insertCRFiles('pro_res_fileslinks', $data2);
				$this->session->set_flashdata('error', 'Resource has been added successfully.');
			}else{
				$this->session->set_flashdata('error', 'Resource insert error. Please try again');
			}
		}else{
			$data['modified_date'] = date('Y-m-d H:i:s');
			
			if($this->Course_model->updateCresource($data, $res_id)){
				$where='res_sl='.$res_id;
				$this->Exam_model->updateData('pro_res_fileslinks', $data2, $where);
				$this->session->set_flashdata('error', 'Resource has been updated successfully.');
			}else{
				$this->session->set_flashdata('error', 'Resource update error. Please try again');
			}
		}
		redirect(base_url().'Teacher/addProgram/'.$progtype.'/?id='.base64_encode($prog));
	}
	/*=======================ASSIGNMENT======================*/
	public function assignment_cu($prog_id=0, $progtype="", $asgn_id=0)
	{
		$data['sections'] = $this->AutoProgramModel->get_section($prog_id);
		$data['prog_id'] = $prog_id;
		$data['progtype'] = $progtype;
		$data['asgn_id'] = $asgn_id;
		$this->load->view('c_d_course/assignment_cu', $data);
	}
	public function cuAssignments($progtype="")
	{
		$data['course_sl'] = trim($_POST['section_id']);
		$prog = trim($_POST['prog_id']);
		$asgn_id = trim($_POST['asgn_id']);
		$data['user_id'] = $_SESSION['userData']['userId'];
		$data['title'] = trim($_POST['title']);
		$data['notify'] = true;
		$data['archive'] = false;
		$data['marks'] = trim($_POST['amarks']);
		$data['tdate'] = date('Y-m-d', strtotime($_POST['sdate']));
		$data['deadline'] = date('Y-m-d', strtotime($_POST['ddate']));
		
		$data1['course_sl'] = trim($_POST['section_id']);
		$data1['type'] = 'Assignment';
		$data1['subject'] = trim($_POST['title']);
		$data1['user_id'] = $_SESSION['userData']['userId'];
		$data1['weightage'] = '40';
		$data1['full_marks'] = trim($_POST['amarks']);
		
		if($asgn_id==0){
			$data['add_date'] = date('Y-m-d H:i:s');
			$data1['tdatetime'] = date('Y-m-d H:i:s');
			$asid = $this->Course_model->insertCAssignment($data);
			if($asid!=NULL){
				$data1['serial'] = $asid;
				$this->Course_model->insertCWeightage($data1);
				$this->session->set_flashdata('error', 'Assignment has been added successfully.');
			}else{
				$this->session->set_flashdata('error', 'Assignment insert error. Please try again.');
			}
		}else{
			$data['modified_date'] = date('Y-m-d H:i:s');
			$data1['modified_datetime'] = date('Y-m-d H:i:s');
			if($this->Course_model->updateCAssignment($data, $asgn_id)){
				$this->Course_model->updateCWeightage($data1, $asgn_id);
				$this->session->set_flashdata('error', 'Assignment has been updated successfully.');
			}else{
				$this->session->set_flashdata('error', 'Assignment update error. Please try again.');
			}
		}
		redirect(base_url().'Teacher/addProgram/'.$progtype.'/?id='.base64_encode($prog));
	}
	/*=========================SECTION=======================*/
	public function section_cu($prog_id=0, $progtype="", $m_id=0)
	{
		$data['prog_id'] = $prog_id;
		$data['progtype'] = $progtype;
		$data['m_id'] = $m_id;
		$this->load->view('certify_prof_courses/section_cu', $data);
	}
	public function cuSections()
	{
		$userid = $_SESSION['userData']['userId'];
		$id = trim($_POST['cid']);
		$pid = trim($_POST['prog_id']);
		
		$data['title'] = trim($_POST['title']);
		$data['c_code'] = $this->generateSectionCode($data['title']).date('ydH');
		$data['start_date'] = date('Y-m-d H:i:s', strtotime($_POST['sdate']));
		$data['end_date'] = date('Y-m-d H:i:s', strtotime($_POST['edate']));
		$data['type'] = trim($_POST['crtype']);
		$data['overview'] = trim($_POST['lec_details']);
		$data['prog_id'] = $pid;
		if($id==0){
			$data['post_date'] = date('Y-m-d H:i:s');
			$cid = $this->Course_model->insertProCourse($data);
			if($cid){
				if($ori!=''){
					move_uploaded_file($tmp,'./uploads/courses/'.$files);
				}
				$data1['course_id'] = $cid;
				$data1['program_id'] = $pid;
				$data1['user_id'] = $userid;
				$data1['sem_id'] = 0;
				$data1['add_date'] = date('Y-m-d H:i:s');
				$this->Course_model->insertProMapCource($data1);
				echo true;
			}else{
				echo false;
			}
		}else{
			if($this->Course_model->updateProCourse($data, $id)){
				if($ori!=''){
					move_uploaded_file($tmp,'./uploads/courses/'.$files);
				}
				$chkpmc = $this->Course_model->checkAvailPMC($pid, $id);
				$data1['course_id'] = $id;
				$data1['program_id'] = $pid;
				$data1['user_id'] = $userid;
				$data1['sem_id'] = 0;
				if($chkpmc==0){
					$data1['add_date'] = date('Y-m-d H:i:s');
					$this->Course_model->insertProMapCource($data1);
				}else{
					$this->Course_model->updateProMapCource($data1, $id, $pid);
				}
				echo true;
			}else{
				echo false;
			}
		}
	}
	/*=========================LESSON=======================*/
	public function lecture_pcu($prog_id=0, $progtype="", $lec_id=0)
	{
		$data['sections'] = $this->AutoProgramModel->get_section($prog_id);
		$data['prog_id'] = $prog_id;
		$data['progtype'] = $progtype;
		$data['lec_id'] = $lec_id;
		$this->load->view('certify_prof_courses/lecture_cu', $data);
	}
	public function pcuLectures()
	{
		/*$tmp = $_FILES['lfiles']['tmp_name'];
		$ori = $_FILES['lfiles']['name'];
		$lfile = uniqid().$ori;*/
		
		$data['course_sl'] = trim($_POST['section_id']);
		$prog = trim($_POST['prog_id']);
		$lecid = trim($_POST['lesson_id']);
		$data['user_id'] = $_SESSION['userData']['userId'];
		$data['title'] = trim($_POST['title']);
		$data['file_type'] = trim($_POST['ltype']);
		if($data['file_type']=='fl'){
			$data['file_name'] = trim($_POST['lec_file']);
		}else{
			$data['link'] = trim($_POST['llink']);
		}
		$data['notify'] = false;
		$data['archive'] = false;
		$data['lec_date'] = date('Y-m-d H:i:s');
		if($lecid==0){
			$data['add_date'] = date('Y-m-d H:i:s');
			if($this->Course_model->insertClecture($data)){
				echo true;
			}else{
				echo false;
			}
		}else{
			$data['modified_date'] = date('Y-m-d H:i:s');
			if($this->Course_model->updateClecture($data, $lecid)){
				echo true;
			}else{
				echo false;
			}
		}
	}
	public function uploadLecFile()
	{
		$allowTypes = array('pdf', 'doc', 'docs', 'xls', 'xlsx', 'mp4', 'ppt', 'pptx');
		$resp = array('status'=>false, 'file_name'=>"");
		$tmp = $_FILES['lfiles']['tmp_name'];
		$ori = $_FILES['lfiles']['name'];
		$lfile = uniqid().$ori;
		$fileType = pathinfo($ori, PATHINFO_EXTENSION);
		if(in_array($fileType, $allowTypes)){ 
			// Upload file to the server 
			if(move_uploaded_file($tmp,'./uploads/courselra/'.$lfile)){ 
				$resp = array('status'=>true, 'file_name'=>$lfile);
			} 
		}
		echo json_encode($resp);exit;
	}
	/*=========================RESOURCE=======================*/
	public function resource_pcu($prog_id=0, $progtype="", $res_id=0)
	{
		$data['sections'] = $this->AutoProgramModel->get_section($prog_id);
		$data['prog_id'] = $prog_id;
		$data['progtype'] = $progtype;
		$data['res_id'] = $res_id;
		$this->load->view('certify_prof_courses/resource_cu', $data);
	}
	public function pcuResources()
	{
		$tmp = $_FILES['lfiles']['tmp_name'];
		$ori = $_FILES['lfiles']['name'];
		$lfile = uniqid().$ori;
		
		$data['course_sl'] = trim($_POST['section_id']);
		$prog = trim($_POST['prog_id']);
		$res_id = trim($_POST['res_id']);
		$data['user_id'] = $_SESSION['userData']['userId'];
		$data['title'] = trim($_POST['title']);
		$data['notify'] = false;
		$data['archive'] = false;
		
		$data2['type'] = trim($_POST['ltype']);
		if($data2['type']=='fl'){
			$data2['file_type'] = pathinfo($_POST['res_file'], PATHINFO_EXTENSION);
			$data2['linkfile'] = $_POST['res_file'];
		}else if($data2['type']=='lk'){
			$data2['linkfile'] = trim($_POST['lk_link']);
		}else if($data2['type']=='yt'){
			$data2['linkfile'] = trim($_POST['yt_link']);
		}
		$data2['add_date'] = date('Y-m-d H:i:s');
		
		if($res_id==0){
			$data['add_date'] = date('Y-m-d H:i:s');
			$res_id = $this->Exam_model->insertDataRetId('pro_resources', $data);
			
			if($res_id!=0){
				$data2['res_sl'] = $res_id;
				$this->Course_model->insertCRFiles('pro_res_fileslinks', $data2);
				echo true;
			}else{
				echo false;
			}
		}else{
			$data['modified_date'] = date('Y-m-d H:i:s');
			
			if($this->Course_model->updateCresource($data, $res_id)){
				$where='res_sl='.$res_id;
				$this->Exam_model->updateData('pro_res_fileslinks', $data2, $where);
				echo true;
			}else{
				echo false;
			}
		}
	}
	public function uploadResFile()
	{
		$allowTypes = array('pdf', 'doc', 'docs', 'xls', 'xlsx', 'mp4', 'ppt', 'pptx');
		$resp = array('status'=>false, 'file_name'=>"");
		$tmp = $_FILES['lfiles']['tmp_name'];
		$ori = $_FILES['lfiles']['name'];
		$lfile = uniqid().$ori;
		$fileType = pathinfo($ori, PATHINFO_EXTENSION);
		if(in_array($fileType, $allowTypes)){ 
			// Upload file to the server 
			if(move_uploaded_file($tmp,'./uploads/cresources/'.$lfile)){ 
				$resp = array('status'=>true, 'file_name'=>$lfile);
			} 
		}
		echo json_encode($resp);exit;
	}
	/*=======================ASSIGNMENT======================*/
	public function assignment_pcu($prog_id=0, $progtype="", $asgn_id=0)
	{
		$data['sections'] = $this->AutoProgramModel->get_section($prog_id);
		$data['prog_id'] = $prog_id;
		$data['progtype'] = $progtype;
		$data['asgn_id'] = $asgn_id;
		$this->load->view('certify_prof_courses/assignment_cu', $data);
	}
	public function pcuAssignments()
	{
		$data['course_sl'] = trim($_POST['section_id']);
		$prog = trim($_POST['prog_id']);
		$asgn_id = trim($_POST['asgn_id']);
		$data['user_id'] = $_SESSION['userData']['userId'];
		$data['title'] = trim($_POST['title']);
		$data['notify'] = false;
		$data['archive'] = false;
		$data['marks'] = trim($_POST['amarks']);
		$data['tdate'] = date('Y-m-d', strtotime($_POST['sdate']));
		$data['deadline'] = date('Y-m-d', strtotime($_POST['ddate']));
		
		$data1['course_sl'] = trim($_POST['section_id']);
		$data1['type'] = 'Assignment';
		$data1['subject'] = trim($_POST['title']);
		$data1['user_id'] = $_SESSION['userData']['userId'];
		$data1['weightage'] = '40';
		$data1['full_marks'] = trim($_POST['amarks']);
		
		if($asgn_id==0){
			$data['add_date'] = date('Y-m-d H:i:s');
			$data1['tdatetime'] = date('Y-m-d H:i:s');
			$asid = $this->Course_model->insertCAssignment($data);
			if($asid!=NULL){
				$data1['serial'] = $asid;
				$this->Course_model->insertCWeightage($data1);
				
				$rfile = trim($_POST['asgn_file']);
				if($rfile!=""){
					$data2['assgn_sl'] = $asid;
					$data2['file_type'] = 'fl';
					$data2['file_ext'] = pathinfo($rfile, PATHINFO_EXTENSION);
					$data2['file_name'] = $rfile;
					$data2['add_date'] = date('Y-m-d H:i:s');
					$this->Course_model->insertCRFiles('pro_assgn_files', $data2);
				}
				/*
				if($_FILES['lfiles']['name']!="")
				{
					$tmp = $_FILES['lfiles']['tmp_name'];
					$ori = $_FILES['lfiles']['name'];
					$rfile = uniqid().$ori;
					
					$data2['assgn_sl'] = $asid;
					$data2['file_type'] = 'fl';
					$data2['file_ext'] = pathinfo($ori, PATHINFO_EXTENSION);
					$data2['file_name'] = $rfile;
					$data2['add_date'] = date('Y-m-d H:i:s');
					
					if($this->Course_model->insertCRFiles('pro_assgn_files', $data2))
					{
						move_uploaded_file($tmp,'./uploads/cassignments/'.$rfile);
					}
				}*/
				echo true;
			}else{
				echo false;
			}
		}else{
			$data['modified_date'] = date('Y-m-d H:i:s');
			$data1['modified_datetime'] = date('Y-m-d H:i:s');
			if($this->Course_model->updateCAssignment($data, $asgn_id)){
				$this->Course_model->updateCWeightage($data1, $asgn_id);
				
				$rfile = trim($_POST['asgn_file']);
				if($rfile!=""){
					$where = 'assgn_sl='.$asgn_id;
					
					$data2['assgn_sl'] = $asgn_id;
					$data2['file_type'] = 'fl';
					$data2['file_ext'] = pathinfo($rfile, PATHINFO_EXTENSION);
					$data2['file_name'] = $rfile;
					$data2['add_date'] = date('Y-m-d H:i:s');
					$this->Exam_model->updateData('pro_assgn_files', $data2, $where);
				}
				/*if($_FILES['lfiles']['name']!="")
				{
					$where = 'assgn_sl='.$asgn_id;
					$this->Exam_model->deleteData('pro_assgn_files', $where);
					
					$tmp = $_FILES['lfiles']['tmp_name'];
					$ori = $_FILES['lfiles']['name'];
					$rfile = uniqid().$ori;
					
					$data2['assgn_sl'] = $asgn_id;
					$data2['file_type'] = 'fl';
					$data2['file_ext'] = pathinfo($ori, PATHINFO_EXTENSION);
					$data2['file_name'] = $rfile;
					$data2['add_date'] = date('Y-m-d H:i:s');
					
					if($this->Course_model->insertCRFiles('pro_assgn_files', $data2))
					{
						move_uploaded_file($tmp,'./uploads/cassignments/'.$rfile);
					}
				}*/
				echo true;
			}else{
				echo false;
			}
		}
	}
	public function uploadAsgnFile()
	{
		$allowTypes = array('pdf', 'doc', 'docs', 'xls', 'xlsx', 'mp4', 'ppt', 'pptx');
		$resp = array('status'=>false, 'file_name'=>"");
		$tmp = $_FILES['lfiles']['tmp_name'];
		$ori = $_FILES['lfiles']['name'];
		$lfile = uniqid().$ori;
		$fileType = pathinfo($ori, PATHINFO_EXTENSION);
		if(in_array($fileType, $allowTypes)){ 
			// Upload file to the server 
			if(move_uploaded_file($tmp,'./uploads/cassignments/'.$lfile)){ 
				$resp = array('status'=>true, 'file_name'=>$lfile);
			} 
		}
		echo json_encode($resp);exit;
	}
	/*=======================================================*/
	public function benefits_cu($prog_id=0, $progtype="", $pb_id=0)
	{
		$data['prog_id'] = $prog_id;
		$data['progtype'] = $progtype;
		$data['pb_id'] = $pb_id;
		$this->load->view('certify_prof_courses/benefits_cu', $data);
	}
	public function pcuBenefits()
	{
		$res = array('status'=>false, 'type'=>"add", 'msg'=>"");
		$pb_id = $_POST['pb_id'];
		$prog_id = $_POST['prog_id'];
		$progtype = $_POST['progtype'];
		
		$data['program_id'] = $prog_id;
		$data['txt_benefit'] = trim($_POST['title']);
		$data['txt_benefit_dtls'] = trim($_POST['details']);
		
		if($pb_id==0){
			$data['dat_sys_date'] = date('Y-m-d H:i:s');
			$pb_id = $this->Exam_model->insertDataRetId('program_benefits', $data);
			if($pb_id!=0){
				$data['pb_id'] = $pb_id;
				$data['progtype'] = $progtype;
				$res['status'] = true;
				$res['msg'] = $data;
			}
		}else{
			$where = 'id='.$pb_id;
			if($this->Exam_model->updateData('program_benefits', $data, $where)){
				$data['pb_id'] = $pb_id;
				$data['progtype'] = $progtype;
				$res['type'] = 'edit';
				$res['status'] = true;
				$res['msg'] = $data;
			}
		}
		echo json_encode($res);
	}
	public function benefit_delete($params)
	{
		$where = 'id='.$params;
		echo $this->Exam_model->deleteData('program_benefits', $where);
	}
	/*=======================================================*/
}