<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once "vendor/autoload.php";
require APPPATH . '/libraries/BaseController.php';

class Student extends BaseController {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper('common_helper');
		$this->load->model('Job_model', 'jm');
		date_default_timezone_set('Asia/Kolkata');
		ini_set('memory_limit', '-1');
	}
	
	public function MailSystem($to, $cc, $subject, $message)
	{
		$email = "admin@billionskills.com";
        $password = "c6spQD82CVfch7aT";

		$mail = new PHPMailer(true);
		//Enable SMTP debugging.
		$mail->SMTPDebug = 0;                               
		//Set PHPMailer to use SMTP.
		$mail->isSMTP();            
		//Set SMTP host name  
		$mail->CharSet = 'utf-8';// set charset to utf8
		//$mail->Encoding = 'base64';
		$mail->SMTPAuth = true;// Enable SMTP authentication
		$mail->SMTPSecure = 'ssl';// Enable TLS encryption, `ssl` also accepted

		$mail->Host = 'lotus.arvixe.com';// Specify main and backup SMTP servers
		$mail->Port = 465;// TCP port to connect to
		$mail->SMTPOptions = array(
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
			)
		);		                     
		//Provide username and password     
		$mail->Username = $email;                 
		$mail->Password = $password;                                                                         

		$mail->From = $email;
		$mail->FromName = 'Magnox+ Plus';

		$mail->addAddress($to);

		$mail->isHTML(true);

		$mail->Subject = $subject;
		$mail->Body = $message;
		//$mail->AltBody = "This is the plain text version of the email content";

		return ($mail->send())? 1 : $mail->ErrorInfo;
	}
	/******************************************************************************/
	public function index()
	{
		if($this->isLoggedIn()){
			$data['userid'] = $_SESSION['userData']['userId'];
			$this->global['pageTitle'] = 'Dashboard | Magnox Learning+ - Student';
			$data['programs'] = $this->Member->getAllStudPrograms($data['userid']);
			//$data['progCleared'] = $this->Member->getStudCompleteProgram($data['userid']);
			$data['studInfo'] = $this->Member->getStudMoreInfo($data['userid']);
			$data['lastLoggedIn'] = $this->Member->getUserLastLogin();
			$i=1;
			foreach($data['programs'] as $row)
			{
				$pid = $row->id;
				$ptype = $row->type;
				$pcertify = $row->bolcertify;
				$data['spc_'.$i] = $this->Member->getStudProgCons($data['userid'], $pid);
				$data['cpsub'.$i] = $this->Member->getTotalProgCourses($pid);
				$data['pcourses'.$i] = $this->Course_model->getProgramCourses($pid);
				$i++;
			}
			$this->loadUserViews("index", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	
	public function findUserApply()
	{
		$resp = array('status'=>false, 'prog_id'=>'', 'prog_name'=>'', 'apply_status'=>false, 'apply_type'=>'userProgram');
		//$stdApply = $this->Member->getUserApplication($_SESSION['userData']['email']);
		if(isset($_COOKIE['userProgram'])){
			$userProg = json_decode(get_cookie('userProgram'));
			$userid = $userProg->userId;
			$progid = $userProg->progId;
			$progname = $userProg->progName;
			if(!$userProg->status){
				$chk_adm = $this->Member->checkReduntAdmission($progid, $userid);
				if($userid==$_SESSION['userData']['userId']){
					if($chk_adm==0){
						$resp = array('status'=>true, 'prog_id'=>$progid, 'prog_name'=>$progname, 'apply_status'=>false);
					}else{
						$resp = array('status'=>true, 'prog_id'=>$progid, 'prog_name'=>$progname, 'apply_status'=>true);
					}
				}
			}else{
				$resp = array('status'=>true, 'prog_id'=>$progid, 'prog_name'=>$progname, 'apply_status'=>true, 'apply_type'=>'userProgram');
			}
			if (isset($_COOKIE['userProgram'])) {
				unset($_COOKIE['userProgram']);
				setcookie('userProgram', '', time() - 3600, '/'); // empty value and old timestamp
			}
		}else if(isset($_COOKIE['userApplyProgram'])){
			$userApplyProgram = json_decode(get_cookie('userApplyProgram'));
			$userid = $userApplyProgram->userId;
			$progid = $userApplyProgram->progId;
			$progname = $userApplyProgram->progName;
			$resp = array('status'=>true, 'prog_id'=>$progid, 'prog_name'=>$progname, 'apply_status'=>true, 'apply_type'=>'userApplyProgram');
			if (isset($_COOKIE['userApplyProgram'])) {
				unset($_COOKIE['userApplyProgram']);
				setcookie('userApplyProgram', '', time() - 3600, '/'); // empty value and old timestamp
			}
		}else{
			$resp = array('status'=>false, 'prog_id'=>'', 'prog_name'=>'', 'apply_status'=>false, 'apply_type'=>'userProgram');
		}
		
		echo json_encode($resp);
	}
	public function updateApplications()
	{
		$str=[];
		$resp = array('title'=>'Failed', 'status'=>'error', 'msg'=>'Something went wrong.');
		if(isset($_POST['stdApply'])){
			$chkUser = $_POST['stdApply'];
			$stdApply = $this->Member->getUserApplication($_SESSION['userData']['email']); 
			$ccu = count($chkUser);
			$csa = count($stdApply);
			if(($csa-$ccu)==0){
				$data['status'] = 'accepted';
				$where = "user_email='".$_SESSION['userData']['email']."' AND status='pending'";
				$this->LoginModel->updateData('pro_user_invite', $data, $where);
				for($i=0; $i<$ccu; $i++){
					$str[$i] = explode("_",$chkUser[$i]);
					$data[$i]['program_id'] = $str[$i][1];
					$data[$i]['user_id'] = $_SESSION['userData']['userId'];
					$data[$i]['role'] = 'Student';
					$data[$i]['add_date'] = date('Y-m-d H:i:s');
					$data[$i]['status'] = 'pending';
					$this->LoginModel->insertData('pro_users_role', $data[$i]);
				}
				$resp = array('title'=>'All programs', 'status'=>'success', 'msg'=>'have been applied. Check on "View request for approval."');
			}else{
				for($i=0; $i<$ccu; $i++){
					$str[$i] = explode("_",$chkUser[$i]);
					$data[$i]['program_id'] = $str[$i][1];
					$data[$i]['user_id'] = $_SESSION['userData']['userId'];
					$data[$i]['role'] = 'Student';
					$data[$i]['add_date'] = date('Y-m-d H:i:s');
					$data[$i]['status'] = 'pending';
					$this->LoginModel->insertData('pro_users_role', $data[$i]);
					
					$where1 = 'sl='.$str[$i][0];
					$data2[$i]['status'] = 'accepted';
					$this->LoginModel->updateData('pro_user_invite', $data2[$i], $where1);
				}
				$where = "user_email='".$_SESSION['userData']['email']."' AND status='pending'";
				$this->LoginModel->deleteData('pro_user_invite', $where);
				$resp = array('title'=>$ccu.' programs', 'status'=>'success', 'msg'=>'have been applied and '.($csa-$ccu).' were removed. Check on "View request for approval."');
			}
		}else{
			$resp = array('status'=>'success', 'msg'=>'application/s removed.');
		}
		echo json_encode($resp);
	}
	
	public function updateUserPassword()
	{
		$resp = array('status' => false);
		$user_id = $_SESSION['userData']['userId'];
		$password = md5(trim($_POST['pass']));
		$data2['password'] = $password;
		$where2 = 'user_id='.$user_id;
		
		if($this->Member->updateData('user_auth', $data2, $where2)){
			$resp['status'] = true;
		}
		echo json_encode($resp);
	}
	public function userProfile()
	{
		if($this->isLoggedIn()){
			$userid = $_SESSION['userData']['userId'];
			$this->global['pageTitle'] = 'Profile | Magnox Learning+ - Student';
			$data['udetails'] = $this->Member->getUserDetailsById($userid);
			$data['degree'] = $this->LoginModel->getAllDegrees();
			$data['skills'] = $this->LoginModel->getAllSkills();
			$data['uskills'] = $this->Member->getAllUserSkillsArray($userid);
			$data['uacademic'] = $this->Member->getAllUserAcademic($userid);
			$this->loadUserViews("profile", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function updateProfile()
	{
		$id = $_SESSION['userData']['userId'];
		$ori = $_FILES['avatar']['name'];
		$tmp = $_FILES['avatar']['tmp_name'];
		$thmb = uniqid().$ori;
		$ori1 = $_FILES['resume']['name'];
		$tmp1 = $_FILES['resume']['tmp_name'];
		$thmb1 = 'Magnox'.date('dmyhi').$ori1;
		
		$data_img = $_POST['crop_img']; 
		if($data_img!=""){
			list($type, $data_img) = explode(';', $data_img);
			list(, $data_img)      = explode(',', $data_img);	
		}
		
		$data['first_name'] = ucfirst(strtolower(trim($_POST['firstname'])));
		$data['last_name'] = ucfirst(strtolower(trim($_POST['lastname'])));
		if($ori!=''){
			$data['photo_sm'] = 'assets/img/users/'.$thmb;
			//$data['photo_lg'] = 'public/image/profile_pic/lg/'.$thmb;
		}
		$data['email'] = strtolower(trim($_POST['email']));
		$data['phone'] = trim($_POST['phone']);
		$data['modified_date_time'] = date('Y-m-d H:i:s');
		
		$this->Member->deleteUserSkills($id);
		$this->Member->deleteUserAcademic($id);
		
		$ecount = trim($_POST['educount']);
		$uskills = $_POST['skills'];
		$cskill = count($uskills);
		
		if($this->Member->updateUserAuth($data, $id)){
			if($ori!=""){
				if($data_img!=''){
					file_put_contents('./assets/img/users/'.$thmb, base64_decode($data_img));
					//file_put_contents('./public/image/profile_pic/lg/'.$thmb, base64_decode($data_img));
				}
			}
			if($ori1!=''){
				move_uploaded_file($tmp1,'./uploads/resume/'.$thmb1);
				$data['resume_heading'] = 'uploads/resume/'.$thmb1;
			}
			$sessionArray = $this->session->userdata('userData');
			$this->session->unset_userdata('userData');
			$sessionArray['userId'] = $id;
			$sessionArray['email'] = trim($data['email']);
			$sessionArray['name'] = trim($data['first_name'].' '.$data['last_name']);
			if($ori!=""){
				$sessionArray['photo'] = 'assets/img/users/'.$thmb;
			}
			$sessionArray['isLoggedIn'] = true;
			$this->session->set_userdata('userData',$sessionArray);
			
			$data['dateofbirth'] = date('Y-m-d',strtotime($_POST['dob']));
			$data['gender'] = $_POST['gender'];
			$data['alt_email'] = trim($_POST['alt_email']);
			$data['alt_phone'] = trim($_POST['alt_phone']);
			$data['website'] = trim($_POST['website']);
			$data['address'] = trim($_POST['address']);
			$data['facebook_link'] = trim($_POST['facebook']);
			$data['linkedin_link'] = trim($_POST['linkedin']);
			$data['google_link'] = trim($_POST['google']);
			$this->Member->updateUserDetails($data, $id);
			
			for($i=0; $i<=$ecount; $i++){
				$data2[$i]['user_id'] = $id;
				$data2[$i]['board'] = strtoupper(trim($_POST['board_'.$i]));
				$data2[$i]['organization'] = ucfirst(strtolower(trim($_POST['org_'.$i])));
				$data2[$i]['degree_id'] = trim($_POST['degree_'.$i]);
				$data2[$i]['aca_status'] = (isset($_POST['status_'.$i]))? 'Completed' : 'Present';
				if($data2[$i]['aca_status']=='Completed'){
					$data2[$i]['passout_year'] = trim($_POST['passout_'.$i]);
					$data2[$i]['marks_per'] = trim($_POST['marks_'.$i]);
				}
				$data2[$i]['create_date_time'] = date('Y-m-d H:i:s');
				
				if($data2[$i]['board']!=""){
					$this->LoginModel->insertUserAcademic($data2[$i]);
				}
			}
			if($cskill>0){
				for($j=0; $j<$cskill; $j++){
					$data3[$j]['user_id'] = $id;
					$data3[$j]['skill_id'] = $uskills[$j];
					$data3[$j]['rank'] = 0;
					$data3[$j]['create_date_time'] = date('Y-m-d H:i:s');
					if($data3[$j]['skill_id']!=""){
						$this->LoginModel->insertUserSkills($data3[$j]);
					}
				}
			}
			
			$this->session->set_flashdata('success', 'Profile details has been updated');
		}else{
			$this->session->set_flashdata('error', 'Profile details updation failed');
		}
		
		redirect(base_url().'Student/userProfile');
	}
	
	public function getUserValidData()
	{
		$userid = $_SESSION['userData']['userId'];
		$data['type'] = trim($_GET['dtype']);
		if($data['type']=='resume'){
			$data['resume'] = $this->Member->getUserDetailsById($userid);
		}else if($data['type']=='skills'){
			$data['skills'] = $this->Member->getAllUserSkillsArray($userid);
		}
		return $this->load->view('users/user_data', $data);
	}
	
	public function programAdmission()
	{
		if($this->isLoggedIn()){
			$userid = $_SESSION['userData']['userId'];
			$data['prog_id'] = base64_decode($_GET['id']);
			$checkAdm = $this->Member->checkReduntAdmission($data['prog_id'], $userid);
			if($checkAdm==0){
				$this->global['pageTitle'] = 'Program Admission | Magnox Learning+ - Student';
				$data['ud'] = $this->Member->getUserDetailsById($userid);
				$data['prog'] = $this->LoginModel->getProgramById($data['prog_id']);
				$data['degree'] = $this->LoginModel->getAllDegrees();
				$data['uacademic'] = $this->Member->getAllUserAcademic($userid);
				$this->loadUserViews("program-admission", $this->global, $data, NULL);
			}else{
				$this->session->set_flashdata('error', 'You have already applied for admission on this program.');
				redirect(base_url().'Student/allPrograms');
			}
		}else{
			redirect(base_url());
		}
	}
	public function userAdmission()
	{
		$this->load->helper('string');
		$roll = random_string('numeric', 3);
		
		$userid = $_SESSION['userData']['userId'];
		$ud = $this->Member->getUserDetailsById($userid);
		$ecount = trim($_POST['educount']);
		$pid = trim($_POST['pid']);
		$pdetails = $this->LoginModel->getProgramInfoById($pid);
		$apply_type = (int)$pdetails[0]->apply_type;
		$enroll = trim($pdetails[0]->code).$roll;
		$coupon_code = trim($_POST['valid_ccode']);
		$feetype = trim($pdetails[0]->feetype);
		if($feetype=='Paid'){
			$pamt = intval(trim($pdetails[0]->total_fee));
			if($coupon_code!='0'){
				$code = explode('_',$coupon_code);
				if($code[1]=='A'){
					$pamt = $pamt-intval($code[0]);
				}else if($code[1]=='P'){
					$pamt = $pamt*floatval(1-floatval(intval($code[0])/100));
				}
			}
			$discount = $pdetails[0]->discount;
		}else{
			$pamt = 0;
			$discount = 0;
		}
		$af = (trim($pdetails[0]->apply_type)=='0')? 1 : 0;
		
		$subject = 'Magnox Learning+ Learning Registration';
		$msg = '<!DOCTYPE html><html lang="en"><head><meta http-equiv="content-type" content="text/html;charset=utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /><meta content="width=device-width, initial-scale=1.0, shrink-to-fit=no" name="viewport" /><style>*{margin:0;padding:0;}body {font-size:14px;font-family: monospace;}table, th, td {border: 1px solid black;border-collapse: collapse;}th {text-align: left;}th, td {padding: 15px;}.container {width: 90%;margin: 0 auto;padding: 3rem 2rem;}</style></head><body><div class="container">Hello '.trim($ud[0]->first_name." ".$ud[0]->last_name).',<br><h4 style="text-center">Thank you for your registration in Magnox Learning +</h4>Your application has been '.(($apply_type==0)? 'approved':'saved').'. You application is as follow:<br><br><table width="100%"><tr><th width="20%">Program</th><td width="0%">'.trim($pdetails[0]->title).'</td></tr><tr><th width="20%">Duration</th><td width="0%">'.trim($pdetails[0]->duration).' '.trim($pdetails[0]->dtype).'(s)</td></tr><tr><th width="20%">Application Start Date</th><td width="0%">'.date('jS M Y',strtotime($pdetails[0]->start_date)).'</td></tr><tr><th width="20%">Program Administrator</th><td width="0%">'.trim($pdetails[0]->uname).'</td></tr><tr><th width="20%">Program details</th><td width="0%">For more details of the program, Please | <a href="'.base_url('programDetails/?id='.base64_encode($pdetails[0]->id)).'" target="_blank">Click Here</a></td></tr></table><br><br>Your Application is half way done. Please have patience for the approval in your dashboard in order to '.(($apply_type==0)? 'access the program':'complete your admission procedure').'.<br><br><br>Thanking you,<br>Magnox Learning Plus,<br>Learning and Course Management Platform</div></body></html>';
		
		if($this->MailSystem(trim($ud[0]->email), '', $subject, $msg)){
			$this->Member->deleteUserAcademic($id);
			for($i=1; $i<=$ecount; $i++){
				$data2[$i]['user_id'] = $userid;
				$data2[$i]['board'] = strtoupper(trim($_POST['board_'.$i]));
				$data2[$i]['organization'] = ucfirst(strtolower(trim($_POST['org_'.$i])));
				$data2[$i]['degree_id'] = trim($_POST['degree_'.$i]);
				$data2[$i]['aca_status'] = (isset($_POST['status_'.$i]))? 'Completed' : 'Present';
				if($data2[$i]['aca_status']=='Completed'){
					$data2[$i]['passout_year'] = trim($_POST['passout_'.$i]);
					$data2[$i]['marks_per'] = trim($_POST['marks_'.$i]);
				}
				$data2[$i]['create_date_time'] = date('Y-m-d H:i:s');
				
				if($data2[$i]['board']!=""){
					$this->LoginModel->insertUserAcademic($data2[$i]);
				}
			}
			
			if($this->LoginModel->insertStudProgAdm($pid, $userid, $pdetails[0]->aca_year, $enroll, $pamt, $discount, $af)){
				$this->session->set_flashdata('error', 'Your application has been accepted. Please wait for approval. Check your mail for further details.');
				
			}else{
				$this->session->set_flashdata('error', 'Your application failed to save. Please try again.');
			}
			/*$data3['prog_id'] = $pid;
			$data3['cand_id'] = $userid;
			$data3['approve_flag'] = 0;
			$data3['prog_status'] = 0;
			$data3['apply_datetime'] = date('Y-m-d H:i:s');
			$this->LoginModel->insertUserAdmission($data3);*/
			
		}else{
			$this->session->set_flashdata('error', 'Mail send error. Server Down. Please try again.');	
		}
		redirect(base_url().'Student/allPrograms');
	}
	public function progAdmission($pid=0)
	{
		$this->load->helper('string');
		$vcode = random_string('numeric', 6);
		$res = array('status'=>false, 'msg'=>"Something went wrong.", 'webURL'=>base_url());
		if($pid!=0){
			$userid = $_SESSION['userData']['userId'];
			$pdetails = $this->LoginModel->getProgramInfoById($pid);
			$apply_type = (int)$pdetails[0]->apply_type;
			$total_fee = (int)trim($pdetails[0]->total_fee);
			$name = $_SESSION['userData']['name'];
			$email = $_SESSION['userData']['email'];
			$mobile = $_SESSION['userData']['mobile'];
			
			$chk_adm = $this->Member->checkReduntAdmission($pid, $userid);
			if($chk_adm==0){
				$this->db->trans_off();
				$this->db->trans_start();
					$data3['prog_id'] = $pid;
					$data3['cand_id'] = $userid;
					$data3['approve_flag'] = ($apply_type=='0')? 1 : 0;
					$data3['prog_status'] = 0;
					$data3['apply_datetime'] = date('Y-m-d H:i:s');
					if($this->Exam_model->insertData('adm_can_apply', $data3)){
						if($apply_type=='0'){
							$data4['stud_id'] = $userid;
							$data4['prog_id'] = $pid;
							$data4['aca_yearid'] = $pdetails[0]->aca_year;
							$data4['admission_date'] = date('Y-m-d H:i:s');
							$data4['enrollment_no'] = trim($pdetails[0]->code).$userid;
							$data4['status'] = 0;
							$data4['totalfees'] = $total_fee;
							$data4['discount'] = (int)trim($pdetails[0]->discount);
							//$data4['apply_coupon'] = false;
							$data4['add_datetime'] = date('Y-m-d H:i:s');
							if(!$this->Exam_model->insertData('stud_prog_cons', $data4)){
								$this->db->trans_rollback();
							}
						}
					}else{
						$this->db->trans_rollback();
					}
				$this->db->trans_complete();
				if ($this->db->trans_status() === FALSE){
					$res['msg'] = 'Your application has failed. Please try again after sometime.';
				}else{
					$subject = 'Magnox Learning+ Student Registration';
					$msg = '<!DOCTYPE html><html lang="en"><head><meta http-equiv="content-type" content="text/html;charset=utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /><meta content="width=device-width, initial-scale=1.0, shrink-to-fit=no" name="viewport" /><style>*{margin:0;padding:0;}body {font-size:14px;font-family: monospace;}table, th, td {border: 1px solid black;border-collapse: collapse;}th {text-align: left;}th, td {padding: 15px;}.container {width: 90%;margin: 0 auto;padding: 3rem 2rem;}</style></head><body><div class="container">Welcome '.$name.',<br>';
					$msg .= '<h4>Your application is as follows:</h4><table width="100%"><tr><th width="20%">Program</th><td width="0%">'.trim($pdetails[0]->title).'</td></tr><tr><th width="20%">Duration</th><td width="0%">'.trim($pdetails[0]->duration).' '.trim($pdetails[0]->dtype).'(s)</td></tr><tr><th width="20%">Application Start Date</th><td width="0%">'.date('jS M Y',strtotime($pdetails[0]->start_date)).'</td></tr><tr><th width="20%">Program Administrator</th><td width="0%">'.trim($pdetails[0]->uname).'</td></tr><tr><th width="20%">Program details</th><td width="0%">For more details of the program, Please | <a href="'.base_url('programDetails/?id='.base64_encode($pdetails[0]->id)).'" target="_blank">Click Here</a></td></tr></table><br><br>';
					if(trim($pdetails[0]->feetype)=='Paid'){
						$msg .= '<br><br><h4>Log in to your account and complete some payment in-order to get final approval from the instructor.</h4>';
					}else{
						$msg .= '<br><br><h4>Log in to your account and await for approval from the instructor.</h4>';
					}
					$msg .= '<br><br>Thanking you,<br>Magnox Learning Plus,<br>Learning and Course Management Platform</div></body></html>';
					
					if($this->MailSystem($email, '', $subject, $msg)){
						$res['status'] = true;
						$res['msg'] = 'Your application has been saved. Please check your inbox/spam mail for further details.';
						$res['webURL'] = base_url('Student');
					}else{
						$res['status'] = true;
						$res['msg'] = 'Your application has been saved. Please log in to dashboard.';
						$res['webURL'] = base_url('Student');
					}
				}
			}else{
				$res['msg'] = 'You have already applied for this program.';
			}
		}
		echo json_encode($res);
	}
	
	public function liveClass()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Live Class | Magnox Learning+ - Student';
			$userid = $_SESSION['userData']['userId'];
			$cid = base64_decode(trim($_GET['id']));
			$data['progcourse'] = $this->Member->getProgramCourse($cid);
			$data['sch_class'] = $this->Course_model->getScheduleClassByCid($cid, $userid);
			$data['pstud'] = $this->Member->getAllRequestByIdRole($data['progcourse'][0]->pid, 'Student', null, 'accepted');
			$data['schClass'] = $this->Course_model->getScheduleClassById($cid);
			$data['pprof'] = $this->Member->getAllRequestByIdRole($data['progcourse'][0]->pid, 'Teacher', null, 'accepted');
			$data['cid'] = $cid;
			$data['user_id'] = $userid;
			$this->loadUserViews("live-class", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	
	public function getNotices()
	{
		$userid = $_SESSION['userData']['userId'];
		$notices = $this->Member->getStudentNotices($userid);
		echo json_encode($notices);
	}
	public function notices()
	{
		if($this->isLoggedIn()){
			$userid = $_SESSION['userData']['userId'];
			$this->global['pageTitle'] = 'Notices | Magnox Learning+ - Student';
			$data['notices'] = $this->Member->getAllNoticesByUid($userid);
			//$data['programs'] = $this->Member->getAllMyPrograms('Teacher', $userid);
			$this->loadUserViews("notice", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function noticeDetails($nid)
	{
		if($this->isLoggedIn()){
			$userid = $_SESSION['userData']['userId'];
			$this->global['pageTitle'] = 'Notices | Magnox Learning+ - Student';
			$data['ndetails'] = $this->Member->getNoticesBYid($nid);
			$this->loadUserViews("notice-details", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	
	public function getMenuContent()
	{
		$userid = $_SESSION['userData']['userId'];
		$page = trim($_GET['page']);
		$cid = trim($_GET['cid']);
		$data['cd'] = $this->Course_model->getCourseDetailsById($cid);
		$data['prog_id'] = trim($_GET['prog']);
		
		if($page=='lectures'){
			$data['clectures'] = $this->Course_model->getLecturessByCid($cid);
		}else if($page=='resources'){
			$data['cresource'] = $this->Course_model->getResourcesByCid($cid);
			$i=1;
			foreach($data['cresource'] as $row){
				$id = $row->sl;
				$data['crfiles'.$i] = $this->Course_model->getResourceFilesById($id);
				$i++;
			}
		}else if($page=='assignments'){
			$data['cassignment'] = $this->Course_model->getAssignmentByCid($cid);
			$i=1;
			foreach($data['cassignment'] as $row){
				$id = $row->sl;
				$data['cafiles'.$i] = $this->Course_model->getAssignmentFilesById($id);
				$i++;
			}
		}else if($page=='grade'){
			$data['title'] = 'Grades';
			$data['pcwt'] = $this->Course_model->getOnlySubjectNameByCid($cid);
			$data['setGrade'] = $this->Course_model->getAllAssignmentMarksByCid($cid, $userid);
		}else if($page=='quiz'){
			$data['title'] = 'Quiz';
		}else if($page=='schedule'){
			$data['title'] = 'Schedule Classes';
			$data['schClass'] = $this->Course_model->getScheduleClassById($cid);
		}else if($page=='doubts'){
			$data['title'] = 'Doubts';
			$data['stud_dbs'] = $this->Course_model->getStudentDoubtsById($cid, $userid);
			$data['schClass'] = $this->Course_model->getScheduleClassById($cid);
			$data['pprof'] = $this->Member->getAllRequestByIdRole($data['prog_id'], 'Teacher', null, 'accepted');
		}else if($page=='teachers'){
			$data['title'] = 'Teacher List';
			$data['pprof'] = $this->Member->getAllRequestByIdRole($data['prog_id'], 'Teacher', null, 'accepted');
		}else if($page=='attendance'){
			$data['title'] = 'Attendance';
		}

		return $this->loadUserCourse('course-'.$page, $data);
	}

	public function jobs()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Jobs | Magnox Learning+ - Student';
			$data['pjobs'] = $this->jm->getAllJobs(null);
			$this->loadUserViews("jobs", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function job_details()
	{
		if($this->isLoggedIn()){
			$id = base64_decode($_GET['id']);
			$data['job'] = $this->jm->getAllJobs($id);
			if(!empty($data['job'])){
				$this->global['pageTitle'] = trim($data['job'][0]->title).' | Magnox Learning+ - Student';
				$this->loadUserViews("jobs-details", $this->global, $data, NULL);
			}else{
				redirect(base_url('Student/jobs'));
			}
		}else{
			redirect(base_url());
		}
	}
	public function user_job_apply()
	{
		$res = array('type'=>"warning", 'title'=>"Error", 'msg'=>"Something went wrong.");
		if(isset($_GET['pnid'])){
			$userid = $_SESSION['userData']['userId'];
			$job_id = $_GET['pnid'];
			$result = $this->jm->setUserJobApply($job_id, $userid);
			if($result == 1){
				$res = array('type'=>"success", 'title'=>"Success!", 'msg'=>"You have successfull applied for this job.");
			}else if($result == 2){
				$res = array('type'=>"warning", 'title'=>"Warning", 'msg'=>"You have already applied for this job.");
			}
		}
		echo json_encode($res);
	}
	
	public function invitations()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Invitations | Magnox Learning+ - Student';
			$useremail = $_SESSION['userData']['email'];
			$data['invData'] = $this->Member->getUserInvitationsByEmail($useremail);
			$this->loadUserViews("program-invitation", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function applyInviteToPRogram()
	{
		$resp = array('status'=>false, 'msg'=>'Something went wrong.');
		$prog_id = trim($_POST['pid']);
		$stat = trim($_POST['stat']).'ed';
		$role = $_POST['role'];
		$userid = $_SESSION['userData']['userId'];
		$pdetails = $this->LoginModel->getProgramInfoById($prog_id);
		$ivnd = $this->Member->checkValidInvite($prog_id, $email, $role);
		$cids = $this->Member->getCourseIdsByProgid($prog_id);
		$ftype = trim($pdetails[0]->feetype);
		
		$chkUserRole = $this->Member->checkUserRoleOnProgram($prog_id, $role, $userid);
		$data['status'] = $stat;
		$data['program_id'] = $prog_id;
		$data['respond_datetime'] = date('Y-m-d H:i:s');
		if($this->Member->updateUserInvite($data, $_POST['puid'])){
			if($stat=='accepted'){
				if($chkUserRole<=0){
					$chkadm = $this->Member->checkRedundantADM($prog_id, $userid);
					if($chkadm<=0){
						$data1['program_id'] = $prog_id;
						$data1['role'] = $role;
						$data1['status'] = $stat;
						$data1['user_id'] = $userid;
						$data1['add_date'] = date('Y-m-d H:i:s');
						$data1['status_ch_date'] = date('Y-m-d H:i:s');
						
						$data2['prog_id'] = $prog_id;
						$data2['cand_id'] = $userid;
						$data2['prog_status'] = 0;
						$data2['apply_datetime'] = date('Y-m-d H:i:s');
						$data2['payment_status'] = false;
						
						$data3['stud_id'] = $userid;
						$data3['prog_id'] = $prog_id;
						$data3['aca_yearid'] = $ivnd[0]->acayear_id;
						$data3['admission_date'] = date('Y-m-d H:i:s');
						$data3['status'] = 0;
						$data3['enrollment_no'] = trim($ivnd[0]->enrollment_no);
						$data3['totalfees'] = (int)trim($pdetails[0]->total_fee);
						$data3['discount'] = (int)trim($pdetails[0]->discount);
						$data3['add_datetime'] = date('Y-m-d H:i:s');
						if($ftype=='Paid'){
							$data2['approve_flag'] = '1';
							$this->LoginModel->insertData('adm_can_apply', $data2);
							$this->LoginModel->insertData('stud_prog_cons', $data3);
							$resp = array('status'=>true, 'msg'=>'Please pay the amount Rs. '.trim($pdetails[0]->total_fee).' and await for final approval under `My Program Status`.');
						}else{
							$data2['approve_flag'] = '2';
							$this->LoginModel->insertData('pro_users_role', $data1);
							$this->LoginModel->insertData('adm_can_apply', $data2);
							$spc_id = $this->LoginModel->insertDataRetId('stud_prog_cons', $data3);
							
							$data4['stud_id'] = $userid;
							$data4['spc_id'] = $spc_id;
							$data4['roll_no'] = trim($ivnd[0]->roll_no);
							$data4['aca_year'] = date('Y');
							$data4['status'] = true;
							$data4['add_date'] = date('Y-m-d H:i:s');
							$sps_id = $this->LoginModel->insertDataRetId('stud_prog_state', $data4);
							if(!empty($cids)){
								$j=1;
								foreach($cids as $crow){
									$data5[$j]['sps_id'] = $sps_id;
									$data5[$j]['course_id'] = $crow->course_id;
									$data5[$j]['add_date'] = date('Y-m-d H:i:s');
									$this->LoginModel->insertData('stud_prog_course', $data5[$j]);
									$j++;
								}
							}
							$resp = array('status'=>true, 'msg'=>'A new program has been added to your dashboard.');
						}
					}else{
						$resp = array('status'=>true, 'msg'=>'You are have already applied for this program Check `My Program Status`.');
					}
					
				}else{
					$resp = array('status'=>true, 'msg'=>'You are already a Student under this program.');
				}
			}else{
				$resp = array('status'=>true, 'msg'=>'You have successfully rejceted the invitation.');
			}
		}
		echo json_encode($resp);
	}
	
	public function addDoubts()
	{
		$resp = array('status'=>'error', 'msg'=>'Something went wrong.');
		$data['program_sl'] = trim($_POST['sch_prog']);
		$data['course_sl'] = trim($_POST['sch_course']);
		$data['stud_sl'] = $_SESSION['userData']['userId'];
		$data['doubts'] = trim($_POST['dbt_details']);
		$data['fac_sl'] = trim($_POST['dbt_prof']);
		$data['schedule_class_sl'] = ($_POST['dbt_schc']!=null)? $_POST['dbt_schc'] : 0;
		$data['status'] = 'pending';
		$data['tdatetime'] = date('Y-m-d H:i:s');
		
		if($data['doubts']!=""){
			if($this->Course_model->insertStudentDoubts($data)){
				$resp = array('status'=>'success', 'msg'=>'has been submitted.');
			}
		}else{
			$resp = array('status'=>'error', 'msg'=>'is empty. Cannot add!');
		}
		
		echo json_encode($resp);
	}
	
	public function message()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Messages | Magnox Learning+ - Student';
			
			$this->loadUserViews("messages", $this->global, Null, NULL);
		}else{
			redirect(base_url());
		}
	}
	/******************************************************************************/
	public function allPrograms()
	{
		if($this->isLoggedIn()){
			$userid = $_SESSION['userData']['userId'];
			$this->global['pageTitle'] = 'All Programs | Magnox Learning+ - Student';
			$data['programs'] = $this->Member->getAllProgramsNotUser('Student', $userid);
			$i=1;
			foreach($data['programs'] as $row)
			{
				$pid = $row->id;
				$data['cpprof'.$i] = $this->Member->getTotalProgByUserRole('Teacher', null, $pid);
				$data['cpstud'.$i] = $this->Member->getTotalProgByUserRole('Student', null, $pid);
				$data['cpsub'.$i] = $this->Member->getTotalProgCourses($pid);
				$data['org'.$i] = $this->Member->getProgOrganizations($pid);
				$i++;
			}
			$this->loadUserViews("all-programs", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function requestPrograms()
	{
		if($this->isLoggedIn()){
			$userid = $_SESSION['userData']['userId'];
			$this->global['pageTitle'] = 'Status Role for Program | Magnox Learning+ - Student';
			//$data['rusers'] = $this->Member->getAllRoleStatusByIdRole($userid);
			$data['rusers'] = $this->Member->getUserApplyProgStatus($userid);
			$i=1;
			foreach($data['rusers'] as $row){
				$pid = $row->prog_id;
				$data['spcdata_'.$i] = $this->Member->getUserSPCData($userid, $pid);
				$i++;
			}
			$this->loadUserViews("program-request", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	
	public function applyRoleToPRogram()
	{
		$userid = $_SESSION['userData']['userId'];
		$role = trim($_POST['role']);
		$pid = trim($_POST['pid']);
		$chkApply = $this->Member->checkRedundLearningApply($userid, $pid);
		if($chkApply==0){
			$data3['prog_id'] = $pid;
			$data3['cand_id'] = $userid;
			$data3['approve_flag'] = 0;
			$data3['prog_status'] = 0;
			$data3['apply_datetime'] = date('Y-m-d H:i:s');
			
			echo ($this->LoginModel->insertUserAdmission($data3))? '1':'0';
		}else{
			echo '3';
		}
	}
	public function viewProgram()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Programs Details | Magnox Learning+ - Student';
			$data['userId'] = $_SESSION['userData']['userId'];
			$data['prog_id'] = base64_decode($_GET['id']);
			$data['prog'] = $this->Member->getProgramById($data['prog_id']);
			/*$data['institute'] = $this->Member->getProgramOrg($data['prog_id']);
			$data['stream'] = $this->Member->getProgramStreams($data['prog_id']);
			$data['pprof'] = $this->Member->getAllRequestByIdRole($data['prog_id'], 'Teacher', null, 'accepted');
			$data['procourse'] = $this->Course_model->getProgramCourses($data['prog_id']);;
			$data['cprof'] = $this->Member->getProgRoleRequest('Teacher', $data['prog_id'], $userId);
			$data['cstud'] = $this->Member->getProgRoleRequest('Student', $data['prog_id'], $userId);*/
			
			$this->loadUserViews("view-program", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function viewProgramDetails()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Programs Details | Magnox Learning+ - Student';
			$userId = $_SESSION['userData']['userId'];
			$data['prog_id'] = base64_decode($_GET['id']);
			/*$data['prog'] = $this->Member->getProgramById($data['prog_id']);
			$data['institute'] = $this->Member->getProgramOrg($data['prog_id']);
			$data['stream'] = $this->Member->getProgramStreams($data['prog_id']);
			$data['pprof'] = $this->Member->getAllRequestByIdRole($data['prog_id'], 'Teacher', null, 'accepted');
			$data['procourse'] = $this->Course_model->getProgramCourses($data['prog_id']);;*/
			//$data['cprof'] = $this->Member->getProgRoleRequest('Teacher', $data['prog_id'], $userId);
			//$data['cstud'] = $this->Member->getProgRoleRequest('Student', $data['prog_id'], $userId);
			$data['prog'] = $this->LoginModel->getProgramById($data['prog_id']);
			$data['institute'] = $this->Member->getProgramOrg($data['prog_id']);
			$data['stream'] = $this->Member->getProgramStreams($data['prog_id']);
			$data['pprof'] = $this->Member->getAllRequestByIdRole($data['prog_id'], 'Teacher', null, 'accepted');
			$data['sems'] = $this->LoginModel->getAllSemsByProgId($data['prog_id']);
			
			$id_set = array(0, (int)$data['prog_id']);
			$data['faq'] = $this->LoginModel->getAllFAQ($id_set);
			
			$countsms = count($data['sems']);
			if($countsms<=1){
				$data['procourse'] = $this->Course_model->getProgramCourses($data['prog_id']);
			}else{
				foreach($data['sems'] as $row){
					$sem_id = $row->id;
					$data['procourse_'.$i] = $this->LoginModel->getProgSemCourses($sem_id, $data['prog_id']);
					$i++;
				}
			}
			
			if(!empty($data['prog'][0]->prog_skills)){
				$skills_set = json_decode($data['prog'][0]->prog_skills);
				if(is_array($skills_set)){
					$data['skills'] = $this->LoginModel->getAllSkillByProgrm($skills_set);
				}
			}
			$data['why_learn'] = $this->LoginModel->getWhyLearnByProgId($data['prog_id']);
			
			$this->loadUserViews("view-programDetails", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	
	public function getProgMenuContent()
	{
		$page = $_GET['page'];
		$data['userId'] = $_SESSION['userData']['userId'];
		$data['prog_id'] = $_GET['prog_id'];
		$data['prog'] = $this->Member->getStudProgramById($data['prog_id'], $data['userId']);
		$data['progtype'] = 'certificate';
		$this->load->view('certify_stud_courses/'.$page, $data);
	}
	/*----------------------------------------------------------------------------------*/
	public function courseDetails()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Course Details | Magnox Learning+ - Student';
			$data['prog_id'] = base64_decode($_GET['prog']);
			$data['cid'] = base64_decode($_GET['cid']);
			$data['prog'] = $this->Member->getProgramById($data['prog_id']);
			$data['cdetails'] = $this->Course_model->getCourseDetailsById($data['cid']);
			
			$this->loadUserViews("course-details", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	
	/*public function viewLectures()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Course Lectures | Magnox Learning+ - Student';
			$data['prog_id'] = base64_decode($_GET['prog']);
			$data['cid'] = base64_decode($_GET['cid']);
			$data['prog'] = $this->Member->getProgramById($data['prog_id']);
			$data['cdetails'] = $this->Course_model->getCourseDetailsById($data['cid']);
			$data['clectures'] = $this->Course_model->getLecturessByCid($data['cid']);
			
			$this->loadUserViews("course-lectures", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}*/
	public function getLecture()
	{
		$clid = trim($_GET['clid']);
		echo json_encode($this->Course_model->getCLectureByID($clid));
	}
	public function getLectureFile()
	{
		$clid = trim($_GET['clid']);
		$lfile = $this->Course_model->getLectureFile($clid);
		$data['user_sl'] = $_SESSION['userData']['userId'];
		$data['lec_sl'] = $clid;
		$data['add_date'] = date('Y-m-d H:i:s');
		if($this->Course_model->insertLecDLrecord($data)){
			echo (file_exists('./uploads/courselra/'.$lfile[0]->file_name))? $lfile[0]->file_name : 0;
		}else{
			echo 0;
		}
	}
	/*public function viewResources()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Course Resource | Magnox Learning+ - Student';
			$data['prog_id'] = base64_decode($_GET['prog']);
			$data['cid'] = base64_decode($_GET['cid']);
			$data['prog'] = $this->Member->getProgramById($data['prog_id']);
			$data['cdetails'] = $this->Course_model->getCourseDetailsById($data['cid']);
			$data['cresource'] = $this->Course_model->getResourcesByCid($data['cid']);
			$i=1;
			foreach($data['cresource'] as $row){
				$id = $row->sl;
				$data['crfiles'.$i] = $this->Course_model->getResourceFilesById($id);
				$i++;
			}
			
			$this->loadUserViews("course-resource", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}*/
	public function getResource()
	{
		$crid = trim($_GET['crid']);
		echo json_encode($this->Course_model->getCResourceByID($crid));
	}
	public function getResourceYTLink()
	{
		$crid = trim($_GET['crid']);
		echo json_encode($this->Course_model->getCResourceFileByID($crid));
	}
	public function getResourceFiles()
	{
		$crid = trim($_GET['crid']);
		$rfile = $this->Member->getCResourceFileByID($crid);
		$data['user_id'] = $_SESSION['userData']['userId'];
		$data['res_sl'] = $crid;
		$data['add_date'] = date('Y-m-d H:i:s');
		if($this->Course_model->insertResDLrecord($data)){
			echo json_encode($rfile);
		}else{
			echo 0;
		}
	}
	/*public function viewAssignments()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Course Assignments | Magnox Learning+ - Student';
			$data['prog_id'] = base64_decode($_GET['prog']);
			$data['cid'] = base64_decode($_GET['cid']);
			$data['prog'] = $this->Member->getProgramById($data['prog_id']);
			$data['cdetails'] = $this->Course_model->getCourseDetailsById($data['cid']);
			$data['cassignment'] = $this->Course_model->getAssignmentByCid($data['cid']);
			/*$i=1;
			foreach($data['cassignment'] as $row){
				$id = $row->sl;
				$data['cafiles'.$i] = $this->Course_model->getAssignmentFilesById($id);
				$i++;
			}
			
			$this->loadUserViews("course-assignments", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}*/
	public function getAssignment()
	{
		$caid = trim($_GET['caid']);
		echo json_encode($this->Course_model->getCAssignmentByID($caid));
	}
	public function viewAssignmentDetails()
	{
		if($this->isLoggedIn()){
			$this->global['pageTitle'] = 'Course Assignments | Magnox Learning+ - Student';
			$aid = base64_decode($_GET['id']);
			$data['prog_id'] = base64_decode($_GET['prog']);
			$data['cid'] = base64_decode($_GET['cid']);
			//$data['cdetails'] = $this->Course_model->getCourseDetailsById($data['cid']);
			$data['cadetails'] = $this->Course_model->getCAssignmentByID($aid);
			foreach($data['cadetails'] as $row){
				$id = $row->sl;
				$data['cafiles'] = $this->Course_model->getAssignmentFilesById($id);
			}
			
			$this->loadUserViews("cassignment-details", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function getAssignmentSolutions()
	{
		$id = trim($_GET['id']);
		$userid = $_SESSION['userData']['userId'];
		$data['assgn_sub'] = $this->Course_model->getAssignmentSubmission($id, $userid);
		if(count($data['assgn_sub'])>0){
			$data['asubfiles'] = $this->Course_model->getAssignSubFiles($data['assgn_sub'][0]->sl);
		}
		return $this->load->view("users/assignment-solution", $data);
	}
	public function submitAssignment()
	{
		$assgn_id=0;
		$ass_sl = $_POST['ass_sl'];
		$pa = $this->Course_model->get_assignment_by_id($ass_sl);
		if($ass_sl!=0){
			$tmp = $_FILES['fl_link01']['tmp_name'];
			$ori = $_FILES['fl_link01']['name'];
			$rfile = str_replace(" ","_",strtolower(trim($pa[0]->title)."_".$_SESSION['userData']['name'])).'.'.pathinfo($ori, PATHINFO_EXTENSION);
			if($ori!=""){
				$this->db->trans_off();
				$this->db->trans_begin();

					$data['ass_sl'] = $ass_sl;
					$data['user_sl'] = $_SESSION['userData']['userId'];
					$data['status'] = 'Submitted';
					$data['details'] = trim($_POST['rdetails']);
					$data['tdate'] = date('Y-m-d H:i:s');
					$assgn_id = $this->Course_model->insertStudAssignment($data);
					if($assgn_id!=0){
						$data1['ass_sub_sl'] = $assgn_id;
						$data1['file_name'] = $rfile;
						$data1['tdate'] = date('Y-m-d H:i:s');
						
						if($this->Course_model->insertCRFiles('pro_ass_sub_files', $data1))
						{
							move_uploaded_file($tmp,'./uploads/stud_assign_sub/'.$rfile);
						}
						
						$data3['stud_sl'] = $_SESSION['userData']['userId'];
						$data3['pro_course_wt_sl'] = $pa[0]->pcw_sl;
						$data3['marks'] = 0;
						$data3['tdatetime'] = date('Y-m-d H:i:s');
						$data3['user_id'] = $pa[0]->user_id;
						$this->Exam_model->insertData('pro_stud_marks', $data3);
					}

				if ($this->db->trans_status() === FALSE) {
						$this->db->trans_rollback();
						echo false;
				}else{
						$this->db->trans_commit();
						echo true;
				}
				/*
				$data['ass_sl'] = $ass_sl;
				$data['user_sl'] = $_SESSION['userData']['userId'];
				$data['status'] = 'Submitted';
				$data['details'] = trim($_POST['rdetails']);
				$data['tdate'] = date('Y-m-d H:i:s');
				$assgn_id = $this->Course_model->insertStudAssignment($data);
				if($assgn_id!=0){
					$data1['ass_sub_sl'] = $assgn_id;
					$data1['file_name'] = $rfile;
					$data1['tdate'] = date('Y-m-d H:i:s');
					
					if($this->Course_model->insertCRFiles('pro_ass_sub_files', $data1))
					{
						move_uploaded_file($tmp,'./uploads/stud_assign_sub/'.$rfile);
					}
					
					$data2['course_sl'] = $pa[0]->course_sl;
					$data2['type'] = 'Assignment';
					$data2['subject'] = trim($pa[0]->title);
					$data2['user_id'] = $pa[0]->user_id;
					$data2['weightage'] = '40';
					$data2['full_marks'] = $pa[0]->user_id;
					$data2['serial'] = $ass_sl;
					$data2['tdatetime'] = date('Y-m-d H:i:s');
					$pcw_id = $this->Exam_model->insertDataRetId('pro_course_wt', $data2);
					
					$data3['stud_sl'] = $_SESSION['userData']['userId'];
					$data3['pro_course_wt_sl'] = $pcw_id;
					$data3['marks'] = 0;
					$data3['tdatetime'] = date('Y-m-d H:i:s');
					$data3['user_id'] = $pa[0]->user_id;
					$this->Exam_model->insertData('pro_stud_marks', $data3);
					
					echo true;
				}else{
					echo false;
				}*/
			}else{
				echo false;
			}
			
		}else{
			echo false;
		}
		
	}
	public function createAssignmentSubmission()
	{
		$assgn_id=0;
		$tmp = $_FILES['fl_link01']['tmp_name'];
		$ori = $_FILES['fl_link01']['name'];
		$rfile = uniqid().$ori;
		
		$data['ass_sl'] = trim($_POST['ass_sl']);
		$data['user_sl'] = $_SESSION['userData']['userId'];
		$data['status'] = 'Submitted';
		$data['details'] = trim($_POST['rdetails']);
		$data['tdate'] = date('Y-m-d H:i:s');
		$assgn_id = $this->Course_model->insertStudAssignment($data);
		if($assgn_id!=0){
			if($ori!=''){
				$data1['ass_sub_sl'] = $assgn_id;
				$data1['file_name'] = $rfile;
				$data1['tdate'] = date('Y-m-d H:i:s');
				
				if($this->Course_model->insertCRFiles('pro_ass_sub_files', $data1))
				{
					move_uploaded_file($tmp,'./uploads/stud_assign_sub/'.$rfile);
				}
			}
			$resp = array('status'=>'success', 'msg'=>'submission has been created.');
		}else{
			$resp = array('status'=>'error', 'msg'=>'submission could not be created.');
		}
		echo json_encode($resp);
	}
	public function cuCAssignmentFiles()
	{
		$assgn_id = trim($_POST['fassgn_id']);
		$counter = trim($_POST['afcount']);
		
		for($i=0; $i<=$counter; $i++)
		{
			$tmp = $_FILES['fl_link'.$i]['tmp_name'];
			$ori = $_FILES['fl_link'.$i]['name'];
			$rfile = uniqid().$ori;
			if($ori!=''){
				$data[$i]['ass_sub_sl'] = $assgn_id;
				$data[$i]['file_name'] = $rfile;
				$data[$i]['tdate'] = date('Y-m-d H:i:s');
				
				if($this->Course_model->insertCRFiles('pro_ass_sub_files', $data[$i]))
				{
					move_uploaded_file($tmp,'./uploads/stud_assign_sub/'.$rfile);
				}
			}
		}
		echo 1;
	}
	
	/*******************************************************************************/
	public function startAutoProgram()
	{
		$prog_id = base64_decode($_GET['id']);
		$userid = $_SESSION['userData']['userId'];
		$title = base64_decode($_GET['title']);
		$spc_id = $this->AutoProgramModel->get_stud_prog_cons_id($prog_id, $userid); 
		$auto_prog = $this->AutoProgramModel->get_auto_prog_ordered_list($prog_id);
		$data['status']='1';
		$data['admission_date'] = date('Y-m-d H:i:s');
		$where = 'prog_id='.$prog_id.' AND stud_id='.$userid.' AND sl='.$spc_id;
		$i=1;
		if($this->Exam_model->updateData('stud_prog_cons', $data, $where)){
			foreach($auto_prog as $row){
				$data1[$i]['stu_prog_self_sl'] = $spc_id;
				$data1[$i]['auto_prog_sl'] = $row->sl;
				$data1[$i]['status'] = 0;
				$this->Exam_model->insertData('stu_prog_self_details', $data1[$i]);
				$i++;
			}
			redirect(base_url().'Student/progressLesson/'.preg_replace("/[^\p{L}\p{N}]/u","_",strtolower($title)).'/'.$prog_id);
		}else{
			redirect(base_url('Student'));
		}
	}
	public function progressLesson($ptitle="",$prog_id=0)
	{
		
		if($this->isLoggedIn()){
			$userid = $_SESSION['userData']['userId'];
			$this->global['pageTitle'] = ucwords(str_replace("_"," ",$ptitle)).' | Magnox Learning+ - Student';
			$data['spc_id'] = $this->AutoProgramModel->get_stud_prog_cons_id($prog_id, $userid); 
			$data['prog_id'] = $prog_id;
			$data['sections'] = $this->AutoProgramModel->get_section($prog_id);
			$data['ltviewed'] = $this->AutoProgramModel->get_total_viewed_lessons($prog_id, $userid);
			$this->loadUserViews("program-lesson", $this->global, $data, NULL);
		}else{
			redirect(base_url());
		}
	}
	public function applyAutoProgram($prog_id="", $feetype="")
	{
		$userid = $_SESSION['userData']['userId'];
		$chk_adm = $this->Member->checkReduntAdmission($prog_id, $userid);
		$pdetails = $this->LoginModel->getProgramInfoById($prog_id);
		if($chk_adm<=0){
			if($feetype=='Free'){
				$data['prog_id'] = $prog_id;
				$data['cand_id'] = $userid;
				$data['approve_flag'] = 2;
				$data['apply_datetime'] = date('Y-m-d H:i:s');
				$adm_id = $this->Exam_model->insertDataRetId('adm_can_apply', $data);
				
				if($adm_id){
					$data2['prog_id'] = $prog_id;
					$data2['stud_id'] = $userid;
					$data2['status'] = 0;
					$data2['totalfees'] = $pdetails[0]->total_fee;
					$data2['discount'] = $pdetails[0]->discount;
					$data2['payment_status'] = '1';
					$data2['add_datetime'] = date('Y-m-d H:i:s');
					$this->Exam_model->insertData('stud_prog_cons', $data2);
					
					$subject = trim($pdetails[0]->title).' - Magnox Learning+ Registration';
					$msg = trim($pdetails[0]->welcome_msg);
					$this->MailSystem($_SESSION['userData']['email'], '', $subject, $msg);
					echo '4';
				}else{
					echo '3';
				}
			}else{
				echo '2';
			}
		}else{
			echo '1';
		}
	}
	public function payAutoProgram()
	{
		$data['inputs'] = $this->input->post();
		$data['razorpay_keys'] = array(
			'key_id' => 'rzp_live_jYfqswMGR7HXJj',
			'key_secret' => 'w2SilXGmAAK2aKnNMR6HVOqG'
		);
		$this->load->view('razorpay/auto-prog-pay', $data);
	}
	public function paymentAPSuccess()
	{
		$data['inputs'] = $this->input->post();
		$data['razorpay_keys'] = array(
			'key_id' => 'rzp_live_jYfqswMGR7HXJj',
			'key_secret' => 'w2SilXGmAAK2aKnNMR6HVOqG'
		);
		$this->load->view('razorpay/autoprog-payment-status', $data);
	}
	public function commitAPSuccess($prog_id=0)
	{
		$pdetails = $this->LoginModel->getProgramInfoById($prog_id);
		$subject = trim($pdetails[0]->title).' - Magnox Learning+ Registration';
		$msg = trim($pdetails[0]->welcome_msg);
		$this->MailSystem($_SESSION['userData']['email'], '', $subject, $msg);
		redirect(base_url().'Student/requestPrograms');
	}
	
	public function studCertificate()
	{
		$userid = $_SESSION['userData']['userId'];
		$this->global['pageTitle'] = 'Certificate of Completion | Magnox Learning+ - Student';
		$pid = base64_decode($_GET['id']);
		$data['timestmp'] = $this->Member->getStudProgCons($userid, $pid);
		$data['student_name'] = $_SESSION['userData']['name'];
		$data['prog'] = $this->Member->getProgramById($pid);
		$data['certificate'] = $this->Member->getCertificateInfoByPid($pid);
		$this->loadUserViews("stud-certificate", $this->global, $data, NULL);
	}
	/*=============================================================*/
	public function checkPayment()
	{
		$userid = $_SESSION['userData']['userId'];
		$data['spcid'] = $_GET['spcid'];
		$data['pamt'] = $_GET['pamt'];
		$prog_id = $_GET['prog_id'];
		$method = $_GET['method'];
		$data['spcdata'] = $this->Member->getUserSPCData($userid, $prog_id);
		$data['pdetails'] = $this->LoginModel->getProgramInfoById($prog_id);
		if($method==0){
			$this->load->view('users/full-payment', $data);
		}else if($method==1){
			$this->load->view('users/emi-payment', $data);
		}else{
			echo "<h4><strong>SIKE!</strong>, You got the wrong number.</h4>";
		}
		
	}
	public function payNow()
	{
		/*
		$data['razorpay_keys'] = array(
			'key_id' => 'rzp_test_kfpz0R8cBSF7cN',
			'key_secret' => 'JQbqDtueD55ZisE9YEFhDHEZ'
		);
		*/
		$data['inputs'] = $this->input->post();
		$data['razorpay_keys'] = array(
			'key_id' => 'rzp_live_6yAItA6B9AU7RM',
			'key_secret' => 'K0sVi6JLXFwx81RZwJdg6Fvb'
		);
		$this->load->view('razorpay/Razorpay', $data);
	}
	public function paymentSuccess()
	{
		$data['inputs'] = $this->input->post();
		$data['razorpay_keys'] = array(
			'key_id' => 'rzp_live_6yAItA6B9AU7RM',
			'key_secret' => 'K0sVi6JLXFwx81RZwJdg6Fvb'
		);
		$this->load->view('razorpay/Razorpay-payment-status', $data);
	}
}