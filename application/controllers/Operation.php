<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Operation extends BaseController {
    public function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		ini_set('memory_limit', '-1');
        $this->load->model('Operation_model','om');
	}

    private function imageResize($image,$file){
		$thumb['image_library'] = 'gd2';
		$thumb['source_image'] = './assets/img/'.$file.'/'.$image;
		$thumb['maintain_ratio'] = TRUE;
		$thumb['width']         = 500;
		$thumb['height']       = 500;
		$thumb['quality'] = '100%';
		$thumb['create_thumb'] = TRUE;
		$thumb['thumb_marker'] = '';
		$this->load->library('image_lib', $thumb);
		if($this->image_lib->resize()){
			return true;
		}else{
			return false;
		}
	}

    public function get_pagination(){
        $data['count']       = $_POST['count'];
        $data['page_number'] = $_POST['page_number'];
        $data['last_page']   = $_POST['last_page'];
        $data['func']        = $_POST['func'];
        return $this->load->view('operation/pagination', $data);
    }

    public function multiple_status_change(){
        $resp     = array('status'=>'', 'message' => '');
        $id       = $_POST['id'];
        $yn_valid = $_POST['yn_valid'];
        $job      = $_POST['job'];
        $data     = array();
        if($job === 'faq'){
            for($i=0; $i<count($id); $i++){
                $data[] = array(
                    'id' => (int)$id[$i],
                    'yn_valid' => ((int)$yn_valid[$i] === 1)?0 : 1,
                );
            }
            if($this->om->multiple_faq_status_change($data)){
                $resp['status'] = 'success';
                $resp['message'] = 'FAQ status change successfully';
            }else{
                $resp['status'] = 'success';
                $resp['message'] = 'FAQ status not change';
            }
        }
        
        elseif($job === 'project'){
            for($i=0; $i<count($id); $i++){
                $data[] = array(
                    'id' => (int)$id[$i],
                    'yn_valid' => ((int)$yn_valid[$i] === 1)?0 : 1,
                );
            }
            if($this->om->multiple_project_status_change($data)){
                $resp['status'] = 'success';
                $resp['message'] = 'Project status change successfully';
            }else{
                $resp['status'] = 'success';
                $resp['message'] = 'Project status not change';
            }
        }

        elseif($job === 'speak'){
            for($i=0; $i<count($id); $i++){
                $data[] = array(
                    'id' => (int)$id[$i],
                    'yn_valid' => ((int)$yn_valid[$i] === 1)?0 : 1,
                );
            }
            if($this->om->multiple_alumni_speak_status_change($data)){
                $resp['status'] = 'success';
                $resp['message'] = 'Alumni Speak status change successfully';
            }else{
                $resp['status'] = 'success';
                $resp['message'] = 'Alumni Speak status not change';
            }
        }

        elseif($job === 'instructor'){
            for($i=0; $i<count($id); $i++){
                $data[] = array(
                    'id' => (int)$id[$i],
                    'txt_active_flag' => ($yn_valid[$i] === 'Y')?'N' : 'Y',
                );
            }
            if($this->om->multiple_instructor_status_change($data)){
                $resp['status'] = 'success';
                $resp['message'] = 'Instructor status change successfully';
            }else{
                $resp['status'] = 'success';
                $resp['message'] = 'Instructor status not change';
            }
        }

        echo json_encode($resp);
    }

    public function single_status_change(){
        $resp     = array('status'=>'', 'message' => '');
        $id       = $_POST['id'];
        $yn_valid = $_POST['yn_valid'];
        $job      = $_POST['job'];
        $data     = array();
        if($job === 'faq'){
            $data = array(
                'yn_valid' => ((int)$yn_valid === 1)?0 : 1
            );
            if($this->om->single_faq_status_change($id, $data)){
                $resp['status'] = 'success';
                $resp['message'] = 'FAQ status change successfully';
            }else{
                $resp['status'] = 'success';
                $resp['message'] = 'FAQ status not change';
            }
        }
        
        elseif($job === 'project'){
            $data = array(
                'yn_valid' => ((int)$yn_valid === 1)?0 : 1
            );
            if($this->om->single_project_status_change($id, $data)){
                $resp['status'] = 'success';
                $resp['message'] = 'Project status change successfully';
            }else{
                $resp['status'] = 'success';
                $resp['message'] = 'Project status not change';
            }
        }

        elseif($job === 'speak'){
            $data = array(
                'yn_valid' => ((int)$yn_valid === 1)?0 : 1
            );
            if($this->om->single_alumni_speak_status_change($id, $data)){
                $resp['status'] = 'success';
                $resp['message'] = 'Alumni Speak  status change successfully';
            }else{
                $resp['status'] = 'success';
                $resp['message'] = 'Alumni Speak  status not change';
            }
        }

        elseif($job === 'instructor'){
            $data = array(
                'txt_active_flag' => ($yn_valid === 'Y')?'N' : 'Y'
            );
            if($this->om->single_instructor_status_change($id, $data)){
                $resp['status'] = 'success';
                $resp['message'] = 'Instructor  status change successfully';
            }else{
                $resp['status'] = 'success';
                $resp['message'] = 'Instructor  status not change';
            }
        }

        echo json_encode($resp);
    }

    public function get_add_edit_modal_body(){
        $prog_id      = base64_decode($_POST['prog_id']);
        $rid          = (!empty($_POST['rid']))?$_POST['rid'] : '';
        $data['page'] = $_POST['page'];
        $data['job']  = $_POST['job'];
        if($data['job'] === 'faq'){
            if(!empty($rid)){
                $data['title']  = 'Update FAQ';
                $data['save']   = 'add_edite_faq';
                $data['result'] = $this->om->get_faq_by_id($rid);
            }else{
                $obj               = array('id'=>'', 'program_id'=>$prog_id,'txt_question'=> '', 'txt_answer' => '');
                $data['title']     = 'Add FAQ';
                $data['save']   = 'add_edite_faq';
                $data['result'][0] = (object)$obj;
            }
        }
        
        elseif($data['job'] === 'project'){
            if(!empty($rid)){
                $data['title']  = 'Update Project';
                $data['save']   = 'add_edite_project';
                $data['result'] = $this->om->get_project_by_id($rid);
            }else{
                $obj            = array('id'=>'', 'program_id'=>$prog_id,'txt_project'=> '', 'txt_project_dtls' => '');
                $data['title']  = 'Add Project';
                $data['save']   = 'add_edite_project';
                $data['result'][0] = (object)$obj;
            }
        }
        
        elseif($data['job'] === 'speak'){
            if(!empty($rid)){
                $data['title']  = 'Update Alumni Speak';
                $data['save']   = 'add_edite_alumni_speak';
                $data['result'] = $this->om->get_alumni_speak_by_id($rid);
            }else{
                $obj            = array('id'=>'', 'program_id'=>$prog_id,'txt_name'=> '', 'txt_profile_pic' => '', 'txt_emailid' => '', 'num_mobile_no' => '', 'txt_organization' => '', 'txt_position' => '', 'txt_message' => '');
                $data['title']  = 'Add Alumni Speak';
                $data['save']   = 'add_edite_alumni_speak';
                $data['result'][0] = (object)$obj;
            }
        }

        elseif($data['job'] === 'instructor'){
            if(!empty($rid)){
                $data['title']  = 'Update Instructor';
                $data['save']   = 'add_edite_instructor';
                $data['rol_des'] = $this->om->get_instructor_role_designation();
                $data['result'] = $this->om->get_instructor_by_id($rid);
            }else{
                $obj            = array('id'=>'', 'txt_instructor_name'=> '', 'txt_instructor_role'=> '', 'txt_instructor_desg'=>'', 'txt_instructor_dtls'=>'','txt_profile_pic' => '', 'txt_emailid' => '', 'num_phone_no' => '');
                $data['title']  = 'Add Instructor';
                $data['save']   = 'add_edite_instructor';
                $data['rol_des'] = $this->om->get_instructor_role_designation();
                $data['result'][0] = (object)$obj;
            }
        }
        
        return $this->load->view('operation/modal-body', $data);
    }

    public function perform_add_edite_operation(){
        $resp = array('status'=>'', 'message' => '');
        if($_POST['job'] === 'faq_add_edit'){
            $rid      = (!empty($_POST['rid']))? $_POST['rid'] : '';
            $question = $_POST['question'];
            $answer   = $_POST['answer'];
            $pid      = $_POST['pid'];
            $data = array(
                'program_id' => (int)$pid,
                'txt_question' => $question, 
                'txt_answer' => $answer,
                'yn_valid' => 1,
                'dat_sys_date' => date('Y-m-d H:i:s') 
            );
            if($this->om->add_edit_faq($rid,$data)){
                if(!empty($rid)){
                    $resp['status'] = 'success';
                    $resp['message'] = 'FAQ successfully updated';
                }else{
                    $resp['status'] = 'success';
                    $resp['message'] = 'FAQ successfully added';
                }
            }else{
                $resp['status'] = 'danger';
                $resp['message'] = 'There is some server problem';
            }
        }
        
        elseif($_POST['job'] === 'project_add_edit'){
            $rid      = (!empty($_POST['rid']))? $_POST['rid'] : '';
            $title    = $_POST['title'];
            $details  = $_POST['details'];
            $pid      = $_POST['pid'];
            $data = array(
                'program_id' => (int)$pid,
                'txt_project' => $title, 
                'txt_project_dtls' => $details,
                'yn_valid' => 1,
                'dat_sys_date' => date('Y-m-d H:i:s') 
            );
            if($this->om->add_edit_project($rid,$data)){
                if(!empty($rid)){
                    $resp['status'] = 'success';
                    $resp['message'] = 'Project successfully updated';
                }else{
                    $resp['status'] = 'success';
                    $resp['message'] = 'Project successfully added';
                }
            }else{
                $resp['status'] = 'danger';
                $resp['message'] = 'There is some server problem';
            }
        } 

        elseif($_POST['job']==='alumni_speak_add_edit'){
            $rid          = (!empty($_POST['rid']))? $_POST['rid'] : '';
            $pid          = $_POST['pid'];
            $name         = $_POST['name'];
            $email        = $_POST['email'];
            $phone        = $_POST['phone'];
            $organization = $_POST['organization'];
            $position     = $_POST['position'];
            $message      = $_POST['message'];
            $preImage     = (!empty($_POST['preImage']))?$_POST['preImage'] : '';
            $image        = '';
            if(!empty($_FILES['img']['name'])){
                $pic = $_FILES['img']['name'];
                $config['upload_path']   = './assets/img/alumni/'; 
                $config['allowed_types'] = 'jpeg|jpg|png'; 
                $config['file_name']  = time().$pic;
                $this->load->library('upload', $config);
                if($this->upload->do_upload('img')){
                    $file = $this->upload->data();
                    print_r($file);
                    if($this->imageResize($file['file_name'], 'alumni')){
                        $image = $file['file_name'];
                        if(!empty($preImage)){
                            unlink(".{$preImage}");
                        }
                    }
                }
            }
            $data = array(
                'program_id' => (int)$pid,
                'txt_profile_pic'=>(!empty($image))?"/assets/img/alumni/{$image}" : $preImage,
                'txt_name'=> $name, 
                'txt_emailid' => $email, 
                'num_mobile_no'=> $phone, 
                'txt_organization'=> $organization, 
                'txt_position'=> $position, 
                'txt_message'=> $message,
                'yn_valid' => 1,
                'dat_sys_date' => date('Y-m-d H:i:s')
            );

            //print_r($data);exit;
            
            if($this->om->add_edit_alumni_speak($rid,$data)){
                if(!empty($rid)){
                    $resp['status'] = 'success';
                    $resp['message'] = 'Alumni Speak successfully updated';
                }else{
                    $resp['status'] = 'success';
                    $resp['message'] = 'Alumni Speak successfully added';
                }
            }else{
                $resp['status'] = 'danger';
                $resp['message'] = 'There is some server problem';
            }
        }

        elseif($_POST['job']==='instructor_add_edit'){
            $rid          = (!empty($_POST['rid']))? $_POST['rid'] : '';
            $name         = $_POST['name'];
            $email        = $_POST['email'];
            $phone        = $_POST['phone'];
            $role         = $_POST['role'];
            $designation  = $_POST['designation'];
            $details      = $_POST['details'];
            $preImage     = (!empty($_POST['preImage']))?$_POST['preImage'] : '';
            $image        = '';
            if(!empty($_FILES['img']['name'])){
                $pic = $_FILES['img']['name'];
                $config['upload_path']   = './assets/img/instructor/'; 
                $config['allowed_types'] = 'jpeg|jpg|png'; 
                $config['file_name']  = time().$pic;
                $this->load->library('upload', $config);
                if($this->upload->do_upload('img')){
                    $file = $this->upload->data();
                    print_r($file);
                    if($this->imageResize($file['file_name'], 'instructor')){
                        $image = $file['file_name'];
                        if(!empty($preImage)){
                            unlink(".{$preImage}");
                        }
                    }
                }
            }
            $data = array(
                'txt_profile_pic'=>(!empty($image))?"/assets/img/instructor/{$image}" : $preImage,
                'txt_instructor_name'=> $name, 
                'txt_emailid' => $email, 
                'num_phone_no'=> $phone, 
                'txt_instructor_role'=> $role, 
                'txt_instructor_desg'=> $designation, 
                'txt_instructor_dtls'=> $details,
                'txt_active_flag' => "Y"
            );
            // print_r($_FILES['img']);
            //print_r($data);exit;
            
            if($this->om->add_edite_instructor($rid,$data)){
                if(!empty($rid)){
                    $resp['status'] = 'success';
                    $resp['message'] = 'Alumni Speak successfully updated';
                }else{
                    $resp['status'] = 'success';
                    $resp['message'] = 'Alumni Speak successfully added';
                }
            }else{
                $resp['status'] = 'danger';
                $resp['message'] = 'There is some server problem';
            }
        }

        echo json_encode($resp);
    }
    
    public function get_faq_list(){
        if($this->isLoggedIn()){
            $prog_id           = (!empty($_POST['prog_id']))? base64_decode($_POST['prog_id']) : '';
            $page_number       = (!empty($_POST['page'])) ? $_POST['page'] : 1;
			$per_page_record   = ($_SESSION['userData']['utype'] === 'admin')?10 : 5;
			$row               = $this->om->get_fqa_total_row($prog_id);
			$last_page         = ceil($row/$per_page_record);
			if($page_number < 1):
				$page_number = 1;
			elseif($page_number > $last_page):
				$page_number = $last_page;
			endif;
			$start_record   = abs(($page_number - 1) * $per_page_record);
            $data['utype']  = $this->session->userdata('userData')['utype'];
            $data['result'] = $this->om->get_faq_list($prog_id, $start_record, $per_page_record);
            $data['page_number']   = $page_number;
            $data['last_page']     = $last_page;
            $data['slNo']   = ($per_page_record * ($page_number - 1)) + 1;
            $data['job']    = 'faq';
            return $this->load->view('operation/faq-list', $data);
        }else{
			redirect(base_url());
		}
    }
    
    public function get_project_list(){
        if($this->isLoggedIn()){
            $prog_id           = (!empty($_POST['prog_id']))? base64_decode($_POST['prog_id']) : '';
            $page_number       = (!empty($_POST['page'])) ? $_POST['page'] : 1;
			$per_page_record   = ($_SESSION['userData']['utype'] === 'admin')?10 : 5;
			$row               = $this->om->get_project_total_row($prog_id);
			$last_page         = ceil($row/$per_page_record);
			if($page_number < 1):
				$page_number = 1;
			elseif($page_number > $last_page):
				$page_number = $last_page;
			endif;
			$start_record   = abs(($page_number - 1) * $per_page_record);
            $data['utype']  = $this->session->userdata('userData')['utype'];
            $data['result'] = $this->om->get_project_list($prog_id, $start_record, $per_page_record);
            $data['page_number']   = $page_number;
            $data['last_page']     = $last_page;
            $data['slNo']   = ($per_page_record * ($page_number - 1)) + 1;
            $data['job']    = 'project';
            return $this->load->view('operation/project-list', $data);
        }else{
			redirect(base_url());
		}
    }
    
    public function get_alumni_speak_list(){
        if($this->isLoggedIn()){
            $prog_id           = (!empty($_POST['prog_id']))? base64_decode($_POST['prog_id']) : '';
            $page_number       = (!empty($_POST['page'])) ? $_POST['page'] : 1;
			$per_page_record   = 10;
			$row               = $this->om->get_alumni_speak_total_row();
			$last_page         = ceil($row/$per_page_record);
			if($page_number < 1):
				$page_number = 1;
			elseif($page_number > $last_page):
				$page_number = $last_page;
			endif;
			$start_record   = abs(($page_number - 1) * $per_page_record);
            $data['utype']  = $this->session->userdata('userData')['utype'];
            $data['result'] = $this->om->get_alumni_speak($start_record, $per_page_record);
            $data['page_number']   = $page_number;
            $data['last_page']     = $last_page;
            $data['slNo']   = ($per_page_record * ($page_number - 1)) + 1;
            $data['job']    = 'speak';
            return $this->load->view('operation/alumni-speak', $data);
        }else{
			redirect(base_url());
		}        
    }

    public function get_instructor_list(){
        if($this->isLoggedIn()){
            //$prog_id           = (!empty($_POST['prog_id']))? base64_decode($_POST['prog_id']) : '';
            $page_number       = (!empty($_POST['page'])) ? $_POST['page'] : 1;
			$per_page_record   = 10;
			$row               = $this->om->get_instructor_list();
			$last_page         = ceil($row/$per_page_record);
			if($page_number < 1):
				$page_number = 1;
			elseif($page_number > $last_page):
				$page_number = $last_page;
			endif;
			$start_record   = abs(($page_number - 1) * $per_page_record);
            $data['utype']  = $this->session->userdata('userData')['utype'];
            $data['result'] = $this->om->get_instructor_list($start_record, $per_page_record);
            $data['page_number']   = $page_number;
            $data['last_page']     = $last_page;
            $data['slNo']   = ($per_page_record * ($page_number - 1)) + 1;
            $data['job']    = 'instructor';
            return $this->load->view('operation/instructor-list', $data);
        }else{
			redirect(base_url());
		}        
    }


    public function get_unselected_instructor_by_program(){
        $prog_id = $_POST['id'];
        $result  = $this->om->get_unselected_instructor_by_program();
        $html    = '<option value=""> --- Select Instructor ---</option>';
        if(!empty($result)){
            foreach($result as $row){
                $html    .= '<option value="'.$row->id.'">'.$row->txt_instructor_name.'</option>';
            }
        }
        echo $html;
    }

    public function get_selected_instructor_by_program(){
        $prog_id = $_POST['id'];
        $data['result']  = $this->om->get_selected_instructor_by_program($prog_id);
        
        return $this->load->view('operation/instructor-by-program', $data);
    }


    public function map_instructor_with_program(){
        $res = array('message'=>'');
        $program = (int)$_POST['program_id'];
        $instructor = (int)$_POST['instructor_id'];
        $data = array(
            'program_id' => $program,
            'instructor_id' => $instructor,
            'date_time' => date('Y-m-d H:i:s')
        );
        
        if($this->om->map_instructor_with_program($data)){
            $res['message'] = 'Success';
        }else{
            $res['message'] = 'Faild';
        }
        echo json_encode($res);
    }

    public function delete_instructor_from_program(){
        $res = array('message'=>'');
        $id = (int)$_POST['id'];
        if($this->om->delete_instructor_from_program($id)){
            $res['message'] = 'Success';
        }else{
            $res['message'] = 'Faild';
        }
        echo json_encode($res);
    }

}