<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\SMTP;
    use PHPMailer\PHPMailer\Exception;

    require_once "vendor/autoload.php";
    require APPPATH . '/libraries/BaseController.php';


    class Veryficertificate extends BaseController {
        public function __construct(){
		    parent::__construct();
            $this->load->model('Verifycertificatemodel','vcm');
        }

        public function index(){
            $this->global['pageTitle'] = 'Certificate Verification | Magnox Learning+ - Verify Certificate';
            $data = array();
            

            if(get_cookie('set_cookie_id')){
                $this->loadVerifyCertificateViews("step_two", $this->global, $data, NULL);
            }else{
                $this->loadVerifyCertificateViews("index", $this->global, $data, NULL);
            }
            
        }

        public function get_verify_certificate_code_and_opt(){
            $certificate = trim($_POST['certificate']);
            $otp         = $_POST['otp'];
            $res         = array('key'=>'', 'message' => '', 'response' => array());
            if(get_cookie('set_cookie_otp') !== $otp){
                $res = array('key'=>'otp_error', 'message' => 'OTP is not matched');
            }else{
                $id = (int)get_cookie('set_cookie_id');
                $data = array('txt_certificate_no' => $certificate);
                $this->vcm->insertCertificateCode($id, $data);
                $result = $this->vcm->matchedCertificateCode($certificate);
                if(!empty($result)){
                    $res['key'] = 'code_success';
                    $res['response'] = array(
                        'sl' => $result[0]->sl,
                        'stud_id' => $result[0]->stud_id,
                        'prog_id' => $result[0]->prog_id,
                        'enrollment_no' => $result[0]->enrollment_no
                    );
                }else{
                    $res = array('key'=>'code_error', 'message' => 'wrong certificate code');
                }
            }
            
            echo json_encode($res);
        }

        private function messageBody($randomid){
            $html = '
                <div>
                    <p>Your OTP (One Time Password) for certificate verification is <strong>'.$randomid.'</strong>. This OTP will be valid for next 40 seconds.</p>
                    <p>
                        Thanks<br/>
                        Magnox Technology Private Ltd.
                    </p>
                </div>
            ';
            return $html;
        }

        public function first_step_varification(){
            $name   = $_POST['name'];
            $email  = $_POST['email'];
            $phone  = $_POST['phone'];
            $data   = array(
                'txt_user_name' => $name,
                'txt_emailid'   => $email,
                'num_mobile_no' => $phone,
                'dat_sys_date'  => date('Y-m-d H:i:s')
            );
            $result = $this->vcm->first_step_varification($data);
            if($result > 0){
                $randomid = rand(100000,999999);
                $subject = 'Certificate Verification OTP';
                $message = $this->messageBody($randomid);
                if($this->MailSystem($email, $subject, $message)){
                    set_cookie('set_cookie_id', $result,'600');
                    set_cookie('set_cookie_email', $email,'600');
                    set_cookie('set_cookie_otp',$randomid,'40');
                    redirect(base_url('Veryficertificate/index'));
                }
            }
        }

        public function resendOtp(){
            $randomid = rand(100000,999999);
            $res = array('key' => '0', 'status'=>'');
            
            $email = get_cookie('set_cookie_email');
            $subject = 'Certificate Verification OTP';
            $message = $this->messageBody($randomid);
            if($this->MailSystem($email, $subject, $message)){
                set_cookie('set_cookie_otp',$randomid,'40');
                $res['key'] = '1';
                echo json_encode($res);
            }
                       
        }

        public function get_certificate_details(){
            if(get_cookie('set_cookie_id') && get_cookie('set_cookie_email') && get_cookie('set_cookie_otp')){
                // $prog_id = (int)$_POST['prog_id'];
                // $stud_id = (int)$_POST['stud_id'];
                $prog_id = (int)base64_decode($_GET['prog_id']);
                $stud_id = (int)base64_decode($_GET['stud_id']);
                $code    = base64_decode($_GET['code']);
                delete_cookie('set_cookie_id');
                delete_cookie('set_cookie_email');
                delete_cookie('set_cookie_otp');
                $this->global['pageTitle'] = 'Schedule Class | Magnox Learning+ - Verify Certificate';
                $data['user_details'] = $this->vcm->get_user_details($stud_id);
                $data['program_details'] = $this->vcm->get_program_details($prog_id);
                $data['code'] = $code;
                //return $this->load->view('verify_certificate/details',$data);
                $this->loadVerifyCertificateViews("details", $this->global, $data, NULL);
            }else{
                redirect(base_url('Veryficertificate/index')); 
            }
        }

        

        private function MailSystem($to, $subject, $message){
            $email = "admin@billionskills.com";
            $password = "c6spQD82CVfch7aT";
    
            $mail = new PHPMailer(true);
            //Enable SMTP debugging.
            $mail->SMTPDebug = 0;                               
            //Set PHPMailer to use SMTP.
            $mail->isSMTP();            
            //Set SMTP host name  
            $mail->CharSet = 'utf-8';// set charset to utf8
            //$mail->Encoding = 'base64';
            $mail->SMTPAuth = true;// Enable SMTP authentication
            $mail->SMTPSecure = 'ssl';// Enable TLS encryption, `ssl` also accepted
    
            $mail->Host = 'lotus.arvixe.com';// Specify main and backup SMTP servers
            $mail->Port = 465;// TCP port to connect to
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );		                     
            //Provide username and password     
            $mail->Username = $email;                 
            $mail->Password = $password;                                                                         
    
            $mail->From = $email;
            $mail->FromName = 'Magnox+ Plus';
    
            $mail->addAddress($to);
    
            $mail->isHTML(true);
    
            $mail->Subject = $subject;
            $mail->Body = $message;
            //$mail->AltBody = "This is the plain text version of the email content";
    
            $mail->send();
		    $mail->clearAddresses();
		    return true;
        }

    }

?>