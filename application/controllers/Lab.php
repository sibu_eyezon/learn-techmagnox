<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use SphereEngine\Api\CompilersClientV4;
use SphereEngine\Api\SphereEngineResponseException;
use SphereEngine\Api\SphereEngineConnectionException;

require_once "vendor/autoload.php";
require APPPATH . '/libraries/BaseController.php';


class Lab extends BaseController {
    private $accessToken = '3614e9fc7e584ff572b7c036dffb3ecd';
    private $endpoint = 'a55efc0e.compilers.sphere-engine.com';	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('LabModel');
        $this->load->model('Member');
		// $this->load->model('Course_model');
		// $this->load->model('LoginModel');
		// $this->load->library('form_validation');
		date_default_timezone_set('Asia/Kolkata');
        ini_set('memory_limit', '-1');
    }

    public function index(){
        $lab_id = $_GET["id"];
        $userid = $_SESSION['userData']['userId'];
        $this->global['pageTitle'] = 'Dashboard | Magnox Learning+ - Teacher';
        $lab = $this->LabModel->getLab($lab_id);
		$data['programs'] = $this->Member->getAllMyPrograms('Teacher', $userid);
        $data['lab'] = $lab[0];
        $this->loadLabViews("lab_details", $this->global, $data, NULL);
    }

    public function use(){
        $lab_clone_id = $_GET["id"];
        $userid = $_SESSION['userData']['userId'];
        $utype = $_SESSION['userData']['utype'];
        $compilers = array(
            "C++" => 1,
            "Java" => 10,
            "Python" => 116,
            "PHP" => 29,
            "Ruby" => 17
        );

        $this->global['pageTitle'] = 'Dashboard | Magnox Learning+ - Use Lab';
        $lab = $this->LabModel->getCloneLab($lab_clone_id);
        $data['lab'] = $lab[0];
        $data['compilers'] = $compilers;
        $data['lab_clone_id'] = $lab_clone_id;
        if($utype == 'teacher'){
            $this->loadLabViews("lab_use", $this->global, $data, NULL);
        }else if($utype == 'student'){
            $this->loadUserLabViews("lab_use", $this->global, $data, NULL);
        }
        
    }

    public function testCode(){
        $this->global['pageTitle'] = 'Dashboard | Magnox Learning+ - Student';
        $this->loadUserViews("testcode", $this->global, NULL, NULL);
    }
    
    public function test(){
        $result=[];
        try{
            $client = new CompilersClientV4($this->accessToken, $this->endpoint);
            $response = $client->test();
            $result['status'] = "success";
            $result['response'] = $response;
        }catch(SphereEngineResponseException $e){
            $result['status'] = "error";
            $result['error_code'] = $e->getCode();
        }
        echo json_encode($result);
    }


    public function submitCode(){
        $src = $_POST['src'];
        $input = $_POST['input'];
        $compiler = $_POST['compiler'];
        $lab_clone_id = $_POST['lab_clone_id'];

        
        $data['lab_clone_id'] = $lab_clone_id;
        $data['user_id'] = $_SESSION['userData']['userId'];
        $data['user_type'] = $_SESSION['userData']['utype'];
        $data['code'] = $src;
        $data['start_datetime'] = date('Y-m-d H:i:s');
        $data['use_datetime'] = date('Y-m-d H:i:s');


        $result=[];
        try{
            $this->LabModel->addLabCloneUse($data);
            $client = new CompilersClientV4($this->accessToken, $this->endpoint);
            $response = $client->createSubmission($src, $compiler, $input);
            $result['status'] = "success";
            $result['response'] = $response;
        }catch(SphereEngineResponseException $e){
            $result['status'] = "error";
            $result['error_code'] = $e->getCode();
        }
        echo json_encode($result);
    }
    
    public function getSubmission(){
        $id = $_POST['id'];
        $result=[];
        try{
            $client = new CompilersClientV4($this->accessToken, $this->endpoint);
            $response = $client->getSubmission($id);
            $result['status'] = "success";
            $result['response'] = $response;
        }catch(SphereEngineResponseException $e){
            $result['status'] = "error";
            $result['error_code'] = $e->getCode();
        }
        echo json_encode($result);
    }

    public function getSubmissionStream(){
        $id = $_POST['id'];
        $stream = $_POST['stream'];
        $result=[];
        try{
            $client = new CompilersClientV4($this->accessToken, $this->endpoint);
            $response = $client->getSubmissionStream($id, $stream);
            $result['status'] = "success";
            $result['response'] = $response;
        }catch(SphereEngineResponseException $e){
            $result['status'] = "error";
            $result['error_code'] = $e->getCode();
        }
        echo json_encode($result);
    }

    public function allLabs(){
        $this->global['pageTitle'] = 'Dashboard | Magnox Learning+ - Teacher';
        $labs = $this->LabModel->getAllLabs();
        $data['labs'] = $labs;
        $this->loadLabViews("all_labs", $this->global, $data, NULL);
    }


    public function userLabs(){
        $this->global['pageTitle'] = 'Dashboard | Magnox Learning+ - Teacher';
        $userid = $_SESSION['userData']['userId'];
        $labs = $this->LabModel->getUserLabs($userid);
        $data['labs'] = $labs;        
        $this->loadUserLabViews("my_labs", $this->global, $data, NULL);        
    }

    public function myLabs(){
        $this->global['pageTitle'] = 'Dashboard | Magnox Learning+ - Teacher';
        $userid = $_SESSION['userData']['userId'];
        $data['labs'] = $this->LabModel->getMyLabs($userid);     
        $this->loadLabViews("my_labs", $this->global, $data, NULL);
    }



    public function cloneLab(){
        $result = array();
        $prog_id = $_POST['prog_id'];
        $lab_id = $_POST['lab_id'];
        $userid = $_SESSION['userData']['userId'];

        // check if already cloned
        $myLabs = $this->LabModel->getMyLabs($userid);

        $cloned = false;
        foreach ($myLabs as $lab) {
            if($lab->id == $lab_id && $lab->prog_id==$prog_id){
                $cloned = true;
                $result['status'] = 'error';
                $result['msg'] = "This lab is alreay cloned";
                break;
            }
        }

        if(!$cloned){
            $data['lab_id'] = $lab_id;
            $data['prog_id'] = $prog_id;
            $data['teacher_id'] = $userid;
            $data['add_datetime'] = date('Y-m-d H:i:s');

            try{
                $this->LabModel->cloneLab($data);
                $result['status'] = 'success';
            }catch(Exception $e){
                $result['status'] = 'error';
                $result['msg'] = $e->getMessage();
            }
        }
        
        echo json_encode($result);
    }
    
}