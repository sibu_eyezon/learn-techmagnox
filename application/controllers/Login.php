<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once "vendor/autoload.php";
require APPPATH . '/libraries/BaseController.php';

class Login extends BaseController {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('LoginModel');
		$this->load->model('Course_model');
		$this->load->model('Member');
		$this->load->library('form_validation');
		date_default_timezone_set('Asia/Kolkata');
		ini_set('memory_limit', '-1');
	}
	/********************************MENU*********************************/
	public function index()
	{
		$this->global['pageTitle'] = 'Login | Magnox Learning+';
		$this->loadViews("index", $this->global);
	}
	
	public function register($type)
	{
		$this->global['pageTitle'] = 'Registration | Magnox Learning+';
		$this->global['userType'] = $type;
		$this->loadViews("register", $this->global, NULL, NULL);
	}
	
	public function portfolio()
	{
		$this->global['pageTitle'] = 'Portfolio | Magnox Learning+';
		$this->loadViews("portfolio", $this->global, NULL, NULL);
	}
	
	public function programs()
	{
		$this->global['pageTitle'] = 'Programs | Magnox Learning+';
		$this->loadViews("programs", $this->global, NULL, NULL);
	}
	
	public function programAdmission()
	{
		$this->global['pageTitle'] = 'Program Admission | Magnox Learning+';
        $data['prog_id'] = base64_decode($_GET['id']);
		$data['prog'] = $this->LoginModel->getProgramById($data['prog_id']);
		$data['degree'] = $this->LoginModel->getAllDegrees();
		$data['streams'] = $this->LoginModel->getAllStreams();
		$this->loadViews("program-admission", $this->global, $data, NULL);
	}
	
	public function requestDemo()
	{
		$this->global['pageTitle'] = 'Request for Demo | Magnox Learning+';
		
		$this->loadViews("request-demo", $this->global, NULL, NULL);
	}
	
	public function contact()
	{
		$this->global['pageTitle'] = 'Contact Us | Magnox Learning+';
		
		$this->loadViews("contact", $this->global, NULL, NULL);
	}
	
	public function errors_404()
	{
		$this->isLoggedIn();
		$this->global['pageTitle'] = 'Magnox Learning+ | 404 - Page Not Found';
        
        $this->loadViews("errors_404", $this->global, NULL, NULL);
	}
	
	public function resetPassword()
	{
		$this->global['email'] = base64_decode($_GET['email']);
		$vcode = base64_decode($_GET['vercode']);
		$chkStatus = $this->LoginModel->checkChngPassStat($this->global['email'], $vcode);
		if($chkStatus[0]->flag!='f'){
			$this->session->set_flashdata('error', 'The link is already used, resend the lost password.');
			redirect(base_url().'login');
		}else{
			$this->global['pageTitle'] = 'Magnox Learning+ | Password Reset';
			$this->loadViews("password-reset", $this->global);
		}
	}
	
	public function pythonRegister()
	{
		$this->global['pageTitle'] = 'Introduction to Python - Registration | Magnox Learning+';
		
		$this->loadViews("prog-register", $this->global, NULL, NULL);
	}
	
	public function purdueRegister()
	{
		$this->global['pageTitle'] = 'Purdue - Registration | Magnox Learning+';
		
		$this->loadViews("purdue-register", $this->global, NULL, NULL);
	}
	
	public function tripura_registration()
	{
		$this->global['pageTitle'] = 'Tripura - Registration | Magnox Learning+';
		$data['progs'] = $this->LoginModel->getSelectivePrograms([47, 48, 49, 50, 54]);
		
		$i=1;
		foreach($data['progs'] as $row){
			$pid = $row->id;
			$data['ins_'.$i] = $this->Member->getProgramOrg($pid);
			$i++;
		}
		$this->loadViews("tripura-register", $this->global, $data, NULL);
	}
	/*********************************************************************/
	public function refund_policy()
	{
		$this->global['pageTitle'] = 'Refund Policy | Magnox Learning+';
		
		$this->loadViews("refund-policy", $this->global, NULL, NULL);
	}
	public function terms_conditions()
	{
		$this->global['pageTitle'] = 'Terms and Conditions | Magnox Learning+';
		
		$this->loadViews("terms-conditions", $this->global, NULL, NULL);
	}
	public function privacy()
	{
		$this->global['pageTitle'] = 'Privay Policy | Magnox Learning+';
		
		$this->loadViews("privacy", $this->global, NULL, NULL);
	}
	
	/*public function checkValidUserProgram()
	{
		$message = 'User already exist';
		
		$email = trim($_);
		
		$message = 'Invalid Coupon Code';
		$cp_code = trim($_POST['c_code']);
		$check_coupon = $this->LoginModel->getCouponByCode($cp_code);
		if(!empty($check_coupon)){
			if($check_coupon[0]->is_active=='t'){
				/*if($check_coupon[0]->coupon_type=='A'){
					$msg = $check_coupon[0]->discount_amt.'_A';
				}else{
					$msg = $check_coupon[0]->discount_percent.'_P';
				}
				$message = true;
			}
		}
		echo json_encode($message);
		exit;
	}*/
	
	/********************************LOGIN********************************/
	public function loginMe()
	{
		$resp = array('accessGranted' => false, 'utype'=>"", 'errors' => '');
		
		$email = strtolower(trim($_POST['username']));
		$password = md5(trim($_POST['password']));
		$grespond = $_POST['g-recaptcha-response'];
		if($grespond!=NULL){
			if($this->validCaptcha($grespond)){
				$user = $this->LoginModel->checkValidUser($email);
				if(count($user)>0){
					if($user[0]->verification_status=='t'){
						if($user[0]->password==$password){
							if($user[0]->status=='t'){
								if (!empty($_SERVER['HTTP_CLIENT_IP']))   
								{
									$ip_address = $_SERVER['HTTP_CLIENT_IP'];
								}else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))  
								{
									$ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
								}else
								{
									$ip_address = $_SERVER['REMOTE_ADDR'];
								}
								//echo $ip_address;
								$data['user_id'] = $user[0]->user_id;
								$data['user_email'] = $user[0]->email;
								$data['login_datetime'] = date('Y-m-d H:i:s');
								$data['ipaddress'] = trim($ip_address);
								$data['status'] = 1;
								$ltId = $this->LoginModel->insertDataRetId('login_track', $data);
								
								$name = trim($user[0]->first_name." ".$user[0]->last_name);
								$utype = trim($user[0]->user_type);
								$userdata = array(
									'userId'=>$user[0]->user_id,
									'name'=>$name,
									'email'=>trim($user[0]->email),
									'mobile'=>trim($user[0]->phone),
									'photo'=>trim($user[0]->photo_sm),
									'utype'=>$utype,
									'lt_id'=>$ltId,
									'isLoggedIn'=>true
								);
								$this->session->set_userdata('userData', $userdata);
								$resp['accessGranted'] = true;
								$resp['utype'] = ucfirst($utype);
							}else{
								$resp['errors'] = 'Your account has been suspended. Please contact the Admin.';
							}
						}else{
							$resp['errors'] = 'Password did not matched. Please try again.';
						}
					}else{
						$resp['errors'] = 'Please verify your email. Find verification link in your email.';
					}
				}else{
					$resp['errors'] = 'User does not exit. Please register.';
				}
			}else{
				$resp['errors'] = 'Invalid reCaptcha. Try again.';
			}
		}else{
			$resp['errors'] = 'Must check reCaptcha.';
		}
		
		echo json_encode($resp);
	}
	public function userProgLogin()
	{
		$resp = array('accessGranted' => false, 'utype'=>"", 'errors' => '');
		$email = strtolower(trim($_POST['username']));
		$password = md5(trim($_POST['password']));
		$prog_id = trim($_POST['prog_id']);
		$user = $this->LoginModel->checkValidUser($email);
		if(count($user)>0){
			if($user[0]->verification_status=='t'){
				if($user[0]->password==$password){
					if (!empty($_SERVER['HTTP_CLIENT_IP']))   
					{
						$ip_address = $_SERVER['HTTP_CLIENT_IP'];
					}else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))  
					{
						$ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
					}else
					{
						$ip_address = $_SERVER['REMOTE_ADDR'];
					}
					//echo $ip_address;
					$data['user_id'] = $user[0]->user_id;
					$data['user_email'] = $user[0]->email;
					$data['login_datetime'] = date('Y-m-d H:i:s');
					$data['ipaddress'] = trim($ip_address);
					$data['status'] = 1;
					$ltId = $this->LoginModel->insertDataRetId('login_track', $data);
					
					$name = trim($user[0]->first_name." ".$user[0]->last_name);
					$utype = trim($user[0]->user_type);
					$userdata = array(
						'userId'=>$user[0]->user_id,
						'name'=>$name,
						'email'=>trim($user[0]->email),
						'mobile'=>trim($user[0]->phone),
						'photo'=>trim($user[0]->photo_sm),
						'utype'=>$utype,
						'lt_id'=>$ltId,
						'isLoggedIn'=>true
					);
					$this->session->set_userdata('userData', $userdata);
					
					$prog = $this->LoginModel->getProgramById($prog_id);
					$uprogset = array('userId'=>$user[0]->user_id,'progId'=>$prog_id, 'progName'=>trim($prog[0]->title), 'status'=>false);
					set_cookie('userProgram', json_encode($uprogset), 5*60);
					
					$resp['accessGranted'] = true;
					$resp['utype'] = ucfirst($utype);
				}else{
					$resp['errors'] = 'Password did not matched. Please try again.';
				}
			}else{
				$resp['errors'] = 'Please verify your email. Find verification link in your email.';
			}
		}else{
			$resp['errors'] = 'User does not exit. Please register.';
		}
		echo json_encode($resp);
	}
	public function updatePassword()
	{
		$resp = array('accessGranted' => false, 'errors' => '');
		
		$vcode = trim($_POST['vcode']);
		$user_id = trim($_POST['email']);
		$password = md5(trim($_POST['password']));
		
		$data1['flag'] = true;
		$where1 = 'user_id='.$user_id.' AND verification_code='.$vcode;
		
		$data2['password'] = $password;
		$data2['modified_date_time'] = date('Y-m-d H:i:s');
		$where2 = 'user_id='.$user_id;
		$grespond = $_POST['g-recaptcha-response'];
		if($grespond!=NULL){
			if($this->validCaptcha($grespond)){
				if($this->LoginModel->updateData('b2c_verification_code', $data1, $where1)){
					$this->LoginModel->updateData('user_auth', $data2, $where2);
					$this->session->set_flashdata('error', 'Password has been changed, please log in.');
					$resp['accessGranted'] = true;
				}else{
					$this->session->set_flashdata('error', 'Password update failed, Server Error!!!. Please try again');
					$resp['errors'] = 'Password update failed, Server Error!!!. Please try again';
				}
			}else{
				$resp['errors'] = 'Invalid reCaptcha. Try again.';
			}
		}else{
			$resp['errors'] = 'Must check reCaptcha.';
		}
		
		echo json_encode($resp);
	}
	/********************************REGISTER*****************************/
	public function checkCouponStatus()
	{
		$message = 'Invalid Coupon Code';
		$cp_code = trim($_POST['c_code']);
		$check_coupon = $this->LoginModel->getCouponByCode($cp_code);
		if(!empty($check_coupon)){
			if($check_coupon[0]->is_active=='t'){
				/*if($check_coupon[0]->coupon_type=='A'){
					$msg = $check_coupon[0]->discount_amt.'_A';
				}else{
					$msg = $check_coupon[0]->discount_percent.'_P';
				}*/
				$message = true;
			}
		}
		echo json_encode($message);
		exit;
	}
	
	public function userAuthentication()
	{
		$resp = array('accessGranted' => false, 'errors' => '');
		$this->load->helper('string');
		$vcode = random_string('numeric', 6);
		$grespond = $_POST['g-recaptcha-response'];
		$weblink = '';
		
		$fname = ucwords(strtolower(trim($_POST['fname'])));
		$lname = ucwords(strtolower(trim($_POST['lname'])));
		$email = strtolower(trim($_POST['email']));
		$password = trim($_POST['passwd']);
		$phone = trim($_POST['phone']);
		$type = trim($_POST['user_type']);
		if($type=='teacher'){
			$data['organization'] = trim($_POST['org']);
			$data['designation'] = ucwords(strtolower(trim($_POST['designation'])));
			$data['website'] = strtolower(trim($_POST['weblink']));
		}

		$subject = 'Magnox Learning+ Registration';
		$msg = '<html><body>Hello {{name}},<br><br><h4 style="text-align:center">Thank you for Signing Up at learn.techmagnox.com</h4><hr><br>Please click <a href="'.base_url().'Login/userVerification/?email='.base64_encode($email).'&vercode='.base64_encode($vcode).'" target="_blank">here</a> to verify your email and complete your registration.<hr></body></html>';
		if($grespond!=NULL){
			if($this->validCaptcha($grespond)){
				if($email!=null){
					$user = $this->LoginModel->checkValidUser($email);
					if(count($user)<=0){
						
						$data['first_name'] = trim($fname);
						$data['last_name'] = trim($lname);
						$data['email'] = $email;
						$data['phone'] = $phone;
						$data['created_date_time'] = date('Y-m-d H:i:s');
						
						$data1['first_name'] = trim($fname);
						$data1['last_name'] = trim($lname);
						$data1['phone'] = $phone;
						$data1['email'] = $email;
						$data1['password'] = md5($password);
						$data1['verification_code'] = $vcode;
						$data1['verification_status'] = false;
						$data1['user_type'] = $type;
						$data1['create_date_time'] = date('Y-m-d H:i:s');
						
						$userid = $this->LoginModel->set_new_user_ret_id($data, $data1);
						if($userid != 0){
							
							$data2['event_name'] = 'STUDENT_REG';
							$data2['data'] = json_encode(['email'=>$email, 'name'=>$fname." ".$lname]);
							$data2['status'] = 'NEW';
							$data2['template_subject_text'] = $subject;
							$data2['template_body_text'] = $msg;
							$data2['message_subject'] = '';
							$data2['message_body'] = '';
							$data2['to_address'] = '';
							$data2['cc_address'] = '';
							$data2['retry_count'] = 0;
							$data2['add_datetime'] = date('Y-m-d H:i:s');
							$this->Exam_model->insertData('email_request', $data2);
							
							$resp['accessGranted'] = true;
						}else{
							$resp['errors'] = 'Registration failed, please try again.';
						}
					}else{
						$resp['errors'] = '<br>The email address already exits.';
					}
				}else{
					$resp['errors'] = 'Fields are empty.';
				}
			}else{
				$resp['errors'] = 'Invalid reCaptcha. Try Again.';
			}
		}else{
			$resp['errors'] = 'Must check reCaptcha.';
		}
		
		echo json_encode($resp);
	}
	public function userVerification()
	{
		$email = base64_decode($_GET['email']);
		$vercode = base64_decode($_GET['vercode']);
		$check = $this->LoginModel->checkVerifuStatus($email, $vercode);
		if($check!=1){
			if($this->LoginModel->updateVerifyStatus($email, $vercode)){
				$this->session->set_flashdata('success', 'Your registration has complete, please login.');
			}else{
				$this->session->set_flashdata('error', 'The email verification failed, please try again.');
			}
		}else{
			$this->session->set_flashdata('error', 'The email is already verified, please login.');
		}
		
		redirect(base_url());
	}
	
	public function progRegistration()
	{
		$userid = 0;
		$this->load->helper('string');
		$vcode = random_string('numeric', 6);
		$roll = random_string('numeric', 3);
		$grespond = $_POST['g-recaptcha-response'];
		
		$data['first_name'] = ucwords(strtolower(trim($_POST['fname'])));
		$data['last_name'] = ucwords(strtolower(trim($_POST['lname'])));
		$data['email'] = strtolower(trim($_POST['email']));
		$data['phone'] = trim($_POST['phone']);
		$type = trim($_POST['user_type']);
		$prog_id = $_POST['prog_id'];
		$pdetails = $this->LoginModel->getProgramInfoById($prog_id);
		$coupon_code = trim($_POST['valid_ccode']);
		$feetype = trim($pdetails[0]->feetype);
		if($feetype=='Paid'){
			$pamt = intval(trim($pdetails[0]->total_fee));
			if($coupon_code!='0'){
				$code = explode('_',$coupon_code);
				if($code[1]=='A'){
					$pamt = $pamt-intval($code[0]);
				}else if($code[1]=='P'){
					$pamt = $pamt*floatval(1-floatval(intval($code[0])/100));
				}
			}
			$discount = $pdetails[0]->discount;
		}else{
			$pamt = 0;
			$discount = 0;
		}
		
		//Intialization...
		$name = array($data['first_name'],$data['last_name']);
		$enroll = trim($pdetails[0]->code).$roll;
		$ivnd[0] = (object)array('user_email'=>$data['email'], 'phone'=>$data['phone']);
		//Mail Config...
		$subject = 'Magnox Learning+ Registration';
		$msg = '<html><body><h6 style="text-align:right"><img src="https://learn.techmagnox.com/assets/img/logo.png" style="width:90px;"/></h6>Hello '.trim($data['first_name']." ".$data['last_name']).',<br><br>';
		
		if($grespond!=NULL){
			if($this->validCaptcha($grespond)){
				if($data['first_name']!="" && $data['last_name']!="" && $data['email']!=""){
					$user = $this->LoginModel->checkValidUser($data['email']);
					if(!empty($user)){
						//Old User
						if($user[0]->verification_status=='t'){
							$userid = $user[0]->user_id;
						}else{
							$this->session->set_flashdata('errors', 'Complete your email verification in your mail inbox.');
						}
					}else{
						//New User
						$userid = $this->createNewUser($name, $ivnd, $vcode, 'Student');
						$msg.= '<h4 style="text-align:center">Thank you for Signing Up at <a href="https://learn.techmagnox.com/" target="_blank">learn.techmagnox.com</a></h4><hr><br>Please click <a href="'.base_url().'Login/userVerification/?email='.base64_encode($data['email']).'&vercode='.base64_encode($vcode).'" target="_blank">here</a> to verify your email and complete your registration.<hr><h5 style="text-align:center">Your password is <strong>'.$vcode.'</strong>.</h5>';
					}
					if($userid!=0){
						$chk_adm = $this->Member->checkReduntAdmission($prog_id, $userid);
						if($chk_adm==0){
							if($chk_adm==0){
								
								if($this->LoginModel->insertStudProgAdm($prog_id, $userid, $pdetails[0]->aca_year, $enroll, $pamt, $discount)){
									$msg.= '<h5 style="text-align:center">You have been enroll to the program: '.trim($pdetails[0]->title).'<br>Enrollment No: '.$enroll.'<br>Roll No: '.$roll.'</h5><hr><h5>Click <a href="'.base_url().'" target="_blank"><u>here</u></a> to start your program using your registered email and password.</h5><hr><br><small><code>This is an autogenerated message. Please do not reply.</code></small><br><br>Thanking you,<br><img src="https://learn.techmagnox.com/assets/img/logo.png" style="width:100px;"/><br>Magnox Learning Plus,<br>Learning and Course Management Platform<br><a href="mailto:sougam@techmagnox.com">sougam@techmagnox.com</a><br><a href="tel:9932242598">9932242598</a></body></html>';
									$this->MailSystem($data['email'], '', $subject, $msg);
									$this->session->set_flashdata('success', 'Your Application is saved. You have been enroll to the program: '.trim(pdetails[0]->title).'. Please check your mail for further details');
								}else{
									$this->session->set_flashdata('errors', 'Your application failed to save. Please try again.');
								}
							}else{
								$this->session->set_flashdata('errors', 'Your application failed to save. Please try again.');
							}
						}else{
							$this->session->set_flashdata('errors', 'This email is already registered and you have already applied for this program.');
						}
					}else{
						$this->session->set_flashdata('errors', 'Something went wrong with server. Please try again after sometime.');
					}
				}else{
					$this->session->set_flashdata('errors', 'Please fill out all * marked fields.');
				}
			}else{
				$this->session->set_flashdata('errors', 'Please fill out all * marked fields.');
			}
		}else{
			$this->session->set_flashdata('errors', 'Recaptcha validation failed.');
		}
		
		redirect(base_url('pythonRegister'));
	}
	public function purdueRegistration()
	{
		$this->load->helper('string');
		$vcode = random_string('numeric', 6);
		$roll = random_string('numeric', 3);
		$grespond = $_POST['g-recaptcha-response'];
		
		$data['first_name'] = ucwords(strtolower(trim($_POST['fname'])));
		$data['last_name'] = ucwords(strtolower(trim($_POST['lname'])));
		$data['email'] = strtolower(trim($_POST['email']));
		$data['phone'] = trim($_POST['phone']);
		$data['address'] = trim($_POST['address']);
		$data['organization'] = trim($_POST['org']);
		$data['country'] = ucwords(strtolower(trim($_POST['country'])));
		$data['gender'] = isset($_POST['gender'])? trim($_POST['gender']) : '';
		$data['category'] = isset($_POST['category'])? trim($_POST['category']) : '';
		
		$subject = 'SPARC 2021 Registration';
		$msg = '<html><body><h6 style="text-align:right"><img src="https://learn.techmagnox.com/assets/img/title.png" style="width:90px;"/></h6>Hello '.trim($data['first_name']." ".$data['last_name']).',<br><br>Thank you for the Registration into the Workshop Indo US SPARC online Workshop on APPLICATIONS OF ELECTROTHERAPY IN HEALTHCARE 
		on May 10-14 , 2021.<br>
		Organized by <a href="https://www.purdue.edu/" target="_blank">Purdue University</a> and <a href="http://www.iitkgp.ac.in/" target="_blank">IIT Kharagpur</a>
		<br>
		<br>
		You will be notified before the Workshop with more details<hr><br><small><code>This is an autogenerated message. Please do not reply.</code></small></body></html>';
		
		if($grespond!=NULL){
			if($this->validCaptcha($grespond)){
				if($data['first_name']!="" && $data['last_name']!="" && $data['email']!=""){
					$chk_user = $this->LoginModel->checkExistingUser($data['email']);
					if($chk_user==0){
						if($this->MailSystem($data['email'], '', $subject, $msg)){
							if($this->LoginModel->insertData('purdue_reg', $data)){
								$this->session->set_flashdata('success', 'Your Application is saved. You have successfull registered in the Purdue. Please check your mail for further details');
							}else{
								$this->session->set_flashdata('errors', 'Your application failed to save. Please try again.');
							}
						}else{
							$this->session->set_flashdata('errors', 'Sending mail error. Please try again.');
						}
					}else{
						$this->session->set_flashdata('errors', 'This email is already registered.');
					}
				}else{
					$this->session->set_flashdata('errors', 'Please fill out all fields.');
				}
			}else{
				$this->session->set_flashdata('errors', 'Recaptcha validation failed.');
			}
		}else{
			$this->session->set_flashdata('errors', 'Please check the recaptcha.');
		}
		redirect(base_url('purdueRegister'));
	}
	
	/********************************CRUD*********************************/
	public function receiveRequestDemo()
	{
		$resp = array('accessGranted' => false, 'errors' => '');
		
		$data['first_name'] = ucwords(strtolower(trim($_POST['fname'])));
		$data['last_name'] = ucwords(strtolower(trim($_POST['lname'])));
		$data['email'] = strtolower(trim($_POST['email']));
		$data['phone'] = trim($_POST['phone']);
		$data['job_title'] = ucwords(strtolower(trim($_POST['job_title'])));
		$data['company'] = trim($_POST['company']);
		$data['description'] = trim($_POST['desc']);
		$data['res_datetime'] = date('Y-m-d H:i:s');
		$data['rd_id'] = 'MLP_'.$data['first_name'][0].$data['last_name'][0].date('YmdHi');
		
		$subject = 'Magnox Learning+ Learning Request for demo';
		$msg = '<html><body>Hello '.trim($data['first_name']." ".$data['last_name']).',<br><br>Your request for demo has been received.<br>This is your request demo code: <strong>'.$data['rd_id'].'</strong>.<br>Please remember this code for further notice. We will get in touch with you within a period of time.<br><hr>
		<code>This is an autogenerated message. Please do not reply.</code></body></html>';
		
		if($this->MailSystem($email, '', $subject, $msg)){
			$this->LoginModel->insertData('request_demo', $data);
			$resp['accessGranted'] = true;
		}else{
			$resp['errors'] = 'Oops! Something went wrong. Please try again.';
		}
		echo json_encode($resp);
	}
	public function getProgramsFilter()
	{
		$ptitle = trim($_POST['title']);
		$cat = (isset($_POST['pcat']))? $_POST['pcat']: "";
		$i=1;
		$data['program'] = $this->LoginModel->getProgramsFiltered($ptitle, $cat);
		foreach($data['program'] as $row){
			$pid = $row->id;
			$data['ins_'.$i] = $this->Member->getProgramOrg($pid);
			$i++;
		}
		
		echo $this->load->view('home/programs_list', $data, true);
	}
	public function programDetails()
	{
		redirect(base_url());
		/*
		$i=1;
		$this->global['pageTitle'] = 'Program Details | Magnox Learning+';
        $data['prog_id'] = base64_decode($_GET['id']);
		$data['prog'] = $this->LoginModel->getProgramById($data['prog_id']);
		$data['institute'] = $this->Member->getProgramOrg($data['prog_id']);
		$data['stream'] = $this->Member->getProgramStreams($data['prog_id']);
		$data['pprof'] = $this->Member->getAllRequestByIdRole($data['prog_id'], 'Teacher', null, 'accepted');
		$data['sems'] = $this->LoginModel->getAllSemsByProgId($data['prog_id']);
		
		$id_set = array(0, (int)$data['prog_id']);
		$data['faq'] = $this->LoginModel->getAllFAQ($id_set);
		
		$countsms = count($data['sems']);
		if($countsms<=1){
			$data['procourse'] = $this->Course_model->getProgramCourses($data['prog_id']);
		}else{
			foreach($data['sems'] as $row){
				$sem_id = $row->id;
				$data['procourse_'.$i] = $this->LoginModel->getProgSemCourses($sem_id, $data['prog_id']);
				$i++;
			}
		}
		
		if(!empty($data['prog'][0]->prog_skills)){
			$skills_set = json_decode($data['prog'][0]->prog_skills);
			if(is_array($skills_set)){
				$data['skills'] = $this->LoginModel->getAllSkillByProgrm($skills_set);
			}
		}
		$data['why_learn'] = $this->LoginModel->getWhyLearnByProgId($data['prog_id']);
		
		$this->loadViews("program-details", $this->global, $data, NULL);*/
	}
	public function getContactUs(){
		$resp = array('status' => '', 'msg'=>'', 'class' => '');
		$first_name = $_POST['first_name'];
		$last_name  = $_POST['last_name'];
		$email      = $_POST['email'];
		$phone      = $_POST['phone'];
		$message    = $_POST['message'];
		$data       = array(
			'txt_name'      => "{$first_name} {$last_name}",
			'txt_emailid'     => $email,
			'num_mobile_no' => $phone,
			'txt_message'   => !empty($message) ? $message : '',
			'dat_update_date' => date('Y-m-d H:i:s')
		); 	
		if($this->LoginModel->getContactUs($data)){
			$resp['status'] = 'success';
			$resp['msg']    = 'Magnox will come back with valid proposal';
			$resp['class']  ='text-success';
		}else{
			$resp['status'] ='error';
			$resp['msg']    = 'Your message sending is failure';
			$resp['class']  ='text-danger';
		}	
		echo json_encode($resp);
	}
	
	public function userAdmission()
	{
		$respond = array('status'=>'error', 'title'=>"", 'msg'=>"", 'webURL'=>"");
		$this->load->helper('string');
		$vcode = random_string('numeric', 6);
		$user_exist = false;
		$userid = 0;
		
		$cp_code = (isset($_POST['c_code']))? trim($_POST['c_code']) : "";
		$email = strtolower(trim($_POST['email']));
		$password = trim($_POST['newpass']);
		$adm_type = trim($_POST['adm_type']);
		$type = 'student';
		$pid = trim($_POST['pid']);
		$pdetails = $this->LoginModel->getProgramInfoById($pid);
		$apply_type = (int)$pdetails[0]->apply_type;
		$total_fee = (int)trim($pdetails[0]->total_fee);
		
		$user = $this->LoginModel->checkValidUser($email);
		if(!empty($user)){
			$user_exist = true;
		}
		
		//case 1 if user exist but trying to register
		if($adm_type == 'register' && $user_exist){
			$respond['title'] = 'Error';
			$respond['msg'] = 'User already exit. Please try to log in.';
		}else if($adm_type == 'signin' && !$user_exist){
			$respond['title'] = 'Error';
			$respond['msg'] = 'User does not exit. Please try to sign up.';
		}else{
			if($adm_type == 'register'){
				$fname = $data['first_name'] = ucwords(strtolower(trim($_POST['fname'])));
				$lname = $data['last_name'] = ucwords(strtolower(trim($_POST['lname'])));
				$mobile = $data['phone'] = trim($_POST['mobile']);
				$data['email'] = $email;
				$data['created_date_time'] = date('Y-m-d H:i:s');
				
				$data1['first_name'] = trim($fname);
				$data1['last_name'] = trim($lname);
				$data1['email'] = $email;
				$data1['phone'] = $mobile;
				$data1['password'] = md5($password);
				$data1['verification_code'] = $vcode;
				$data1['verification_status'] = true;
				$data1['status'] = true;
				$data1['user_type'] = $type;
				$data1['create_date_time'] = date('Y-m-d H:i:s');
				
				$userid = $this->LoginModel->set_new_user_ret_id($data, $data1);
			}else if($adm_type == 'signin'){
				$userid = (isset($user[0]->user_id))? $user[0]->user_id : 0;
			}
			if($userid != 0){
				//check existing admission
				$chk_adm = $this->Member->checkReduntAdmission($pid, $userid);
				if($chk_adm == 0){
					if($cp_code!="" && $total_fee!=0){
						$check_coupon = $this->LoginModel->getCouponByCode($cp_code);
						if(!empty($check_coupon)){
							if($check_coupon[0]->is_active=='t'){
								if($check_coupon[0]->coupon_type=='A'){
									$da = (int)$check_coupon[0]->discount_amt;
									$total_fee = $total_fee-$da;
								}else{
									$dp = (int)$check_coupon[0]->discount_percent;
									$total_fee = $total_fee*(1-($dp/100));
								}
							}
						}
					}
					
					$data3['prog_id'] = $pid;
					$data3['cand_id'] = $userid;
					$data3['approve_flag'] = ($apply_type=='0')? 1 : 0;
					$data3['prog_status'] = 0;
					$data3['apply_datetime'] = date('Y-m-d H:i:s');
					
					$data4['stud_id'] = $userid;
					$data4['prog_id'] = $pid;
					$data4['aca_yearid'] = $pdetails[0]->aca_year;
					$data4['admission_date'] = date('Y-m-d H:i:s');
					$data4['enrollment_no'] = trim($pdetails[0]->code).$userid;
					$data4['status'] = 0;
					$data4['totalfees'] = $total_fee;
					$data4['discount'] = (int)trim($pdetails[0]->discount);
					//$data4['apply_coupon'] = false;
					$data4['add_datetime'] = date('Y-m-d H:i:s');
					
					if($this->LoginModel->setup_admission_for_user($data3, $apply_type, $data4)){
						
						$subject = 'Magnox Learning+ Student Registration';
						$msg = '<!DOCTYPE html><html lang="en"><head><meta http-equiv="content-type" content="text/html;charset=utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /><meta content="width=device-width, initial-scale=1.0, shrink-to-fit=no" name="viewport" /><style>*{margin:0;padding:0;}body {font-size:14px;font-family: monospace;}table, th, td {border: 1px solid black;border-collapse: collapse;}th {text-align: left;}th, td {padding: 15px;}.container {width: 90%;margin: 0 auto;padding: 3rem 2rem;}</style></head><body><div class="container"><img src="https://learn.techmagnox.com/assets/img/logo.png" width="100"/><br><br>Welcome {{name}},<br>';
						$msg .= '<h4>Your application is as follows:</h4><table width="100%"><tr><th width="20%">Program</th><td width="0%">'.trim($pdetails[0]->title).'</td></tr><tr><th width="20%">Duration</th><td width="0%">'.trim($pdetails[0]->duration).' '.trim($pdetails[0]->dtype).'(s)</td></tr><tr><th width="20%">Application Start Date</th><td width="0%">'.date('jS M Y',strtotime($pdetails[0]->start_date)).' at '.date('h:i A',strtotime($pdetails[0]->start_date)).'</td></tr><tr><th width="20%">Program Dashboard</th><td width="0%">Login | <a href="'.base_url().'" target="_blank">Here</a></td></tr></table><br><br>';
						if(trim($pdetails[0]->feetype)=='Paid'){
							$msg .= '<br><br><h4>Log in to your account and complete some payment in-order to get final approval from the instructor.</h4>';
						}else{
							$msg .= '<br><br><h4>Log in to your account and await for approval from the instructor.</h4>';
						}
						$msg .= '<br><br>Thanking you,<br>Magnox Learning Plus,<br>Learning and Course Management Platform</div></body></html>';
						
						/*==================================================
						if(!$this->MailSystem($email, '', $subject, $msg)){
							return true;
						}
						===================================================*/
						$data6['event_name'] = 'STUDENT_PROGRAM_REG';
						$data6['data'] = json_encode(['email'=>$email, 'name'=>$fname." ".$lname]);
						$data6['status'] = 'NEW';
						$data6['template_subject_text'] = $subject;
						$data6['template_body_text'] = $msg;
						$data6['message_subject'] = '';
						$data6['message_body'] = '';
						$data6['to_address'] = '';
						$data6['cc_address'] = '';
						$data6['retry_count'] = 0;
						$data6['add_datetime'] = date('Y-m-d H:i:s');
						$this->Exam_model->insertData('email_request', $data6);
						
						/*Login into the dashboard*/
						if (!empty($_SERVER['HTTP_CLIENT_IP']))   
						{
							$ip_address = $_SERVER['HTTP_CLIENT_IP'];
						}else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))  
						{
							$ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
						}else
						{
							$ip_address = $_SERVER['REMOTE_ADDR'];
						}
						//echo $ip_address;
						$data5['user_id'] = $userid;
						$data5['user_email'] = $email;
						$data5['login_datetime'] = date('Y-m-d H:i:s');
						$data5['ipaddress'] = trim($ip_address);
						$data5['status'] = 1;
						$ltId = $this->LoginModel->insertDataRetId('login_track', $data5);
						
						$name = trim($fname." ".$lname);
						$utype = $type;
						$userdata = array(
							'userId'=>$userid,
							'name'=>$name,
							'email'=>trim($email),
							'mobile'=>((!empty($user))? $user[0]->phone : $mobile),
							'photo'=>((!empty($user))? trim($user[0]->photo_sm) : 'assets/img/default-avatar.png'),
							'utype'=>$type,
							'lt_id'=>$ltId,
							'isLoggedIn'=>true
						);
						$this->session->set_userdata('userData', $userdata);
						$uprogset = array('userId'=>$userid, 'progId'=>$pid, 'progName'=>trim($pdetails[0]->title));
						set_cookie('userApplyProgram', json_encode($uprogset), 5*60);
						
						$respond['status'] = 'success';
						$respond['title'] = 'Success';
						$respond['msg'] = 'Your application has been saved. Please check your inbox/spam mail for further details.';
						$respond['webURL'] = 'Student';
					}else{
						$respond['title'] = 'Error';
						$respond['msg'] = 'Your application has failed. Please try again after sometime.';
					}
				}else{
					if (!empty($_SERVER['HTTP_CLIENT_IP']))   
					{
						$ip_address = $_SERVER['HTTP_CLIENT_IP'];
					}else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))  
					{
						$ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
					}else
					{
						$ip_address = $_SERVER['REMOTE_ADDR'];
					}
					//echo $ip_address;
					$data5['user_id'] = $userid;
					$data5['user_email'] = $email;
					$data5['login_datetime'] = date('Y-m-d H:i:s');
					$data5['ipaddress'] = trim($ip_address);
					$data5['status'] = 1;
					$ltId = $this->LoginModel->insertDataRetId('login_track', $data5);
					
					$name = trim($fname." ".$lname);
					$utype = $type;
					$userdata = array(
						'userId'=>$userid,
						'name'=>$name,
						'email'=>trim($email),
						'mobile'=>((!empty($user))? $user[0]->phone : $mobile),
						'photo'=>((!empty($user))? trim($user[0]->photo_sm) : 'assets/img/default-avatar.png'),
						'utype'=>$type,
						'lt_id'=>$ltId,
						'isLoggedIn'=>true
					);
					$this->session->set_userdata('userData', $userdata);
					
					$respond['title'] = 'Error';
					$respond['msg'] = 'Your application already exist. Please log in to your dashboard.';
					$respond['webURL'] = 'Student';
				}
				
			}else{
				$respond['title'] = 'Error';
				$respond['msg'] = ($adm_type == 'register')? 'Unable to register. Please try again after sometime' : 'Unable to get user details. Please try again after sometime';
			}
		}
		
		echo json_encode($respond);
	}
	
	public function inviteRespondStatus()
	{
		$userid = 0;
		$this->load->helper('string');
		$vcode = random_string('numeric', 6);
		$lname = '';
		$email = trim(base64_decode($_GET['email']));
		$prog_id = trim(base64_decode($_GET['prog']));
		$role = trim($_GET['role']);
		$stat = trim($_GET['status']);
		$user = $this->LoginModel->checkVerifyUser($email);
		$pdetails = $this->LoginModel->getProgramInfoById($prog_id);
		$ivnd = $this->Member->checkValidInvite($prog_id, $email, $role);
		$cids = $this->Member->getCourseIdsByProgid($prog_id);
		$name = explode(" ",trim($ivnd[0]->fullname));
		$ftype = trim($pdetails[0]->feetype);
		
		$subject = 'Magnox Learning+ Learning Invitation Status';
		$message = '<!DOCTYPE html><html lang="en"><head><meta http-equiv="content-type" content="text/html;charset=utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /><meta content="width=device-width, initial-scale=1.0, shrink-to-fit=no" name="viewport" /><style>*{margin:0;padding:0;}body {font-size:14px;font-family: monospace;}table, th, td {border: 1px solid black;border-collapse: collapse;}th {text-align: left;}th, td {padding: 15px;}.container {width: 90%;margin: 0 auto;padding: 3rem 2rem;}</style></head><body><div class="container">Hello '.trim($ivnd[0]->fullname).'<br>You have successfull '.$stat.' the invitation from '.trim($pdetails[0]->uname).' for the program: '.trim($pdetails[0]->title).' as a '.$role.'<br><br>';
		
		if(trim($ivnd[0]->status)=='pending'){
			$data['status'] = $stat;
			$data['respond_datetime'] = date('Y-m-d H:i:s');
			$where = "program_id=".$prog_id." and user_email='".$email."'";
			$this->LoginModel->updateData('pro_user_invite', $data, $where);
			if($stat=='accepted'){
				if(!empty($user)){
					//Existing User
					$userid = $user[0]->user_id;
				}else{
					//New User
					$userid = $this->createNewUser($name, $ivnd, $vcode, $role);
					$message.='Your account has been created. Please verify your email <a href="'.base_url().'Login/userVerification/?email='.base64_encode($email).'&vercode='.base64_encode($vcode).'" target="_blank">here</a> before you can log in to your dashboard.<br>Your username is '.$email.' and password is '.$vcode.'<br>';
				}
				if($userid!=0){
					$chkUserRole = $this->Member->checkUserRoleOnProgram($prog_id, $role, $userid);
					if($chkUserRole<=0){
						if($role=='Teacher'){
							$data1['program_id'] = $prog_id;
							$data1['role'] = $role;
							$data1['status'] = $stat;
							$data1['user_id'] = $userid;
							$data1['add_date'] = date('Y-m-d H:i:s');
							$data1['status_ch_date'] = date('Y-m-d H:i:s');
							$this->LoginModel->insertData('pro_users_role', $data1);
						}else if($role=='Student'){
							$chkadm = $this->Member->checkRedundantADM($prog_id, $userid);
							if($chkadm<=0){
								$data1['program_id'] = $prog_id;
								$data1['role'] = $role;
								$data1['status'] = $stat;
								$data1['user_id'] = $userid;
								$data1['add_date'] = date('Y-m-d H:i:s');
								$data1['status_ch_date'] = date('Y-m-d H:i:s');
								
								$data2['prog_id'] = $prog_id;
								$data2['cand_id'] = $userid;
								$data2['prog_status'] = 0;
								$data2['apply_datetime'] = date('Y-m-d H:i:s');
								$data2['payment_status'] = false;
								
								$data3['stud_id'] = $userid;
								$data3['prog_id'] = $prog_id;
								$data3['aca_yearid'] = $ivnd[0]->acayear_id;
								$data3['admission_date'] = date('Y-m-d H:i:s');
								$data3['status'] = 0;
								$data3['enrollment_no'] = trim($ivnd[0]->enrollment_no);
								$data3['totalfees'] = (int)trim($pdetails[0]->total_fee);
								$data3['discount'] = (int)trim($pdetails[0]->discount);
								$data3['add_datetime'] = date('Y-m-d H:i:s');
								if($ftype=='Paid'){
									$data2['approve_flag'] = '1';
									$this->LoginModel->insertData('adm_can_apply', $data2);
									$this->LoginModel->insertData('stud_prog_cons', $data3);
									$message.='The program fee is Rs. '.trim($pdetails[0]->total_fee).(($pdetails[0]->discount!=0)? ' with discount of '.$pdetails[0]->discount.'%' : '.').'<br>Please pay the amount and await for final approval.<br>';
								}else{
									$data2['approve_flag'] = '2';
									$this->LoginModel->insertData('pro_users_role', $data1);
									$this->LoginModel->insertData('adm_can_apply', $data2);
									$spc_id = $this->LoginModel->insertDataRetId('stud_prog_cons', $data3);
									
									$data4['stud_id'] = $userid;
									$data4['spc_id'] = $spc_id;
									$data4['roll_no'] = trim($ivnd[0]->roll_no);
									$data4['aca_year'] = date('Y');
									$data4['status'] = true;
									$data4['add_date'] = date('Y-m-d H:i:s');
									$sps_id = $this->LoginModel->insertDataRetId('stud_prog_state', $data4);
									if(!empty($cids)){
										$j=1;
										foreach($cids as $crow){
											$data5[$j]['sps_id'] = $sps_id;
											$data5[$j]['course_id'] = $crow->course_id;
											$data5[$j]['add_date'] = date('Y-m-d H:i:s');
											$this->LoginModel->insertData('stud_prog_course', $data5[$j]);
											$j++;
										}
									}
									$message.='Please log in to your dashboard and access the program utilities.';
								}
							}else{
								$data0['msg'] = 'You have already applied in the program.';
							}
						}
					}else{
						$data0['msg'] = 'You are already in the program.';
					}
				}
				$message.= '<br><br><br>Thanking you,<br><img src="https://learn.techmagnox.com/assets/img/logo.png" style="width:100px;"/><br>Magnox Learning Plus,<br>Learning and Course Management Platform</div></body></html>';
			
				$this->MailSystem($email, '', $subject, $message);
				$data0['msg'] = 'Your respond has been saved. Please check your email for further details.<br><code>Check your spam, if not found in inbox.</code>';
			}else if($stat=='rejected'){
				$data0['msg'] = 'You have successfull rejected the invitation.';
			}
		}else{
			$data0['msg'] = 'You have already responded to this invitation.';
		}
		$this->load->view("home/landing-page", $data0);
	}
	/********************************INTEGRATED***************************/
	public function createNewUser($name, $ivnd, $vcode, $role)
	{
		$userid = 0;
		$ncount = count($name);
				
		$data['first_name'] = trim($name[0]);
		for($i=1; $i<$ncount; $i++){
			$lname.= trim($name[$i])." ";
		}
		$data['last_name'] = trim($lname);
		$data['email'] = $ivnd[0]->user_email;
		$data['phone'] = $ivnd[0]->phone;
		$data['created_date_time'] = date('Y-m-d H:i:s');
		
		$userid = $this->LoginModel->insertUserDRetId($data);
		if($userid!=0){
			$data1['user_id'] = $userid;
			$data1['first_name'] = trim($name[0]);
			$data1['last_name'] = trim($lname);
			//$data1['gender'] = $gender;
			$data1['phone'] = $ivnd[0]->phone;
			$data1['email'] = $ivnd[0]->user_email;
			$data1['password'] = md5($vcode);
			$data1['verification_code'] = $vcode;
			$data1['verification_status'] = false;
			$data1['user_type'] = strtolower($role);
			$data1['create_date_time'] = date('Y-m-d H:i:s');
			$this->LoginModel->insertUserAData($data1);
		}
		return $userid;
	}
	public function validCaptcha($captcha)
	{
		//$captcha = $this->input->post('g-recaptcha-response');
		$response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LcAodEZAAAAANsalr7vBuB0aJvr6xy-uz84aVjN&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']);
        if ($response . 'success' == false) {
            return FALSE;
        } else {
            return TRUE;
        }
	}
	public function SendMailOTPConfirmation()
	{
		$response = array(
				'status'=>'error',
				'title'=>'Oops',
				'email'=>"", 
				'code'=>"",
				'msg'=>'Something went wrong!'
			);
		$this->load->helper('string');
		$code = random_string('numeric', 6);
		
		$value = trim($_GET['qemail']);
		
		if($value!=""){
			if(preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $value)){
				$user = $this->LoginModel->checkVerifyUser($value);
				if(count($user)==1){
					$phone = trim($user[0]->phone);
					$msg = '<html>
							<body>
							Respected '.$user[0]->first_name.',
							<p style="text-align:justify;margin-left:10px;">
								We recently received a request to recover
								the Magnox [Magnox Learning+] account: '.$value.'.<br>Please click on <a
								href="'.base_url().'Login/resetPassword/?email='.base64_encode($user[0]->user_id).'&vercode='.base64_encode($code).'"
								target="_blank">This Link</a> or enter the code: '.$code.' in the reset password page to change it.
								<br>
								</p>
								<code style="font-weight: 600;">[This is a computer generated email. ]</code>
							</body>
						</html>';
					
					$subject = "Reset Password";
					$data['user_id'] = $user[0]->user_id;
					$data['date_time'] = date('Y-m-d H:i:s');
					$data['verification_code'] = $code;
					$data['verification_type'] = 'fpass';
					$data['flag'] = false;
					
					if($phone!='' && strlen($phone)==10){
						$mail = $this->MailSystem($value, "", $subject, $msg);
						$sms = $this->SendOTPConfirmation($phone, $code);
						//echo json_encode($mail); exit;
						if($mail || $sms){
							$this->LoginModel->insertData('b2c_verification_code', $data);
							$response = array(
								'status'=>'success',
								'title'=>'OTP Send',
								'email'=>$user[0]->user_id, 
								'code'=>$code,
								'msg'=>'Check your mail or your phone to get OTP.'
							);
						}
					}else{
						$mail = $this->MailSystem($value, "", $subject, $msg);
						$this->LoginModel->insertData('b2c_verification_code', $data);
						$response = array(
							'status'=>'success',
							'title'=>'OTP Send',
							'email'=>$user[0]->user_id, 
							'code'=>$code,
							'msg'=>'Please check your mail and<br>update your mobile for future reference.'
						);
					}
					
				}else{
					$response = array(
						'status'=>'error',
						'title'=>'Invalid',
						'email'=>"", 
						'code'=>"",
						'msg'=>'Email not registered. Invalid User!'
					);
				}
			}else{
				$response = array(
					'status'=>'error',
					'title'=>'Invalid',
					'email'=>"", 
					'code'=>"",
					'msg'=>'Invalid Email!'
				);
			}
		}else{
			$response = array(
				'status'=>'error',
				'title'=>'Missing',
				'email'=>"", 
				'code'=>"",
				'msg'=>'Email value must be entered!'
			);
		}
		echo json_encode($response);
	}
	public function SendOTPConfirmation($phone, $code)
	{
		$authKey = "220681Ap3YnI61Qsx5b237012";
		$senderId = "Magnox Learning+";
		$url="http://api.msg91.com/api/sendhttp.php";
		$route = 4; //"default";
		
		$message = urlencode('Your OTP is '.$code);
		$postData = array(
			'authkey' => $authKey,
			'mobiles' => $phone,
			'message' => $message,
			'sender' => $senderId,
			'route' => $route
		);
		$ch = curl_init();
		curl_setopt_array($ch, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $postData
			//,CURLOPT_FOLLOWLOCATION => true
		));


		//Ignore SSL certificate verification
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


		//get response
		$output = curl_exec($ch);

		//Print error if any
		/*if(curl_errno($ch))
		{
			//echo json_encode(array('error'=>curl_error($ch)));
			$response = array(
				'status'=>'error',
				'title'=>'SMS Error',
				'msg'=>'SMS cannot be send!'
			);
		}*/
		$flag = (curl_errno($ch))? false : true;

		curl_close($ch);
		
		return $flag;
	}
	
	public function MailSystem($to, $cc, $subject, $message)
	{
		$email = "admin@billionskills.com";
        $password = "c6spQD82CVfch7aT";

		$mail = new PHPMailer(true);
		//Enable SMTP debugging.
		$mail->SMTPDebug = 0;                               
		//Set PHPMailer to use SMTP.
		$mail->isSMTP();            
		//Set SMTP host name  
		$mail->CharSet = 'utf-8';// set charset to utf8
		//$mail->Encoding = 'base64';
		$mail->SMTPAuth = true;// Enable SMTP authentication
		$mail->SMTPSecure = 'ssl';// Enable TLS encryption, `ssl` also accepted

		$mail->Host = 'lotus.arvixe.com';// Specify main and backup SMTP servers
		$mail->Port = 465;// TCP port to connect to
		$mail->SMTPOptions = array(
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
			)
		);		                     
		//Provide username and password     
		$mail->Username = $email;                 
		$mail->Password = $password;                                                                         

		$mail->From = $email;
		$mail->FromName = 'Magnox+ Plus';

		$mail->addAddress($to);

		$mail->isHTML(true);

		$mail->Subject = $subject;
		$mail->Body = $message;
		//$mail->AltBody = "This is the plain text version of the email content";

		return ($mail->send())? 1 : 0;
		//$mail->ErrorInfo;
	}
	/*****************************************************************/
}